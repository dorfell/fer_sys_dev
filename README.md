-------------------------------------------------------------------

     =========================================================
     fer_sys_dev - Développement d'un système pour applications FER
     =========================================================

                         Liste des fichiers
                         ---------------

Ce dépôt a des fichiers  qui sont utilisées pour le développement d'un 
système de Facial Expression Recognition (FER).

00_sw
 -Développement en software du système.

01_hw  
 - Développement en hardware de système.

docs  
 - Documentation utile pour le développement.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
