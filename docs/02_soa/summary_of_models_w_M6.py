# -*- coding: utf-8 -*-
## 
# @file    summary_of_models_w_M6.py 
# @brief   Plot SOA models metrics. 
# @details Plot the number of parameters, accuracy and size of
#          some models from the SOA.. 
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2023/03/17
# @version 0.1
"""@package docstring
"""


import matplotlib
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16});
plt.rc('text', usetex=True);
plt.rc('font', family='serif');


import pandas as pd
import seaborn as sns
#sns.set_theme(style="whitegrid")
#sns.set_theme(style="darkgrid");
sns.set_style("darkgrid", {'grid.linestyle': '--'});
sns.set_theme(font_scale=2);



# Data frame
df = pd.DataFrame(
  {
   'model_name': ["$I^2CNN$",   "$WMDNN+shallow~CNN$", "$WMDNN+Partial~VGG16$", "$Appearance feature-based CNN$", "$M6$"],
   'model': ["$A$",         "$B$",             "$C$",         "$D$", "$E$"],

   'grp1':  ["parameters", "parameters",      "parameters",  "parameters", "parameters"],
   'grp2':  ["size",       "size",            "size",        "size",       "size"],
   'grp3':  ["accuracy",   "accuracy",        "accuracy",    "accuracy",   "accuracy"],

   'par' :  [ 57882823,    41674118,           16902347,        68651442,  306182],
   'sz'  :  [ 221,         159,                64,              262,          1.2],
   'acc':   [ 75.3,        88.3,               90.9,            95.2,          94]

#   'model': ["$I^2CNN$",   "$WMDNN+shallow$", "$WMDNN+CNN$", "$kim2019$",
#             "$I^2CNN$",   "$WMDNN+shallow$", "$WMDNN+CNN$", "$kim2019$",
#             "$I^2CNN$",   "$WMDNN+shallow$", "$WMDNN+CNN$", "$kim2019$"],

#   'grp1':  ["parameters", "parameters",      "parameters",  "parameters",
#             "size",       "size",            "size",        "size",
#             "accuracy",   "accuracy",        "accuracy",    "accuracy"],

#   'par' :  [ 57882823,    41674118,           16902347,          999999,
#                   999,         999,                999,            9999,
#                   555,         555,                555,            555]
  }  );



#################################
# Plot window
#################################


# parameters plot
#################################
g = sns.catplot(kind='bar',  data=df, x='model', y='par', row='grp1',
                height=7, aspect=11.7/8.27,
                #hue='param', palette='Paired', dodge=False, sharex=False);
                #hue='param', palette='hls', dodge=False, sharex=False);
                hue='model', palette='tab10', dodge=False, sharex=True, sharey=False);

g.set_axis_labels("$CNN-based~models~in~literature$", "$\#parameters$");
g.set_titles(" ");
#g.fig.suptitle("figure title");
#g.set(ylim=(0, 70000000));
[plt.setp(ax.get_xticklabels(), rotation=0, ) for ax in g.axes.flat];
plt.yscale("log");


# iterate through axes
for ax in g.axes.ravel():
  # add annotations
  for c in ax.containers:
    labels = [f'{(v.get_height())/1000000:0.1f}M' for v in c];
    ax.bar_label(c, labels=labels, label_type='edge');
  ax.margins(y=0.2);



model_to_color = {
    "$A: I^2CNN~[6]$":        "tab:blue",
    "$B: WMDNN+shallow~[7]$": "tab:orange",
    "$C:WMDNN+CNN~[7]$":     "tab:green",
    "$D:Appearance~feature-based~CNN~[8]$": "tab:red",
    "$E:M6$":             "tab:purple"
};

patches = [matplotlib.patches.Patch(color=v, label=k) for k,v in model_to_color.items()];
matplotlib.pyplot.legend(handles=patches, loc="lower left");
#matplotlib.pyplot.legend(handles=patches, title="");




# size plot
#################################
h = sns.catplot(kind='bar',  data=df, x='model', y='sz', row='grp2',
                height=7, aspect=11.7/8.27,
                #hue='param', palette='Paired', dodge=False, sharex=False);
                #hue='param', palette='hls', dodge=False, sharex=False);
                hue='model', palette='tab10', dodge=False, sharex=True, sharey=False);

h.set_axis_labels("", "$sizes~(MB)$");
h.set_titles(" ");
#g.fig.suptitle("figure title");
#g.set(ylim=(0, 70000000));
[plt.setp(ax.get_xticklabels(), rotation=0, ) for ax in h.axes.flat];
plt.yscale("log");


# iterate through axes
for ax in h.axes.ravel():
  # add annotations
  for c in ax.containers:
    labels = [f'{(v.get_height()):0.1f}MB' for v in c];
    ax.bar_label(c, labels=labels, label_type='edge');
  ax.margins(y=0.2);



model_to_color = {
    "$A: I^2CNN$":        "tab:blue",
    "$B: WMDNN+shallow$": "tab:orange",
    "$C:WMDNN+CNN$":      "tab:green",
    "$D:Appearance~feature-based~CNN$": "tab:red",
    "$E:M6$":             "tab:purple"
};

patches = [matplotlib.patches.Patch(color=v, label=k) for k,v in model_to_color.items()];
#matplotlib.pyplot.legend(handles=patches, loc="center right");




# accuracy plot
#################################
i = sns.catplot(kind='bar',  data=df, x='model', y='acc', row='grp3',
                height=7, aspect=11.7/8.27,
                #hue='param', palette='Paired', dodge=False, sharex=False);
                #hue='param', palette='hls', dodge=False, sharex=False);
                hue='model', palette='tab10', dodge=False, sharex=True, sharey=False);

i.set_axis_labels("$CNN-based~models~in~literature$", "$Accuracies~(\%)$");
i.set_titles(" ");
#g.fig.suptitle("figure title");
#g.set(ylim=(0, 70000000));
[plt.setp(ax.get_xticklabels(), rotation=0, ) for ax in i.axes.flat];


# iterate through axes
for ax in i.axes.ravel():
  # add annotations
  for c in ax.containers:
    labels = [f'{(v.get_height()):0.1f}\%' for v in c];
    ax.bar_label(c, labels=labels, label_type='edge');
  ax.margins(y=0.2);



model_to_color = {
    "$A: I^2CNN~[6]$":        "tab:blue",
    "$B: WMDNN+shallow~[7]$": "tab:orange",
    "$C:WMDNN+CNN~[7]$":      "tab:green",
    "$D:Appearance~feature-based~CNN~[8]$": "tab:red",
    "$E:M6$":             "tab:purple"
};

patches = [matplotlib.patches.Patch(color=v, label=k) for k,v in model_to_color.items()];
matplotlib.pyplot.legend(handles=patches, loc="lower right");




plt.tight_layout();

plt.show()

