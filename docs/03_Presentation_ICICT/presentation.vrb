\vspace{0.5cm}
 %\frametitle{\textbf{Data set pre-processing}}
 \begin{center} \textbf{\slategray{Data set pre-processing}} \end{center}
 \begin{adjustbox}{max totalsize={1.1\textwidth}{1.1\textheight}, center}
  \begin{tikzpicture}[
    auto,
    start/.style    = { circle, draw=blue, thick, fill=blue!10,
                        text width=2em, text centered,
                        minimum height=2em, rounded corners },
    input/.style    = { trapezium, draw=blue, thick, fill=blue!10,
                        text width=6em, text centered,
                        minimum height=3em, rounded corners,
                        trapezium left angle=70, trapezium right angle=110},
    process/.style  = { rectangle, draw=blue, thick, fill=blue!10,
                        text width=12em, text centered,
                        minimum height=4em, rounded corners },
    decision/.style = { diamond, draw=blue, thick, fill=blue!10,
                        text width=10em, text centered,
                        minimum height=3em, rounded corners }
    ]


    % Define nodes in a matrix
    \matrix [column sep=5mm, row sep=10mm, anchor=center] {
      & \node [process] (org) {\textbf{Original image}\\       \includegraphics[scale=0.4]{./img/fer/jaffe_orig.png}\\  size: $256 \times 256$ px};            &
      & \node [process] (eye) {\textbf{Eyes Detection}\\       \includegraphics[scale=0.4]{./img/fer/jaffe_dlib.png}\\  $Dlib + Landmarks$};                   &
      & \node [process] (rot) {\textbf{Rotation      }\\       \includegraphics[scale=0.4]{./img/fer/jaffe_rot.png}\\   OpenCV: $getRotationMatrix2D$};        &
      & \node [process] (roi) {\textbf{Region of Interest}\\   \includegraphics[scale=0.4]{./img/fer/jaffe_roi.png}\\   Eliminate irrelevant information};     & \\
      & \node [process] (lbp) {\textbf{Local binary pattern}\\ \includegraphics[scale=0.177]{./img/fer/jaffe_lbp.png}\\ scikit-image: $LBP + var$};            &
      & \node [process] (his) {\textbf{Equalize Histogram}\\   \includegraphics[scale=0.4]{./img/fer/jaffe_his.png}\\   OpenCV: $EqualizeHist$};               &
      & \node [process] (res) {\textbf{Resize            }\\   \includegraphics[scale=0.4]{./img/fer/jaffe_res.png}\\   OpenCV: $resize$ $128 \times 128$ px}; & \\
    };

    % connect all nodes defined above
    \draw[ultra thick, ->,draw=black] (org) to (eye);
    \draw[ultra thick, ->,draw=black] (eye) to (rot);
    \draw[ultra thick, ->,draw=black] (rot) to (roi);
    \draw[ultra thick, ->,draw=black] (roi.south) to [out=270, in=0] (res.east);
    \draw[ultra thick, ->,draw=black] (res) to (his);
    \draw[ultra thick, ->,draw=black] (his) to (lbp);

  \end{tikzpicture}

\end{adjustbox}

 \vspace*{0.37cm} \hspace*{-0.7cm}
 \fcolorbox{white}{white}{\hbox to 5pc{\vbox to 5pc{
   \textcolor{slategray}{\tiny \textbf{$\bullet$}} \tiny L. Shan, D. Weihing, ``Deep facial expression recognition: A survey''. \url{https://arxiv.org/abs/1804.08348.}\\

   \textcolor{slategray}{\tiny \textbf{$\bullet$}} \tiny J.~Kim et al., ``Efficient FER Algorithm Based on Hierarchical DNN Structure,'' IEEE Access, 2019.
 }}}

