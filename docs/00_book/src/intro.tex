%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% XX Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{ \hfil  \hspace{2.0cm} INTRODUCTION \hfil}


\noindent %%%%%%%%%%%%% INTRODUCTION%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Facial expression recognition (FER) systems are essential
for applications such as human-computer communication,
animation, psychiatry, and automobile safety \cite{bet2012},
\cite{yan2017}, \cite{kim2019}.
However, due to the diversity in face phenotypes and variability
in the structure of facial expressions from person to person,
designing FER systems remains challenging. Additionally,
the image acquisition process is subject to factors that increase
the complexity of the classification problem, including poor
illumination, face rotations, and noise (e.g., hair, glasses).
Furthermore, there is a lack of guidelines and code examples in
the literature to provide training to designers.
FER systems typically use complex multi-channel
Machine Learning (ML) architectures suitable for implementation
with high-performance hardware, but not with embedded devices.
These ML algorithms include Convolutional Neural Networks (CNN),
known for their potential to extract features from the input
data. CNNs are used in several applications, like object detection
and semantic segmentation \cite{omni2021}, \cite{dpuframe2020}. Also,
they have been shown to increase the performance
of the traditional FER applications \cite{xie2019}, \cite{li2018}.\\


One of the open-source frameworks for developing
ML applications is TensorFlow \cite{tf2017}. When it's
used for training CNN models, the parameters obtained
are represented by floating-point numbers.
Unfortunately, keeping double precision on devices with limited
resources could be unfeasible. One of the strategies to successfully
execute the inference of CNNs on those devices is to represent
the model parameters and computations with integer numbers.
This could be achieved by means of techniques such as quantization,
available in the TensorFlow's library, tflite \cite{qat2022}.
Nonetheless, the process implies a trade-off between the model's
accuracy and its behavior in hardware.\\


On the other hand, embedded systems are commonly bound to the
inference stage of a ML application, because the training phase
is complex and entails a large number of memory and computational
resources. For instance, \cite{merenda2020} presents the challenges
that face IoT devices when used in ML scenarios.
However, training ML algorithms locally could be relevant in scenarios where
new data samples must be considered at run-time or where previously trained
models will be retrained, as in the Transfer Learning technique \cite{tltf2022}.
But the reports on training are scarce and evidence a gap in frameworks
for training CNNs in custom hardware-software architectures.\\
To work around the drawbacks presented above, the following objectives have been proposed:\\

\textbf{General Objective}\\
Propose a framework that allows to train and execute a CNN in an embedded system
(hardware-software), being able to use custom or the most popular software
libraries. The framework will create the files needed to describe the architecture
and will provide the tools for simulating and implementing it. Moreover, the
framework will give advice about the hardware-software trade-offs needed to
improve the performance.\\


\textbf{Specific Objectives}
\begin{itemize}
 \item[$\bullet$] Design and implement a basic FER application based on CNNs by
                  means of software libraries.
 \item[$\bullet$] Design and evaluate accelerators for implementing the CNN
                  feed forward execution in hardware.
 \item[$\bullet$] Design and validate a hardware-software architecture that
                  implement the training algorithm and fits in an autonomous
                  embedded system.
 \item[$\bullet$] Design and implement a framework that integrates the previously
                  defined objectives and employs a design space exploration to
                  propose the most cost-effective hardware.
 \item[$\bullet$] Evaluate the framework performance by means of metrics used
                  in the literature such as the neural network accuracy and
                  the execution time.
\end{itemize}



The thesis has the following structure: Chapter 1 presents a design methodology
for a single-channel CNN-based FER system, along with the image pre-processing,
the data augmentation, and the M6 CNN model trained with the JAFFE
data set \cite{jaf1998}.
Then, chapter 2 describes the quantization process applied to the M6 model
using tflite and the IP cores designed for the custom hardware-software
architecture to accelerate its inference in hardware.
Lastly, chapter 3 shows a base system designed for Pynq that runs a Jupyter
server on a Zybo-z7 development board and is capable of running the
ML frameworks TensorFlow and PyTorch. In addition, the Resiliency
framework is introduced. Resiliency puts together all the approaches used in
this work,  provides the design resources needed to train CNNs on embedded
systems, and gives guidelines for the design space exploration.
