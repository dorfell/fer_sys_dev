%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Facial Expression Recognition (FER) Systems}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Facial Expression Recognition (FER) Systems}
\label{ch:fer}
 
Facial expression recognition (FER) systems are examples of interaction technology 
based on Machine Learning (ML) and are fundamental for applications such as Human-Computer
Communication, animation, psychiatry, automobile safety, etc. \cite{bet2012}, \cite{udd2017}, \cite{yan2017}.
However, the design of FER systems is a challenging task due to the
different phenotypes around the world.


Besides, the acquisition process is subject to illumination conditions, face rotation,
noise (e.g., hair, glasses), etc., that increase the complexity of the classification
problem. Hence, it is recommended to preprocess the images before using ML algorithms
like Convolutional Neural Networks (CNNs). According to the literature, CNNs have
increased the performance of FER systems, making them an interesting study topic that
will be covered in this chapter.

First, the traditional FER system structure will be presented in section \ref{sc:fer_sys}.
Subsection \ref{ssc:fer_cnn} introduces the CNN-based FER systems; a recent strategy to
integrate ML and traditional FER systems.

Section \ref{sc:fer_ds} shows the data sets used for training and validating the
FER systems proposed in the literature. Section \ref{sc:fer_pre} depicts the basic data
set preprocessing, while section \ref{sc:fer_pre_ex} depicts the extended preprocessing:
Local Binary Pattern (LBP) and Data Augmentation (DA).


The design process of a single channel CNN-based FER system is described in section
\ref{sc:fer_des}, including some of the models tested and their learning curves.
The section concludes with M6, a feasible model for running inference on embedded systems.
Lastly, the discussion and conclusions can be found in sections \ref{sc:fer_dis} and
\ref{sc:fer_con}.

\cleardoublepage


% Facial Expression Recognition (FER) Systems
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Facial Expression Recognition (FER) Systems}
\label{sc:fer_sys}

Traditional FER systems had been implemented based on the detection of facial
actions associated with prototypical expressions like happiness, sadness,
anger, surprise, disgust, and fear. For instance, the Facial Action Coding
System (FACS) proposed in  1977 by Erman and Friesen \cite{bet2012},
associated these facial actions with expressions.
In FACS, individual or group changes in the facial muscles are coded as Action
Units (AUs). For instance, the action of raising the inner brow, caused by the 
Frontalis and Pars Medialis muscles, is known as AU1 while AU19 is the action 
of ``Tongue Out'' \cite{kan2001}.
Moreover, AUs can be additive when they appear independently and non-additive
when different AUs modify each other. Some AUs for the upper and lower face 
are shown in Figure \ref{fg:faceau}.


\begin{figure}[!htbp]
  \centering
  \subfloat[Upper face AUs.]{\includegraphics[scale=0.55]{./img/fer/upper_face_AU.png}}
  \hspace{0.5cm}
  \subfloat[Lower face AUs.]{\includegraphics[scale=0.55]{./img/fer/lower_face_AU.png}}
  \caption{Examples of face action units (AUs). Taken from \cite{kan2001}.}
  \label{fg:faceau}
\end{figure}

Unfortunately, FER systems based on traditional methods present drawbacks
due to the different ways AU changes from individual to individual, even
in the same expression. Moreover, the image acquisition process could lead
to different conditions of illumination, exposure, face angle, rotation, etc.
that could affect the classification accuracy of the FER system \cite{xie2019}.

On the other hand, CNNs have become a feasible alternative to implementing
FER systems, mainly because they are able to extract features from the input
data and train their  parameters by using those feature maps \cite{zha2017},
\cite{yan2017}, \cite{kim2019}, \cite{xie2019}.



% CNN-based Facial Expression Recognition (FER) Systems
%---------------------------------------------------
\subsection{CNN-based FER Systems}
\label{ssc:fer_cnn}
According to the literature, a CNN-based FER system can be composed of input
data preprocessing and the classification architecture. This architecture could
include one or more channels with the goal of increasing accuracy \cite{zha2017},
\cite{yan2017}, \cite{kim2019}, \cite{xie2019}.
For example, one channel could be a CNN while the other used a ML algorithm such
as a Support Vector Machine (SVM) or another CNN.

An extensive survey on FER can be found in \cite{li2018}. It covers the databases
available and their pre-processing, along with the deep networks used: CNNs,
Deep Belief Networks (DBN), Deep Autoencoder (DAE), Recurrent Neural Network (RNN),
and General Adversarial Network (GAN). Moreover, the work points out that deep
networks can underperform if the training data is insufficient.
Other works found are listed below.

In \cite{zha2017} authors proposed a single channel system called $I^2CNN$, which is shown in figure \ref{fg:cnn_fer}a. $I^2CNN$ uses five convolutional layers and one SVM and reaches an accuracy of $75\%$ with JAFFE and $98.3\%$ with CK+, both data sets using six emotions.
On the other hand, \cite{yan2017} presented a two-channel approach named WMDNN (see figure \ref{fg:cnn_fer}b) that integrates two CNNs: a partial VGG16 network and a shallow CNN. Its input were $72\times72$ pixels images from a Local Binary Pattern (LBP), and it achieved accuracy of $92.21\%$ with JAFFE and $97.02\%$ with CK+.

Another example of a two-channel FER system is \cite{kim2019} shown in figure \ref{fg:cnn_fer}c. This work integrates two CNNs: one using geometric features and the other using appearance features from LBP images. This system achieves $91.27\%$ with JAFFE and $96.46\%$ with CK+.


\begin{figure}[!htbp]
  \centering
  \subfloat[$I^2$CNN: One channel architecture. Taken from \cite{zha2017}.]{\includegraphics[scale=0.22]{./img/fer/fer_zhang2017.png}}\\
  \hspace{0.5cm}
  \subfloat[WMDNN: Two channels architecture. Taken from \cite{yan2017}.]{\includegraphics[scale=0.4]{./img/fer/fer_yang2017.png}}\\
  \hspace{0.5cm}
  \subfloat[Hierarchical DNN: Two channels architecture. Taken from \cite{kim2019}.]{\includegraphics[scale=0.35]{./img/fer/fer_kim2019.png}}\\
  \caption{Examples of CNN-based FER systems architectures proposed in literature.}
  \label{fg:cnn_fer}
\end{figure}


\cleardoublepage



% Data sets for FER systems
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data sets for FER systems}
\label{sc:fer_ds}

Data sets suitable for training FER systems are composed of several 
images that share characteristics like resolution, color, etc. 
Usually, these data sets must face problems such as \cite{bet2012}:
 
\begin{itemize}
 \item[$\bullet$] Emotions are expressed at different intensities for each subject.
 \item[$\bullet$] There is a loss of authenticity when the subjects are aware that they are being photographed.
 \item[$\bullet$] Subject's spontaneous expressions may be limited or inhibited by the environment.
\end{itemize}


Among all the different data sets available, the most popular in literature are the Cohn-Kanade data set, proposed in 1998 \cite{kan2001} and extended as CK+ in 2010 \cite{kan2010}, and the Japanese Female Facial Expression (JAFFE) data set, proposed in \cite{jaf1998} and \cite{jaf2020}.

The Cohn-Kanade data set (CK+) is composed of $500$ images from $100$ subjects with ages ranging from 18 to 30 years, and ethnicity is distributed as $15\%$ African-Americans and $3\%$ Asians and Latino-Americans. $65\%$ images are of women, and the data set includes only posed expressions. Images were taken using two cameras: one directly in front of the subject and the other positioned 30 degrees to the subject's right. However, the data set contains only the images taken from the frontal camera. Some images from the CK and CK+ data sets are shown in figure \ref{fg:fer_ds}a.\\


\begin{figure}[!htbp]
  \centering
  \subfloat[Example images from CK (1st row) and CK+ (2nd row). Taken from \cite{kan2010}.]{\includegraphics[scale=0.2]{./img/fer/ck_ex.png}}\\
  \subfloat[Example images from JAFFE. Taken from \cite{jaf2020}.]{\includegraphics[scale=0.25]{./img/fer/jaffe_ex.png}}\\
  \caption{Examples of data sets used in FER systems.}
  \label{fg:fer_ds}
\end{figure}


On the other hand, the Japanese Female Facial Expression (JAFFE) data set is composed of
$219$ images of $7$ facial expressions ($6$ basic facial expressions $+$ 1 neutral) with
only posed expressions from $10$ Japanese female models \cite{jaf1998}.
The photos have been taken under strictly controlled conditions with similar lighting
by using the acquisition setup presented in figure \ref{fg:fer_ds_acq}. Models have
tied their hair away from their faces to avoid hiding their expressions. Figure
\ref{fg:fer_ds}b shows some images from the JAFFE data set.


\begin{figure}[!htbp]
  \centering
  \includegraphics[scale=0.18]{./img/fer/jaffe_adq.png}
  \caption{Acquisition setup used for JAFFE. Taken from \cite{jaf2020}.}
  \label{fg:fer_ds_acq}
\end{figure}



% Basic data set pre-processing for FER systems
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Basic data set preprocessing for FER systems}
\label{sc:fer_pre}
Data set preprocessing can improve the FER system's accuracy by eliminating
irrelevant information such as the background, and adjusting parameters such
as the input size, resolution, etc.
Figure \ref{fg:ds_pre} summarizes the steps involved in basic data set preprocessing.\\


\begin{figure}[!htbp]
  %\centering
  %\hspace{-2.5cm}
  \begin{tikzpicture}[
    auto,
    start/.style    = { circle, draw=blue, thick, fill=blue!10, 
                        text width=2em, text centered,
                        minimum height=2em, rounded corners },
    input/.style    = { trapezium, draw=blue, thick, fill=blue!10, 
                        text width=6em, text centered,
                        minimum height=3em, rounded corners,
                        trapezium left angle=70, trapezium right angle=110},
    process/.style  = { rectangle, draw=blue, thick, fill=blue!10, 
                        text width=12em, text centered,
                        minimum height=4em, rounded corners },
    decision/.style = { diamond, draw=blue, thick, fill=blue!10, 
                        text width=10em, text centered,
                        minimum height=3em, rounded corners }                        
    ]

    
    % Define nodes in a matrix
    \matrix [column sep=5mm, row sep=10mm, anchor=center] {
      & \node [process] (org) {\textbf{Original image}\\     \includegraphics[scale=0.4]{./img/fer/jaffe_orig.png}\\ size: $256 \times 256$ px};            & 
      & \node [process] (eye) {\textbf{Eyes Detection}\\     \includegraphics[scale=0.4]{./img/fer/jaffe_dlib.png}\\ $Dlib + Landmarks$};                   & 
      & \node [process] (rot) {\textbf{Rotation      }\\     \includegraphics[scale=0.4]{./img/fer/jaffe_rot.png}\\  OpenCV: $getRotationMatrix2D$};        & \\
      & \node [process] (his) {\textbf{Equalize Histogram}\\ \includegraphics[scale=0.4]{./img/fer/jaffe_his.png}\\  OpenCV: $EqualizeHist$};               & 
      & \node [process] (res) {\textbf{Resize            }\\ \includegraphics[scale=0.4]{./img/fer/jaffe_res.png}\\  OpenCV: $resize$ $128 \times 128$ px}; & 
      & \node [process] (roi) {\textbf{Region of Interest}\\ \includegraphics[scale=0.4]{./img/fer/jaffe_roi.png}\\  Eliminate irrelevant information};     & \\ 
    };
    
    % connect all nodes defined above
    \draw[ultra thick, ->,draw=black] (org) to (eye);
    \draw[ultra thick, ->,draw=black] (eye) to (rot);
    \draw[ultra thick, ->,draw=black] (rot.south) to (roi.north);
    \draw[ultra thick, ->,draw=black] (roi) to (res);
    \draw[ultra thick, ->,draw=black] (res) to (his);
        
  \end{tikzpicture}
  \caption{JAFFE Pre-processing for FER. Adapted from \cite{kha2021}.}
  \label{fg:ds_pre}
\end{figure}


The original image has a size of $256 \times 256$ pixels and comes directly from the 
data set. Faces are commonly detected by using algorithms such as the Adaboost 
learning algorithm, proposed by Viola and Jones in 2004 \cite{vio2004}.
In this work, the landmarks released by Davis King in \cite{kng2021} and first 
published in \cite{sag2013}, are employed (see figure \ref{fg:landmarks}).
These landmarks are read using a C++ toolkit for ML algorithms known as Dlib \cite{dlib2009}.


\begin{figure}[!htbp]
  \centering
  \includegraphics[scale=0.15]{./img/fer/figure_68_markup.png}
  \caption{Facial Point Annotations. Taken from \cite{sag2013}.}
  \label{fg:landmarks}
\end{figure}


The Dlib methods used are $get\_frontal\_face\_detector()$ and $shape\_predictor(landmarks)$, which are both used in Anas Khayata's algorithm \cite{kha2021}.
For instance, the eye detection algorithm uses the points $36-42$ for the right eye and the points $42-48$ for the left eye.

Next, the rotation angle is calculated by using a line between the eyes, while a vertical line traversing the image is used as a reference (see figure \ref{fg:ds_pre}). Rotation makes use of the functions $getRotationMatrix2D$ and $warpAffine$ from the OpenCV library \cite{ocv2021}.\\

After that, the Region of Interest (ROI) is cropped from the image, eliminating background, and non-relevant parts such as the hair and the neck. The image is then resized to $128 \times 128$ pixels.

Lastly, the image contrast is improved by Histogram Equalization using the OpenCV $equalizeHist()$ function.
This implementation can be found at 
\url{https://gitlab.com/dorfell/fer_sys_dev/-/blob/master/00_sw/00_jaffe_pre-traitement/00_dlib_lbp_comparaison/mes_fonctions.py}.\\




% Extended data set pre-processing for FER systems
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Extended data set preprocessing for FER systems}
\label{sc:fer_pre_ex}

% Local Binary Pattern
%---------------------------------------------------
\subsection{Local Binary Pattern}

The visual descriptor known as Local Binary Pattern (LBP) is frequently used in FER 
systems \cite{yan2017}, \cite{kim2019}, due to the enhancement of textures associated 
with the action units (AU). To compute the LBP codes, a pixel from the grayscale image
is compared to the pixels around it in a $3 \times 3$ array.
The encoding process is summarized by equations \ref{eq:lbp0} and \ref{eq:lbp1}, where
$i_c$ and $i_p$ represent the grayscale image values from the center and its neighbors
pixels, respectively, and N is the number of neighbors' pixels.
The encoding process ends with a decimal value used to build the new LBP image, as
shown in figure \ref{fg:lbp_code}.

\begin{center}
  \begin{equation}
    LBP=\sum\limits_{n=1}^{N} {s(i_p - i_c)} \times {2^n}
    \label{eq:lbp0}
  \end{equation}\\

  \begin{equation}
    s(z) =
    \begin{cases}
      1, & \text{$z~\geq~0$}\\
      0, & \text{$z~<~0$}
    \end{cases}
    \label{eq:lbp1}
  \end{equation}
\end{center}

\vspace{1.2cm}

\begin{figure}[!htbp]
  \centering
  \includegraphics[scale=0.45]{./img/fer/lbp_encoding.png}
  \caption{LBP encoding example. Adapted from from \cite{yan2017}, \cite{kim2019}.}
  \label{fg:lbp_code}
\end{figure}


\cleardoublepage


Implementation of LBP in this work was made by employing the Scikit-image library
\cite{sci2014}. In particular, the $local\_binary\_pattern$ function from
$skimage.feature$ was used.
This function provides five methods for determining the pattern as described in
\cite{lbp2021}:\\


\begin{itemize}
 \item[$\bullet$] \textbf{``default'':} Rotation-variant LBP. 
 \item[$\bullet$] \textbf{``ror'':} Rotation-invariant LBP. 
 \item[$\bullet$] \textbf{``uniform'':} LBP using improved rotation-invariance and uniform patterns.
 \item[$\bullet$] \textbf{``nri\_unifor'':} LBP with uniform patterns but non-rotation-invariant.
 \item[$\bullet$] \textbf{``var'':} LBP based on rotation-invariant variance measures of the contrast of local image texture.\\
\end{itemize}


Figure \ref{fg:lbp_methods} shows the different LBP methods 
that were applied to the same JAFFE image. Implementation 
can be found at 
\textit{\url{https://gitlab.com/dorfell/fer_sys_dev/-/blob/master/00_sw/00_jaffe_pre-traitement/00_dlib_lbp_comparaison/mes_fonctions.py}}.

As the smoothest result was obtained with the ``var'' method, 
this was selected for applying LBP to the entire JAFFE data 
set. Finally, the LBP image is resized to $128 \times 128$
pixels using $OpenCV.resize$ with LANCZOS4 as the
interpolation method. \\

\begin{figure}[!htbp]
  \centering
  \includegraphics[scale=0.6]{./img/fer/lbp_comp2.png}
  \caption{LBP methods comparison with JAFFE.}
  \label{fg:lbp_methods}
\end{figure}



% Data Augmentation
%---------------------------------------------------
\subsection{Data Augmentation}
Training CNNs requires a large number of images to get acceptable precision and avoid overfitting. As the JAFFE data set size is about 256 images, it is necessary to
increase the number of training samples by means of techniques like data 
augmentation (DA).

\cleardoublepage

In DA, each training sample is subject to transformations such as rotation, flip, 
etc.; to create new training data. In this work, a Python library for data 
augmentation named Albumentation was used \cite{alb2020}. $15$ transformations, 
including shift, scale, rotation, flip, and some combinations, were applied to 
images of 6 emotions as reported in the literature \cite{yan2017},  \cite{kim2019}.

After this process, the number of training images increased to $2640$. Figure
\ref{fg:fer_da} depicts some DA examples over JAFFE resized to $64 \times 64$, and 
the implementation can be found at 
\url{https://gitlab.com/dorfell/fer_sys_dev/-/blob/master/00_sw/00_jaffe_pre-traitement/07_dlib_var_red_aug15/00_dlib_var_red_aug15.py}.



\begin{figure}[htbp]
  \centering
  %\hspace{-0.5cm}
  \includegraphics[scale=0.8]{./img/fer/jaffe_DA_3.png}
  \caption{Examples of data augmentation with JAFFE.}
  \label{fg:fer_da}
\end{figure}


\cleardoublepage


% Designing a CNN-based FER system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Designing a CNN-based FER system}
\label{sc:fer_des}
The architectures of some CNN-based FER systems proposed in the literature \cite{zha2017}, \cite{yan2017}, \cite{kim2019}, \cite{xie2019} were presented 
in subsection \ref{ssc:fer_cnn}.
The CNN models designed in those works are summarized in table \ref{tb:fer_arch}, 
as well as the accuracy obtained with the JAFFE data set. \\


% Table: CNN-based FER systems architectures
%------------------------------------------------------------------------
{
\newcommand{\sm}{\small}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tb}{\textbf}
\newcommand{\tn}{\tabularnewline}
\definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}

\begin{table*}[!htbp]
  \begin{center}
  %\hspace*{-2cm}
  \begin{tabular}{p{0.5cm} p{1.5cm} p{1.4cm} p{10cm} }\hline
    % use packages: color,colortbl
    \rowcolor{tcA}
    \textbf{Ref.}     & \textbf{Model} & \textbf{Accuracy} &\textbf{Architecture}\\\hline
    \cn\cite{zha2017} & $I^{2}CNN$ for grayscale images 
                                       & $75.28\%$         & \raisebox{-\totalheight}{\includegraphics[scale=0.25]{./img/fer/fer_i2cnn.png}}\tn
    \cn\cite{yan2017} & $WMDNN$ Channel 1: Partial VGG16 for grayscale images
                                       & $90.86\%$         & \raisebox{-\totalheight}{\includegraphics[scale=0.25]{./img/fer/fer_wmdnn_vgg16.png}}\tn         
    \cn\cite{yan2017} & $WMDNN$ Channel 2: Shallow CNN for LBP images
                                       & $88.33\%$         & \raisebox{-\totalheight}{\includegraphics[scale=0.25]{./img/fer/fer_wmdnn_shallow.png}}\tn         
    \cn\cite{kim2019} & Appearance feature-based CNN for LBP images
                                       & $89.33\%$         & \raisebox{-\totalheight}{\includegraphics[scale=0.25]{./img/fer/fer_appearance.png}}\tn\hline 

  \end{tabular}
  \end{center}

  \caption[Architectures of some CNN-based FER systems and their accuracy for JAFFE.]
          {Architectures of some CNN-based FER systems and their accuracy for JAFFE.}
  \label{tb:fer_arch}

\end{table*}
}


\cleardoublepage


On the other hand, the performance of CNN inference on hardware-software architectures
is affected by the Logic Resources available, the operations involved, and the model
size. Thus, the most suitable option for an embedded system is a single-channel
CNN-based FER system.\\


The CNN's design is an iterative process that depends on previously acquired knowledge
about the input data and the model's accuracy. Moreover, expertise on CNNs could
limit the design exploration space (DES) to tuning the most relevant parameters.
Besides, some known tips are:\\

\begin{itemize}
 \item[$\bullet$] Start with a convolutional layer with a few filters and  increase the                filters in the following layers. The number of filters should be multiples of two.
 \item[$\bullet$] Use a subsampling layer such as \textit{AveragePooling} or
                  \textit{MaxPooling} after each convolutional layer to 
                  decrease the size of the feature maps.
 \item[$\bullet$] The number of neurons in the last layer (e.g., Dense) should be equal to the number of classes. Remember that for the FER system, each class corresponds to an emotion.
 \item[$\bullet$] Compare the performance of different optimizers (e.g., SGD, Adam) using metrics such as accuracy, loss or graphics like learning curves.
 \item[$\bullet$] Adjust the learning rate (lr, $\alpha$) to improve the training process. Some of the recommended values are $\alpha=0.001$ and $\alpha=0.0005$.
  \item[$\bullet$] Tune the batch size (bs) and the number of epochs (a.k.a. \'epoques) to avoid underfitting and overfitting.\\
\end{itemize}


This work employed the JAFFE data set with 6 and 7 emotions, processed with the Local Binary Pattern (LBP) using the VAR method and data augmentation with  Albumentation, as well as TensorFlow to train the CNN models. TensorFlow is an open-source framework for developing machine learning systems \cite{tf2017}.
To evaluate the model's performance, the learning curves for accuracy (a.k.a. exactitude)
and loss (a.k.a. perte) were analyzed. Hence, three diagnostics are possible:\\

\begin{itemize}
 \item[$\bullet$] \textbf{Underfitting:} The model doesn't reach a lower error value, and the training loss doesn't decrease no matter the number of epochs. Another scenario is that the training loss is decreasing but the training is halted, thus it is needed to increase the number of epochs until the curve gets stable.

 \cleardoublepage

\item[$\bullet$] \textbf{Overfitting:} The model learned the training data set but fails to generalize, as a consequence, it presents high accuracy with the training data set and low accuracy with the validation data set. The validation loss curve could decrease in the first epochs and then start to increase, creating a gap between the validation and training loss curves.
\item[$\bullet$] \textbf{Good fit:} Can be identified when the curves for the 
                  validation loss and the training loss decrease to an 
                  acceptable value, while keeping a small gap between the curves.
                  Here, it is important to observe that increasing the number 
                  of epochs without control can result in overfitting.\\
\end{itemize}


Additionally, the ML framework typically provide techniques to fine-tune the training.
For instance, EarlyStopping allows to end the training when a metric
(e.g., validation loss) no longer improves \cite{earlystopping}, whereas, ReduceLROnPlateau
decreases the learning rate value when a metric improvement halts \cite{reducelronplateau}.\\


In the following subsections, some models tested are presented, as well as the model
selected to develop the work goals. The ML framework employed was TensorFlow
\cite{tf2017}.
TensorFlow is an open source project currently used by several companies, including
AMD, ARM, Google, Intel, Lenovo, Nvidia, and Texas Instruments, among others.
More than $60$ tests were performed  with $3$ to $10$ repetitions. In addition, the
source code and the training reports, including the model architectures and the 
learning curves, can be found at 
\url{https://gitlab.com/dorfell/fer_sys_dev/-/tree/master/00_sw/01_etu_mod_jaffe/01_MX_fp}.\\



% Testing model M1
%---------------------------------------------------
\subsection{Testing model M1}
The architecture of model M1 (see figure \ref{fg:fer_M1}) was adapted from the 
example ``Training a neural network on MNIST with Keras'' found in 
\url{https://www.tensorflow.org/datasets/keras_example}.
It was chosen as the starting point because, for the MNIST data set, it reached 
an accuracy above $90\%$ with just a few layers.\\

The M1 input for FER came from the JAFFE data set with samples of $128 \times 128~px$.
The parameters under test were the batch size (bs), the learning rate (lr) and 
the number of filters in the convolutional layers, among others.


\cleardoublepage
    

\begin{figure}[!htbp]
  \centering
  \subfloat[Original architecture.]{\captionsetup{width=1.2\linewidth} 
                                   \includegraphics[scale=0.31]{./img/fer/M1.png}}\\
  \subfloat[$bs=256$, $lr=0.001$.]{
             \includegraphics[scale=0.29]{./img/fer/M1_acc01.png}
             \includegraphics[scale=0.29]{./img/fer/M1_loss01.png}}\\
  \subfloat[$bs=128$, $lr=0.001$, with 128 filters.]{
             \includegraphics[scale=0.29]{./img/fer/M1_acc02.png}
             \includegraphics[scale=0.29]{./img/fer/M1_loss02.png}}\\
  \caption{Tests with the M1 model.}
  \label{fg:fer_M1}
\end{figure}


According to the accuracy and loss learning curves presented in figure 
\ref{fg:fer_M1}, the model is underfitting with less than $50\%$ of accuracy for 
the validation data set. These tests are located in 
\url{https://gitlab.com/dorfell/fer_sys_dev/-/blob/master/00_sw/01_etu_mod_jaffe/01_MX_fp/06_entrainement_dlib128_aug15_20210219.pdf}.




% Testing model M2
%---------------------------------------------------
\subsection{Testing model M2}
The architecture of model M2 (see figure \ref{fg:fer_M2}) is based 
on the partial VGG16 proposed for the WMDNN FER system \cite{yan2017}
shown in table \ref{tb:fer_arch}. 


\cleardoublepage

The model was tested with and without \textit{Dropout} varying the batch size, the learning rate, and using \textit{MaxPooling} instead of \textit{AveragePooling}. The learning curves show instability, and the accuracy is under $50\%$. The training reports for this model can be found at
\url{https://gitlab.com/dorfell/fer_sys_dev/-/blob/master/00_sw/01_etu_mod_jaffe/01_MX_fp/01_entrainement_dlib128_aug15_20210217.pdf}.\\


\begin{figure}[!htbp]
  \centering
  \subfloat[Original architecture.]{\includegraphics[scale=0.4]{./img/fer/M2.png}}\\
  \subfloat[$bs=256$, $lr=0.001$]{
             \includegraphics[scale=0.3]{./img/fer/M2_acc01.png}
             \includegraphics[scale=0.3]{./img/fer/M2_loss01.png}}\\
  \subfloat[$bs=128$, $lr=0.001$, without dropout and MaxPooling instead of AveragePooling.]{
             \includegraphics[scale=0.3]{./img/fer/M2_acc02.png}
             \includegraphics[scale=0.3]{./img/fer/M2_loss02.png}}\\
  \caption{Tests with the M2 model.}
  \label{fg:fer_M2}
\end{figure}


\cleardoublepage


% Testing model M3
%---------------------------------------------------
\subsection{Testing model M3}
The architecture of model M3 (see figure \ref{fg:fer_M3}) is based on model M2 but the kernel size for the convolutional layers is increased from $3 \times 3$ to $5 \times 5$. The tests made with M3 include varying the batch size, the learning rate, with and without \textit{Dropout} and using \textit{MaxPooling} instead of  \textit{AveragePooling}. 
The learning curves obtained show accuracy improvements up to $80\%$ after 100 epochs (\'epoques), but the model tends to overfit. These tests are located in \url{https://gitlab.com/dorfell/fer_sys_dev/-/blob/master/00_sw/01_etu_mod_jaffe/01_MX_fp/17_entrainement_dlib_var128_aug15_20210219.pdf}.


\begin{figure}[!htbp]
  \centering
  \subfloat[Original architecture.]{\includegraphics[scale=0.36]{./img/fer/M3.png}}\\
  \subfloat[$bs=64$, $lr=0.001$]{
             \includegraphics[scale=0.3]{./img/fer/M3_acc01.png}
             \includegraphics[scale=0.3]{./img/fer/M3_loss01.png}}\\
  \subfloat[$bs=64$, $lr=0.001$, without Dropout, Dense(1024), Dense(512) and MaxPooling instead of AveragePooling.]{
             \includegraphics[scale=0.3]{./img/fer/M3_acc02.png}
             \includegraphics[scale=0.3]{./img/fer/M3_loss02.png}}\\
  \caption{Tests with the M3 model.}
  \label{fg:fer_M3}
\end{figure}


\cleardoublepage

  
% Testing model M4
%---------------------------------------------------
\subsection{Testing model M4}
The architecture of model M4 (see figure \ref{fg:fer_M4}) is based on model M3 but
includes only 1 \textit{Dense} layer to avoid overfitting. Besides, the input size
is changed to $64 \times 64$ and the number of epochs is reduced from $100$ to $30$,
because the accuracy and loss barely improve after epoch 15. The model was
tested by varying the batch size, the learning rate, and the number of filters per
\textit{Convolutional} layer among other parameters.
The learning curves show a validation accuracy of around $70\%$ but the gap between
the training and the validation loss indicates that overfitting is still present. 
The test reports can be found at
\url{https://gitlab.com/dorfell/fer_sys_dev/-/blob/master/00_sw/01_etu_mod_jaffe/01_MX_fp/42_entrainement_dlib_var64_aug15_20210302.pdf}.


\begin{figure}[!htbp]
  \centering
  \subfloat[Original architecture]{\includegraphics[scale=0.34]{./img/fer/M4.png}}\\
  \subfloat[$bs=64$, $lr=0.0001$]{
             \includegraphics[scale=0.3]{./img/fer/M4_acc01.png}
             \includegraphics[scale=0.3]{./img/fer/M4_loss01.png}}\\
  \subfloat[$bs=64$, $lr=0.0001$, with Conv of 32, 64 and 128 filtres, and kernel of (5,5).]{
             \includegraphics[scale=0.3]{./img/fer/M4_acc02.png}
             \includegraphics[scale=0.3]{./img/fer/M4_loss02.png}}\\
  \caption{Tests with the M4 model.}
  \label{fg:fer_M4}
\end{figure}


\cleardoublepage 


 
% Testing model M6
%---------------------------------------------------
\subsection{Testing model M6}
Model M5 is based on model M4, but the stride parameter is replaced 
for the \textit{Convolutional} and the \textit{MaxPooling} layers
from $(2,2)$ to $(1,1)$. Test reports for this model are located in \\
\url{https://gitlab.com/dorfell/fer_sys_dev/-/blob/master/00_sw/01_etu_mod_jaffe/01_MX_fp/60_entrainement_M5_jaffe_dlib_var64_aug15_20210313.pdf}.


The architecture of model M6 (see figure \ref{fg:fer_M6}) is based on model M5 but
changes the number of filters in the \textit{Convolutional} layer to $32$, $64$, 
and $128$. In addition, the \textit{Dense} layer includes six neurons instead of
seven, thus training is made with the data set including $6$ emotions, as found
in the literature. The learning curves show the validation accuracy stabilizing 
around of $90\%$ and the validation loss around $0.4$. It is worth mentioning that
the gap between the training and the validation loss is approximately $0.3$, which
indicates an acceptable fit. As the model M6 is a single-channel CNN that achieves acceptable performance, this will be the CNN-based FER system proposed in this 
work. The training reports for model M6 can be found at
\url{https://gitlab.com/dorfell/fer_sys_dev/-/blob/master/00_sw/02_M6_jaffe/00_M6_jaffe_dlib_var64_aug15/00_entrainement_M6_jaffe_dlib_var64_aug15_6emo_20210324.pdf}
The python script used is also available in the appendix \ref{app:codes} as listing \ref{cd:M6_6emo}.


\begin{figure}[!htbp]
  \centering
  \subfloat[Original architecture]{\includegraphics[scale=0.4]{./img/fer/M6.png}}\\
  \subfloat[$bs=32$, $lr=0.001$]{
             \includegraphics[scale=0.3]{./img/fer/M6_acc01.png}
             \includegraphics[scale=0.3]{./img/fer/M6_loss01.png}}\\
  \caption{Tests with the M6 model.}
  \label{fg:fer_M6}
\end{figure}

  
 
 
% Discussion 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion}
\label{sc:fer_dis}
The recent use of CNN models for FER systems has been widely adopted in the state 
of the art, mainly because of the performance that can be attained. However, 
because there is not a clear agreement on what architecture should be used for 
the FER system, it remains an intriguing topic for research.\\

Moreover, the applications for FER systems have different requirements with 
different relevance according to the specific problem. For instance, latency
and accuracy are crucial for automobile safety, while computational complexity
could be more relevant for embedded systems with human-computer interaction 
capabilities.\\

On the other hand, although there are several algorithms that could be used to 
enhance the data set (e.g., the Canny Edge detector), the stages implemented in 
this work for the basic and extended preprocessing were taken from the literature,
making the results comparable with similar works.\\

Besides, this work trained a single-channel CNN with a small data set, which led
to learning curves for accuracy and loss that had ripple effects with some outliers.
This behavior could cause techniques like EarlyStop and ReduceLROnPlateau to
randomly stop the training or change the learning rate before getting to the
optimal values.\\

Furthermore, because the work aims to run CNNs inference in a hardware-software
architecture, the design process of a CNN-based FER system involves a trade-off
between performance and the  computational resources available in the target platform.



% Conclusion 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}
\label{sc:fer_con}
This chapter briefly presented the basis of FER systems, from their traditional
architectures to those integrating ML algorithms.
Furthermore, due to the inherent complexity of FER systems, it is advised to 
follow the literature along with the data sets used, the basic preprocessing 
stages (e.g., eliminating background, face rotation, histogram equalization, etc.), 
the LBP and the DA. Hence, it is worth mentioning that ``the input data could
be as important as the system architecture''.

In addition, the design process of a CNN-based FER system was described. From 
the several tests made, the relevance of each parameter is studied, as are the 
relationships between overfitting/underfitting and the number of layers, 
samples, epochs, etc.

In addition, as mentioned before, designing a FER system is an observation process 
with several iterations. Where analyzing the learning curves and not only the final
values of metrics is essential.
Lastly, a single-channel CNN-based FER system trained with TensorFlow and the 
JAFFE data set that achieves an accuracy of above $90\%$ is proposed.

