%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hardware Neural Networks (HNN)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{State of the art of the Hardware Neural Networks (HNNs)}
\label{ch:soa}
 
With the appearance of high performance platforms in the last decade (i.e. GPU, FPGAs),
Neural Networks (NNs) are becoming an attractive tool for many classification and object
recognition problems. Despite this, the existent APIs for running NNs don't exploit the
hardware resources efficiently and for this reason several researches have turn back
their attention to Hardware Neural Networks (HNNs) \cite{zhang2015},
\cite{venieris2016}, \cite{dundar2016}.
HNNs are the implementation of accelerators architectures that evaluate the NNs
forward propagation algorithms, by using reconfigurable platforms like FPGAs, or
hardcore designs like the VLSI circuits \cite{misra2010}, \cite{li2016}. Usually,
the networks are trained by means of tools such as Caffe \cite{cf2017}, MATLAB \cite{mt2017},
TensorFlow \cite{tf2017}, Microsoft Cognitive Toolkit \cite{ct2017},  etc. and then,
with the weights and bias calculated, a hardware accelerator is implemented
\cite{chen2014}, \cite{zhou2015}, \cite{murakimi2016}.
Nevertheless, the discussion of how to design HNNs had been widen due to number of
works that had been proposed in the last few years \cite{misra2010}, and up to day,
a new literature review is needed to identify the most relevant search streams that
had appeared lately, the NN types being used, the organizations leading the research,
the research limitations and the quality of the implementation frameworks being proposed.
In this chapter, we aim to answer all of these questions by means of a Systematic Literature
Review (SLR) \cite{kitchenham2009}, \cite{kitchenham2010}.
This chapter is organized as follows; Section I describes the carried out Systematic
Literature Review. Section II presents the SLR results and Section III the discussion.
Finally, the conclusion is drawn in Section IV and the Research Questions and Hypothesis
in sections V and VI.



 
% Systematic Literature Review Method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Systematic Literature Review Method}
The literature review was made by applying the Systematic Literature Review (SLR)
method, proposed by Kitchenham et al. in \cite{kitchenham2009} and
\cite{kitchenham2010}. The goal of this review is to identify the gaps existing in
the HNN implementation process by studying the relevant works in the state of the art.
Additionally, according to the literature NN implementations on traditional platforms
like general purpose processors don't use hardware resources efficiently, and therefore
there is a growing interest in exploring others platforms  \cite{zhang2015}, \cite{venieris2016}, \cite{dundar2016}.
On the other hand,  Graphical Processing Units (GPUs) are being widely used for training NN because
of their high throughput, but those implementations are limited by the local memory
available and the communication bandwidth between the host and the GPU, \cite{krizhevsky2014},
\cite{chetlur2014}, \cite{dundar2016} besides, its power consumption make them not
practicals  for embedded systems.
Fortunately, these implementations can be improved by using accelerators in custom
hardware (e.g. ASICs, FPGA), which have demonstrated to have better performance during
the feedforward prediction stage and low power consumption \cite{dundar2016}, \cite{ortega2016}.
Moreover, FPGAs are easily
programmable  and inexpensive, these being why they are the most advantageous
option. Furthermore, with the appearance of more optimization design techniques and the
amount of FPGA logic resources available being increased, the design exploration space
is enlarged boosting the HNN implementation on FPGA-based platforms.
By taking into
account the above-mentioned factors, it was decided to focus this literature review on
HNN works and implementation frameworks designed for these platforms, without including
GPU-based works. Lastly, the SLR method steps are documented below.


% Research Questions (RQ)
%---------------------------------------------------
\subsection{Research Questions (RQ)}
The research questions addressed by this review are:

\begin{itemize}
 \item[$\bullet$] RQ1: How many frameworks for implementing HNN has been proposed since
                       2010?
 \item[$\bullet$] RQ2: What types of Neural Networks are being addressed?
 \item[$\bullet$] RQ3: What individuals and organizations are leading the research?
 \item[$\bullet$] RQ4: What are the limitations of current research?
 \item[$\bullet$] RQ5: Is the quality of the implementation frameworks improving?
\end{itemize}

With respect to RQ1, the review starts at 2010 because HNNs start to be a feasible
option when the resources and computation platforms available were sufficient for
the efficient implementation around 2010.
With respect to RQ2, there are several types of NN that can be implemented in hardware,
however, differences in input data, classification tasks, activation function, etc. can lead to
choose one instead of another. For this reason it is important to know which are the
NN types being used for HNN.
With respect to RQ3, it is essential to identify HNN research trends, relevant authors
and leading works to be aware of the current problems being studied.
With respect to limitations of HNN research (RQ4) the following issues are going to be
considered:

\begin{itemize}
 \item[$\bullet$] RQ4.1: Were the scope of HNN implementation frameworks limited?
 \item[$\bullet$] RQ4.2: Is there evidence that the use of HNNs is limited due to lack of
                         implementation frameworks?
 \item[$\bullet$] RQ4.3: Is the quality of implementation frameworks appropriate?
 \item[$\bullet$] RQ4.4: Are frameworks contributing to the implementation of HNN by
                         defining practice guidelines?
\end{itemize}

With respect to RQ5, it is important to know if the proposed frameworks are being
improved in subsequent works or if the new proposed frameworks are taking
different approaches.


% Search Process
%--------------------------------------------------- 
\subsection{Search Process}
The IEEE Computer Society Digital Library, the SCOPUS indexing system and Open Access
organizations like IAES, IOSR were used in
the search process. All searches were based on titles, keywords and abstracts of works
published in journals, conferences and symposiums since 2010, which are shown
in Table \ref{tab:jou_sym_con}.
The search string used in the IEEE library was ``Neural Networks'' AND ``Hardware''
and the search string used in SCOPUS  was TITLE-ABS-KEY(``Neural Networks'') AND
TITLE-ABS-KEY(``Hardware'') OR TITLE-ABS-KEY(``Framework'').


% Table: Selected journals and conference proceddings
%------------------------------------------------------------------------
{

\newcommand{\sm}{\small}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tb}{\textbf}
\newcommand{\tn}{\tabularnewline}
\definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}

\begin{table*}[!t]
  \begin{center}
  \begin{tabular}{p{8.0cm} p{1.5cm} p{2.2cm} p{2.0cm}}\hline
    % use packages: color,colortbl
    \rowcolor{tcA} \sm
    \tb{Source}                                               & \tb{Acronym} & \tb{Organization}  & \tb{Publication}\\\hline
    % -- Journals
    Transactions on Neural Networks and Learning Systems      & TNNLS        & IEEE               & Journal         \tn
    Transactions on Very Large Scale Integration Systems      & TVLSIS       & IEEE               & Journal         \tn
    Transactions on Computers                                 & TC           & IEEE               & Journal         \tn
    Neural Networks                                           & NN           & ELSEVIER           & Journal         \tn
    Neurocomputing                                            & NC           & ELSEVIER           & Journal         \tn
    Information Fusion                                        & IF           & ELSEVIER           & Journal         \tn
    Computer Methods and Programs in Biomedicine              & CMPB         & ELSEVIER           & Journal         \tn
    Pattern Recognition                                       & PR           & ELSEVIER           & Journal         \tn
    Engineering Applications of Artificial Intelligence       & EAAI         & ELSEVIER           & Journal         \tn
    Engineering Research And Development                      & IJERD        & Peer Reviewed      & Journal         \tn
    Electrical and Computer Engineering                       & IJECE        & IAES               & Journal         \tn
    Electronics and Communication Engineering                 & JECE         & IOSR               & Journal         \tn\hline
    % -- Conferences
    International Conference on Data Mining Workshops         & ICDMW        & IEEE               & Conference      \tn
    International Conference on Computer Science and Network
    Technology                                                & ICCSNT       & IEEE               & Conference      \tn
    International Conference on Architectural Support for Programming
    Languages and Operating Systems                           & ASPLOS       & IEEE/ACM           & Conference      \tn
    International Conference on Parallel Architectures and
    Compilation Techniques                                    & PACT         & IEEE/ACM           & Conference      \tn\hline
    % -- Symposiums
    Annual International Symposium on Computer Architecture   & ISCA         & IEEE/ACM           & Symposium       \tn
    Annual International Symposium on Field Programmable
    Custom Computing Machines                                 & ISFPCCM      & IEEE               & Symposium       \tn
    Annual International Symposium on Field Programmable
    Gate Arrays                                               & FPGA         & IEEE/ACM           & Symposium       \tn\hline

  \end{tabular}
  \end{center}

  \caption[Selected journals, symposiums and conference proceedings.]
          {Selected journals, symposiums and conference proceedings.}
  \label{tab:jou_sym_con}

\end{table*}
}


% Study selection
%--------------------------------------------------- 
\subsection{Study selection}

The results for the different searches were added, obtaining a total number of 491
papers published between Jan 1st 2010 and March 31th 2017: 143 from the IEEE digital
library, 343 from SCOPUS indexing system and 5 from the open access organizations.
To these papers, the following inclusion and exclusion criteria were applied.\\
Topics used to include the papers:

\begin{itemize}
  \item[$\bullet$] Frameworks for implementing HNN with defined research questions,
                   search process, data extraction and data presentation, whether or
                   not the researchers referred to their study as a implementation
                   framework.
  \item[$\bullet$] Approach to optimize current implementations of HNN.
\end{itemize}

Papers on the following topics were excluded:

\begin{itemize}
  \item[$\bullet$] Informal literature surveys (no defined research questions;
                   no defined  search process; no defined data extraction process).
  \item[$\bullet$] Papers presenting implementations of HNN with not defined procedures
                   or not discussing the procedures used.
  \item[$\bullet$] Papers presenting GPU-based works, which are limited by local memory
                   constraints and bandwidth communication will also be excluded.
  \item[$\bullet$] Duplicate reports of the same study (when several reports of a study
                   exist in different journals the most complete version of the study
                   was included in the review).
\end{itemize}

After excluding papers that were obviously irrelevant, had not enough information,
or were duplicates, there were 61 papers remaining. Those papers were then
subject to a more detailed assessment, where each paper was reviewed to identify papers
that could be rejected on the basis that they did not include literature reviews,
or that they were not related to implementation frameworks. This led to the exclusion
of 41 papers. The remaining papers are shown in Table \ref{tab:srs_HNN}.

                  
% Quality assessment (QA)
%--------------------------------------------------- 
\subsection{Quality assessment (QA)}
Each article was evaluated using the following quality assessment (QA) questions based
on \cite{kitchenham2009}:

\begin{itemize}
  \item[$\bullet$] QA1: Is the HNN implementation process presented explicitly?
  \item[$\bullet$] QA2: Is the literature search likely to have covered all
                        relevant studies?
  \item[$\bullet$] QA3: Did the HNN implementation assess the quality/validity of previous
                        included  studies?
  \item[$\bullet$] QA4: Were the basic data/studies adequately described?

\end{itemize}

There are three possible outputs for each question with the following score:
$Y~(yes)~=~1$, $P~(partly)~=~$ $0.5$ and $N~(no)~=~0$. For QA1: $Y$ the implementation process is presented
explicitly, $P$ the implementation process is implicit and $N$ the implementation
process is not defined and cannot be readily inferred.\\
For QA2: $Y$ the authors had cited at least 20 works including highly cited works, $P$
the authors had cited between 15 and 19 works including relevant works. $N$ the authors
had cited less than 15 works or they had cited irrelevant works.\\
For QA3: $Y$ The HNN implementation performance improved  former ones in more
than 2x, $P$ the performance was less than 2x of previous ones, and $N$ performance was
not reported. \\
For QA4: $Y$ information of each primary study is presented, $P$ each primary study is
barely presented, and $N$ any information of each primary study is given.


% Data collection
%--------------------------------------------------- 
\subsection{Data collection}
The data extracted from each work were:

\begin{itemize}
  \item[$\bullet$] The source (journal, conference or symposium) and full reference.
  \item[$\bullet$] Classification of the study type (i.e. HNN implementation, VLSI
                   design, HNN implementation frameworks).
  \item[$\bullet$] Main topic area.
  \item[$\bullet$] The author(s), their institution and the country where it is
                   situated.
  \item[$\bullet$] Summary of the study including the main research questions and
                   the answers.
  \item[$\bullet$] Research question/issue.
  \item[$\bullet$] Quality evaluation.
  \item[$\bullet$] How many primary studies were used in the work.

\end{itemize}


% Data analysis
%--------------------------------------------------- 
\subsection{Data analysis}
The data was tabulated (see Tables \ref{tab:jou_sym_con} and \ref{tab:qu_ev_slr})
to show:

\begin{itemize}
  \item[$\bullet$] The number of HNN works published per year and their source
                   (addressing RQ1).
  \item[$\bullet$] Whether the HNN work referenced others papers (addressing RQ1).
  \item[$\bullet$] The topics studied by the HNN works, i.e. HNN implementation,
                   VLSI design, HNN implementation frameworks (addressing RQ2 and RQ4.1).
  \item[$\bullet$] The authors: the affiliations of the authors and their institutions
                   was reviewed but not tabulated (addressing RQ3).
  \item[$\bullet$] The number of previous HNN works in each paper (addressing RQ4.2).
  \item[$\bullet$] The quality score for each HNN work (addressing RQ4.3).

\end{itemize}




% Results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results}


% Search Results
%--------------------------------------------------- 
\subsection{Search Results}
Including works from journals, symposiums and conferences they were 20 papers reviewed.
These papers are shown in Table \ref{tab:srs_HNN}.


% Table: Systematic Review of HNN Studies
%------------------------------------------------------------------------
{
\newcommand{\sm}{\small}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tb}{\textbf}
\newcommand{\tn}{\tabularnewline}
\definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}

\begin{table*}[!htbp]
  \begin{center}
  \begin{tabular}{p{1.0cm} p{2.5cm} p{1.0cm} p{1.5cm} p{1.5cm} p{5.5cm} }\hline
    % use packages: color,colortbl
    \rowcolor{tcA}
    \textbf{Study Ref.}   & \textbf{Authors}        & \textbf{Date} & \textbf{Paper type} & \textbf{Number primary studies} & \textbf{Review topics}\\\hline
    \cn\cite{misra2010}   & Misra \& Saha           & \cn 2010      & Journal             & \cn 278                         & Overview of HNN models: HNN chips,
                                                                                                                              Cellular HNN, Neuromorphic HNN,
                                                                                                                              Optical NN.                        \tn       %00
    \cn\cite{farabet2011} & Farabet et al.          & \cn 2011      & Conference          & \cn 27                          & HNN implementation,
                                                                                                                              hardware architectures.            \tn       %01
    \cn\cite{rana2012}    & Rana D. Abdu-Aljabar    & \cn 2012      & Journal             & \cn 23                          & HNN implementation.                \tn       %02
    \cn\cite{shak2013}    & Shakoory                & \cn 2013      & Journal             & \cn 12                          & HNN implementation.                \tn       %03
    \cn\cite{moha2013}    & Mohammed et al.         & \cn 2013      & Journal             & \cn  9                          & HNN implementation.                \tn       %04
    \cn\cite{chen2014}    & Chen et al.             & \cn 2014      & Conference          & \cn 44                          & HNN implementation,
                                                                                                                              VLSI design.                       \tn       %05
    \cn\cite{singh2015}   & Singh et al.            & \cn 2015      & Journal             & \cn 11                          & HNN implementation.                \tn       %06                                                                                                                              
    \cn\cite{zhang2015}   & Zhang et al.            & \cn 2015      & Symposium           & \cn 16                          & HNN implementation.                \tn       %07
    \cn\cite{zhou2015}    & Zhou, Y., \& Jiang, J.  & \cn 2015      & Conference          & \cn 12                          & HNN implementation.                \tn       %08
    \cn\cite{du2015}      & Du et al.               & \cn 2015      & Symposium           & \cn 61                          & HNN implementation,
                                                                                                                              VLSI design.                       \tn       %09
    \cn\cite{venieris2016}& Venieris et al.         & \cn 2016      & Symposium           & \cn 16                          & HNN implementation framework.      \tn       %10
    \cn\cite{murakimi2016}& Murakami, Y.            & \cn 2016      & Conference          & \cn 8                           & HNN implementation.                \tn       %11
    \cn\cite{motamedi2016}& Motamedi et al.         & \cn 2016      & Conference          & \cn 10                          & HNN Parallelism.                   \tn       %12
    \cn\cite{dundar2016}  & Dundar et al.           & \cn 2016      & Journal             & \cn 49                          & HNN implementation.                \tn       %13
    \cn\cite{li2016}      & Li et al.               & \cn 2016      & Symposium           & \cn 11                          & HNN implementation.                \tn       %14
    \cn\cite{saldanha2016}& Saldanha et al.         & \cn 2016      & Symposium           & \cn 13                          & HNN implementation.                \tn       %15
    \cn\cite{wang2016}    & Wang et al.             & \cn 2016      & Symposium           & \cn 19                          & HNN implementation,
                                                                                                                              VLSI design.                       \tn       %16
    \cn\cite{ortega2016}  & Ortega-Zamorano et al.  & \cn 2016      & Journal             & \cn 44                          & HNN implementation framework for
                                                                                                                              Backpropagation.                   \tn       %17
    \cn\cite{kyrkou2016}  & Kyrkou et al.           & \cn 2016      & Journal             & \cn 41                          & HNN implementation                 \tn       %18                                                                                                                              
    \cn\cite{luo2016}     & Luo et al.              & \cn 2017      & Journal             & \cn 66                          & HNN implementation,
                                                                                                                              VLSI design.                       \tn\hline %19

  \end{tabular}
  \end{center}

  \caption[Systematic Review of HNN Studies.]
          {Systematic Review of HNN Studies.}
  \label{tab:srs_HNN}

\end{table*}
}


% Quality evaluation of the HNN works.
%--------------------------------------------------- 
\subsection{Quality evaluation of the HNN works.}
The HNN works shown in Table \ref{tab:srs_HNN} were assessed based on the quality
assessment (QA) questions. These results are shown in Table \ref{tab:qu_ev_slr}.


% Table: Quality evaluation of the HNN studies.
%------------------------------------------------------------------------
{
\newcommand{\sm}{\small}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tb}{\textbf}
\newcommand{\tn}{\tabularnewline}
\definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}

\begin{table}[!htbp]
  \begin{center}
  \begin{tabular}{p{1.0cm} p{1.5cm} p{0.5cm} p{0.5cm} p{0.5cm} p{0.5cm} p{1.0cm}}\hline
    % use packages: color,colortbl
    \rowcolor{tcA}
    \textbf{Study Ref.}   & \textbf{Paper type} & \textbf{QA1} & \textbf{QA2} & \textbf{QA3} & \textbf{QA4} & \textbf{Total Score} \\\hline
    \cn\cite{misra2010}   & Journal             & \cn N        & \cn Y        & \cn N        & \cn Y        & \cn 2.0 \tn %00
    \cn\cite{farabet2011} & Conference          & \cn Y        & \cn Y        & \cn Y        & \cn P        & \cn 3.5 \tn %01
    \cn\cite{rana2012}    & Journal             & \cn Y        & \cn P        & \cn P        & \cn Y        & \cn 3.0 \tn %02
    \cn\cite{shak2013}    & Journal             & \cn Y        & \cn N        & \cn P        & \cn P        & \cn 2.0 \tn %03
    \cn\cite{moha2013}    & Journal             & \cn N        & \cn N        & \cn N        & \cn N        & \cn 0.0 \tn %04
    \cn\cite{chen2014}    & Conference          & \cn Y        & \cn Y        & \cn Y        & \cn Y        & \cn 4.0 \tn %05
    \cn\cite{singh2015}   & Journal             & \cn Y        & \cn N        & \cn P        & \cn N        & \cn 1.5 \tn %06                                                                                                                                  
    \cn\cite{zhang2015}   & Symposium           & \cn Y        & \cn P        & \cn Y        & \cn P        & \cn 3.0 \tn %07
    \cn\cite{zhou2015}    & Conference          & \cn P        & \cn P        & \cn P        & \cn P        & \cn 2.0 \tn %08
    \cn\cite{du2015}      & Symposium           & \cn Y        & \cn Y        & \cn Y        & \cn Y        & \cn 4.0 \tn %09
    \cn\cite{venieris2016}& Symposium           & \cn Y        & \cn P        & \cn Y        & \cn Y        & \cn 3.5 \tn %10
    \cn\cite{murakimi2016}& Conference          & \cn Y        & \cn N        & \cn P        & \cn P        & \cn 2.0 \tn %11
    \cn\cite{motamedi2016}& Conference          & \cn Y        & \cn P        & \cn Y        & \cn Y        & \cn 3.5 \tn %12
    \cn\cite{dundar2016}  & Journal             & \cn Y        & \cn Y        & \cn Y        & \cn Y        & \cn 4.0 \tn %13
    \cn\cite{li2016}      & Symposium           & \cn Y        & \cn P        & \cn P        & \cn N        & \cn 2.0 \tn %14
    \cn\cite{saldanha2016}& Symposium           & \cn P        & \cn P        & \cn P        & \cn N        & \cn 1.5 \tn %15
    \cn\cite{wang2016}    & Symposium           & \cn P        & \cn P        & \cn P        & \cn P        & \cn 2.0 \tn %16
    \cn\cite{ortega2016}  & Journal             & \cn Y        & \cn Y        & \cn Y        & \cn Y        & \cn 4.0 \tn %17
    \cn\cite{kyrkou2016}  & Journal             & \cn Y        & \cn Y        & \cn Y        & \cn Y        & \cn 4.0 \tn %18                                                                                                                                  
    \cn\cite{luo2016}     & Journal             & \cn Y        & \cn Y        & \cn Y        & \cn Y        & \cn 4.0 \tn\hline %19
  \end{tabular}
  \end{center}

  \caption[Quality evaluation of the HNN studies.]
          {Quality evaluation of the HNN studies.}
  \label{tab:qu_ev_slr}
  
\end{table}
}


% Quality factors
%--------------------------------------------------- 
\subsection{Quality factors}
The average Quality Scores (QS) for studies each year, the mean and the
standard deviation $\sigma$ are shown in Table \ref{tab:avg_qual_sco}.
As can be seen the number of HNN studies in the last few years has grew up from
1 study per year up to 9 studies, showing the growing interest for HNN. Also, the
average QS per year has been quasi-stable around 3.0 (i.e. 2.88), which
can be seen as an increase in the number of most comprehensive works on the topic.

% Table: Average quality score for studies 
%------------------------------------------------------------------------
{
\newcommand{\sm}{\small}
\newcommand{\cc}{\cellcolor}
\newcommand{\cn}{\centering}
\newcommand{\tb}{\textbf}
\newcommand{\tn}{\tabularnewline}
 \definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}

\begin{table}[!htbp]
  \begin{center}
  \begin{tabular}{p{1.2cm} p{0.5cm} p{0.5cm} p{0.5cm} p{0.5cm} p{0.5cm} p{0.5cm} p{0.5cm} p{0.5cm}  }%\hline
    % use packages: color,colortbl
    %\rowcolor{tcA}
    \cline{2-9}
                         & \multicolumn{8}{c}{\tb{Year}}                                                                     \tn \cline{2-9}
                         & \tb{2010} & \tb{2011}  & \tb{2012}  & \tb{2013} & \tb{2014} & \tb{2015} & \tb{2016}  & \tb{2017}  \tn \hline
     \# of papers        & \cn 1     & \cn 1      & \cn 1      & \cn 2     & \cn 1     & \cn 4     & \cn 9      & \cn 1      \tn
     QS Mean             & \cn 2.0   & \cn 3.5    & \cn 3.0    & \cn 1.0   & \cn 4.0   & \cn 2.625 & \cn 2.94   & \cn 4      \tn
     QS $\sigma$         & \cn 0.88  & \cn 0.625  & \cn 0.12   & \cn 1.88  & \cn 1.12  & \cn 0.255 & \cn 0.06   & \cn 1.12   \tn \hline
  \end{tabular}
  \end{center}

  \caption[Average Quality Scores  (QS) for studies by publication date.]
          {Average Quality Scores  (QS) for studies by publication date.}
  \label{tab:avg_qual_sco}

\end{table}
}




% Discussion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion}
The answers to the research questions are discussed in this section.

\begin{itemize}

 \item[$\bullet$] RQ1: How many frameworks for implementing HNN has been proposed since
                  2010?\\
                  The revision of several studies from 2010 to 2017 lead to 20 relevant
                  HNN studies. Moreover, it is observed that the interest in HNN is
                  growing up  as the number of studies per year. \\
                  Relevant studies included 1 survey of the HNN implementations proposed
                  before 2010 \cite{misra2010}, 4 studies of HNN Very Large Scale
                  Integration (VLSI) designs  \cite{chen2014}, \cite{du2015},
                  \cite{luo2016}, \cite{wang2016}, and the rest of studies proposed an
                  HNN design implemented in a reconfigurable platform (e.g. FPGA).
                  Despite not all of them presented  an explicit framework,  the
                  implementation process could be readily inferred.

 \item[$\bullet$] RQ2: What types of Neural Networks are being addressed?\\
                  Most of the works aimed to implement Convolutional Neural Networks
                  (CNN) \cite{duffner2007}, \cite{saidane2011}.  CNN has been highly
                  accepted in classification and object recognition problems because
                  of its accuracy and relatively fair cost.  CNNs are a class of Deep
                  Neural Networks (DNN) where weights are shared across neurons, thus
                  reducing the memory needed to stored the training parameters.

 \item[$\bullet$] RQ3: What individuals and organizations are leading the research?\\
                  The leadership of HNNs implementation can be divided by approach.
                  The VLSI  design  of HNNs is lead by the work group form by the State
                  Key Laboratory of Computer Architecture in China, the Institute of
                  Computing  Technology processing in China and the Inria Institution
                  in France. They had designed and implemented at least 4 different
                  HNN chips \cite{chen2014}, \cite{du2015},  \cite{wang2016}. \\
                  On the other hand, there are different authors that had contributed
                  with several studies about HNNs implementation in reconfigurable
                  platforms. For example, Eugenio Culurciello from the Courant Institute
                  of Mathematical Sciences, New York University and Yann LeCun from
                  the Electrical Engineering Department, Yale University both in
                  USA presented works that include a dataflow processor for vision
                  \cite{farabet2011},
                  and an Embedded Streaming DNN Accelerator \cite{dundar2016}.
                  In addition, Stylianos Veneris and Christos-Savvas Bougaris from the
                  Department of Electrical and Electronic Engineering, Imperial College
                  in London had presented fpgaConvNet,  a framework for mapping CNN on
                  FPGAs in \cite{venieris2016}.
                  Finally, Francisco Ortega-Zambrano et al. from the Departamento de
                  Lenguajes y Ciencias de la Computaci\'on, Universidad de M\'alaga in
                  Spain had proposed an efficient  implementation of the NN training
                  stage on FPGA in \cite{ortega2016}.


 \item[$\bullet$] RQ4: What are the limitations of current research?\\
                  Currently, due to the number of computational resources (i.e. memory
                  and processing) needed by HNNs implementations, researches aim to
                  commercials FPGA-based systems (i.e. mostly large FPGAs), while the
                  design of HNNs VLSI chips remains limited. Moreover, there are others
                  factors that restrict the VLSI research such as: the power
                  consumption, die size, fabrication technology and cost.
                  Another important reason is that there is not a general agreement
                  between approaches and there are studies that consider parameters that
                  are not important to others studies, widening the exploration space but
                  reducing the concentrated efforts. Thus, there are a few
                  implementation frameworks proposed and their quality variates from
                  barely acceptable to regular.


 \item[$\bullet$] RQ5: Is the quality of the implementation frameworks improving?\\
                  Due to the growing interest in implementing HNN,  the number of
                  proposed frameworks by year has been increasing, and so the quality
                  of studies published. For example, current studies offer a more
                  complete description of the proposed work, make comparisons with
                  similar studies and provide external links that widen the
                  information available.
\end{itemize}




% Conclusions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}
This chapter presented a Systematic Literature Review of the latest works related to
the implementation of neural networks into hardware, from which two main streams can be
identified: HNNs VLSI designs and HNNs implementation in reconfigurable platforms.
Hence, the last one has the majority of studies due to the known constrictions
of the VLSI fabrication process. Also, it was found that CNNs are the most  aimed
NN because of its features and applications.
In addition, the awaken interest in HNNs has revealed the necessity of implementation
frameworks, that allow to identify relevant parameters and that present a solid number
of stages for accomplish the implementation.
Unfortunately, frameworks available in the state of the art lack of simplicity, usually
aim to bigger hardware platforms, have an excessive use of logic resources and present
an acceptable accuracy.\\
Moreover, there are several problems in the implementation process that still had to
be tackle down like: memory bottlenecks, scarce number of resources, complexity of the
NNs, implementation precision and accuracy, and efficient HNNs training.


% Research Questions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Research Questions}
These are the research questions that emerge from the literature review.

\begin{itemize}

  \item[$\bullet$] How to create a framework that allow to implement neural networks 
                   in hardware?

  \item[$\bullet$] Is it possible to implement a NN accelerator to approach different
                   problems?
                   
  \item[$\bullet$] What are the NN parameters that affect the implementation of 
                   NN in hardware platforms?
  
\end{itemize}
 

% Hypothesis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Hypothesis}
This section presents some hypothesis formulated around the research questions:

\begin{itemize}

 \item[$\bullet$] How the input data type and the processing tasks can be used to 
                  determine the NN topology and the design of the HNN accelerator?
 
 \item[$\bullet$] How the different works that can be found in the literature can be 
                  use to implement a cost-effective and easy-deployment framework 
                  that aim to solve different problems?

 \item[$\bullet$] How are the number of neurons per layer related with the number of 
                  computational resources needed to implement the HNN in a hardware
                  platform?
                  
 \item[$\bullet$] How are the neurons activation function related with the amount of 
                  computational resources needed to implement the HNN in a hardware
                  platform?
                  
 \item[$\bullet$] How the use of fixed point or floating point algorithms affects the
                  NN accuracy?
\end{itemize}


% 
% 
% % Table: Selected journals and conference proceddings
% %------------------------------------------------------------------------
% {
% \newcommand{\sm}{\small}
% \newcommand{\cc}{\cellcolor}
% \newcommand{\cn}{\centering}
% \newcommand{\tb}{\textbf}
% \newcommand{\tn}{\tabularnewline}
%  \definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}
% 
% \begin{table}[!htbp] 
%   \begin{center}
%   \begin{tabular}{|p{3cm}|p{3cm}|p{3cm}|p{3cm}|p{3cm}|}\hline
%     % use packages: color,colortbl
%     \rowcolor{tcA}
%                                         & \tb{Year} & \tb{}     & \tb{}     & \tb{}   \tn \hline
%                                         & \tb{2004} & \tb{2005} & \tb{2006} & \tb{2007} \tn \hline
%     Number of studies                   & XX & XX & XX & XX \tn \hline
%      Mean quality score                  & XX & XX & XX & XX \tn \hline
%     Standard deviation of quality score & XX & XX & XX & XX \tn \hline
%   \end{tabular}
%   \end{center}
%   \label{tab:seljouconpro}
%   \caption[Selected journals and conference proceedings.]
%           {Selected journals and conference proceedings.}
% \end{table}
% }
% 


