\nobreakspace {}\hfill \textbf {Pag.}\par 
\contentsline {chapter}{Contents}{viii}{section*.1}%
\contentsline {chapter}{List of Figures}{xi}{section*.2}%
\contentsline {chapter}{List of Tables}{xii}{section*.3}%
\contentsline {chapter}{\chapternumberline {1}Facial Expression Recognition (FER) Systems}{3}{chapter.1}%
\contentsline {section}{\numberline {1}Facial Expression Recognition (FER) Systems}{4}{section.1.1}%
\contentsline {subsection}{\numberline {1.1}CNN-based FER Systems}{4}{subsection.1.1.1}%
\contentsline {section}{\numberline {2}Data sets for FER systems}{7}{section.1.2}%
\contentsline {section}{\numberline {3}Basic data set preprocessing for FER systems}{8}{section.1.3}%
\contentsline {section}{\numberline {4}Extended data set preprocessing for FER systems}{10}{section.1.4}%
\contentsline {subsection}{\numberline {4.1}Local Binary Pattern}{10}{subsection.1.4.1}%
\contentsline {subsection}{\numberline {4.2}Data Augmentation}{11}{subsection.1.4.2}%
\contentsline {section}{\numberline {5}Designing a CNN-based FER system}{13}{section.1.5}%
\contentsline {subsection}{\numberline {5.1}Testing model M1}{15}{subsection.1.5.1}%
\contentsline {subsection}{\numberline {5.2}Testing model M2}{16}{subsection.1.5.2}%
\contentsline {subsection}{\numberline {5.3}Testing model M3}{18}{subsection.1.5.3}%
\contentsline {subsection}{\numberline {5.4}Testing model M4}{19}{subsection.1.5.4}%
\contentsline {subsection}{\numberline {5.5}Testing model M6}{20}{subsection.1.5.5}%
\contentsline {section}{\numberline {6}Discussion}{21}{section.1.6}%
\contentsline {section}{\numberline {7}Conclusion}{21}{section.1.7}%
\contentsline {chapter}{\chapternumberline {2}Hardware Neural Networks (HNN)}{22}{chapter.2}%
\contentsline {section}{\numberline {1}Quantization Aware Training}{23}{section.2.1}%
\contentsline {section}{\numberline {2}Hardware Platform}{25}{section.2.2}%
\contentsline {section}{\numberline {3}Hardware-software Development}{29}{section.2.3}%
\contentsline {subsection}{\numberline {3.1}Base SoC Design}{29}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {3.2}Tflite Core}{31}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {3.3}Conv Core}{34}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {3.4}MaxPooling Core}{36}{subsection.2.3.4}%
\contentsline {subsection}{\numberline {3.5}Dense Core}{37}{subsection.2.3.5}%
\contentsline {subsection}{\numberline {3.6}Additional functions}{39}{subsection.2.3.6}%
\contentsline {section}{\numberline {4}Putting all together: the Hardware Neural Network Accelerator}{41}{section.2.4}%
\contentsline {subsection}{\numberline {4.1}FER SoC design}{41}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {4.2}FER SoC testing}{45}{subsection.2.4.2}%
\contentsline {section}{\numberline {5}Discussion}{47}{section.2.5}%
\contentsline {section}{\numberline {6}Conclusion}{47}{section.2.6}%
\contentsline {chapter}{\chapternumberline {3}Resiliency: A Framework for Training a CNN with a custom Hardware-Software (HW-SW) Architecture.}{48}{chapter.3}%
\contentsline {section}{\numberline {1}Custom hardware-software architectures aimed to ML algorithms}{49}{section.3.1}%
\contentsline {section}{\numberline {2}Creating a Pynq image for Zybo-Z7}{51}{section.3.2}%
\contentsline {subsection}{\numberline {2.1}Create the Board Directory}{52}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {2.2}Create the Board Support Package (BSP) (30 min approx.)}{53}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {2.3}Create the image (2h30 min approx.)}{55}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {2.4}Copy image to micro SD card (20 min approx.)}{58}{subsection.3.2.4}%
\contentsline {section}{\numberline {3}Testing ML Frameworks on the hw-sw architectures proposed}{59}{section.3.3}%
\contentsline {subsection}{\numberline {3.1}Tensorflow 2.5 in Zybo-Z7 running Pynq 2.7}{59}{subsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Installing Python3.7 (50 min approx.)}{59}{subsubsection.3.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Create a virtual environment for Python3.7 (5 min approx.)}{59}{subsubsection.3.3.1.2}%
\contentsline {subsubsection}{\numberline {3.1.3}Install TensorFlow in the virtual environment (\textgreater \nobreakspace {}5h approx.)}{60}{subsubsection.3.3.1.3}%
\contentsline {subsubsection}{\numberline {3.1.4}Create the IPython Kernel (1h approx.)}{61}{subsubsection.3.3.1.4}%
\contentsline {subsubsection}{\numberline {3.1.5}Testing Tensorflow\nobreakspace {}2.5 running on Zybo-Z7 with Pynq 2.7}{61}{subsubsection.3.3.1.5}%
\contentsline {subsection}{\numberline {3.2}PyTorch 1.8 in Zybo-Z7 running Pynq 2.7}{63}{subsection.3.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Installing Python3.7 (50 min approx.)}{63}{subsubsection.3.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Create a virtual environment for Python3.7 (5 min approx.)}{63}{subsubsection.3.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}Install PyTorch in the virtual environment (\textgreater \nobreakspace {}2h approx.)}{63}{subsubsection.3.3.2.3}%
\contentsline {subsubsection}{\numberline {3.2.4}Create the IPython Kernel (1h approx.)}{64}{subsubsection.3.3.2.4}%
\contentsline {subsubsection}{\numberline {3.2.5}Testing PyTorch\nobreakspace {}1.8 running on Zybo-Z7 with Pynq 2.7}{64}{subsubsection.3.3.2.5}%
\contentsline {subsection}{\numberline {3.3}Testing model M6 in Zybo-Z7 running Pynq 2.7}{66}{subsection.3.3.3}%
\contentsline {section}{\numberline {4}Resiliency: A Framework for Training a CNN with a custom Hardware-Software Architecture.}{68}{section.3.4}%
\contentsline {section}{\numberline {5}Discussion}{71}{section.3.5}%
\contentsline {section}{\numberline {6}Conclusion}{72}{section.3.6}%
\contentsline {chapter}{Bibliography}{75}{chapter*.63}%
\contentsline {appendix}{\chapternumberline {A}State of the art of the Hardware Neural Networks (HNNs)}{81}{appendix.A}%
\contentsline {section}{\numberline {1}Systematic Literature Review Method}{81}{section.A.1}%
\contentsline {subsection}{\numberline {1.1}Research Questions (RQ)}{82}{subsection.A.1.1}%
\contentsline {subsection}{\numberline {1.2}Search Process}{82}{subsection.A.1.2}%
\contentsline {subsection}{\numberline {1.3}Study selection}{83}{subsection.A.1.3}%
\contentsline {subsection}{\numberline {1.4}Quality assessment (QA)}{84}{subsection.A.1.4}%
\contentsline {subsection}{\numberline {1.5}Data collection}{84}{subsection.A.1.5}%
\contentsline {subsection}{\numberline {1.6}Data analysis}{85}{subsection.A.1.6}%
\contentsline {section}{\numberline {2}Results}{85}{section.A.2}%
\contentsline {subsection}{\numberline {2.1}Search Results}{85}{subsection.A.2.1}%
\contentsline {subsection}{\numberline {2.2}Quality evaluation of the HNN works.}{85}{subsection.A.2.2}%
\contentsline {subsection}{\numberline {2.3}Quality factors}{85}{subsection.A.2.3}%
\contentsline {section}{\numberline {3}Discussion}{86}{section.A.3}%
\contentsline {section}{\numberline {4}Conclusions}{88}{section.A.4}%
\contentsline {section}{\numberline {5}Research Questions}{88}{section.A.5}%
\contentsline {section}{\numberline {6}Hypothesis}{89}{section.A.6}%
\contentsline {appendix}{\chapternumberline {B}Useful Scripts}{90}{appendix.B}%
\contentsline {appendix}{\chapternumberline {C}Complementary Resources}{98}{appendix.C}%
