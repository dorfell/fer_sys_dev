%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 \documentclass[mathserif,compress]{beamer}

\usetheme{Singapore}
\usecolortheme[rgb={0.4392,0.5019,0.5647}]{structure}% Slate Gray
\definecolor{slategray}{rgb}{0.4392,0.5019,0.5647}

%*******************************************%
% ***  My Commands and Packages  ***

\usepackage[latin1]{inputenc}
%\usepackage[spanish]{babel}

\usepackage{amsfonts,amsmath,amsthm}
\usepackage{amssymb,array,amssymb}

\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{verbatim}

\usepackage{color}
\usepackage{alltt}
\usepackage{rotating}
\usepackage{multirow}
\usepackage{ragged2e}
\usepackage{textcomp}
\usepackage{listings}
%\usepackage{multicolumn}


\usepackage{amsmath}   % Equations
\usepackage{mathtools} % Equations

\usepackage{multicol}  % Tables Col
\usepackage{multirow}  % Tables Row
%\usepackage[table]{xcolor} % Table Color
\usepackage{color}     %Color
\usepackage{colortbl}  %Table color

\usepackage{caption} % Caption package
%\usepackage{subcaption}
\captionsetup[table]{name=Tabla}
\captionsetup[figure]{name=Figura}


% Centering table values
\usepackage{array}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}} % Horizontal centering
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}} % Vertical centering
% use in the following way: 
% \begin{tabular}{|P{2.5cm}|P{2.5cm}|P{2.5cm}}\hline or
% \begin{tabular}{|M{2.5cm}|M{2.5cm}|M{2.5cm}}\hline or   
   
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercovered{highly dynamic}

\newcommand{\red}[1]{\textcolor[rgb]{1.0,0.0,0.0}{ #1}}
\newcommand{\slategray}[1]{\textcolor[rgb]{0.4392,0.5019,0.5647}{ #1}}% Slate Gray
\renewcommand\footnoterule{\rule{\linewidth}{0pt}} % Footnote without  line
\renewcommand\footnotemark{}


\setbeamertemplate{caption}[numbered]
\title{\bf Modelo Anal\'itico para estimar el tiempo de ejecuci\'on de una implementaci\'on
           de la ecuaci\'on de onda ac\'ustica $\mathbf{3D}$ usando FDTD en una GPU\\}

%\subtitle{Propuesta de Tesis II}
\vspace{-1cm}
\author{ \slategray{Presentado por:} Dorfell L. Parra Prada}

\institute[Universidad Nacional de Colombia]{\large Facultad de Ingenier\'ias\\
\slategray{Universidad Nacional de Colombia}}

\date{Junio 10, 2016}

\usefonttheme{professionalfonts}
%\usebackgroundtemplate{\includegraphics[width=\paperwidth]{img/logos/fondo2.eps}}


\logo{%
  \makebox[0.95\paperwidth]{%
    %\includegraphics[width=2cm,height=2cm,keepaspectratio]{./img/logos/unal_dorfell.eps}
    \hfill%
    \includegraphics[width=2cm,height=2cm,keepaspectratio]{./img/logos/unal.eps}%
    \hspace{0.5cm}%    
  }%
}


% Attache color to a struct (footlinerule)
\setbeamercolor{footlinerule}{use=structure,bg=structure.fg!500!bg,black}
\setbeamertemplate
 {footline}{ 
	    \begin{beamercolorbox}[wd=\paperwidth,ht=0.3ex,dp=0ex,center]{footlinerule}
            \end{beamercolorbox}%
            \begin{beamercolorbox}[wd=\paperwidth,ht=0.6ex,dp=0ex,center]{empty}
            \end{beamercolorbox}%
            \leavevmode%
            Propuesta de Investigaci\'on  \hfill    Dorfell L. Parra Prada
            \quad\insertframenumber/\inserttotalframenumber\strut\quad
            } 


\begin{document}

\lstset{numbers=left, numberstyle=\tiny, stepnumber=1, numbersep=8pt, framexleftmargin=5mm, frame=shadowbox, captionpos=b, showstringspaces=0}

\lstdefinestyle{C}{
language=[Handel]C, basicstyle=\scriptsize, commentstyle=\color{blue}, stringstyle=\ttfamily\color{magenta}, classoffset=0, otherkeywords={!}, keywordstyle=\color{green}, classoffset=1, keywords=[1]{ssize_t, size_t, depura}, keywordstyle=[1]\color{green}, classoffset=2, keywords=[2]{EFAULT, NULL}, keywordstyle=[2]\color{magenta},  classoffset=0, keywords=[3]{_asm_}, keywordstyle=[3]\color{blue}
}

\lstdefinestyle{consola}{
language=[Handel]C, basicstyle=\scriptsize, backgroundcolor=\color{gray75},
}

\lstdefinestyle{vhdl}{
language=[AMS]VHDL, basicstyle=\scriptsize, commentstyle=\color{green}, classoffset=0, otherkeywords={!}, keywordstyle=\color{blue}, classoffset=1, keywords=[1]{std_logic, std_logic_vector, STD_LOGIC_1164, STD_LOGIC_ARITH, STD_LOGIC_UNSIGNED, IEEE}, keywordstyle=[1]\color{magenta}, classoffset=0, morecomment=[l][\color{green}]{\#}
}

\lstdefinestyle{Assembler}{
language=[x86masm]Assembler, basicstyle=\scriptsize, commentstyle=\color{blue}, backgroundcolor=\color{gray75}, keywordstyle=\color{green}, morecomment=[l][\color{blue}]{\#}, otherkeywords={move, lw, addiu}, emph=[1]{\$s0,\$s1,\$s2,\$s3,\$s4, \$t1, \$t2, \$t3, \$t4, \$t5, \$t6, \$fp}, emphstyle=[1]\color{white}, emph=[2]{\$2, \$3, \$16, \$17, \$18, \$19, \$20, \$21 }, emphstyle=[2]\color{red}
}
 
 
%===============================================
% Presentation- Template
%===============================================
\begin{frame}
\titlepage
\vspace{-1.5cm} \hspace{-0.7 cm}
%\begin{minipage}[r]{1.5cm}\includegraphics[width=1.5cm]{img/logos/uis.eps}\end{minipage}
\end{frame}

%===============================================
% Schedule
%===============================================
\begin{frame}
\frametitle{Agenda}
\tableofcontents[hideallsubsections] 
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{ }
%===============================================
% 
%===============================================
%\subsection{  }
%\begin{frame}
%  \frametitle{  }
%  \begin{figure}
%    \centering
%    \includegraphics[scale=0.4]{img/  .eps}
%    \caption{GPU applications. Taken from \cite{GK110}}
%  \end{figure}
%\end{frame}

%\begin{frame}
%  \frametitle{   }
%  \begin{block}{   }
%  \begin{itemize}
%    \item 
%    \item 
%  \end{itemize}
%  \end{block}
%\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Formulaci\'on del Problema 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Problema}
\begin{frame}
  \begin{center}
    \textcolor{slategray}{\textbf{\Huge{Formulaci\'on del Problema}}}
   \end{center}
\end{frame}



%===============================================
% Modelado S\'ismico
%===============================================
\subsection{}
\begin{frame}
  \frametitle{Modelado S\'ismico}
  \vspace{-1cm}
  \begin{columns}
    \begin{column}{0.49\textwidth}
      \begin{figure}
	\centering
	\includegraphics[scale=0.09]{img/2layers_3D.eps}
	\caption{\small{Modelo de Velocidad de dos capas.}}
      \end{figure}
      \vspace{0.5cm}
      \footnotesize{
      \begin{equation}
	\nabla^2 p = \frac{\partial^2p}{{\partial x}^2} + \frac{\partial^2p}{{\partial y}^2} +
	\frac{\partial^2p}{{\partial z}^2} = \frac{1}{v^2}\frac{\partial^2p}{{\partial t}^2} \tag{1}
	\label{eq:onda}
      \end{equation}}
    \end{column}
    \begin{column}{0.49\textwidth}
      \begin{figure}
	\centering
	\visible<3-|handout:0>{\includegraphics[scale=0.12]{img/cpu_imp_svm_02.eps}\\}
	\visible<4-|handout:0>{\includegraphics[scale=0.12]{img/cpu_imp_svm_05.eps}}\par
	\caption{\\Propagaci\'on.}
      \end{figure}
   \end{column}
  \end{columns} 
  \vspace{1cm}
 \end{frame}



%===============================================
% Unidad de Procesamiento Gr\'afico GPU 
%===============================================
\subsection{Unidad de Procesamiento Gr\'afico GPU}
\begin{frame}
  \frametitle{Unidad de Procesamiento Gr\'afico GPU}
  \begin{figure}
    \centering
    \includegraphics[scale=0.4]{img/gpu_nvidia.eps}
    \caption{GPU Nvidia. Adaptado de \cite{NVIDIA}.}
  \end{figure}
\end{frame}



%===============================================
% Formulaci\'on / Declaraci\'on del Problema 
%===============================================
\subsection{Formulaci\'on del Problema}
\begin{frame}
  \frametitle{Formulaci\'on del Problema}
  \pause
  �C\'omo podemos predecir el tiempo de una implementaci\'on?
 \end{frame}
 
 
\begin{frame}
  \frametitle{Formulaci\'on del Problema  }
  �C\'omo podemos predecir el tiempo de una implementaci\'on?
  \begin{figure}
    \centering
    \includegraphics[scale=0.4]{img/modelo_general.eps}
    \caption{Modelo General.}
  \end{figure}
\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Propuesta de Investigaci\'on 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Propuesta de Investigaci\'on}
\begin{frame}
  \begin{center}
    \textcolor{slategray}{\textbf{\Huge{Propuesta de Investigaci\'on}}}
   \end{center}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Objetivos 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Objetivos}
%===============================================
% Objetivo General
%===============================================
\subsection{Objetivo General}
\begin{frame}
  %\frametitle{}
  \begin{block}{Objetivo General}
  \begin{itemize}
    \item [$\bullet$] Proponer una metodolog\'ia que permita  predecir el tiempo
	              de ejecuci\'on, de la implementaci\'on de la soluci\'on de 
	              la ecuaci\'on de onda ac\'ustica 3D con densidad constante,
	              usando FDTD en una GPU.
    
  \end{itemize}
  \end{block}
\end{frame}


\begin{frame}
  %\frametitle{}
  \begin{block}{Objetivos Espec\'ificos}
  \begin{itemize}
      \item [$\bullet$] Implementar la soluci\'on de la ecuaci\'on de onda ac\'ustica 3D
                        con densidad constante, usando FDTD en una GPU.
      \item [$\bullet$] Proponer un modelo general que prediga el tiempo de ejecuci\'on
	                de la implementaci\'on en una GPU, usando como base \textit{benchmarks}
			y  modelos propuestos en la literatura.
      \item [$\bullet$] Validar la predicci\'on del tiempo de  ejecuci\'on de la implementaci\'on
			obtenido con el modelo general propuesto.
      \item [$\bullet$] Documentar la metodolog\'ia empleada en la construcci\'on y el
                        uso del modelo general.
    
  \end{itemize}
  \end{block}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Resultados 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Resultados}
%===============================================
% Resultados y Productos
%===============================================
\subsection{Resultados y Productos}
\begin{frame}
  \frametitle{Resultados y Productos}
    Durante el desarrollo de la tesis se proponen los siguientes resultados y productos:
      {
      \newcommand{\sm}{\small}
      \newcommand{\cc}{\cellcolor}
      \newcommand{\cn}{\centering}
      \newcommand{\tn}{\tabularnewline}
      \definecolor{tcA}{rgb}{0.764706,0.764706,0.764706}
      \tiny
    \begin{table}[!htbp] 
      \begin{center}
	\begin{tabular}{|M{2cm}|M{5cm}|M{1.5cm}|}\hline
	% use packages: color,colortbl
	\rowcolor{tcA}
	\textbf{Resultado}	             & \textbf{Indicador Verificable}                   & \textbf{Fecha}\\\hline
	{Implementaci\'on del algoritmo de 
	 FDTD 3D en una GPU}   	             & Implementaci\'on b\'asica del algoritmo de FDTD 
	                                       3D en una GPU. Sobre esta implementaci\'on se 
	                                       aplicar\'an las estrategias de mejora de acceso
	                                       a memoria global \textit{coalesced} y uso de 
	                                       memoria compartida.                              & \cn M1-M4 \tn\hline
	{Identificaci\'on de par\'ametros
	 y especificaciones hardware}        & Identificaci\'on de los par\'ametros de la 
	                                       implementaci\'on  FDTD 3D y especificaciones del
	                                       hardware de la GPU  que afectan el  
	                                       tiempo de ejecuci\'on.                           & \cn M1-M4\tn\hline
	{Modelo General}                     & Modelo general que permite predecir el tiempo
					       de ejecuci\'on  de la implementaci\'on FDTD 3D   
					       en una GPU a partir de los par\'ametros y 
					       especificaciones identificadas.                  & \cn M3-M7 \tn\hline
	{Metodolog\'ia}  		     & Documento que describe la serie de pasos 
					       empleados en la construcci\'on del modelo general
					       y el c\'omo usar el modelo general.		& \cn M8-M10 \tn\hline
        {Informe de resultados}  	     & Informes peri\'odicos que presenten los avances
					       en el desarrollo de la tesis y las observaciones
					       encontradas  en el proceso.			& \cn M1-M10 \tn\hline
	\end{tabular}
     \end{center}
     \caption[Indicadores y Plazos de los Resultados del Proyecto]{Indicadores y Plazos de los Resultados del Proyecto.}
    \end{table}
      }
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Metodolog\'ia 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Metodolog\'ia}
%===============================================
% Metodolog\'ia 
%===============================================
\subsection{Metodolog\'ia}
\begin{frame}
  \frametitle{Metodolog\'ia}
  \begin{figure}
    \centering
    \includegraphics[scale=0.3]{img/metodologia_final.eps}
    \caption{Metodolog\'ia Propuesta. }
  \end{figure}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cronograma 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Cronograma}
%===============================================
% Cronograma
%===============================================
\subsection{Cronograma}
\begin{frame}
 \frametitle{Cronograma}
 \begin{table}[!htbp]
    \newcommand{\cc}{\cellcolor}
    \newcommand{\cn}{\centering}
    \newcommand{\tn}{\tabularnewline}
    \newcommand{\ti}{\textit}
    \definecolor{tcB}{rgb}{0.752941,0.8509,1} % blue :-)
    \definecolor{tcR}{rgb}{1       ,0     ,0} % Red
    \definecolor{tcO}{rgb}{1       ,0.5843,0} % Orange
    \definecolor{tcY}{rgb}{1       ,1     ,0} % Yellow
    \definecolor{tcGY}{rgb}{0.8235 ,1     ,0.0274} % Green-Yellow
    \definecolor{tcG}{rgb}{0       ,1      ,0} % Green
    \definecolor{tcW}{rgb}{1       ,1     ,1} % White
    \begin{center}
       \scriptsize
       \hspace{-0.5cm} 2014  \hspace{4cm} 2015  
      \begin{tabular}{|p{1.8cm}|p{0.3cm}|p{0.3cm}|p{0.03cm}|p{0.3cm}|p{0.3cm}|p{0.3cm}|p{0.3cm}|p{0.3cm}|p{0.3cm}|p{0.3cm}|p{0.3cm}|p{0.6cm}|p{0.6cm}|p{0.6cm}|}\hline
        \rowcolor{tcB}                  
        \ti{Actividad}                            &\cn{\ti{M1}} &\cn{\ti{M2}}  &\cn{\ti{M3}}  &\cn{\ti{M4}}  &\cn{\ti{M5}}  &\cn{\ti{M6}}  &\cn{\ti{M7}}  &\cn{\ti{M8}}  &\cn{\ti{M9}}  &\cn{\ti{M10}} &\cn{\ti{M11}} &\ti{M12} \\\hline
	\cn{\ti{Implementaci\'on FDTD 3D en CPU}} &\cc{tcG}     &\cc{tcG}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW} \\\hline
	\cn{\ti{Implementaci\'on FDTD 3D en GPU}} &\cc{tcG}     &\cc{tcG}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW} \\\hline
	\cn{\ti{Identificaci\'on Par\'ametros}}   &\cc{tcW}     &\cc{tcGY}     &\cc{tcGY}     &\cc{tcGY}     &\cc{tcGY}     &\cc{tcGY}     &\cc{tcGY}     &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW} \\\hline
	\cn{\ti{Modelo General}}                  &\cc{tcW}     &\cc{tcW}      &\cc{tcW}      &\cc{tcY}      &\cc{tcY}      &\cc{tcY}      &\cc{tcY}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW} \\\hline
	\cn{\ti{Informe de Resultados}}           &\cc{tcW}     &\cc{tcW}      &\cc{tcO}      &\cc{tcO}      &\cc{tcO}      &\cc{tcO}      &\cc{tcO}      &\cc{tcO}      &\cc{tcO}      &\cc{tcO}      &\cc{tcO}      &\cc{tcW} \\\hline
	\cn{\ti{Metodolog\'ia}}                   &\cc{tcW}     &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcW}      &\cc{tcR}      &\cc{tcR}      &\cc{tcR}      &\cc{tcR}      &\cc{tcR} \\\hline
      \end{tabular}
      \end{center}
      \caption[Cronograma del Proyecto]{Cronograma del Proyecto.}
      \label{tab_cron}
    \end{table}
\end{frame}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Appendix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Ap\'endice}
%===============================================
% Appendix
%===============================================
%\subsection{  }
%\begin{frame}
%  \frametitle{Stencil Equation  }
%  \begin{figure}
%    \centering
%    \includegraphics[scale=0.6]{img/eq_1.eps}
%    \caption{Stencil Equation. Taken from \cite{Mici2009}}
%  \end{figure}
%    \footnote{\tiny{Paulius Micikevicius.``3D Finite Difference Computation on
%                    GPUs using CUDA'', Nvidia, ACM 2009}}  
%\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Questions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{�Questions?}
%===============================================
%  Questions
%===============================================
%\subsection{�Preguntas?}
%\begin{frame}
%   \vspace{1.3cm}
%    \includegraphics[scale=0.40]{./img/logos/mafalda.eps}
%    \includegraphics[scale=0.12]{./img/logos/burro.eps}
%    \includegraphics[scale=0.175]{./img/logos/babe.eps}
%\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\input{bibliography.tex}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Appendix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\input{appendix.tex}



%===============================================
% END GNU License
%===============================================
\begin{frame}

\includegraphics[height=1.0cm]{img/logos/logo_deed.eps}\\
Non-Comercial Colombia Attribution 2.5 

You are free to:
\begin{itemize}
 \item copy, distribute, show, and play this work.
 \item Modify the work.
\end{itemize}

Under the following conditions:
\begin{description}
  \item[]\includegraphics[height=0.7cm]{img/logos/deed.eps} Atribution.
	 You must attribute the work in the way specified by author or the licenses.
  \item[]\includegraphics[height=0.7cm] {img/logos/deed2.eps} Non Comercial. 
         You can not use this work with profit purposes.
\end{description}

Its legal uses and rigths are not affected by the previously dispose.

\href{http://creativecommons.org/licenses/by-nc/2.5/co/legalcode}
{\hspace{0.6cm}\red{ http://creativecommons.org/licenses/by-nc/2.5/co/legalcode}}

\end{frame}






\end{document}
