#!/bin/bash -f
# ****************************************************************************
# Vivado (TM) v2021.1 (64-bit)
#
# Filename    : compile.sh
# Simulator   : Xilinx Vivado Simulator
# Description : Script for compiling the simulation design source files
#
# Generated by Vivado on Sat Sep 04 11:56:29 -05 2021
# SW Build 3247384 on Thu Jun 10 19:36:07 MDT 2021
#
# IP Build 3246043 on Fri Jun 11 00:30:35 MDT 2021
#
# usage: compile.sh
#
# ****************************************************************************
set -Eeuo pipefail
# compile Verilog/System Verilog design sources
echo "xvlog --incr --relax -L uvm -L axi_vip_v1_1_10 -L processing_system7_vip_v1_0_12 -L xilinx_vip -prj fer_soc_bd_wrapper_vlog.prj"
xvlog --incr --relax -L uvm -L axi_vip_v1_1_10 -L processing_system7_vip_v1_0_12 -L xilinx_vip -prj fer_soc_bd_wrapper_vlog.prj 2>&1 | tee compile.log

# compile VHDL design sources
echo "xvhdl --incr --relax -prj fer_soc_bd_wrapper_vhdl.prj"
xvhdl --incr --relax -prj fer_soc_bd_wrapper_vhdl.prj 2>&1 | tee -a compile.log

echo "Waiting for jobs to finish..."
echo "No pending jobs, compilation finished."
