#!/bin/sh

# 
# Vivado(TM)
# runme.sh: a Vivado-generated Runs Script for UNIX
# Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
# 

if [ -z "$PATH" ]; then
  PATH=/usr/local/Xilinx_2021.01/Vitis/2021.1/bin:/usr/local/Xilinx_2021.01/Vivado/2021.1/ids_lite/ISE/bin/lin64:/usr/local/Xilinx_2021.01/Vivado/2021.1/bin
else
  PATH=/usr/local/Xilinx_2021.01/Vitis/2021.1/bin:/usr/local/Xilinx_2021.01/Vivado/2021.1/ids_lite/ISE/bin/lin64:/usr/local/Xilinx_2021.01/Vivado/2021.1/bin:$PATH
fi
export PATH

if [ -z "$LD_LIBRARY_PATH" ]; then
  LD_LIBRARY_PATH=
else
  LD_LIBRARY_PATH=:$LD_LIBRARY_PATH
fi
export LD_LIBRARY_PATH

HD_PWD='/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc.runs/synth_1'
cd "$HD_PWD"

HD_LOG=runme.log
/bin/touch $HD_LOG

ISEStep="./ISEWrap.sh"
EAStep()
{
     $ISEStep $HD_LOG "$@" >> $HD_LOG 2>&1
     if [ $? -ne 0 ]
     then
         exit
     fi
}

EAStep vivado -log fer_soc_bd_wrapper.vds -m64 -product Vivado -mode batch -messageDb vivado.pb -notrace -source fer_soc_bd_wrapper.tcl
