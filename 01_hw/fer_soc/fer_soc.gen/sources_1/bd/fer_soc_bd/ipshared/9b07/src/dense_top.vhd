---------------------------------------------------------------------
----                                                             ----
---- file:    dense_top.vhd                                      ----
---- brief:   Module de Dense TOP                                ----
---- details: Module top level top level pour le noyau de        ----
----          Dense ou full connected à la facon de tflite.      ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/11/15                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity dense_top is
  port( 
    clk, rstn, start :  in std_logic;

    -- Entrée 
    x01, x02, x03, x04, x05, x06, x07, x08, x09, x10, x11, x12, x13, x14, x15, x16 :  in signed( 7 downto 0);
    x17, x18, x19, x20, x21, x22, x23, x24, x25, x26, x27, x28, x29, x30, x31, x32 :  in signed( 7 downto 0);
    x33, x34, x35, x36, x37, x38, x39, x40, x41, x42, x43, x44, x45, x46, x47, x48 :  in signed( 7 downto 0);
    x49, x50, x51, x52, x53, x54, x55, x56, x57, x58, x59, x60, x61, x62, x63, x64 :  in signed( 7 downto 0);
    
    -- Filtre, poids w
    w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, w11, w12, w13, w14, w15, w16 :  in signed( 7 downto 0);
    w17, w18, w19, w20, w21, w22, w23, w24, w25, w26, w27, w28, w29, w30, w31, w32 :  in signed( 7 downto 0);
    w33, w34, w35, w36, w37, w38, w39, w40, w41, w42, w43, w44, w45, w46, w47, w48 :  in signed( 7 downto 0);
    w49, w50, w51, w52, w53, w54, w55, w56, w57, w58, w59, w60, w61, w62, w63, w64 :  in signed( 7 downto 0);

    -- Offset entree
    offset_ent :  in signed(15 downto 0);

    -- ds_tab[0, i] valeur précédent
    ds_in      :  in signed(31 downto 0);
    
    -- Sortie du noyau: (x + offset_ent)*w + ds_in[0,i,j,f]
    ds_out     : out signed(31 downto 0);
    
    -- dense_top done
    dn_ds      : out std_logic );
end dense_top;


architecture Behavioral of dense_top is


  -- Component Declaration for the instances
  ------------------------------------------------------

  -- dense_core0: (x + offset_ent)
  component dense_core0 is
    port( 
      -- Entrée 
      x01, x02, x03, x04, x05, x06, x07, x08, x09, x10, x11, x12, x13, x14, x15, x16 :  in signed( 7 downto 0);
      x17, x18, x19, x20, x21, x22, x23, x24, x25, x26, x27, x28, x29, x30, x31, x32 :  in signed( 7 downto 0);
      x33, x34, x35, x36, x37, x38, x39, x40, x41, x42, x43, x44, x45, x46, x47, x48 :  in signed( 7 downto 0);
      x49, x50, x51, x52, x53, x54, x55, x56, x57, x58, x59, x60, x61, x62, x63, x64 :  in signed( 7 downto 0);

      -- Offset entree 
      offset_ent : in signed(15 downto 0);

      -- Sortie (x + offset_ent)
      xo01, xo02, xo03, xo04, xo05, xo06, xo07, xo08, xo09, xo10, xo11, xo12, xo13, xo14, xo15, xo16 : out signed(15 downto 0);
      xo17, xo18, xo19, xo20, xo21, xo22, xo23, xo24, xo25, xo26, xo27, xo28, xo29, xo30, xo31, xo32 : out signed(15 downto 0);
      xo33, xo34, xo35, xo36, xo37, xo38, xo39, xo40, xo41, xo42, xo43, xo44, xo45, xo46, xo47, xo48 : out signed(15 downto 0);
      xo49, xo50, xo51, xo52, xo53, xo54, xo55, xo56, xo57, xo58, xo59, xo60, xo61, xo62, xo63, xo64 : out signed(15 downto 0) );
  end component dense_core0;

  
  -- conv_core1: (x + offset_ent)*w
  component dense_core1 is
    port( 
      clk        :  in std_logic;

      -- Entrée  (x + offset_ent)
      xo01, xo02, xo03, xo04, xo05, xo06, xo07, xo08, xo09, xo10, xo11, xo12, xo13, xo14, xo15, xo16 :  in signed(15 downto 0);
      xo17, xo18, xo19, xo20, xo21, xo22, xo23, xo24, xo25, xo26, xo27, xo28, xo29, xo30, xo31, xo32 :  in signed(15 downto 0);
      xo33, xo34, xo35, xo36, xo37, xo38, xo39, xo40, xo41, xo42, xo43, xo44, xo45, xo46, xo47, xo48 :  in signed(15 downto 0);
      xo49, xo50, xo51, xo52, xo53, xo54, xo55, xo56, xo57, xo58, xo59, xo60, xo61, xo62, xo63, xo64 :  in signed(15 downto 0); 

      -- Filtre, poids w
      w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, w11, w12, w13, w14, w15, w16 :  in signed( 7 downto 0);
      w17, w18, w19, w20, w21, w22, w23, w24, w25, w26, w27, w28, w29, w30, w31, w32 :  in signed( 7 downto 0);
      w33, w34, w35, w36, w37, w38, w39, w40, w41, w42, w43, w44, w45, w46, w47, w48 :  in signed( 7 downto 0);
      w49, w50, w51, w52, w53, w54, w55, w56, w57, w58, w59, w60, w61, w62, w63, w64 :  in signed( 7 downto 0);

      -- Sortie (x + offset_ent)*w
      xow01, xow02, xow03, xow04, xow05, xow06, xow07, xow08, xow09, xow10, xow11, xow12, xow13, xow14, xow15, xow16 : out signed(31 downto 0);
      xow17, xow18, xow19, xow20, xow21, xow22, xow23, xow24, xow25, xow26, xow27, xow28, xow29, xow30, xow31, xow32 : out signed(31 downto 0);
      xow33, xow34, xow35, xow36, xow37, xow38, xow39, xow40, xow41, xow42, xow43, xow44, xow45, xow46, xow47, xow48 : out signed(31 downto 0);
      xow49, xow50, xow51, xow52, xow53, xow54, xow55, xow56, xow57, xow58, xow59, xow60, xow61, xow62, xow63, xow64 : out signed(31 downto 0) ); 
  end component dense_core1;


  -- signal pour (x + offset_ent)
  signal sg_xo01, sg_xo02, sg_xo03, sg_xo04, sg_xo05, sg_xo06, sg_xo07, sg_xo08 : signed(15 downto 0);
  signal sg_xo09, sg_xo10, sg_xo11, sg_xo12, sg_xo13, sg_xo14, sg_xo15, sg_xo16 : signed(15 downto 0);
  signal sg_xo17, sg_xo18, sg_xo19, sg_xo20, sg_xo21, sg_xo22, sg_xo23, sg_xo24 : signed(15 downto 0);
  signal sg_xo25, sg_xo26, sg_xo27, sg_xo28, sg_xo29, sg_xo30, sg_xo31, sg_xo32 : signed(15 downto 0);
 
  signal sg_xo33, sg_xo34, sg_xo35, sg_xo36, sg_xo37, sg_xo38, sg_xo39, sg_xo40 : signed(15 downto 0);
  signal sg_xo41, sg_xo42, sg_xo43, sg_xo44, sg_xo45, sg_xo46, sg_xo47, sg_xo48 : signed(15 downto 0);
  signal sg_xo49, sg_xo50, sg_xo51, sg_xo52, sg_xo53, sg_xo54, sg_xo55, sg_xo56 : signed(15 downto 0);
  signal sg_xo57, sg_xo58, sg_xo59, sg_xo60, sg_xo61, sg_xo62, sg_xo63, sg_xo64 : signed(15 downto 0);
 
 
  -- signal pour (x + offset_ent)*w
  signal sg_xow01, sg_xow02, sg_xow03, sg_xow04, sg_xow05, sg_xow06, sg_xow07, sg_xow08 : signed(31 downto 0);
  signal sg_xow09, sg_xow10, sg_xow11, sg_xow12, sg_xow13, sg_xow14, sg_xow15, sg_xow16 : signed(31 downto 0);
  signal sg_xow17, sg_xow18, sg_xow19, sg_xow20, sg_xow21, sg_xow22, sg_xow23, sg_xow24 : signed(31 downto 0);
  signal sg_xow25, sg_xow26, sg_xow27, sg_xow28, sg_xow29, sg_xow30, sg_xow31, sg_xow32 : signed(31 downto 0);
  
  signal sg_xow33, sg_xow34, sg_xow35, sg_xow36, sg_xow37, sg_xow38, sg_xow39, sg_xow40 : signed(31 downto 0);
  signal sg_xow41, sg_xow42, sg_xow43, sg_xow44, sg_xow45, sg_xow46, sg_xow47, sg_xow48 : signed(31 downto 0);
  signal sg_xow49, sg_xow50, sg_xow51, sg_xow52, sg_xow53, sg_xow54, sg_xow55, sg_xow56 : signed(31 downto 0);
  signal sg_xow57, sg_xow58, sg_xow59, sg_xow60, sg_xow61, sg_xow62, sg_xow63, sg_xow64 : signed(31 downto 0);
  

  -- Compteur de cycles: registre, enable,done. 
  signal dd, qq   : std_logic_vector(6 downto 0) := (others=>'0');

  
begin




  ---------------------
  -- Compteur de cycles (cc)
  ---------------------
  -- Overflow pour clock cycles
  mux_ovf: 
    dd <= qq when (qq = x"48") else qq + 1;

  -- Registre du compteur
  process (clk, rstn)
    begin  
      if (rstn = '0') then
        qq <= (others => '0');
      elsif (clk'event and clk = '1') then
        if (start = '1') then
          qq <= dd;
        else
          qq <= qq;
        end if;
      end if;
  end process;

  -- Done aprés cycles
  dn_ds <= '1' when (qq = x"48") else '0'; 




  --! Instantiate the submodules Convolution Core (CC)
  -----------------------------------------
  
  CC0: dense_core0
    port map(
      x01 => x01, x02 => x02, x03 => x03, x04 => x04, x05 => x05, x06 => x06, x07 => x07, x08 => x08,
      x09 => x09, x10 => x10, x11 => x11, x12 => x12, x13 => x13, x14 => x14, x15 => x15, x16 => x16,
      
      x17 => x17, x18 => x18, x19 => x19, x20 => x20, x21 => x21, x22 => x22, x23 => x23, x24 => x24,
      x25 => x25, x26 => x26, x27 => x27, x28 => x28, x29 => x29, x30 => x30, x31 => x31, x32 => x32,
      
      x33 => x33, x34 => x34, x35 => x35, x36 => x36, x37 => x37, x38 => x38, x39 => x39, x40 => x40,
      x41 => x41, x42 => x42, x43 => x43, x44 => x44, x45 => x45, x46 => x46, x47 => x47, x48 => x48,
      
      x49 => x49, x50 => x50, x51 => x51, x52 => x52, x53 => x53, x54 => x54, x55 => x55, x56 => x56,
      x57 => x57, x58 => x58, x59 => x59, x60 => x60, x61 => x61, x62 => x62, x63 => x63, x64 => x64,

  
      offset_ent => offset_ent,

  
      xo01 => sg_xo01, xo02 => sg_xo02, xo03 => sg_xo03, xo04 => sg_xo04, xo05 => sg_xo05, xo06 => sg_xo06, xo07 => sg_xo07, xo08 => sg_xo08, 
      xo09 => sg_xo09, xo10 => sg_xo10, xo11 => sg_xo11, xo12 => sg_xo12, xo13 => sg_xo13, xo14 => sg_xo14, xo15 => sg_xo15, xo16 => sg_xo16,
      
      xo17 => sg_xo17, xo18 => sg_xo18, xo19 => sg_xo19, xo20 => sg_xo20, xo21 => sg_xo21, xo22 => sg_xo22, xo23 => sg_xo23, xo24 => sg_xo24,
      xo25 => sg_xo25, xo26 => sg_xo26, xo27 => sg_xo27, xo28 => sg_xo28, xo29 => sg_xo29, xo30 => sg_xo30, xo31 => sg_xo31, xo32 => sg_xo32,
      
      xo33 => sg_xo33, xo34 => sg_xo34, xo35 => sg_xo35, xo36 => sg_xo36, xo37 => sg_xo37, xo38 => sg_xo38, xo39 => sg_xo39, xo40 => sg_xo40,
      xo41 => sg_xo41, xo42 => sg_xo42, xo43 => sg_xo43, xo44 => sg_xo44, xo45 => sg_xo45, xo46 => sg_xo46, xo47 => sg_xo47, xo48 => sg_xo48,
      
      xo49 => sg_xo49, xo50 => sg_xo50, xo51 => sg_xo51, xo52 => sg_xo52, xo53 => sg_xo53, xo54 => sg_xo54, xo55 => sg_xo55, xo56 => sg_xo56,
      xo57 => sg_xo57, xo58 => sg_xo58, xo59 => sg_xo59, xo60 => sg_xo60, xo61 => sg_xo61, xo62 => sg_xo62, xo63 => sg_xo63, xo64 => sg_xo64

    );


  CC1: dense_core1
    port map(
      clk => clk,
    
    
      xo01 => sg_xo01, xo02 => sg_xo02, xo03 => sg_xo03, xo04 => sg_xo04, xo05 => sg_xo05, xo06 => sg_xo06, xo07 => sg_xo07, xo08 => sg_xo08, 
      xo09 => sg_xo09, xo10 => sg_xo10, xo11 => sg_xo11, xo12 => sg_xo12, xo13 => sg_xo13, xo14 => sg_xo14, xo15 => sg_xo15, xo16 => sg_xo16,
      xo17 => sg_xo17, xo18 => sg_xo18, xo19 => sg_xo19, xo20 => sg_xo20, xo21 => sg_xo21, xo22 => sg_xo22, xo23 => sg_xo23, xo24 => sg_xo24,
      xo25 => sg_xo25, xo26 => sg_xo26, xo27 => sg_xo27, xo28 => sg_xo28, xo29 => sg_xo29, xo30 => sg_xo30, xo31 => sg_xo31, xo32 => sg_xo32,
      xo33 => sg_xo33, xo34 => sg_xo34, xo35 => sg_xo35, xo36 => sg_xo36, xo37 => sg_xo37, xo38 => sg_xo38, xo39 => sg_xo39, xo40 => sg_xo40,
      xo41 => sg_xo41, xo42 => sg_xo42, xo43 => sg_xo43, xo44 => sg_xo44, xo45 => sg_xo45, xo46 => sg_xo46, xo47 => sg_xo47, xo48 => sg_xo48,
      xo49 => sg_xo49, xo50 => sg_xo50, xo51 => sg_xo51, xo52 => sg_xo52, xo53 => sg_xo53, xo54 => sg_xo54, xo55 => sg_xo55, xo56 => sg_xo56,
      xo57 => sg_xo57, xo58 => sg_xo58, xo59 => sg_xo59, xo60 => sg_xo60, xo61 => sg_xo61, xo62 => sg_xo62, xo63 => sg_xo63, xo64 => sg_xo64,

    
      w01 => w01, w02 => w02, w03 => w03, w04 => w04, w05 => w05, w06 => w06, w07 => w07, w08 => w08,
      w09 => w09, w10 => w10, w11 => w11, w12 => w12, w13 => w13, w14 => w14, w15 => w15, w16 => w16,
      w17 => w17, w18 => w18, w19 => w19, w20 => w20, w21 => w21, w22 => w22, w23 => w23, w24 => w24,
      w25 => w25, w26 => w26, w27 => w27, w28 => w28, w29 => w29, w30 => w30, w31 => w31, w32 => w32,
      w33 => w33, w34 => w34, w35 => w35, w36 => w36, w37 => w37, w38 => w38, w39 => w39, w40 => w40,
      w41 => w41, w42 => w42, w43 => w43, w44 => w44, w45 => w45, w46 => w46, w47 => w47, w48 => w48,
      w49 => w49, w50 => w50, w51 => w51, w52 => w52, w53 => w53, w54 => w54, w55 => w55, w56 => w56,
      w57 => w57, w58 => w58, w59 => w59, w60 => w60, w61 => w61, w62 => w62, w63 => w63, w64 => w64,

    
      xow01 => sg_xow01, xow02 => sg_xow02, xow03 => sg_xow03, xow04 => sg_xow04, xow05 => sg_xow05, xow06 => sg_xow06, xow07 => sg_xow07, xow08 => sg_xow08,
      xow09 => sg_xow09, xow10 => sg_xow10, xow11 => sg_xow11, xow12 => sg_xow12, xow13 => sg_xow13, xow14 => sg_xow14, xow15 => sg_xow15, xow16 => sg_xow16,
      xow17 => sg_xow17, xow18 => sg_xow18, xow19 => sg_xow19, xow20 => sg_xow20, xow21 => sg_xow21, xow22 => sg_xow22, xow23 => sg_xow23, xow24 => sg_xow24,
      xow25 => sg_xow25, xow26 => sg_xow26, xow27 => sg_xow27, xow28 => sg_xow28, xow29 => sg_xow29, xow30 => sg_xow30, xow31 => sg_xow31, xow32 => sg_xow32,
      xow33 => sg_xow33, xow34 => sg_xow34, xow35 => sg_xow35, xow36 => sg_xow36, xow37 => sg_xow37, xow38 => sg_xow38, xow39 => sg_xow39, xow40 => sg_xow40,
      xow41 => sg_xow41, xow42 => sg_xow42, xow43 => sg_xow43, xow44 => sg_xow44, xow45 => sg_xow45, xow46 => sg_xow46, xow47 => sg_xow47, xow48 => sg_xow48,
      xow49 => sg_xow49, xow50 => sg_xow50, xow51 => sg_xow51, xow52 => sg_xow52, xow53 => sg_xow53, xow54 => sg_xow54, xow55 => sg_xow55, xow56 => sg_xow56,
      xow57 => sg_xow57, xow58 => sg_xow58, xow59 => sg_xow59, xow60 => sg_xow60, xow61 => sg_xow61, xow62 => sg_xow62, xow63 => sg_xow63, xow64 => sg_xow64      
    );

  ds_out <= sg_xow01 + sg_xow02 + sg_xow03 + sg_xow04 + sg_xow05 + sg_xow06 + sg_xow07 + sg_xow08 +
            sg_xow09 + sg_xow10 + sg_xow11 + sg_xow12 + sg_xow13 + sg_xow14 + sg_xow15 + sg_xow16 +
            sg_xow17 + sg_xow18 + sg_xow19 + sg_xow20 + sg_xow21 + sg_xow22 + sg_xow23 + sg_xow24 +
            sg_xow25 + sg_xow26 + sg_xow27 + sg_xow28 + sg_xow29 + sg_xow30 + sg_xow31 + sg_xow32 +
            sg_xow33 + sg_xow34 + sg_xow35 + sg_xow36 + sg_xow37 + sg_xow38 + sg_xow39 + sg_xow40 +
            sg_xow41 + sg_xow42 + sg_xow43 + sg_xow44 + sg_xow45 + sg_xow46 + sg_xow47 + sg_xow48 +
            sg_xow49 + sg_xow50 + sg_xow51 + sg_xow52 + sg_xow53 + sg_xow54 + sg_xow55 + sg_xow56 +
            sg_xow57 + sg_xow58 + sg_xow59 + sg_xow60 + sg_xow61 + sg_xow62 + sg_xow63 + sg_xow64 + 
            ds_in;


end Behavioral;