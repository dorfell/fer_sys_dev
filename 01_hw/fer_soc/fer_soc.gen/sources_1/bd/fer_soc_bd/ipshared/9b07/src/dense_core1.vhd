---------------------------------------------------------------------
----                                                             ----
---- file:    conv_core1.vhd                                     ----
---- brief:   Noyau 1 pour la dense                              ----
---- details: Module pour implementer (x + offset_ent)*w         ----
----          Les entrée sont en int8 et int16,  et les          ----
----          résultats en (int32).                              ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/11/15                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity dense_core1 is
  port( 
    clk        :  in std_logic;

    -- Entrée  (x + offset_ent)
    xo01, xo02, xo03, xo04, xo05, xo06, xo07, xo08, xo09, xo10, xo11, xo12, xo13, xo14, xo15, xo16 :  in signed(15 downto 0);
    xo17, xo18, xo19, xo20, xo21, xo22, xo23, xo24, xo25, xo26, xo27, xo28, xo29, xo30, xo31, xo32 :  in signed(15 downto 0);
    xo33, xo34, xo35, xo36, xo37, xo38, xo39, xo40, xo41, xo42, xo43, xo44, xo45, xo46, xo47, xo48 :  in signed(15 downto 0);
    xo49, xo50, xo51, xo52, xo53, xo54, xo55, xo56, xo57, xo58, xo59, xo60, xo61, xo62, xo63, xo64 :  in signed(15 downto 0); 

    -- Filtre, poids w
    w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, w11, w12, w13, w14, w15, w16 :  in signed( 7 downto 0);
    w17, w18, w19, w20, w21, w22, w23, w24, w25, w26, w27, w28, w29, w30, w31, w32 :  in signed( 7 downto 0);
    w33, w34, w35, w36, w37, w38, w39, w40, w41, w42, w43, w44, w45, w46, w47, w48 :  in signed( 7 downto 0);
    w49, w50, w51, w52, w53, w54, w55, w56, w57, w58, w59, w60, w61, w62, w63, w64 :  in signed( 7 downto 0);

    -- Sortie (x + offset_ent)*w
    xow01, xow02, xow03, xow04, xow05, xow06, xow07, xow08, xow09, xow10, xow11, xow12, xow13, xow14, xow15, xow16 : out signed(31 downto 0);
    xow17, xow18, xow19, xow20, xow21, xow22, xow23, xow24, xow25, xow26, xow27, xow28, xow29, xow30, xow31, xow32 : out signed(31 downto 0);
    xow33, xow34, xow35, xow36, xow37, xow38, xow39, xow40, xow41, xow42, xow43, xow44, xow45, xow46, xow47, xow48 : out signed(31 downto 0);
    xow49, xow50, xow51, xow52, xow53, xow54, xow55, xow56, xow57, xow58, xow59, xow60, xow61, xow62, xow63, xow64 : out signed(31 downto 0) ); 
end dense_core1;


architecture Behavioral of dense_core1 is

  -- signaux pour les registres de pipeline. 3 registers de pipeline p0, p1, p2.
  signal sg_xo01_p0, sg_xo01_p1, sg_xo01_p2 : signed(15 downto 0);
  signal sg_xo02_p0, sg_xo02_p1, sg_xo02_p2 : signed(15 downto 0); 
  signal sg_xo03_p0, sg_xo03_p1, sg_xo03_p2 : signed(15 downto 0); 
  signal sg_xo04_p0, sg_xo04_p1, sg_xo04_p2 : signed(15 downto 0); 
  signal sg_xo05_p0, sg_xo05_p1, sg_xo05_p2 : signed(15 downto 0); 
  signal sg_xo06_p0, sg_xo06_p1, sg_xo06_p2 : signed(15 downto 0); 
  signal sg_xo07_p0, sg_xo07_p1, sg_xo07_p2 : signed(15 downto 0); 
  signal sg_xo08_p0, sg_xo08_p1, sg_xo08_p2 : signed(15 downto 0); 
  signal sg_xo09_p0, sg_xo09_p1, sg_xo09_p2 : signed(15 downto 0); 
  signal sg_xo10_p0, sg_xo10_p1, sg_xo10_p2 : signed(15 downto 0); 
  signal sg_xo11_p0, sg_xo11_p1, sg_xo11_p2 : signed(15 downto 0); 
  signal sg_xo12_p0, sg_xo12_p1, sg_xo12_p2 : signed(15 downto 0); 
  signal sg_xo13_p0, sg_xo13_p1, sg_xo13_p2 : signed(15 downto 0); 
  signal sg_xo14_p0, sg_xo14_p1, sg_xo14_p2 : signed(15 downto 0); 
  signal sg_xo15_p0, sg_xo15_p1, sg_xo15_p2 : signed(15 downto 0); 
  signal sg_xo16_p0, sg_xo16_p1, sg_xo16_p2 : signed(15 downto 0); 
  signal sg_xo17_p0, sg_xo17_p1, sg_xo17_p2 : signed(15 downto 0); 
  signal sg_xo18_p0, sg_xo18_p1, sg_xo18_p2 : signed(15 downto 0); 
  signal sg_xo19_p0, sg_xo19_p1, sg_xo19_p2 : signed(15 downto 0); 
  signal sg_xo20_p0, sg_xo20_p1, sg_xo20_p2 : signed(15 downto 0); 
  signal sg_xo21_p0, sg_xo21_p1, sg_xo21_p2 : signed(15 downto 0); 
  signal sg_xo22_p0, sg_xo22_p1, sg_xo22_p2 : signed(15 downto 0); 
  signal sg_xo23_p0, sg_xo23_p1, sg_xo23_p2 : signed(15 downto 0); 
  signal sg_xo24_p0, sg_xo24_p1, sg_xo24_p2 : signed(15 downto 0); 
  signal sg_xo25_p0, sg_xo25_p1, sg_xo25_p2 : signed(15 downto 0); 
  signal sg_xo26_p0, sg_xo26_p1, sg_xo26_p2 : signed(15 downto 0);
  signal sg_xo27_p0, sg_xo27_p1, sg_xo27_p2 : signed(15 downto 0);
  signal sg_xo28_p0, sg_xo28_p1, sg_xo28_p2 : signed(15 downto 0);
  signal sg_xo29_p0, sg_xo29_p1, sg_xo29_p2 : signed(15 downto 0);
  signal sg_xo30_p0, sg_xo30_p1, sg_xo30_p2 : signed(15 downto 0);
  signal sg_xo31_p0, sg_xo31_p1, sg_xo31_p2 : signed(15 downto 0);
  signal sg_xo32_p0, sg_xo32_p1, sg_xo32_p2 : signed(15 downto 0);
  signal sg_xo33_p0, sg_xo33_p1, sg_xo33_p2 : signed(15 downto 0);
  signal sg_xo34_p0, sg_xo34_p1, sg_xo34_p2 : signed(15 downto 0); 
  signal sg_xo35_p0, sg_xo35_p1, sg_xo35_p2 : signed(15 downto 0); 
  signal sg_xo36_p0, sg_xo36_p1, sg_xo36_p2 : signed(15 downto 0); 
  signal sg_xo37_p0, sg_xo37_p1, sg_xo37_p2 : signed(15 downto 0); 
  signal sg_xo38_p0, sg_xo38_p1, sg_xo38_p2 : signed(15 downto 0); 
  signal sg_xo39_p0, sg_xo39_p1, sg_xo39_p2 : signed(15 downto 0); 
  signal sg_xo40_p0, sg_xo40_p1, sg_xo40_p2 : signed(15 downto 0); 
  signal sg_xo41_p0, sg_xo41_p1, sg_xo41_p2 : signed(15 downto 0); 
  signal sg_xo42_p0, sg_xo42_p1, sg_xo42_p2 : signed(15 downto 0); 
  signal sg_xo43_p0, sg_xo43_p1, sg_xo43_p2 : signed(15 downto 0); 
  signal sg_xo44_p0, sg_xo44_p1, sg_xo44_p2 : signed(15 downto 0); 
  signal sg_xo45_p0, sg_xo45_p1, sg_xo45_p2 : signed(15 downto 0); 
  signal sg_xo46_p0, sg_xo46_p1, sg_xo46_p2 : signed(15 downto 0); 
  signal sg_xo47_p0, sg_xo47_p1, sg_xo47_p2 : signed(15 downto 0); 
  signal sg_xo48_p0, sg_xo48_p1, sg_xo48_p2 : signed(15 downto 0); 
  signal sg_xo49_p0, sg_xo49_p1, sg_xo49_p2 : signed(15 downto 0); 
  signal sg_xo50_p0, sg_xo50_p1, sg_xo50_p2 : signed(15 downto 0); 
  signal sg_xo51_p0, sg_xo51_p1, sg_xo51_p2 : signed(15 downto 0); 
  signal sg_xo52_p0, sg_xo52_p1, sg_xo52_p2 : signed(15 downto 0); 
  signal sg_xo53_p0, sg_xo53_p1, sg_xo53_p2 : signed(15 downto 0); 
  signal sg_xo54_p0, sg_xo54_p1, sg_xo54_p2 : signed(15 downto 0); 
  signal sg_xo55_p0, sg_xo55_p1, sg_xo55_p2 : signed(15 downto 0); 
  signal sg_xo56_p0, sg_xo56_p1, sg_xo56_p2 : signed(15 downto 0); 
  signal sg_xo57_p0, sg_xo57_p1, sg_xo57_p2 : signed(15 downto 0); 
  signal sg_xo58_p0, sg_xo58_p1, sg_xo58_p2 : signed(15 downto 0);
  signal sg_xo59_p0, sg_xo59_p1, sg_xo59_p2 : signed(15 downto 0);
  signal sg_xo60_p0, sg_xo60_p1, sg_xo60_p2 : signed(15 downto 0);
  signal sg_xo61_p0, sg_xo61_p1, sg_xo61_p2 : signed(15 downto 0);
  signal sg_xo62_p0, sg_xo62_p1, sg_xo62_p2 : signed(15 downto 0);
  signal sg_xo63_p0, sg_xo63_p1, sg_xo63_p2 : signed(15 downto 0);
  signal sg_xo64_p0, sg_xo64_p1, sg_xo64_p2 : signed(15 downto 0);
  

  signal sg_w01_p0, sg_w01_p1, sg_w01_p2 : signed(15 downto 0);
  signal sg_w02_p0, sg_w02_p1, sg_w02_p2 : signed(15 downto 0); 
  signal sg_w03_p0, sg_w03_p1, sg_w03_p2 : signed(15 downto 0); 
  signal sg_w04_p0, sg_w04_p1, sg_w04_p2 : signed(15 downto 0); 
  signal sg_w05_p0, sg_w05_p1, sg_w05_p2 : signed(15 downto 0); 
  signal sg_w06_p0, sg_w06_p1, sg_w06_p2 : signed(15 downto 0); 
  signal sg_w07_p0, sg_w07_p1, sg_w07_p2 : signed(15 downto 0); 
  signal sg_w08_p0, sg_w08_p1, sg_w08_p2 : signed(15 downto 0); 
  signal sg_w09_p0, sg_w09_p1, sg_w09_p2 : signed(15 downto 0); 
  signal sg_w10_p0, sg_w10_p1, sg_w10_p2 : signed(15 downto 0); 
  signal sg_w11_p0, sg_w11_p1, sg_w11_p2 : signed(15 downto 0); 
  signal sg_w12_p0, sg_w12_p1, sg_w12_p2 : signed(15 downto 0); 
  signal sg_w13_p0, sg_w13_p1, sg_w13_p2 : signed(15 downto 0); 
  signal sg_w14_p0, sg_w14_p1, sg_w14_p2 : signed(15 downto 0); 
  signal sg_w15_p0, sg_w15_p1, sg_w15_p2 : signed(15 downto 0); 
  signal sg_w16_p0, sg_w16_p1, sg_w16_p2 : signed(15 downto 0); 
  signal sg_w17_p0, sg_w17_p1, sg_w17_p2 : signed(15 downto 0); 
  signal sg_w18_p0, sg_w18_p1, sg_w18_p2 : signed(15 downto 0); 
  signal sg_w19_p0, sg_w19_p1, sg_w19_p2 : signed(15 downto 0); 
  signal sg_w20_p0, sg_w20_p1, sg_w20_p2 : signed(15 downto 0); 
  signal sg_w21_p0, sg_w21_p1, sg_w21_p2 : signed(15 downto 0); 
  signal sg_w22_p0, sg_w22_p1, sg_w22_p2 : signed(15 downto 0); 
  signal sg_w23_p0, sg_w23_p1, sg_w23_p2 : signed(15 downto 0); 
  signal sg_w24_p0, sg_w24_p1, sg_w24_p2 : signed(15 downto 0); 
  signal sg_w25_p0, sg_w25_p1, sg_w25_p2 : signed(15 downto 0); 
  signal sg_w26_p0, sg_w26_p1, sg_w26_p2 : signed(15 downto 0);
  signal sg_w27_p0, sg_w27_p1, sg_w27_p2 : signed(15 downto 0); 
  signal sg_w28_p0, sg_w28_p1, sg_w28_p2 : signed(15 downto 0); 
  signal sg_w29_p0, sg_w29_p1, sg_w29_p2 : signed(15 downto 0); 
  signal sg_w30_p0, sg_w30_p1, sg_w30_p2 : signed(15 downto 0); 
  signal sg_w31_p0, sg_w31_p1, sg_w31_p2 : signed(15 downto 0); 
  signal sg_w32_p0, sg_w32_p1, sg_w32_p2 : signed(15 downto 0); 
  signal sg_w33_p0, sg_w33_p1, sg_w33_p2 : signed(15 downto 0); 
  signal sg_w34_p0, sg_w34_p1, sg_w34_p2 : signed(15 downto 0); 
  signal sg_w35_p0, sg_w35_p1, sg_w35_p2 : signed(15 downto 0); 
  signal sg_w36_p0, sg_w36_p1, sg_w36_p2 : signed(15 downto 0); 
  signal sg_w37_p0, sg_w37_p1, sg_w37_p2 : signed(15 downto 0); 
  signal sg_w38_p0, sg_w38_p1, sg_w38_p2 : signed(15 downto 0); 
  signal sg_w39_p0, sg_w39_p1, sg_w39_p2 : signed(15 downto 0); 
  signal sg_w40_p0, sg_w40_p1, sg_w40_p2 : signed(15 downto 0); 
  signal sg_w41_p0, sg_w41_p1, sg_w41_p2 : signed(15 downto 0); 
  signal sg_w42_p0, sg_w42_p1, sg_w42_p2 : signed(15 downto 0); 
  signal sg_w43_p0, sg_w43_p1, sg_w43_p2 : signed(15 downto 0); 
  signal sg_w44_p0, sg_w44_p1, sg_w44_p2 : signed(15 downto 0); 
  signal sg_w45_p0, sg_w45_p1, sg_w45_p2 : signed(15 downto 0); 
  signal sg_w46_p0, sg_w46_p1, sg_w46_p2 : signed(15 downto 0); 
  signal sg_w47_p0, sg_w47_p1, sg_w47_p2 : signed(15 downto 0); 
  signal sg_w48_p0, sg_w48_p1, sg_w48_p2 : signed(15 downto 0); 
  signal sg_w49_p0, sg_w49_p1, sg_w49_p2 : signed(15 downto 0); 
  signal sg_w50_p0, sg_w50_p1, sg_w50_p2 : signed(15 downto 0); 
  signal sg_w51_p0, sg_w51_p1, sg_w51_p2 : signed(15 downto 0);
  signal sg_w52_p0, sg_w52_p1, sg_w52_p2 : signed(15 downto 0);
  signal sg_w53_p0, sg_w53_p1, sg_w53_p2 : signed(15 downto 0);
  signal sg_w54_p0, sg_w54_p1, sg_w54_p2 : signed(15 downto 0);
  signal sg_w55_p0, sg_w55_p1, sg_w55_p2 : signed(15 downto 0);
  signal sg_w56_p0, sg_w56_p1, sg_w56_p2 : signed(15 downto 0);
  signal sg_w57_p0, sg_w57_p1, sg_w57_p2 : signed(15 downto 0);
  signal sg_w58_p0, sg_w58_p1, sg_w58_p2 : signed(15 downto 0);
  signal sg_w59_p0, sg_w59_p1, sg_w59_p2 : signed(15 downto 0);
  signal sg_w60_p0, sg_w60_p1, sg_w60_p2 : signed(15 downto 0);
  signal sg_w61_p0, sg_w61_p1, sg_w61_p2 : signed(15 downto 0);
  signal sg_w62_p0, sg_w62_p1, sg_w62_p2 : signed(15 downto 0);
  signal sg_w63_p0, sg_w63_p1, sg_w63_p2 : signed(15 downto 0);
  signal sg_w64_p0, sg_w64_p1, sg_w64_p2 : signed(15 downto 0);
            

  signal sg_xow01_p0, sg_xow01_p1, sg_xow01_p2 : signed(31 downto 0);
  signal sg_xow02_p0, sg_xow02_p1, sg_xow02_p2 : signed(31 downto 0);
  signal sg_xow03_p0, sg_xow03_p1, sg_xow03_p2 : signed(31 downto 0);
  signal sg_xow04_p0, sg_xow04_p1, sg_xow04_p2 : signed(31 downto 0);
  signal sg_xow05_p0, sg_xow05_p1, sg_xow05_p2 : signed(31 downto 0);
  signal sg_xow06_p0, sg_xow06_p1, sg_xow06_p2 : signed(31 downto 0);
  signal sg_xow07_p0, sg_xow07_p1, sg_xow07_p2 : signed(31 downto 0);
  signal sg_xow08_p0, sg_xow08_p1, sg_xow08_p2 : signed(31 downto 0);
  signal sg_xow09_p0, sg_xow09_p1, sg_xow09_p2 : signed(31 downto 0);
  signal sg_xow10_p0, sg_xow10_p1, sg_xow10_p2 : signed(31 downto 0);
  signal sg_xow11_p0, sg_xow11_p1, sg_xow11_p2 : signed(31 downto 0);
  signal sg_xow12_p0, sg_xow12_p1, sg_xow12_p2 : signed(31 downto 0);
  signal sg_xow13_p0, sg_xow13_p1, sg_xow13_p2 : signed(31 downto 0);
  signal sg_xow14_p0, sg_xow14_p1, sg_xow14_p2 : signed(31 downto 0);
  signal sg_xow15_p0, sg_xow15_p1, sg_xow15_p2 : signed(31 downto 0);
  signal sg_xow16_p0, sg_xow16_p1, sg_xow16_p2 : signed(31 downto 0);
  signal sg_xow17_p0, sg_xow17_p1, sg_xow17_p2 : signed(31 downto 0);
  signal sg_xow18_p0, sg_xow18_p1, sg_xow18_p2 : signed(31 downto 0);
  signal sg_xow19_p0, sg_xow19_p1, sg_xow19_p2 : signed(31 downto 0);
  signal sg_xow20_p0, sg_xow20_p1, sg_xow20_p2 : signed(31 downto 0);
  signal sg_xow21_p0, sg_xow21_p1, sg_xow21_p2 : signed(31 downto 0);
  signal sg_xow22_p0, sg_xow22_p1, sg_xow22_p2 : signed(31 downto 0);
  signal sg_xow23_p0, sg_xow23_p1, sg_xow23_p2 : signed(31 downto 0);
  signal sg_xow24_p0, sg_xow24_p1, sg_xow24_p2 : signed(31 downto 0);
  signal sg_xow25_p0, sg_xow25_p1, sg_xow25_p2 : signed(31 downto 0);
  signal sg_xow26_p0, sg_xow26_p1, sg_xow26_p2 : signed(31 downto 0);
  signal sg_xow27_p0, sg_xow27_p1, sg_xow27_p2 : signed(31 downto 0);  
  signal sg_xow28_p0, sg_xow28_p1, sg_xow28_p2 : signed(31 downto 0);  
  signal sg_xow29_p0, sg_xow29_p1, sg_xow29_p2 : signed(31 downto 0);  
  signal sg_xow30_p0, sg_xow30_p1, sg_xow30_p2 : signed(31 downto 0);  
  signal sg_xow31_p0, sg_xow31_p1, sg_xow31_p2 : signed(31 downto 0);  
  signal sg_xow32_p0, sg_xow32_p1, sg_xow32_p2 : signed(31 downto 0);  
  signal sg_xow33_p0, sg_xow33_p1, sg_xow33_p2 : signed(31 downto 0);  
  signal sg_xow34_p0, sg_xow34_p1, sg_xow34_p2 : signed(31 downto 0);
  signal sg_xow35_p0, sg_xow35_p1, sg_xow35_p2 : signed(31 downto 0);
  signal sg_xow36_p0, sg_xow36_p1, sg_xow36_p2 : signed(31 downto 0);
  signal sg_xow37_p0, sg_xow37_p1, sg_xow37_p2 : signed(31 downto 0);
  signal sg_xow38_p0, sg_xow38_p1, sg_xow38_p2 : signed(31 downto 0);      
  signal sg_xow39_p0, sg_xow39_p1, sg_xow39_p2 : signed(31 downto 0);  
  signal sg_xow40_p0, sg_xow40_p1, sg_xow40_p2 : signed(31 downto 0);  
  signal sg_xow41_p0, sg_xow41_p1, sg_xow41_p2 : signed(31 downto 0);  
  signal sg_xow42_p0, sg_xow42_p1, sg_xow42_p2 : signed(31 downto 0);  
  signal sg_xow43_p0, sg_xow43_p1, sg_xow43_p2 : signed(31 downto 0);  
  signal sg_xow44_p0, sg_xow44_p1, sg_xow44_p2 : signed(31 downto 0);  
  signal sg_xow45_p0, sg_xow45_p1, sg_xow45_p2 : signed(31 downto 0);  
  signal sg_xow46_p0, sg_xow46_p1, sg_xow46_p2 : signed(31 downto 0);  
  signal sg_xow47_p0, sg_xow47_p1, sg_xow47_p2 : signed(31 downto 0);  
  signal sg_xow48_p0, sg_xow48_p1, sg_xow48_p2 : signed(31 downto 0);  
  signal sg_xow49_p0, sg_xow49_p1, sg_xow49_p2 : signed(31 downto 0);  
  signal sg_xow50_p0, sg_xow50_p1, sg_xow50_p2 : signed(31 downto 0);  
  signal sg_xow51_p0, sg_xow51_p1, sg_xow51_p2 : signed(31 downto 0);  
  signal sg_xow52_p0, sg_xow52_p1, sg_xow52_p2 : signed(31 downto 0);  
  signal sg_xow53_p0, sg_xow53_p1, sg_xow53_p2 : signed(31 downto 0);  
  signal sg_xow54_p0, sg_xow54_p1, sg_xow54_p2 : signed(31 downto 0);  
  signal sg_xow55_p0, sg_xow55_p1, sg_xow55_p2 : signed(31 downto 0);  
  signal sg_xow56_p0, sg_xow56_p1, sg_xow56_p2 : signed(31 downto 0);  
  signal sg_xow57_p0, sg_xow57_p1, sg_xow57_p2 : signed(31 downto 0);  
  signal sg_xow58_p0, sg_xow58_p1, sg_xow58_p2 : signed(31 downto 0);  
  signal sg_xow59_p0, sg_xow59_p1, sg_xow59_p2 : signed(31 downto 0);  
  signal sg_xow60_p0, sg_xow60_p1, sg_xow60_p2 : signed(31 downto 0);  
  signal sg_xow61_p0, sg_xow61_p1, sg_xow61_p2 : signed(31 downto 0);  
  signal sg_xow62_p0, sg_xow62_p1, sg_xow62_p2 : signed(31 downto 0);
  signal sg_xow63_p0, sg_xow63_p1, sg_xow63_p2 : signed(31 downto 0);
  signal sg_xow64_p0, sg_xow64_p1, sg_xow64_p2 : signed(31 downto 0);      
    
  
  attribute USE_DSP : string;
  attribute USE_DSP of sg_xow01_p0, sg_xow02_p0, sg_xow03_p0, sg_xow04_p0, sg_xow05_p0, sg_xow06_p0, sg_xow07_p0, sg_xow08_p0 : signal is "YES";
  attribute USE_DSP of sg_xow09_p0, sg_xow10_p0, sg_xow11_p0, sg_xow12_p0, sg_xow13_p0, sg_xow14_p0, sg_xow15_p0, sg_xow16_p0 : signal is "YES";
  attribute USE_DSP of sg_xow17_p0, sg_xow18_p0, sg_xow19_p0, sg_xow20_p0, sg_xow21_p0, sg_xow22_p0, sg_xow23_p0, sg_xow24_p0 : signal is "YES";
  attribute USE_DSP of sg_xow25_p0, sg_xow26_p0, sg_xow27_p0, sg_xow28_p0, sg_xow29_p0, sg_xow30_p0, sg_xow31_p0, sg_xow32_p0 : signal is "YES";
  
  attribute USE_DSP of sg_xow33_p0, sg_xow34_p0, sg_xow35_p0, sg_xow36_p0, sg_xow37_p0, sg_xow38_p0, sg_xow39_p0, sg_xow40_p0 : signal is "YES";
  attribute USE_DSP of sg_xow41_p0, sg_xow42_p0, sg_xow43_p0, sg_xow44_p0, sg_xow45_p0, sg_xow46_p0, sg_xow47_p0, sg_xow48_p0 : signal is "YES";
  attribute USE_DSP of sg_xow49_p0, sg_xow50_p0, sg_xow51_p0, sg_xow52_p0, sg_xow53_p0, sg_xow54_p0, sg_xow55_p0, sg_xow56_p0 : signal is "YES";          
  attribute USE_DSP of sg_xow57_p0, sg_xow58_p0, sg_xow59_p0, sg_xow60_p0, sg_xow61_p0, sg_xow62_p0, sg_xow63_p0, sg_xow64_p0 : signal is "YES";




begin


  -- Computer (X + offset_ent)*W --
  ---------------------------------
  process(clk)
    begin
      if (clk'event and clk = '1') then

        -- wx01
        sg_xo01_p0 <= xo01;
        sg_xo01_p1 <= sg_xo01_p0;
        sg_xo01_p2 <= sg_xo01_p1;

        sg_w01_p0  <= resize(w01, 16); 
        sg_w01_p1  <= sg_w01_p0;
        sg_w01_p2  <= sg_w01_p1;

        sg_xow01_p0 <= sg_xo01_p2 * sg_w01_p2;
        sg_xow01_p1 <= sg_xow01_p0;
        sg_xow01_p2 <= sg_xow01_p1;
        

        -- wx02
        sg_xo02_p0 <= xo02;
        sg_xo02_p1 <= sg_xo02_p0;
        sg_xo02_p2 <= sg_xo02_p1;

        sg_w02_p0  <= resize(w02, 16); 
        sg_w02_p1  <= sg_w02_p0;
        sg_w02_p2  <= sg_w02_p1;

        sg_xow02_p0 <= sg_xo02_p2 * sg_w02_p2;
        sg_xow02_p1 <= sg_xow02_p0;
        sg_xow02_p2 <= sg_xow02_p1;


        -- wx03
        sg_xo03_p0 <= xo03;
        sg_xo03_p1 <= sg_xo03_p0;
        sg_xo03_p2 <= sg_xo03_p1;

        sg_w03_p0  <= resize(w03, 16); 
        sg_w03_p1  <= sg_w03_p0;
        sg_w03_p2  <= sg_w03_p1;

        sg_xow03_p0 <= sg_xo03_p2 * sg_w03_p2;
        sg_xow03_p1 <= sg_xow03_p0;
        sg_xow03_p2 <= sg_xow03_p1;
        

        -- wx04
        sg_xo04_p0 <= xo04;
        sg_xo04_p1 <= sg_xo04_p0;
        sg_xo04_p2 <= sg_xo04_p1;

        sg_w04_p0  <= resize(w04, 16); 
        sg_w04_p1  <= sg_w04_p0;
        sg_w04_p2  <= sg_w04_p1;

        sg_xow04_p0 <= sg_xo04_p2 * sg_w04_p2;
        sg_xow04_p1 <= sg_xow04_p0;
        sg_xow04_p2 <= sg_xow04_p1;
        

        -- wx05
        sg_xo05_p0 <= xo05;
        sg_xo05_p1 <= sg_xo05_p0;
        sg_xo05_p2 <= sg_xo05_p1;

        sg_w05_p0  <= resize(w05, 16); 
        sg_w05_p1  <= sg_w05_p0;
        sg_w05_p2  <= sg_w05_p1;

        sg_xow05_p0 <= sg_xo05_p2 * sg_w05_p2;
        sg_xow05_p1 <= sg_xow05_p0;
        sg_xow05_p2 <= sg_xow05_p1;
        

        -- wx06
        sg_xo06_p0 <= xo06;
        sg_xo06_p1 <= sg_xo06_p0;
        sg_xo06_p2 <= sg_xo06_p1;

        sg_w06_p0  <= resize(w06, 16); 
        sg_w06_p1  <= sg_w06_p0;
        sg_w06_p2  <= sg_w06_p1;

        sg_xow06_p0 <= sg_xo06_p2 * sg_w06_p2;
        sg_xow06_p1 <= sg_xow06_p0;
        sg_xow06_p2 <= sg_xow06_p1;
        

        -- wx07
        sg_xo07_p0 <= xo07;
        sg_xo07_p1 <= sg_xo07_p0;
        sg_xo07_p2 <= sg_xo07_p1;

        sg_w07_p0  <= resize(w07, 16); 
        sg_w07_p1  <= sg_w07_p0;
        sg_w07_p2  <= sg_w07_p1;

        sg_xow07_p0 <= sg_xo07_p2 * sg_w07_p2;
        sg_xow07_p1 <= sg_xow07_p0;
        sg_xow07_p2 <= sg_xow07_p1;
        

        -- wx08
        sg_xo08_p0 <= xo08;
        sg_xo08_p1 <= sg_xo08_p0;
        sg_xo08_p2 <= sg_xo08_p1;

        sg_w08_p0  <= resize(w08, 16); 
        sg_w08_p1  <= sg_w08_p0;
        sg_w08_p2  <= sg_w08_p1;

        sg_xow08_p0 <= sg_xo08_p2 * sg_w08_p2;
        sg_xow08_p1 <= sg_xow08_p0;
        sg_xow08_p2 <= sg_xow08_p1;
        

        -- wx09
        sg_xo09_p0 <= xo09;
        sg_xo09_p1 <= sg_xo09_p0;
        sg_xo09_p2 <= sg_xo09_p1;

        sg_w09_p0  <= resize(w09, 16); 
        sg_w09_p1  <= sg_w09_p0;
        sg_w09_p2  <= sg_w09_p1;

        sg_xow09_p0 <= sg_xo09_p2 * sg_w09_p2;
        sg_xow09_p1 <= sg_xow09_p0;
        sg_xow09_p2 <= sg_xow09_p1;
        

        -- wx10
        sg_xo10_p0 <= xo10;
        sg_xo10_p1 <= sg_xo10_p0;
        sg_xo10_p2 <= sg_xo10_p1;

        sg_w10_p0  <= resize(w10, 16);
        sg_w10_p1  <= sg_w10_p0;
        sg_w10_p2  <= sg_w10_p1;

        sg_xow10_p0 <= sg_xo10_p2 * sg_w10_p2;
        sg_xow10_p1 <= sg_xow10_p0;
        sg_xow10_p2 <= sg_xow10_p1;
        

        -- wx11
        sg_xo11_p0 <= xo11;
        sg_xo11_p1 <= sg_xo11_p0;
        sg_xo11_p2 <= sg_xo11_p1;

        sg_w11_p0  <= resize(w11, 16);
        sg_w11_p1  <= sg_w11_p0;
        sg_w11_p2  <= sg_w11_p1;

        sg_xow11_p0 <= sg_xo11_p2 * sg_w11_p2;
        sg_xow11_p1 <= sg_xow11_p0;
        sg_xow11_p2 <= sg_xow11_p1;
        

        -- wx12
        sg_xo12_p0 <= xo12;
        sg_xo12_p1 <= sg_xo12_p0;
        sg_xo12_p2 <= sg_xo12_p1;

        sg_w12_p0  <= resize(w12, 16); 
        sg_w12_p1  <= sg_w12_p0;
        sg_w12_p2  <= sg_w12_p1;

        sg_xow12_p0 <= sg_xo12_p2 * sg_w12_p2;
        sg_xow12_p1 <= sg_xow12_p0;
        sg_xow12_p2 <= sg_xow12_p1;


        -- wx13
        sg_xo13_p0 <= xo13;
        sg_xo13_p1 <= sg_xo13_p0;
        sg_xo13_p2 <= sg_xo13_p1;

        sg_w13_p0  <= resize(w13, 16); 
        sg_w13_p1  <= sg_w13_p0;
        sg_w13_p2  <= sg_w13_p1;

        sg_xow13_p0 <= sg_xo13_p2 * sg_w13_p2;
        sg_xow13_p1 <= sg_xow13_p0;
        sg_xow13_p2 <= sg_xow13_p1;
        

        -- wx14
        sg_xo14_p0 <= xo14;
        sg_xo14_p1 <= sg_xo14_p0;
        sg_xo14_p2 <= sg_xo14_p1;

        sg_w14_p0  <= resize(w14, 16); 
        sg_w14_p1  <= sg_w14_p0;
        sg_w14_p2  <= sg_w14_p1;

        sg_xow14_p0 <= sg_xo14_p2 * sg_w14_p2;
        sg_xow14_p1 <= sg_xow14_p0;
        sg_xow14_p2 <= sg_xow14_p1;
        

        -- wx15
        sg_xo15_p0 <= xo15;
        sg_xo15_p1 <= sg_xo15_p0;
        sg_xo15_p2 <= sg_xo15_p1;

        sg_w15_p0  <= resize(w15, 16); 
        sg_w15_p1  <= sg_w15_p0;
        sg_w15_p2  <= sg_w15_p1;

        sg_xow15_p0 <= sg_xo15_p2 * sg_w15_p2;
        sg_xow15_p1 <= sg_xow15_p0;
        sg_xow15_p2 <= sg_xow15_p1;
        

        -- wx16
        sg_xo16_p0 <= xo16;
        sg_xo16_p1 <= sg_xo16_p0;
        sg_xo16_p2 <= sg_xo16_p1;

        sg_w16_p0  <= resize(w16, 16); 
        sg_w16_p1  <= sg_w16_p0;
        sg_w16_p2  <= sg_w16_p1;

        sg_xow16_p0 <= sg_xo16_p2 * sg_w16_p2;
        sg_xow16_p1 <= sg_xow16_p0;
        sg_xow16_p2 <= sg_xow16_p1;


        -- wx17
        sg_xo17_p0 <= xo17;
        sg_xo17_p1 <= sg_xo17_p0;
        sg_xo17_p2 <= sg_xo17_p1;

        sg_w17_p0  <= resize(w17, 16); 
        sg_w17_p1  <= sg_w17_p0;
        sg_w17_p2  <= sg_w17_p1;

        sg_xow17_p0 <= sg_xo17_p2 * sg_w17_p2;
        sg_xow17_p1 <= sg_xow17_p0;
        sg_xow17_p2 <= sg_xow17_p1;


        -- wx18
        sg_xo18_p0 <= xo18;
        sg_xo18_p1 <= sg_xo18_p0;
        sg_xo18_p2 <= sg_xo18_p1;

        sg_w18_p0  <= resize(w18, 16); 
        sg_w18_p1  <= sg_w18_p0;
        sg_w18_p2  <= sg_w18_p1;

        sg_xow18_p0 <= sg_xo18_p2 *  sg_w18_p2;
        sg_xow18_p1 <= sg_xow18_p0;
        sg_xow18_p2 <= sg_xow18_p1;


        -- wx19
        sg_xo19_p0 <= xo19;
        sg_xo19_p1 <= sg_xo19_p0;
        sg_xo19_p2 <= sg_xo19_p1;

        sg_w19_p0  <= resize(w19, 16); 
        sg_w19_p1  <= sg_w19_p0;
        sg_w19_p2  <= sg_w19_p1;

        sg_xow19_p0 <= sg_xo19_p2 * sg_w19_p2;
        sg_xow19_p1 <= sg_xow19_p0;
        sg_xow19_p2 <= sg_xow19_p1;


        -- wx20
        sg_xo20_p0 <= xo20;
        sg_xo20_p1 <= sg_xo20_p0;
        sg_xo20_p2 <= sg_xo20_p1;

        sg_w20_p0  <= resize(w20, 16); 
        sg_w20_p1  <= sg_w20_p0;
        sg_w20_p2  <= sg_w20_p1;

        sg_xow20_p0 <= sg_xo20_p2 * sg_w20_p2;
        sg_xow20_p1 <= sg_xow20_p0;
        sg_xow20_p2 <= sg_xow20_p1;


        -- wx21
        sg_xo21_p0 <= xo21;
        sg_xo21_p1 <= sg_xo21_p0;
        sg_xo21_p2 <= sg_xo21_p1;

        sg_w21_p0  <= resize(w21, 16); 
        sg_w21_p1  <= sg_w21_p0;
        sg_w21_p2  <= sg_w21_p1;

        sg_xow21_p0 <= sg_xo21_p2 * sg_w21_p2;
        sg_xow21_p1 <= sg_xow21_p0;
        sg_xow21_p2 <= sg_xow21_p1;


        -- wx22
        sg_xo22_p0 <= xo22;
        sg_xo22_p1 <= sg_xo22_p0;
        sg_xo22_p2 <= sg_xo22_p1;

        sg_w22_p0  <= resize(w22, 16); 
        sg_w22_p1  <= sg_w22_p0;
        sg_w22_p2  <= sg_w22_p1;

        sg_xow22_p0 <= sg_xo22_p2 * sg_w22_p2;
        sg_xow22_p1 <= sg_xow22_p0;
        sg_xow22_p2 <= sg_xow22_p1;


        -- wx23
        sg_xo23_p0 <= xo23;
        sg_xo23_p1 <= sg_xo23_p0;
        sg_xo23_p2 <= sg_xo23_p1;

        sg_w23_p0  <= resize(w23, 16); 
        sg_w23_p1  <= sg_w23_p0;
        sg_w23_p2  <= sg_w23_p1;

        sg_xow23_p0 <= sg_xo23_p2 * sg_w23_p2;
        sg_xow23_p1 <= sg_xow23_p0;
        sg_xow23_p2 <= sg_xow23_p1;


        -- wx24
        sg_xo24_p0 <= xo24;
        sg_xo24_p1 <= sg_xo24_p0;
        sg_xo24_p2 <= sg_xo24_p1;

        sg_w24_p0  <= resize(w24, 16); 
        sg_w24_p1  <= sg_w24_p0;
        sg_w24_p2  <= sg_w24_p1;

        sg_xow24_p0 <= sg_xo24_p2 * sg_w24_p2;
        sg_xow24_p1 <= sg_xow24_p0;
        sg_xow24_p2 <= sg_xow24_p1;


        -- wx25
        sg_xo25_p0 <= xo25;
        sg_xo25_p1 <= sg_xo25_p0;
        sg_xo25_p2 <= sg_xo25_p1;

        sg_w25_p0  <= resize(w25, 16); 
        sg_w25_p1  <= sg_w25_p0;
        sg_w25_p2  <= sg_w25_p1;

        sg_xow25_p0 <= sg_xo25_p2 * sg_w25_p2;
        sg_xow25_p1 <= sg_xow25_p0;
        sg_xow25_p2 <= sg_xow25_p1;


        -- wx26
        sg_xo26_p0 <= xo26;
        sg_xo26_p1 <= sg_xo26_p0;
        sg_xo26_p2 <= sg_xo26_p1;

        sg_w26_p0  <= resize(w26, 16); 
        sg_w26_p1  <= sg_w26_p0;
        sg_w26_p2  <= sg_w26_p1;

        sg_xow26_p0 <= sg_xo26_p2 * sg_w26_p2;
        sg_xow26_p1 <= sg_xow26_p0;
        sg_xow26_p2 <= sg_xow26_p1;


        -- wx27
        sg_xo27_p0 <= xo27;
        sg_xo27_p1 <= sg_xo27_p0;
        sg_xo27_p2 <= sg_xo27_p1;

        sg_w27_p0  <= resize(w27, 16); 
        sg_w27_p1  <= sg_w27_p0;
        sg_w27_p2  <= sg_w27_p1;

        sg_xow27_p0 <= sg_xo27_p2 * sg_w27_p2;
        sg_xow27_p1 <= sg_xow27_p0;
        sg_xow27_p2 <= sg_xow27_p1;


        -- wx28
        sg_xo28_p0 <= xo28;
        sg_xo28_p1 <= sg_xo28_p0;
        sg_xo28_p2 <= sg_xo28_p1;

        sg_w28_p0  <= resize(w28, 16); 
        sg_w28_p1  <= sg_w28_p0;
        sg_w28_p2  <= sg_w28_p1;

        sg_xow28_p0 <= sg_xo28_p2 * sg_w28_p2;
        sg_xow28_p1 <= sg_xow28_p0;
        sg_xow28_p2 <= sg_xow28_p1;


        -- wx29
        sg_xo29_p0 <= xo29;
        sg_xo29_p1 <= sg_xo29_p0;
        sg_xo29_p2 <= sg_xo29_p1;

        sg_w29_p0  <= resize(w29, 16); 
        sg_w29_p1  <= sg_w29_p0;
        sg_w29_p2  <= sg_w29_p1;

        sg_xow29_p0 <= sg_xo29_p2 * sg_w29_p2;
        sg_xow29_p1 <= sg_xow29_p0;
        sg_xow29_p2 <= sg_xow29_p1;


        -- wx30
        sg_xo30_p0 <= xo30;
        sg_xo30_p1 <= sg_xo30_p0;
        sg_xo30_p2 <= sg_xo30_p1;

        sg_w30_p0  <= resize(w30, 16); 
        sg_w30_p1  <= sg_w30_p0;
        sg_w30_p2  <= sg_w30_p1;

        sg_xow30_p0 <= sg_xo30_p2 * sg_w30_p2;
        sg_xow30_p1 <= sg_xow30_p0;
        sg_xow30_p2 <= sg_xow30_p1;


        -- wx31
        sg_xo31_p0 <= xo31;
        sg_xo31_p1 <= sg_xo31_p0;
        sg_xo31_p2 <= sg_xo31_p1;

        sg_w31_p0  <= resize(w31, 16); 
        sg_w31_p1  <= sg_w31_p0;
        sg_w31_p2  <= sg_w31_p1;

        sg_xow31_p0 <= sg_xo31_p2 * sg_w31_p2;
        sg_xow31_p1 <= sg_xow31_p0;
        sg_xow31_p2 <= sg_xow31_p1;


        -- wx32
        sg_xo32_p0 <= xo32;
        sg_xo32_p1 <= sg_xo32_p0;
        sg_xo32_p2 <= sg_xo32_p1;

        sg_w32_p0  <= resize(w32, 16); 
        sg_w32_p1  <= sg_w32_p0;
        sg_w32_p2  <= sg_w32_p1;

        sg_xow32_p0 <= sg_xo32_p2 * sg_w32_p2;
        sg_xow32_p1 <= sg_xow32_p0;
        sg_xow32_p2 <= sg_xow32_p1;


        -- wx33
        sg_xo33_p0 <= xo33;
        sg_xo33_p1 <= sg_xo33_p0;
        sg_xo33_p2 <= sg_xo33_p1;

        sg_w33_p0  <= resize(w33, 16); 
        sg_w33_p1  <= sg_w33_p0;
        sg_w33_p2  <= sg_w33_p1;

        sg_xow33_p0 <= sg_xo33_p2 * sg_w33_p2;
        sg_xow33_p1 <= sg_xow33_p0;
        sg_xow33_p2 <= sg_xow33_p1;


        -- wx34
        sg_xo34_p0 <= xo34;
        sg_xo34_p1 <= sg_xo34_p0;
        sg_xo34_p2 <= sg_xo34_p1;

        sg_w34_p0  <= resize(w34, 16); 
        sg_w34_p1  <= sg_w34_p0;
        sg_w34_p2  <= sg_w34_p1;

        sg_xow34_p0 <= sg_xo34_p2 * sg_w34_p2;
        sg_xow34_p1 <= sg_xow34_p0;
        sg_xow34_p2 <= sg_xow34_p1;


        -- wx35
        sg_xo35_p0 <= xo35;
        sg_xo35_p1 <= sg_xo35_p0;
        sg_xo35_p2 <= sg_xo35_p1;

        sg_w35_p0  <= resize(w35, 16); 
        sg_w35_p1  <= sg_w35_p0;
        sg_w35_p2  <= sg_w35_p1;

        sg_xow35_p0 <= sg_xo35_p2 * sg_w35_p2;
        sg_xow35_p1 <= sg_xow35_p0;
        sg_xow35_p2 <= sg_xow35_p1;


        -- wx36
        sg_xo36_p0 <= xo36;
        sg_xo36_p1 <= sg_xo36_p0;
        sg_xo36_p2 <= sg_xo36_p1;

        sg_w36_p0  <= resize(w36, 16); 
        sg_w36_p1  <= sg_w36_p0;
        sg_w36_p2  <= sg_w36_p1;

        sg_xow36_p0 <= sg_xo36_p2 * sg_w36_p2;
        sg_xow36_p1 <= sg_xow36_p0;
        sg_xow36_p2 <= sg_xow36_p1;


        -- wx37
        sg_xo37_p0 <= xo37;
        sg_xo37_p1 <= sg_xo37_p0;
        sg_xo37_p2 <= sg_xo37_p1;

        sg_w37_p0  <= resize(w37, 16); 
        sg_w37_p1  <= sg_w37_p0;
        sg_w37_p2  <= sg_w37_p1;

        sg_xow37_p0 <= sg_xo37_p2 * sg_w37_p2;
        sg_xow37_p1 <= sg_xow37_p0;
        sg_xow37_p2 <= sg_xow37_p1;


        -- wx38
        sg_xo38_p0 <= xo38;
        sg_xo38_p1 <= sg_xo38_p0;
        sg_xo38_p2 <= sg_xo38_p1;

        sg_w38_p0  <= resize(w38, 16); 
        sg_w38_p1  <= sg_w38_p0;
        sg_w38_p2  <= sg_w38_p1;

        sg_xow38_p0 <= sg_xo38_p2 * sg_w38_p2;
        sg_xow38_p1 <= sg_xow38_p0;
        sg_xow38_p2 <= sg_xow38_p1;


        -- wx39
        sg_xo39_p0 <= xo39;
        sg_xo39_p1 <= sg_xo39_p0;
        sg_xo39_p2 <= sg_xo39_p1;

        sg_w39_p0  <= resize(w39, 16); 
        sg_w39_p1  <= sg_w39_p0;
        sg_w39_p2  <= sg_w39_p1;

        sg_xow39_p0 <= sg_xo39_p2 * sg_w39_p2;
        sg_xow39_p1 <= sg_xow39_p0;
        sg_xow39_p2 <= sg_xow39_p1;


        -- wx40
        sg_xo40_p0 <= xo40;
        sg_xo40_p1 <= sg_xo40_p0;
        sg_xo40_p2 <= sg_xo40_p1;

        sg_w40_p0  <= resize(w40, 16); 
        sg_w40_p1  <= sg_w40_p0;
        sg_w40_p2  <= sg_w40_p1;

        sg_xow40_p0 <= sg_xo40_p2 * sg_w40_p2;
        sg_xow40_p1 <= sg_xow40_p0;
        sg_xow40_p2 <= sg_xow40_p1;


        -- wx41
        sg_xo41_p0 <= xo41;
        sg_xo41_p1 <= sg_xo41_p0;
        sg_xo41_p2 <= sg_xo41_p1;

        sg_w41_p0  <= resize(w41, 16); 
        sg_w41_p1  <= sg_w41_p0;
        sg_w41_p2  <= sg_w41_p1;

        sg_xow41_p0 <= sg_xo41_p2 * sg_w41_p2;
        sg_xow41_p1 <= sg_xow41_p0;
        sg_xow41_p2 <= sg_xow41_p1;


        -- wx42
        sg_xo42_p0 <= xo42;
        sg_xo42_p1 <= sg_xo42_p0;
        sg_xo42_p2 <= sg_xo42_p1;

        sg_w42_p0  <= resize(w42, 16); 
        sg_w42_p1  <= sg_w42_p0;
        sg_w42_p2  <= sg_w42_p1;

        sg_xow42_p0 <= sg_xo42_p2 * sg_w42_p2;
        sg_xow42_p1 <= sg_xow42_p0;
        sg_xow42_p2 <= sg_xow42_p1;


        -- wx43
        sg_xo43_p0 <= xo43;
        sg_xo43_p1 <= sg_xo43_p0;
        sg_xo43_p2 <= sg_xo43_p1;

        sg_w43_p0  <= resize(w43, 16); 
        sg_w43_p1  <= sg_w43_p0;
        sg_w43_p2  <= sg_w43_p1;

        sg_xow43_p0 <= sg_xo43_p2 * sg_w43_p2;
        sg_xow43_p1 <= sg_xow43_p0;
        sg_xow43_p2 <= sg_xow43_p1;


        -- wx44
        sg_xo44_p0 <= xo44;
        sg_xo44_p1 <= sg_xo44_p0;
        sg_xo44_p2 <= sg_xo44_p1;

        sg_w44_p0  <= resize(w44, 16); 
        sg_w44_p1  <= sg_w44_p0;
        sg_w44_p2  <= sg_w44_p1;

        sg_xow44_p0 <= sg_xo44_p2 * sg_w44_p2;
        sg_xow44_p1 <= sg_xow44_p0;
        sg_xow44_p2 <= sg_xow44_p1;


        -- wx45
        sg_xo45_p0 <= xo45;
        sg_xo45_p1 <= sg_xo45_p0;
        sg_xo45_p2 <= sg_xo45_p1;

        sg_w45_p0  <= resize(w45, 16); 
        sg_w45_p1  <= sg_w45_p0;
        sg_w45_p2  <= sg_w45_p1;

        sg_xow45_p0 <= sg_xo45_p2 * sg_w45_p2;
        sg_xow45_p1 <= sg_xow45_p0;
        sg_xow45_p2 <= sg_xow45_p1;


        -- wx46
        sg_xo46_p0 <= xo46;
        sg_xo46_p1 <= sg_xo46_p0;
        sg_xo46_p2 <= sg_xo46_p1;

        sg_w46_p0  <= resize(w46, 16); 
        sg_w46_p1  <= sg_w46_p0;
        sg_w46_p2  <= sg_w46_p1;

        sg_xow46_p0 <= sg_xo46_p2 * sg_w46_p2;
        sg_xow46_p1 <= sg_xow46_p0;
        sg_xow46_p2 <= sg_xow46_p1;


        -- wx47
        sg_xo47_p0 <= xo47;
        sg_xo47_p1 <= sg_xo47_p0;
        sg_xo47_p2 <= sg_xo47_p1;

        sg_w47_p0  <= resize(w47, 16); 
        sg_w47_p1  <= sg_w47_p0;
        sg_w47_p2  <= sg_w47_p1;

        sg_xow47_p0 <= sg_xo47_p2 * sg_w47_p2;
        sg_xow47_p1 <= sg_xow47_p0;
        sg_xow47_p2 <= sg_xow47_p1;


        -- wx48
        sg_xo48_p0 <= xo48;
        sg_xo48_p1 <= sg_xo48_p0;
        sg_xo48_p2 <= sg_xo48_p1;

        sg_w48_p0  <= resize(w48, 16); 
        sg_w48_p1  <= sg_w48_p0;
        sg_w48_p2  <= sg_w48_p1;

        sg_xow48_p0 <= sg_xo48_p2 * sg_w48_p2;
        sg_xow48_p1 <= sg_xow48_p0;
        sg_xow48_p2 <= sg_xow48_p1;


        -- wx49
        sg_xo49_p0 <= xo49;
        sg_xo49_p1 <= sg_xo49_p0;
        sg_xo49_p2 <= sg_xo49_p1;

        sg_w49_p0  <= resize(w49, 16); 
        sg_w49_p1  <= sg_w49_p0;
        sg_w49_p2  <= sg_w49_p1;

        sg_xow49_p0 <= sg_xo49_p2 * sg_w49_p2;
        sg_xow49_p1 <= sg_xow49_p0;
        sg_xow49_p2 <= sg_xow49_p1;


        -- wx50
        sg_xo50_p0 <= xo50;
        sg_xo50_p1 <= sg_xo50_p0;
        sg_xo50_p2 <= sg_xo50_p1;

        sg_w50_p0  <= resize(w50, 16); 
        sg_w50_p1  <= sg_w50_p0;
        sg_w50_p2  <= sg_w50_p1;

        sg_xow50_p0 <= sg_xo50_p2 * sg_w50_p2;
        sg_xow50_p1 <= sg_xow50_p0;
        sg_xow50_p2 <= sg_xow50_p1;


        -- wx51
        sg_xo51_p0 <= xo51;
        sg_xo51_p1 <= sg_xo51_p0;
        sg_xo51_p2 <= sg_xo51_p1;

        sg_w51_p0  <= resize(w51, 16); 
        sg_w51_p1  <= sg_w51_p0;
        sg_w51_p2  <= sg_w51_p1;

        sg_xow51_p0 <= sg_xo51_p2 * sg_w51_p2;
        sg_xow51_p1 <= sg_xow51_p0;
        sg_xow51_p2 <= sg_xow51_p1;


        -- wx52
        sg_xo52_p0 <= xo52;
        sg_xo52_p1 <= sg_xo52_p0;
        sg_xo52_p2 <= sg_xo52_p1;

        sg_w52_p0  <= resize(w52, 16); 
        sg_w52_p1  <= sg_w52_p0;
        sg_w52_p2  <= sg_w52_p1;

        sg_xow52_p0 <= sg_xo52_p2 * sg_w52_p2;
        sg_xow52_p1 <= sg_xow52_p0;
        sg_xow52_p2 <= sg_xow52_p1;


        -- wx53
        sg_xo53_p0 <= xo53;
        sg_xo53_p1 <= sg_xo53_p0;
        sg_xo53_p2 <= sg_xo53_p1;

        sg_w53_p0  <= resize(w53, 16); 
        sg_w53_p1  <= sg_w53_p0;
        sg_w53_p2  <= sg_w53_p1;

        sg_xow53_p0 <= sg_xo53_p2 * sg_w53_p2;
        sg_xow53_p1 <= sg_xow53_p0;
        sg_xow53_p2 <= sg_xow53_p1;


        -- wx54
        sg_xo54_p0 <= xo54;
        sg_xo54_p1 <= sg_xo54_p0;
        sg_xo54_p2 <= sg_xo54_p1;

        sg_w54_p0  <= resize(w54, 16); 
        sg_w54_p1  <= sg_w54_p0;
        sg_w54_p2  <= sg_w54_p1;

        sg_xow54_p0 <= sg_xo54_p2 * sg_w54_p2;
        sg_xow54_p1 <= sg_xow54_p0;
        sg_xow54_p2 <= sg_xow54_p1;


        -- wx55
        sg_xo55_p0 <= xo55;
        sg_xo55_p1 <= sg_xo55_p0;
        sg_xo55_p2 <= sg_xo55_p1;

        sg_w55_p0  <= resize(w55, 16); 
        sg_w55_p1  <= sg_w55_p0;
        sg_w55_p2  <= sg_w55_p1;

        sg_xow55_p0 <= sg_xo55_p2 * sg_w55_p2;
        sg_xow55_p1 <= sg_xow55_p0;
        sg_xow55_p2 <= sg_xow55_p1;


        -- wx56
        sg_xo56_p0 <= xo56;
        sg_xo56_p1 <= sg_xo56_p0;
        sg_xo56_p2 <= sg_xo56_p1;

        sg_w56_p0  <= resize(w56, 16); 
        sg_w56_p1  <= sg_w56_p0;
        sg_w56_p2  <= sg_w56_p1;

        sg_xow56_p0 <= sg_xo56_p2 * sg_w56_p2;
        sg_xow56_p1 <= sg_xow56_p0;
        sg_xow56_p2 <= sg_xow56_p1;


        -- wx57
        sg_xo57_p0 <= xo57;
        sg_xo57_p1 <= sg_xo57_p0;
        sg_xo57_p2 <= sg_xo57_p1;

        sg_w57_p0  <= resize(w57, 16); 
        sg_w57_p1  <= sg_w57_p0;
        sg_w57_p2  <= sg_w57_p1;

        sg_xow57_p0 <= sg_xo57_p2 * sg_w57_p2;
        sg_xow57_p1 <= sg_xow57_p0;
        sg_xow57_p2 <= sg_xow57_p1;


        -- wx58
        sg_xo58_p0 <= xo58;
        sg_xo58_p1 <= sg_xo58_p0;
        sg_xo58_p2 <= sg_xo58_p1;

        sg_w58_p0  <= resize(w58, 16); 
        sg_w58_p1  <= sg_w58_p0;
        sg_w58_p2  <= sg_w58_p1;

        sg_xow58_p0 <= sg_xo58_p2 * sg_w58_p2;
        sg_xow58_p1 <= sg_xow58_p0;
        sg_xow58_p2 <= sg_xow58_p1;


        -- wx59
        sg_xo59_p0 <= xo59;
        sg_xo59_p1 <= sg_xo59_p0;
        sg_xo59_p2 <= sg_xo59_p1;

        sg_w59_p0  <= resize(w59, 16); 
        sg_w59_p1  <= sg_w59_p0;
        sg_w59_p2  <= sg_w59_p1;

        sg_xow59_p0 <= sg_xo59_p2 * sg_w59_p2;
        sg_xow59_p1 <= sg_xow59_p0;
        sg_xow59_p2 <= sg_xow59_p1;


        -- wx60
        sg_xo60_p0 <= xo60;
        sg_xo60_p1 <= sg_xo60_p0;
        sg_xo60_p2 <= sg_xo60_p1;

        sg_w60_p0  <= resize(w60, 16); 
        sg_w60_p1  <= sg_w60_p0;
        sg_w60_p2  <= sg_w60_p1;

        sg_xow60_p0 <= sg_xo60_p2 * sg_w60_p2;
        sg_xow60_p1 <= sg_xow60_p0;
        sg_xow60_p2 <= sg_xow60_p1;


        -- wx61
        sg_xo61_p0 <= xo61;
        sg_xo61_p1 <= sg_xo61_p0;
        sg_xo61_p2 <= sg_xo61_p1;

        sg_w61_p0  <= resize(w61, 16); 
        sg_w61_p1  <= sg_w61_p0;
        sg_w61_p2  <= sg_w61_p1;

        sg_xow61_p0 <= sg_xo61_p2 * sg_w61_p2;
        sg_xow61_p1 <= sg_xow61_p0;
        sg_xow61_p2 <= sg_xow61_p1;


        -- wx62
        sg_xo62_p0 <= xo62;
        sg_xo62_p1 <= sg_xo62_p0;
        sg_xo62_p2 <= sg_xo62_p1;

        sg_w62_p0  <= resize(w62, 16); 
        sg_w62_p1  <= sg_w62_p0;
        sg_w62_p2  <= sg_w62_p1;

        sg_xow62_p0 <= sg_xo62_p2 * sg_w62_p2;
        sg_xow62_p1 <= sg_xow62_p0;
        sg_xow62_p2 <= sg_xow62_p1;


        -- wx63
        sg_xo63_p0 <= xo63;
        sg_xo63_p1 <= sg_xo63_p0;
        sg_xo63_p2 <= sg_xo63_p1;

        sg_w63_p0  <= resize(w63, 16); 
        sg_w63_p1  <= sg_w63_p0;
        sg_w63_p2  <= sg_w63_p1;

        sg_xow63_p0 <= sg_xo63_p2 * sg_w63_p2;
        sg_xow63_p1 <= sg_xow63_p0;
        sg_xow63_p2 <= sg_xow63_p1;


        -- wx64
        sg_xo64_p0 <= xo64;
        sg_xo64_p1 <= sg_xo64_p0;
        sg_xo64_p2 <= sg_xo64_p1;

        sg_w64_p0  <= resize(w64, 16); 
        sg_w64_p1  <= sg_w64_p0;
        sg_w64_p2  <= sg_w64_p1;

        sg_xow64_p0 <= sg_xo64_p2 * sg_w64_p2;
        sg_xow64_p1 <= sg_xow64_p0;
        sg_xow64_p2 <= sg_xow64_p1;


      end if;
  end process;
  
  xow01 <= sg_xow01_p2; xow02 <= sg_xow02_p2; xow03 <= sg_xow03_p2; xow04 <= sg_xow04_p2; xow05 <= sg_xow05_p2; xow06 <= sg_xow06_p2; xow07 <= sg_xow07_p2; xow08 <= sg_xow08_p2;  
  xow09 <= sg_xow09_p2; xow10 <= sg_xow10_p2; xow11 <= sg_xow11_p2; xow12 <= sg_xow12_p2; xow13 <= sg_xow13_p2; xow14 <= sg_xow14_p2; xow15 <= sg_xow15_p2; xow16 <= sg_xow16_p2;  
  xow17 <= sg_xow17_p2; xow18 <= sg_xow18_p2; xow19 <= sg_xow19_p2; xow20 <= sg_xow20_p2; xow21 <= sg_xow21_p2; xow22 <= sg_xow22_p2; xow23 <= sg_xow23_p2; xow24 <= sg_xow24_p2;  
  xow25 <= sg_xow25_p2; xow26 <= sg_xow26_p2; xow27 <= sg_xow27_p2; xow28 <= sg_xow28_p2; xow29 <= sg_xow29_p2; xow30 <= sg_xow30_p2; xow31 <= sg_xow31_p2; xow32 <= sg_xow32_p2;
  xow33 <= sg_xow33_p2; xow34 <= sg_xow34_p2; xow35 <= sg_xow35_p2; xow36 <= sg_xow36_p2; xow37 <= sg_xow37_p2; xow38 <= sg_xow38_p2; xow39 <= sg_xow39_p2; xow40 <= sg_xow40_p2;
  xow41 <= sg_xow41_p2; xow42 <= sg_xow42_p2; xow43 <= sg_xow43_p2; xow44 <= sg_xow44_p2; xow45 <= sg_xow45_p2; xow46 <= sg_xow46_p2; xow47 <= sg_xow47_p2; xow48 <= sg_xow48_p2;
  xow49 <= sg_xow49_p2; xow50 <= sg_xow50_p2; xow51 <= sg_xow51_p2; xow52 <= sg_xow52_p2; xow53 <= sg_xow53_p2; xow54 <= sg_xow54_p2; xow55 <= sg_xow55_p2; xow56 <= sg_xow56_p2;
  xow57 <= sg_xow57_p2; xow58 <= sg_xow58_p2; xow59 <= sg_xow59_p2; xow60 <= sg_xow60_p2; xow61 <= sg_xow61_p2; xow62 <= sg_xow62_p2; xow63 <= sg_xow63_p2; xow64 <= sg_xow64_p2;
  
  
end Behavioral;