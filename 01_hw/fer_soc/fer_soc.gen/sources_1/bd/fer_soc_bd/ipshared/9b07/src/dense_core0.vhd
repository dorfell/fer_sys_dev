---------------------------------------------------------------------
----                                                             ----
---- file:    dense_core0.vhd                                    ----
---- brief:   Noyau 0 pour la Dense                              ----
---- details: Module pour implementer x + offset_ent             ----
----          Les entrées sont nombres entiers entre             ----
----          -128 et 127 (int8). Les résultats sont             ----
----          nombres entiers en int16.                          ----
----          Couche Dense ou full connecte                      ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/11/15                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity dense_core0 is
  port( 
    -- Entrée 
    x01, x02, x03, x04, x05, x06, x07, x08, x09, x10, x11, x12, x13, x14, x15, x16 :  in signed( 7 downto 0);
    x17, x18, x19, x20, x21, x22, x23, x24, x25, x26, x27, x28, x29, x30, x31, x32 :  in signed( 7 downto 0);
    x33, x34, x35, x36, x37, x38, x39, x40, x41, x42, x43, x44, x45, x46, x47, x48 :  in signed( 7 downto 0);
    x49, x50, x51, x52, x53, x54, x55, x56, x57, x58, x59, x60, x61, x62, x63, x64 :  in signed( 7 downto 0);

    -- Offset entree 
    offset_ent : in signed(15 downto 0);

    -- Sortie (x + offset_ent)
    xo01, xo02, xo03, xo04, xo05, xo06, xo07, xo08, xo09, xo10, xo11, xo12, xo13, xo14, xo15, xo16 : out signed(15 downto 0);
    xo17, xo18, xo19, xo20, xo21, xo22, xo23, xo24, xo25, xo26, xo27, xo28, xo29, xo30, xo31, xo32 : out signed(15 downto 0);
    xo33, xo34, xo35, xo36, xo37, xo38, xo39, xo40, xo41, xo42, xo43, xo44, xo45, xo46, xo47, xo48 : out signed(15 downto 0);
    xo49, xo50, xo51, xo52, xo53, xo54, xo55, xo56, xo57, xo58, xo59, xo60, xo61, xo62, xo63, xo64 : out signed(15 downto 0) );
end dense_core0;


architecture Behavioral of dense_core0 is
  
begin

  -- Computer (x + offset_ent)   --
  ---------------------------------
  xo01 <= resize(x01, 16) + offset_ent; xo02 <= resize(x02, 16) + offset_ent; xo03 <= resize(x03, 16) + offset_ent; xo04 <= resize(x04, 16) + offset_ent;
  xo05 <= resize(x05, 16) + offset_ent; xo06 <= resize(x06, 16) + offset_ent; xo07 <= resize(x07, 16) + offset_ent; xo08 <= resize(x08, 16) + offset_ent; 
  xo09 <= resize(x09, 16) + offset_ent; xo10 <= resize(x10, 16) + offset_ent; xo11 <= resize(x11, 16) + offset_ent; xo12 <= resize(x12, 16) + offset_ent; 
  xo13 <= resize(x13, 16) + offset_ent; xo14 <= resize(x14, 16) + offset_ent; xo15 <= resize(x15, 16) + offset_ent; xo16 <= resize(x16, 16) + offset_ent;
    
  xo17 <= resize(x17, 16) + offset_ent; xo18 <= resize(x18, 16) + offset_ent; xo19 <= resize(x19, 16) + offset_ent; xo20 <= resize(x20, 16) + offset_ent;
  xo21 <= resize(x21, 16) + offset_ent; xo22 <= resize(x22, 16) + offset_ent; xo23 <= resize(x23, 16) + offset_ent; xo24 <= resize(x24, 16) + offset_ent; 
  xo25 <= resize(x25, 16) + offset_ent; xo26 <= resize(x26, 16) + offset_ent; xo27 <= resize(x27, 16) + offset_ent; xo28 <= resize(x28, 16) + offset_ent; 
  xo29 <= resize(x29, 16) + offset_ent; xo30 <= resize(x30, 16) + offset_ent; xo31 <= resize(x31, 16) + offset_ent; xo32 <= resize(x32, 16) + offset_ent; 

  xo33 <= resize(x33, 16) + offset_ent; xo34 <= resize(x34, 16) + offset_ent; xo35 <= resize(x35, 16) + offset_ent; xo36 <= resize(x36, 16) + offset_ent;
  xo37 <= resize(x37, 16) + offset_ent; xo38 <= resize(x38, 16) + offset_ent; xo39 <= resize(x39, 16) + offset_ent; xo40 <= resize(x40, 16) + offset_ent;
  xo41 <= resize(x41, 16) + offset_ent; xo42 <= resize(x42, 16) + offset_ent; xo43 <= resize(x43, 16) + offset_ent; xo44 <= resize(x44, 16) + offset_ent;
  xo45 <= resize(x45, 16) + offset_ent; xo46 <= resize(x46, 16) + offset_ent; xo47 <= resize(x47, 16) + offset_ent; xo48 <= resize(x48, 16) + offset_ent;

  xo49 <= resize(x49, 16) + offset_ent; xo50 <= resize(x50, 16) + offset_ent; xo51 <= resize(x51, 16) + offset_ent; xo52 <= resize(x52, 16) + offset_ent; 
  xo53 <= resize(x53, 16) + offset_ent; xo54 <= resize(x54, 16) + offset_ent; xo55 <= resize(x55, 16) + offset_ent; xo56 <= resize(x56, 16) + offset_ent;
  xo57 <= resize(x57, 16) + offset_ent; xo58 <= resize(x58, 16) + offset_ent; xo59 <= resize(x59, 16) + offset_ent; xo60 <= resize(x60, 16) + offset_ent;
  xo61 <= resize(x61, 16) + offset_ent; xo62 <= resize(x62, 16) + offset_ent; xo63 <= resize(x63, 16) + offset_ent; xo64 <= resize(x64, 16) + offset_ent;

end Behavioral;