---------------------------------------------------------------------
----                                                             ----
---- file:    tflite_core3.vhd                                   ----
---- brief:   Module noyau 3 de tflite                           ----
---- details: Module pour implementer les operations             ----
----          arithmetiques qui ont besoint de pipeline.         ----
----          SaturatingRoundingDoublingHighMul                  ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/07/04                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity tflite_core3 is
  port( 
    -- SaturatingRoundingDoublingHighMul (SRDHM) 
    overflow :  in std_logic;
    -- (ab_64 + nudge)
    ab_nudge :  in signed(63 downto 0);

    -- SaturatingRoundingDoublingHighMul (SRDHM) sortie
    -- srdhm <= 2147483647 when overflow = '1' else sg_ab_x2_high32;
    srdhm    : out signed(31 downto 0) );
end tflite_core3;


architecture Behavioral of tflite_core3 is
  
  -- Signaux pour SaturatingRoundingDoublingHighMul
  --signal sg_ls_1         : signed(47 downto 0);
  signal sg_ab_x2_div    : signed(63 downto 0);
  signal sg_ab_x2_high32 : signed(31 downto 0);
  
begin


  -- (1ll << 31);
  --sg_ls_1 <= shift_left(to_signed(1,48), 31);

  -- ab_x2_high32 = (int32_t) (ab_64 + nudge) / (1ll << 31); 
  -- remember 1ll is x1 = 0001.
  sg_ab_x2_div    <= shift_right(ab_nudge, 31);
  sg_ab_x2_high32 <= resize(sg_ab_x2_div, 32); 
        
  -- SRDHM sortie
  srdhm    <= to_signed(2147483647, 32) when overflow = '1' else sg_ab_x2_high32;
    
end Behavioral;