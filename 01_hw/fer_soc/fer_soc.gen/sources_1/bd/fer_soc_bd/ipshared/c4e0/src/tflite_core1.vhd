---------------------------------------------------------------------
----                                                             ----
---- file:    tflite_core1.vhd                                   ----
---- brief:   Module noyau 1 de tflite                           ----
---- details: Module pour implementer les operations             ----
----          arithmetiques qui ont besoin de pipeline.          ----
----          SaturatingRoundingDoublingHighMul                  ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/07/04                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity tflite_core1 is
  port( 
    clk      :  in std_logic;

    -- x * (1 << left_shift) 
    xls      :  in signed(31 downto 0);
        
    -- quantized_multiplier (M0)
    quantized_multiplier :  in signed(31 downto 0);
    
    -- SaturatingRoundingDoublingHighMul (SRDHM)
    -- ab_64 = (int32_t)(a) * (int32_t)(b)
    ab_64    : out signed(63 downto 0);
    nudge    : out signed(31 downto 0) );
end tflite_core1;


architecture Behavioral of tflite_core1 is


  -- SRDHM nudge
  function SRDHM_nudge(
    ab_64 : signed(63 downto 0) ) return signed is
    
    variable nudge : signed(31 downto 0);
    
    begin
     
      -- nudge
      if (ab_64 >= 0) then
        -- (1 << 30)
        nudge :=  to_signed(1073741824, 32);
      else   
        -- (1 - (1 << 30))
        nudge := to_signed(-1073741823, 32);
      end if;   
      
      return nudge;
  end function SRDHM_nudge;


  -- ab_64: signaux pour les registres de pipeline. registers de pipeline p0, p1, p2.
  signal sg_a_p0,     sg_a_p1,     sg_a_p2     : signed(31 downto 0);
  signal sg_b_p0,     sg_b_p1,     sg_b_p2     : signed(31 downto 0);
  signal sg_ab_64_p0, sg_ab_64_p1, sg_ab_64_p2 : signed(63 downto 0);
  signal sg_ab_64_p3                           : signed(63 downto 0);


  attribute USE_DSP : string;
  attribute USE_DSP of sg_ab_64_p0  : signal is "YES";


begin


  -- Depuis SaturatingRoundingDoublingHighMul
  process(clk)
    begin
      if (clk'event and clk = '1') then

        -- ab_64 = (int32_t)(a) * (int32_t)(b)
        sg_a_p0 <= xls;
        sg_a_p1 <= sg_a_p0;
        sg_a_p2 <= sg_a_p1;
       
        sg_b_p0 <= quantized_multiplier;
        sg_b_p1 <= sg_b_p0;
        sg_b_p2 <= sg_b_p1;
      
        sg_ab_64_p0  <= sg_a_p2 * sg_b_p2;
        sg_ab_64_p1 <= sg_ab_64_p0;
        sg_ab_64_p2 <= sg_ab_64_p1;
        sg_ab_64_p3 <= sg_ab_64_p2;

      end if;
  end process;
  
  
  --sortie: ab_64 = (int32_t)(a) * (int32_t)(b)
  ab_64 <= sg_ab_64_p3;
  
  -- SRDHM nudge
  nudge <= SRDHM_nudge(ab_64 => sg_ab_64_p3);

end Behavioral;
