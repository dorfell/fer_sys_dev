-- Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2021.1 (lin64) Build 3247384 Thu Jun 10 19:36:07 MDT 2021
-- Date        : Thu Nov 11 19:27:38 2021
-- Host        : dplegion running 64-bit Ubuntu 20.04.2 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top fer_soc_bd_tflite_mbqm_0_0 -prefix
--               fer_soc_bd_tflite_mbqm_0_0_ fer_soc_bd_tflite_mbqm_0_0_sim_netlist.vhdl
-- Design      : fer_soc_bd_tflite_mbqm_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fer_soc_bd_tflite_mbqm_0_0_tflite_core0 is
  port (
    xls : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg4_reg[5]\ : out STD_LOGIC;
    \slv_reg1_reg[11]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg4_reg[5]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg4_reg[7]\ : out STD_LOGIC;
    \slv_reg4_reg[7]_0\ : out STD_LOGIC;
    \slv_reg4_reg[7]_1\ : out STD_LOGIC;
    \slv_reg4_reg[7]_2\ : out STD_LOGIC;
    \slv_reg4_reg[0]\ : out STD_LOGIC;
    \slv_reg4_reg[0]_0\ : out STD_LOGIC;
    \slv_reg4_reg[3]\ : out STD_LOGIC;
    \slv_reg4_reg[3]_0\ : out STD_LOGIC;
    \slv_reg1_reg[23]\ : out STD_LOGIC;
    \axi_rdata[31]_i_8_0\ : out STD_LOGIC_VECTOR ( 22 downto 0 );
    \axi_rdata[11]_i_7_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    sg_ab_64_p3_reg : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sg_a_p0_reg[31]\ : in STD_LOGIC_VECTOR ( 30 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[3]_i_2_0\ : in STD_LOGIC;
    \axi_rdata_reg[3]_i_2_1\ : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata[19]_i_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata[19]_i_5_0\ : in STD_LOGIC;
    \axi_rdata_reg[31]_i_3_0\ : in STD_LOGIC;
    sel0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_rdata_reg[0]\ : in STD_LOGIC;
    \axi_rdata_reg[0]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \axi_rdata_reg[11]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_rdata_reg[3]_i_2_2\ : in STD_LOGIC;
    \axi_rdata[31]_i_17_0\ : in STD_LOGIC;
    \axi_rdata[31]_i_7_0\ : in STD_LOGIC;
    \axi_rdata_reg[3]_i_2_3\ : in STD_LOGIC;
    \axi_rdata_reg[3]_i_2_4\ : in STD_LOGIC;
    \axi_rdata[27]_i_15_0\ : in STD_LOGIC;
    \axi_rdata_reg[3]_i_2_5\ : in STD_LOGIC;
    \axi_rdata_reg[7]_i_2_0\ : in STD_LOGIC;
    \axi_rdata_reg[7]_i_2_1\ : in STD_LOGIC;
    \axi_rdata_reg[7]_i_2_2\ : in STD_LOGIC;
    \axi_rdata_reg[7]_i_2_3\ : in STD_LOGIC;
    \axi_rdata_reg[11]_i_2_0\ : in STD_LOGIC;
    \axi_rdata_reg[11]_i_2_1\ : in STD_LOGIC;
    \axi_rdata[11]_i_7_1\ : in STD_LOGIC;
    \axi_rdata[11]_i_5\ : in STD_LOGIC;
    \axi_rdata_reg[19]_i_2\ : in STD_LOGIC;
    \axi_rdata[23]_i_7_0\ : in STD_LOGIC;
    \axi_rdata[19]_i_5_1\ : in STD_LOGIC;
    \axi_rdata_reg[23]_i_2_0\ : in STD_LOGIC;
    \axi_rdata[23]_i_6_0\ : in STD_LOGIC;
    \axi_rdata[19]_i_4_0\ : in STD_LOGIC;
    \axi_rdata_reg[23]_i_2_1\ : in STD_LOGIC;
    \axi_rdata_reg[23]_i_2_2\ : in STD_LOGIC;
    \axi_rdata_reg[23]_i_2_3\ : in STD_LOGIC;
    \axi_rdata_reg[27]_i_2_0\ : in STD_LOGIC;
    \axi_rdata_reg[27]_i_2_1\ : in STD_LOGIC;
    \axi_rdata_reg[27]_i_2_2\ : in STD_LOGIC;
    \axi_rdata_reg[27]_i_2_3\ : in STD_LOGIC;
    \axi_rdata[3]_i_22_0\ : in STD_LOGIC;
    ab_nudge : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \axi_rdata_reg[31]_i_3_1\ : in STD_LOGIC;
    \axi_rdata_reg[31]_i_3_2\ : in STD_LOGIC;
    O : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_rdata_reg[31]_i_3_3\ : in STD_LOGIC;
    \axi_rdata_reg[23]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \sg_a_p0_reg[27]\ : in STD_LOGIC_VECTOR ( 27 downto 0 );
    \i__carry_i_1__1\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end fer_soc_bd_tflite_mbqm_0_0_tflite_core0;

architecture STRUCTURE of fer_soc_bd_tflite_mbqm_0_0_tflite_core0 is
  signal SHIFT_RIGHT : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \SRDHM_overflow0_inferred__0/i__carry__0_n_0\ : STD_LOGIC;
  signal \SRDHM_overflow0_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \SRDHM_overflow0_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \SRDHM_overflow0_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \SRDHM_overflow0_inferred__0/i__carry__1_n_1\ : STD_LOGIC;
  signal \SRDHM_overflow0_inferred__0/i__carry__1_n_2\ : STD_LOGIC;
  signal \SRDHM_overflow0_inferred__0/i__carry__1_n_3\ : STD_LOGIC;
  signal \SRDHM_overflow0_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \SRDHM_overflow0_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \SRDHM_overflow0_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \SRDHM_overflow0_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_13_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_15_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_17_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_18_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_19_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_20_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_21_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_22_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_23_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_20_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_21_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_22_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_23_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_24_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_13_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_15_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_16_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_13_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_15_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_16_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_17_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_18_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_19_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_20_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_21_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_13_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_17_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_18_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_19_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_20_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_14_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_17_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_18_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_19_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_20_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_21_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_22_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_23_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_24_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_15_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_16_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_17_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_18_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_19_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_20_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_2_n_1\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_2_n_2\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_2_n_3\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_2_n_1\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_2_n_2\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_2_n_3\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_2_n_1\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_2_n_2\ : STD_LOGIC;
  signal \axi_rdata_reg[27]_i_2_n_3\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_3_n_1\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_3_n_2\ : STD_LOGIC;
  signal \axi_rdata_reg[31]_i_3_n_3\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_2_n_1\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_2_n_2\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_2_n_3\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_2_n_1\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_2_n_2\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_2_n_3\ : STD_LOGIC;
  signal \i__carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_10_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_11_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_12_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_7_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_8_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_9_n_0\ : STD_LOGIC;
  signal \i__carry_i_10_n_0\ : STD_LOGIC;
  signal \i__carry_i_11_n_0\ : STD_LOGIC;
  signal \i__carry_i_12_n_0\ : STD_LOGIC;
  signal \i__carry_i_13_n_0\ : STD_LOGIC;
  signal \i__carry_i_14_n_0\ : STD_LOGIC;
  signal \i__carry_i_15_n_0\ : STD_LOGIC;
  signal \i__carry_i_16_n_0\ : STD_LOGIC;
  signal \i__carry_i_1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_4_n_0\ : STD_LOGIC;
  signal \i__carry_i_9_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__0_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__0_n_1\ : STD_LOGIC;
  signal \sg_xowb_carry__0_n_2\ : STD_LOGIC;
  signal \sg_xowb_carry__0_n_3\ : STD_LOGIC;
  signal \sg_xowb_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__1_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__1_n_1\ : STD_LOGIC;
  signal \sg_xowb_carry__1_n_2\ : STD_LOGIC;
  signal \sg_xowb_carry__1_n_3\ : STD_LOGIC;
  signal \sg_xowb_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__2_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__2_n_1\ : STD_LOGIC;
  signal \sg_xowb_carry__2_n_2\ : STD_LOGIC;
  signal \sg_xowb_carry__2_n_3\ : STD_LOGIC;
  signal \sg_xowb_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__3_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__3_n_1\ : STD_LOGIC;
  signal \sg_xowb_carry__3_n_2\ : STD_LOGIC;
  signal \sg_xowb_carry__3_n_3\ : STD_LOGIC;
  signal \sg_xowb_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__4_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__4_n_1\ : STD_LOGIC;
  signal \sg_xowb_carry__4_n_2\ : STD_LOGIC;
  signal \sg_xowb_carry__4_n_3\ : STD_LOGIC;
  signal \sg_xowb_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__5_n_0\ : STD_LOGIC;
  signal \sg_xowb_carry__5_n_1\ : STD_LOGIC;
  signal \sg_xowb_carry__5_n_2\ : STD_LOGIC;
  signal \sg_xowb_carry__5_n_3\ : STD_LOGIC;
  signal \sg_xowb_carry__6_n_1\ : STD_LOGIC;
  signal \sg_xowb_carry__6_n_2\ : STD_LOGIC;
  signal \sg_xowb_carry__6_n_3\ : STD_LOGIC;
  signal sg_xowb_carry_i_1_n_0 : STD_LOGIC;
  signal sg_xowb_carry_i_2_n_0 : STD_LOGIC;
  signal sg_xowb_carry_i_3_n_0 : STD_LOGIC;
  signal sg_xowb_carry_i_4_n_0 : STD_LOGIC;
  signal sg_xowb_carry_n_0 : STD_LOGIC;
  signal sg_xowb_carry_n_1 : STD_LOGIC;
  signal sg_xowb_carry_n_2 : STD_LOGIC;
  signal sg_xowb_carry_n_3 : STD_LOGIC;
  signal \^slv_reg1_reg[11]\ : STD_LOGIC;
  signal \^slv_reg1_reg[23]\ : STD_LOGIC;
  signal \^slv_reg4_reg[0]\ : STD_LOGIC;
  signal \^slv_reg4_reg[0]_0\ : STD_LOGIC;
  signal \^slv_reg4_reg[3]\ : STD_LOGIC;
  signal \^slv_reg4_reg[3]_0\ : STD_LOGIC;
  signal \^slv_reg4_reg[5]\ : STD_LOGIC;
  signal \^slv_reg4_reg[7]\ : STD_LOGIC;
  signal \^slv_reg4_reg[7]_0\ : STD_LOGIC;
  signal \^slv_reg4_reg[7]_1\ : STD_LOGIC;
  signal \^slv_reg4_reg[7]_2\ : STD_LOGIC;
  signal tf_out : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^xls\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_SRDHM_overflow0_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_SRDHM_overflow0_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_SRDHM_overflow0_inferred__0/i__carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_SRDHM_overflow0_inferred__0/i__carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_axi_rdata_reg[31]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_sg_xowb_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_24\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \axi_rdata[27]_i_9\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_10\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_13\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_17\ : label is "soft_lutpair2";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \axi_rdata_reg[11]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \axi_rdata_reg[23]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \axi_rdata_reg[27]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \axi_rdata_reg[31]_i_3\ : label is 35;
  attribute ADDER_THRESHOLD of \axi_rdata_reg[3]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \axi_rdata_reg[7]_i_2\ : label is 35;
  attribute SOFT_HLUTNM of \i__carry_i_9\ : label is "soft_lutpair0";
  attribute ADDER_THRESHOLD of sg_xowb_carry : label is 35;
  attribute ADDER_THRESHOLD of \sg_xowb_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \sg_xowb_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \sg_xowb_carry__2\ : label is 35;
  attribute ADDER_THRESHOLD of \sg_xowb_carry__3\ : label is 35;
  attribute ADDER_THRESHOLD of \sg_xowb_carry__4\ : label is 35;
  attribute ADDER_THRESHOLD of \sg_xowb_carry__5\ : label is 35;
  attribute ADDER_THRESHOLD of \sg_xowb_carry__6\ : label is 35;
begin
  \slv_reg1_reg[11]\ <= \^slv_reg1_reg[11]\;
  \slv_reg1_reg[23]\ <= \^slv_reg1_reg[23]\;
  \slv_reg4_reg[0]\ <= \^slv_reg4_reg[0]\;
  \slv_reg4_reg[0]_0\ <= \^slv_reg4_reg[0]_0\;
  \slv_reg4_reg[3]\ <= \^slv_reg4_reg[3]\;
  \slv_reg4_reg[3]_0\ <= \^slv_reg4_reg[3]_0\;
  \slv_reg4_reg[5]\ <= \^slv_reg4_reg[5]\;
  \slv_reg4_reg[7]\ <= \^slv_reg4_reg[7]\;
  \slv_reg4_reg[7]_0\ <= \^slv_reg4_reg[7]_0\;
  \slv_reg4_reg[7]_1\ <= \^slv_reg4_reg[7]_1\;
  \slv_reg4_reg[7]_2\ <= \^slv_reg4_reg[7]_2\;
  xls(31 downto 0) <= \^xls\(31 downto 0);
\SRDHM_overflow0_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \SRDHM_overflow0_inferred__0/i__carry_n_0\,
      CO(2) => \SRDHM_overflow0_inferred__0/i__carry_n_1\,
      CO(1) => \SRDHM_overflow0_inferred__0/i__carry_n_2\,
      CO(0) => \SRDHM_overflow0_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_SRDHM_overflow0_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry_i_1_n_0\,
      S(2) => \i__carry_i_2_n_0\,
      S(1) => \i__carry_i_3_n_0\,
      S(0) => \i__carry_i_4_n_0\
    );
\SRDHM_overflow0_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \SRDHM_overflow0_inferred__0/i__carry_n_0\,
      CO(3) => \SRDHM_overflow0_inferred__0/i__carry__0_n_0\,
      CO(2) => \SRDHM_overflow0_inferred__0/i__carry__0_n_1\,
      CO(1) => \SRDHM_overflow0_inferred__0/i__carry__0_n_2\,
      CO(0) => \SRDHM_overflow0_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_SRDHM_overflow0_inferred__0/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__0_i_1_n_0\,
      S(2) => \i__carry__0_i_2_n_0\,
      S(1) => \i__carry__0_i_3_n_0\,
      S(0) => \i__carry__0_i_4_n_0\
    );
\SRDHM_overflow0_inferred__0/i__carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \SRDHM_overflow0_inferred__0/i__carry__0_n_0\,
      CO(3) => \NLW_SRDHM_overflow0_inferred__0/i__carry__1_CO_UNCONNECTED\(3),
      CO(2) => \SRDHM_overflow0_inferred__0/i__carry__1_n_1\,
      CO(1) => \SRDHM_overflow0_inferred__0/i__carry__1_n_2\,
      CO(0) => \SRDHM_overflow0_inferred__0/i__carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_SRDHM_overflow0_inferred__0/i__carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \i__carry__1_i_1_n_0\,
      S(1) => \i__carry__1_i_2_n_0\,
      S(0) => \i__carry__1_i_3_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"30BB3088"
    )
        port map (
      I0 => \axi_rdata_reg[0]_0\,
      I1 => sel0(1),
      I2 => tf_out(0),
      I3 => sel0(0),
      I4 => \axi_rdata[19]_i_5\(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[11]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[11]_i_5\,
      I1 => \^slv_reg4_reg[7]_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \^slv_reg4_reg[7]_2\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[11]_i_17_n_0\,
      O => \^slv_reg4_reg[0]\
    );
\axi_rdata[11]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[11]_i_7_1\,
      I1 => \^slv_reg4_reg[7]\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \^slv_reg4_reg[7]_1\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[11]_i_18_n_0\,
      O => \axi_rdata[11]_i_13_n_0\
    );
\axi_rdata[11]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg4_reg[7]_2\,
      I1 => \axi_rdata[11]_i_17_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \^slv_reg4_reg[7]_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[11]_i_19_n_0\,
      O => \axi_rdata[11]_i_15_n_0\
    );
\axi_rdata[11]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888BFFFF888B0000"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(19),
      I3 => \^slv_reg1_reg[23]\,
      I4 => \axi_rdata[27]_i_15_0\,
      I5 => \axi_rdata[11]_i_20_n_0\,
      O => \^slv_reg4_reg[7]\
    );
\axi_rdata[11]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888BFFFF888B0000"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(18),
      I3 => \^slv_reg1_reg[23]\,
      I4 => \axi_rdata[27]_i_15_0\,
      I5 => \axi_rdata[11]_i_21_n_0\,
      O => \axi_rdata[11]_i_17_n_0\
    );
\axi_rdata[11]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888BFFFF888B0000"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(17),
      I3 => \^slv_reg1_reg[23]\,
      I4 => \axi_rdata[27]_i_15_0\,
      I5 => \axi_rdata[11]_i_22_n_0\,
      O => \axi_rdata[11]_i_18_n_0\
    );
\axi_rdata[11]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888BFFFF888B0000"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(16),
      I3 => \^slv_reg1_reg[23]\,
      I4 => \axi_rdata[27]_i_15_0\,
      I5 => \axi_rdata[11]_i_23_n_0\,
      O => \axi_rdata[11]_i_19_n_0\
    );
\axi_rdata[11]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747470047474747"
    )
        port map (
      I0 => ab_nudge(27),
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(11),
      I3 => \i__carry_i_11_n_0\,
      I4 => \i__carry_i_10_n_0\,
      I5 => \axi_rdata[15]_i_23_n_0\,
      O => \axi_rdata[11]_i_20_n_0\
    );
\axi_rdata[11]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747470047474747"
    )
        port map (
      I0 => ab_nudge(26),
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(10),
      I3 => \i__carry_i_11_n_0\,
      I4 => \i__carry_i_10_n_0\,
      I5 => \axi_rdata[15]_i_23_n_0\,
      O => \axi_rdata[11]_i_21_n_0\
    );
\axi_rdata[11]_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747470047474747"
    )
        port map (
      I0 => ab_nudge(25),
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(9),
      I3 => \i__carry_i_11_n_0\,
      I4 => \i__carry_i_10_n_0\,
      I5 => \axi_rdata[15]_i_23_n_0\,
      O => \axi_rdata[11]_i_22_n_0\
    );
\axi_rdata[11]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747470047474747"
    )
        port map (
      I0 => ab_nudge(24),
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(8),
      I3 => \i__carry_i_11_n_0\,
      I4 => \i__carry_i_10_n_0\,
      I5 => \axi_rdata[15]_i_23_n_0\,
      O => \axi_rdata[11]_i_23_n_0\
    );
\axi_rdata[11]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata_reg[11]_i_2_1\,
      I1 => \^slv_reg4_reg[5]\,
      I2 => \axi_rdata[11]_i_13_n_0\,
      I3 => \axi_rdata_reg[3]_i_2_2\,
      I4 => \^slv_reg4_reg[0]\,
      I5 => \axi_rdata_reg[3]_i_2_1\,
      O => SHIFT_RIGHT(9)
    );
\axi_rdata[11]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata_reg[11]_i_2_0\,
      I1 => \^slv_reg4_reg[5]\,
      I2 => \axi_rdata[11]_i_13_n_0\,
      I3 => \axi_rdata_reg[3]_i_2_1\,
      I4 => \axi_rdata[11]_i_15_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_2\,
      O => SHIFT_RIGHT(8)
    );
\axi_rdata[15]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888BFFFF888B0000"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(22),
      I3 => \^slv_reg1_reg[23]\,
      I4 => \axi_rdata[27]_i_15_0\,
      I5 => \axi_rdata[15]_i_20_n_0\,
      O => \^slv_reg4_reg[7]_2\
    );
\axi_rdata[15]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888BFFFF888B0000"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(21),
      I3 => \^slv_reg1_reg[23]\,
      I4 => \axi_rdata[27]_i_15_0\,
      I5 => \axi_rdata[15]_i_21_n_0\,
      O => \^slv_reg4_reg[7]_1\
    );
\axi_rdata[15]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888BFFFF888B0000"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(20),
      I3 => \^slv_reg1_reg[23]\,
      I4 => \axi_rdata[27]_i_15_0\,
      I5 => \axi_rdata[15]_i_22_n_0\,
      O => \^slv_reg4_reg[7]_0\
    );
\axi_rdata[15]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747470047474747"
    )
        port map (
      I0 => ab_nudge(30),
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(14),
      I3 => \i__carry_i_11_n_0\,
      I4 => \i__carry_i_10_n_0\,
      I5 => \axi_rdata[15]_i_23_n_0\,
      O => \axi_rdata[15]_i_20_n_0\
    );
\axi_rdata[15]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747470047474747"
    )
        port map (
      I0 => ab_nudge(29),
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(13),
      I3 => \i__carry_i_11_n_0\,
      I4 => \i__carry_i_10_n_0\,
      I5 => \axi_rdata[15]_i_23_n_0\,
      O => \axi_rdata[15]_i_21_n_0\
    );
\axi_rdata[15]_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747470047474747"
    )
        port map (
      I0 => ab_nudge(28),
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(12),
      I3 => \i__carry_i_11_n_0\,
      I4 => \i__carry_i_10_n_0\,
      I5 => \axi_rdata[15]_i_23_n_0\,
      O => \axi_rdata[15]_i_22_n_0\
    );
\axi_rdata[15]_i_23\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \^xls\(19),
      I1 => \^xls\(15),
      I2 => \^xls\(21),
      I3 => \i__carry_i_12_n_0\,
      I4 => \axi_rdata[15]_i_24_n_0\,
      O => \axi_rdata[15]_i_23_n_0\
    );
\axi_rdata[15]_i_24\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^xls\(18),
      I1 => \^xls\(8),
      I2 => \^xls\(23),
      I3 => \^xls\(12),
      O => \axi_rdata[15]_i_24_n_0\
    );
\axi_rdata[19]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata_reg[19]_i_2\,
      I1 => \^slv_reg4_reg[5]\,
      I2 => \axi_rdata[23]_i_15_n_0\,
      I3 => \axi_rdata_reg[3]_i_2_1\,
      I4 => \^slv_reg4_reg[0]_0\,
      I5 => \axi_rdata_reg[3]_i_2_2\,
      O => \slv_reg4_reg[5]_0\(0)
    );
\axi_rdata[19]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[27]_i_20_n_0\,
      I1 => \axi_rdata[23]_i_7_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \^slv_reg4_reg[3]\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[19]_i_5_1\,
      O => \^slv_reg4_reg[0]_0\
    );
\axi_rdata[23]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[27]_i_21_n_0\,
      I1 => \^slv_reg4_reg[3]_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[27]_i_18_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[23]_i_6_0\,
      O => \axi_rdata[23]_i_11_n_0\
    );
\axi_rdata[23]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[23]_i_16_n_0\,
      I1 => \^slv_reg4_reg[3]\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[27]_i_20_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[23]_i_7_0\,
      O => \axi_rdata[23]_i_13_n_0\
    );
\axi_rdata[23]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[27]_i_18_n_0\,
      I1 => \axi_rdata[23]_i_6_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \^slv_reg4_reg[3]_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[19]_i_4_0\,
      O => \axi_rdata[23]_i_15_n_0\
    );
\axi_rdata[23]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C8C8C8CD"
    )
        port map (
      I0 => \axi_rdata[27]_i_15_0\,
      I1 => \^slv_reg1_reg[11]\,
      I2 => \axi_rdata[3]_i_22_0\,
      I3 => ab_nudge(27),
      I4 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[23]_i_16_n_0\
    );
\axi_rdata[23]_i_17\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C8C8C8CD"
    )
        port map (
      I0 => \axi_rdata[27]_i_15_0\,
      I1 => \^slv_reg1_reg[11]\,
      I2 => \axi_rdata[3]_i_22_0\,
      I3 => ab_nudge(23),
      I4 => \^slv_reg1_reg[23]\,
      O => \^slv_reg4_reg[3]\
    );
\axi_rdata[23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA20AA2020"
    )
        port map (
      I0 => \axi_rdata_reg[23]_i_2_3\,
      I1 => \axi_rdata[23]_i_9_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_2\,
      I3 => \axi_rdata[27]_i_15_n_0\,
      I4 => \axi_rdata_reg[3]_i_2_1\,
      I5 => \^slv_reg4_reg[5]\,
      O => SHIFT_RIGHT(23)
    );
\axi_rdata[23]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA20AA2020"
    )
        port map (
      I0 => \axi_rdata_reg[23]_i_2_2\,
      I1 => \axi_rdata[23]_i_11_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_2\,
      I3 => \axi_rdata[23]_i_9_n_0\,
      I4 => \axi_rdata_reg[3]_i_2_1\,
      I5 => \^slv_reg4_reg[5]\,
      O => SHIFT_RIGHT(22)
    );
\axi_rdata[23]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA20AA2020"
    )
        port map (
      I0 => \axi_rdata_reg[23]_i_2_1\,
      I1 => \axi_rdata[23]_i_13_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_2\,
      I3 => \axi_rdata[23]_i_11_n_0\,
      I4 => \axi_rdata_reg[3]_i_2_1\,
      I5 => \^slv_reg4_reg[5]\,
      O => SHIFT_RIGHT(21)
    );
\axi_rdata[23]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata_reg[23]_i_2_0\,
      I1 => \^slv_reg4_reg[5]\,
      I2 => \axi_rdata[23]_i_13_n_0\,
      I3 => \axi_rdata_reg[3]_i_2_1\,
      I4 => \axi_rdata[23]_i_15_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_2\,
      O => SHIFT_RIGHT(20)
    );
\axi_rdata[23]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[27]_i_19_n_0\,
      I1 => \axi_rdata[27]_i_20_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[23]_i_16_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \^slv_reg4_reg[3]\,
      O => \axi_rdata[23]_i_9_n_0\
    );
\axi_rdata[27]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[31]_i_20_n_0\,
      I1 => \axi_rdata[31]_i_7_0\,
      I2 => \axi_rdata[27]_i_17_n_0\,
      I3 => \axi_rdata[31]_i_17_0\,
      I4 => \axi_rdata[27]_i_18_n_0\,
      O => \axi_rdata[27]_i_11_n_0\
    );
\axi_rdata[27]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[27]_i_16_n_0\,
      I1 => \axi_rdata[31]_i_7_0\,
      I2 => \axi_rdata[27]_i_19_n_0\,
      I3 => \axi_rdata[31]_i_17_0\,
      I4 => \axi_rdata[27]_i_20_n_0\,
      O => \axi_rdata[27]_i_13_n_0\
    );
\axi_rdata[27]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[27]_i_17_n_0\,
      I1 => \axi_rdata[27]_i_18_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[27]_i_21_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \^slv_reg4_reg[3]_0\,
      O => \axi_rdata[27]_i_15_n_0\
    );
\axi_rdata[27]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0E0F0E0F0E0F0F1"
    )
        port map (
      I0 => \axi_rdata[31]_i_17_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => \^slv_reg1_reg[11]\,
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(27),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[27]_i_16_n_0\
    );
\axi_rdata[27]_i_17\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C8C8C8CD"
    )
        port map (
      I0 => \axi_rdata[27]_i_15_0\,
      I1 => \^slv_reg1_reg[11]\,
      I2 => \axi_rdata[3]_i_22_0\,
      I3 => ab_nudge(30),
      I4 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[27]_i_17_n_0\
    );
\axi_rdata[27]_i_18\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C8C8C8CD"
    )
        port map (
      I0 => \axi_rdata[27]_i_15_0\,
      I1 => \^slv_reg1_reg[11]\,
      I2 => \axi_rdata[3]_i_22_0\,
      I3 => ab_nudge(26),
      I4 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[27]_i_18_n_0\
    );
\axi_rdata[27]_i_19\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C8C8C8CD"
    )
        port map (
      I0 => \axi_rdata[27]_i_15_0\,
      I1 => \^slv_reg1_reg[11]\,
      I2 => \axi_rdata[3]_i_22_0\,
      I3 => ab_nudge(29),
      I4 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[27]_i_19_n_0\
    );
\axi_rdata[27]_i_20\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C8C8C8CD"
    )
        port map (
      I0 => \axi_rdata[27]_i_15_0\,
      I1 => \^slv_reg1_reg[11]\,
      I2 => \axi_rdata[3]_i_22_0\,
      I3 => ab_nudge(25),
      I4 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[27]_i_20_n_0\
    );
\axi_rdata[27]_i_21\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C8C8C8CD"
    )
        port map (
      I0 => \axi_rdata[27]_i_15_0\,
      I1 => \^slv_reg1_reg[11]\,
      I2 => \axi_rdata[3]_i_22_0\,
      I3 => ab_nudge(28),
      I4 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[27]_i_21_n_0\
    );
\axi_rdata[27]_i_22\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C8C8C8CD"
    )
        port map (
      I0 => \axi_rdata[27]_i_15_0\,
      I1 => \^slv_reg1_reg[11]\,
      I2 => \axi_rdata[3]_i_22_0\,
      I3 => ab_nudge(24),
      I4 => \^slv_reg1_reg[23]\,
      O => \^slv_reg4_reg[3]_0\
    );
\axi_rdata[27]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata_reg[27]_i_2_3\,
      I1 => \^slv_reg4_reg[5]\,
      I2 => \axi_rdata[31]_i_17_n_0\,
      I3 => \axi_rdata_reg[3]_i_2_1\,
      I4 => \axi_rdata[27]_i_9_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_2\,
      O => SHIFT_RIGHT(27)
    );
\axi_rdata[27]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata_reg[27]_i_2_2\,
      I1 => \^slv_reg4_reg[5]\,
      I2 => \axi_rdata[27]_i_9_n_0\,
      I3 => \axi_rdata_reg[3]_i_2_1\,
      I4 => \axi_rdata[27]_i_11_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_2\,
      O => SHIFT_RIGHT(26)
    );
\axi_rdata[27]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata_reg[27]_i_2_1\,
      I1 => \^slv_reg4_reg[5]\,
      I2 => \axi_rdata[27]_i_11_n_0\,
      I3 => \axi_rdata_reg[3]_i_2_1\,
      I4 => \axi_rdata[27]_i_13_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_2\,
      O => SHIFT_RIGHT(25)
    );
\axi_rdata[27]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA20AA2020"
    )
        port map (
      I0 => \axi_rdata_reg[27]_i_2_0\,
      I1 => \axi_rdata[27]_i_15_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_2\,
      I3 => \axi_rdata[27]_i_13_n_0\,
      I4 => \axi_rdata_reg[3]_i_2_1\,
      I5 => \^slv_reg4_reg[5]\,
      O => SHIFT_RIGHT(24)
    );
\axi_rdata[27]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \axi_rdata[31]_i_19_n_0\,
      I1 => \axi_rdata[31]_i_7_0\,
      I2 => \axi_rdata[27]_i_16_n_0\,
      O => \axi_rdata[27]_i_9_n_0\
    );
\axi_rdata[31]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata[31]_i_7_0\,
      I2 => \axi_rdata[31]_i_18_n_0\,
      O => \axi_rdata[31]_i_10_n_0\
    );
\axi_rdata[31]_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata[31]_i_7_0\,
      I2 => \axi_rdata[31]_i_19_n_0\,
      O => \axi_rdata[31]_i_13_n_0\
    );
\axi_rdata[31]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000DF00FFFFFFFF"
    )
        port map (
      I0 => \axi_rdata[19]_i_5\(1),
      I1 => \axi_rdata[19]_i_5_0\,
      I2 => \axi_rdata[19]_i_5\(2),
      I3 => \axi_rdata[19]_i_5\(3),
      I4 => \^slv_reg1_reg[11]\,
      I5 => \axi_rdata_reg[31]_i_3_0\,
      O => \^slv_reg4_reg[5]\
    );
\axi_rdata[31]_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \axi_rdata[31]_i_18_n_0\,
      I1 => \axi_rdata[31]_i_7_0\,
      I2 => \axi_rdata[31]_i_20_n_0\,
      O => \axi_rdata[31]_i_17_n_0\
    );
\axi_rdata[31]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0E0F0E0F0E0F0F1"
    )
        port map (
      I0 => \axi_rdata[31]_i_17_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => \^slv_reg1_reg[11]\,
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(30),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[31]_i_18_n_0\
    );
\axi_rdata[31]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0E0F0E0F0E0F0F1"
    )
        port map (
      I0 => \axi_rdata[31]_i_17_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => \^slv_reg1_reg[11]\,
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(29),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[31]_i_19_n_0\
    );
\axi_rdata[31]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0E0F0E0F0E0F0F1"
    )
        port map (
      I0 => \axi_rdata[31]_i_17_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => \^slv_reg1_reg[11]\,
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(28),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[31]_i_20_n_0\
    );
\axi_rdata[31]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4474"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata_reg[31]_i_3_0\,
      I2 => O(1),
      I3 => \axi_rdata_reg[31]_i_3_3\,
      O => SHIFT_RIGHT(31)
    );
\axi_rdata[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"474747470000FF00"
    )
        port map (
      I0 => \axi_rdata[31]_i_10_n_0\,
      I1 => \axi_rdata_reg[3]_i_2_2\,
      I2 => \^slv_reg1_reg[11]\,
      I3 => O(0),
      I4 => \axi_rdata_reg[31]_i_3_3\,
      I5 => \axi_rdata_reg[31]_i_3_0\,
      O => SHIFT_RIGHT(30)
    );
\axi_rdata[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA20AAAAAA20AA20"
    )
        port map (
      I0 => \axi_rdata_reg[31]_i_3_2\,
      I1 => \axi_rdata[31]_i_13_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_2\,
      I3 => \^slv_reg4_reg[5]\,
      I4 => \axi_rdata[31]_i_10_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_1\,
      O => SHIFT_RIGHT(29)
    );
\axi_rdata[31]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata_reg[31]_i_3_1\,
      I1 => \^slv_reg4_reg[5]\,
      I2 => \axi_rdata[31]_i_17_n_0\,
      I3 => \axi_rdata_reg[3]_i_2_2\,
      I4 => \axi_rdata[31]_i_13_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_1\,
      O => SHIFT_RIGHT(28)
    );
\axi_rdata[3]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[7]_i_16_n_0\,
      I1 => \axi_rdata[3]_i_20_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[7]_i_18_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[3]_i_21_n_0\,
      O => \axi_rdata[3]_i_10_n_0\
    );
\axi_rdata[3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[11]_i_18_n_0\,
      I1 => \axi_rdata[7]_i_18_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[7]_i_16_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[3]_i_20_n_0\,
      O => \axi_rdata[3]_i_12_n_0\
    );
\axi_rdata[3]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[11]_i_19_n_0\,
      I1 => \axi_rdata[7]_i_19_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[7]_i_17_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[3]_i_18_n_0\,
      O => \axi_rdata[3]_i_14_n_0\
    );
\axi_rdata[3]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000002A2AAAA02A2"
    )
        port map (
      I0 => \axi_rdata_reg[3]_i_2_2\,
      I1 => \axi_rdata[3]_i_19_n_0\,
      I2 => \axi_rdata[31]_i_17_0\,
      I3 => \axi_rdata[7]_i_19_n_0\,
      I4 => \axi_rdata[31]_i_7_0\,
      I5 => \axi_rdata[3]_i_22_n_0\,
      O => \axi_rdata[3]_i_17_n_0\
    );
\axi_rdata[3]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888888888B888BBB"
    )
        port map (
      I0 => \axi_rdata[11]_i_21_n_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => ab_nudge(18),
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(2),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[3]_i_18_n_0\
    );
\axi_rdata[3]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888888888B888BBB"
    )
        port map (
      I0 => \axi_rdata[11]_i_23_n_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => ab_nudge(16),
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(0),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[3]_i_19_n_0\
    );
\axi_rdata[3]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888888888B888BBB"
    )
        port map (
      I0 => \axi_rdata[11]_i_20_n_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => ab_nudge(19),
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(3),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[3]_i_20_n_0\
    );
\axi_rdata[3]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888888888B888BBB"
    )
        port map (
      I0 => \axi_rdata[11]_i_22_n_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => ab_nudge(17),
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(1),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[3]_i_21_n_0\
    );
\axi_rdata[3]_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[15]_i_20_n_0\,
      I1 => \axi_rdata[3]_i_23_n_0\,
      I2 => \axi_rdata[31]_i_17_0\,
      I3 => \axi_rdata[11]_i_21_n_0\,
      I4 => \axi_rdata[27]_i_15_0\,
      I5 => \axi_rdata[3]_i_24_n_0\,
      O => \axi_rdata[3]_i_22_n_0\
    );
\axi_rdata[3]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747470047474747"
    )
        port map (
      I0 => ab_nudge(22),
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(6),
      I3 => \i__carry_i_11_n_0\,
      I4 => \i__carry_i_10_n_0\,
      I5 => \axi_rdata[15]_i_23_n_0\,
      O => \axi_rdata[3]_i_23_n_0\
    );
\axi_rdata[3]_i_24\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747470047474747"
    )
        port map (
      I0 => ab_nudge(18),
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(2),
      I3 => \i__carry_i_11_n_0\,
      I4 => \i__carry_i_10_n_0\,
      I5 => \axi_rdata[15]_i_23_n_0\,
      O => \axi_rdata[3]_i_24_n_0\
    );
\axi_rdata[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BABAFFBA"
    )
        port map (
      I0 => \^slv_reg4_reg[5]\,
      I1 => \axi_rdata[3]_i_9_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_2\,
      I3 => \axi_rdata_reg[3]_i_2_1\,
      I4 => \axi_rdata[3]_i_10_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_0\,
      O => SHIFT_RIGHT(0)
    );
\axi_rdata[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BABAFFBA"
    )
        port map (
      I0 => \^slv_reg4_reg[5]\,
      I1 => \axi_rdata[7]_i_15_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_1\,
      I3 => \axi_rdata_reg[3]_i_2_2\,
      I4 => \axi_rdata[3]_i_12_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_5\,
      O => SHIFT_RIGHT(3)
    );
\axi_rdata[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BABAFFBA"
    )
        port map (
      I0 => \^slv_reg4_reg[5]\,
      I1 => \axi_rdata[3]_i_12_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_1\,
      I3 => \axi_rdata_reg[3]_i_2_2\,
      I4 => \axi_rdata[3]_i_14_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_4\,
      O => SHIFT_RIGHT(2)
    );
\axi_rdata[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BABAFFBA"
    )
        port map (
      I0 => \^slv_reg4_reg[5]\,
      I1 => \axi_rdata[3]_i_10_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_2\,
      I3 => \axi_rdata_reg[3]_i_2_1\,
      I4 => \axi_rdata[3]_i_14_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_3\,
      O => SHIFT_RIGHT(1)
    );
\axi_rdata[3]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555510AAAAAAEF"
    )
        port map (
      I0 => \axi_rdata_reg[3]_i_2_0\,
      I1 => \axi_rdata[3]_i_10_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_1\,
      I3 => \axi_rdata[3]_i_17_n_0\,
      I4 => \^slv_reg4_reg[5]\,
      I5 => CO(0),
      O => \axi_rdata[3]_i_8_n_0\
    );
\axi_rdata[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[7]_i_17_n_0\,
      I1 => \axi_rdata[3]_i_18_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[7]_i_19_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[3]_i_19_n_0\,
      O => \axi_rdata[3]_i_9_n_0\
    );
\axi_rdata[7]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg4_reg[7]_0\,
      I1 => \axi_rdata[11]_i_19_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[11]_i_17_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[7]_i_17_n_0\,
      O => \axi_rdata[7]_i_10_n_0\
    );
\axi_rdata[7]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg4_reg[7]\,
      I1 => \axi_rdata[7]_i_16_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[11]_i_18_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[7]_i_18_n_0\,
      O => \axi_rdata[7]_i_12_n_0\
    );
\axi_rdata[7]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[11]_i_17_n_0\,
      I1 => \axi_rdata[7]_i_17_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \axi_rdata[11]_i_19_n_0\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[7]_i_19_n_0\,
      O => \axi_rdata[7]_i_15_n_0\
    );
\axi_rdata[7]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888BFFFF888B0000"
    )
        port map (
      I0 => \^slv_reg1_reg[11]\,
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(15),
      I3 => \^slv_reg1_reg[23]\,
      I4 => \axi_rdata[27]_i_15_0\,
      I5 => \axi_rdata[7]_i_20_n_0\,
      O => \axi_rdata[7]_i_16_n_0\
    );
\axi_rdata[7]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888888888B888BBB"
    )
        port map (
      I0 => \axi_rdata[15]_i_20_n_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => ab_nudge(22),
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(6),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[7]_i_17_n_0\
    );
\axi_rdata[7]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888888888B888BBB"
    )
        port map (
      I0 => \axi_rdata[15]_i_21_n_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => ab_nudge(21),
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(5),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[7]_i_18_n_0\
    );
\axi_rdata[7]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888888888B888BBB"
    )
        port map (
      I0 => \axi_rdata[15]_i_22_n_0\,
      I1 => \axi_rdata[27]_i_15_0\,
      I2 => ab_nudge(20),
      I3 => \axi_rdata[3]_i_22_0\,
      I4 => ab_nudge(4),
      I5 => \^slv_reg1_reg[23]\,
      O => \axi_rdata[7]_i_19_n_0\
    );
\axi_rdata[7]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747470047474747"
    )
        port map (
      I0 => ab_nudge(23),
      I1 => \axi_rdata[3]_i_22_0\,
      I2 => ab_nudge(7),
      I3 => \i__carry_i_11_n_0\,
      I4 => \i__carry_i_10_n_0\,
      I5 => \axi_rdata[15]_i_23_n_0\,
      O => \axi_rdata[7]_i_20_n_0\
    );
\axi_rdata[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BABAFFBA"
    )
        port map (
      I0 => \^slv_reg4_reg[5]\,
      I1 => \axi_rdata[7]_i_8_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_2\,
      I3 => \axi_rdata_reg[3]_i_2_1\,
      I4 => \axi_rdata[11]_i_15_n_0\,
      I5 => \axi_rdata_reg[7]_i_2_3\,
      O => SHIFT_RIGHT(7)
    );
\axi_rdata[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BABAFFBA"
    )
        port map (
      I0 => \^slv_reg4_reg[5]\,
      I1 => \axi_rdata[7]_i_8_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_1\,
      I3 => \axi_rdata_reg[3]_i_2_2\,
      I4 => \axi_rdata[7]_i_10_n_0\,
      I5 => \axi_rdata_reg[7]_i_2_2\,
      O => SHIFT_RIGHT(6)
    );
\axi_rdata[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BABAFFBA"
    )
        port map (
      I0 => \^slv_reg4_reg[5]\,
      I1 => \axi_rdata[7]_i_10_n_0\,
      I2 => \axi_rdata_reg[3]_i_2_1\,
      I3 => \axi_rdata_reg[3]_i_2_2\,
      I4 => \axi_rdata[7]_i_12_n_0\,
      I5 => \axi_rdata_reg[7]_i_2_1\,
      O => SHIFT_RIGHT(5)
    );
\axi_rdata[7]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata_reg[7]_i_2_0\,
      I1 => \^slv_reg4_reg[5]\,
      I2 => \axi_rdata[7]_i_15_n_0\,
      I3 => \axi_rdata_reg[3]_i_2_2\,
      I4 => \axi_rdata[7]_i_12_n_0\,
      I5 => \axi_rdata_reg[3]_i_2_1\,
      O => SHIFT_RIGHT(4)
    );
\axi_rdata[7]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg4_reg[7]_1\,
      I1 => \axi_rdata[11]_i_18_n_0\,
      I2 => \axi_rdata[31]_i_7_0\,
      I3 => \^slv_reg4_reg[7]\,
      I4 => \axi_rdata[31]_i_17_0\,
      I5 => \axi_rdata[7]_i_16_n_0\,
      O => \axi_rdata[7]_i_8_n_0\
    );
\axi_rdata_reg[0]_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata_reg[0]\,
      I1 => \axi_rdata[0]_i_3_n_0\,
      O => D(0),
      S => sel0(2)
    );
\axi_rdata_reg[11]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_rdata_reg[7]_i_2_n_0\,
      CO(3) => \axi_rdata[11]_i_7_0\(0),
      CO(2) => \axi_rdata_reg[11]_i_2_n_1\,
      CO(1) => \axi_rdata_reg[11]_i_2_n_2\,
      CO(0) => \axi_rdata_reg[11]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \axi_rdata[31]_i_8_0\(10 downto 7),
      S(3 downto 2) => \axi_rdata_reg[11]\(1 downto 0),
      S(1 downto 0) => SHIFT_RIGHT(9 downto 8)
    );
\axi_rdata_reg[23]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_rdata_reg[23]\(0),
      CO(3) => \axi_rdata_reg[23]_i_2_n_0\,
      CO(2) => \axi_rdata_reg[23]_i_2_n_1\,
      CO(1) => \axi_rdata_reg[23]_i_2_n_2\,
      CO(0) => \axi_rdata_reg[23]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \axi_rdata[31]_i_8_0\(14 downto 11),
      S(3 downto 0) => SHIFT_RIGHT(23 downto 20)
    );
\axi_rdata_reg[27]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_rdata_reg[23]_i_2_n_0\,
      CO(3) => \axi_rdata_reg[27]_i_2_n_0\,
      CO(2) => \axi_rdata_reg[27]_i_2_n_1\,
      CO(1) => \axi_rdata_reg[27]_i_2_n_2\,
      CO(0) => \axi_rdata_reg[27]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \axi_rdata[31]_i_8_0\(18 downto 15),
      S(3 downto 0) => SHIFT_RIGHT(27 downto 24)
    );
\axi_rdata_reg[31]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_rdata_reg[27]_i_2_n_0\,
      CO(3) => \NLW_axi_rdata_reg[31]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \axi_rdata_reg[31]_i_3_n_1\,
      CO(1) => \axi_rdata_reg[31]_i_3_n_2\,
      CO(0) => \axi_rdata_reg[31]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \axi_rdata[31]_i_8_0\(22 downto 19),
      S(3 downto 0) => SHIFT_RIGHT(31 downto 28)
    );
\axi_rdata_reg[3]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \axi_rdata_reg[3]_i_2_n_0\,
      CO(2) => \axi_rdata_reg[3]_i_2_n_1\,
      CO(1) => \axi_rdata_reg[3]_i_2_n_2\,
      CO(0) => \axi_rdata_reg[3]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => SHIFT_RIGHT(0),
      O(3 downto 1) => \axi_rdata[31]_i_8_0\(2 downto 0),
      O(0) => tf_out(0),
      S(3 downto 1) => SHIFT_RIGHT(3 downto 1),
      S(0) => \axi_rdata[3]_i_8_n_0\
    );
\axi_rdata_reg[7]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_rdata_reg[3]_i_2_n_0\,
      CO(3) => \axi_rdata_reg[7]_i_2_n_0\,
      CO(2) => \axi_rdata_reg[7]_i_2_n_1\,
      CO(1) => \axi_rdata_reg[7]_i_2_n_2\,
      CO(0) => \axi_rdata_reg[7]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \axi_rdata[31]_i_8_0\(6 downto 3),
      S(3 downto 0) => SHIFT_RIGHT(7 downto 4)
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^xls\(21),
      I1 => Q(21),
      I2 => \^xls\(22),
      I3 => Q(22),
      I4 => Q(23),
      I5 => \^xls\(23),
      O => \i__carry__0_i_1_n_0\
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^xls\(19),
      I1 => Q(19),
      I2 => \^xls\(18),
      I3 => Q(18),
      I4 => Q(20),
      I5 => \^xls\(20),
      O => \i__carry__0_i_2_n_0\
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^xls\(15),
      I1 => Q(15),
      I2 => \^xls\(16),
      I3 => Q(16),
      I4 => Q(17),
      I5 => \^xls\(17),
      O => \i__carry__0_i_3_n_0\
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^xls\(12),
      I1 => Q(12),
      I2 => \^xls\(13),
      I3 => Q(13),
      I4 => Q(14),
      I5 => \^xls\(14),
      O => \i__carry__0_i_4_n_0\
    );
\i__carry__1_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^xls\(31),
      I1 => Q(31),
      I2 => Q(30),
      I3 => \^xls\(30),
      O => \i__carry__1_i_1_n_0\
    );
\i__carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^xls\(27),
      I1 => Q(27),
      I2 => \^xls\(28),
      I3 => Q(28),
      I4 => Q(29),
      I5 => \^xls\(29),
      O => \i__carry__1_i_2_n_0\
    );
\i__carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^xls\(24),
      I1 => Q(24),
      I2 => \^xls\(25),
      I3 => Q(25),
      I4 => Q(26),
      I5 => \^xls\(26),
      O => \i__carry__1_i_3_n_0\
    );
\i__carry__6_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \^xls\(18),
      I1 => \^xls\(19),
      I2 => \^xls\(20),
      I3 => \^xls\(23),
      I4 => \^xls\(15),
      I5 => \^xls\(12),
      O => \i__carry__6_i_10_n_0\
    );
\i__carry__6_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \^xls\(29),
      I1 => \^xls\(28),
      I2 => \^xls\(24),
      I3 => \^xls\(27),
      I4 => \^xls\(25),
      I5 => \^xls\(26),
      O => \i__carry__6_i_11_n_0\
    );
\i__carry__6_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \^xls\(30),
      I1 => \^xls\(2),
      I2 => \^xls\(31),
      I3 => \^xls\(1),
      O => \i__carry__6_i_12_n_0\
    );
\i__carry__6_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00010000FFFFFFFF"
    )
        port map (
      I0 => \i__carry__6_i_7_n_0\,
      I1 => \^xls\(8),
      I2 => \^xls\(11),
      I3 => \i__carry__6_i_8_n_0\,
      I4 => \i__carry__6_i_9_n_0\,
      I5 => ab_nudge(31),
      O => \^slv_reg1_reg[11]\
    );
\i__carry__6_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \^xls\(7),
      I1 => \^xls\(6),
      I2 => \^xls\(9),
      I3 => \^xls\(14),
      I4 => \^xls\(10),
      I5 => \^xls\(13),
      O => \i__carry__6_i_7_n_0\
    );
\i__carry__6_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFEF"
    )
        port map (
      I0 => \i__carry__6_i_10_n_0\,
      I1 => \^xls\(21),
      I2 => \SRDHM_overflow0_inferred__0/i__carry__1_n_1\,
      I3 => \^xls\(22),
      I4 => \^xls\(16),
      I5 => \^xls\(17),
      O => \i__carry__6_i_8_n_0\
    );
\i__carry__6_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \i__carry__6_i_11_n_0\,
      I1 => \i__carry__6_i_12_n_0\,
      I2 => \^xls\(5),
      I3 => \^xls\(4),
      I4 => \^xls\(3),
      I5 => \^xls\(0),
      O => \i__carry__6_i_9_n_0\
    );
\i__carry_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^xls\(9),
      I1 => Q(9),
      I2 => \^xls\(10),
      I3 => Q(10),
      I4 => Q(11),
      I5 => \^xls\(11),
      O => \i__carry_i_1_n_0\
    );
\i__carry_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \i__carry_i_13_n_0\,
      I1 => \i__carry_i_14_n_0\,
      I2 => \^xls\(3),
      I3 => \^xls\(2),
      I4 => \i__carry_i_15_n_0\,
      I5 => \i__carry_i_16_n_0\,
      O => \i__carry_i_10_n_0\
    );
\i__carry_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => \^xls\(4),
      I1 => \^xls\(0),
      I2 => \^xls\(10),
      I3 => \^xls\(25),
      I4 => \^xls\(9),
      I5 => \^xls\(31),
      O => \i__carry_i_11_n_0\
    );
\i__carry_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^xls\(24),
      I1 => \^xls\(13),
      I2 => \^xls\(30),
      I3 => \^xls\(11),
      O => \i__carry_i_12_n_0\
    );
\i__carry_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^xls\(28),
      I1 => \^xls\(26),
      I2 => \^xls\(29),
      I3 => \^xls\(5),
      O => \i__carry_i_13_n_0\
    );
\i__carry_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^xls\(6),
      I1 => \^xls\(7),
      O => \i__carry_i_14_n_0\
    );
\i__carry_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^xls\(20),
      I1 => \^xls\(14),
      I2 => \^xls\(17),
      I3 => \^xls\(1),
      O => \i__carry_i_15_n_0\
    );
\i__carry_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \SRDHM_overflow0_inferred__0/i__carry__1_n_1\,
      I1 => \^xls\(22),
      I2 => \^xls\(27),
      I3 => \^xls\(16),
      O => \i__carry_i_16_n_0\
    );
\i__carry_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^xls\(8),
      I1 => Q(8),
      I2 => \^xls\(6),
      I3 => Q(6),
      I4 => Q(7),
      I5 => \^xls\(7),
      O => \i__carry_i_2_n_0\
    );
\i__carry_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^xls\(3),
      I1 => Q(3),
      I2 => \^xls\(4),
      I3 => Q(4),
      I4 => Q(5),
      I5 => \^xls\(5),
      O => \i__carry_i_3_n_0\
    );
\i__carry_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^xls\(0),
      I1 => Q(0),
      I2 => \^xls\(1),
      I3 => Q(1),
      I4 => Q(2),
      I5 => \^xls\(2),
      O => \i__carry_i_4_n_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9999999A"
    )
        port map (
      I0 => \i__carry_i_1__1\(0),
      I1 => \^slv_reg1_reg[11]\,
      I2 => \^slv_reg1_reg[23]\,
      I3 => ab_nudge(30),
      I4 => ab_nudge(31),
      O => sg_ab_64_p3_reg(0)
    );
\i__carry_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \i__carry_i_9_n_0\,
      I1 => \^xls\(21),
      I2 => \^xls\(15),
      I3 => \^xls\(19),
      I4 => \i__carry_i_10_n_0\,
      I5 => \i__carry_i_11_n_0\,
      O => \^slv_reg1_reg[23]\
    );
\i__carry_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^xls\(12),
      I1 => \^xls\(23),
      I2 => \^xls\(8),
      I3 => \^xls\(18),
      I4 => \i__carry_i_12_n_0\,
      O => \i__carry_i_9_n_0\
    );
sg_xowb_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => sg_xowb_carry_n_0,
      CO(2) => sg_xowb_carry_n_1,
      CO(1) => sg_xowb_carry_n_2,
      CO(0) => sg_xowb_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => \sg_a_p0_reg[31]\(3 downto 0),
      O(3 downto 0) => \^xls\(3 downto 0),
      S(3) => sg_xowb_carry_i_1_n_0,
      S(2) => sg_xowb_carry_i_2_n_0,
      S(1) => sg_xowb_carry_i_3_n_0,
      S(0) => sg_xowb_carry_i_4_n_0
    );
\sg_xowb_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => sg_xowb_carry_n_0,
      CO(3) => \sg_xowb_carry__0_n_0\,
      CO(2) => \sg_xowb_carry__0_n_1\,
      CO(1) => \sg_xowb_carry__0_n_2\,
      CO(0) => \sg_xowb_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \sg_a_p0_reg[31]\(7 downto 4),
      O(3 downto 0) => \^xls\(7 downto 4),
      S(3) => \sg_xowb_carry__0_i_1_n_0\,
      S(2) => \sg_xowb_carry__0_i_2_n_0\,
      S(1) => \sg_xowb_carry__0_i_3_n_0\,
      S(0) => \sg_xowb_carry__0_i_4_n_0\
    );
\sg_xowb_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(7),
      I1 => \sg_a_p0_reg[27]\(7),
      O => \sg_xowb_carry__0_i_1_n_0\
    );
\sg_xowb_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(6),
      I1 => \sg_a_p0_reg[27]\(6),
      O => \sg_xowb_carry__0_i_2_n_0\
    );
\sg_xowb_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(5),
      I1 => \sg_a_p0_reg[27]\(5),
      O => \sg_xowb_carry__0_i_3_n_0\
    );
\sg_xowb_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(4),
      I1 => \sg_a_p0_reg[27]\(4),
      O => \sg_xowb_carry__0_i_4_n_0\
    );
\sg_xowb_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \sg_xowb_carry__0_n_0\,
      CO(3) => \sg_xowb_carry__1_n_0\,
      CO(2) => \sg_xowb_carry__1_n_1\,
      CO(1) => \sg_xowb_carry__1_n_2\,
      CO(0) => \sg_xowb_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \sg_a_p0_reg[31]\(11 downto 8),
      O(3 downto 0) => \^xls\(11 downto 8),
      S(3) => \sg_xowb_carry__1_i_1_n_0\,
      S(2) => \sg_xowb_carry__1_i_2_n_0\,
      S(1) => \sg_xowb_carry__1_i_3_n_0\,
      S(0) => \sg_xowb_carry__1_i_4_n_0\
    );
\sg_xowb_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(11),
      I1 => \sg_a_p0_reg[27]\(11),
      O => \sg_xowb_carry__1_i_1_n_0\
    );
\sg_xowb_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(10),
      I1 => \sg_a_p0_reg[27]\(10),
      O => \sg_xowb_carry__1_i_2_n_0\
    );
\sg_xowb_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(9),
      I1 => \sg_a_p0_reg[27]\(9),
      O => \sg_xowb_carry__1_i_3_n_0\
    );
\sg_xowb_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(8),
      I1 => \sg_a_p0_reg[27]\(8),
      O => \sg_xowb_carry__1_i_4_n_0\
    );
\sg_xowb_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \sg_xowb_carry__1_n_0\,
      CO(3) => \sg_xowb_carry__2_n_0\,
      CO(2) => \sg_xowb_carry__2_n_1\,
      CO(1) => \sg_xowb_carry__2_n_2\,
      CO(0) => \sg_xowb_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \sg_a_p0_reg[31]\(15 downto 12),
      O(3 downto 0) => \^xls\(15 downto 12),
      S(3) => \sg_xowb_carry__2_i_1_n_0\,
      S(2) => \sg_xowb_carry__2_i_2_n_0\,
      S(1) => \sg_xowb_carry__2_i_3_n_0\,
      S(0) => \sg_xowb_carry__2_i_4_n_0\
    );
\sg_xowb_carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(15),
      I1 => \sg_a_p0_reg[27]\(15),
      O => \sg_xowb_carry__2_i_1_n_0\
    );
\sg_xowb_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(14),
      I1 => \sg_a_p0_reg[27]\(14),
      O => \sg_xowb_carry__2_i_2_n_0\
    );
\sg_xowb_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(13),
      I1 => \sg_a_p0_reg[27]\(13),
      O => \sg_xowb_carry__2_i_3_n_0\
    );
\sg_xowb_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(12),
      I1 => \sg_a_p0_reg[27]\(12),
      O => \sg_xowb_carry__2_i_4_n_0\
    );
\sg_xowb_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \sg_xowb_carry__2_n_0\,
      CO(3) => \sg_xowb_carry__3_n_0\,
      CO(2) => \sg_xowb_carry__3_n_1\,
      CO(1) => \sg_xowb_carry__3_n_2\,
      CO(0) => \sg_xowb_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \sg_a_p0_reg[31]\(19 downto 16),
      O(3 downto 0) => \^xls\(19 downto 16),
      S(3) => \sg_xowb_carry__3_i_1_n_0\,
      S(2) => \sg_xowb_carry__3_i_2_n_0\,
      S(1) => \sg_xowb_carry__3_i_3_n_0\,
      S(0) => \sg_xowb_carry__3_i_4_n_0\
    );
\sg_xowb_carry__3_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(19),
      I1 => \sg_a_p0_reg[27]\(19),
      O => \sg_xowb_carry__3_i_1_n_0\
    );
\sg_xowb_carry__3_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(18),
      I1 => \sg_a_p0_reg[27]\(18),
      O => \sg_xowb_carry__3_i_2_n_0\
    );
\sg_xowb_carry__3_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(17),
      I1 => \sg_a_p0_reg[27]\(17),
      O => \sg_xowb_carry__3_i_3_n_0\
    );
\sg_xowb_carry__3_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(16),
      I1 => \sg_a_p0_reg[27]\(16),
      O => \sg_xowb_carry__3_i_4_n_0\
    );
\sg_xowb_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \sg_xowb_carry__3_n_0\,
      CO(3) => \sg_xowb_carry__4_n_0\,
      CO(2) => \sg_xowb_carry__4_n_1\,
      CO(1) => \sg_xowb_carry__4_n_2\,
      CO(0) => \sg_xowb_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \sg_a_p0_reg[31]\(23 downto 20),
      O(3 downto 0) => \^xls\(23 downto 20),
      S(3) => \sg_xowb_carry__4_i_1_n_0\,
      S(2) => \sg_xowb_carry__4_i_2_n_0\,
      S(1) => \sg_xowb_carry__4_i_3_n_0\,
      S(0) => \sg_xowb_carry__4_i_4_n_0\
    );
\sg_xowb_carry__4_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(23),
      I1 => \sg_a_p0_reg[27]\(23),
      O => \sg_xowb_carry__4_i_1_n_0\
    );
\sg_xowb_carry__4_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(22),
      I1 => \sg_a_p0_reg[27]\(22),
      O => \sg_xowb_carry__4_i_2_n_0\
    );
\sg_xowb_carry__4_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(21),
      I1 => \sg_a_p0_reg[27]\(21),
      O => \sg_xowb_carry__4_i_3_n_0\
    );
\sg_xowb_carry__4_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(20),
      I1 => \sg_a_p0_reg[27]\(20),
      O => \sg_xowb_carry__4_i_4_n_0\
    );
\sg_xowb_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \sg_xowb_carry__4_n_0\,
      CO(3) => \sg_xowb_carry__5_n_0\,
      CO(2) => \sg_xowb_carry__5_n_1\,
      CO(1) => \sg_xowb_carry__5_n_2\,
      CO(0) => \sg_xowb_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \sg_a_p0_reg[31]\(27 downto 24),
      O(3 downto 0) => \^xls\(27 downto 24),
      S(3) => \sg_xowb_carry__5_i_1_n_0\,
      S(2) => \sg_xowb_carry__5_i_2_n_0\,
      S(1) => \sg_xowb_carry__5_i_3_n_0\,
      S(0) => \sg_xowb_carry__5_i_4_n_0\
    );
\sg_xowb_carry__5_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(27),
      I1 => \sg_a_p0_reg[27]\(27),
      O => \sg_xowb_carry__5_i_1_n_0\
    );
\sg_xowb_carry__5_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(26),
      I1 => \sg_a_p0_reg[27]\(26),
      O => \sg_xowb_carry__5_i_2_n_0\
    );
\sg_xowb_carry__5_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(25),
      I1 => \sg_a_p0_reg[27]\(25),
      O => \sg_xowb_carry__5_i_3_n_0\
    );
\sg_xowb_carry__5_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(24),
      I1 => \sg_a_p0_reg[27]\(24),
      O => \sg_xowb_carry__5_i_4_n_0\
    );
\sg_xowb_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \sg_xowb_carry__5_n_0\,
      CO(3) => \NLW_sg_xowb_carry__6_CO_UNCONNECTED\(3),
      CO(2) => \sg_xowb_carry__6_n_1\,
      CO(1) => \sg_xowb_carry__6_n_2\,
      CO(0) => \sg_xowb_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \sg_a_p0_reg[31]\(30 downto 28),
      O(3 downto 0) => \^xls\(31 downto 28),
      S(3 downto 0) => S(3 downto 0)
    );
sg_xowb_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(3),
      I1 => \sg_a_p0_reg[27]\(3),
      O => sg_xowb_carry_i_1_n_0
    );
sg_xowb_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(2),
      I1 => \sg_a_p0_reg[27]\(2),
      O => sg_xowb_carry_i_2_n_0
    );
sg_xowb_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(1),
      I1 => \sg_a_p0_reg[27]\(1),
      O => sg_xowb_carry_i_3_n_0
    );
sg_xowb_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]\(0),
      I1 => \sg_a_p0_reg[27]\(0),
      O => sg_xowb_carry_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fer_soc_bd_tflite_mbqm_0_0_tflite_core1 is
  port (
    sg_ab_64_p3_reg_0 : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \sg_ab_64_p3_reg[14]__0_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sg_ab_64_p3_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    xls : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \sg_a_p0_reg[31]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \sg_a_p0_reg[31]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end fer_soc_bd_tflite_mbqm_0_0_tflite_core1;

architecture STRUCTURE of fer_soc_bd_tflite_mbqm_0_0_tflite_core1 is
  signal sg_a_p0 : STD_LOGIC_VECTOR ( 31 downto 17 );
  signal sg_a_p1 : STD_LOGIC_VECTOR ( 31 downto 17 );
  signal sg_a_p2 : STD_LOGIC_VECTOR ( 31 downto 17 );
  signal sg_ab_64_p0_reg_n_100 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_101 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_102 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_103 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_104 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_105 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_106 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_107 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_108 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_109 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_110 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_111 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_112 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_113 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_114 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_115 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_116 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_117 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_118 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_119 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_120 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_121 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_122 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_123 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_124 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_125 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_126 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_127 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_128 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_129 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_130 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_131 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_132 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_133 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_134 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_135 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_136 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_137 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_138 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_139 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_140 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_141 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_142 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_143 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_144 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_145 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_146 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_147 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_148 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_149 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_150 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_151 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_152 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_153 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_24 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_25 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_26 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_27 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_28 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_29 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_30 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_31 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_32 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_33 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_34 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_35 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_36 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_37 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_38 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_39 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_40 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_41 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_42 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_43 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_44 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_45 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_46 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_47 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_48 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_49 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_50 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_51 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_52 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_53 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_58 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_59 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_60 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_61 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_62 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_63 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_64 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_65 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_66 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_67 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_68 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_69 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_70 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_71 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_72 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_73 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_74 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_75 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_76 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_77 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_78 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_79 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_80 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_81 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_82 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_83 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_84 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_85 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_86 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_87 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_88 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_89 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_90 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_91 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_92 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_93 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_94 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_95 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_96 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_97 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_98 : STD_LOGIC;
  signal sg_ab_64_p0_reg_n_99 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_106 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_107 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_108 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_109 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_110 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_111 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_112 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_113 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_114 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_115 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_116 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_117 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_118 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_119 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_120 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_121 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_122 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_123 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_124 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_125 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_126 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_127 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_128 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_129 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_130 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_131 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_132 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_133 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_134 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_135 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_136 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_137 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_138 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_139 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_140 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_141 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_142 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_143 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_144 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_145 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_146 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_147 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_148 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_149 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_150 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_151 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_152 : STD_LOGIC;
  signal sg_ab_64_p1_reg_n_153 : STD_LOGIC;
  signal \sg_ab_64_p2_reg[0]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[10]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[11]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[12]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[13]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[14]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[15]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[16]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[1]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[2]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[3]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[4]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[5]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[6]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[7]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[8]_srl2_n_0\ : STD_LOGIC;
  signal \sg_ab_64_p2_reg[9]_srl2_n_0\ : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_100 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_101 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_102 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_103 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_104 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_105 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_106 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_107 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_108 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_109 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_110 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_111 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_112 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_113 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_114 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_115 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_116 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_117 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_118 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_119 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_120 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_121 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_122 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_123 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_124 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_125 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_126 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_127 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_128 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_129 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_130 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_131 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_132 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_133 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_134 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_135 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_136 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_137 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_138 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_139 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_140 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_141 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_142 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_143 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_144 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_145 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_146 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_147 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_148 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_149 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_150 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_151 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_152 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_153 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_58 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_59 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_60 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_61 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_62 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_63 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_64 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_65 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_66 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_67 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_68 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_69 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_70 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_71 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_72 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_73 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_74 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_75 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_76 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_77 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_78 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_79 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_80 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_81 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_82 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_83 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_84 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_85 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_86 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_87 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_88 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_89 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_90 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_91 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_92 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_93 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_94 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_95 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_96 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_97 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_98 : STD_LOGIC;
  signal sg_ab_64_p2_reg_n_99 : STD_LOGIC;
  signal \^sg_ab_64_p3_reg_0\ : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal sg_ab_64_p3_reg_n_58 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_59 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_60 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_61 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_62 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_63 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_64 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_65 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_66 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_67 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_68 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_69 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_70 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_71 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_72 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_73 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_74 : STD_LOGIC;
  signal sg_ab_64_p3_reg_n_75 : STD_LOGIC;
  signal sg_b_p0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal sg_b_p1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal sg_b_p2 : STD_LOGIC_VECTOR ( 31 downto 17 );
  signal NLW_sg_ab_64_p0_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p0_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p0_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p0_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p0_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p0_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p0_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_sg_ab_64_p0_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_sg_ab_64_p1_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p1_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p1_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p1_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p1_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p1_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p1_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_sg_ab_64_p1_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_sg_ab_64_p1_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_sg_ab_64_p1_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_sg_ab_64_p2_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p2_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p2_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p2_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p2_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p2_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p2_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_sg_ab_64_p2_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_sg_ab_64_p2_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_sg_ab_64_p3_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p3_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p3_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p3_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p3_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p3_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_sg_ab_64_p3_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_sg_ab_64_p3_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_sg_ab_64_p3_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_sg_ab_64_p3_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  attribute srl_bus_name : string;
  attribute srl_bus_name of \sg_ab_64_p2_reg[0]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name : string;
  attribute srl_name of \sg_ab_64_p2_reg[0]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[0]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[10]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[10]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[10]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[11]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[11]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[11]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[12]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[12]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[12]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[13]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[13]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[13]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[14]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[14]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[14]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[15]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[15]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[15]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[16]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[16]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[16]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[1]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[1]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[1]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[2]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[2]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[2]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[3]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[3]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[3]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[4]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[4]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[4]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[5]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[5]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[5]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[6]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[6]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[6]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[7]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[7]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[7]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[8]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[8]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[8]_srl2 ";
  attribute srl_bus_name of \sg_ab_64_p2_reg[9]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg ";
  attribute srl_name of \sg_ab_64_p2_reg[9]_srl2\ : label is "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[9]_srl2 ";
begin
  sg_ab_64_p3_reg_0(63 downto 0) <= \^sg_ab_64_p3_reg_0\(63 downto 0);
\ab_nudge_carry__6_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^sg_ab_64_p3_reg_0\(31),
      I1 => \^sg_ab_64_p3_reg_0\(63),
      O => \sg_ab_64_p3_reg[14]__0_0\(1)
    );
\ab_nudge_carry__6_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^sg_ab_64_p3_reg_0\(30),
      O => \sg_ab_64_p3_reg[14]__0_0\(0)
    );
ab_nudge_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^sg_ab_64_p3_reg_0\(0),
      I1 => \^sg_ab_64_p3_reg_0\(63),
      O => \sg_ab_64_p3_reg[0]_0\(0)
    );
\sg_a_p0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(17),
      Q => sg_a_p0(17),
      R => '0'
    );
\sg_a_p0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(18),
      Q => sg_a_p0(18),
      R => '0'
    );
\sg_a_p0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(19),
      Q => sg_a_p0(19),
      R => '0'
    );
\sg_a_p0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(20),
      Q => sg_a_p0(20),
      R => '0'
    );
\sg_a_p0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(21),
      Q => sg_a_p0(21),
      R => '0'
    );
\sg_a_p0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(22),
      Q => sg_a_p0(22),
      R => '0'
    );
\sg_a_p0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(23),
      Q => sg_a_p0(23),
      R => '0'
    );
\sg_a_p0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(24),
      Q => sg_a_p0(24),
      R => '0'
    );
\sg_a_p0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(25),
      Q => sg_a_p0(25),
      R => '0'
    );
\sg_a_p0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(26),
      Q => sg_a_p0(26),
      R => '0'
    );
\sg_a_p0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(27),
      Q => sg_a_p0(27),
      R => '0'
    );
\sg_a_p0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(28),
      Q => sg_a_p0(28),
      R => '0'
    );
\sg_a_p0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(29),
      Q => sg_a_p0(29),
      R => '0'
    );
\sg_a_p0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(30),
      Q => sg_a_p0(30),
      R => '0'
    );
\sg_a_p0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => xls(31),
      Q => sg_a_p0(31),
      R => '0'
    );
\sg_a_p1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(17),
      Q => sg_a_p1(17),
      R => '0'
    );
\sg_a_p1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(18),
      Q => sg_a_p1(18),
      R => '0'
    );
\sg_a_p1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(19),
      Q => sg_a_p1(19),
      R => '0'
    );
\sg_a_p1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(20),
      Q => sg_a_p1(20),
      R => '0'
    );
\sg_a_p1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(21),
      Q => sg_a_p1(21),
      R => '0'
    );
\sg_a_p1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(22),
      Q => sg_a_p1(22),
      R => '0'
    );
\sg_a_p1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(23),
      Q => sg_a_p1(23),
      R => '0'
    );
\sg_a_p1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(24),
      Q => sg_a_p1(24),
      R => '0'
    );
\sg_a_p1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(25),
      Q => sg_a_p1(25),
      R => '0'
    );
\sg_a_p1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(26),
      Q => sg_a_p1(26),
      R => '0'
    );
\sg_a_p1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(27),
      Q => sg_a_p1(27),
      R => '0'
    );
\sg_a_p1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(28),
      Q => sg_a_p1(28),
      R => '0'
    );
\sg_a_p1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(29),
      Q => sg_a_p1(29),
      R => '0'
    );
\sg_a_p1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(30),
      Q => sg_a_p1(30),
      R => '0'
    );
\sg_a_p1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p0(31),
      Q => sg_a_p1(31),
      R => '0'
    );
\sg_a_p2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(17),
      Q => sg_a_p2(17),
      R => '0'
    );
\sg_a_p2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(18),
      Q => sg_a_p2(18),
      R => '0'
    );
\sg_a_p2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(19),
      Q => sg_a_p2(19),
      R => '0'
    );
\sg_a_p2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(20),
      Q => sg_a_p2(20),
      R => '0'
    );
\sg_a_p2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(21),
      Q => sg_a_p2(21),
      R => '0'
    );
\sg_a_p2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(22),
      Q => sg_a_p2(22),
      R => '0'
    );
\sg_a_p2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(23),
      Q => sg_a_p2(23),
      R => '0'
    );
\sg_a_p2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(24),
      Q => sg_a_p2(24),
      R => '0'
    );
\sg_a_p2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(25),
      Q => sg_a_p2(25),
      R => '0'
    );
\sg_a_p2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(26),
      Q => sg_a_p2(26),
      R => '0'
    );
\sg_a_p2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(27),
      Q => sg_a_p2(27),
      R => '0'
    );
\sg_a_p2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(28),
      Q => sg_a_p2(28),
      R => '0'
    );
\sg_a_p2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(29),
      Q => sg_a_p2(29),
      R => '0'
    );
\sg_a_p2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(30),
      Q => sg_a_p2(30),
      R => '0'
    );
\sg_a_p2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_a_p1(31),
      Q => sg_a_p2(31),
      R => '0'
    );
sg_ab_64_p0_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 1,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 1,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 17) => B"0000000000000",
      A(16 downto 0) => xls(16 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29) => sg_ab_64_p0_reg_n_24,
      ACOUT(28) => sg_ab_64_p0_reg_n_25,
      ACOUT(27) => sg_ab_64_p0_reg_n_26,
      ACOUT(26) => sg_ab_64_p0_reg_n_27,
      ACOUT(25) => sg_ab_64_p0_reg_n_28,
      ACOUT(24) => sg_ab_64_p0_reg_n_29,
      ACOUT(23) => sg_ab_64_p0_reg_n_30,
      ACOUT(22) => sg_ab_64_p0_reg_n_31,
      ACOUT(21) => sg_ab_64_p0_reg_n_32,
      ACOUT(20) => sg_ab_64_p0_reg_n_33,
      ACOUT(19) => sg_ab_64_p0_reg_n_34,
      ACOUT(18) => sg_ab_64_p0_reg_n_35,
      ACOUT(17) => sg_ab_64_p0_reg_n_36,
      ACOUT(16) => sg_ab_64_p0_reg_n_37,
      ACOUT(15) => sg_ab_64_p0_reg_n_38,
      ACOUT(14) => sg_ab_64_p0_reg_n_39,
      ACOUT(13) => sg_ab_64_p0_reg_n_40,
      ACOUT(12) => sg_ab_64_p0_reg_n_41,
      ACOUT(11) => sg_ab_64_p0_reg_n_42,
      ACOUT(10) => sg_ab_64_p0_reg_n_43,
      ACOUT(9) => sg_ab_64_p0_reg_n_44,
      ACOUT(8) => sg_ab_64_p0_reg_n_45,
      ACOUT(7) => sg_ab_64_p0_reg_n_46,
      ACOUT(6) => sg_ab_64_p0_reg_n_47,
      ACOUT(5) => sg_ab_64_p0_reg_n_48,
      ACOUT(4) => sg_ab_64_p0_reg_n_49,
      ACOUT(3) => sg_ab_64_p0_reg_n_50,
      ACOUT(2) => sg_ab_64_p0_reg_n_51,
      ACOUT(1) => sg_ab_64_p0_reg_n_52,
      ACOUT(0) => sg_ab_64_p0_reg_n_53,
      ALUMODE(3 downto 0) => B"0000",
      B(17) => '0',
      B(16 downto 0) => Q(16 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_sg_ab_64_p0_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_sg_ab_64_p0_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_sg_ab_64_p0_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '1',
      CEA2 => '1',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => '1',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '1',
      CEP => '1',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_sg_ab_64_p0_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_sg_ab_64_p0_reg_OVERFLOW_UNCONNECTED,
      P(47) => sg_ab_64_p0_reg_n_58,
      P(46) => sg_ab_64_p0_reg_n_59,
      P(45) => sg_ab_64_p0_reg_n_60,
      P(44) => sg_ab_64_p0_reg_n_61,
      P(43) => sg_ab_64_p0_reg_n_62,
      P(42) => sg_ab_64_p0_reg_n_63,
      P(41) => sg_ab_64_p0_reg_n_64,
      P(40) => sg_ab_64_p0_reg_n_65,
      P(39) => sg_ab_64_p0_reg_n_66,
      P(38) => sg_ab_64_p0_reg_n_67,
      P(37) => sg_ab_64_p0_reg_n_68,
      P(36) => sg_ab_64_p0_reg_n_69,
      P(35) => sg_ab_64_p0_reg_n_70,
      P(34) => sg_ab_64_p0_reg_n_71,
      P(33) => sg_ab_64_p0_reg_n_72,
      P(32) => sg_ab_64_p0_reg_n_73,
      P(31) => sg_ab_64_p0_reg_n_74,
      P(30) => sg_ab_64_p0_reg_n_75,
      P(29) => sg_ab_64_p0_reg_n_76,
      P(28) => sg_ab_64_p0_reg_n_77,
      P(27) => sg_ab_64_p0_reg_n_78,
      P(26) => sg_ab_64_p0_reg_n_79,
      P(25) => sg_ab_64_p0_reg_n_80,
      P(24) => sg_ab_64_p0_reg_n_81,
      P(23) => sg_ab_64_p0_reg_n_82,
      P(22) => sg_ab_64_p0_reg_n_83,
      P(21) => sg_ab_64_p0_reg_n_84,
      P(20) => sg_ab_64_p0_reg_n_85,
      P(19) => sg_ab_64_p0_reg_n_86,
      P(18) => sg_ab_64_p0_reg_n_87,
      P(17) => sg_ab_64_p0_reg_n_88,
      P(16) => sg_ab_64_p0_reg_n_89,
      P(15) => sg_ab_64_p0_reg_n_90,
      P(14) => sg_ab_64_p0_reg_n_91,
      P(13) => sg_ab_64_p0_reg_n_92,
      P(12) => sg_ab_64_p0_reg_n_93,
      P(11) => sg_ab_64_p0_reg_n_94,
      P(10) => sg_ab_64_p0_reg_n_95,
      P(9) => sg_ab_64_p0_reg_n_96,
      P(8) => sg_ab_64_p0_reg_n_97,
      P(7) => sg_ab_64_p0_reg_n_98,
      P(6) => sg_ab_64_p0_reg_n_99,
      P(5) => sg_ab_64_p0_reg_n_100,
      P(4) => sg_ab_64_p0_reg_n_101,
      P(3) => sg_ab_64_p0_reg_n_102,
      P(2) => sg_ab_64_p0_reg_n_103,
      P(1) => sg_ab_64_p0_reg_n_104,
      P(0) => sg_ab_64_p0_reg_n_105,
      PATTERNBDETECT => NLW_sg_ab_64_p0_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_sg_ab_64_p0_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47) => sg_ab_64_p0_reg_n_106,
      PCOUT(46) => sg_ab_64_p0_reg_n_107,
      PCOUT(45) => sg_ab_64_p0_reg_n_108,
      PCOUT(44) => sg_ab_64_p0_reg_n_109,
      PCOUT(43) => sg_ab_64_p0_reg_n_110,
      PCOUT(42) => sg_ab_64_p0_reg_n_111,
      PCOUT(41) => sg_ab_64_p0_reg_n_112,
      PCOUT(40) => sg_ab_64_p0_reg_n_113,
      PCOUT(39) => sg_ab_64_p0_reg_n_114,
      PCOUT(38) => sg_ab_64_p0_reg_n_115,
      PCOUT(37) => sg_ab_64_p0_reg_n_116,
      PCOUT(36) => sg_ab_64_p0_reg_n_117,
      PCOUT(35) => sg_ab_64_p0_reg_n_118,
      PCOUT(34) => sg_ab_64_p0_reg_n_119,
      PCOUT(33) => sg_ab_64_p0_reg_n_120,
      PCOUT(32) => sg_ab_64_p0_reg_n_121,
      PCOUT(31) => sg_ab_64_p0_reg_n_122,
      PCOUT(30) => sg_ab_64_p0_reg_n_123,
      PCOUT(29) => sg_ab_64_p0_reg_n_124,
      PCOUT(28) => sg_ab_64_p0_reg_n_125,
      PCOUT(27) => sg_ab_64_p0_reg_n_126,
      PCOUT(26) => sg_ab_64_p0_reg_n_127,
      PCOUT(25) => sg_ab_64_p0_reg_n_128,
      PCOUT(24) => sg_ab_64_p0_reg_n_129,
      PCOUT(23) => sg_ab_64_p0_reg_n_130,
      PCOUT(22) => sg_ab_64_p0_reg_n_131,
      PCOUT(21) => sg_ab_64_p0_reg_n_132,
      PCOUT(20) => sg_ab_64_p0_reg_n_133,
      PCOUT(19) => sg_ab_64_p0_reg_n_134,
      PCOUT(18) => sg_ab_64_p0_reg_n_135,
      PCOUT(17) => sg_ab_64_p0_reg_n_136,
      PCOUT(16) => sg_ab_64_p0_reg_n_137,
      PCOUT(15) => sg_ab_64_p0_reg_n_138,
      PCOUT(14) => sg_ab_64_p0_reg_n_139,
      PCOUT(13) => sg_ab_64_p0_reg_n_140,
      PCOUT(12) => sg_ab_64_p0_reg_n_141,
      PCOUT(11) => sg_ab_64_p0_reg_n_142,
      PCOUT(10) => sg_ab_64_p0_reg_n_143,
      PCOUT(9) => sg_ab_64_p0_reg_n_144,
      PCOUT(8) => sg_ab_64_p0_reg_n_145,
      PCOUT(7) => sg_ab_64_p0_reg_n_146,
      PCOUT(6) => sg_ab_64_p0_reg_n_147,
      PCOUT(5) => sg_ab_64_p0_reg_n_148,
      PCOUT(4) => sg_ab_64_p0_reg_n_149,
      PCOUT(3) => sg_ab_64_p0_reg_n_150,
      PCOUT(2) => sg_ab_64_p0_reg_n_151,
      PCOUT(1) => sg_ab_64_p0_reg_n_152,
      PCOUT(0) => sg_ab_64_p0_reg_n_153,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_sg_ab_64_p0_reg_UNDERFLOW_UNCONNECTED
    );
sg_ab_64_p1_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "CASCADE",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 1,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 0) => B"000000000000000000000000000000",
      ACIN(29) => sg_ab_64_p0_reg_n_24,
      ACIN(28) => sg_ab_64_p0_reg_n_25,
      ACIN(27) => sg_ab_64_p0_reg_n_26,
      ACIN(26) => sg_ab_64_p0_reg_n_27,
      ACIN(25) => sg_ab_64_p0_reg_n_28,
      ACIN(24) => sg_ab_64_p0_reg_n_29,
      ACIN(23) => sg_ab_64_p0_reg_n_30,
      ACIN(22) => sg_ab_64_p0_reg_n_31,
      ACIN(21) => sg_ab_64_p0_reg_n_32,
      ACIN(20) => sg_ab_64_p0_reg_n_33,
      ACIN(19) => sg_ab_64_p0_reg_n_34,
      ACIN(18) => sg_ab_64_p0_reg_n_35,
      ACIN(17) => sg_ab_64_p0_reg_n_36,
      ACIN(16) => sg_ab_64_p0_reg_n_37,
      ACIN(15) => sg_ab_64_p0_reg_n_38,
      ACIN(14) => sg_ab_64_p0_reg_n_39,
      ACIN(13) => sg_ab_64_p0_reg_n_40,
      ACIN(12) => sg_ab_64_p0_reg_n_41,
      ACIN(11) => sg_ab_64_p0_reg_n_42,
      ACIN(10) => sg_ab_64_p0_reg_n_43,
      ACIN(9) => sg_ab_64_p0_reg_n_44,
      ACIN(8) => sg_ab_64_p0_reg_n_45,
      ACIN(7) => sg_ab_64_p0_reg_n_46,
      ACIN(6) => sg_ab_64_p0_reg_n_47,
      ACIN(5) => sg_ab_64_p0_reg_n_48,
      ACIN(4) => sg_ab_64_p0_reg_n_49,
      ACIN(3) => sg_ab_64_p0_reg_n_50,
      ACIN(2) => sg_ab_64_p0_reg_n_51,
      ACIN(1) => sg_ab_64_p0_reg_n_52,
      ACIN(0) => sg_ab_64_p0_reg_n_53,
      ACOUT(29 downto 0) => NLW_sg_ab_64_p1_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => sg_b_p0(31),
      B(16) => sg_b_p0(31),
      B(15) => sg_b_p0(31),
      B(14 downto 0) => sg_b_p0(31 downto 17),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_sg_ab_64_p1_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_sg_ab_64_p1_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_sg_ab_64_p1_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '1',
      CEA2 => '1',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => '1',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '1',
      CEP => '1',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_sg_ab_64_p1_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"1010101",
      OVERFLOW => NLW_sg_ab_64_p1_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 0) => NLW_sg_ab_64_p1_reg_P_UNCONNECTED(47 downto 0),
      PATTERNBDETECT => NLW_sg_ab_64_p1_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_sg_ab_64_p1_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47) => sg_ab_64_p0_reg_n_106,
      PCIN(46) => sg_ab_64_p0_reg_n_107,
      PCIN(45) => sg_ab_64_p0_reg_n_108,
      PCIN(44) => sg_ab_64_p0_reg_n_109,
      PCIN(43) => sg_ab_64_p0_reg_n_110,
      PCIN(42) => sg_ab_64_p0_reg_n_111,
      PCIN(41) => sg_ab_64_p0_reg_n_112,
      PCIN(40) => sg_ab_64_p0_reg_n_113,
      PCIN(39) => sg_ab_64_p0_reg_n_114,
      PCIN(38) => sg_ab_64_p0_reg_n_115,
      PCIN(37) => sg_ab_64_p0_reg_n_116,
      PCIN(36) => sg_ab_64_p0_reg_n_117,
      PCIN(35) => sg_ab_64_p0_reg_n_118,
      PCIN(34) => sg_ab_64_p0_reg_n_119,
      PCIN(33) => sg_ab_64_p0_reg_n_120,
      PCIN(32) => sg_ab_64_p0_reg_n_121,
      PCIN(31) => sg_ab_64_p0_reg_n_122,
      PCIN(30) => sg_ab_64_p0_reg_n_123,
      PCIN(29) => sg_ab_64_p0_reg_n_124,
      PCIN(28) => sg_ab_64_p0_reg_n_125,
      PCIN(27) => sg_ab_64_p0_reg_n_126,
      PCIN(26) => sg_ab_64_p0_reg_n_127,
      PCIN(25) => sg_ab_64_p0_reg_n_128,
      PCIN(24) => sg_ab_64_p0_reg_n_129,
      PCIN(23) => sg_ab_64_p0_reg_n_130,
      PCIN(22) => sg_ab_64_p0_reg_n_131,
      PCIN(21) => sg_ab_64_p0_reg_n_132,
      PCIN(20) => sg_ab_64_p0_reg_n_133,
      PCIN(19) => sg_ab_64_p0_reg_n_134,
      PCIN(18) => sg_ab_64_p0_reg_n_135,
      PCIN(17) => sg_ab_64_p0_reg_n_136,
      PCIN(16) => sg_ab_64_p0_reg_n_137,
      PCIN(15) => sg_ab_64_p0_reg_n_138,
      PCIN(14) => sg_ab_64_p0_reg_n_139,
      PCIN(13) => sg_ab_64_p0_reg_n_140,
      PCIN(12) => sg_ab_64_p0_reg_n_141,
      PCIN(11) => sg_ab_64_p0_reg_n_142,
      PCIN(10) => sg_ab_64_p0_reg_n_143,
      PCIN(9) => sg_ab_64_p0_reg_n_144,
      PCIN(8) => sg_ab_64_p0_reg_n_145,
      PCIN(7) => sg_ab_64_p0_reg_n_146,
      PCIN(6) => sg_ab_64_p0_reg_n_147,
      PCIN(5) => sg_ab_64_p0_reg_n_148,
      PCIN(4) => sg_ab_64_p0_reg_n_149,
      PCIN(3) => sg_ab_64_p0_reg_n_150,
      PCIN(2) => sg_ab_64_p0_reg_n_151,
      PCIN(1) => sg_ab_64_p0_reg_n_152,
      PCIN(0) => sg_ab_64_p0_reg_n_153,
      PCOUT(47) => sg_ab_64_p1_reg_n_106,
      PCOUT(46) => sg_ab_64_p1_reg_n_107,
      PCOUT(45) => sg_ab_64_p1_reg_n_108,
      PCOUT(44) => sg_ab_64_p1_reg_n_109,
      PCOUT(43) => sg_ab_64_p1_reg_n_110,
      PCOUT(42) => sg_ab_64_p1_reg_n_111,
      PCOUT(41) => sg_ab_64_p1_reg_n_112,
      PCOUT(40) => sg_ab_64_p1_reg_n_113,
      PCOUT(39) => sg_ab_64_p1_reg_n_114,
      PCOUT(38) => sg_ab_64_p1_reg_n_115,
      PCOUT(37) => sg_ab_64_p1_reg_n_116,
      PCOUT(36) => sg_ab_64_p1_reg_n_117,
      PCOUT(35) => sg_ab_64_p1_reg_n_118,
      PCOUT(34) => sg_ab_64_p1_reg_n_119,
      PCOUT(33) => sg_ab_64_p1_reg_n_120,
      PCOUT(32) => sg_ab_64_p1_reg_n_121,
      PCOUT(31) => sg_ab_64_p1_reg_n_122,
      PCOUT(30) => sg_ab_64_p1_reg_n_123,
      PCOUT(29) => sg_ab_64_p1_reg_n_124,
      PCOUT(28) => sg_ab_64_p1_reg_n_125,
      PCOUT(27) => sg_ab_64_p1_reg_n_126,
      PCOUT(26) => sg_ab_64_p1_reg_n_127,
      PCOUT(25) => sg_ab_64_p1_reg_n_128,
      PCOUT(24) => sg_ab_64_p1_reg_n_129,
      PCOUT(23) => sg_ab_64_p1_reg_n_130,
      PCOUT(22) => sg_ab_64_p1_reg_n_131,
      PCOUT(21) => sg_ab_64_p1_reg_n_132,
      PCOUT(20) => sg_ab_64_p1_reg_n_133,
      PCOUT(19) => sg_ab_64_p1_reg_n_134,
      PCOUT(18) => sg_ab_64_p1_reg_n_135,
      PCOUT(17) => sg_ab_64_p1_reg_n_136,
      PCOUT(16) => sg_ab_64_p1_reg_n_137,
      PCOUT(15) => sg_ab_64_p1_reg_n_138,
      PCOUT(14) => sg_ab_64_p1_reg_n_139,
      PCOUT(13) => sg_ab_64_p1_reg_n_140,
      PCOUT(12) => sg_ab_64_p1_reg_n_141,
      PCOUT(11) => sg_ab_64_p1_reg_n_142,
      PCOUT(10) => sg_ab_64_p1_reg_n_143,
      PCOUT(9) => sg_ab_64_p1_reg_n_144,
      PCOUT(8) => sg_ab_64_p1_reg_n_145,
      PCOUT(7) => sg_ab_64_p1_reg_n_146,
      PCOUT(6) => sg_ab_64_p1_reg_n_147,
      PCOUT(5) => sg_ab_64_p1_reg_n_148,
      PCOUT(4) => sg_ab_64_p1_reg_n_149,
      PCOUT(3) => sg_ab_64_p1_reg_n_150,
      PCOUT(2) => sg_ab_64_p1_reg_n_151,
      PCOUT(1) => sg_ab_64_p1_reg_n_152,
      PCOUT(0) => sg_ab_64_p1_reg_n_153,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_sg_ab_64_p1_reg_UNDERFLOW_UNCONNECTED
    );
sg_ab_64_p2_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 1,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 17) => B"0000000000000",
      A(16 downto 0) => sg_b_p1(16 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_sg_ab_64_p2_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => sg_a_p1(31),
      B(16) => sg_a_p1(31),
      B(15) => sg_a_p1(31),
      B(14 downto 0) => sg_a_p1(31 downto 17),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_sg_ab_64_p2_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_sg_ab_64_p2_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_sg_ab_64_p2_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '1',
      CEA2 => '1',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => '1',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '1',
      CEP => '1',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_sg_ab_64_p2_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0010101",
      OVERFLOW => NLW_sg_ab_64_p2_reg_OVERFLOW_UNCONNECTED,
      P(47) => sg_ab_64_p2_reg_n_58,
      P(46) => sg_ab_64_p2_reg_n_59,
      P(45) => sg_ab_64_p2_reg_n_60,
      P(44) => sg_ab_64_p2_reg_n_61,
      P(43) => sg_ab_64_p2_reg_n_62,
      P(42) => sg_ab_64_p2_reg_n_63,
      P(41) => sg_ab_64_p2_reg_n_64,
      P(40) => sg_ab_64_p2_reg_n_65,
      P(39) => sg_ab_64_p2_reg_n_66,
      P(38) => sg_ab_64_p2_reg_n_67,
      P(37) => sg_ab_64_p2_reg_n_68,
      P(36) => sg_ab_64_p2_reg_n_69,
      P(35) => sg_ab_64_p2_reg_n_70,
      P(34) => sg_ab_64_p2_reg_n_71,
      P(33) => sg_ab_64_p2_reg_n_72,
      P(32) => sg_ab_64_p2_reg_n_73,
      P(31) => sg_ab_64_p2_reg_n_74,
      P(30) => sg_ab_64_p2_reg_n_75,
      P(29) => sg_ab_64_p2_reg_n_76,
      P(28) => sg_ab_64_p2_reg_n_77,
      P(27) => sg_ab_64_p2_reg_n_78,
      P(26) => sg_ab_64_p2_reg_n_79,
      P(25) => sg_ab_64_p2_reg_n_80,
      P(24) => sg_ab_64_p2_reg_n_81,
      P(23) => sg_ab_64_p2_reg_n_82,
      P(22) => sg_ab_64_p2_reg_n_83,
      P(21) => sg_ab_64_p2_reg_n_84,
      P(20) => sg_ab_64_p2_reg_n_85,
      P(19) => sg_ab_64_p2_reg_n_86,
      P(18) => sg_ab_64_p2_reg_n_87,
      P(17) => sg_ab_64_p2_reg_n_88,
      P(16) => sg_ab_64_p2_reg_n_89,
      P(15) => sg_ab_64_p2_reg_n_90,
      P(14) => sg_ab_64_p2_reg_n_91,
      P(13) => sg_ab_64_p2_reg_n_92,
      P(12) => sg_ab_64_p2_reg_n_93,
      P(11) => sg_ab_64_p2_reg_n_94,
      P(10) => sg_ab_64_p2_reg_n_95,
      P(9) => sg_ab_64_p2_reg_n_96,
      P(8) => sg_ab_64_p2_reg_n_97,
      P(7) => sg_ab_64_p2_reg_n_98,
      P(6) => sg_ab_64_p2_reg_n_99,
      P(5) => sg_ab_64_p2_reg_n_100,
      P(4) => sg_ab_64_p2_reg_n_101,
      P(3) => sg_ab_64_p2_reg_n_102,
      P(2) => sg_ab_64_p2_reg_n_103,
      P(1) => sg_ab_64_p2_reg_n_104,
      P(0) => sg_ab_64_p2_reg_n_105,
      PATTERNBDETECT => NLW_sg_ab_64_p2_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_sg_ab_64_p2_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47) => sg_ab_64_p1_reg_n_106,
      PCIN(46) => sg_ab_64_p1_reg_n_107,
      PCIN(45) => sg_ab_64_p1_reg_n_108,
      PCIN(44) => sg_ab_64_p1_reg_n_109,
      PCIN(43) => sg_ab_64_p1_reg_n_110,
      PCIN(42) => sg_ab_64_p1_reg_n_111,
      PCIN(41) => sg_ab_64_p1_reg_n_112,
      PCIN(40) => sg_ab_64_p1_reg_n_113,
      PCIN(39) => sg_ab_64_p1_reg_n_114,
      PCIN(38) => sg_ab_64_p1_reg_n_115,
      PCIN(37) => sg_ab_64_p1_reg_n_116,
      PCIN(36) => sg_ab_64_p1_reg_n_117,
      PCIN(35) => sg_ab_64_p1_reg_n_118,
      PCIN(34) => sg_ab_64_p1_reg_n_119,
      PCIN(33) => sg_ab_64_p1_reg_n_120,
      PCIN(32) => sg_ab_64_p1_reg_n_121,
      PCIN(31) => sg_ab_64_p1_reg_n_122,
      PCIN(30) => sg_ab_64_p1_reg_n_123,
      PCIN(29) => sg_ab_64_p1_reg_n_124,
      PCIN(28) => sg_ab_64_p1_reg_n_125,
      PCIN(27) => sg_ab_64_p1_reg_n_126,
      PCIN(26) => sg_ab_64_p1_reg_n_127,
      PCIN(25) => sg_ab_64_p1_reg_n_128,
      PCIN(24) => sg_ab_64_p1_reg_n_129,
      PCIN(23) => sg_ab_64_p1_reg_n_130,
      PCIN(22) => sg_ab_64_p1_reg_n_131,
      PCIN(21) => sg_ab_64_p1_reg_n_132,
      PCIN(20) => sg_ab_64_p1_reg_n_133,
      PCIN(19) => sg_ab_64_p1_reg_n_134,
      PCIN(18) => sg_ab_64_p1_reg_n_135,
      PCIN(17) => sg_ab_64_p1_reg_n_136,
      PCIN(16) => sg_ab_64_p1_reg_n_137,
      PCIN(15) => sg_ab_64_p1_reg_n_138,
      PCIN(14) => sg_ab_64_p1_reg_n_139,
      PCIN(13) => sg_ab_64_p1_reg_n_140,
      PCIN(12) => sg_ab_64_p1_reg_n_141,
      PCIN(11) => sg_ab_64_p1_reg_n_142,
      PCIN(10) => sg_ab_64_p1_reg_n_143,
      PCIN(9) => sg_ab_64_p1_reg_n_144,
      PCIN(8) => sg_ab_64_p1_reg_n_145,
      PCIN(7) => sg_ab_64_p1_reg_n_146,
      PCIN(6) => sg_ab_64_p1_reg_n_147,
      PCIN(5) => sg_ab_64_p1_reg_n_148,
      PCIN(4) => sg_ab_64_p1_reg_n_149,
      PCIN(3) => sg_ab_64_p1_reg_n_150,
      PCIN(2) => sg_ab_64_p1_reg_n_151,
      PCIN(1) => sg_ab_64_p1_reg_n_152,
      PCIN(0) => sg_ab_64_p1_reg_n_153,
      PCOUT(47) => sg_ab_64_p2_reg_n_106,
      PCOUT(46) => sg_ab_64_p2_reg_n_107,
      PCOUT(45) => sg_ab_64_p2_reg_n_108,
      PCOUT(44) => sg_ab_64_p2_reg_n_109,
      PCOUT(43) => sg_ab_64_p2_reg_n_110,
      PCOUT(42) => sg_ab_64_p2_reg_n_111,
      PCOUT(41) => sg_ab_64_p2_reg_n_112,
      PCOUT(40) => sg_ab_64_p2_reg_n_113,
      PCOUT(39) => sg_ab_64_p2_reg_n_114,
      PCOUT(38) => sg_ab_64_p2_reg_n_115,
      PCOUT(37) => sg_ab_64_p2_reg_n_116,
      PCOUT(36) => sg_ab_64_p2_reg_n_117,
      PCOUT(35) => sg_ab_64_p2_reg_n_118,
      PCOUT(34) => sg_ab_64_p2_reg_n_119,
      PCOUT(33) => sg_ab_64_p2_reg_n_120,
      PCOUT(32) => sg_ab_64_p2_reg_n_121,
      PCOUT(31) => sg_ab_64_p2_reg_n_122,
      PCOUT(30) => sg_ab_64_p2_reg_n_123,
      PCOUT(29) => sg_ab_64_p2_reg_n_124,
      PCOUT(28) => sg_ab_64_p2_reg_n_125,
      PCOUT(27) => sg_ab_64_p2_reg_n_126,
      PCOUT(26) => sg_ab_64_p2_reg_n_127,
      PCOUT(25) => sg_ab_64_p2_reg_n_128,
      PCOUT(24) => sg_ab_64_p2_reg_n_129,
      PCOUT(23) => sg_ab_64_p2_reg_n_130,
      PCOUT(22) => sg_ab_64_p2_reg_n_131,
      PCOUT(21) => sg_ab_64_p2_reg_n_132,
      PCOUT(20) => sg_ab_64_p2_reg_n_133,
      PCOUT(19) => sg_ab_64_p2_reg_n_134,
      PCOUT(18) => sg_ab_64_p2_reg_n_135,
      PCOUT(17) => sg_ab_64_p2_reg_n_136,
      PCOUT(16) => sg_ab_64_p2_reg_n_137,
      PCOUT(15) => sg_ab_64_p2_reg_n_138,
      PCOUT(14) => sg_ab_64_p2_reg_n_139,
      PCOUT(13) => sg_ab_64_p2_reg_n_140,
      PCOUT(12) => sg_ab_64_p2_reg_n_141,
      PCOUT(11) => sg_ab_64_p2_reg_n_142,
      PCOUT(10) => sg_ab_64_p2_reg_n_143,
      PCOUT(9) => sg_ab_64_p2_reg_n_144,
      PCOUT(8) => sg_ab_64_p2_reg_n_145,
      PCOUT(7) => sg_ab_64_p2_reg_n_146,
      PCOUT(6) => sg_ab_64_p2_reg_n_147,
      PCOUT(5) => sg_ab_64_p2_reg_n_148,
      PCOUT(4) => sg_ab_64_p2_reg_n_149,
      PCOUT(3) => sg_ab_64_p2_reg_n_150,
      PCOUT(2) => sg_ab_64_p2_reg_n_151,
      PCOUT(1) => sg_ab_64_p2_reg_n_152,
      PCOUT(0) => sg_ab_64_p2_reg_n_153,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_sg_ab_64_p2_reg_UNDERFLOW_UNCONNECTED
    );
\sg_ab_64_p2_reg[0]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_105,
      Q => \sg_ab_64_p2_reg[0]_srl2_n_0\
    );
\sg_ab_64_p2_reg[10]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_95,
      Q => \sg_ab_64_p2_reg[10]_srl2_n_0\
    );
\sg_ab_64_p2_reg[11]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_94,
      Q => \sg_ab_64_p2_reg[11]_srl2_n_0\
    );
\sg_ab_64_p2_reg[12]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_93,
      Q => \sg_ab_64_p2_reg[12]_srl2_n_0\
    );
\sg_ab_64_p2_reg[13]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_92,
      Q => \sg_ab_64_p2_reg[13]_srl2_n_0\
    );
\sg_ab_64_p2_reg[14]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_91,
      Q => \sg_ab_64_p2_reg[14]_srl2_n_0\
    );
\sg_ab_64_p2_reg[15]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_90,
      Q => \sg_ab_64_p2_reg[15]_srl2_n_0\
    );
\sg_ab_64_p2_reg[16]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_89,
      Q => \sg_ab_64_p2_reg[16]_srl2_n_0\
    );
\sg_ab_64_p2_reg[1]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_104,
      Q => \sg_ab_64_p2_reg[1]_srl2_n_0\
    );
\sg_ab_64_p2_reg[2]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_103,
      Q => \sg_ab_64_p2_reg[2]_srl2_n_0\
    );
\sg_ab_64_p2_reg[3]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_102,
      Q => \sg_ab_64_p2_reg[3]_srl2_n_0\
    );
\sg_ab_64_p2_reg[4]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_101,
      Q => \sg_ab_64_p2_reg[4]_srl2_n_0\
    );
\sg_ab_64_p2_reg[5]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_100,
      Q => \sg_ab_64_p2_reg[5]_srl2_n_0\
    );
\sg_ab_64_p2_reg[6]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_99,
      Q => \sg_ab_64_p2_reg[6]_srl2_n_0\
    );
\sg_ab_64_p2_reg[7]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_98,
      Q => \sg_ab_64_p2_reg[7]_srl2_n_0\
    );
\sg_ab_64_p2_reg[8]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_97,
      Q => \sg_ab_64_p2_reg[8]_srl2_n_0\
    );
\sg_ab_64_p2_reg[9]_srl2\: unisim.vcomponents.SRL16E
     port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => s00_axi_aclk,
      D => sg_ab_64_p0_reg_n_96,
      Q => \sg_ab_64_p2_reg[9]_srl2_n_0\
    );
sg_ab_64_p3_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 1,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => sg_a_p2(31),
      A(28) => sg_a_p2(31),
      A(27) => sg_a_p2(31),
      A(26) => sg_a_p2(31),
      A(25) => sg_a_p2(31),
      A(24) => sg_a_p2(31),
      A(23) => sg_a_p2(31),
      A(22) => sg_a_p2(31),
      A(21) => sg_a_p2(31),
      A(20) => sg_a_p2(31),
      A(19) => sg_a_p2(31),
      A(18) => sg_a_p2(31),
      A(17) => sg_a_p2(31),
      A(16) => sg_a_p2(31),
      A(15) => sg_a_p2(31),
      A(14 downto 0) => sg_a_p2(31 downto 17),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_sg_ab_64_p3_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => sg_b_p2(31),
      B(16) => sg_b_p2(31),
      B(15) => sg_b_p2(31),
      B(14 downto 0) => sg_b_p2(31 downto 17),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_sg_ab_64_p3_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_sg_ab_64_p3_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_sg_ab_64_p3_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '1',
      CEA2 => '1',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '1',
      CEB2 => '1',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '1',
      CEP => '1',
      CLK => s00_axi_aclk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_sg_ab_64_p3_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"1010101",
      OVERFLOW => NLW_sg_ab_64_p3_reg_OVERFLOW_UNCONNECTED,
      P(47) => sg_ab_64_p3_reg_n_58,
      P(46) => sg_ab_64_p3_reg_n_59,
      P(45) => sg_ab_64_p3_reg_n_60,
      P(44) => sg_ab_64_p3_reg_n_61,
      P(43) => sg_ab_64_p3_reg_n_62,
      P(42) => sg_ab_64_p3_reg_n_63,
      P(41) => sg_ab_64_p3_reg_n_64,
      P(40) => sg_ab_64_p3_reg_n_65,
      P(39) => sg_ab_64_p3_reg_n_66,
      P(38) => sg_ab_64_p3_reg_n_67,
      P(37) => sg_ab_64_p3_reg_n_68,
      P(36) => sg_ab_64_p3_reg_n_69,
      P(35) => sg_ab_64_p3_reg_n_70,
      P(34) => sg_ab_64_p3_reg_n_71,
      P(33) => sg_ab_64_p3_reg_n_72,
      P(32) => sg_ab_64_p3_reg_n_73,
      P(31) => sg_ab_64_p3_reg_n_74,
      P(30) => sg_ab_64_p3_reg_n_75,
      P(29 downto 0) => \^sg_ab_64_p3_reg_0\(63 downto 34),
      PATTERNBDETECT => NLW_sg_ab_64_p3_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_sg_ab_64_p3_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47) => sg_ab_64_p2_reg_n_106,
      PCIN(46) => sg_ab_64_p2_reg_n_107,
      PCIN(45) => sg_ab_64_p2_reg_n_108,
      PCIN(44) => sg_ab_64_p2_reg_n_109,
      PCIN(43) => sg_ab_64_p2_reg_n_110,
      PCIN(42) => sg_ab_64_p2_reg_n_111,
      PCIN(41) => sg_ab_64_p2_reg_n_112,
      PCIN(40) => sg_ab_64_p2_reg_n_113,
      PCIN(39) => sg_ab_64_p2_reg_n_114,
      PCIN(38) => sg_ab_64_p2_reg_n_115,
      PCIN(37) => sg_ab_64_p2_reg_n_116,
      PCIN(36) => sg_ab_64_p2_reg_n_117,
      PCIN(35) => sg_ab_64_p2_reg_n_118,
      PCIN(34) => sg_ab_64_p2_reg_n_119,
      PCIN(33) => sg_ab_64_p2_reg_n_120,
      PCIN(32) => sg_ab_64_p2_reg_n_121,
      PCIN(31) => sg_ab_64_p2_reg_n_122,
      PCIN(30) => sg_ab_64_p2_reg_n_123,
      PCIN(29) => sg_ab_64_p2_reg_n_124,
      PCIN(28) => sg_ab_64_p2_reg_n_125,
      PCIN(27) => sg_ab_64_p2_reg_n_126,
      PCIN(26) => sg_ab_64_p2_reg_n_127,
      PCIN(25) => sg_ab_64_p2_reg_n_128,
      PCIN(24) => sg_ab_64_p2_reg_n_129,
      PCIN(23) => sg_ab_64_p2_reg_n_130,
      PCIN(22) => sg_ab_64_p2_reg_n_131,
      PCIN(21) => sg_ab_64_p2_reg_n_132,
      PCIN(20) => sg_ab_64_p2_reg_n_133,
      PCIN(19) => sg_ab_64_p2_reg_n_134,
      PCIN(18) => sg_ab_64_p2_reg_n_135,
      PCIN(17) => sg_ab_64_p2_reg_n_136,
      PCIN(16) => sg_ab_64_p2_reg_n_137,
      PCIN(15) => sg_ab_64_p2_reg_n_138,
      PCIN(14) => sg_ab_64_p2_reg_n_139,
      PCIN(13) => sg_ab_64_p2_reg_n_140,
      PCIN(12) => sg_ab_64_p2_reg_n_141,
      PCIN(11) => sg_ab_64_p2_reg_n_142,
      PCIN(10) => sg_ab_64_p2_reg_n_143,
      PCIN(9) => sg_ab_64_p2_reg_n_144,
      PCIN(8) => sg_ab_64_p2_reg_n_145,
      PCIN(7) => sg_ab_64_p2_reg_n_146,
      PCIN(6) => sg_ab_64_p2_reg_n_147,
      PCIN(5) => sg_ab_64_p2_reg_n_148,
      PCIN(4) => sg_ab_64_p2_reg_n_149,
      PCIN(3) => sg_ab_64_p2_reg_n_150,
      PCIN(2) => sg_ab_64_p2_reg_n_151,
      PCIN(1) => sg_ab_64_p2_reg_n_152,
      PCIN(0) => sg_ab_64_p2_reg_n_153,
      PCOUT(47 downto 0) => NLW_sg_ab_64_p3_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_sg_ab_64_p3_reg_UNDERFLOW_UNCONNECTED
    );
\sg_ab_64_p3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[0]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(0),
      R => '0'
    );
\sg_ab_64_p3_reg[0]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_105,
      Q => \^sg_ab_64_p3_reg_0\(17),
      R => '0'
    );
\sg_ab_64_p3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[10]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(10),
      R => '0'
    );
\sg_ab_64_p3_reg[10]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_95,
      Q => \^sg_ab_64_p3_reg_0\(27),
      R => '0'
    );
\sg_ab_64_p3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[11]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(11),
      R => '0'
    );
\sg_ab_64_p3_reg[11]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_94,
      Q => \^sg_ab_64_p3_reg_0\(28),
      R => '0'
    );
\sg_ab_64_p3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[12]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(12),
      R => '0'
    );
\sg_ab_64_p3_reg[12]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_93,
      Q => \^sg_ab_64_p3_reg_0\(29),
      R => '0'
    );
\sg_ab_64_p3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[13]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(13),
      R => '0'
    );
\sg_ab_64_p3_reg[13]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_92,
      Q => \^sg_ab_64_p3_reg_0\(30),
      R => '0'
    );
\sg_ab_64_p3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[14]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(14),
      R => '0'
    );
\sg_ab_64_p3_reg[14]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_91,
      Q => \^sg_ab_64_p3_reg_0\(31),
      R => '0'
    );
\sg_ab_64_p3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[15]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(15),
      R => '0'
    );
\sg_ab_64_p3_reg[15]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_90,
      Q => \^sg_ab_64_p3_reg_0\(32),
      R => '0'
    );
\sg_ab_64_p3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[16]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(16),
      R => '0'
    );
\sg_ab_64_p3_reg[16]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_89,
      Q => \^sg_ab_64_p3_reg_0\(33),
      R => '0'
    );
\sg_ab_64_p3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[1]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(1),
      R => '0'
    );
\sg_ab_64_p3_reg[1]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_104,
      Q => \^sg_ab_64_p3_reg_0\(18),
      R => '0'
    );
\sg_ab_64_p3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[2]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(2),
      R => '0'
    );
\sg_ab_64_p3_reg[2]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_103,
      Q => \^sg_ab_64_p3_reg_0\(19),
      R => '0'
    );
\sg_ab_64_p3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[3]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(3),
      R => '0'
    );
\sg_ab_64_p3_reg[3]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_102,
      Q => \^sg_ab_64_p3_reg_0\(20),
      R => '0'
    );
\sg_ab_64_p3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[4]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(4),
      R => '0'
    );
\sg_ab_64_p3_reg[4]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_101,
      Q => \^sg_ab_64_p3_reg_0\(21),
      R => '0'
    );
\sg_ab_64_p3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[5]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(5),
      R => '0'
    );
\sg_ab_64_p3_reg[5]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_100,
      Q => \^sg_ab_64_p3_reg_0\(22),
      R => '0'
    );
\sg_ab_64_p3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[6]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(6),
      R => '0'
    );
\sg_ab_64_p3_reg[6]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_99,
      Q => \^sg_ab_64_p3_reg_0\(23),
      R => '0'
    );
\sg_ab_64_p3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[7]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(7),
      R => '0'
    );
\sg_ab_64_p3_reg[7]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_98,
      Q => \^sg_ab_64_p3_reg_0\(24),
      R => '0'
    );
\sg_ab_64_p3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[8]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(8),
      R => '0'
    );
\sg_ab_64_p3_reg[8]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_97,
      Q => \^sg_ab_64_p3_reg_0\(25),
      R => '0'
    );
\sg_ab_64_p3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \sg_ab_64_p2_reg[9]_srl2_n_0\,
      Q => \^sg_ab_64_p3_reg_0\(9),
      R => '0'
    );
\sg_ab_64_p3_reg[9]__0\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_ab_64_p2_reg_n_96,
      Q => \^sg_ab_64_p3_reg_0\(26),
      R => '0'
    );
\sg_b_p0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(0),
      Q => sg_b_p0(0),
      R => '0'
    );
\sg_b_p0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(10),
      Q => sg_b_p0(10),
      R => '0'
    );
\sg_b_p0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(11),
      Q => sg_b_p0(11),
      R => '0'
    );
\sg_b_p0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(12),
      Q => sg_b_p0(12),
      R => '0'
    );
\sg_b_p0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(13),
      Q => sg_b_p0(13),
      R => '0'
    );
\sg_b_p0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(14),
      Q => sg_b_p0(14),
      R => '0'
    );
\sg_b_p0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(15),
      Q => sg_b_p0(15),
      R => '0'
    );
\sg_b_p0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(16),
      Q => sg_b_p0(16),
      R => '0'
    );
\sg_b_p0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(17),
      Q => sg_b_p0(17),
      R => '0'
    );
\sg_b_p0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(18),
      Q => sg_b_p0(18),
      R => '0'
    );
\sg_b_p0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(19),
      Q => sg_b_p0(19),
      R => '0'
    );
\sg_b_p0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(1),
      Q => sg_b_p0(1),
      R => '0'
    );
\sg_b_p0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(20),
      Q => sg_b_p0(20),
      R => '0'
    );
\sg_b_p0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(21),
      Q => sg_b_p0(21),
      R => '0'
    );
\sg_b_p0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(22),
      Q => sg_b_p0(22),
      R => '0'
    );
\sg_b_p0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(23),
      Q => sg_b_p0(23),
      R => '0'
    );
\sg_b_p0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(24),
      Q => sg_b_p0(24),
      R => '0'
    );
\sg_b_p0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(25),
      Q => sg_b_p0(25),
      R => '0'
    );
\sg_b_p0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(26),
      Q => sg_b_p0(26),
      R => '0'
    );
\sg_b_p0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(27),
      Q => sg_b_p0(27),
      R => '0'
    );
\sg_b_p0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(28),
      Q => sg_b_p0(28),
      R => '0'
    );
\sg_b_p0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(29),
      Q => sg_b_p0(29),
      R => '0'
    );
\sg_b_p0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(2),
      Q => sg_b_p0(2),
      R => '0'
    );
\sg_b_p0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(30),
      Q => sg_b_p0(30),
      R => '0'
    );
\sg_b_p0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(31),
      Q => sg_b_p0(31),
      R => '0'
    );
\sg_b_p0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(3),
      Q => sg_b_p0(3),
      R => '0'
    );
\sg_b_p0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(4),
      Q => sg_b_p0(4),
      R => '0'
    );
\sg_b_p0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(5),
      Q => sg_b_p0(5),
      R => '0'
    );
\sg_b_p0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(6),
      Q => sg_b_p0(6),
      R => '0'
    );
\sg_b_p0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(7),
      Q => sg_b_p0(7),
      R => '0'
    );
\sg_b_p0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(8),
      Q => sg_b_p0(8),
      R => '0'
    );
\sg_b_p0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => Q(9),
      Q => sg_b_p0(9),
      R => '0'
    );
\sg_b_p1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(0),
      Q => sg_b_p1(0),
      R => '0'
    );
\sg_b_p1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(10),
      Q => sg_b_p1(10),
      R => '0'
    );
\sg_b_p1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(11),
      Q => sg_b_p1(11),
      R => '0'
    );
\sg_b_p1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(12),
      Q => sg_b_p1(12),
      R => '0'
    );
\sg_b_p1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(13),
      Q => sg_b_p1(13),
      R => '0'
    );
\sg_b_p1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(14),
      Q => sg_b_p1(14),
      R => '0'
    );
\sg_b_p1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(15),
      Q => sg_b_p1(15),
      R => '0'
    );
\sg_b_p1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(16),
      Q => sg_b_p1(16),
      R => '0'
    );
\sg_b_p1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(17),
      Q => sg_b_p1(17),
      R => '0'
    );
\sg_b_p1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(18),
      Q => sg_b_p1(18),
      R => '0'
    );
\sg_b_p1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(19),
      Q => sg_b_p1(19),
      R => '0'
    );
\sg_b_p1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(1),
      Q => sg_b_p1(1),
      R => '0'
    );
\sg_b_p1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(20),
      Q => sg_b_p1(20),
      R => '0'
    );
\sg_b_p1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(21),
      Q => sg_b_p1(21),
      R => '0'
    );
\sg_b_p1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(22),
      Q => sg_b_p1(22),
      R => '0'
    );
\sg_b_p1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(23),
      Q => sg_b_p1(23),
      R => '0'
    );
\sg_b_p1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(24),
      Q => sg_b_p1(24),
      R => '0'
    );
\sg_b_p1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(25),
      Q => sg_b_p1(25),
      R => '0'
    );
\sg_b_p1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(26),
      Q => sg_b_p1(26),
      R => '0'
    );
\sg_b_p1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(27),
      Q => sg_b_p1(27),
      R => '0'
    );
\sg_b_p1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(28),
      Q => sg_b_p1(28),
      R => '0'
    );
\sg_b_p1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(29),
      Q => sg_b_p1(29),
      R => '0'
    );
\sg_b_p1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(2),
      Q => sg_b_p1(2),
      R => '0'
    );
\sg_b_p1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(30),
      Q => sg_b_p1(30),
      R => '0'
    );
\sg_b_p1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(31),
      Q => sg_b_p1(31),
      R => '0'
    );
\sg_b_p1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(3),
      Q => sg_b_p1(3),
      R => '0'
    );
\sg_b_p1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(4),
      Q => sg_b_p1(4),
      R => '0'
    );
\sg_b_p1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(5),
      Q => sg_b_p1(5),
      R => '0'
    );
\sg_b_p1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(6),
      Q => sg_b_p1(6),
      R => '0'
    );
\sg_b_p1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(7),
      Q => sg_b_p1(7),
      R => '0'
    );
\sg_b_p1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(8),
      Q => sg_b_p1(8),
      R => '0'
    );
\sg_b_p1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p0(9),
      Q => sg_b_p1(9),
      R => '0'
    );
\sg_b_p2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(17),
      Q => sg_b_p2(17),
      R => '0'
    );
\sg_b_p2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(18),
      Q => sg_b_p2(18),
      R => '0'
    );
\sg_b_p2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(19),
      Q => sg_b_p2(19),
      R => '0'
    );
\sg_b_p2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(20),
      Q => sg_b_p2(20),
      R => '0'
    );
\sg_b_p2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(21),
      Q => sg_b_p2(21),
      R => '0'
    );
\sg_b_p2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(22),
      Q => sg_b_p2(22),
      R => '0'
    );
\sg_b_p2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(23),
      Q => sg_b_p2(23),
      R => '0'
    );
\sg_b_p2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(24),
      Q => sg_b_p2(24),
      R => '0'
    );
\sg_b_p2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(25),
      Q => sg_b_p2(25),
      R => '0'
    );
\sg_b_p2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(26),
      Q => sg_b_p2(26),
      R => '0'
    );
\sg_b_p2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(27),
      Q => sg_b_p2(27),
      R => '0'
    );
\sg_b_p2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(28),
      Q => sg_b_p2(28),
      R => '0'
    );
\sg_b_p2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(29),
      Q => sg_b_p2(29),
      R => '0'
    );
\sg_b_p2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(30),
      Q => sg_b_p2(30),
      R => '0'
    );
\sg_b_p2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sg_b_p1(31),
      Q => sg_b_p2(31),
      R => '0'
    );
\sg_xowb_carry__6_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]_0\(3),
      I1 => \sg_a_p0_reg[31]_1\(3),
      O => S(3)
    );
\sg_xowb_carry__6_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]_1\(2),
      I1 => \sg_a_p0_reg[31]_0\(2),
      O => S(2)
    );
\sg_xowb_carry__6_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]_1\(1),
      I1 => \sg_a_p0_reg[31]_0\(1),
      O => S(1)
    );
\sg_xowb_carry__6_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sg_a_p0_reg[31]_1\(0),
      I1 => \sg_a_p0_reg[31]_0\(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fer_soc_bd_tflite_mbqm_0_0_tflite_core2 is
  port (
    ab_nudge : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg4_reg[7]\ : out STD_LOGIC;
    \sg_ab_64_p3_reg[14]__0\ : out STD_LOGIC;
    \slv_reg4_reg[7]_0\ : out STD_LOGIC;
    \slv_reg4_reg[7]_1\ : out STD_LOGIC;
    \slv_reg4_reg[7]_2\ : out STD_LOGIC;
    \slv_reg4_reg[7]_3\ : out STD_LOGIC;
    \slv_reg4_reg[7]_4\ : out STD_LOGIC;
    \slv_reg4_reg[7]_5\ : out STD_LOGIC;
    \slv_reg4_reg[5]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    sg_ab_64_p3_reg : out STD_LOGIC;
    sg_ab_64_p3_reg_0 : out STD_LOGIC;
    sg_ab_64_p3_reg_1 : out STD_LOGIC;
    sg_ab_64_p3_reg_2 : out STD_LOGIC;
    sg_ab_64_p3_reg_3 : out STD_LOGIC;
    sg_ab_64_p3_reg_4 : out STD_LOGIC;
    \slv_reg4_reg[7]_6\ : out STD_LOGIC;
    \slv_reg4_reg[7]_7\ : out STD_LOGIC;
    \slv_reg4_reg[7]_8\ : out STD_LOGIC;
    \slv_reg4_reg[7]_9\ : out STD_LOGIC;
    \slv_reg4_reg[7]_10\ : out STD_LOGIC;
    \slv_reg4_reg[7]_11\ : out STD_LOGIC;
    \slv_reg4_reg[7]_12\ : out STD_LOGIC;
    \slv_reg4_reg[7]_13\ : out STD_LOGIC;
    \slv_reg4_reg[7]_14\ : out STD_LOGIC;
    \slv_reg4_reg[7]_15\ : out STD_LOGIC;
    \slv_reg4_reg[7]_16\ : out STD_LOGIC;
    \slv_reg4_reg[7]_17\ : out STD_LOGIC;
    \slv_reg4_reg[7]_18\ : out STD_LOGIC;
    \slv_reg4_reg[7]_19\ : out STD_LOGIC;
    \axi_rdata[19]_i_7_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \axi_rdata[19]_i_7_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata[15]_i_21\ : in STD_LOGIC_VECTOR ( 63 downto 0 );
    \ab_nudge_carry__0_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata[3]_i_19\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_rdata[7]_i_4\ : in STD_LOGIC;
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata[7]_i_7\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata[3]_i_4\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata_reg[19]_i_2_0\ : in STD_LOGIC;
    \axi_rdata_reg[11]_i_2\ : in STD_LOGIC;
    \axi_rdata_reg[11]_i_2_0\ : in STD_LOGIC;
    \axi_rdata_reg[11]_i_2_1\ : in STD_LOGIC;
    \axi_rdata[15]_i_7_0\ : in STD_LOGIC;
    \axi_rdata[19]_i_6_0\ : in STD_LOGIC;
    \axi_rdata[19]_i_6_1\ : in STD_LOGIC;
    \axi_rdata[11]_i_5_0\ : in STD_LOGIC;
    \axi_rdata_reg[19]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata[15]_i_6_0\ : in STD_LOGIC;
    \axi_rdata[11]_i_4_0\ : in STD_LOGIC;
    \axi_rdata[19]_i_7_2\ : in STD_LOGIC;
    \axi_rdata_reg[19]_i_2_1\ : in STD_LOGIC;
    \axi_rdata[19]_i_6_2\ : in STD_LOGIC;
    \axi_rdata[23]_i_11\ : in STD_LOGIC;
    \axi_rdata[23]_i_11_0\ : in STD_LOGIC;
    \axi_rdata[23]_i_11_1\ : in STD_LOGIC;
    \axi_rdata[31]_i_5\ : in STD_LOGIC;
    \axi_rdata[15]_i_7_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata[19]_i_7_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata[23]_i_7\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata[27]_i_7\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata[31]_i_8\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata[31]_i_7\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata_reg[15]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end fer_soc_bd_tflite_mbqm_0_0_tflite_core2;

architecture STRUCTURE of fer_soc_bd_tflite_mbqm_0_0_tflite_core2 is
  signal SHIFT_RIGHT : STD_LOGIC_VECTOR ( 18 downto 12 );
  signal \^ab_nudge\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \ab_nudge_carry__0_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__0_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__0_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__0_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__10_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__10_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__10_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__10_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__11_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__11_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__11_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__11_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__12_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__12_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__12_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__12_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__13_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__13_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__13_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__13_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__14_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__14_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__14_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__1_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__1_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__1_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__1_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__2_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__2_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__2_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__2_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__3_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__3_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__3_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__3_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__4_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__4_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__4_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__4_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__5_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__5_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__5_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__5_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__6_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__6_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__6_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__6_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__7_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__7_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__7_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__7_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__8_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__8_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__8_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__8_n_3\ : STD_LOGIC;
  signal \ab_nudge_carry__9_n_0\ : STD_LOGIC;
  signal \ab_nudge_carry__9_n_1\ : STD_LOGIC;
  signal \ab_nudge_carry__9_n_2\ : STD_LOGIC;
  signal \ab_nudge_carry__9_n_3\ : STD_LOGIC;
  signal ab_nudge_carry_n_0 : STD_LOGIC;
  signal ab_nudge_carry_n_1 : STD_LOGIC;
  signal ab_nudge_carry_n_2 : STD_LOGIC;
  signal ab_nudge_carry_n_3 : STD_LOGIC;
  signal \axi_rdata[11]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_13_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_14_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_15_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_8_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_10_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_12_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_13_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_14_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_15_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_17_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_18_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_2_n_1\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_2_n_2\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_2_n_3\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_2_n_1\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_2_n_2\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_2_n_3\ : STD_LOGIC;
  signal \^sg_ab_64_p3_reg\ : STD_LOGIC;
  signal \^sg_ab_64_p3_reg[14]__0\ : STD_LOGIC;
  signal \^sg_ab_64_p3_reg_0\ : STD_LOGIC;
  signal \^sg_ab_64_p3_reg_1\ : STD_LOGIC;
  signal \^sg_ab_64_p3_reg_2\ : STD_LOGIC;
  signal \^sg_ab_64_p3_reg_3\ : STD_LOGIC;
  signal \^sg_ab_64_p3_reg_4\ : STD_LOGIC;
  signal NLW_ab_nudge_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ab_nudge_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ab_nudge_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ab_nudge_carry__14_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_ab_nudge_carry__14_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_ab_nudge_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ab_nudge_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ab_nudge_carry__4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ab_nudge_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_ab_nudge_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of ab_nudge_carry : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__10\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__11\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__12\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__13\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__14\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__2\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__3\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__4\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__5\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__6\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__7\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__8\ : label is 35;
  attribute ADDER_THRESHOLD of \ab_nudge_carry__9\ : label is 35;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_rdata[11]_i_10\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \axi_rdata[11]_i_12\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \axi_rdata[11]_i_14\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \axi_rdata[11]_i_8\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_10\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_12\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_14\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_8\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \axi_rdata[19]_i_10\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \axi_rdata[19]_i_12\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \axi_rdata[19]_i_14\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \axi_rdata[19]_i_8\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \axi_rdata[23]_i_10\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \axi_rdata[23]_i_12\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \axi_rdata[23]_i_14\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \axi_rdata[23]_i_8\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \axi_rdata[27]_i_10\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \axi_rdata[27]_i_12\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \axi_rdata[27]_i_14\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \axi_rdata[27]_i_8\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_12\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_16\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \axi_rdata[3]_i_11\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_rdata[3]_i_13\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_rdata[3]_i_15\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \axi_rdata[3]_i_16\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \axi_rdata[7]_i_11\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \axi_rdata[7]_i_13\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_rdata[7]_i_14\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_rdata[7]_i_9\ : label is "soft_lutpair3";
  attribute ADDER_THRESHOLD of \axi_rdata_reg[15]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \axi_rdata_reg[19]_i_2\ : label is 35;
begin
  ab_nudge(31 downto 0) <= \^ab_nudge\(31 downto 0);
  sg_ab_64_p3_reg <= \^sg_ab_64_p3_reg\;
  \sg_ab_64_p3_reg[14]__0\ <= \^sg_ab_64_p3_reg[14]__0\;
  sg_ab_64_p3_reg_0 <= \^sg_ab_64_p3_reg_0\;
  sg_ab_64_p3_reg_1 <= \^sg_ab_64_p3_reg_1\;
  sg_ab_64_p3_reg_2 <= \^sg_ab_64_p3_reg_2\;
  sg_ab_64_p3_reg_3 <= \^sg_ab_64_p3_reg_3\;
  sg_ab_64_p3_reg_4 <= \^sg_ab_64_p3_reg_4\;
ab_nudge_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => ab_nudge_carry_n_0,
      CO(2) => ab_nudge_carry_n_1,
      CO(1) => ab_nudge_carry_n_2,
      CO(0) => ab_nudge_carry_n_3,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \axi_rdata[15]_i_21\(0),
      O(3 downto 0) => NLW_ab_nudge_carry_O_UNCONNECTED(3 downto 0),
      S(3 downto 1) => \axi_rdata[15]_i_21\(3 downto 1),
      S(0) => \ab_nudge_carry__0_0\(0)
    );
\ab_nudge_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => ab_nudge_carry_n_0,
      CO(3) => \ab_nudge_carry__0_n_0\,
      CO(2) => \ab_nudge_carry__0_n_1\,
      CO(1) => \ab_nudge_carry__0_n_2\,
      CO(0) => \ab_nudge_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_ab_nudge_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \axi_rdata[15]_i_21\(7 downto 4)
    );
\ab_nudge_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__0_n_0\,
      CO(3) => \ab_nudge_carry__1_n_0\,
      CO(2) => \ab_nudge_carry__1_n_1\,
      CO(1) => \ab_nudge_carry__1_n_2\,
      CO(0) => \ab_nudge_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_ab_nudge_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \axi_rdata[15]_i_21\(11 downto 8)
    );
\ab_nudge_carry__10\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__9_n_0\,
      CO(3) => \ab_nudge_carry__10_n_0\,
      CO(2) => \ab_nudge_carry__10_n_1\,
      CO(1) => \ab_nudge_carry__10_n_2\,
      CO(0) => \ab_nudge_carry__10_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \^ab_nudge\(16 downto 13),
      S(3 downto 0) => \axi_rdata[15]_i_21\(47 downto 44)
    );
\ab_nudge_carry__11\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__10_n_0\,
      CO(3) => \ab_nudge_carry__11_n_0\,
      CO(2) => \ab_nudge_carry__11_n_1\,
      CO(1) => \ab_nudge_carry__11_n_2\,
      CO(0) => \ab_nudge_carry__11_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \^ab_nudge\(20 downto 17),
      S(3 downto 0) => \axi_rdata[15]_i_21\(51 downto 48)
    );
\ab_nudge_carry__12\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__11_n_0\,
      CO(3) => \ab_nudge_carry__12_n_0\,
      CO(2) => \ab_nudge_carry__12_n_1\,
      CO(1) => \ab_nudge_carry__12_n_2\,
      CO(0) => \ab_nudge_carry__12_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \^ab_nudge\(24 downto 21),
      S(3 downto 0) => \axi_rdata[15]_i_21\(55 downto 52)
    );
\ab_nudge_carry__13\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__12_n_0\,
      CO(3) => \ab_nudge_carry__13_n_0\,
      CO(2) => \ab_nudge_carry__13_n_1\,
      CO(1) => \ab_nudge_carry__13_n_2\,
      CO(0) => \ab_nudge_carry__13_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \^ab_nudge\(28 downto 25),
      S(3 downto 0) => \axi_rdata[15]_i_21\(59 downto 56)
    );
\ab_nudge_carry__14\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__13_n_0\,
      CO(3) => \NLW_ab_nudge_carry__14_CO_UNCONNECTED\(3),
      CO(2) => \ab_nudge_carry__14_n_1\,
      CO(1) => \ab_nudge_carry__14_n_2\,
      CO(0) => \ab_nudge_carry__14_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \^ab_nudge\(31),
      O(2) => \NLW_ab_nudge_carry__14_O_UNCONNECTED\(2),
      O(1 downto 0) => \^ab_nudge\(30 downto 29),
      S(3 downto 0) => \axi_rdata[15]_i_21\(63 downto 60)
    );
\ab_nudge_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__1_n_0\,
      CO(3) => \ab_nudge_carry__2_n_0\,
      CO(2) => \ab_nudge_carry__2_n_1\,
      CO(1) => \ab_nudge_carry__2_n_2\,
      CO(0) => \ab_nudge_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_ab_nudge_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \axi_rdata[15]_i_21\(15 downto 12)
    );
\ab_nudge_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__2_n_0\,
      CO(3) => \ab_nudge_carry__3_n_0\,
      CO(2) => \ab_nudge_carry__3_n_1\,
      CO(1) => \ab_nudge_carry__3_n_2\,
      CO(0) => \ab_nudge_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_ab_nudge_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \axi_rdata[15]_i_21\(19 downto 16)
    );
\ab_nudge_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__3_n_0\,
      CO(3) => \ab_nudge_carry__4_n_0\,
      CO(2) => \ab_nudge_carry__4_n_1\,
      CO(1) => \ab_nudge_carry__4_n_2\,
      CO(0) => \ab_nudge_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_ab_nudge_carry__4_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \axi_rdata[15]_i_21\(23 downto 20)
    );
\ab_nudge_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__4_n_0\,
      CO(3) => \ab_nudge_carry__5_n_0\,
      CO(2) => \ab_nudge_carry__5_n_1\,
      CO(1) => \ab_nudge_carry__5_n_2\,
      CO(0) => \ab_nudge_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_ab_nudge_carry__5_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => \axi_rdata[15]_i_21\(27 downto 24)
    );
\ab_nudge_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__5_n_0\,
      CO(3) => \ab_nudge_carry__6_n_0\,
      CO(2) => \ab_nudge_carry__6_n_1\,
      CO(1) => \ab_nudge_carry__6_n_2\,
      CO(0) => \ab_nudge_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => \axi_rdata[15]_i_21\(31 downto 30),
      DI(1 downto 0) => B"00",
      O(3) => \^ab_nudge\(0),
      O(2 downto 0) => \NLW_ab_nudge_carry__6_O_UNCONNECTED\(2 downto 0),
      S(3 downto 2) => \axi_rdata[3]_i_19\(1 downto 0),
      S(1 downto 0) => \axi_rdata[15]_i_21\(29 downto 28)
    );
\ab_nudge_carry__7\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__6_n_0\,
      CO(3) => \ab_nudge_carry__7_n_0\,
      CO(2) => \ab_nudge_carry__7_n_1\,
      CO(1) => \ab_nudge_carry__7_n_2\,
      CO(0) => \ab_nudge_carry__7_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \^ab_nudge\(4 downto 1),
      S(3 downto 0) => \axi_rdata[15]_i_21\(35 downto 32)
    );
\ab_nudge_carry__8\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__7_n_0\,
      CO(3) => \ab_nudge_carry__8_n_0\,
      CO(2) => \ab_nudge_carry__8_n_1\,
      CO(1) => \ab_nudge_carry__8_n_2\,
      CO(0) => \ab_nudge_carry__8_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \^ab_nudge\(8 downto 5),
      S(3 downto 0) => \axi_rdata[15]_i_21\(39 downto 36)
    );
\ab_nudge_carry__9\: unisim.vcomponents.CARRY4
     port map (
      CI => \ab_nudge_carry__8_n_0\,
      CO(3) => \ab_nudge_carry__9_n_0\,
      CO(2) => \ab_nudge_carry__9_n_1\,
      CO(1) => \ab_nudge_carry__9_n_2\,
      CO(0) => \ab_nudge_carry__9_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \^ab_nudge\(12 downto 9),
      S(3 downto 0) => \axi_rdata[15]_i_21\(43 downto 40)
    );
\axi_rdata[11]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[15]_i_7_1\(1),
      O => \axi_rdata[11]_i_10_n_0\
    );
\axi_rdata[11]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[15]_i_7_1\(0),
      O => \slv_reg4_reg[7]_8\
    );
\axi_rdata[11]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => O(3),
      O => \slv_reg4_reg[7]_7\
    );
\axi_rdata[11]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata[11]_i_8_n_0\,
      I1 => \axi_rdata_reg[19]_i_2_0\,
      I2 => \axi_rdata[15]_i_15_n_0\,
      I3 => \axi_rdata_reg[11]_i_2_1\,
      I4 => \axi_rdata[11]_i_9_n_0\,
      I5 => \axi_rdata_reg[11]_i_2_0\,
      O => \slv_reg4_reg[5]\(1)
    );
\axi_rdata[11]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata[11]_i_10_n_0\,
      I1 => \axi_rdata_reg[19]_i_2_0\,
      I2 => \axi_rdata_reg[11]_i_2\,
      I3 => \axi_rdata_reg[11]_i_2_0\,
      I4 => \axi_rdata[11]_i_9_n_0\,
      I5 => \axi_rdata_reg[11]_i_2_1\,
      O => \slv_reg4_reg[5]\(0)
    );
\axi_rdata[11]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[15]_i_7_1\(2),
      O => \axi_rdata[11]_i_8_n_0\
    );
\axi_rdata[11]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[19]_i_18_n_0\,
      I1 => \axi_rdata[15]_i_7_0\,
      I2 => \axi_rdata[19]_i_6_0\,
      I3 => \^sg_ab_64_p3_reg\,
      I4 => \axi_rdata[19]_i_6_1\,
      I5 => \axi_rdata[11]_i_5_0\,
      O => \axi_rdata[11]_i_9_n_0\
    );
\axi_rdata[15]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[19]_i_7_3\(1),
      O => \axi_rdata[15]_i_10_n_0\
    );
\axi_rdata[15]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^sg_ab_64_p3_reg_2\,
      I1 => \^sg_ab_64_p3_reg_0\,
      I2 => \axi_rdata[19]_i_6_0\,
      I3 => \axi_rdata[19]_i_17_n_0\,
      I4 => \axi_rdata[19]_i_6_1\,
      I5 => \axi_rdata[15]_i_6_0\,
      O => \axi_rdata[15]_i_11_n_0\
    );
\axi_rdata[15]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[19]_i_7_3\(0),
      O => \axi_rdata[15]_i_12_n_0\
    );
\axi_rdata[15]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^sg_ab_64_p3_reg_1\,
      I1 => \^sg_ab_64_p3_reg\,
      I2 => \axi_rdata[19]_i_6_0\,
      I3 => \axi_rdata[19]_i_18_n_0\,
      I4 => \axi_rdata[19]_i_6_1\,
      I5 => \axi_rdata[15]_i_7_0\,
      O => \axi_rdata[15]_i_13_n_0\
    );
\axi_rdata[15]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[15]_i_7_1\(3),
      O => \axi_rdata[15]_i_14_n_0\
    );
\axi_rdata[15]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[19]_i_17_n_0\,
      I1 => \axi_rdata[15]_i_6_0\,
      I2 => \axi_rdata[19]_i_6_0\,
      I3 => \^sg_ab_64_p3_reg_0\,
      I4 => \axi_rdata[19]_i_6_1\,
      I5 => \axi_rdata[11]_i_4_0\,
      O => \axi_rdata[15]_i_15_n_0\
    );
\axi_rdata[15]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000F000F044F077"
    )
        port map (
      I0 => \^ab_nudge\(23),
      I1 => \axi_rdata[23]_i_11\,
      I2 => \axi_rdata[23]_i_11_0\,
      I3 => \axi_rdata[23]_i_11_1\,
      I4 => \^ab_nudge\(15),
      I5 => \axi_rdata[31]_i_5\,
      O => \^sg_ab_64_p3_reg\
    );
\axi_rdata[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata[15]_i_8_n_0\,
      I1 => \axi_rdata_reg[19]_i_2_0\,
      I2 => \axi_rdata[19]_i_15_n_0\,
      I3 => \axi_rdata_reg[11]_i_2_1\,
      I4 => \axi_rdata[15]_i_9_n_0\,
      I5 => \axi_rdata_reg[11]_i_2_0\,
      O => SHIFT_RIGHT(15)
    );
\axi_rdata[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata[15]_i_10_n_0\,
      I1 => \axi_rdata_reg[19]_i_2_0\,
      I2 => \axi_rdata[15]_i_11_n_0\,
      I3 => \axi_rdata_reg[11]_i_2_0\,
      I4 => \axi_rdata[15]_i_9_n_0\,
      I5 => \axi_rdata_reg[11]_i_2_1\,
      O => SHIFT_RIGHT(14)
    );
\axi_rdata[15]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata[15]_i_12_n_0\,
      I1 => \axi_rdata_reg[19]_i_2_0\,
      I2 => \axi_rdata[15]_i_11_n_0\,
      I3 => \axi_rdata_reg[11]_i_2_1\,
      I4 => \axi_rdata[15]_i_13_n_0\,
      I5 => \axi_rdata_reg[11]_i_2_0\,
      O => SHIFT_RIGHT(13)
    );
\axi_rdata[15]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata[15]_i_14_n_0\,
      I1 => \axi_rdata_reg[19]_i_2_0\,
      I2 => \axi_rdata[15]_i_15_n_0\,
      I3 => \axi_rdata_reg[11]_i_2_0\,
      I4 => \axi_rdata[15]_i_13_n_0\,
      I5 => \axi_rdata_reg[11]_i_2_1\,
      O => SHIFT_RIGHT(12)
    );
\axi_rdata[15]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[19]_i_7_3\(2),
      O => \axi_rdata[15]_i_8_n_0\
    );
\axi_rdata[15]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^sg_ab_64_p3_reg_3\,
      I1 => \axi_rdata[19]_i_18_n_0\,
      I2 => \axi_rdata[19]_i_6_0\,
      I3 => \^sg_ab_64_p3_reg_1\,
      I4 => \axi_rdata[19]_i_6_1\,
      I5 => \^sg_ab_64_p3_reg\,
      O => \axi_rdata[15]_i_9_n_0\
    );
\axi_rdata[19]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[23]_i_7\(1),
      O => \axi_rdata[19]_i_10_n_0\
    );
\axi_rdata[19]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[19]_i_6_2\,
      I1 => \^sg_ab_64_p3_reg_2\,
      I2 => \axi_rdata[19]_i_6_0\,
      I3 => \^sg_ab_64_p3_reg_4\,
      I4 => \axi_rdata[19]_i_6_1\,
      I5 => \axi_rdata[19]_i_17_n_0\,
      O => \axi_rdata[19]_i_11_n_0\
    );
\axi_rdata[19]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[23]_i_7\(0),
      O => \axi_rdata[19]_i_12_n_0\
    );
\axi_rdata[19]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata[19]_i_7_2\,
      I1 => \^sg_ab_64_p3_reg_1\,
      I2 => \axi_rdata[19]_i_6_0\,
      I3 => \^sg_ab_64_p3_reg_3\,
      I4 => \axi_rdata[19]_i_6_1\,
      I5 => \axi_rdata[19]_i_18_n_0\,
      O => \axi_rdata[19]_i_13_n_0\
    );
\axi_rdata[19]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[19]_i_7_3\(3),
      O => \axi_rdata[19]_i_14_n_0\
    );
\axi_rdata[19]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^sg_ab_64_p3_reg_4\,
      I1 => \axi_rdata[19]_i_17_n_0\,
      I2 => \axi_rdata[19]_i_6_0\,
      I3 => \^sg_ab_64_p3_reg_2\,
      I4 => \axi_rdata[19]_i_6_1\,
      I5 => \^sg_ab_64_p3_reg_0\,
      O => \axi_rdata[19]_i_15_n_0\
    );
\axi_rdata[19]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000F000F044F077"
    )
        port map (
      I0 => \^ab_nudge\(27),
      I1 => \axi_rdata[23]_i_11\,
      I2 => \axi_rdata[23]_i_11_0\,
      I3 => \axi_rdata[23]_i_11_1\,
      I4 => \^ab_nudge\(19),
      I5 => \axi_rdata[31]_i_5\,
      O => \^sg_ab_64_p3_reg_1\
    );
\axi_rdata[19]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000F000F044F077"
    )
        port map (
      I0 => \^ab_nudge\(26),
      I1 => \axi_rdata[23]_i_11\,
      I2 => \axi_rdata[23]_i_11_0\,
      I3 => \axi_rdata[23]_i_11_1\,
      I4 => \^ab_nudge\(18),
      I5 => \axi_rdata[31]_i_5\,
      O => \axi_rdata[19]_i_17_n_0\
    );
\axi_rdata[19]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000F000F044F077"
    )
        port map (
      I0 => \^ab_nudge\(25),
      I1 => \axi_rdata[23]_i_11\,
      I2 => \axi_rdata[23]_i_11_0\,
      I3 => \axi_rdata[23]_i_11_1\,
      I4 => \^ab_nudge\(17),
      I5 => \axi_rdata[31]_i_5\,
      O => \axi_rdata[19]_i_18_n_0\
    );
\axi_rdata[19]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000F000F044F077"
    )
        port map (
      I0 => \^ab_nudge\(24),
      I1 => \axi_rdata[23]_i_11\,
      I2 => \axi_rdata[23]_i_11_0\,
      I3 => \axi_rdata[23]_i_11_1\,
      I4 => \^ab_nudge\(16),
      I5 => \axi_rdata[31]_i_5\,
      O => \^sg_ab_64_p3_reg_0\
    );
\axi_rdata[19]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata[19]_i_10_n_0\,
      I1 => \axi_rdata_reg[19]_i_2_0\,
      I2 => \axi_rdata_reg[19]_i_2_1\,
      I3 => \axi_rdata_reg[11]_i_2_1\,
      I4 => \axi_rdata[19]_i_11_n_0\,
      I5 => \axi_rdata_reg[11]_i_2_0\,
      O => SHIFT_RIGHT(18)
    );
\axi_rdata[19]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA20AA2020"
    )
        port map (
      I0 => \axi_rdata[19]_i_12_n_0\,
      I1 => \axi_rdata[19]_i_13_n_0\,
      I2 => \axi_rdata_reg[11]_i_2_0\,
      I3 => \axi_rdata[19]_i_11_n_0\,
      I4 => \axi_rdata_reg[11]_i_2_1\,
      I5 => \axi_rdata_reg[19]_i_2_0\,
      O => SHIFT_RIGHT(17)
    );
\axi_rdata[19]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8A88AAAA8A888A88"
    )
        port map (
      I0 => \axi_rdata[19]_i_14_n_0\,
      I1 => \axi_rdata_reg[19]_i_2_0\,
      I2 => \axi_rdata[19]_i_13_n_0\,
      I3 => \axi_rdata_reg[11]_i_2_1\,
      I4 => \axi_rdata[19]_i_15_n_0\,
      I5 => \axi_rdata_reg[11]_i_2_0\,
      O => SHIFT_RIGHT(16)
    );
\axi_rdata[19]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[23]_i_7\(2),
      O => \slv_reg4_reg[7]_9\
    );
\axi_rdata[23]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[27]_i_7\(1),
      O => \slv_reg4_reg[7]_12\
    );
\axi_rdata[23]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[27]_i_7\(0),
      O => \slv_reg4_reg[7]_11\
    );
\axi_rdata[23]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[23]_i_7\(3),
      O => \slv_reg4_reg[7]_10\
    );
\axi_rdata[23]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000F000F044F077"
    )
        port map (
      I0 => \^ab_nudge\(30),
      I1 => \axi_rdata[23]_i_11\,
      I2 => \axi_rdata[23]_i_11_0\,
      I3 => \axi_rdata[23]_i_11_1\,
      I4 => \^ab_nudge\(22),
      I5 => \axi_rdata[31]_i_5\,
      O => \^sg_ab_64_p3_reg_4\
    );
\axi_rdata[23]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000F000F044F077"
    )
        port map (
      I0 => \^ab_nudge\(29),
      I1 => \axi_rdata[23]_i_11\,
      I2 => \axi_rdata[23]_i_11_0\,
      I3 => \axi_rdata[23]_i_11_1\,
      I4 => \^ab_nudge\(21),
      I5 => \axi_rdata[31]_i_5\,
      O => \^sg_ab_64_p3_reg_3\
    );
\axi_rdata[23]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000F000F044F077"
    )
        port map (
      I0 => \^ab_nudge\(28),
      I1 => \axi_rdata[23]_i_11\,
      I2 => \axi_rdata[23]_i_11_0\,
      I3 => \axi_rdata[23]_i_11_1\,
      I4 => \^ab_nudge\(20),
      I5 => \axi_rdata[31]_i_5\,
      O => \^sg_ab_64_p3_reg_2\
    );
\axi_rdata[23]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[27]_i_7\(2),
      O => \slv_reg4_reg[7]_13\
    );
\axi_rdata[27]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[31]_i_8\(1),
      O => \slv_reg4_reg[7]_16\
    );
\axi_rdata[27]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[31]_i_8\(0),
      O => \slv_reg4_reg[7]_15\
    );
\axi_rdata[27]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[27]_i_7\(3),
      O => \slv_reg4_reg[7]_14\
    );
\axi_rdata[27]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[31]_i_8\(2),
      O => \slv_reg4_reg[7]_17\
    );
\axi_rdata[31]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[31]_i_7\(0),
      O => \slv_reg4_reg[7]_19\
    );
\axi_rdata[31]_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[31]_i_8\(3),
      O => \slv_reg4_reg[7]_18\
    );
\axi_rdata[3]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[3]_i_4\(0),
      O => \slv_reg4_reg[7]_5\
    );
\axi_rdata[3]_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[7]_i_7\(2),
      O => \slv_reg4_reg[7]_2\
    );
\axi_rdata[3]_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[7]_i_7\(1),
      O => \slv_reg4_reg[7]_3\
    );
\axi_rdata[3]_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[7]_i_7\(0),
      O => \slv_reg4_reg[7]_4\
    );
\axi_rdata[7]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => O(1),
      O => \slv_reg4_reg[7]_0\
    );
\axi_rdata[7]_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => O(0),
      O => \slv_reg4_reg[7]_1\
    );
\axi_rdata[7]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => \axi_rdata[7]_i_7\(3),
      O => \slv_reg4_reg[7]_6\
    );
\axi_rdata[7]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \axi_rdata[7]_i_4\,
      I1 => \^sg_ab_64_p3_reg[14]__0\,
      I2 => O(2),
      O => \slv_reg4_reg[7]\
    );
\axi_rdata_reg[15]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_rdata_reg[15]\(0),
      CO(3) => \axi_rdata_reg[15]_i_2_n_0\,
      CO(2) => \axi_rdata_reg[15]_i_2_n_1\,
      CO(1) => \axi_rdata_reg[15]_i_2_n_2\,
      CO(0) => \axi_rdata_reg[15]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \axi_rdata[19]_i_7_0\(3 downto 0),
      S(3 downto 0) => SHIFT_RIGHT(15 downto 12)
    );
\axi_rdata_reg[19]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_rdata_reg[15]_i_2_n_0\,
      CO(3) => \axi_rdata[19]_i_7_1\(0),
      CO(2) => \axi_rdata_reg[19]_i_2_n_1\,
      CO(1) => \axi_rdata_reg[19]_i_2_n_2\,
      CO(0) => \axi_rdata_reg[19]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \axi_rdata[19]_i_7_0\(7 downto 4),
      S(3) => \axi_rdata_reg[19]\(0),
      S(2 downto 0) => SHIFT_RIGHT(18 downto 16)
    );
\i__carry_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^ab_nudge\(0),
      I1 => \axi_rdata[31]_i_5\,
      O => \^sg_ab_64_p3_reg[14]__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fer_soc_bd_tflite_mbqm_0_0_tflite_core4 is
  port (
    \slv_reg4_reg[7]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg4_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg4_reg[2]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg4_reg[1]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg4_reg[2]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg4_reg[2]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg4_reg[2]_2\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \slv_reg4_reg[1]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg4_reg[2]_3\ : out STD_LOGIC;
    \slv_reg4_reg[3]\ : out STD_LOGIC;
    \slv_reg4_reg[7]_1\ : out STD_LOGIC;
    \slv_reg4_reg[0]\ : out STD_LOGIC;
    \slv_reg4_reg[1]_1\ : out STD_LOGIC;
    \minusOp_carry__4_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \_inferred__3/i__carry_0\ : in STD_LOGIC;
    \_inferred__3/i__carry_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    ab_nudge : in STD_LOGIC_VECTOR ( 29 downto 0 );
    \_inferred__3/i__carry__6_0\ : in STD_LOGIC;
    \_inferred__3/i__carry__6_1\ : in STD_LOGIC
  );
end fer_soc_bd_tflite_mbqm_0_0_tflite_core4;

architecture STRUCTURE of fer_soc_bd_tflite_mbqm_0_0_tflite_core4 is
  signal SHIFT_LEFT : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \SHIFT_RIGHT3_inferred__0/i__carry__0_n_0\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__1_n_0\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__1_n_1\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__1_n_2\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__1_n_3\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__2_n_0\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__2_n_1\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__2_n_2\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__2_n_3\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__3_n_0\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__3_n_1\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__3_n_2\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__3_n_3\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__4_n_0\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__4_n_1\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__4_n_2\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__4_n_3\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__5_n_0\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__5_n_1\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__5_n_2\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__5_n_3\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__6_n_2\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry__6_n_3\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \SHIFT_RIGHT3_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \_inferred__3/i__carry__0_n_0\ : STD_LOGIC;
  signal \_inferred__3/i__carry__0_n_1\ : STD_LOGIC;
  signal \_inferred__3/i__carry__0_n_2\ : STD_LOGIC;
  signal \_inferred__3/i__carry__0_n_3\ : STD_LOGIC;
  signal \_inferred__3/i__carry__1_n_0\ : STD_LOGIC;
  signal \_inferred__3/i__carry__1_n_1\ : STD_LOGIC;
  signal \_inferred__3/i__carry__1_n_2\ : STD_LOGIC;
  signal \_inferred__3/i__carry__1_n_3\ : STD_LOGIC;
  signal \_inferred__3/i__carry__2_n_0\ : STD_LOGIC;
  signal \_inferred__3/i__carry__2_n_1\ : STD_LOGIC;
  signal \_inferred__3/i__carry__2_n_2\ : STD_LOGIC;
  signal \_inferred__3/i__carry__2_n_3\ : STD_LOGIC;
  signal \_inferred__3/i__carry__3_n_0\ : STD_LOGIC;
  signal \_inferred__3/i__carry__3_n_1\ : STD_LOGIC;
  signal \_inferred__3/i__carry__3_n_2\ : STD_LOGIC;
  signal \_inferred__3/i__carry__3_n_3\ : STD_LOGIC;
  signal \_inferred__3/i__carry__4_n_0\ : STD_LOGIC;
  signal \_inferred__3/i__carry__4_n_1\ : STD_LOGIC;
  signal \_inferred__3/i__carry__4_n_2\ : STD_LOGIC;
  signal \_inferred__3/i__carry__4_n_3\ : STD_LOGIC;
  signal \_inferred__3/i__carry__5_n_0\ : STD_LOGIC;
  signal \_inferred__3/i__carry__5_n_1\ : STD_LOGIC;
  signal \_inferred__3/i__carry__5_n_2\ : STD_LOGIC;
  signal \_inferred__3/i__carry__5_n_3\ : STD_LOGIC;
  signal \_inferred__3/i__carry__6_n_1\ : STD_LOGIC;
  signal \_inferred__3/i__carry__6_n_2\ : STD_LOGIC;
  signal \_inferred__3/i__carry__6_n_3\ : STD_LOGIC;
  signal \_inferred__3/i__carry_n_0\ : STD_LOGIC;
  signal \_inferred__3/i__carry_n_1\ : STD_LOGIC;
  signal \_inferred__3/i__carry_n_2\ : STD_LOGIC;
  signal \_inferred__3/i__carry_n_3\ : STD_LOGIC;
  signal b : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \i__carry__0_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_1__1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_1__1_n_1\ : STD_LOGIC;
  signal \i__carry__0_i_1__1_n_2\ : STD_LOGIC;
  signal \i__carry__0_i_1__1_n_3\ : STD_LOGIC;
  signal \i__carry__0_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2__1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3__1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4__1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_1__1_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_1__1_n_1\ : STD_LOGIC;
  signal \i__carry__1_i_1__1_n_2\ : STD_LOGIC;
  signal \i__carry__1_i_1__1_n_3\ : STD_LOGIC;
  signal \i__carry__1_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_2__1_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_3__1_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_1__0_n_1\ : STD_LOGIC;
  signal \i__carry__2_i_1__0_n_2\ : STD_LOGIC;
  signal \i__carry__2_i_1__0_n_3\ : STD_LOGIC;
  signal \i__carry__2_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__3_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry__3_i_1__0_n_1\ : STD_LOGIC;
  signal \i__carry__3_i_1__0_n_2\ : STD_LOGIC;
  signal \i__carry__3_i_1__0_n_3\ : STD_LOGIC;
  signal \i__carry__3_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__3_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry__3_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__3_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry__3_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__3_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry__3_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__3_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__4_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry__4_i_1__0_n_1\ : STD_LOGIC;
  signal \i__carry__4_i_1__0_n_2\ : STD_LOGIC;
  signal \i__carry__4_i_1__0_n_3\ : STD_LOGIC;
  signal \i__carry__4_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__4_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry__4_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__4_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry__4_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__4_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry__4_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__4_i_5__0_n_0\ : STD_LOGIC;
  signal \i__carry__4_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__5_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry__5_i_1__0_n_1\ : STD_LOGIC;
  signal \i__carry__5_i_1__0_n_2\ : STD_LOGIC;
  signal \i__carry__5_i_1__0_n_3\ : STD_LOGIC;
  signal \i__carry__5_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__5_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry__5_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__5_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry__5_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__5_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry__5_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__5_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_1__0_n_2\ : STD_LOGIC;
  signal \i__carry__6_i_1__0_n_3\ : STD_LOGIC;
  signal \i__carry__6_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__6_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_1__1_n_0\ : STD_LOGIC;
  signal \i__carry_i_1__1_n_1\ : STD_LOGIC;
  signal \i__carry_i_1__1_n_2\ : STD_LOGIC;
  signal \i__carry_i_1__1_n_3\ : STD_LOGIC;
  signal \i__carry_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_2__1_n_0\ : STD_LOGIC;
  signal \i__carry_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_3__1_n_0\ : STD_LOGIC;
  signal \i__carry_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_4__1_n_0\ : STD_LOGIC;
  signal \i__carry_i_5_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_n_0\ : STD_LOGIC;
  signal \minusOp_carry__0_n_1\ : STD_LOGIC;
  signal \minusOp_carry__0_n_2\ : STD_LOGIC;
  signal \minusOp_carry__0_n_3\ : STD_LOGIC;
  signal \minusOp_carry__0_n_4\ : STD_LOGIC;
  signal \minusOp_carry__0_n_5\ : STD_LOGIC;
  signal \minusOp_carry__0_n_6\ : STD_LOGIC;
  signal \minusOp_carry__0_n_7\ : STD_LOGIC;
  signal \minusOp_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \minusOp_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \minusOp_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \minusOp_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \minusOp_carry__1_n_0\ : STD_LOGIC;
  signal \minusOp_carry__1_n_1\ : STD_LOGIC;
  signal \minusOp_carry__1_n_2\ : STD_LOGIC;
  signal \minusOp_carry__1_n_3\ : STD_LOGIC;
  signal \minusOp_carry__1_n_4\ : STD_LOGIC;
  signal \minusOp_carry__1_n_5\ : STD_LOGIC;
  signal \minusOp_carry__1_n_6\ : STD_LOGIC;
  signal \minusOp_carry__1_n_7\ : STD_LOGIC;
  signal \minusOp_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \minusOp_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \minusOp_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \minusOp_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \minusOp_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \minusOp_carry__2_n_0\ : STD_LOGIC;
  signal \minusOp_carry__2_n_1\ : STD_LOGIC;
  signal \minusOp_carry__2_n_2\ : STD_LOGIC;
  signal \minusOp_carry__2_n_3\ : STD_LOGIC;
  signal \minusOp_carry__2_n_4\ : STD_LOGIC;
  signal \minusOp_carry__2_n_5\ : STD_LOGIC;
  signal \minusOp_carry__2_n_6\ : STD_LOGIC;
  signal \minusOp_carry__2_n_7\ : STD_LOGIC;
  signal \minusOp_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \minusOp_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \minusOp_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \minusOp_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \minusOp_carry__3_n_0\ : STD_LOGIC;
  signal \minusOp_carry__3_n_1\ : STD_LOGIC;
  signal \minusOp_carry__3_n_2\ : STD_LOGIC;
  signal \minusOp_carry__3_n_3\ : STD_LOGIC;
  signal \minusOp_carry__3_n_4\ : STD_LOGIC;
  signal \minusOp_carry__3_n_5\ : STD_LOGIC;
  signal \minusOp_carry__3_n_6\ : STD_LOGIC;
  signal \minusOp_carry__3_n_7\ : STD_LOGIC;
  signal \minusOp_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \minusOp_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \minusOp_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \minusOp_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \minusOp_carry__4_i_9_n_0\ : STD_LOGIC;
  signal \minusOp_carry__4_n_0\ : STD_LOGIC;
  signal \minusOp_carry__4_n_1\ : STD_LOGIC;
  signal \minusOp_carry__4_n_2\ : STD_LOGIC;
  signal \minusOp_carry__4_n_3\ : STD_LOGIC;
  signal \minusOp_carry__4_n_4\ : STD_LOGIC;
  signal \minusOp_carry__4_n_5\ : STD_LOGIC;
  signal \minusOp_carry__4_n_6\ : STD_LOGIC;
  signal \minusOp_carry__4_n_7\ : STD_LOGIC;
  signal \minusOp_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \minusOp_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \minusOp_carry__5_i_7_n_0\ : STD_LOGIC;
  signal \minusOp_carry__5_i_8_n_0\ : STD_LOGIC;
  signal \minusOp_carry__5_i_9_n_0\ : STD_LOGIC;
  signal \minusOp_carry__5_n_0\ : STD_LOGIC;
  signal \minusOp_carry__5_n_1\ : STD_LOGIC;
  signal \minusOp_carry__5_n_2\ : STD_LOGIC;
  signal \minusOp_carry__5_n_3\ : STD_LOGIC;
  signal \minusOp_carry__5_n_4\ : STD_LOGIC;
  signal \minusOp_carry__5_n_5\ : STD_LOGIC;
  signal \minusOp_carry__5_n_6\ : STD_LOGIC;
  signal \minusOp_carry__5_n_7\ : STD_LOGIC;
  signal \minusOp_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \minusOp_carry__6_i_4_n_0\ : STD_LOGIC;
  signal \minusOp_carry__6_i_5_n_0\ : STD_LOGIC;
  signal \minusOp_carry__6_n_2\ : STD_LOGIC;
  signal \minusOp_carry__6_n_3\ : STD_LOGIC;
  signal \minusOp_carry__6_n_5\ : STD_LOGIC;
  signal \minusOp_carry__6_n_6\ : STD_LOGIC;
  signal \minusOp_carry__6_n_7\ : STD_LOGIC;
  signal minusOp_carry_i_10_n_0 : STD_LOGIC;
  signal minusOp_carry_i_11_n_0 : STD_LOGIC;
  signal minusOp_carry_i_6_n_0 : STD_LOGIC;
  signal minusOp_carry_i_7_n_0 : STD_LOGIC;
  signal minusOp_carry_i_8_n_0 : STD_LOGIC;
  signal minusOp_carry_i_9_n_0 : STD_LOGIC;
  signal minusOp_carry_n_0 : STD_LOGIC;
  signal minusOp_carry_n_1 : STD_LOGIC;
  signal minusOp_carry_n_2 : STD_LOGIC;
  signal minusOp_carry_n_3 : STD_LOGIC;
  signal minusOp_carry_n_4 : STD_LOGIC;
  signal minusOp_carry_n_5 : STD_LOGIC;
  signal minusOp_carry_n_6 : STD_LOGIC;
  signal \^slv_reg4_reg[0]\ : STD_LOGIC;
  signal \^slv_reg4_reg[1]_1\ : STD_LOGIC;
  signal \^slv_reg4_reg[2]_3\ : STD_LOGIC;
  signal \^slv_reg4_reg[3]\ : STD_LOGIC;
  signal \^slv_reg4_reg[7]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^slv_reg4_reg[7]_1\ : STD_LOGIC;
  signal \NLW_SHIFT_RIGHT3_inferred__0/i__carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_SHIFT_RIGHT3_inferred__0/i__carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW__inferred__3/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW__inferred__3/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW__inferred__3/i__carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW__inferred__3/i__carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW__inferred__3/i__carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW__inferred__3/i__carry__4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW__inferred__3/i__carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW__inferred__3/i__carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_i__carry__6_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_i__carry__6_i_1__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_minusOp_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_minusOp_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \SHIFT_RIGHT3_inferred__0/i__carry\ : label is 35;
  attribute ADDER_THRESHOLD of \SHIFT_RIGHT3_inferred__0/i__carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \SHIFT_RIGHT3_inferred__0/i__carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \SHIFT_RIGHT3_inferred__0/i__carry__2\ : label is 35;
  attribute ADDER_THRESHOLD of \SHIFT_RIGHT3_inferred__0/i__carry__3\ : label is 35;
  attribute ADDER_THRESHOLD of \SHIFT_RIGHT3_inferred__0/i__carry__4\ : label is 35;
  attribute ADDER_THRESHOLD of \SHIFT_RIGHT3_inferred__0/i__carry__5\ : label is 35;
  attribute ADDER_THRESHOLD of \SHIFT_RIGHT3_inferred__0/i__carry__6\ : label is 35;
  attribute ADDER_THRESHOLD of minusOp_carry : label is 35;
  attribute ADDER_THRESHOLD of \minusOp_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \minusOp_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \minusOp_carry__2\ : label is 35;
  attribute ADDER_THRESHOLD of \minusOp_carry__3\ : label is 35;
  attribute ADDER_THRESHOLD of \minusOp_carry__4\ : label is 35;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \minusOp_carry__4_i_9\ : label is "soft_lutpair19";
  attribute ADDER_THRESHOLD of \minusOp_carry__5\ : label is 35;
  attribute ADDER_THRESHOLD of \minusOp_carry__6\ : label is 35;
  attribute SOFT_HLUTNM of minusOp_carry_i_13 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of minusOp_carry_i_14 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of minusOp_carry_i_15 : label is "soft_lutpair18";
begin
  \slv_reg4_reg[0]\ <= \^slv_reg4_reg[0]\;
  \slv_reg4_reg[1]_1\ <= \^slv_reg4_reg[1]_1\;
  \slv_reg4_reg[2]_3\ <= \^slv_reg4_reg[2]_3\;
  \slv_reg4_reg[3]\ <= \^slv_reg4_reg[3]\;
  \slv_reg4_reg[7]\(0) <= \^slv_reg4_reg[7]\(0);
  \slv_reg4_reg[7]_1\ <= \^slv_reg4_reg[7]_1\;
\SHIFT_RIGHT3_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \SHIFT_RIGHT3_inferred__0/i__carry_n_0\,
      CO(2) => \SHIFT_RIGHT3_inferred__0/i__carry_n_1\,
      CO(1) => \SHIFT_RIGHT3_inferred__0/i__carry_n_2\,
      CO(0) => \SHIFT_RIGHT3_inferred__0/i__carry_n_3\,
      CYINIT => SHIFT_LEFT(0),
      DI(3 downto 0) => B"1111",
      O(3 downto 0) => \slv_reg4_reg[7]_0\(3 downto 0),
      S(3) => \i__carry_i_1__0_n_0\,
      S(2) => \i__carry_i_2__0_n_0\,
      S(1) => \i__carry_i_3__0_n_0\,
      S(0) => \i__carry_i_4__0_n_0\
    );
\SHIFT_RIGHT3_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \SHIFT_RIGHT3_inferred__0/i__carry_n_0\,
      CO(3) => \SHIFT_RIGHT3_inferred__0/i__carry__0_n_0\,
      CO(2) => \SHIFT_RIGHT3_inferred__0/i__carry__0_n_1\,
      CO(1) => \SHIFT_RIGHT3_inferred__0/i__carry__0_n_2\,
      CO(0) => \SHIFT_RIGHT3_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"1111",
      O(3 downto 0) => O(3 downto 0),
      S(3) => \i__carry__0_i_1__0_n_0\,
      S(2) => \i__carry__0_i_2__0_n_0\,
      S(1) => \i__carry__0_i_3__0_n_0\,
      S(0) => \i__carry__0_i_4__0_n_0\
    );
\SHIFT_RIGHT3_inferred__0/i__carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \SHIFT_RIGHT3_inferred__0/i__carry__0_n_0\,
      CO(3) => \SHIFT_RIGHT3_inferred__0/i__carry__1_n_0\,
      CO(2) => \SHIFT_RIGHT3_inferred__0/i__carry__1_n_1\,
      CO(1) => \SHIFT_RIGHT3_inferred__0/i__carry__1_n_2\,
      CO(0) => \SHIFT_RIGHT3_inferred__0/i__carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"1111",
      O(3 downto 0) => \slv_reg4_reg[2]\(3 downto 0),
      S(3) => \i__carry__1_i_1__0_n_0\,
      S(2) => \i__carry__1_i_2__0_n_0\,
      S(1) => \i__carry__1_i_3__0_n_0\,
      S(0) => \i__carry__1_i_4_n_0\
    );
\SHIFT_RIGHT3_inferred__0/i__carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \SHIFT_RIGHT3_inferred__0/i__carry__1_n_0\,
      CO(3) => \SHIFT_RIGHT3_inferred__0/i__carry__2_n_0\,
      CO(2) => \SHIFT_RIGHT3_inferred__0/i__carry__2_n_1\,
      CO(1) => \SHIFT_RIGHT3_inferred__0/i__carry__2_n_2\,
      CO(0) => \SHIFT_RIGHT3_inferred__0/i__carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"1111",
      O(3 downto 0) => \slv_reg4_reg[1]\(3 downto 0),
      S(3) => \i__carry__2_i_1_n_0\,
      S(2) => \i__carry__2_i_2_n_0\,
      S(1) => \i__carry__2_i_3_n_0\,
      S(0) => \i__carry__2_i_4_n_0\
    );
\SHIFT_RIGHT3_inferred__0/i__carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \SHIFT_RIGHT3_inferred__0/i__carry__2_n_0\,
      CO(3) => \SHIFT_RIGHT3_inferred__0/i__carry__3_n_0\,
      CO(2) => \SHIFT_RIGHT3_inferred__0/i__carry__3_n_1\,
      CO(1) => \SHIFT_RIGHT3_inferred__0/i__carry__3_n_2\,
      CO(0) => \SHIFT_RIGHT3_inferred__0/i__carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"1111",
      O(3 downto 0) => \slv_reg4_reg[2]_0\(3 downto 0),
      S(3) => \i__carry__3_i_1_n_0\,
      S(2) => \i__carry__3_i_2_n_0\,
      S(1) => \i__carry__3_i_3_n_0\,
      S(0) => \i__carry__3_i_4_n_0\
    );
\SHIFT_RIGHT3_inferred__0/i__carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \SHIFT_RIGHT3_inferred__0/i__carry__3_n_0\,
      CO(3) => \SHIFT_RIGHT3_inferred__0/i__carry__4_n_0\,
      CO(2) => \SHIFT_RIGHT3_inferred__0/i__carry__4_n_1\,
      CO(1) => \SHIFT_RIGHT3_inferred__0/i__carry__4_n_2\,
      CO(0) => \SHIFT_RIGHT3_inferred__0/i__carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry__4_i_1_n_0\,
      DI(2 downto 0) => B"111",
      O(3 downto 0) => \slv_reg4_reg[2]_1\(3 downto 0),
      S(3) => \i__carry__4_i_2_n_0\,
      S(2) => \i__carry__4_i_3_n_0\,
      S(1) => \i__carry__4_i_4_n_0\,
      S(0) => \i__carry__4_i_5_n_0\
    );
\SHIFT_RIGHT3_inferred__0/i__carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \SHIFT_RIGHT3_inferred__0/i__carry__4_n_0\,
      CO(3) => \SHIFT_RIGHT3_inferred__0/i__carry__5_n_0\,
      CO(2) => \SHIFT_RIGHT3_inferred__0/i__carry__5_n_1\,
      CO(1) => \SHIFT_RIGHT3_inferred__0/i__carry__5_n_2\,
      CO(0) => \SHIFT_RIGHT3_inferred__0/i__carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"1111",
      O(3 downto 0) => \slv_reg4_reg[2]_2\(3 downto 0),
      S(3) => \i__carry__5_i_1_n_0\,
      S(2) => \i__carry__5_i_2_n_0\,
      S(1) => \i__carry__5_i_3_n_0\,
      S(0) => \i__carry__5_i_4_n_0\
    );
\SHIFT_RIGHT3_inferred__0/i__carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \SHIFT_RIGHT3_inferred__0/i__carry__5_n_0\,
      CO(3 downto 2) => \NLW_SHIFT_RIGHT3_inferred__0/i__carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \SHIFT_RIGHT3_inferred__0/i__carry__6_n_2\,
      CO(0) => \SHIFT_RIGHT3_inferred__0/i__carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0011",
      O(3) => \NLW_SHIFT_RIGHT3_inferred__0/i__carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => \slv_reg4_reg[1]_0\(2 downto 0),
      S(3) => '0',
      S(2) => \i__carry__6_i_1_n_0\,
      S(1) => \i__carry__6_i_2_n_0\,
      S(0) => \i__carry__6_i_3_n_0\
    );
\_inferred__3/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \_inferred__3/i__carry_n_0\,
      CO(2) => \_inferred__3/i__carry_n_1\,
      CO(1) => \_inferred__3/i__carry_n_2\,
      CO(0) => \_inferred__3/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => b(3 downto 0),
      O(3 downto 0) => \NLW__inferred__3/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry_i_2__1_n_0\,
      S(2) => \i__carry_i_3__1_n_0\,
      S(1) => \i__carry_i_4__1_n_0\,
      S(0) => \i__carry_i_5_n_0\
    );
\_inferred__3/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \_inferred__3/i__carry_n_0\,
      CO(3) => \_inferred__3/i__carry__0_n_0\,
      CO(2) => \_inferred__3/i__carry__0_n_1\,
      CO(1) => \_inferred__3/i__carry__0_n_2\,
      CO(0) => \_inferred__3/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(7 downto 4),
      O(3 downto 0) => \NLW__inferred__3/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__0_i_2__1_n_0\,
      S(2) => \i__carry__0_i_3__1_n_0\,
      S(1) => \i__carry__0_i_4__1_n_0\,
      S(0) => \i__carry__0_i_5_n_0\
    );
\_inferred__3/i__carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \_inferred__3/i__carry__0_n_0\,
      CO(3) => \_inferred__3/i__carry__1_n_0\,
      CO(2) => \_inferred__3/i__carry__1_n_1\,
      CO(1) => \_inferred__3/i__carry__1_n_2\,
      CO(0) => \_inferred__3/i__carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(11 downto 8),
      O(3 downto 0) => \NLW__inferred__3/i__carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__1_i_2__1_n_0\,
      S(2) => \i__carry__1_i_3__1_n_0\,
      S(1) => \i__carry__1_i_4__0_n_0\,
      S(0) => \i__carry__1_i_5_n_0\
    );
\_inferred__3/i__carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \_inferred__3/i__carry__1_n_0\,
      CO(3) => \_inferred__3/i__carry__2_n_0\,
      CO(2) => \_inferred__3/i__carry__2_n_1\,
      CO(1) => \_inferred__3/i__carry__2_n_2\,
      CO(0) => \_inferred__3/i__carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(15 downto 12),
      O(3 downto 0) => \NLW__inferred__3/i__carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__2_i_2__0_n_0\,
      S(2) => \i__carry__2_i_3__0_n_0\,
      S(1) => \i__carry__2_i_4__0_n_0\,
      S(0) => \i__carry__2_i_5_n_0\
    );
\_inferred__3/i__carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \_inferred__3/i__carry__2_n_0\,
      CO(3) => \_inferred__3/i__carry__3_n_0\,
      CO(2) => \_inferred__3/i__carry__3_n_1\,
      CO(1) => \_inferred__3/i__carry__3_n_2\,
      CO(0) => \_inferred__3/i__carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(19 downto 16),
      O(3 downto 0) => \NLW__inferred__3/i__carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__3_i_2__0_n_0\,
      S(2) => \i__carry__3_i_3__0_n_0\,
      S(1) => \i__carry__3_i_4__0_n_0\,
      S(0) => \i__carry__3_i_5_n_0\
    );
\_inferred__3/i__carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \_inferred__3/i__carry__3_n_0\,
      CO(3) => \_inferred__3/i__carry__4_n_0\,
      CO(2) => \_inferred__3/i__carry__4_n_1\,
      CO(1) => \_inferred__3/i__carry__4_n_2\,
      CO(0) => \_inferred__3/i__carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(23 downto 20),
      O(3 downto 0) => \NLW__inferred__3/i__carry__4_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__4_i_2__0_n_0\,
      S(2) => \i__carry__4_i_3__0_n_0\,
      S(1) => \i__carry__4_i_4__0_n_0\,
      S(0) => \i__carry__4_i_5__0_n_0\
    );
\_inferred__3/i__carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \_inferred__3/i__carry__4_n_0\,
      CO(3) => \_inferred__3/i__carry__5_n_0\,
      CO(2) => \_inferred__3/i__carry__5_n_1\,
      CO(1) => \_inferred__3/i__carry__5_n_2\,
      CO(0) => \_inferred__3/i__carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => b(27 downto 24),
      O(3 downto 0) => \NLW__inferred__3/i__carry__5_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__5_i_2__0_n_0\,
      S(2) => \i__carry__5_i_3__0_n_0\,
      S(1) => \i__carry__5_i_4__0_n_0\,
      S(0) => \i__carry__5_i_5_n_0\
    );
\_inferred__3/i__carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \_inferred__3/i__carry__5_n_0\,
      CO(3) => CO(0),
      CO(2) => \_inferred__3/i__carry__6_n_1\,
      CO(1) => \_inferred__3/i__carry__6_n_2\,
      CO(0) => \_inferred__3/i__carry__6_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry__6_i_1__0_n_0\,
      DI(2 downto 0) => b(30 downto 28),
      O(3 downto 0) => \NLW__inferred__3/i__carry__6_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__6_i_2__0_n_0\,
      S(2) => \i__carry__6_i_3__0_n_0\,
      S(1) => \i__carry__6_i_4_n_0\,
      S(0) => \i__carry__6_i_5_n_0\
    );
\i__carry__0_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAFFFE"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(2),
      I4 => minusOp_carry_i_10_n_0,
      O => \i__carry__0_i_1__0_n_0\
    );
\i__carry__0_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \i__carry_i_1__1_n_0\,
      CO(3) => \i__carry__0_i_1__1_n_0\,
      CO(2) => \i__carry__0_i_1__1_n_1\,
      CO(1) => \i__carry__0_i_1__1_n_2\,
      CO(0) => \i__carry__0_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => b(7 downto 4),
      S(3) => \minusOp_carry__0_n_4\,
      S(2) => \minusOp_carry__0_n_5\,
      S(1) => \minusOp_carry__0_n_6\,
      S(0) => \minusOp_carry__0_n_7\
    );
\i__carry__0_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => minusOp_carry_i_10_n_0,
      O => \i__carry__0_i_2__0_n_0\
    );
\i__carry__0_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(7),
      I1 => \minusOp_carry__0_n_5\,
      I2 => ab_nudge(6),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__0_i_2__1_n_0\
    );
\i__carry__0_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \^slv_reg4_reg[0]\,
      I2 => \minusOp_carry__4_0\(0),
      I3 => \^slv_reg4_reg[1]_1\,
      O => \i__carry__0_i_3__0_n_0\
    );
\i__carry__0_i_3__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(6),
      I1 => \minusOp_carry__0_n_6\,
      I2 => ab_nudge(5),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__0_i_3__1_n_0\
    );
\i__carry__0_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => minusOp_carry_i_10_n_0,
      O => \i__carry__0_i_4__0_n_0\
    );
\i__carry__0_i_4__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(5),
      I1 => \minusOp_carry__0_n_7\,
      I2 => ab_nudge(4),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__0_i_4__1_n_0\
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(4),
      I1 => minusOp_carry_n_4,
      I2 => ab_nudge(3),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__0_i_5_n_0\
    );
\i__carry__1_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFB"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => minusOp_carry_i_10_n_0,
      O => \i__carry__1_i_1__0_n_0\
    );
\i__carry__1_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \i__carry__0_i_1__1_n_0\,
      CO(3) => \i__carry__1_i_1__1_n_0\,
      CO(2) => \i__carry__1_i_1__1_n_1\,
      CO(1) => \i__carry__1_i_1__1_n_2\,
      CO(0) => \i__carry__1_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => b(11 downto 8),
      S(3) => \minusOp_carry__1_n_4\,
      S(2) => \minusOp_carry__1_n_5\,
      S(1) => \minusOp_carry__1_n_6\,
      S(0) => \minusOp_carry__1_n_7\
    );
\i__carry__1_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => \i__carry__1_i_2__0_n_0\
    );
\i__carry__1_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(11),
      I1 => \minusOp_carry__1_n_5\,
      I2 => ab_nudge(10),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__1_i_2__1_n_0\
    );
\i__carry__1_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \^slv_reg4_reg[1]_1\,
      I2 => \^slv_reg4_reg[0]\,
      I3 => \minusOp_carry__4_0\(0),
      O => \i__carry__1_i_3__0_n_0\
    );
\i__carry__1_i_3__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(10),
      I1 => \minusOp_carry__1_n_6\,
      I2 => ab_nudge(9),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__1_i_3__1_n_0\
    );
\i__carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => \i__carry__1_i_4_n_0\
    );
\i__carry__1_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(9),
      I1 => \minusOp_carry__1_n_7\,
      I2 => ab_nudge(8),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__1_i_4__0_n_0\
    );
\i__carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(8),
      I1 => \minusOp_carry__0_n_4\,
      I2 => ab_nudge(7),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__1_i_5_n_0\
    );
\i__carry__2_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAFFFE"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(2),
      I4 => minusOp_carry_i_10_n_0,
      O => \i__carry__2_i_1_n_0\
    );
\i__carry__2_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \i__carry__1_i_1__1_n_0\,
      CO(3) => \i__carry__2_i_1__0_n_0\,
      CO(2) => \i__carry__2_i_1__0_n_1\,
      CO(1) => \i__carry__2_i_1__0_n_2\,
      CO(0) => \i__carry__2_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => b(15 downto 12),
      S(3) => \minusOp_carry__2_n_4\,
      S(2) => \minusOp_carry__2_n_5\,
      S(1) => \minusOp_carry__2_n_6\,
      S(0) => \minusOp_carry__2_n_7\
    );
\i__carry__2_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => minusOp_carry_i_10_n_0,
      O => \i__carry__2_i_2_n_0\
    );
\i__carry__2_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(15),
      I1 => \minusOp_carry__2_n_5\,
      I2 => ab_nudge(14),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__2_i_2__0_n_0\
    );
\i__carry__2_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \^slv_reg4_reg[0]\,
      I2 => \minusOp_carry__4_0\(0),
      I3 => \^slv_reg4_reg[1]_1\,
      O => \i__carry__2_i_3_n_0\
    );
\i__carry__2_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(14),
      I1 => \minusOp_carry__2_n_6\,
      I2 => ab_nudge(13),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__2_i_3__0_n_0\
    );
\i__carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => minusOp_carry_i_10_n_0,
      O => \i__carry__2_i_4_n_0\
    );
\i__carry__2_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(13),
      I1 => \minusOp_carry__2_n_7\,
      I2 => ab_nudge(12),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__2_i_4__0_n_0\
    );
\i__carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(12),
      I1 => \minusOp_carry__1_n_4\,
      I2 => ab_nudge(11),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__2_i_5_n_0\
    );
\i__carry__3_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFB"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => minusOp_carry_i_10_n_0,
      O => \i__carry__3_i_1_n_0\
    );
\i__carry__3_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \i__carry__2_i_1__0_n_0\,
      CO(3) => \i__carry__3_i_1__0_n_0\,
      CO(2) => \i__carry__3_i_1__0_n_1\,
      CO(1) => \i__carry__3_i_1__0_n_2\,
      CO(0) => \i__carry__3_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => b(19 downto 16),
      S(3) => \minusOp_carry__3_n_4\,
      S(2) => \minusOp_carry__3_n_5\,
      S(1) => \minusOp_carry__3_n_6\,
      S(0) => \minusOp_carry__3_n_7\
    );
\i__carry__3_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => \i__carry__3_i_2_n_0\
    );
\i__carry__3_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(19),
      I1 => \minusOp_carry__3_n_5\,
      I2 => ab_nudge(18),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__3_i_2__0_n_0\
    );
\i__carry__3_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \^slv_reg4_reg[1]_1\,
      I2 => \^slv_reg4_reg[0]\,
      I3 => \minusOp_carry__4_0\(0),
      O => \i__carry__3_i_3_n_0\
    );
\i__carry__3_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(18),
      I1 => \minusOp_carry__3_n_6\,
      I2 => ab_nudge(17),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__3_i_3__0_n_0\
    );
\i__carry__3_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => \i__carry__3_i_4_n_0\
    );
\i__carry__3_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(17),
      I1 => \minusOp_carry__3_n_7\,
      I2 => ab_nudge(16),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__3_i_4__0_n_0\
    );
\i__carry__3_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(16),
      I1 => \minusOp_carry__2_n_4\,
      I2 => ab_nudge(15),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__3_i_5_n_0\
    );
\i__carry__4_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44444445"
    )
        port map (
      I0 => \minusOp_carry__5_i_9_n_0\,
      I1 => minusOp_carry_i_10_n_0,
      I2 => \minusOp_carry__4_0\(2),
      I3 => \minusOp_carry__4_0\(0),
      I4 => \minusOp_carry__4_0\(1),
      O => \i__carry__4_i_1_n_0\
    );
\i__carry__4_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \i__carry__3_i_1__0_n_0\,
      CO(3) => \i__carry__4_i_1__0_n_0\,
      CO(2) => \i__carry__4_i_1__0_n_1\,
      CO(1) => \i__carry__4_i_1__0_n_2\,
      CO(0) => \i__carry__4_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => b(23 downto 20),
      S(3) => \minusOp_carry__4_n_4\,
      S(2) => \minusOp_carry__4_n_5\,
      S(1) => \minusOp_carry__4_n_6\,
      S(0) => \minusOp_carry__4_n_7\
    );
\i__carry__4_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF00FE"
    )
        port map (
      I0 => \minusOp_carry__4_0\(1),
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(2),
      I3 => minusOp_carry_i_10_n_0,
      I4 => \minusOp_carry__5_i_9_n_0\,
      O => \i__carry__4_i_2_n_0\
    );
\i__carry__4_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(23),
      I1 => \minusOp_carry__4_n_5\,
      I2 => ab_nudge(22),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__4_i_2__0_n_0\
    );
\i__carry__4_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFB"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(1),
      I3 => minusOp_carry_i_10_n_0,
      I4 => \minusOp_carry__2_i_9_n_0\,
      O => \i__carry__4_i_3_n_0\
    );
\i__carry__4_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(22),
      I1 => \minusOp_carry__4_n_6\,
      I2 => ab_nudge(21),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__4_i_3__0_n_0\
    );
\i__carry__4_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \^slv_reg4_reg[0]\,
      I2 => \minusOp_carry__4_0\(0),
      I3 => \^slv_reg4_reg[1]_1\,
      O => \i__carry__4_i_4_n_0\
    );
\i__carry__4_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(21),
      I1 => \minusOp_carry__4_n_7\,
      I2 => ab_nudge(20),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__4_i_4__0_n_0\
    );
\i__carry__4_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => minusOp_carry_i_10_n_0,
      O => \i__carry__4_i_5_n_0\
    );
\i__carry__4_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(20),
      I1 => \minusOp_carry__3_n_4\,
      I2 => ab_nudge(19),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__4_i_5__0_n_0\
    );
\i__carry__5_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => minusOp_carry_i_10_n_0,
      I4 => \minusOp_carry__5_i_9_n_0\,
      O => \i__carry__5_i_1_n_0\
    );
\i__carry__5_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \i__carry__4_i_1__0_n_0\,
      CO(3) => \i__carry__5_i_1__0_n_0\,
      CO(2) => \i__carry__5_i_1__0_n_1\,
      CO(1) => \i__carry__5_i_1__0_n_2\,
      CO(0) => \i__carry__5_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => b(27 downto 24),
      S(3) => \minusOp_carry__5_n_4\,
      S(2) => \minusOp_carry__5_n_5\,
      S(1) => \minusOp_carry__5_n_6\,
      S(0) => \minusOp_carry__5_n_7\
    );
\i__carry__5_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFDFF"
    )
        port map (
      I0 => \minusOp_carry__4_0\(0),
      I1 => minusOp_carry_i_10_n_0,
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(2),
      I4 => \minusOp_carry__5_i_9_n_0\,
      O => \i__carry__5_i_2_n_0\
    );
\i__carry__5_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(27),
      I1 => \minusOp_carry__5_n_5\,
      I2 => ab_nudge(26),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__5_i_2__0_n_0\
    );
\i__carry__5_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFB"
    )
        port map (
      I0 => \^slv_reg4_reg[1]_1\,
      I1 => \^slv_reg4_reg[0]\,
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__5_i_9_n_0\,
      O => \i__carry__5_i_3_n_0\
    );
\i__carry__5_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(26),
      I1 => \minusOp_carry__5_n_6\,
      I2 => ab_nudge(25),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__5_i_3__0_n_0\
    );
\i__carry__5_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFDFFF"
    )
        port map (
      I0 => \minusOp_carry__4_0\(0),
      I1 => minusOp_carry_i_10_n_0,
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(2),
      I4 => \minusOp_carry__5_i_9_n_0\,
      O => \i__carry__5_i_4_n_0\
    );
\i__carry__5_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(25),
      I1 => \minusOp_carry__5_n_7\,
      I2 => ab_nudge(24),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__5_i_4__0_n_0\
    );
\i__carry__5_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(24),
      I1 => \minusOp_carry__4_n_4\,
      I2 => ab_nudge(23),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__5_i_5_n_0\
    );
\i__carry__6_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => \minusOp_carry__5_i_9_n_0\,
      I1 => minusOp_carry_i_10_n_0,
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => \minusOp_carry__4_0\(2),
      O => \i__carry__6_i_1_n_0\
    );
\i__carry__6_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \i__carry__5_i_1__0_n_0\,
      CO(3) => \i__carry__6_i_1__0_n_0\,
      CO(2) => \NLW_i__carry__6_i_1__0_CO_UNCONNECTED\(2),
      CO(1) => \i__carry__6_i_1__0_n_2\,
      CO(0) => \i__carry__6_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0100",
      O(3) => \NLW_i__carry__6_i_1__0_O_UNCONNECTED\(3),
      O(2 downto 0) => b(30 downto 28),
      S(3) => '1',
      S(2) => \minusOp_carry__6_n_5\,
      S(1) => \minusOp_carry__6_n_6\,
      S(0) => \minusOp_carry__6_n_7\
    );
\i__carry__6_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => \^slv_reg4_reg[0]\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => \^slv_reg4_reg[1]_1\,
      I3 => \minusOp_carry__5_i_9_n_0\,
      O => \i__carry__6_i_2_n_0\
    );
\i__carry__6_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"9A"
    )
        port map (
      I0 => \i__carry__6_i_1__0_n_0\,
      I1 => \_inferred__3/i__carry__6_1\,
      I2 => \minusOp_carry__6_n_5\,
      O => \i__carry__6_i_2__0_n_0\
    );
\i__carry__6_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFBF"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(1),
      I3 => minusOp_carry_i_10_n_0,
      I4 => \minusOp_carry__5_i_9_n_0\,
      O => \i__carry__6_i_3_n_0\
    );
\i__carry__6_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(30),
      I1 => \minusOp_carry__6_n_6\,
      I2 => ab_nudge(29),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__6_i_3__0_n_0\
    );
\i__carry__6_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(29),
      I1 => \minusOp_carry__6_n_7\,
      I2 => ab_nudge(28),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__6_i_4_n_0\
    );
\i__carry__6_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(28),
      I1 => \minusOp_carry__5_n_4\,
      I2 => ab_nudge(27),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry__6_i_5_n_0\
    );
\i__carry_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFB"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => minusOp_carry_i_10_n_0,
      O => \i__carry_i_1__0_n_0\
    );
\i__carry_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \i__carry_i_1__1_n_0\,
      CO(2) => \i__carry_i_1__1_n_1\,
      CO(1) => \i__carry_i_1__1_n_2\,
      CO(0) => \i__carry_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \^slv_reg4_reg[7]\(0),
      O(3 downto 0) => b(3 downto 0),
      S(3) => minusOp_carry_n_4,
      S(2) => minusOp_carry_n_5,
      S(1) => minusOp_carry_n_6,
      S(0) => \_inferred__3/i__carry_1\(0)
    );
\i__carry_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => \i__carry_i_2__0_n_0\
    );
\i__carry_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(3),
      I1 => minusOp_carry_n_5,
      I2 => ab_nudge(2),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry_i_2__1_n_0\
    );
\i__carry_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \^slv_reg4_reg[1]_1\,
      I2 => \^slv_reg4_reg[0]\,
      I3 => \minusOp_carry__4_0\(0),
      O => \i__carry_i_3__0_n_0\
    );
\i__carry_i_3__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(2),
      I1 => minusOp_carry_n_6,
      I2 => ab_nudge(1),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry_i_3__1_n_0\
    );
\i__carry_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => \i__carry_i_4__0_n_0\
    );
\i__carry_i_4__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9995"
    )
        port map (
      I0 => b(1),
      I1 => \^slv_reg4_reg[7]\(0),
      I2 => ab_nudge(0),
      I3 => \_inferred__3/i__carry__6_0\,
      O => \i__carry_i_4__1_n_0\
    );
\i__carry_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"65"
    )
        port map (
      I0 => b(0),
      I1 => \_inferred__3/i__carry_0\,
      I2 => \minusOp_carry__4_0\(7),
      O => \i__carry_i_5_n_0\
    );
minusOp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => minusOp_carry_n_0,
      CO(2) => minusOp_carry_n_1,
      CO(1) => minusOp_carry_n_2,
      CO(0) => minusOp_carry_n_3,
      CYINIT => SHIFT_LEFT(0),
      DI(3 downto 0) => SHIFT_LEFT(4 downto 1),
      O(3) => minusOp_carry_n_4,
      O(2) => minusOp_carry_n_5,
      O(1) => minusOp_carry_n_6,
      O(0) => \^slv_reg4_reg[7]\(0),
      S(3) => minusOp_carry_i_6_n_0,
      S(2) => minusOp_carry_i_7_n_0,
      S(1) => minusOp_carry_i_8_n_0,
      S(0) => minusOp_carry_i_9_n_0
    );
\minusOp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => minusOp_carry_n_0,
      CO(3) => \minusOp_carry__0_n_0\,
      CO(2) => \minusOp_carry__0_n_1\,
      CO(1) => \minusOp_carry__0_n_2\,
      CO(0) => \minusOp_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => SHIFT_LEFT(8 downto 5),
      O(3) => \minusOp_carry__0_n_4\,
      O(2) => \minusOp_carry__0_n_5\,
      O(1) => \minusOp_carry__0_n_6\,
      O(0) => \minusOp_carry__0_n_7\,
      S(3) => \minusOp_carry__0_i_5_n_0\,
      S(2) => \minusOp_carry__0_i_6_n_0\,
      S(1) => \minusOp_carry__0_i_7_n_0\,
      S(0) => \minusOp_carry__0_i_8_n_0\
    );
\minusOp_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AAAB"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__0_i_9_n_0\,
      O => SHIFT_LEFT(8)
    );
\minusOp_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(2),
      I4 => minusOp_carry_i_11_n_0,
      O => SHIFT_LEFT(7)
    );
\minusOp_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \^slv_reg4_reg[1]_1\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => \^slv_reg4_reg[0]\,
      I3 => minusOp_carry_i_11_n_0,
      O => SHIFT_LEFT(6)
    );
\minusOp_carry__0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000040"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(2),
      I4 => minusOp_carry_i_11_n_0,
      O => SHIFT_LEFT(5)
    );
\minusOp_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAFFFE"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(2),
      I4 => minusOp_carry_i_10_n_0,
      O => \minusOp_carry__0_i_5_n_0\
    );
\minusOp_carry__0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => minusOp_carry_i_10_n_0,
      O => \minusOp_carry__0_i_6_n_0\
    );
\minusOp_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \^slv_reg4_reg[0]\,
      I2 => \minusOp_carry__4_0\(0),
      I3 => \^slv_reg4_reg[1]_1\,
      O => \minusOp_carry__0_i_7_n_0\
    );
\minusOp_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => minusOp_carry_i_10_n_0,
      O => \minusOp_carry__0_i_8_n_0\
    );
\minusOp_carry__0_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFDF00FFFFFFFF"
    )
        port map (
      I0 => \minusOp_carry__4_0\(5),
      I1 => \^slv_reg4_reg[2]_3\,
      I2 => \minusOp_carry__4_0\(6),
      I3 => \minusOp_carry__4_0\(7),
      I4 => \^slv_reg4_reg[7]_1\,
      I5 => \^slv_reg4_reg[3]\,
      O => \minusOp_carry__0_i_9_n_0\
    );
\minusOp_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \minusOp_carry__0_n_0\,
      CO(3) => \minusOp_carry__1_n_0\,
      CO(2) => \minusOp_carry__1_n_1\,
      CO(1) => \minusOp_carry__1_n_2\,
      CO(0) => \minusOp_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => SHIFT_LEFT(12 downto 9),
      O(3) => \minusOp_carry__1_n_4\,
      O(2) => \minusOp_carry__1_n_5\,
      O(1) => \minusOp_carry__1_n_6\,
      O(0) => \minusOp_carry__1_n_7\,
      S(3) => \minusOp_carry__1_i_5_n_0\,
      S(2) => \minusOp_carry__1_i_6_n_0\,
      S(1) => \minusOp_carry__1_i_7_n_0\,
      S(0) => \minusOp_carry__1_i_8_n_0\
    );
\minusOp_carry__1_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000100"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(2),
      I4 => \minusOp_carry__0_i_9_n_0\,
      O => SHIFT_LEFT(12)
    );
\minusOp_carry__1_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000200"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(1),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(0),
      I4 => \minusOp_carry__0_i_9_n_0\,
      O => SHIFT_LEFT(11)
    );
\minusOp_carry__1_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \minusOp_carry__4_0\(0),
      I1 => \^slv_reg4_reg[0]\,
      I2 => \^slv_reg4_reg[1]_1\,
      I3 => \minusOp_carry__0_i_9_n_0\,
      O => SHIFT_LEFT(10)
    );
\minusOp_carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000800"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(1),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(0),
      I4 => \minusOp_carry__0_i_9_n_0\,
      O => SHIFT_LEFT(9)
    );
\minusOp_carry__1_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFB"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => minusOp_carry_i_10_n_0,
      O => \minusOp_carry__1_i_5_n_0\
    );
\minusOp_carry__1_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => \minusOp_carry__1_i_6_n_0\
    );
\minusOp_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \^slv_reg4_reg[1]_1\,
      I2 => \^slv_reg4_reg[0]\,
      I3 => \minusOp_carry__4_0\(0),
      O => \minusOp_carry__1_i_7_n_0\
    );
\minusOp_carry__1_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => \minusOp_carry__1_i_8_n_0\
    );
\minusOp_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \minusOp_carry__1_n_0\,
      CO(3) => \minusOp_carry__2_n_0\,
      CO(2) => \minusOp_carry__2_n_1\,
      CO(1) => \minusOp_carry__2_n_2\,
      CO(0) => \minusOp_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => SHIFT_LEFT(16 downto 13),
      O(3) => \minusOp_carry__2_n_4\,
      O(2) => \minusOp_carry__2_n_5\,
      O(1) => \minusOp_carry__2_n_6\,
      O(0) => \minusOp_carry__2_n_7\,
      S(3) => \minusOp_carry__2_i_5_n_0\,
      S(2) => \minusOp_carry__2_i_6_n_0\,
      S(1) => \minusOp_carry__2_i_7_n_0\,
      S(0) => \minusOp_carry__2_i_8_n_0\
    );
\minusOp_carry__2_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AAAB"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__2_i_9_n_0\,
      O => SHIFT_LEFT(16)
    );
\minusOp_carry__2_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(2),
      I4 => \minusOp_carry__0_i_9_n_0\,
      O => SHIFT_LEFT(15)
    );
\minusOp_carry__2_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \^slv_reg4_reg[1]_1\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => \^slv_reg4_reg[0]\,
      I3 => \minusOp_carry__0_i_9_n_0\,
      O => SHIFT_LEFT(14)
    );
\minusOp_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000040"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(2),
      I4 => \minusOp_carry__0_i_9_n_0\,
      O => SHIFT_LEFT(13)
    );
\minusOp_carry__2_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAFFFE"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(2),
      I4 => minusOp_carry_i_10_n_0,
      O => \minusOp_carry__2_i_5_n_0\
    );
\minusOp_carry__2_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => minusOp_carry_i_10_n_0,
      O => \minusOp_carry__2_i_6_n_0\
    );
\minusOp_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \^slv_reg4_reg[0]\,
      I2 => \minusOp_carry__4_0\(0),
      I3 => \^slv_reg4_reg[1]_1\,
      O => \minusOp_carry__2_i_7_n_0\
    );
\minusOp_carry__2_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => \minusOp_carry__0_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => minusOp_carry_i_10_n_0,
      O => \minusOp_carry__2_i_8_n_0\
    );
\minusOp_carry__2_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFDF00FFFFFFFF"
    )
        port map (
      I0 => \minusOp_carry__4_0\(5),
      I1 => \^slv_reg4_reg[2]_3\,
      I2 => \minusOp_carry__4_0\(6),
      I3 => \minusOp_carry__4_0\(7),
      I4 => \^slv_reg4_reg[3]\,
      I5 => \^slv_reg4_reg[7]_1\,
      O => \minusOp_carry__2_i_9_n_0\
    );
\minusOp_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \minusOp_carry__2_n_0\,
      CO(3) => \minusOp_carry__3_n_0\,
      CO(2) => \minusOp_carry__3_n_1\,
      CO(1) => \minusOp_carry__3_n_2\,
      CO(0) => \minusOp_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => SHIFT_LEFT(20 downto 17),
      O(3) => \minusOp_carry__3_n_4\,
      O(2) => \minusOp_carry__3_n_5\,
      O(1) => \minusOp_carry__3_n_6\,
      O(0) => \minusOp_carry__3_n_7\,
      S(3) => \minusOp_carry__3_i_5_n_0\,
      S(2) => \minusOp_carry__3_i_6_n_0\,
      S(1) => \minusOp_carry__3_i_7_n_0\,
      S(0) => \minusOp_carry__3_i_8_n_0\
    );
\minusOp_carry__3_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000100"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(2),
      I4 => \minusOp_carry__2_i_9_n_0\,
      O => SHIFT_LEFT(20)
    );
\minusOp_carry__3_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000200"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(1),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(0),
      I4 => \minusOp_carry__2_i_9_n_0\,
      O => SHIFT_LEFT(19)
    );
\minusOp_carry__3_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \minusOp_carry__4_0\(0),
      I1 => \^slv_reg4_reg[0]\,
      I2 => \^slv_reg4_reg[1]_1\,
      I3 => \minusOp_carry__2_i_9_n_0\,
      O => SHIFT_LEFT(18)
    );
\minusOp_carry__3_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000800"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(1),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(0),
      I4 => \minusOp_carry__2_i_9_n_0\,
      O => SHIFT_LEFT(17)
    );
\minusOp_carry__3_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFB"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => minusOp_carry_i_10_n_0,
      O => \minusOp_carry__3_i_5_n_0\
    );
\minusOp_carry__3_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => \minusOp_carry__3_i_6_n_0\
    );
\minusOp_carry__3_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \^slv_reg4_reg[1]_1\,
      I2 => \^slv_reg4_reg[0]\,
      I3 => \minusOp_carry__4_0\(0),
      O => \minusOp_carry__3_i_7_n_0\
    );
\minusOp_carry__3_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => \minusOp_carry__3_i_8_n_0\
    );
\minusOp_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \minusOp_carry__3_n_0\,
      CO(3) => \minusOp_carry__4_n_0\,
      CO(2) => \minusOp_carry__4_n_1\,
      CO(1) => \minusOp_carry__4_n_2\,
      CO(0) => \minusOp_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => SHIFT_LEFT(24 downto 21),
      O(3) => \minusOp_carry__4_n_4\,
      O(2) => \minusOp_carry__4_n_5\,
      O(1) => \minusOp_carry__4_n_6\,
      O(0) => \minusOp_carry__4_n_7\,
      S(3) => \minusOp_carry__4_i_5_n_0\,
      S(2) => \minusOp_carry__4_i_6_n_0\,
      S(1) => \minusOp_carry__4_i_7_n_0\,
      S(0) => \minusOp_carry__4_i_8_n_0\
    );
\minusOp_carry__4_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000080000000000"
    )
        port map (
      I0 => \minusOp_carry__4_0\(7),
      I1 => \minusOp_carry__4_0\(6),
      I2 => \minusOp_carry__4_0\(4),
      I3 => \minusOp_carry__4_0\(3),
      I4 => \minusOp_carry__4_i_9_n_0\,
      I5 => \minusOp_carry__4_0\(5),
      O => SHIFT_LEFT(24)
    );
\minusOp_carry__4_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000100"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => minusOp_carry_i_10_n_0,
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => \minusOp_carry__4_0\(2),
      O => SHIFT_LEFT(23)
    );
\minusOp_carry__4_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \^slv_reg4_reg[1]_1\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => \^slv_reg4_reg[0]\,
      I3 => \minusOp_carry__2_i_9_n_0\,
      O => SHIFT_LEFT(22)
    );
\minusOp_carry__4_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000040"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(2),
      I4 => \minusOp_carry__2_i_9_n_0\,
      O => SHIFT_LEFT(21)
    );
\minusOp_carry__4_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => \minusOp_carry__4_0\(5),
      I1 => \minusOp_carry__4_i_9_n_0\,
      I2 => \minusOp_carry__4_0\(3),
      I3 => \minusOp_carry__4_0\(4),
      I4 => \minusOp_carry__4_0\(6),
      I5 => \minusOp_carry__4_0\(7),
      O => \minusOp_carry__4_i_5_n_0\
    );
\minusOp_carry__4_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFB"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(1),
      I3 => minusOp_carry_i_10_n_0,
      I4 => \minusOp_carry__2_i_9_n_0\,
      O => \minusOp_carry__4_i_6_n_0\
    );
\minusOp_carry__4_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \^slv_reg4_reg[0]\,
      I2 => \minusOp_carry__4_0\(0),
      I3 => \^slv_reg4_reg[1]_1\,
      O => \minusOp_carry__4_i_7_n_0\
    );
\minusOp_carry__4_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => \minusOp_carry__2_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => minusOp_carry_i_10_n_0,
      O => \minusOp_carry__4_i_8_n_0\
    );
\minusOp_carry__4_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(1),
      O => \minusOp_carry__4_i_9_n_0\
    );
\minusOp_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \minusOp_carry__4_n_0\,
      CO(3) => \minusOp_carry__5_n_0\,
      CO(2) => \minusOp_carry__5_n_1\,
      CO(1) => \minusOp_carry__5_n_2\,
      CO(0) => \minusOp_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => SHIFT_LEFT(28 downto 25),
      O(3) => \minusOp_carry__5_n_4\,
      O(2) => \minusOp_carry__5_n_5\,
      O(1) => \minusOp_carry__5_n_6\,
      O(0) => \minusOp_carry__5_n_7\,
      S(3) => \minusOp_carry__5_i_5_n_0\,
      S(2) => \minusOp_carry__5_i_6_n_0\,
      S(1) => \minusOp_carry__5_i_7_n_0\,
      S(0) => \minusOp_carry__5_i_8_n_0\
    );
\minusOp_carry__5_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => \minusOp_carry__5_i_9_n_0\,
      I1 => minusOp_carry_i_10_n_0,
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => SHIFT_LEFT(28)
    );
\minusOp_carry__5_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00040000"
    )
        port map (
      I0 => \minusOp_carry__5_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(1),
      I3 => minusOp_carry_i_10_n_0,
      I4 => \minusOp_carry__4_0\(0),
      O => SHIFT_LEFT(27)
    );
\minusOp_carry__5_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => \minusOp_carry__5_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => \^slv_reg4_reg[0]\,
      I3 => \^slv_reg4_reg[1]_1\,
      O => SHIFT_LEFT(26)
    );
\minusOp_carry__5_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00400000"
    )
        port map (
      I0 => \minusOp_carry__5_i_9_n_0\,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(1),
      I3 => minusOp_carry_i_10_n_0,
      I4 => \minusOp_carry__4_0\(0),
      O => SHIFT_LEFT(25)
    );
\minusOp_carry__5_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => minusOp_carry_i_10_n_0,
      I4 => \minusOp_carry__5_i_9_n_0\,
      O => \minusOp_carry__5_i_5_n_0\
    );
\minusOp_carry__5_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFDFF"
    )
        port map (
      I0 => \minusOp_carry__4_0\(0),
      I1 => minusOp_carry_i_10_n_0,
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(2),
      I4 => \minusOp_carry__5_i_9_n_0\,
      O => \minusOp_carry__5_i_6_n_0\
    );
\minusOp_carry__5_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFB"
    )
        port map (
      I0 => \^slv_reg4_reg[1]_1\,
      I1 => \^slv_reg4_reg[0]\,
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__5_i_9_n_0\,
      O => \minusOp_carry__5_i_7_n_0\
    );
\minusOp_carry__5_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFDFFF"
    )
        port map (
      I0 => \minusOp_carry__4_0\(0),
      I1 => minusOp_carry_i_10_n_0,
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(2),
      I4 => \minusOp_carry__5_i_9_n_0\,
      O => \minusOp_carry__5_i_8_n_0\
    );
\minusOp_carry__5_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DF00FFFFFFFFFFFF"
    )
        port map (
      I0 => \minusOp_carry__4_0\(5),
      I1 => \^slv_reg4_reg[2]_3\,
      I2 => \minusOp_carry__4_0\(6),
      I3 => \minusOp_carry__4_0\(7),
      I4 => \^slv_reg4_reg[7]_1\,
      I5 => \^slv_reg4_reg[3]\,
      O => \minusOp_carry__5_i_9_n_0\
    );
\minusOp_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \minusOp_carry__5_n_0\,
      CO(3 downto 2) => \NLW_minusOp_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \minusOp_carry__6_n_2\,
      CO(0) => \minusOp_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => SHIFT_LEFT(30 downto 29),
      O(3) => \NLW_minusOp_carry__6_O_UNCONNECTED\(3),
      O(2) => \minusOp_carry__6_n_5\,
      O(1) => \minusOp_carry__6_n_6\,
      O(0) => \minusOp_carry__6_n_7\,
      S(3) => '0',
      S(2) => \minusOp_carry__6_i_3_n_0\,
      S(1) => \minusOp_carry__6_i_4_n_0\,
      S(0) => \minusOp_carry__6_i_5_n_0\
    );
\minusOp_carry__6_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => \minusOp_carry__5_i_9_n_0\,
      I1 => \^slv_reg4_reg[1]_1\,
      I2 => \minusOp_carry__4_0\(0),
      I3 => \^slv_reg4_reg[0]\,
      O => SHIFT_LEFT(30)
    );
\minusOp_carry__6_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00001000"
    )
        port map (
      I0 => \minusOp_carry__5_i_9_n_0\,
      I1 => minusOp_carry_i_10_n_0,
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => \minusOp_carry__4_0\(2),
      O => SHIFT_LEFT(29)
    );
\minusOp_carry__6_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => \minusOp_carry__5_i_9_n_0\,
      I1 => minusOp_carry_i_10_n_0,
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => \minusOp_carry__4_0\(2),
      O => \minusOp_carry__6_i_3_n_0\
    );
\minusOp_carry__6_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => \^slv_reg4_reg[0]\,
      I1 => \minusOp_carry__4_0\(0),
      I2 => \^slv_reg4_reg[1]_1\,
      I3 => \minusOp_carry__5_i_9_n_0\,
      O => \minusOp_carry__6_i_4_n_0\
    );
\minusOp_carry__6_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFBF"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(1),
      I3 => minusOp_carry_i_10_n_0,
      I4 => \minusOp_carry__5_i_9_n_0\,
      O => \minusOp_carry__6_i_5_n_0\
    );
minusOp_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \minusOp_carry__4_0\(7),
      O => SHIFT_LEFT(0)
    );
minusOp_carry_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5545"
    )
        port map (
      I0 => \minusOp_carry__4_0\(7),
      I1 => \minusOp_carry__4_0\(6),
      I2 => \^slv_reg4_reg[2]_3\,
      I3 => \minusOp_carry__4_0\(5),
      O => minusOp_carry_i_10_n_0
    );
minusOp_carry_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFDF00"
    )
        port map (
      I0 => \minusOp_carry__4_0\(5),
      I1 => \^slv_reg4_reg[2]_3\,
      I2 => \minusOp_carry__4_0\(6),
      I3 => \minusOp_carry__4_0\(7),
      I4 => \^slv_reg4_reg[3]\,
      I5 => \^slv_reg4_reg[7]_1\,
      O => minusOp_carry_i_11_n_0
    );
minusOp_carry_i_12: unisim.vcomponents.LUT3
    generic map(
      INIT => X"14"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(1),
      O => \^slv_reg4_reg[0]\
    );
minusOp_carry_i_13: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0154"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(1),
      I2 => \minusOp_carry__4_0\(0),
      I3 => \minusOp_carry__4_0\(2),
      O => \^slv_reg4_reg[1]_1\
    );
minusOp_carry_i_14: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(3),
      I4 => \minusOp_carry__4_0\(4),
      O => \^slv_reg4_reg[2]_3\
    );
minusOp_carry_i_15: unisim.vcomponents.LUT5
    generic map(
      INIT => X"11111114"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(3),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => \minusOp_carry__4_0\(2),
      O => \^slv_reg4_reg[3]\
    );
minusOp_carry_i_16: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222228"
    )
        port map (
      I0 => \minusOp_carry__4_0\(7),
      I1 => \minusOp_carry__4_0\(4),
      I2 => \minusOp_carry__4_0\(3),
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(0),
      I5 => \minusOp_carry__4_0\(2),
      O => \^slv_reg4_reg[7]_1\
    );
minusOp_carry_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000100"
    )
        port map (
      I0 => minusOp_carry_i_10_n_0,
      I1 => \minusOp_carry__4_0\(0),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(2),
      I4 => minusOp_carry_i_11_n_0,
      O => SHIFT_LEFT(4)
    );
minusOp_carry_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000200"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(1),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(0),
      I4 => minusOp_carry_i_11_n_0,
      O => SHIFT_LEFT(3)
    );
minusOp_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \minusOp_carry__4_0\(0),
      I1 => \^slv_reg4_reg[0]\,
      I2 => \^slv_reg4_reg[1]_1\,
      I3 => minusOp_carry_i_11_n_0,
      O => SHIFT_LEFT(2)
    );
minusOp_carry_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000800"
    )
        port map (
      I0 => \minusOp_carry__4_0\(2),
      I1 => \minusOp_carry__4_0\(1),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(0),
      I4 => minusOp_carry_i_11_n_0,
      O => SHIFT_LEFT(1)
    );
minusOp_carry_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFB"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \minusOp_carry__4_0\(2),
      I2 => \minusOp_carry__4_0\(1),
      I3 => \minusOp_carry__4_0\(0),
      I4 => minusOp_carry_i_10_n_0,
      O => minusOp_carry_i_6_n_0
    );
minusOp_carry_i_7: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFBFFFF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => minusOp_carry_i_7_n_0
    );
minusOp_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \^slv_reg4_reg[1]_1\,
      I2 => \^slv_reg4_reg[0]\,
      I3 => \minusOp_carry__4_0\(0),
      O => minusOp_carry_i_8_n_0
    );
minusOp_carry_i_9: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFFFFFF"
    )
        port map (
      I0 => minusOp_carry_i_11_n_0,
      I1 => \minusOp_carry__4_0\(0),
      I2 => minusOp_carry_i_10_n_0,
      I3 => \minusOp_carry__4_0\(1),
      I4 => \minusOp_carry__4_0\(2),
      O => minusOp_carry_i_9_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm is
  port (
    \slv_reg4_reg[2]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata[31]_i_8\ : out STD_LOGIC_VECTOR ( 30 downto 0 );
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aclk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \sg_a_p0_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \axi_rdata_reg[3]_i_2\ : in STD_LOGIC;
    \axi_rdata[19]_i_5\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \axi_rdata_reg[31]_i_3\ : in STD_LOGIC;
    sel0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_rdata_reg[0]\ : in STD_LOGIC;
    \qq_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \sg_a_p0_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \axi_rdata_reg[3]_i_2_0\ : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
end fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm;

architecture STRUCTURE of fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm is
  signal SHIFT_RIGHT : STD_LOGIC_VECTOR ( 19 downto 10 );
  signal SM0_n_32 : STD_LOGIC;
  signal SM0_n_33 : STD_LOGIC;
  signal SM0_n_36 : STD_LOGIC;
  signal SM0_n_37 : STD_LOGIC;
  signal SM0_n_38 : STD_LOGIC;
  signal SM0_n_39 : STD_LOGIC;
  signal SM0_n_40 : STD_LOGIC;
  signal SM0_n_41 : STD_LOGIC;
  signal SM0_n_42 : STD_LOGIC;
  signal SM0_n_43 : STD_LOGIC;
  signal SM0_n_44 : STD_LOGIC;
  signal SM0_n_68 : STD_LOGIC;
  signal SM0_n_69 : STD_LOGIC;
  signal SM1_n_64 : STD_LOGIC;
  signal SM1_n_65 : STD_LOGIC;
  signal SM1_n_66 : STD_LOGIC;
  signal SM1_n_67 : STD_LOGIC;
  signal SM1_n_68 : STD_LOGIC;
  signal SM1_n_69 : STD_LOGIC;
  signal SM1_n_70 : STD_LOGIC;
  signal SM2_n_32 : STD_LOGIC;
  signal SM2_n_33 : STD_LOGIC;
  signal SM2_n_34 : STD_LOGIC;
  signal SM2_n_35 : STD_LOGIC;
  signal SM2_n_36 : STD_LOGIC;
  signal SM2_n_37 : STD_LOGIC;
  signal SM2_n_38 : STD_LOGIC;
  signal SM2_n_39 : STD_LOGIC;
  signal SM2_n_42 : STD_LOGIC;
  signal SM2_n_43 : STD_LOGIC;
  signal SM2_n_44 : STD_LOGIC;
  signal SM2_n_45 : STD_LOGIC;
  signal SM2_n_46 : STD_LOGIC;
  signal SM2_n_47 : STD_LOGIC;
  signal SM2_n_48 : STD_LOGIC;
  signal SM2_n_49 : STD_LOGIC;
  signal SM2_n_50 : STD_LOGIC;
  signal SM2_n_51 : STD_LOGIC;
  signal SM2_n_52 : STD_LOGIC;
  signal SM2_n_53 : STD_LOGIC;
  signal SM2_n_54 : STD_LOGIC;
  signal SM2_n_55 : STD_LOGIC;
  signal SM2_n_56 : STD_LOGIC;
  signal SM2_n_57 : STD_LOGIC;
  signal SM2_n_58 : STD_LOGIC;
  signal SM2_n_59 : STD_LOGIC;
  signal SM2_n_60 : STD_LOGIC;
  signal SM2_n_61 : STD_LOGIC;
  signal SM2_n_70 : STD_LOGIC;
  signal SM4_n_0 : STD_LOGIC;
  signal SM4_n_1 : STD_LOGIC;
  signal SM4_n_10 : STD_LOGIC;
  signal SM4_n_11 : STD_LOGIC;
  signal SM4_n_12 : STD_LOGIC;
  signal SM4_n_13 : STD_LOGIC;
  signal SM4_n_14 : STD_LOGIC;
  signal SM4_n_15 : STD_LOGIC;
  signal SM4_n_16 : STD_LOGIC;
  signal SM4_n_17 : STD_LOGIC;
  signal SM4_n_18 : STD_LOGIC;
  signal SM4_n_19 : STD_LOGIC;
  signal SM4_n_2 : STD_LOGIC;
  signal SM4_n_20 : STD_LOGIC;
  signal SM4_n_21 : STD_LOGIC;
  signal SM4_n_22 : STD_LOGIC;
  signal SM4_n_23 : STD_LOGIC;
  signal SM4_n_24 : STD_LOGIC;
  signal SM4_n_25 : STD_LOGIC;
  signal SM4_n_26 : STD_LOGIC;
  signal SM4_n_27 : STD_LOGIC;
  signal SM4_n_28 : STD_LOGIC;
  signal SM4_n_29 : STD_LOGIC;
  signal SM4_n_3 : STD_LOGIC;
  signal SM4_n_30 : STD_LOGIC;
  signal SM4_n_31 : STD_LOGIC;
  signal SM4_n_32 : STD_LOGIC;
  signal SM4_n_34 : STD_LOGIC;
  signal SM4_n_35 : STD_LOGIC;
  signal SM4_n_36 : STD_LOGIC;
  signal SM4_n_37 : STD_LOGIC;
  signal SM4_n_4 : STD_LOGIC;
  signal SM4_n_5 : STD_LOGIC;
  signal SM4_n_6 : STD_LOGIC;
  signal SM4_n_7 : STD_LOGIC;
  signal SM4_n_8 : STD_LOGIC;
  signal SM4_n_9 : STD_LOGIC;
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ab_nudge : STD_LOGIC_VECTOR ( 63 downto 31 );
  signal \axi_rdata[0]_i_4_n_0\ : STD_LOGIC;
  signal dd : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal qq : STD_LOGIC;
  signal \qq[6]_i_3_n_0\ : STD_LOGIC;
  signal \qq[6]_i_4_n_0\ : STD_LOGIC;
  signal qq_reg : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal sg_ab_64_p3_reg : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal \^slv_reg4_reg[2]\ : STD_LOGIC;
  signal xls : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \qq[1]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \qq[2]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \qq[3]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \qq[4]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \qq[6]_i_3\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \qq[6]_i_4\ : label is "soft_lutpair20";
begin
  SR(0) <= \^sr\(0);
  \slv_reg4_reg[2]\ <= \^slv_reg4_reg[2]\;
SM0: entity work.fer_soc_bd_tflite_mbqm_0_0_tflite_core0
     port map (
      CO(0) => SM4_n_32,
      D(0) => D(0),
      O(1) => SM4_n_29,
      O(0) => SM4_n_30,
      Q(31 downto 0) => Q(31 downto 0),
      S(3) => SM1_n_64,
      S(2) => SM1_n_65,
      S(1) => SM1_n_66,
      S(0) => SM1_n_67,
      ab_nudge(31) => ab_nudge(63),
      ab_nudge(30 downto 0) => ab_nudge(61 downto 31),
      \axi_rdata[11]_i_5\ => SM2_n_43,
      \axi_rdata[11]_i_7_0\(0) => SM0_n_68,
      \axi_rdata[11]_i_7_1\ => SM2_n_42,
      \axi_rdata[19]_i_4_0\ => SM2_n_45,
      \axi_rdata[19]_i_5\(3 downto 1) => \axi_rdata[19]_i_5\(7 downto 5),
      \axi_rdata[19]_i_5\(0) => \axi_rdata[19]_i_5\(0),
      \axi_rdata[19]_i_5_0\ => \^slv_reg4_reg[2]\,
      \axi_rdata[19]_i_5_1\ => SM2_n_44,
      \axi_rdata[23]_i_6_0\ => SM2_n_47,
      \axi_rdata[23]_i_7_0\ => SM2_n_46,
      \axi_rdata[27]_i_15_0\ => SM4_n_34,
      \axi_rdata[31]_i_17_0\ => SM4_n_37,
      \axi_rdata[31]_i_7_0\ => SM4_n_36,
      \axi_rdata[31]_i_8_0\(22 downto 11) => \axi_rdata[31]_i_8\(30 downto 19),
      \axi_rdata[31]_i_8_0\(10 downto 0) => \axi_rdata[31]_i_8\(10 downto 0),
      \axi_rdata[3]_i_22_0\ => SM4_n_35,
      \axi_rdata_reg[0]\ => \axi_rdata_reg[0]\,
      \axi_rdata_reg[0]_0\ => \axi_rdata[0]_i_4_n_0\,
      \axi_rdata_reg[11]\(1 downto 0) => SHIFT_RIGHT(11 downto 10),
      \axi_rdata_reg[11]_i_2_0\ => SM2_n_49,
      \axi_rdata_reg[11]_i_2_1\ => SM2_n_50,
      \axi_rdata_reg[19]_i_2\ => SM2_n_51,
      \axi_rdata_reg[23]\(0) => SM2_n_70,
      \axi_rdata_reg[23]_i_2_0\ => SM2_n_52,
      \axi_rdata_reg[23]_i_2_1\ => SM2_n_53,
      \axi_rdata_reg[23]_i_2_2\ => SM2_n_54,
      \axi_rdata_reg[23]_i_2_3\ => SM2_n_55,
      \axi_rdata_reg[27]_i_2_0\ => SM2_n_56,
      \axi_rdata_reg[27]_i_2_1\ => SM2_n_57,
      \axi_rdata_reg[27]_i_2_2\ => SM2_n_58,
      \axi_rdata_reg[27]_i_2_3\ => SM2_n_59,
      \axi_rdata_reg[31]_i_3_0\ => \axi_rdata_reg[31]_i_3\,
      \axi_rdata_reg[31]_i_3_1\ => SM2_n_60,
      \axi_rdata_reg[31]_i_3_2\ => SM2_n_61,
      \axi_rdata_reg[31]_i_3_3\ => SM2_n_33,
      \axi_rdata_reg[3]_i_2_0\ => SM2_n_39,
      \axi_rdata_reg[3]_i_2_1\ => \axi_rdata_reg[3]_i_2\,
      \axi_rdata_reg[3]_i_2_2\ => \axi_rdata_reg[3]_i_2_0\,
      \axi_rdata_reg[3]_i_2_3\ => SM2_n_38,
      \axi_rdata_reg[3]_i_2_4\ => SM2_n_37,
      \axi_rdata_reg[3]_i_2_5\ => SM2_n_36,
      \axi_rdata_reg[7]_i_2_0\ => SM2_n_48,
      \axi_rdata_reg[7]_i_2_1\ => SM2_n_35,
      \axi_rdata_reg[7]_i_2_2\ => SM2_n_34,
      \axi_rdata_reg[7]_i_2_3\ => SM2_n_32,
      \i__carry_i_1__1\(0) => SM4_n_0,
      sel0(2 downto 0) => sel0(2 downto 0),
      \sg_a_p0_reg[27]\(27 downto 0) => \sg_a_p0_reg[31]_0\(27 downto 0),
      \sg_a_p0_reg[31]\(30 downto 0) => \sg_a_p0_reg[31]\(30 downto 0),
      sg_ab_64_p3_reg(0) => SM0_n_69,
      \slv_reg1_reg[11]\ => SM0_n_33,
      \slv_reg1_reg[23]\ => SM0_n_44,
      \slv_reg4_reg[0]\ => SM0_n_40,
      \slv_reg4_reg[0]_0\ => SM0_n_41,
      \slv_reg4_reg[3]\ => SM0_n_42,
      \slv_reg4_reg[3]_0\ => SM0_n_43,
      \slv_reg4_reg[5]\ => SM0_n_32,
      \slv_reg4_reg[5]_0\(0) => SHIFT_RIGHT(19),
      \slv_reg4_reg[7]\ => SM0_n_36,
      \slv_reg4_reg[7]_0\ => SM0_n_37,
      \slv_reg4_reg[7]_1\ => SM0_n_38,
      \slv_reg4_reg[7]_2\ => SM0_n_39,
      xls(31 downto 0) => xls(31 downto 0)
    );
SM1: entity work.fer_soc_bd_tflite_mbqm_0_0_tflite_core1
     port map (
      Q(31 downto 0) => Q(31 downto 0),
      S(3) => SM1_n_64,
      S(2) => SM1_n_65,
      S(1) => SM1_n_66,
      S(0) => SM1_n_67,
      s00_axi_aclk => s00_axi_aclk,
      \sg_a_p0_reg[31]_0\(3 downto 0) => \sg_a_p0_reg[31]_0\(31 downto 28),
      \sg_a_p0_reg[31]_1\(3 downto 0) => \sg_a_p0_reg[31]\(31 downto 28),
      \sg_ab_64_p3_reg[0]_0\(0) => SM1_n_70,
      \sg_ab_64_p3_reg[14]__0_0\(1) => SM1_n_68,
      \sg_ab_64_p3_reg[14]__0_0\(0) => SM1_n_69,
      sg_ab_64_p3_reg_0(63 downto 0) => sg_ab_64_p3_reg(63 downto 0),
      xls(31 downto 0) => xls(31 downto 0)
    );
SM2: entity work.fer_soc_bd_tflite_mbqm_0_0_tflite_core2
     port map (
      O(3) => SM4_n_5,
      O(2) => SM4_n_6,
      O(1) => SM4_n_7,
      O(0) => SM4_n_8,
      ab_nudge(31) => ab_nudge(63),
      ab_nudge(30 downto 0) => ab_nudge(61 downto 31),
      \ab_nudge_carry__0_0\(0) => SM1_n_70,
      \axi_rdata[11]_i_4_0\ => SM0_n_37,
      \axi_rdata[11]_i_5_0\ => SM0_n_36,
      \axi_rdata[15]_i_21\(63 downto 0) => sg_ab_64_p3_reg(63 downto 0),
      \axi_rdata[15]_i_6_0\ => SM0_n_39,
      \axi_rdata[15]_i_7_0\ => SM0_n_38,
      \axi_rdata[15]_i_7_1\(3) => SM4_n_9,
      \axi_rdata[15]_i_7_1\(2) => SM4_n_10,
      \axi_rdata[15]_i_7_1\(1) => SM4_n_11,
      \axi_rdata[15]_i_7_1\(0) => SM4_n_12,
      \axi_rdata[19]_i_6_0\ => SM4_n_36,
      \axi_rdata[19]_i_6_1\ => SM4_n_37,
      \axi_rdata[19]_i_6_2\ => SM0_n_43,
      \axi_rdata[19]_i_7_0\(7 downto 0) => \axi_rdata[31]_i_8\(18 downto 11),
      \axi_rdata[19]_i_7_1\(0) => SM2_n_70,
      \axi_rdata[19]_i_7_2\ => SM0_n_42,
      \axi_rdata[19]_i_7_3\(3) => SM4_n_13,
      \axi_rdata[19]_i_7_3\(2) => SM4_n_14,
      \axi_rdata[19]_i_7_3\(1) => SM4_n_15,
      \axi_rdata[19]_i_7_3\(0) => SM4_n_16,
      \axi_rdata[23]_i_11\ => SM4_n_34,
      \axi_rdata[23]_i_11_0\ => SM0_n_33,
      \axi_rdata[23]_i_11_1\ => SM4_n_35,
      \axi_rdata[23]_i_7\(3) => SM4_n_17,
      \axi_rdata[23]_i_7\(2) => SM4_n_18,
      \axi_rdata[23]_i_7\(1) => SM4_n_19,
      \axi_rdata[23]_i_7\(0) => SM4_n_20,
      \axi_rdata[27]_i_7\(3) => SM4_n_21,
      \axi_rdata[27]_i_7\(2) => SM4_n_22,
      \axi_rdata[27]_i_7\(1) => SM4_n_23,
      \axi_rdata[27]_i_7\(0) => SM4_n_24,
      \axi_rdata[31]_i_5\ => SM0_n_44,
      \axi_rdata[31]_i_7\(0) => SM4_n_31,
      \axi_rdata[31]_i_8\(3) => SM4_n_25,
      \axi_rdata[31]_i_8\(2) => SM4_n_26,
      \axi_rdata[31]_i_8\(1) => SM4_n_27,
      \axi_rdata[31]_i_8\(0) => SM4_n_28,
      \axi_rdata[3]_i_19\(1) => SM1_n_68,
      \axi_rdata[3]_i_19\(0) => SM1_n_69,
      \axi_rdata[3]_i_4\(0) => \axi_rdata[19]_i_5\(7),
      \axi_rdata[7]_i_4\ => \axi_rdata_reg[31]_i_3\,
      \axi_rdata[7]_i_7\(3) => SM4_n_1,
      \axi_rdata[7]_i_7\(2) => SM4_n_2,
      \axi_rdata[7]_i_7\(1) => SM4_n_3,
      \axi_rdata[7]_i_7\(0) => SM4_n_4,
      \axi_rdata_reg[11]_i_2\ => SM0_n_40,
      \axi_rdata_reg[11]_i_2_0\ => \axi_rdata_reg[3]_i_2_0\,
      \axi_rdata_reg[11]_i_2_1\ => \axi_rdata_reg[3]_i_2\,
      \axi_rdata_reg[15]\(0) => SM0_n_68,
      \axi_rdata_reg[19]\(0) => SHIFT_RIGHT(19),
      \axi_rdata_reg[19]_i_2_0\ => SM0_n_32,
      \axi_rdata_reg[19]_i_2_1\ => SM0_n_41,
      sg_ab_64_p3_reg => SM2_n_42,
      \sg_ab_64_p3_reg[14]__0\ => SM2_n_33,
      sg_ab_64_p3_reg_0 => SM2_n_43,
      sg_ab_64_p3_reg_1 => SM2_n_44,
      sg_ab_64_p3_reg_2 => SM2_n_45,
      sg_ab_64_p3_reg_3 => SM2_n_46,
      sg_ab_64_p3_reg_4 => SM2_n_47,
      \slv_reg4_reg[5]\(1 downto 0) => SHIFT_RIGHT(11 downto 10),
      \slv_reg4_reg[7]\ => SM2_n_32,
      \slv_reg4_reg[7]_0\ => SM2_n_34,
      \slv_reg4_reg[7]_1\ => SM2_n_35,
      \slv_reg4_reg[7]_10\ => SM2_n_52,
      \slv_reg4_reg[7]_11\ => SM2_n_53,
      \slv_reg4_reg[7]_12\ => SM2_n_54,
      \slv_reg4_reg[7]_13\ => SM2_n_55,
      \slv_reg4_reg[7]_14\ => SM2_n_56,
      \slv_reg4_reg[7]_15\ => SM2_n_57,
      \slv_reg4_reg[7]_16\ => SM2_n_58,
      \slv_reg4_reg[7]_17\ => SM2_n_59,
      \slv_reg4_reg[7]_18\ => SM2_n_60,
      \slv_reg4_reg[7]_19\ => SM2_n_61,
      \slv_reg4_reg[7]_2\ => SM2_n_36,
      \slv_reg4_reg[7]_3\ => SM2_n_37,
      \slv_reg4_reg[7]_4\ => SM2_n_38,
      \slv_reg4_reg[7]_5\ => SM2_n_39,
      \slv_reg4_reg[7]_6\ => SM2_n_48,
      \slv_reg4_reg[7]_7\ => SM2_n_49,
      \slv_reg4_reg[7]_8\ => SM2_n_50,
      \slv_reg4_reg[7]_9\ => SM2_n_51
    );
SM4: entity work.fer_soc_bd_tflite_mbqm_0_0_tflite_core4
     port map (
      CO(0) => SM4_n_32,
      O(3) => SM4_n_5,
      O(2) => SM4_n_6,
      O(1) => SM4_n_7,
      O(0) => SM4_n_8,
      \_inferred__3/i__carry_0\ => SM2_n_33,
      \_inferred__3/i__carry_1\(0) => SM0_n_69,
      \_inferred__3/i__carry__6_0\ => SM0_n_44,
      \_inferred__3/i__carry__6_1\ => SM0_n_33,
      ab_nudge(29 downto 0) => ab_nudge(61 downto 32),
      \minusOp_carry__4_0\(7 downto 0) => \axi_rdata[19]_i_5\(7 downto 0),
      \slv_reg4_reg[0]\ => SM4_n_36,
      \slv_reg4_reg[1]\(3) => SM4_n_13,
      \slv_reg4_reg[1]\(2) => SM4_n_14,
      \slv_reg4_reg[1]\(1) => SM4_n_15,
      \slv_reg4_reg[1]\(0) => SM4_n_16,
      \slv_reg4_reg[1]_0\(2) => SM4_n_29,
      \slv_reg4_reg[1]_0\(1) => SM4_n_30,
      \slv_reg4_reg[1]_0\(0) => SM4_n_31,
      \slv_reg4_reg[1]_1\ => SM4_n_37,
      \slv_reg4_reg[2]\(3) => SM4_n_9,
      \slv_reg4_reg[2]\(2) => SM4_n_10,
      \slv_reg4_reg[2]\(1) => SM4_n_11,
      \slv_reg4_reg[2]\(0) => SM4_n_12,
      \slv_reg4_reg[2]_0\(3) => SM4_n_17,
      \slv_reg4_reg[2]_0\(2) => SM4_n_18,
      \slv_reg4_reg[2]_0\(1) => SM4_n_19,
      \slv_reg4_reg[2]_0\(0) => SM4_n_20,
      \slv_reg4_reg[2]_1\(3) => SM4_n_21,
      \slv_reg4_reg[2]_1\(2) => SM4_n_22,
      \slv_reg4_reg[2]_1\(1) => SM4_n_23,
      \slv_reg4_reg[2]_1\(0) => SM4_n_24,
      \slv_reg4_reg[2]_2\(3) => SM4_n_25,
      \slv_reg4_reg[2]_2\(2) => SM4_n_26,
      \slv_reg4_reg[2]_2\(1) => SM4_n_27,
      \slv_reg4_reg[2]_2\(0) => SM4_n_28,
      \slv_reg4_reg[2]_3\ => \^slv_reg4_reg[2]\,
      \slv_reg4_reg[3]\ => SM4_n_34,
      \slv_reg4_reg[7]\(0) => SM4_n_0,
      \slv_reg4_reg[7]_0\(3) => SM4_n_1,
      \slv_reg4_reg[7]_0\(2) => SM4_n_2,
      \slv_reg4_reg[7]_0\(1) => SM4_n_3,
      \slv_reg4_reg[7]_0\(0) => SM4_n_4,
      \slv_reg4_reg[7]_1\ => SM4_n_35
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => \^sr\(0)
    );
\axi_rdata[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => qq_reg(5),
      I1 => qq_reg(3),
      I2 => qq_reg(4),
      I3 => \qq[6]_i_3_n_0\,
      O => \axi_rdata[0]_i_4_n_0\
    );
\qq[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => qq_reg(0),
      O => dd(0)
    );
\qq[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => qq_reg(0),
      I1 => qq_reg(1),
      O => dd(1)
    );
\qq[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => qq_reg(2),
      I1 => qq_reg(1),
      I2 => qq_reg(0),
      O => dd(2)
    );
\qq[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => qq_reg(3),
      I1 => qq_reg(0),
      I2 => qq_reg(1),
      I3 => qq_reg(2),
      O => dd(3)
    );
\qq[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => qq_reg(4),
      I1 => qq_reg(2),
      I2 => qq_reg(1),
      I3 => qq_reg(0),
      I4 => qq_reg(3),
      O => dd(4)
    );
\qq[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => qq_reg(5),
      I1 => qq_reg(4),
      I2 => qq_reg(3),
      I3 => qq_reg(0),
      I4 => qq_reg(1),
      I5 => qq_reg(2),
      O => dd(5)
    );
\qq[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAA8AA"
    )
        port map (
      I0 => \qq_reg[0]_0\(0),
      I1 => \qq[6]_i_3_n_0\,
      I2 => qq_reg(4),
      I3 => qq_reg(3),
      I4 => qq_reg(5),
      O => qq
    );
\qq[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => qq_reg(6),
      I1 => \qq[6]_i_4_n_0\,
      I2 => qq_reg(5),
      O => dd(6)
    );
\qq[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => qq_reg(1),
      I1 => qq_reg(0),
      I2 => qq_reg(6),
      I3 => qq_reg(2),
      O => \qq[6]_i_3_n_0\
    );
\qq[6]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => qq_reg(2),
      I1 => qq_reg(1),
      I2 => qq_reg(0),
      I3 => qq_reg(3),
      I4 => qq_reg(4),
      O => \qq[6]_i_4_n_0\
    );
\qq_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => qq,
      CLR => \^sr\(0),
      D => dd(0),
      Q => qq_reg(0)
    );
\qq_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => qq,
      CLR => \^sr\(0),
      D => dd(1),
      Q => qq_reg(1)
    );
\qq_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => qq,
      CLR => \^sr\(0),
      D => dd(2),
      Q => qq_reg(2)
    );
\qq_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => qq,
      CLR => \^sr\(0),
      D => dd(3),
      Q => qq_reg(3)
    );
\qq_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => qq,
      CLR => \^sr\(0),
      D => dd(4),
      Q => qq_reg(4)
    );
\qq_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => qq,
      CLR => \^sr\(0),
      D => dd(5),
      Q => qq_reg(5)
    );
\qq_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => qq,
      CLR => \^sr\(0),
      D => dd(6),
      Q => qq_reg(6)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0_S00_AXI is
  port (
    axi_wready_reg_0 : out STD_LOGIC;
    axi_awready_reg_0 : out STD_LOGIC;
    axi_arready_reg_0 : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    aw_en_reg_0 : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    axi_bvalid_reg_0 : in STD_LOGIC;
    aw_en_reg_1 : in STD_LOGIC;
    axi_rvalid_reg_0 : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC
  );
end fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0_S00_AXI;

architecture STRUCTURE of fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0_S00_AXI is
  signal SM0_n_0 : STD_LOGIC;
  signal SM0_n_33 : STD_LOGIC;
  signal \^aw_en_reg_0\ : STD_LOGIC;
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[4]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal \^axi_arready_reg_0\ : STD_LOGIC;
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[4]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal \^axi_awready_reg_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_11_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_15_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_9_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal \^axi_wready_reg_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal slv_reg0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \slv_reg0__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg2 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg3 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg3[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg4 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \slv_reg4[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4__0\ : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal slv_reg_rden : STD_LOGIC;
  signal \slv_reg_wren__2\ : STD_LOGIC;
  signal tf_out : STD_LOGIC_VECTOR ( 31 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_11\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_15\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair24";
begin
  aw_en_reg_0 <= \^aw_en_reg_0\;
  axi_arready_reg_0 <= \^axi_arready_reg_0\;
  axi_awready_reg_0 <= \^axi_awready_reg_0\;
  axi_wready_reg_0 <= \^axi_wready_reg_0\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
SM0: entity work.fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm
     port map (
      D(0) => reg_data_out(0),
      Q(31 downto 0) => slv_reg3(31 downto 0),
      SR(0) => SM0_n_33,
      \axi_rdata[19]_i_5\(7 downto 0) => slv_reg4(7 downto 0),
      \axi_rdata[31]_i_8\(30 downto 0) => tf_out(31 downto 1),
      \axi_rdata_reg[0]\ => \axi_rdata[0]_i_2_n_0\,
      \axi_rdata_reg[31]_i_3\ => \axi_rdata[31]_i_9_n_0\,
      \axi_rdata_reg[3]_i_2\ => \axi_rdata[31]_i_15_n_0\,
      \axi_rdata_reg[3]_i_2_0\ => \axi_rdata[31]_i_11_n_0\,
      \qq_reg[0]_0\(0) => \slv_reg0__0\(0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      sel0(2 downto 0) => sel0(2 downto 0),
      \sg_a_p0_reg[31]\(31 downto 0) => slv_reg1(31 downto 0),
      \sg_a_p0_reg[31]_0\(31 downto 0) => slv_reg2(31 downto 0),
      \slv_reg4_reg[2]\ => SM0_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_reg_1,
      Q => \^aw_en_reg_0\,
      S => SM0_n_33
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(0),
      I1 => s00_axi_arvalid,
      I2 => \^axi_arready_reg_0\,
      I3 => sel0(0),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(1),
      I1 => s00_axi_arvalid,
      I2 => \^axi_arready_reg_0\,
      I3 => sel0(1),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(2),
      I1 => s00_axi_arvalid,
      I2 => \^axi_arready_reg_0\,
      I3 => sel0(2),
      O => \axi_araddr[4]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => sel0(0),
      S => SM0_n_33
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => sel0(1),
      S => SM0_n_33
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[4]_i_1_n_0\,
      Q => sel0(2),
      S => SM0_n_33
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^axi_arready_reg_0\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^axi_arready_reg_0\,
      R => SM0_n_33
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(0),
      I1 => \^aw_en_reg_0\,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^axi_awready_reg_0\,
      I5 => p_0_in(0),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(1),
      I1 => \^aw_en_reg_0\,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^axi_awready_reg_0\,
      I5 => p_0_in(1),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => s00_axi_awaddr(2),
      I1 => \^aw_en_reg_0\,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      I4 => \^axi_awready_reg_0\,
      I5 => p_0_in(2),
      O => \axi_awaddr[4]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => p_0_in(0),
      R => SM0_n_33
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => p_0_in(1),
      R => SM0_n_33
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[4]_i_1_n_0\,
      Q => p_0_in(2),
      R => SM0_n_33
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \^aw_en_reg_0\,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^axi_awready_reg_0\,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^axi_awready_reg_0\,
      R => SM0_n_33
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_reg_0,
      Q => s00_axi_bvalid,
      R => SM0_n_33
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(0),
      I1 => slv_reg2(0),
      I2 => sel0(1),
      I3 => slv_reg1(0),
      I4 => sel0(0),
      I5 => \slv_reg0__0\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(10),
      I1 => sel0(0),
      I2 => tf_out(10),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[10]_i_2_n_0\,
      O => reg_data_out(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(10),
      I1 => slv_reg2(10),
      I2 => sel0(1),
      I3 => slv_reg1(10),
      I4 => sel0(0),
      I5 => slv_reg0(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(11),
      I1 => sel0(0),
      I2 => tf_out(11),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[11]_i_3_n_0\,
      O => reg_data_out(11)
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(11),
      I1 => slv_reg2(11),
      I2 => sel0(1),
      I3 => slv_reg1(11),
      I4 => sel0(0),
      I5 => slv_reg0(11),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(12),
      I1 => sel0(0),
      I2 => tf_out(12),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[12]_i_2_n_0\,
      O => reg_data_out(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(12),
      I1 => slv_reg2(12),
      I2 => sel0(1),
      I3 => slv_reg1(12),
      I4 => sel0(0),
      I5 => slv_reg0(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(13),
      I1 => sel0(0),
      I2 => tf_out(13),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[13]_i_2_n_0\,
      O => reg_data_out(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(13),
      I1 => slv_reg2(13),
      I2 => sel0(1),
      I3 => slv_reg1(13),
      I4 => sel0(0),
      I5 => slv_reg0(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(14),
      I1 => sel0(0),
      I2 => tf_out(14),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[14]_i_2_n_0\,
      O => reg_data_out(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(14),
      I1 => slv_reg2(14),
      I2 => sel0(1),
      I3 => slv_reg1(14),
      I4 => sel0(0),
      I5 => slv_reg0(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(15),
      I1 => sel0(0),
      I2 => tf_out(15),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[15]_i_3_n_0\,
      O => reg_data_out(15)
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(15),
      I1 => slv_reg2(15),
      I2 => sel0(1),
      I3 => slv_reg1(15),
      I4 => sel0(0),
      I5 => slv_reg0(15),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(16),
      I1 => sel0(0),
      I2 => tf_out(16),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[16]_i_2_n_0\,
      O => reg_data_out(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(16),
      I1 => slv_reg2(16),
      I2 => sel0(1),
      I3 => slv_reg1(16),
      I4 => sel0(0),
      I5 => slv_reg0(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(17),
      I1 => sel0(0),
      I2 => tf_out(17),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[17]_i_2_n_0\,
      O => reg_data_out(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(17),
      I1 => slv_reg2(17),
      I2 => sel0(1),
      I3 => slv_reg1(17),
      I4 => sel0(0),
      I5 => slv_reg0(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(18),
      I1 => sel0(0),
      I2 => tf_out(18),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[18]_i_2_n_0\,
      O => reg_data_out(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(18),
      I1 => slv_reg2(18),
      I2 => sel0(1),
      I3 => slv_reg1(18),
      I4 => sel0(0),
      I5 => slv_reg0(18),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(19),
      I1 => sel0(0),
      I2 => tf_out(19),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[19]_i_3_n_0\,
      O => reg_data_out(19)
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(19),
      I1 => slv_reg2(19),
      I2 => sel0(1),
      I3 => slv_reg1(19),
      I4 => sel0(0),
      I5 => slv_reg0(19),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg4(1),
      I1 => sel0(0),
      I2 => tf_out(1),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[1]_i_2_n_0\,
      O => reg_data_out(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(1),
      I1 => slv_reg2(1),
      I2 => sel0(1),
      I3 => slv_reg1(1),
      I4 => sel0(0),
      I5 => slv_reg0(1),
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(20),
      I1 => sel0(0),
      I2 => tf_out(20),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[20]_i_2_n_0\,
      O => reg_data_out(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(20),
      I1 => slv_reg2(20),
      I2 => sel0(1),
      I3 => slv_reg1(20),
      I4 => sel0(0),
      I5 => slv_reg0(20),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(21),
      I1 => sel0(0),
      I2 => tf_out(21),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[21]_i_2_n_0\,
      O => reg_data_out(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(21),
      I1 => slv_reg2(21),
      I2 => sel0(1),
      I3 => slv_reg1(21),
      I4 => sel0(0),
      I5 => slv_reg0(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(22),
      I1 => sel0(0),
      I2 => tf_out(22),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[22]_i_2_n_0\,
      O => reg_data_out(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(22),
      I1 => slv_reg2(22),
      I2 => sel0(1),
      I3 => slv_reg1(22),
      I4 => sel0(0),
      I5 => slv_reg0(22),
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(23),
      I1 => sel0(0),
      I2 => tf_out(23),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[23]_i_3_n_0\,
      O => reg_data_out(23)
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(23),
      I1 => slv_reg2(23),
      I2 => sel0(1),
      I3 => slv_reg1(23),
      I4 => sel0(0),
      I5 => slv_reg0(23),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(24),
      I1 => sel0(0),
      I2 => tf_out(24),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[24]_i_2_n_0\,
      O => reg_data_out(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(24),
      I1 => slv_reg2(24),
      I2 => sel0(1),
      I3 => slv_reg1(24),
      I4 => sel0(0),
      I5 => slv_reg0(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(25),
      I1 => sel0(0),
      I2 => tf_out(25),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[25]_i_2_n_0\,
      O => reg_data_out(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(25),
      I1 => slv_reg2(25),
      I2 => sel0(1),
      I3 => slv_reg1(25),
      I4 => sel0(0),
      I5 => slv_reg0(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(26),
      I1 => sel0(0),
      I2 => tf_out(26),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[26]_i_2_n_0\,
      O => reg_data_out(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(26),
      I1 => slv_reg2(26),
      I2 => sel0(1),
      I3 => slv_reg1(26),
      I4 => sel0(0),
      I5 => slv_reg0(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(27),
      I1 => sel0(0),
      I2 => tf_out(27),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[27]_i_3_n_0\,
      O => reg_data_out(27)
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(27),
      I1 => slv_reg2(27),
      I2 => sel0(1),
      I3 => slv_reg1(27),
      I4 => sel0(0),
      I5 => slv_reg0(27),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(28),
      I1 => sel0(0),
      I2 => tf_out(28),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[28]_i_2_n_0\,
      O => reg_data_out(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(28),
      I1 => slv_reg2(28),
      I2 => sel0(1),
      I3 => slv_reg1(28),
      I4 => sel0(0),
      I5 => slv_reg0(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(29),
      I1 => sel0(0),
      I2 => tf_out(29),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[29]_i_2_n_0\,
      O => reg_data_out(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(29),
      I1 => slv_reg2(29),
      I2 => sel0(1),
      I3 => slv_reg1(29),
      I4 => sel0(0),
      I5 => slv_reg0(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg4(2),
      I1 => sel0(0),
      I2 => tf_out(2),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[2]_i_2_n_0\,
      O => reg_data_out(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(2),
      I1 => slv_reg2(2),
      I2 => sel0(1),
      I3 => slv_reg1(2),
      I4 => sel0(0),
      I5 => slv_reg0(2),
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(30),
      I1 => sel0(0),
      I2 => tf_out(30),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[30]_i_2_n_0\,
      O => reg_data_out(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(30),
      I1 => slv_reg2(30),
      I2 => sel0(1),
      I3 => slv_reg1(30),
      I4 => sel0(0),
      I5 => slv_reg0(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^axi_arready_reg_0\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata[31]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"33733133"
    )
        port map (
      I0 => slv_reg4(0),
      I1 => slv_reg4(7),
      I2 => slv_reg4(6),
      I3 => SM0_n_0,
      I4 => slv_reg4(5),
      O => \axi_rdata[31]_i_11_n_0\
    );
\axi_rdata[31]_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00800200"
    )
        port map (
      I0 => slv_reg4(0),
      I1 => slv_reg4(7),
      I2 => slv_reg4(6),
      I3 => SM0_n_0,
      I4 => slv_reg4(5),
      O => \axi_rdata[31]_i_15_n_0\
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(31),
      I1 => sel0(0),
      I2 => tf_out(31),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[31]_i_4_n_0\,
      O => reg_data_out(31)
    );
\axi_rdata[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(31),
      I1 => slv_reg2(31),
      I2 => sel0(1),
      I3 => slv_reg1(31),
      I4 => sel0(0),
      I5 => slv_reg0(31),
      O => \axi_rdata[31]_i_4_n_0\
    );
\axi_rdata[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => slv_reg4(7),
      I1 => slv_reg4(6),
      I2 => SM0_n_0,
      I3 => slv_reg4(5),
      O => \axi_rdata[31]_i_9_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg4(3),
      I1 => sel0(0),
      I2 => tf_out(3),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[3]_i_3_n_0\,
      O => reg_data_out(3)
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(3),
      I1 => slv_reg2(3),
      I2 => sel0(1),
      I3 => slv_reg1(3),
      I4 => sel0(0),
      I5 => slv_reg0(3),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg4(4),
      I1 => sel0(0),
      I2 => tf_out(4),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[4]_i_2_n_0\,
      O => reg_data_out(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(4),
      I1 => slv_reg2(4),
      I2 => sel0(1),
      I3 => slv_reg1(4),
      I4 => sel0(0),
      I5 => slv_reg0(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg4(5),
      I1 => sel0(0),
      I2 => tf_out(5),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[5]_i_2_n_0\,
      O => reg_data_out(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(5),
      I1 => slv_reg2(5),
      I2 => sel0(1),
      I3 => slv_reg1(5),
      I4 => sel0(0),
      I5 => slv_reg0(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg4(6),
      I1 => sel0(0),
      I2 => tf_out(6),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[6]_i_2_n_0\,
      O => reg_data_out(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(6),
      I1 => slv_reg2(6),
      I2 => sel0(1),
      I3 => slv_reg1(6),
      I4 => sel0(0),
      I5 => slv_reg0(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => slv_reg4(7),
      I1 => sel0(0),
      I2 => tf_out(7),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[7]_i_3_n_0\,
      O => reg_data_out(7)
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(7),
      I1 => slv_reg2(7),
      I2 => sel0(1),
      I3 => slv_reg1(7),
      I4 => sel0(0),
      I5 => slv_reg0(7),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(8),
      I1 => sel0(0),
      I2 => tf_out(8),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[8]_i_2_n_0\,
      O => reg_data_out(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(8),
      I1 => slv_reg2(8),
      I2 => sel0(1),
      I3 => slv_reg1(8),
      I4 => sel0(0),
      I5 => slv_reg0(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => \slv_reg4__0\(9),
      I1 => sel0(0),
      I2 => tf_out(9),
      I3 => sel0(1),
      I4 => sel0(2),
      I5 => \axi_rdata[9]_i_2_n_0\,
      O => reg_data_out(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(9),
      I1 => slv_reg2(9),
      I2 => sel0(1),
      I3 => slv_reg1(9),
      I4 => sel0(0),
      I5 => slv_reg0(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => SM0_n_33
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => SM0_n_33
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => SM0_n_33
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => SM0_n_33
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => SM0_n_33
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => SM0_n_33
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => SM0_n_33
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => SM0_n_33
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => SM0_n_33
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => SM0_n_33
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => SM0_n_33
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => SM0_n_33
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => SM0_n_33
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => SM0_n_33
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => SM0_n_33
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => SM0_n_33
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => SM0_n_33
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => SM0_n_33
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => SM0_n_33
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => SM0_n_33
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => SM0_n_33
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => SM0_n_33
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => SM0_n_33
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => SM0_n_33
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => SM0_n_33
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => SM0_n_33
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => SM0_n_33
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => SM0_n_33
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => SM0_n_33
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => SM0_n_33
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => SM0_n_33
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => SM0_n_33
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_reg_0,
      Q => \^s00_axi_rvalid\,
      R => SM0_n_33
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \^aw_en_reg_0\,
      I1 => s00_axi_wvalid,
      I2 => s00_axi_awvalid,
      I3 => \^axi_wready_reg_0\,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^axi_wready_reg_0\,
      R => SM0_n_33
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(1),
      O => p_1_in(15)
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(2),
      O => p_1_in(23)
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(3),
      O => p_1_in(31)
    );
\slv_reg0[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^axi_awready_reg_0\,
      I2 => \^axi_wready_reg_0\,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__2\
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(0),
      O => p_1_in(7)
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => \slv_reg0__0\(0),
      R => SM0_n_33
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => slv_reg0(10),
      R => SM0_n_33
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => slv_reg0(11),
      R => SM0_n_33
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => slv_reg0(12),
      R => SM0_n_33
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => slv_reg0(13),
      R => SM0_n_33
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => slv_reg0(14),
      R => SM0_n_33
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => slv_reg0(15),
      R => SM0_n_33
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => slv_reg0(16),
      R => SM0_n_33
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => slv_reg0(17),
      R => SM0_n_33
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => slv_reg0(18),
      R => SM0_n_33
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => slv_reg0(19),
      R => SM0_n_33
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => slv_reg0(1),
      R => SM0_n_33
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => slv_reg0(20),
      R => SM0_n_33
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => slv_reg0(21),
      R => SM0_n_33
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => slv_reg0(22),
      R => SM0_n_33
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => slv_reg0(23),
      R => SM0_n_33
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => slv_reg0(24),
      R => SM0_n_33
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => slv_reg0(25),
      R => SM0_n_33
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => slv_reg0(26),
      R => SM0_n_33
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => slv_reg0(27),
      R => SM0_n_33
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => slv_reg0(28),
      R => SM0_n_33
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => slv_reg0(29),
      R => SM0_n_33
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => slv_reg0(2),
      R => SM0_n_33
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => slv_reg0(30),
      R => SM0_n_33
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => slv_reg0(31),
      R => SM0_n_33
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => slv_reg0(3),
      R => SM0_n_33
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => slv_reg0(4),
      R => SM0_n_33
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => slv_reg0(5),
      R => SM0_n_33
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => slv_reg0(6),
      R => SM0_n_33
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => slv_reg0(7),
      R => SM0_n_33
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => slv_reg0(8),
      R => SM0_n_33
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => slv_reg0(9),
      R => SM0_n_33
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(0),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(0),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(0),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(0),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg1(0),
      R => SM0_n_33
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg1(10),
      R => SM0_n_33
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg1(11),
      R => SM0_n_33
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg1(12),
      R => SM0_n_33
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg1(13),
      R => SM0_n_33
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg1(14),
      R => SM0_n_33
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg1(15),
      R => SM0_n_33
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg1(16),
      R => SM0_n_33
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg1(17),
      R => SM0_n_33
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg1(18),
      R => SM0_n_33
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg1(19),
      R => SM0_n_33
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg1(1),
      R => SM0_n_33
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg1(20),
      R => SM0_n_33
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg1(21),
      R => SM0_n_33
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg1(22),
      R => SM0_n_33
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg1(23),
      R => SM0_n_33
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg1(24),
      R => SM0_n_33
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg1(25),
      R => SM0_n_33
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg1(26),
      R => SM0_n_33
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg1(27),
      R => SM0_n_33
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg1(28),
      R => SM0_n_33
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg1(29),
      R => SM0_n_33
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg1(2),
      R => SM0_n_33
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg1(30),
      R => SM0_n_33
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg1(31),
      R => SM0_n_33
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg1(3),
      R => SM0_n_33
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg1(4),
      R => SM0_n_33
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg1(5),
      R => SM0_n_33
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg1(6),
      R => SM0_n_33
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg1(7),
      R => SM0_n_33
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg1(8),
      R => SM0_n_33
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg1(9),
      R => SM0_n_33
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(1),
      I4 => p_0_in(1),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(2),
      I4 => p_0_in(1),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(3),
      I4 => p_0_in(1),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(0),
      I3 => s00_axi_wstrb(0),
      I4 => p_0_in(1),
      O => \slv_reg2[7]_i_1_n_0\
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg2(0),
      R => SM0_n_33
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg2(10),
      R => SM0_n_33
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg2(11),
      R => SM0_n_33
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg2(12),
      R => SM0_n_33
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg2(13),
      R => SM0_n_33
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg2(14),
      R => SM0_n_33
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg2(15),
      R => SM0_n_33
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg2(16),
      R => SM0_n_33
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg2(17),
      R => SM0_n_33
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg2(18),
      R => SM0_n_33
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg2(19),
      R => SM0_n_33
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg2(1),
      R => SM0_n_33
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg2(20),
      R => SM0_n_33
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg2(21),
      R => SM0_n_33
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg2(22),
      R => SM0_n_33
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg2(23),
      R => SM0_n_33
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg2(24),
      R => SM0_n_33
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg2(25),
      R => SM0_n_33
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg2(26),
      R => SM0_n_33
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg2(27),
      R => SM0_n_33
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg2(28),
      R => SM0_n_33
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg2(29),
      R => SM0_n_33
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg2(2),
      R => SM0_n_33
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg2(30),
      R => SM0_n_33
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg2(31),
      R => SM0_n_33
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg2(3),
      R => SM0_n_33
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg2(4),
      R => SM0_n_33
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg2(5),
      R => SM0_n_33
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg2(6),
      R => SM0_n_33
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg2(7),
      R => SM0_n_33
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg2(8),
      R => SM0_n_33
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg2(9),
      R => SM0_n_33
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => p_0_in(0),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg3[15]_i_1_n_0\
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => p_0_in(0),
      I4 => s00_axi_wstrb(2),
      O => \slv_reg3[23]_i_1_n_0\
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => p_0_in(0),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg3[31]_i_1_n_0\
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(2),
      I2 => p_0_in(1),
      I3 => p_0_in(0),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg3[7]_i_1_n_0\
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg3(0),
      R => SM0_n_33
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg3(10),
      R => SM0_n_33
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg3(11),
      R => SM0_n_33
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg3(12),
      R => SM0_n_33
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg3(13),
      R => SM0_n_33
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg3(14),
      R => SM0_n_33
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg3(15),
      R => SM0_n_33
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg3(16),
      R => SM0_n_33
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg3(17),
      R => SM0_n_33
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg3(18),
      R => SM0_n_33
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg3(19),
      R => SM0_n_33
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg3(1),
      R => SM0_n_33
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg3(20),
      R => SM0_n_33
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg3(21),
      R => SM0_n_33
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg3(22),
      R => SM0_n_33
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg3(23),
      R => SM0_n_33
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg3(24),
      R => SM0_n_33
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg3(25),
      R => SM0_n_33
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg3(26),
      R => SM0_n_33
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg3(27),
      R => SM0_n_33
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg3(28),
      R => SM0_n_33
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg3(29),
      R => SM0_n_33
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg3(2),
      R => SM0_n_33
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg3(30),
      R => SM0_n_33
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg3(31),
      R => SM0_n_33
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg3(3),
      R => SM0_n_33
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg3(4),
      R => SM0_n_33
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg3(5),
      R => SM0_n_33
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg3(6),
      R => SM0_n_33
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg3(7),
      R => SM0_n_33
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg3(8),
      R => SM0_n_33
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg3(9),
      R => SM0_n_33
    );
\slv_reg4[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg4[15]_i_1_n_0\
    );
\slv_reg4[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(2),
      O => \slv_reg4[23]_i_1_n_0\
    );
\slv_reg4[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg4[31]_i_1_n_0\
    );
\slv_reg4[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__2\,
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg4[7]_i_1_n_0\
    );
\slv_reg4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg4(0),
      R => SM0_n_33
    );
\slv_reg4_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg4__0\(10),
      R => SM0_n_33
    );
\slv_reg4_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg4__0\(11),
      R => SM0_n_33
    );
\slv_reg4_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg4__0\(12),
      R => SM0_n_33
    );
\slv_reg4_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg4__0\(13),
      R => SM0_n_33
    );
\slv_reg4_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg4__0\(14),
      R => SM0_n_33
    );
\slv_reg4_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg4__0\(15),
      R => SM0_n_33
    );
\slv_reg4_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg4__0\(16),
      R => SM0_n_33
    );
\slv_reg4_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg4__0\(17),
      R => SM0_n_33
    );
\slv_reg4_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg4__0\(18),
      R => SM0_n_33
    );
\slv_reg4_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg4__0\(19),
      R => SM0_n_33
    );
\slv_reg4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg4(1),
      R => SM0_n_33
    );
\slv_reg4_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg4__0\(20),
      R => SM0_n_33
    );
\slv_reg4_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg4__0\(21),
      R => SM0_n_33
    );
\slv_reg4_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg4__0\(22),
      R => SM0_n_33
    );
\slv_reg4_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg4__0\(23),
      R => SM0_n_33
    );
\slv_reg4_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg4__0\(24),
      R => SM0_n_33
    );
\slv_reg4_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg4__0\(25),
      R => SM0_n_33
    );
\slv_reg4_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg4__0\(26),
      R => SM0_n_33
    );
\slv_reg4_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg4__0\(27),
      R => SM0_n_33
    );
\slv_reg4_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg4__0\(28),
      R => SM0_n_33
    );
\slv_reg4_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg4__0\(29),
      R => SM0_n_33
    );
\slv_reg4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg4(2),
      R => SM0_n_33
    );
\slv_reg4_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg4__0\(30),
      R => SM0_n_33
    );
\slv_reg4_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg4__0\(31),
      R => SM0_n_33
    );
\slv_reg4_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg4(3),
      R => SM0_n_33
    );
\slv_reg4_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg4(4),
      R => SM0_n_33
    );
\slv_reg4_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg4(5),
      R => SM0_n_33
    );
\slv_reg4_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg4(6),
      R => SM0_n_33
    );
\slv_reg4_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg4(7),
      R => SM0_n_33
    );
\slv_reg4_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \slv_reg4__0\(8),
      R => SM0_n_33
    );
\slv_reg4_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \slv_reg4__0\(9),
      R => SM0_n_33
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0 is
  port (
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0;

architecture STRUCTURE of fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0 is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal tflite_mbqm_v1_0_S00_AXI_inst_n_4 : STD_LOGIC;
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFBF00BF00BF00"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => s00_axi_awvalid,
      I2 => s00_axi_wvalid,
      I3 => tflite_mbqm_v1_0_S00_AXI_inst_n_4,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \^s_axi_wready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
tflite_mbqm_v1_0_S00_AXI_inst: entity work.fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0_S00_AXI
     port map (
      aw_en_reg_0 => tflite_mbqm_v1_0_S00_AXI_inst_n_4,
      aw_en_reg_1 => aw_en_i_1_n_0,
      axi_arready_reg_0 => \^s_axi_arready\,
      axi_awready_reg_0 => \^s_axi_awready\,
      axi_bvalid_reg_0 => axi_bvalid_i_1_n_0,
      axi_rvalid_reg_0 => axi_rvalid_i_1_n_0,
      axi_wready_reg_0 => \^s_axi_wready\,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(2 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(2 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bvalid => \^s00_axi_bvalid\,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rvalid => \^s00_axi_rvalid\,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fer_soc_bd_tflite_mbqm_0_0 is
  port (
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of fer_soc_bd_tflite_mbqm_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of fer_soc_bd_tflite_mbqm_0_0 : entity is "fer_soc_bd_tflite_mbqm_0_0,tflite_mbqm_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of fer_soc_bd_tflite_mbqm_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of fer_soc_bd_tflite_mbqm_0_0 : entity is "tflite_mbqm_v1_0,Vivado 2021.1";
end fer_soc_bd_tflite_mbqm_0_0;

architecture STRUCTURE of fer_soc_bd_tflite_mbqm_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 50000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN fer_soc_bd_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 7, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN fer_soc_bd_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0
     port map (
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(4 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(4 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
