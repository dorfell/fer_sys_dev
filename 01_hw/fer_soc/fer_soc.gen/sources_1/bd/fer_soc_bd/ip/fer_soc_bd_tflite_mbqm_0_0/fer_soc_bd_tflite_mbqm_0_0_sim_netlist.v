// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.1 (lin64) Build 3247384 Thu Jun 10 19:36:07 MDT 2021
// Date        : Thu Nov 11 19:27:38 2021
// Host        : dplegion running 64-bit Ubuntu 20.04.2 LTS
// Command     : write_verilog -force -mode funcsim -rename_top fer_soc_bd_tflite_mbqm_0_0 -prefix
//               fer_soc_bd_tflite_mbqm_0_0_ fer_soc_bd_tflite_mbqm_0_0_sim_netlist.v
// Design      : fer_soc_bd_tflite_mbqm_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "fer_soc_bd_tflite_mbqm_0_0,tflite_mbqm_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "tflite_mbqm_v1_0,Vivado 2021.1" *) 
(* NotValidForBitStream *)
module fer_soc_bd_tflite_mbqm_0_0
   (s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 7, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN fer_soc_bd_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [4:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [4:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 50000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN fer_soc_bd_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module fer_soc_bd_tflite_mbqm_0_0_tflite_core0
   (xls,
    \slv_reg4_reg[5] ,
    \slv_reg1_reg[11] ,
    D,
    \slv_reg4_reg[5]_0 ,
    \slv_reg4_reg[7] ,
    \slv_reg4_reg[7]_0 ,
    \slv_reg4_reg[7]_1 ,
    \slv_reg4_reg[7]_2 ,
    \slv_reg4_reg[0] ,
    \slv_reg4_reg[0]_0 ,
    \slv_reg4_reg[3] ,
    \slv_reg4_reg[3]_0 ,
    \slv_reg1_reg[23] ,
    \axi_rdata[31]_i_8_0 ,
    \axi_rdata[11]_i_7_0 ,
    sg_ab_64_p3_reg,
    \sg_a_p0_reg[31] ,
    S,
    \axi_rdata_reg[3]_i_2_0 ,
    \axi_rdata_reg[3]_i_2_1 ,
    CO,
    \axi_rdata[19]_i_5 ,
    \axi_rdata[19]_i_5_0 ,
    \axi_rdata_reg[31]_i_3_0 ,
    sel0,
    \axi_rdata_reg[0] ,
    \axi_rdata_reg[0]_0 ,
    Q,
    \axi_rdata_reg[11] ,
    \axi_rdata_reg[3]_i_2_2 ,
    \axi_rdata[31]_i_17_0 ,
    \axi_rdata[31]_i_7_0 ,
    \axi_rdata_reg[3]_i_2_3 ,
    \axi_rdata_reg[3]_i_2_4 ,
    \axi_rdata[27]_i_15_0 ,
    \axi_rdata_reg[3]_i_2_5 ,
    \axi_rdata_reg[7]_i_2_0 ,
    \axi_rdata_reg[7]_i_2_1 ,
    \axi_rdata_reg[7]_i_2_2 ,
    \axi_rdata_reg[7]_i_2_3 ,
    \axi_rdata_reg[11]_i_2_0 ,
    \axi_rdata_reg[11]_i_2_1 ,
    \axi_rdata[11]_i_7_1 ,
    \axi_rdata[11]_i_5 ,
    \axi_rdata_reg[19]_i_2 ,
    \axi_rdata[23]_i_7_0 ,
    \axi_rdata[19]_i_5_1 ,
    \axi_rdata_reg[23]_i_2_0 ,
    \axi_rdata[23]_i_6_0 ,
    \axi_rdata[19]_i_4_0 ,
    \axi_rdata_reg[23]_i_2_1 ,
    \axi_rdata_reg[23]_i_2_2 ,
    \axi_rdata_reg[23]_i_2_3 ,
    \axi_rdata_reg[27]_i_2_0 ,
    \axi_rdata_reg[27]_i_2_1 ,
    \axi_rdata_reg[27]_i_2_2 ,
    \axi_rdata_reg[27]_i_2_3 ,
    \axi_rdata[3]_i_22_0 ,
    ab_nudge,
    \axi_rdata_reg[31]_i_3_1 ,
    \axi_rdata_reg[31]_i_3_2 ,
    O,
    \axi_rdata_reg[31]_i_3_3 ,
    \axi_rdata_reg[23] ,
    \sg_a_p0_reg[27] ,
    i__carry_i_1__1);
  output [31:0]xls;
  output \slv_reg4_reg[5] ;
  output \slv_reg1_reg[11] ;
  output [0:0]D;
  output [0:0]\slv_reg4_reg[5]_0 ;
  output \slv_reg4_reg[7] ;
  output \slv_reg4_reg[7]_0 ;
  output \slv_reg4_reg[7]_1 ;
  output \slv_reg4_reg[7]_2 ;
  output \slv_reg4_reg[0] ;
  output \slv_reg4_reg[0]_0 ;
  output \slv_reg4_reg[3] ;
  output \slv_reg4_reg[3]_0 ;
  output \slv_reg1_reg[23] ;
  output [22:0]\axi_rdata[31]_i_8_0 ;
  output [0:0]\axi_rdata[11]_i_7_0 ;
  output [0:0]sg_ab_64_p3_reg;
  input [30:0]\sg_a_p0_reg[31] ;
  input [3:0]S;
  input \axi_rdata_reg[3]_i_2_0 ;
  input \axi_rdata_reg[3]_i_2_1 ;
  input [0:0]CO;
  input [3:0]\axi_rdata[19]_i_5 ;
  input \axi_rdata[19]_i_5_0 ;
  input \axi_rdata_reg[31]_i_3_0 ;
  input [2:0]sel0;
  input \axi_rdata_reg[0] ;
  input \axi_rdata_reg[0]_0 ;
  input [31:0]Q;
  input [1:0]\axi_rdata_reg[11] ;
  input \axi_rdata_reg[3]_i_2_2 ;
  input \axi_rdata[31]_i_17_0 ;
  input \axi_rdata[31]_i_7_0 ;
  input \axi_rdata_reg[3]_i_2_3 ;
  input \axi_rdata_reg[3]_i_2_4 ;
  input \axi_rdata[27]_i_15_0 ;
  input \axi_rdata_reg[3]_i_2_5 ;
  input \axi_rdata_reg[7]_i_2_0 ;
  input \axi_rdata_reg[7]_i_2_1 ;
  input \axi_rdata_reg[7]_i_2_2 ;
  input \axi_rdata_reg[7]_i_2_3 ;
  input \axi_rdata_reg[11]_i_2_0 ;
  input \axi_rdata_reg[11]_i_2_1 ;
  input \axi_rdata[11]_i_7_1 ;
  input \axi_rdata[11]_i_5 ;
  input \axi_rdata_reg[19]_i_2 ;
  input \axi_rdata[23]_i_7_0 ;
  input \axi_rdata[19]_i_5_1 ;
  input \axi_rdata_reg[23]_i_2_0 ;
  input \axi_rdata[23]_i_6_0 ;
  input \axi_rdata[19]_i_4_0 ;
  input \axi_rdata_reg[23]_i_2_1 ;
  input \axi_rdata_reg[23]_i_2_2 ;
  input \axi_rdata_reg[23]_i_2_3 ;
  input \axi_rdata_reg[27]_i_2_0 ;
  input \axi_rdata_reg[27]_i_2_1 ;
  input \axi_rdata_reg[27]_i_2_2 ;
  input \axi_rdata_reg[27]_i_2_3 ;
  input \axi_rdata[3]_i_22_0 ;
  input [31:0]ab_nudge;
  input \axi_rdata_reg[31]_i_3_1 ;
  input \axi_rdata_reg[31]_i_3_2 ;
  input [1:0]O;
  input \axi_rdata_reg[31]_i_3_3 ;
  input [0:0]\axi_rdata_reg[23] ;
  input [27:0]\sg_a_p0_reg[27] ;
  input [0:0]i__carry_i_1__1;

  wire [0:0]CO;
  wire [0:0]D;
  wire [1:0]O;
  wire [31:0]Q;
  wire [3:0]S;
  wire [31:0]SHIFT_RIGHT;
  wire \SRDHM_overflow0_inferred__0/i__carry__0_n_0 ;
  wire \SRDHM_overflow0_inferred__0/i__carry__0_n_1 ;
  wire \SRDHM_overflow0_inferred__0/i__carry__0_n_2 ;
  wire \SRDHM_overflow0_inferred__0/i__carry__0_n_3 ;
  wire \SRDHM_overflow0_inferred__0/i__carry__1_n_1 ;
  wire \SRDHM_overflow0_inferred__0/i__carry__1_n_2 ;
  wire \SRDHM_overflow0_inferred__0/i__carry__1_n_3 ;
  wire \SRDHM_overflow0_inferred__0/i__carry_n_0 ;
  wire \SRDHM_overflow0_inferred__0/i__carry_n_1 ;
  wire \SRDHM_overflow0_inferred__0/i__carry_n_2 ;
  wire \SRDHM_overflow0_inferred__0/i__carry_n_3 ;
  wire [31:0]ab_nudge;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[11]_i_13_n_0 ;
  wire \axi_rdata[11]_i_15_n_0 ;
  wire \axi_rdata[11]_i_17_n_0 ;
  wire \axi_rdata[11]_i_18_n_0 ;
  wire \axi_rdata[11]_i_19_n_0 ;
  wire \axi_rdata[11]_i_20_n_0 ;
  wire \axi_rdata[11]_i_21_n_0 ;
  wire \axi_rdata[11]_i_22_n_0 ;
  wire \axi_rdata[11]_i_23_n_0 ;
  wire \axi_rdata[11]_i_5 ;
  wire [0:0]\axi_rdata[11]_i_7_0 ;
  wire \axi_rdata[11]_i_7_1 ;
  wire \axi_rdata[15]_i_20_n_0 ;
  wire \axi_rdata[15]_i_21_n_0 ;
  wire \axi_rdata[15]_i_22_n_0 ;
  wire \axi_rdata[15]_i_23_n_0 ;
  wire \axi_rdata[15]_i_24_n_0 ;
  wire \axi_rdata[19]_i_4_0 ;
  wire [3:0]\axi_rdata[19]_i_5 ;
  wire \axi_rdata[19]_i_5_0 ;
  wire \axi_rdata[19]_i_5_1 ;
  wire \axi_rdata[23]_i_11_n_0 ;
  wire \axi_rdata[23]_i_13_n_0 ;
  wire \axi_rdata[23]_i_15_n_0 ;
  wire \axi_rdata[23]_i_16_n_0 ;
  wire \axi_rdata[23]_i_6_0 ;
  wire \axi_rdata[23]_i_7_0 ;
  wire \axi_rdata[23]_i_9_n_0 ;
  wire \axi_rdata[27]_i_11_n_0 ;
  wire \axi_rdata[27]_i_13_n_0 ;
  wire \axi_rdata[27]_i_15_0 ;
  wire \axi_rdata[27]_i_15_n_0 ;
  wire \axi_rdata[27]_i_16_n_0 ;
  wire \axi_rdata[27]_i_17_n_0 ;
  wire \axi_rdata[27]_i_18_n_0 ;
  wire \axi_rdata[27]_i_19_n_0 ;
  wire \axi_rdata[27]_i_20_n_0 ;
  wire \axi_rdata[27]_i_21_n_0 ;
  wire \axi_rdata[27]_i_9_n_0 ;
  wire \axi_rdata[31]_i_10_n_0 ;
  wire \axi_rdata[31]_i_13_n_0 ;
  wire \axi_rdata[31]_i_17_0 ;
  wire \axi_rdata[31]_i_17_n_0 ;
  wire \axi_rdata[31]_i_18_n_0 ;
  wire \axi_rdata[31]_i_19_n_0 ;
  wire \axi_rdata[31]_i_20_n_0 ;
  wire \axi_rdata[31]_i_7_0 ;
  wire [22:0]\axi_rdata[31]_i_8_0 ;
  wire \axi_rdata[3]_i_10_n_0 ;
  wire \axi_rdata[3]_i_12_n_0 ;
  wire \axi_rdata[3]_i_14_n_0 ;
  wire \axi_rdata[3]_i_17_n_0 ;
  wire \axi_rdata[3]_i_18_n_0 ;
  wire \axi_rdata[3]_i_19_n_0 ;
  wire \axi_rdata[3]_i_20_n_0 ;
  wire \axi_rdata[3]_i_21_n_0 ;
  wire \axi_rdata[3]_i_22_0 ;
  wire \axi_rdata[3]_i_22_n_0 ;
  wire \axi_rdata[3]_i_23_n_0 ;
  wire \axi_rdata[3]_i_24_n_0 ;
  wire \axi_rdata[3]_i_8_n_0 ;
  wire \axi_rdata[3]_i_9_n_0 ;
  wire \axi_rdata[7]_i_10_n_0 ;
  wire \axi_rdata[7]_i_12_n_0 ;
  wire \axi_rdata[7]_i_15_n_0 ;
  wire \axi_rdata[7]_i_16_n_0 ;
  wire \axi_rdata[7]_i_17_n_0 ;
  wire \axi_rdata[7]_i_18_n_0 ;
  wire \axi_rdata[7]_i_19_n_0 ;
  wire \axi_rdata[7]_i_20_n_0 ;
  wire \axi_rdata[7]_i_8_n_0 ;
  wire \axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire [1:0]\axi_rdata_reg[11] ;
  wire \axi_rdata_reg[11]_i_2_0 ;
  wire \axi_rdata_reg[11]_i_2_1 ;
  wire \axi_rdata_reg[11]_i_2_n_1 ;
  wire \axi_rdata_reg[11]_i_2_n_2 ;
  wire \axi_rdata_reg[11]_i_2_n_3 ;
  wire \axi_rdata_reg[19]_i_2 ;
  wire [0:0]\axi_rdata_reg[23] ;
  wire \axi_rdata_reg[23]_i_2_0 ;
  wire \axi_rdata_reg[23]_i_2_1 ;
  wire \axi_rdata_reg[23]_i_2_2 ;
  wire \axi_rdata_reg[23]_i_2_3 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_1 ;
  wire \axi_rdata_reg[23]_i_2_n_2 ;
  wire \axi_rdata_reg[23]_i_2_n_3 ;
  wire \axi_rdata_reg[27]_i_2_0 ;
  wire \axi_rdata_reg[27]_i_2_1 ;
  wire \axi_rdata_reg[27]_i_2_2 ;
  wire \axi_rdata_reg[27]_i_2_3 ;
  wire \axi_rdata_reg[27]_i_2_n_0 ;
  wire \axi_rdata_reg[27]_i_2_n_1 ;
  wire \axi_rdata_reg[27]_i_2_n_2 ;
  wire \axi_rdata_reg[27]_i_2_n_3 ;
  wire \axi_rdata_reg[31]_i_3_0 ;
  wire \axi_rdata_reg[31]_i_3_1 ;
  wire \axi_rdata_reg[31]_i_3_2 ;
  wire \axi_rdata_reg[31]_i_3_3 ;
  wire \axi_rdata_reg[31]_i_3_n_1 ;
  wire \axi_rdata_reg[31]_i_3_n_2 ;
  wire \axi_rdata_reg[31]_i_3_n_3 ;
  wire \axi_rdata_reg[3]_i_2_0 ;
  wire \axi_rdata_reg[3]_i_2_1 ;
  wire \axi_rdata_reg[3]_i_2_2 ;
  wire \axi_rdata_reg[3]_i_2_3 ;
  wire \axi_rdata_reg[3]_i_2_4 ;
  wire \axi_rdata_reg[3]_i_2_5 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_1 ;
  wire \axi_rdata_reg[3]_i_2_n_2 ;
  wire \axi_rdata_reg[3]_i_2_n_3 ;
  wire \axi_rdata_reg[7]_i_2_0 ;
  wire \axi_rdata_reg[7]_i_2_1 ;
  wire \axi_rdata_reg[7]_i_2_2 ;
  wire \axi_rdata_reg[7]_i_2_3 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_1 ;
  wire \axi_rdata_reg[7]_i_2_n_2 ;
  wire \axi_rdata_reg[7]_i_2_n_3 ;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__1_i_1_n_0;
  wire i__carry__1_i_2_n_0;
  wire i__carry__1_i_3_n_0;
  wire i__carry__6_i_10_n_0;
  wire i__carry__6_i_11_n_0;
  wire i__carry__6_i_12_n_0;
  wire i__carry__6_i_7_n_0;
  wire i__carry__6_i_8_n_0;
  wire i__carry__6_i_9_n_0;
  wire i__carry_i_10_n_0;
  wire i__carry_i_11_n_0;
  wire i__carry_i_12_n_0;
  wire i__carry_i_13_n_0;
  wire i__carry_i_14_n_0;
  wire i__carry_i_15_n_0;
  wire i__carry_i_16_n_0;
  wire [0:0]i__carry_i_1__1;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_9_n_0;
  wire [2:0]sel0;
  wire [27:0]\sg_a_p0_reg[27] ;
  wire [30:0]\sg_a_p0_reg[31] ;
  wire [0:0]sg_ab_64_p3_reg;
  wire sg_xowb_carry__0_i_1_n_0;
  wire sg_xowb_carry__0_i_2_n_0;
  wire sg_xowb_carry__0_i_3_n_0;
  wire sg_xowb_carry__0_i_4_n_0;
  wire sg_xowb_carry__0_n_0;
  wire sg_xowb_carry__0_n_1;
  wire sg_xowb_carry__0_n_2;
  wire sg_xowb_carry__0_n_3;
  wire sg_xowb_carry__1_i_1_n_0;
  wire sg_xowb_carry__1_i_2_n_0;
  wire sg_xowb_carry__1_i_3_n_0;
  wire sg_xowb_carry__1_i_4_n_0;
  wire sg_xowb_carry__1_n_0;
  wire sg_xowb_carry__1_n_1;
  wire sg_xowb_carry__1_n_2;
  wire sg_xowb_carry__1_n_3;
  wire sg_xowb_carry__2_i_1_n_0;
  wire sg_xowb_carry__2_i_2_n_0;
  wire sg_xowb_carry__2_i_3_n_0;
  wire sg_xowb_carry__2_i_4_n_0;
  wire sg_xowb_carry__2_n_0;
  wire sg_xowb_carry__2_n_1;
  wire sg_xowb_carry__2_n_2;
  wire sg_xowb_carry__2_n_3;
  wire sg_xowb_carry__3_i_1_n_0;
  wire sg_xowb_carry__3_i_2_n_0;
  wire sg_xowb_carry__3_i_3_n_0;
  wire sg_xowb_carry__3_i_4_n_0;
  wire sg_xowb_carry__3_n_0;
  wire sg_xowb_carry__3_n_1;
  wire sg_xowb_carry__3_n_2;
  wire sg_xowb_carry__3_n_3;
  wire sg_xowb_carry__4_i_1_n_0;
  wire sg_xowb_carry__4_i_2_n_0;
  wire sg_xowb_carry__4_i_3_n_0;
  wire sg_xowb_carry__4_i_4_n_0;
  wire sg_xowb_carry__4_n_0;
  wire sg_xowb_carry__4_n_1;
  wire sg_xowb_carry__4_n_2;
  wire sg_xowb_carry__4_n_3;
  wire sg_xowb_carry__5_i_1_n_0;
  wire sg_xowb_carry__5_i_2_n_0;
  wire sg_xowb_carry__5_i_3_n_0;
  wire sg_xowb_carry__5_i_4_n_0;
  wire sg_xowb_carry__5_n_0;
  wire sg_xowb_carry__5_n_1;
  wire sg_xowb_carry__5_n_2;
  wire sg_xowb_carry__5_n_3;
  wire sg_xowb_carry__6_n_1;
  wire sg_xowb_carry__6_n_2;
  wire sg_xowb_carry__6_n_3;
  wire sg_xowb_carry_i_1_n_0;
  wire sg_xowb_carry_i_2_n_0;
  wire sg_xowb_carry_i_3_n_0;
  wire sg_xowb_carry_i_4_n_0;
  wire sg_xowb_carry_n_0;
  wire sg_xowb_carry_n_1;
  wire sg_xowb_carry_n_2;
  wire sg_xowb_carry_n_3;
  wire \slv_reg1_reg[11] ;
  wire \slv_reg1_reg[23] ;
  wire \slv_reg4_reg[0] ;
  wire \slv_reg4_reg[0]_0 ;
  wire \slv_reg4_reg[3] ;
  wire \slv_reg4_reg[3]_0 ;
  wire \slv_reg4_reg[5] ;
  wire [0:0]\slv_reg4_reg[5]_0 ;
  wire \slv_reg4_reg[7] ;
  wire \slv_reg4_reg[7]_0 ;
  wire \slv_reg4_reg[7]_1 ;
  wire \slv_reg4_reg[7]_2 ;
  wire [0:0]tf_out;
  wire [31:0]xls;
  wire [3:0]\NLW_SRDHM_overflow0_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_SRDHM_overflow0_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:3]\NLW_SRDHM_overflow0_inferred__0/i__carry__1_CO_UNCONNECTED ;
  wire [3:0]\NLW_SRDHM_overflow0_inferred__0/i__carry__1_O_UNCONNECTED ;
  wire [3:3]\NLW_axi_rdata_reg[31]_i_3_CO_UNCONNECTED ;
  wire [3:3]NLW_sg_xowb_carry__6_CO_UNCONNECTED;

  CARRY4 \SRDHM_overflow0_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\SRDHM_overflow0_inferred__0/i__carry_n_0 ,\SRDHM_overflow0_inferred__0/i__carry_n_1 ,\SRDHM_overflow0_inferred__0/i__carry_n_2 ,\SRDHM_overflow0_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_SRDHM_overflow0_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}));
  CARRY4 \SRDHM_overflow0_inferred__0/i__carry__0 
       (.CI(\SRDHM_overflow0_inferred__0/i__carry_n_0 ),
        .CO({\SRDHM_overflow0_inferred__0/i__carry__0_n_0 ,\SRDHM_overflow0_inferred__0/i__carry__0_n_1 ,\SRDHM_overflow0_inferred__0/i__carry__0_n_2 ,\SRDHM_overflow0_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_SRDHM_overflow0_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_1_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4_n_0}));
  CARRY4 \SRDHM_overflow0_inferred__0/i__carry__1 
       (.CI(\SRDHM_overflow0_inferred__0/i__carry__0_n_0 ),
        .CO({\NLW_SRDHM_overflow0_inferred__0/i__carry__1_CO_UNCONNECTED [3],\SRDHM_overflow0_inferred__0/i__carry__1_n_1 ,\SRDHM_overflow0_inferred__0/i__carry__1_n_2 ,\SRDHM_overflow0_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_SRDHM_overflow0_inferred__0/i__carry__1_O_UNCONNECTED [3:0]),
        .S({1'b0,i__carry__1_i_1_n_0,i__carry__1_i_2_n_0,i__carry__1_i_3_n_0}));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[0]_i_3 
       (.I0(\axi_rdata_reg[0]_0 ),
        .I1(sel0[1]),
        .I2(tf_out),
        .I3(sel0[0]),
        .I4(\axi_rdata[19]_i_5 [0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_11 
       (.I0(\axi_rdata[11]_i_5 ),
        .I1(\slv_reg4_reg[7]_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\slv_reg4_reg[7]_2 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[11]_i_17_n_0 ),
        .O(\slv_reg4_reg[0] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_13 
       (.I0(\axi_rdata[11]_i_7_1 ),
        .I1(\slv_reg4_reg[7] ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\slv_reg4_reg[7]_1 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[11]_i_18_n_0 ),
        .O(\axi_rdata[11]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_15 
       (.I0(\slv_reg4_reg[7]_2 ),
        .I1(\axi_rdata[11]_i_17_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\slv_reg4_reg[7]_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[11]_i_19_n_0 ),
        .O(\axi_rdata[11]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h888BFFFF888B0000)) 
    \axi_rdata[11]_i_16 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[19]),
        .I3(\slv_reg1_reg[23] ),
        .I4(\axi_rdata[27]_i_15_0 ),
        .I5(\axi_rdata[11]_i_20_n_0 ),
        .O(\slv_reg4_reg[7] ));
  LUT6 #(
    .INIT(64'h888BFFFF888B0000)) 
    \axi_rdata[11]_i_17 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[18]),
        .I3(\slv_reg1_reg[23] ),
        .I4(\axi_rdata[27]_i_15_0 ),
        .I5(\axi_rdata[11]_i_21_n_0 ),
        .O(\axi_rdata[11]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h888BFFFF888B0000)) 
    \axi_rdata[11]_i_18 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[17]),
        .I3(\slv_reg1_reg[23] ),
        .I4(\axi_rdata[27]_i_15_0 ),
        .I5(\axi_rdata[11]_i_22_n_0 ),
        .O(\axi_rdata[11]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'h888BFFFF888B0000)) 
    \axi_rdata[11]_i_19 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[16]),
        .I3(\slv_reg1_reg[23] ),
        .I4(\axi_rdata[27]_i_15_0 ),
        .I5(\axi_rdata[11]_i_23_n_0 ),
        .O(\axi_rdata[11]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h4747470047474747)) 
    \axi_rdata[11]_i_20 
       (.I0(ab_nudge[27]),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[11]),
        .I3(i__carry_i_11_n_0),
        .I4(i__carry_i_10_n_0),
        .I5(\axi_rdata[15]_i_23_n_0 ),
        .O(\axi_rdata[11]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'h4747470047474747)) 
    \axi_rdata[11]_i_21 
       (.I0(ab_nudge[26]),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[10]),
        .I3(i__carry_i_11_n_0),
        .I4(i__carry_i_10_n_0),
        .I5(\axi_rdata[15]_i_23_n_0 ),
        .O(\axi_rdata[11]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'h4747470047474747)) 
    \axi_rdata[11]_i_22 
       (.I0(ab_nudge[25]),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[9]),
        .I3(i__carry_i_11_n_0),
        .I4(i__carry_i_10_n_0),
        .I5(\axi_rdata[15]_i_23_n_0 ),
        .O(\axi_rdata[11]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'h4747470047474747)) 
    \axi_rdata[11]_i_23 
       (.I0(ab_nudge[24]),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[8]),
        .I3(i__carry_i_11_n_0),
        .I4(i__carry_i_10_n_0),
        .I5(\axi_rdata[15]_i_23_n_0 ),
        .O(\axi_rdata[11]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[11]_i_6 
       (.I0(\axi_rdata_reg[11]_i_2_1 ),
        .I1(\slv_reg4_reg[5] ),
        .I2(\axi_rdata[11]_i_13_n_0 ),
        .I3(\axi_rdata_reg[3]_i_2_2 ),
        .I4(\slv_reg4_reg[0] ),
        .I5(\axi_rdata_reg[3]_i_2_1 ),
        .O(SHIFT_RIGHT[9]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[11]_i_7 
       (.I0(\axi_rdata_reg[11]_i_2_0 ),
        .I1(\slv_reg4_reg[5] ),
        .I2(\axi_rdata[11]_i_13_n_0 ),
        .I3(\axi_rdata_reg[3]_i_2_1 ),
        .I4(\axi_rdata[11]_i_15_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_2 ),
        .O(SHIFT_RIGHT[8]));
  LUT6 #(
    .INIT(64'h888BFFFF888B0000)) 
    \axi_rdata[15]_i_17 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[22]),
        .I3(\slv_reg1_reg[23] ),
        .I4(\axi_rdata[27]_i_15_0 ),
        .I5(\axi_rdata[15]_i_20_n_0 ),
        .O(\slv_reg4_reg[7]_2 ));
  LUT6 #(
    .INIT(64'h888BFFFF888B0000)) 
    \axi_rdata[15]_i_18 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[21]),
        .I3(\slv_reg1_reg[23] ),
        .I4(\axi_rdata[27]_i_15_0 ),
        .I5(\axi_rdata[15]_i_21_n_0 ),
        .O(\slv_reg4_reg[7]_1 ));
  LUT6 #(
    .INIT(64'h888BFFFF888B0000)) 
    \axi_rdata[15]_i_19 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[20]),
        .I3(\slv_reg1_reg[23] ),
        .I4(\axi_rdata[27]_i_15_0 ),
        .I5(\axi_rdata[15]_i_22_n_0 ),
        .O(\slv_reg4_reg[7]_0 ));
  LUT6 #(
    .INIT(64'h4747470047474747)) 
    \axi_rdata[15]_i_20 
       (.I0(ab_nudge[30]),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[14]),
        .I3(i__carry_i_11_n_0),
        .I4(i__carry_i_10_n_0),
        .I5(\axi_rdata[15]_i_23_n_0 ),
        .O(\axi_rdata[15]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'h4747470047474747)) 
    \axi_rdata[15]_i_21 
       (.I0(ab_nudge[29]),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[13]),
        .I3(i__carry_i_11_n_0),
        .I4(i__carry_i_10_n_0),
        .I5(\axi_rdata[15]_i_23_n_0 ),
        .O(\axi_rdata[15]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'h4747470047474747)) 
    \axi_rdata[15]_i_22 
       (.I0(ab_nudge[28]),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[12]),
        .I3(i__carry_i_11_n_0),
        .I4(i__carry_i_10_n_0),
        .I5(\axi_rdata[15]_i_23_n_0 ),
        .O(\axi_rdata[15]_i_22_n_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \axi_rdata[15]_i_23 
       (.I0(xls[19]),
        .I1(xls[15]),
        .I2(xls[21]),
        .I3(i__carry_i_12_n_0),
        .I4(\axi_rdata[15]_i_24_n_0 ),
        .O(\axi_rdata[15]_i_23_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \axi_rdata[15]_i_24 
       (.I0(xls[18]),
        .I1(xls[8]),
        .I2(xls[23]),
        .I3(xls[12]),
        .O(\axi_rdata[15]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[19]_i_4 
       (.I0(\axi_rdata_reg[19]_i_2 ),
        .I1(\slv_reg4_reg[5] ),
        .I2(\axi_rdata[23]_i_15_n_0 ),
        .I3(\axi_rdata_reg[3]_i_2_1 ),
        .I4(\slv_reg4_reg[0]_0 ),
        .I5(\axi_rdata_reg[3]_i_2_2 ),
        .O(\slv_reg4_reg[5]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_9 
       (.I0(\axi_rdata[27]_i_20_n_0 ),
        .I1(\axi_rdata[23]_i_7_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\slv_reg4_reg[3] ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[19]_i_5_1 ),
        .O(\slv_reg4_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_11 
       (.I0(\axi_rdata[27]_i_21_n_0 ),
        .I1(\slv_reg4_reg[3]_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[27]_i_18_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[23]_i_6_0 ),
        .O(\axi_rdata[23]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_13 
       (.I0(\axi_rdata[23]_i_16_n_0 ),
        .I1(\slv_reg4_reg[3] ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[27]_i_20_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[23]_i_7_0 ),
        .O(\axi_rdata[23]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_15 
       (.I0(\axi_rdata[27]_i_18_n_0 ),
        .I1(\axi_rdata[23]_i_6_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\slv_reg4_reg[3]_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[19]_i_4_0 ),
        .O(\axi_rdata[23]_i_15_n_0 ));
  LUT5 #(
    .INIT(32'hC8C8C8CD)) 
    \axi_rdata[23]_i_16 
       (.I0(\axi_rdata[27]_i_15_0 ),
        .I1(\slv_reg1_reg[11] ),
        .I2(\axi_rdata[3]_i_22_0 ),
        .I3(ab_nudge[27]),
        .I4(\slv_reg1_reg[23] ),
        .O(\axi_rdata[23]_i_16_n_0 ));
  LUT5 #(
    .INIT(32'hC8C8C8CD)) 
    \axi_rdata[23]_i_17 
       (.I0(\axi_rdata[27]_i_15_0 ),
        .I1(\slv_reg1_reg[11] ),
        .I2(\axi_rdata[3]_i_22_0 ),
        .I3(ab_nudge[23]),
        .I4(\slv_reg1_reg[23] ),
        .O(\slv_reg4_reg[3] ));
  LUT6 #(
    .INIT(64'hAAAAAAAA20AA2020)) 
    \axi_rdata[23]_i_4 
       (.I0(\axi_rdata_reg[23]_i_2_3 ),
        .I1(\axi_rdata[23]_i_9_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_2 ),
        .I3(\axi_rdata[27]_i_15_n_0 ),
        .I4(\axi_rdata_reg[3]_i_2_1 ),
        .I5(\slv_reg4_reg[5] ),
        .O(SHIFT_RIGHT[23]));
  LUT6 #(
    .INIT(64'hAAAAAAAA20AA2020)) 
    \axi_rdata[23]_i_5 
       (.I0(\axi_rdata_reg[23]_i_2_2 ),
        .I1(\axi_rdata[23]_i_11_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_2 ),
        .I3(\axi_rdata[23]_i_9_n_0 ),
        .I4(\axi_rdata_reg[3]_i_2_1 ),
        .I5(\slv_reg4_reg[5] ),
        .O(SHIFT_RIGHT[22]));
  LUT6 #(
    .INIT(64'hAAAAAAAA20AA2020)) 
    \axi_rdata[23]_i_6 
       (.I0(\axi_rdata_reg[23]_i_2_1 ),
        .I1(\axi_rdata[23]_i_13_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_2 ),
        .I3(\axi_rdata[23]_i_11_n_0 ),
        .I4(\axi_rdata_reg[3]_i_2_1 ),
        .I5(\slv_reg4_reg[5] ),
        .O(SHIFT_RIGHT[21]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[23]_i_7 
       (.I0(\axi_rdata_reg[23]_i_2_0 ),
        .I1(\slv_reg4_reg[5] ),
        .I2(\axi_rdata[23]_i_13_n_0 ),
        .I3(\axi_rdata_reg[3]_i_2_1 ),
        .I4(\axi_rdata[23]_i_15_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_2 ),
        .O(SHIFT_RIGHT[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_9 
       (.I0(\axi_rdata[27]_i_19_n_0 ),
        .I1(\axi_rdata[27]_i_20_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[23]_i_16_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\slv_reg4_reg[3] ),
        .O(\axi_rdata[23]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[27]_i_11 
       (.I0(\axi_rdata[31]_i_20_n_0 ),
        .I1(\axi_rdata[31]_i_7_0 ),
        .I2(\axi_rdata[27]_i_17_n_0 ),
        .I3(\axi_rdata[31]_i_17_0 ),
        .I4(\axi_rdata[27]_i_18_n_0 ),
        .O(\axi_rdata[27]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[27]_i_13 
       (.I0(\axi_rdata[27]_i_16_n_0 ),
        .I1(\axi_rdata[31]_i_7_0 ),
        .I2(\axi_rdata[27]_i_19_n_0 ),
        .I3(\axi_rdata[31]_i_17_0 ),
        .I4(\axi_rdata[27]_i_20_n_0 ),
        .O(\axi_rdata[27]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_15 
       (.I0(\axi_rdata[27]_i_17_n_0 ),
        .I1(\axi_rdata[27]_i_18_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[27]_i_21_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\slv_reg4_reg[3]_0 ),
        .O(\axi_rdata[27]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hF0E0F0E0F0E0F0F1)) 
    \axi_rdata[27]_i_16 
       (.I0(\axi_rdata[31]_i_17_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(\slv_reg1_reg[11] ),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[27]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[27]_i_16_n_0 ));
  LUT5 #(
    .INIT(32'hC8C8C8CD)) 
    \axi_rdata[27]_i_17 
       (.I0(\axi_rdata[27]_i_15_0 ),
        .I1(\slv_reg1_reg[11] ),
        .I2(\axi_rdata[3]_i_22_0 ),
        .I3(ab_nudge[30]),
        .I4(\slv_reg1_reg[23] ),
        .O(\axi_rdata[27]_i_17_n_0 ));
  LUT5 #(
    .INIT(32'hC8C8C8CD)) 
    \axi_rdata[27]_i_18 
       (.I0(\axi_rdata[27]_i_15_0 ),
        .I1(\slv_reg1_reg[11] ),
        .I2(\axi_rdata[3]_i_22_0 ),
        .I3(ab_nudge[26]),
        .I4(\slv_reg1_reg[23] ),
        .O(\axi_rdata[27]_i_18_n_0 ));
  LUT5 #(
    .INIT(32'hC8C8C8CD)) 
    \axi_rdata[27]_i_19 
       (.I0(\axi_rdata[27]_i_15_0 ),
        .I1(\slv_reg1_reg[11] ),
        .I2(\axi_rdata[3]_i_22_0 ),
        .I3(ab_nudge[29]),
        .I4(\slv_reg1_reg[23] ),
        .O(\axi_rdata[27]_i_19_n_0 ));
  LUT5 #(
    .INIT(32'hC8C8C8CD)) 
    \axi_rdata[27]_i_20 
       (.I0(\axi_rdata[27]_i_15_0 ),
        .I1(\slv_reg1_reg[11] ),
        .I2(\axi_rdata[3]_i_22_0 ),
        .I3(ab_nudge[25]),
        .I4(\slv_reg1_reg[23] ),
        .O(\axi_rdata[27]_i_20_n_0 ));
  LUT5 #(
    .INIT(32'hC8C8C8CD)) 
    \axi_rdata[27]_i_21 
       (.I0(\axi_rdata[27]_i_15_0 ),
        .I1(\slv_reg1_reg[11] ),
        .I2(\axi_rdata[3]_i_22_0 ),
        .I3(ab_nudge[28]),
        .I4(\slv_reg1_reg[23] ),
        .O(\axi_rdata[27]_i_21_n_0 ));
  LUT5 #(
    .INIT(32'hC8C8C8CD)) 
    \axi_rdata[27]_i_22 
       (.I0(\axi_rdata[27]_i_15_0 ),
        .I1(\slv_reg1_reg[11] ),
        .I2(\axi_rdata[3]_i_22_0 ),
        .I3(ab_nudge[24]),
        .I4(\slv_reg1_reg[23] ),
        .O(\slv_reg4_reg[3]_0 ));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[27]_i_4 
       (.I0(\axi_rdata_reg[27]_i_2_3 ),
        .I1(\slv_reg4_reg[5] ),
        .I2(\axi_rdata[31]_i_17_n_0 ),
        .I3(\axi_rdata_reg[3]_i_2_1 ),
        .I4(\axi_rdata[27]_i_9_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_2 ),
        .O(SHIFT_RIGHT[27]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[27]_i_5 
       (.I0(\axi_rdata_reg[27]_i_2_2 ),
        .I1(\slv_reg4_reg[5] ),
        .I2(\axi_rdata[27]_i_9_n_0 ),
        .I3(\axi_rdata_reg[3]_i_2_1 ),
        .I4(\axi_rdata[27]_i_11_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_2 ),
        .O(SHIFT_RIGHT[26]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[27]_i_6 
       (.I0(\axi_rdata_reg[27]_i_2_1 ),
        .I1(\slv_reg4_reg[5] ),
        .I2(\axi_rdata[27]_i_11_n_0 ),
        .I3(\axi_rdata_reg[3]_i_2_1 ),
        .I4(\axi_rdata[27]_i_13_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_2 ),
        .O(SHIFT_RIGHT[25]));
  LUT6 #(
    .INIT(64'hAAAAAAAA20AA2020)) 
    \axi_rdata[27]_i_7 
       (.I0(\axi_rdata_reg[27]_i_2_0 ),
        .I1(\axi_rdata[27]_i_15_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_2 ),
        .I3(\axi_rdata[27]_i_13_n_0 ),
        .I4(\axi_rdata_reg[3]_i_2_1 ),
        .I5(\slv_reg4_reg[5] ),
        .O(SHIFT_RIGHT[24]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[27]_i_9 
       (.I0(\axi_rdata[31]_i_19_n_0 ),
        .I1(\axi_rdata[31]_i_7_0 ),
        .I2(\axi_rdata[27]_i_16_n_0 ),
        .O(\axi_rdata[27]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[31]_i_10 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata[31]_i_7_0 ),
        .I2(\axi_rdata[31]_i_18_n_0 ),
        .O(\axi_rdata[31]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[31]_i_13 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata[31]_i_7_0 ),
        .I2(\axi_rdata[31]_i_19_n_0 ),
        .O(\axi_rdata[31]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h0000DF00FFFFFFFF)) 
    \axi_rdata[31]_i_14 
       (.I0(\axi_rdata[19]_i_5 [1]),
        .I1(\axi_rdata[19]_i_5_0 ),
        .I2(\axi_rdata[19]_i_5 [2]),
        .I3(\axi_rdata[19]_i_5 [3]),
        .I4(\slv_reg1_reg[11] ),
        .I5(\axi_rdata_reg[31]_i_3_0 ),
        .O(\slv_reg4_reg[5] ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[31]_i_17 
       (.I0(\axi_rdata[31]_i_18_n_0 ),
        .I1(\axi_rdata[31]_i_7_0 ),
        .I2(\axi_rdata[31]_i_20_n_0 ),
        .O(\axi_rdata[31]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hF0E0F0E0F0E0F0F1)) 
    \axi_rdata[31]_i_18 
       (.I0(\axi_rdata[31]_i_17_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(\slv_reg1_reg[11] ),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[30]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[31]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hF0E0F0E0F0E0F0F1)) 
    \axi_rdata[31]_i_19 
       (.I0(\axi_rdata[31]_i_17_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(\slv_reg1_reg[11] ),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[29]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[31]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hF0E0F0E0F0E0F0F1)) 
    \axi_rdata[31]_i_20 
       (.I0(\axi_rdata[31]_i_17_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(\slv_reg1_reg[11] ),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[28]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[31]_i_20_n_0 ));
  LUT4 #(
    .INIT(16'h4474)) 
    \axi_rdata[31]_i_5 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata_reg[31]_i_3_0 ),
        .I2(O[1]),
        .I3(\axi_rdata_reg[31]_i_3_3 ),
        .O(SHIFT_RIGHT[31]));
  LUT6 #(
    .INIT(64'h474747470000FF00)) 
    \axi_rdata[31]_i_6 
       (.I0(\axi_rdata[31]_i_10_n_0 ),
        .I1(\axi_rdata_reg[3]_i_2_2 ),
        .I2(\slv_reg1_reg[11] ),
        .I3(O[0]),
        .I4(\axi_rdata_reg[31]_i_3_3 ),
        .I5(\axi_rdata_reg[31]_i_3_0 ),
        .O(SHIFT_RIGHT[30]));
  LUT6 #(
    .INIT(64'hAA20AAAAAA20AA20)) 
    \axi_rdata[31]_i_7 
       (.I0(\axi_rdata_reg[31]_i_3_2 ),
        .I1(\axi_rdata[31]_i_13_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_2 ),
        .I3(\slv_reg4_reg[5] ),
        .I4(\axi_rdata[31]_i_10_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_1 ),
        .O(SHIFT_RIGHT[29]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[31]_i_8 
       (.I0(\axi_rdata_reg[31]_i_3_1 ),
        .I1(\slv_reg4_reg[5] ),
        .I2(\axi_rdata[31]_i_17_n_0 ),
        .I3(\axi_rdata_reg[3]_i_2_2 ),
        .I4(\axi_rdata[31]_i_13_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_1 ),
        .O(SHIFT_RIGHT[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_10 
       (.I0(\axi_rdata[7]_i_16_n_0 ),
        .I1(\axi_rdata[3]_i_20_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[7]_i_18_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[3]_i_21_n_0 ),
        .O(\axi_rdata[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_12 
       (.I0(\axi_rdata[11]_i_18_n_0 ),
        .I1(\axi_rdata[7]_i_18_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[7]_i_16_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[3]_i_20_n_0 ),
        .O(\axi_rdata[3]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_14 
       (.I0(\axi_rdata[11]_i_19_n_0 ),
        .I1(\axi_rdata[7]_i_19_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[7]_i_17_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[3]_i_18_n_0 ),
        .O(\axi_rdata[3]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'h000002A2AAAA02A2)) 
    \axi_rdata[3]_i_17 
       (.I0(\axi_rdata_reg[3]_i_2_2 ),
        .I1(\axi_rdata[3]_i_19_n_0 ),
        .I2(\axi_rdata[31]_i_17_0 ),
        .I3(\axi_rdata[7]_i_19_n_0 ),
        .I4(\axi_rdata[31]_i_7_0 ),
        .I5(\axi_rdata[3]_i_22_n_0 ),
        .O(\axi_rdata[3]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h888888888B888BBB)) 
    \axi_rdata[3]_i_18 
       (.I0(\axi_rdata[11]_i_21_n_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(ab_nudge[18]),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[2]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[3]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'h888888888B888BBB)) 
    \axi_rdata[3]_i_19 
       (.I0(\axi_rdata[11]_i_23_n_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(ab_nudge[16]),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[0]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[3]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h888888888B888BBB)) 
    \axi_rdata[3]_i_20 
       (.I0(\axi_rdata[11]_i_20_n_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(ab_nudge[19]),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[3]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[3]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'h888888888B888BBB)) 
    \axi_rdata[3]_i_21 
       (.I0(\axi_rdata[11]_i_22_n_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(ab_nudge[17]),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[1]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[3]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_22 
       (.I0(\axi_rdata[15]_i_20_n_0 ),
        .I1(\axi_rdata[3]_i_23_n_0 ),
        .I2(\axi_rdata[31]_i_17_0 ),
        .I3(\axi_rdata[11]_i_21_n_0 ),
        .I4(\axi_rdata[27]_i_15_0 ),
        .I5(\axi_rdata[3]_i_24_n_0 ),
        .O(\axi_rdata[3]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'h4747470047474747)) 
    \axi_rdata[3]_i_23 
       (.I0(ab_nudge[22]),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[6]),
        .I3(i__carry_i_11_n_0),
        .I4(i__carry_i_10_n_0),
        .I5(\axi_rdata[15]_i_23_n_0 ),
        .O(\axi_rdata[3]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'h4747470047474747)) 
    \axi_rdata[3]_i_24 
       (.I0(ab_nudge[18]),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[2]),
        .I3(i__carry_i_11_n_0),
        .I4(i__carry_i_10_n_0),
        .I5(\axi_rdata[15]_i_23_n_0 ),
        .O(\axi_rdata[3]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'h00000000BABAFFBA)) 
    \axi_rdata[3]_i_4 
       (.I0(\slv_reg4_reg[5] ),
        .I1(\axi_rdata[3]_i_9_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_2 ),
        .I3(\axi_rdata_reg[3]_i_2_1 ),
        .I4(\axi_rdata[3]_i_10_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_0 ),
        .O(SHIFT_RIGHT[0]));
  LUT6 #(
    .INIT(64'h00000000BABAFFBA)) 
    \axi_rdata[3]_i_5 
       (.I0(\slv_reg4_reg[5] ),
        .I1(\axi_rdata[7]_i_15_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_1 ),
        .I3(\axi_rdata_reg[3]_i_2_2 ),
        .I4(\axi_rdata[3]_i_12_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_5 ),
        .O(SHIFT_RIGHT[3]));
  LUT6 #(
    .INIT(64'h00000000BABAFFBA)) 
    \axi_rdata[3]_i_6 
       (.I0(\slv_reg4_reg[5] ),
        .I1(\axi_rdata[3]_i_12_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_1 ),
        .I3(\axi_rdata_reg[3]_i_2_2 ),
        .I4(\axi_rdata[3]_i_14_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_4 ),
        .O(SHIFT_RIGHT[2]));
  LUT6 #(
    .INIT(64'h00000000BABAFFBA)) 
    \axi_rdata[3]_i_7 
       (.I0(\slv_reg4_reg[5] ),
        .I1(\axi_rdata[3]_i_10_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_2 ),
        .I3(\axi_rdata_reg[3]_i_2_1 ),
        .I4(\axi_rdata[3]_i_14_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_3 ),
        .O(SHIFT_RIGHT[1]));
  LUT6 #(
    .INIT(64'h55555510AAAAAAEF)) 
    \axi_rdata[3]_i_8 
       (.I0(\axi_rdata_reg[3]_i_2_0 ),
        .I1(\axi_rdata[3]_i_10_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_1 ),
        .I3(\axi_rdata[3]_i_17_n_0 ),
        .I4(\slv_reg4_reg[5] ),
        .I5(CO),
        .O(\axi_rdata[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_9 
       (.I0(\axi_rdata[7]_i_17_n_0 ),
        .I1(\axi_rdata[3]_i_18_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[7]_i_19_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[3]_i_19_n_0 ),
        .O(\axi_rdata[3]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_10 
       (.I0(\slv_reg4_reg[7]_0 ),
        .I1(\axi_rdata[11]_i_19_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[11]_i_17_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[7]_i_17_n_0 ),
        .O(\axi_rdata[7]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_12 
       (.I0(\slv_reg4_reg[7] ),
        .I1(\axi_rdata[7]_i_16_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[11]_i_18_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[7]_i_18_n_0 ),
        .O(\axi_rdata[7]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_15 
       (.I0(\axi_rdata[11]_i_17_n_0 ),
        .I1(\axi_rdata[7]_i_17_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\axi_rdata[11]_i_19_n_0 ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[7]_i_19_n_0 ),
        .O(\axi_rdata[7]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h888BFFFF888B0000)) 
    \axi_rdata[7]_i_16 
       (.I0(\slv_reg1_reg[11] ),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[15]),
        .I3(\slv_reg1_reg[23] ),
        .I4(\axi_rdata[27]_i_15_0 ),
        .I5(\axi_rdata[7]_i_20_n_0 ),
        .O(\axi_rdata[7]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h888888888B888BBB)) 
    \axi_rdata[7]_i_17 
       (.I0(\axi_rdata[15]_i_20_n_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(ab_nudge[22]),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[6]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[7]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h888888888B888BBB)) 
    \axi_rdata[7]_i_18 
       (.I0(\axi_rdata[15]_i_21_n_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(ab_nudge[21]),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[5]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[7]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'h888888888B888BBB)) 
    \axi_rdata[7]_i_19 
       (.I0(\axi_rdata[15]_i_22_n_0 ),
        .I1(\axi_rdata[27]_i_15_0 ),
        .I2(ab_nudge[20]),
        .I3(\axi_rdata[3]_i_22_0 ),
        .I4(ab_nudge[4]),
        .I5(\slv_reg1_reg[23] ),
        .O(\axi_rdata[7]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h4747470047474747)) 
    \axi_rdata[7]_i_20 
       (.I0(ab_nudge[23]),
        .I1(\axi_rdata[3]_i_22_0 ),
        .I2(ab_nudge[7]),
        .I3(i__carry_i_11_n_0),
        .I4(i__carry_i_10_n_0),
        .I5(\axi_rdata[15]_i_23_n_0 ),
        .O(\axi_rdata[7]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'h00000000BABAFFBA)) 
    \axi_rdata[7]_i_4 
       (.I0(\slv_reg4_reg[5] ),
        .I1(\axi_rdata[7]_i_8_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_2 ),
        .I3(\axi_rdata_reg[3]_i_2_1 ),
        .I4(\axi_rdata[11]_i_15_n_0 ),
        .I5(\axi_rdata_reg[7]_i_2_3 ),
        .O(SHIFT_RIGHT[7]));
  LUT6 #(
    .INIT(64'h00000000BABAFFBA)) 
    \axi_rdata[7]_i_5 
       (.I0(\slv_reg4_reg[5] ),
        .I1(\axi_rdata[7]_i_8_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_1 ),
        .I3(\axi_rdata_reg[3]_i_2_2 ),
        .I4(\axi_rdata[7]_i_10_n_0 ),
        .I5(\axi_rdata_reg[7]_i_2_2 ),
        .O(SHIFT_RIGHT[6]));
  LUT6 #(
    .INIT(64'h00000000BABAFFBA)) 
    \axi_rdata[7]_i_6 
       (.I0(\slv_reg4_reg[5] ),
        .I1(\axi_rdata[7]_i_10_n_0 ),
        .I2(\axi_rdata_reg[3]_i_2_1 ),
        .I3(\axi_rdata_reg[3]_i_2_2 ),
        .I4(\axi_rdata[7]_i_12_n_0 ),
        .I5(\axi_rdata_reg[7]_i_2_1 ),
        .O(SHIFT_RIGHT[5]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[7]_i_7 
       (.I0(\axi_rdata_reg[7]_i_2_0 ),
        .I1(\slv_reg4_reg[5] ),
        .I2(\axi_rdata[7]_i_15_n_0 ),
        .I3(\axi_rdata_reg[3]_i_2_2 ),
        .I4(\axi_rdata[7]_i_12_n_0 ),
        .I5(\axi_rdata_reg[3]_i_2_1 ),
        .O(SHIFT_RIGHT[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_8 
       (.I0(\slv_reg4_reg[7]_1 ),
        .I1(\axi_rdata[11]_i_18_n_0 ),
        .I2(\axi_rdata[31]_i_7_0 ),
        .I3(\slv_reg4_reg[7] ),
        .I4(\axi_rdata[31]_i_17_0 ),
        .I5(\axi_rdata[7]_i_16_n_0 ),
        .O(\axi_rdata[7]_i_8_n_0 ));
  MUXF7 \axi_rdata_reg[0]_i_1 
       (.I0(\axi_rdata_reg[0] ),
        .I1(\axi_rdata[0]_i_3_n_0 ),
        .O(D),
        .S(sel0[2]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \axi_rdata_reg[11]_i_2 
       (.CI(\axi_rdata_reg[7]_i_2_n_0 ),
        .CO({\axi_rdata[11]_i_7_0 ,\axi_rdata_reg[11]_i_2_n_1 ,\axi_rdata_reg[11]_i_2_n_2 ,\axi_rdata_reg[11]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\axi_rdata[31]_i_8_0 [10:7]),
        .S({\axi_rdata_reg[11] ,SHIFT_RIGHT[9:8]}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \axi_rdata_reg[23]_i_2 
       (.CI(\axi_rdata_reg[23] ),
        .CO({\axi_rdata_reg[23]_i_2_n_0 ,\axi_rdata_reg[23]_i_2_n_1 ,\axi_rdata_reg[23]_i_2_n_2 ,\axi_rdata_reg[23]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\axi_rdata[31]_i_8_0 [14:11]),
        .S(SHIFT_RIGHT[23:20]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \axi_rdata_reg[27]_i_2 
       (.CI(\axi_rdata_reg[23]_i_2_n_0 ),
        .CO({\axi_rdata_reg[27]_i_2_n_0 ,\axi_rdata_reg[27]_i_2_n_1 ,\axi_rdata_reg[27]_i_2_n_2 ,\axi_rdata_reg[27]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\axi_rdata[31]_i_8_0 [18:15]),
        .S(SHIFT_RIGHT[27:24]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \axi_rdata_reg[31]_i_3 
       (.CI(\axi_rdata_reg[27]_i_2_n_0 ),
        .CO({\NLW_axi_rdata_reg[31]_i_3_CO_UNCONNECTED [3],\axi_rdata_reg[31]_i_3_n_1 ,\axi_rdata_reg[31]_i_3_n_2 ,\axi_rdata_reg[31]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\axi_rdata[31]_i_8_0 [22:19]),
        .S(SHIFT_RIGHT[31:28]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \axi_rdata_reg[3]_i_2 
       (.CI(1'b0),
        .CO({\axi_rdata_reg[3]_i_2_n_0 ,\axi_rdata_reg[3]_i_2_n_1 ,\axi_rdata_reg[3]_i_2_n_2 ,\axi_rdata_reg[3]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,SHIFT_RIGHT[0]}),
        .O({\axi_rdata[31]_i_8_0 [2:0],tf_out}),
        .S({SHIFT_RIGHT[3:1],\axi_rdata[3]_i_8_n_0 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \axi_rdata_reg[7]_i_2 
       (.CI(\axi_rdata_reg[3]_i_2_n_0 ),
        .CO({\axi_rdata_reg[7]_i_2_n_0 ,\axi_rdata_reg[7]_i_2_n_1 ,\axi_rdata_reg[7]_i_2_n_2 ,\axi_rdata_reg[7]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\axi_rdata[31]_i_8_0 [6:3]),
        .S(SHIFT_RIGHT[7:4]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_1
       (.I0(xls[21]),
        .I1(Q[21]),
        .I2(xls[22]),
        .I3(Q[22]),
        .I4(Q[23]),
        .I5(xls[23]),
        .O(i__carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_2
       (.I0(xls[19]),
        .I1(Q[19]),
        .I2(xls[18]),
        .I3(Q[18]),
        .I4(Q[20]),
        .I5(xls[20]),
        .O(i__carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_3
       (.I0(xls[15]),
        .I1(Q[15]),
        .I2(xls[16]),
        .I3(Q[16]),
        .I4(Q[17]),
        .I5(xls[17]),
        .O(i__carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_4
       (.I0(xls[12]),
        .I1(Q[12]),
        .I2(xls[13]),
        .I3(Q[13]),
        .I4(Q[14]),
        .I5(xls[14]),
        .O(i__carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__1_i_1
       (.I0(xls[31]),
        .I1(Q[31]),
        .I2(Q[30]),
        .I3(xls[30]),
        .O(i__carry__1_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__1_i_2
       (.I0(xls[27]),
        .I1(Q[27]),
        .I2(xls[28]),
        .I3(Q[28]),
        .I4(Q[29]),
        .I5(xls[29]),
        .O(i__carry__1_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__1_i_3
       (.I0(xls[24]),
        .I1(Q[24]),
        .I2(xls[25]),
        .I3(Q[25]),
        .I4(Q[26]),
        .I5(xls[26]),
        .O(i__carry__1_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    i__carry__6_i_10
       (.I0(xls[18]),
        .I1(xls[19]),
        .I2(xls[20]),
        .I3(xls[23]),
        .I4(xls[15]),
        .I5(xls[12]),
        .O(i__carry__6_i_10_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    i__carry__6_i_11
       (.I0(xls[29]),
        .I1(xls[28]),
        .I2(xls[24]),
        .I3(xls[27]),
        .I4(xls[25]),
        .I5(xls[26]),
        .O(i__carry__6_i_11_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    i__carry__6_i_12
       (.I0(xls[30]),
        .I1(xls[2]),
        .I2(xls[31]),
        .I3(xls[1]),
        .O(i__carry__6_i_12_n_0));
  LUT6 #(
    .INIT(64'h00010000FFFFFFFF)) 
    i__carry__6_i_6
       (.I0(i__carry__6_i_7_n_0),
        .I1(xls[8]),
        .I2(xls[11]),
        .I3(i__carry__6_i_8_n_0),
        .I4(i__carry__6_i_9_n_0),
        .I5(ab_nudge[31]),
        .O(\slv_reg1_reg[11] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    i__carry__6_i_7
       (.I0(xls[7]),
        .I1(xls[6]),
        .I2(xls[9]),
        .I3(xls[14]),
        .I4(xls[10]),
        .I5(xls[13]),
        .O(i__carry__6_i_7_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFEF)) 
    i__carry__6_i_8
       (.I0(i__carry__6_i_10_n_0),
        .I1(xls[21]),
        .I2(\SRDHM_overflow0_inferred__0/i__carry__1_n_1 ),
        .I3(xls[22]),
        .I4(xls[16]),
        .I5(xls[17]),
        .O(i__carry__6_i_8_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    i__carry__6_i_9
       (.I0(i__carry__6_i_11_n_0),
        .I1(i__carry__6_i_12_n_0),
        .I2(xls[5]),
        .I3(xls[4]),
        .I4(xls[3]),
        .I5(xls[0]),
        .O(i__carry__6_i_9_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_1
       (.I0(xls[9]),
        .I1(Q[9]),
        .I2(xls[10]),
        .I3(Q[10]),
        .I4(Q[11]),
        .I5(xls[11]),
        .O(i__carry_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    i__carry_i_10
       (.I0(i__carry_i_13_n_0),
        .I1(i__carry_i_14_n_0),
        .I2(xls[3]),
        .I3(xls[2]),
        .I4(i__carry_i_15_n_0),
        .I5(i__carry_i_16_n_0),
        .O(i__carry_i_10_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    i__carry_i_11
       (.I0(xls[4]),
        .I1(xls[0]),
        .I2(xls[10]),
        .I3(xls[25]),
        .I4(xls[9]),
        .I5(xls[31]),
        .O(i__carry_i_11_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    i__carry_i_12
       (.I0(xls[24]),
        .I1(xls[13]),
        .I2(xls[30]),
        .I3(xls[11]),
        .O(i__carry_i_12_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    i__carry_i_13
       (.I0(xls[28]),
        .I1(xls[26]),
        .I2(xls[29]),
        .I3(xls[5]),
        .O(i__carry_i_13_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    i__carry_i_14
       (.I0(xls[6]),
        .I1(xls[7]),
        .O(i__carry_i_14_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    i__carry_i_15
       (.I0(xls[20]),
        .I1(xls[14]),
        .I2(xls[17]),
        .I3(xls[1]),
        .O(i__carry_i_15_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    i__carry_i_16
       (.I0(\SRDHM_overflow0_inferred__0/i__carry__1_n_1 ),
        .I1(xls[22]),
        .I2(xls[27]),
        .I3(xls[16]),
        .O(i__carry_i_16_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_2
       (.I0(xls[8]),
        .I1(Q[8]),
        .I2(xls[6]),
        .I3(Q[6]),
        .I4(Q[7]),
        .I5(xls[7]),
        .O(i__carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_3
       (.I0(xls[3]),
        .I1(Q[3]),
        .I2(xls[4]),
        .I3(Q[4]),
        .I4(Q[5]),
        .I5(xls[5]),
        .O(i__carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_4
       (.I0(xls[0]),
        .I1(Q[0]),
        .I2(xls[1]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(xls[2]),
        .O(i__carry_i_4_n_0));
  LUT5 #(
    .INIT(32'h9999999A)) 
    i__carry_i_6
       (.I0(i__carry_i_1__1),
        .I1(\slv_reg1_reg[11] ),
        .I2(\slv_reg1_reg[23] ),
        .I3(ab_nudge[30]),
        .I4(ab_nudge[31]),
        .O(sg_ab_64_p3_reg));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    i__carry_i_7
       (.I0(i__carry_i_9_n_0),
        .I1(xls[21]),
        .I2(xls[15]),
        .I3(xls[19]),
        .I4(i__carry_i_10_n_0),
        .I5(i__carry_i_11_n_0),
        .O(\slv_reg1_reg[23] ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    i__carry_i_9
       (.I0(xls[12]),
        .I1(xls[23]),
        .I2(xls[8]),
        .I3(xls[18]),
        .I4(i__carry_i_12_n_0),
        .O(i__carry_i_9_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sg_xowb_carry
       (.CI(1'b0),
        .CO({sg_xowb_carry_n_0,sg_xowb_carry_n_1,sg_xowb_carry_n_2,sg_xowb_carry_n_3}),
        .CYINIT(1'b0),
        .DI(\sg_a_p0_reg[31] [3:0]),
        .O(xls[3:0]),
        .S({sg_xowb_carry_i_1_n_0,sg_xowb_carry_i_2_n_0,sg_xowb_carry_i_3_n_0,sg_xowb_carry_i_4_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sg_xowb_carry__0
       (.CI(sg_xowb_carry_n_0),
        .CO({sg_xowb_carry__0_n_0,sg_xowb_carry__0_n_1,sg_xowb_carry__0_n_2,sg_xowb_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\sg_a_p0_reg[31] [7:4]),
        .O(xls[7:4]),
        .S({sg_xowb_carry__0_i_1_n_0,sg_xowb_carry__0_i_2_n_0,sg_xowb_carry__0_i_3_n_0,sg_xowb_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__0_i_1
       (.I0(\sg_a_p0_reg[31] [7]),
        .I1(\sg_a_p0_reg[27] [7]),
        .O(sg_xowb_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__0_i_2
       (.I0(\sg_a_p0_reg[31] [6]),
        .I1(\sg_a_p0_reg[27] [6]),
        .O(sg_xowb_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__0_i_3
       (.I0(\sg_a_p0_reg[31] [5]),
        .I1(\sg_a_p0_reg[27] [5]),
        .O(sg_xowb_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__0_i_4
       (.I0(\sg_a_p0_reg[31] [4]),
        .I1(\sg_a_p0_reg[27] [4]),
        .O(sg_xowb_carry__0_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sg_xowb_carry__1
       (.CI(sg_xowb_carry__0_n_0),
        .CO({sg_xowb_carry__1_n_0,sg_xowb_carry__1_n_1,sg_xowb_carry__1_n_2,sg_xowb_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(\sg_a_p0_reg[31] [11:8]),
        .O(xls[11:8]),
        .S({sg_xowb_carry__1_i_1_n_0,sg_xowb_carry__1_i_2_n_0,sg_xowb_carry__1_i_3_n_0,sg_xowb_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__1_i_1
       (.I0(\sg_a_p0_reg[31] [11]),
        .I1(\sg_a_p0_reg[27] [11]),
        .O(sg_xowb_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__1_i_2
       (.I0(\sg_a_p0_reg[31] [10]),
        .I1(\sg_a_p0_reg[27] [10]),
        .O(sg_xowb_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__1_i_3
       (.I0(\sg_a_p0_reg[31] [9]),
        .I1(\sg_a_p0_reg[27] [9]),
        .O(sg_xowb_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__1_i_4
       (.I0(\sg_a_p0_reg[31] [8]),
        .I1(\sg_a_p0_reg[27] [8]),
        .O(sg_xowb_carry__1_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sg_xowb_carry__2
       (.CI(sg_xowb_carry__1_n_0),
        .CO({sg_xowb_carry__2_n_0,sg_xowb_carry__2_n_1,sg_xowb_carry__2_n_2,sg_xowb_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(\sg_a_p0_reg[31] [15:12]),
        .O(xls[15:12]),
        .S({sg_xowb_carry__2_i_1_n_0,sg_xowb_carry__2_i_2_n_0,sg_xowb_carry__2_i_3_n_0,sg_xowb_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__2_i_1
       (.I0(\sg_a_p0_reg[31] [15]),
        .I1(\sg_a_p0_reg[27] [15]),
        .O(sg_xowb_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__2_i_2
       (.I0(\sg_a_p0_reg[31] [14]),
        .I1(\sg_a_p0_reg[27] [14]),
        .O(sg_xowb_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__2_i_3
       (.I0(\sg_a_p0_reg[31] [13]),
        .I1(\sg_a_p0_reg[27] [13]),
        .O(sg_xowb_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__2_i_4
       (.I0(\sg_a_p0_reg[31] [12]),
        .I1(\sg_a_p0_reg[27] [12]),
        .O(sg_xowb_carry__2_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sg_xowb_carry__3
       (.CI(sg_xowb_carry__2_n_0),
        .CO({sg_xowb_carry__3_n_0,sg_xowb_carry__3_n_1,sg_xowb_carry__3_n_2,sg_xowb_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(\sg_a_p0_reg[31] [19:16]),
        .O(xls[19:16]),
        .S({sg_xowb_carry__3_i_1_n_0,sg_xowb_carry__3_i_2_n_0,sg_xowb_carry__3_i_3_n_0,sg_xowb_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__3_i_1
       (.I0(\sg_a_p0_reg[31] [19]),
        .I1(\sg_a_p0_reg[27] [19]),
        .O(sg_xowb_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__3_i_2
       (.I0(\sg_a_p0_reg[31] [18]),
        .I1(\sg_a_p0_reg[27] [18]),
        .O(sg_xowb_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__3_i_3
       (.I0(\sg_a_p0_reg[31] [17]),
        .I1(\sg_a_p0_reg[27] [17]),
        .O(sg_xowb_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__3_i_4
       (.I0(\sg_a_p0_reg[31] [16]),
        .I1(\sg_a_p0_reg[27] [16]),
        .O(sg_xowb_carry__3_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sg_xowb_carry__4
       (.CI(sg_xowb_carry__3_n_0),
        .CO({sg_xowb_carry__4_n_0,sg_xowb_carry__4_n_1,sg_xowb_carry__4_n_2,sg_xowb_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(\sg_a_p0_reg[31] [23:20]),
        .O(xls[23:20]),
        .S({sg_xowb_carry__4_i_1_n_0,sg_xowb_carry__4_i_2_n_0,sg_xowb_carry__4_i_3_n_0,sg_xowb_carry__4_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__4_i_1
       (.I0(\sg_a_p0_reg[31] [23]),
        .I1(\sg_a_p0_reg[27] [23]),
        .O(sg_xowb_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__4_i_2
       (.I0(\sg_a_p0_reg[31] [22]),
        .I1(\sg_a_p0_reg[27] [22]),
        .O(sg_xowb_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__4_i_3
       (.I0(\sg_a_p0_reg[31] [21]),
        .I1(\sg_a_p0_reg[27] [21]),
        .O(sg_xowb_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__4_i_4
       (.I0(\sg_a_p0_reg[31] [20]),
        .I1(\sg_a_p0_reg[27] [20]),
        .O(sg_xowb_carry__4_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sg_xowb_carry__5
       (.CI(sg_xowb_carry__4_n_0),
        .CO({sg_xowb_carry__5_n_0,sg_xowb_carry__5_n_1,sg_xowb_carry__5_n_2,sg_xowb_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(\sg_a_p0_reg[31] [27:24]),
        .O(xls[27:24]),
        .S({sg_xowb_carry__5_i_1_n_0,sg_xowb_carry__5_i_2_n_0,sg_xowb_carry__5_i_3_n_0,sg_xowb_carry__5_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__5_i_1
       (.I0(\sg_a_p0_reg[31] [27]),
        .I1(\sg_a_p0_reg[27] [27]),
        .O(sg_xowb_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__5_i_2
       (.I0(\sg_a_p0_reg[31] [26]),
        .I1(\sg_a_p0_reg[27] [26]),
        .O(sg_xowb_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__5_i_3
       (.I0(\sg_a_p0_reg[31] [25]),
        .I1(\sg_a_p0_reg[27] [25]),
        .O(sg_xowb_carry__5_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__5_i_4
       (.I0(\sg_a_p0_reg[31] [24]),
        .I1(\sg_a_p0_reg[27] [24]),
        .O(sg_xowb_carry__5_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sg_xowb_carry__6
       (.CI(sg_xowb_carry__5_n_0),
        .CO({NLW_sg_xowb_carry__6_CO_UNCONNECTED[3],sg_xowb_carry__6_n_1,sg_xowb_carry__6_n_2,sg_xowb_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,\sg_a_p0_reg[31] [30:28]}),
        .O(xls[31:28]),
        .S(S));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry_i_1
       (.I0(\sg_a_p0_reg[31] [3]),
        .I1(\sg_a_p0_reg[27] [3]),
        .O(sg_xowb_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry_i_2
       (.I0(\sg_a_p0_reg[31] [2]),
        .I1(\sg_a_p0_reg[27] [2]),
        .O(sg_xowb_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry_i_3
       (.I0(\sg_a_p0_reg[31] [1]),
        .I1(\sg_a_p0_reg[27] [1]),
        .O(sg_xowb_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry_i_4
       (.I0(\sg_a_p0_reg[31] [0]),
        .I1(\sg_a_p0_reg[27] [0]),
        .O(sg_xowb_carry_i_4_n_0));
endmodule

module fer_soc_bd_tflite_mbqm_0_0_tflite_core1
   (sg_ab_64_p3_reg_0,
    S,
    \sg_ab_64_p3_reg[14]__0_0 ,
    \sg_ab_64_p3_reg[0]_0 ,
    s00_axi_aclk,
    Q,
    xls,
    \sg_a_p0_reg[31]_0 ,
    \sg_a_p0_reg[31]_1 );
  output [63:0]sg_ab_64_p3_reg_0;
  output [3:0]S;
  output [1:0]\sg_ab_64_p3_reg[14]__0_0 ;
  output [0:0]\sg_ab_64_p3_reg[0]_0 ;
  input s00_axi_aclk;
  input [31:0]Q;
  input [31:0]xls;
  input [3:0]\sg_a_p0_reg[31]_0 ;
  input [3:0]\sg_a_p0_reg[31]_1 ;

  wire [31:0]Q;
  wire [3:0]S;
  wire s00_axi_aclk;
  wire [31:17]sg_a_p0;
  wire [3:0]\sg_a_p0_reg[31]_0 ;
  wire [3:0]\sg_a_p0_reg[31]_1 ;
  wire [31:17]sg_a_p1;
  wire [31:17]sg_a_p2;
  wire sg_ab_64_p0_reg_n_100;
  wire sg_ab_64_p0_reg_n_101;
  wire sg_ab_64_p0_reg_n_102;
  wire sg_ab_64_p0_reg_n_103;
  wire sg_ab_64_p0_reg_n_104;
  wire sg_ab_64_p0_reg_n_105;
  wire sg_ab_64_p0_reg_n_106;
  wire sg_ab_64_p0_reg_n_107;
  wire sg_ab_64_p0_reg_n_108;
  wire sg_ab_64_p0_reg_n_109;
  wire sg_ab_64_p0_reg_n_110;
  wire sg_ab_64_p0_reg_n_111;
  wire sg_ab_64_p0_reg_n_112;
  wire sg_ab_64_p0_reg_n_113;
  wire sg_ab_64_p0_reg_n_114;
  wire sg_ab_64_p0_reg_n_115;
  wire sg_ab_64_p0_reg_n_116;
  wire sg_ab_64_p0_reg_n_117;
  wire sg_ab_64_p0_reg_n_118;
  wire sg_ab_64_p0_reg_n_119;
  wire sg_ab_64_p0_reg_n_120;
  wire sg_ab_64_p0_reg_n_121;
  wire sg_ab_64_p0_reg_n_122;
  wire sg_ab_64_p0_reg_n_123;
  wire sg_ab_64_p0_reg_n_124;
  wire sg_ab_64_p0_reg_n_125;
  wire sg_ab_64_p0_reg_n_126;
  wire sg_ab_64_p0_reg_n_127;
  wire sg_ab_64_p0_reg_n_128;
  wire sg_ab_64_p0_reg_n_129;
  wire sg_ab_64_p0_reg_n_130;
  wire sg_ab_64_p0_reg_n_131;
  wire sg_ab_64_p0_reg_n_132;
  wire sg_ab_64_p0_reg_n_133;
  wire sg_ab_64_p0_reg_n_134;
  wire sg_ab_64_p0_reg_n_135;
  wire sg_ab_64_p0_reg_n_136;
  wire sg_ab_64_p0_reg_n_137;
  wire sg_ab_64_p0_reg_n_138;
  wire sg_ab_64_p0_reg_n_139;
  wire sg_ab_64_p0_reg_n_140;
  wire sg_ab_64_p0_reg_n_141;
  wire sg_ab_64_p0_reg_n_142;
  wire sg_ab_64_p0_reg_n_143;
  wire sg_ab_64_p0_reg_n_144;
  wire sg_ab_64_p0_reg_n_145;
  wire sg_ab_64_p0_reg_n_146;
  wire sg_ab_64_p0_reg_n_147;
  wire sg_ab_64_p0_reg_n_148;
  wire sg_ab_64_p0_reg_n_149;
  wire sg_ab_64_p0_reg_n_150;
  wire sg_ab_64_p0_reg_n_151;
  wire sg_ab_64_p0_reg_n_152;
  wire sg_ab_64_p0_reg_n_153;
  wire sg_ab_64_p0_reg_n_24;
  wire sg_ab_64_p0_reg_n_25;
  wire sg_ab_64_p0_reg_n_26;
  wire sg_ab_64_p0_reg_n_27;
  wire sg_ab_64_p0_reg_n_28;
  wire sg_ab_64_p0_reg_n_29;
  wire sg_ab_64_p0_reg_n_30;
  wire sg_ab_64_p0_reg_n_31;
  wire sg_ab_64_p0_reg_n_32;
  wire sg_ab_64_p0_reg_n_33;
  wire sg_ab_64_p0_reg_n_34;
  wire sg_ab_64_p0_reg_n_35;
  wire sg_ab_64_p0_reg_n_36;
  wire sg_ab_64_p0_reg_n_37;
  wire sg_ab_64_p0_reg_n_38;
  wire sg_ab_64_p0_reg_n_39;
  wire sg_ab_64_p0_reg_n_40;
  wire sg_ab_64_p0_reg_n_41;
  wire sg_ab_64_p0_reg_n_42;
  wire sg_ab_64_p0_reg_n_43;
  wire sg_ab_64_p0_reg_n_44;
  wire sg_ab_64_p0_reg_n_45;
  wire sg_ab_64_p0_reg_n_46;
  wire sg_ab_64_p0_reg_n_47;
  wire sg_ab_64_p0_reg_n_48;
  wire sg_ab_64_p0_reg_n_49;
  wire sg_ab_64_p0_reg_n_50;
  wire sg_ab_64_p0_reg_n_51;
  wire sg_ab_64_p0_reg_n_52;
  wire sg_ab_64_p0_reg_n_53;
  wire sg_ab_64_p0_reg_n_58;
  wire sg_ab_64_p0_reg_n_59;
  wire sg_ab_64_p0_reg_n_60;
  wire sg_ab_64_p0_reg_n_61;
  wire sg_ab_64_p0_reg_n_62;
  wire sg_ab_64_p0_reg_n_63;
  wire sg_ab_64_p0_reg_n_64;
  wire sg_ab_64_p0_reg_n_65;
  wire sg_ab_64_p0_reg_n_66;
  wire sg_ab_64_p0_reg_n_67;
  wire sg_ab_64_p0_reg_n_68;
  wire sg_ab_64_p0_reg_n_69;
  wire sg_ab_64_p0_reg_n_70;
  wire sg_ab_64_p0_reg_n_71;
  wire sg_ab_64_p0_reg_n_72;
  wire sg_ab_64_p0_reg_n_73;
  wire sg_ab_64_p0_reg_n_74;
  wire sg_ab_64_p0_reg_n_75;
  wire sg_ab_64_p0_reg_n_76;
  wire sg_ab_64_p0_reg_n_77;
  wire sg_ab_64_p0_reg_n_78;
  wire sg_ab_64_p0_reg_n_79;
  wire sg_ab_64_p0_reg_n_80;
  wire sg_ab_64_p0_reg_n_81;
  wire sg_ab_64_p0_reg_n_82;
  wire sg_ab_64_p0_reg_n_83;
  wire sg_ab_64_p0_reg_n_84;
  wire sg_ab_64_p0_reg_n_85;
  wire sg_ab_64_p0_reg_n_86;
  wire sg_ab_64_p0_reg_n_87;
  wire sg_ab_64_p0_reg_n_88;
  wire sg_ab_64_p0_reg_n_89;
  wire sg_ab_64_p0_reg_n_90;
  wire sg_ab_64_p0_reg_n_91;
  wire sg_ab_64_p0_reg_n_92;
  wire sg_ab_64_p0_reg_n_93;
  wire sg_ab_64_p0_reg_n_94;
  wire sg_ab_64_p0_reg_n_95;
  wire sg_ab_64_p0_reg_n_96;
  wire sg_ab_64_p0_reg_n_97;
  wire sg_ab_64_p0_reg_n_98;
  wire sg_ab_64_p0_reg_n_99;
  wire sg_ab_64_p1_reg_n_106;
  wire sg_ab_64_p1_reg_n_107;
  wire sg_ab_64_p1_reg_n_108;
  wire sg_ab_64_p1_reg_n_109;
  wire sg_ab_64_p1_reg_n_110;
  wire sg_ab_64_p1_reg_n_111;
  wire sg_ab_64_p1_reg_n_112;
  wire sg_ab_64_p1_reg_n_113;
  wire sg_ab_64_p1_reg_n_114;
  wire sg_ab_64_p1_reg_n_115;
  wire sg_ab_64_p1_reg_n_116;
  wire sg_ab_64_p1_reg_n_117;
  wire sg_ab_64_p1_reg_n_118;
  wire sg_ab_64_p1_reg_n_119;
  wire sg_ab_64_p1_reg_n_120;
  wire sg_ab_64_p1_reg_n_121;
  wire sg_ab_64_p1_reg_n_122;
  wire sg_ab_64_p1_reg_n_123;
  wire sg_ab_64_p1_reg_n_124;
  wire sg_ab_64_p1_reg_n_125;
  wire sg_ab_64_p1_reg_n_126;
  wire sg_ab_64_p1_reg_n_127;
  wire sg_ab_64_p1_reg_n_128;
  wire sg_ab_64_p1_reg_n_129;
  wire sg_ab_64_p1_reg_n_130;
  wire sg_ab_64_p1_reg_n_131;
  wire sg_ab_64_p1_reg_n_132;
  wire sg_ab_64_p1_reg_n_133;
  wire sg_ab_64_p1_reg_n_134;
  wire sg_ab_64_p1_reg_n_135;
  wire sg_ab_64_p1_reg_n_136;
  wire sg_ab_64_p1_reg_n_137;
  wire sg_ab_64_p1_reg_n_138;
  wire sg_ab_64_p1_reg_n_139;
  wire sg_ab_64_p1_reg_n_140;
  wire sg_ab_64_p1_reg_n_141;
  wire sg_ab_64_p1_reg_n_142;
  wire sg_ab_64_p1_reg_n_143;
  wire sg_ab_64_p1_reg_n_144;
  wire sg_ab_64_p1_reg_n_145;
  wire sg_ab_64_p1_reg_n_146;
  wire sg_ab_64_p1_reg_n_147;
  wire sg_ab_64_p1_reg_n_148;
  wire sg_ab_64_p1_reg_n_149;
  wire sg_ab_64_p1_reg_n_150;
  wire sg_ab_64_p1_reg_n_151;
  wire sg_ab_64_p1_reg_n_152;
  wire sg_ab_64_p1_reg_n_153;
  wire \sg_ab_64_p2_reg[0]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[10]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[11]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[12]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[13]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[14]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[15]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[16]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[1]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[2]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[3]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[4]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[5]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[6]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[7]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[8]_srl2_n_0 ;
  wire \sg_ab_64_p2_reg[9]_srl2_n_0 ;
  wire sg_ab_64_p2_reg_n_100;
  wire sg_ab_64_p2_reg_n_101;
  wire sg_ab_64_p2_reg_n_102;
  wire sg_ab_64_p2_reg_n_103;
  wire sg_ab_64_p2_reg_n_104;
  wire sg_ab_64_p2_reg_n_105;
  wire sg_ab_64_p2_reg_n_106;
  wire sg_ab_64_p2_reg_n_107;
  wire sg_ab_64_p2_reg_n_108;
  wire sg_ab_64_p2_reg_n_109;
  wire sg_ab_64_p2_reg_n_110;
  wire sg_ab_64_p2_reg_n_111;
  wire sg_ab_64_p2_reg_n_112;
  wire sg_ab_64_p2_reg_n_113;
  wire sg_ab_64_p2_reg_n_114;
  wire sg_ab_64_p2_reg_n_115;
  wire sg_ab_64_p2_reg_n_116;
  wire sg_ab_64_p2_reg_n_117;
  wire sg_ab_64_p2_reg_n_118;
  wire sg_ab_64_p2_reg_n_119;
  wire sg_ab_64_p2_reg_n_120;
  wire sg_ab_64_p2_reg_n_121;
  wire sg_ab_64_p2_reg_n_122;
  wire sg_ab_64_p2_reg_n_123;
  wire sg_ab_64_p2_reg_n_124;
  wire sg_ab_64_p2_reg_n_125;
  wire sg_ab_64_p2_reg_n_126;
  wire sg_ab_64_p2_reg_n_127;
  wire sg_ab_64_p2_reg_n_128;
  wire sg_ab_64_p2_reg_n_129;
  wire sg_ab_64_p2_reg_n_130;
  wire sg_ab_64_p2_reg_n_131;
  wire sg_ab_64_p2_reg_n_132;
  wire sg_ab_64_p2_reg_n_133;
  wire sg_ab_64_p2_reg_n_134;
  wire sg_ab_64_p2_reg_n_135;
  wire sg_ab_64_p2_reg_n_136;
  wire sg_ab_64_p2_reg_n_137;
  wire sg_ab_64_p2_reg_n_138;
  wire sg_ab_64_p2_reg_n_139;
  wire sg_ab_64_p2_reg_n_140;
  wire sg_ab_64_p2_reg_n_141;
  wire sg_ab_64_p2_reg_n_142;
  wire sg_ab_64_p2_reg_n_143;
  wire sg_ab_64_p2_reg_n_144;
  wire sg_ab_64_p2_reg_n_145;
  wire sg_ab_64_p2_reg_n_146;
  wire sg_ab_64_p2_reg_n_147;
  wire sg_ab_64_p2_reg_n_148;
  wire sg_ab_64_p2_reg_n_149;
  wire sg_ab_64_p2_reg_n_150;
  wire sg_ab_64_p2_reg_n_151;
  wire sg_ab_64_p2_reg_n_152;
  wire sg_ab_64_p2_reg_n_153;
  wire sg_ab_64_p2_reg_n_58;
  wire sg_ab_64_p2_reg_n_59;
  wire sg_ab_64_p2_reg_n_60;
  wire sg_ab_64_p2_reg_n_61;
  wire sg_ab_64_p2_reg_n_62;
  wire sg_ab_64_p2_reg_n_63;
  wire sg_ab_64_p2_reg_n_64;
  wire sg_ab_64_p2_reg_n_65;
  wire sg_ab_64_p2_reg_n_66;
  wire sg_ab_64_p2_reg_n_67;
  wire sg_ab_64_p2_reg_n_68;
  wire sg_ab_64_p2_reg_n_69;
  wire sg_ab_64_p2_reg_n_70;
  wire sg_ab_64_p2_reg_n_71;
  wire sg_ab_64_p2_reg_n_72;
  wire sg_ab_64_p2_reg_n_73;
  wire sg_ab_64_p2_reg_n_74;
  wire sg_ab_64_p2_reg_n_75;
  wire sg_ab_64_p2_reg_n_76;
  wire sg_ab_64_p2_reg_n_77;
  wire sg_ab_64_p2_reg_n_78;
  wire sg_ab_64_p2_reg_n_79;
  wire sg_ab_64_p2_reg_n_80;
  wire sg_ab_64_p2_reg_n_81;
  wire sg_ab_64_p2_reg_n_82;
  wire sg_ab_64_p2_reg_n_83;
  wire sg_ab_64_p2_reg_n_84;
  wire sg_ab_64_p2_reg_n_85;
  wire sg_ab_64_p2_reg_n_86;
  wire sg_ab_64_p2_reg_n_87;
  wire sg_ab_64_p2_reg_n_88;
  wire sg_ab_64_p2_reg_n_89;
  wire sg_ab_64_p2_reg_n_90;
  wire sg_ab_64_p2_reg_n_91;
  wire sg_ab_64_p2_reg_n_92;
  wire sg_ab_64_p2_reg_n_93;
  wire sg_ab_64_p2_reg_n_94;
  wire sg_ab_64_p2_reg_n_95;
  wire sg_ab_64_p2_reg_n_96;
  wire sg_ab_64_p2_reg_n_97;
  wire sg_ab_64_p2_reg_n_98;
  wire sg_ab_64_p2_reg_n_99;
  wire [0:0]\sg_ab_64_p3_reg[0]_0 ;
  wire [1:0]\sg_ab_64_p3_reg[14]__0_0 ;
  wire [63:0]sg_ab_64_p3_reg_0;
  wire sg_ab_64_p3_reg_n_58;
  wire sg_ab_64_p3_reg_n_59;
  wire sg_ab_64_p3_reg_n_60;
  wire sg_ab_64_p3_reg_n_61;
  wire sg_ab_64_p3_reg_n_62;
  wire sg_ab_64_p3_reg_n_63;
  wire sg_ab_64_p3_reg_n_64;
  wire sg_ab_64_p3_reg_n_65;
  wire sg_ab_64_p3_reg_n_66;
  wire sg_ab_64_p3_reg_n_67;
  wire sg_ab_64_p3_reg_n_68;
  wire sg_ab_64_p3_reg_n_69;
  wire sg_ab_64_p3_reg_n_70;
  wire sg_ab_64_p3_reg_n_71;
  wire sg_ab_64_p3_reg_n_72;
  wire sg_ab_64_p3_reg_n_73;
  wire sg_ab_64_p3_reg_n_74;
  wire sg_ab_64_p3_reg_n_75;
  wire [31:0]sg_b_p0;
  wire [31:0]sg_b_p1;
  wire [31:17]sg_b_p2;
  wire [31:0]xls;
  wire NLW_sg_ab_64_p0_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_sg_ab_64_p0_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_sg_ab_64_p0_reg_OVERFLOW_UNCONNECTED;
  wire NLW_sg_ab_64_p0_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_sg_ab_64_p0_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_sg_ab_64_p0_reg_UNDERFLOW_UNCONNECTED;
  wire [17:0]NLW_sg_ab_64_p0_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_sg_ab_64_p0_reg_CARRYOUT_UNCONNECTED;
  wire NLW_sg_ab_64_p1_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_sg_ab_64_p1_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_sg_ab_64_p1_reg_OVERFLOW_UNCONNECTED;
  wire NLW_sg_ab_64_p1_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_sg_ab_64_p1_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_sg_ab_64_p1_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_sg_ab_64_p1_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_sg_ab_64_p1_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_sg_ab_64_p1_reg_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_sg_ab_64_p1_reg_P_UNCONNECTED;
  wire NLW_sg_ab_64_p2_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_sg_ab_64_p2_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_sg_ab_64_p2_reg_OVERFLOW_UNCONNECTED;
  wire NLW_sg_ab_64_p2_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_sg_ab_64_p2_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_sg_ab_64_p2_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_sg_ab_64_p2_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_sg_ab_64_p2_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_sg_ab_64_p2_reg_CARRYOUT_UNCONNECTED;
  wire NLW_sg_ab_64_p3_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_sg_ab_64_p3_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_sg_ab_64_p3_reg_OVERFLOW_UNCONNECTED;
  wire NLW_sg_ab_64_p3_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_sg_ab_64_p3_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_sg_ab_64_p3_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_sg_ab_64_p3_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_sg_ab_64_p3_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_sg_ab_64_p3_reg_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_sg_ab_64_p3_reg_PCOUT_UNCONNECTED;

  LUT2 #(
    .INIT(4'h6)) 
    ab_nudge_carry__6_i_1
       (.I0(sg_ab_64_p3_reg_0[31]),
        .I1(sg_ab_64_p3_reg_0[63]),
        .O(\sg_ab_64_p3_reg[14]__0_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    ab_nudge_carry__6_i_2
       (.I0(sg_ab_64_p3_reg_0[30]),
        .O(\sg_ab_64_p3_reg[14]__0_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    ab_nudge_carry_i_1
       (.I0(sg_ab_64_p3_reg_0[0]),
        .I1(sg_ab_64_p3_reg_0[63]),
        .O(\sg_ab_64_p3_reg[0]_0 ));
  FDRE \sg_a_p0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[17]),
        .Q(sg_a_p0[17]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[18]),
        .Q(sg_a_p0[18]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[19]),
        .Q(sg_a_p0[19]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[20]),
        .Q(sg_a_p0[20]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[21]),
        .Q(sg_a_p0[21]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[22]),
        .Q(sg_a_p0[22]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[23]),
        .Q(sg_a_p0[23]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[24]),
        .Q(sg_a_p0[24]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[25]),
        .Q(sg_a_p0[25]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[26]),
        .Q(sg_a_p0[26]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[27]),
        .Q(sg_a_p0[27]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[28]),
        .Q(sg_a_p0[28]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[29]),
        .Q(sg_a_p0[29]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[30]),
        .Q(sg_a_p0[30]),
        .R(1'b0));
  FDRE \sg_a_p0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(xls[31]),
        .Q(sg_a_p0[31]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[17]),
        .Q(sg_a_p1[17]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[18]),
        .Q(sg_a_p1[18]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[19]),
        .Q(sg_a_p1[19]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[20]),
        .Q(sg_a_p1[20]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[21]),
        .Q(sg_a_p1[21]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[22]),
        .Q(sg_a_p1[22]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[23]),
        .Q(sg_a_p1[23]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[24]),
        .Q(sg_a_p1[24]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[25]),
        .Q(sg_a_p1[25]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[26]),
        .Q(sg_a_p1[26]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[27]),
        .Q(sg_a_p1[27]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[28]),
        .Q(sg_a_p1[28]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[29]),
        .Q(sg_a_p1[29]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[30]),
        .Q(sg_a_p1[30]),
        .R(1'b0));
  FDRE \sg_a_p1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p0[31]),
        .Q(sg_a_p1[31]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[17]),
        .Q(sg_a_p2[17]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[18]),
        .Q(sg_a_p2[18]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[19]),
        .Q(sg_a_p2[19]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[20]),
        .Q(sg_a_p2[20]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[21]),
        .Q(sg_a_p2[21]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[22]),
        .Q(sg_a_p2[22]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[23]),
        .Q(sg_a_p2[23]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[24]),
        .Q(sg_a_p2[24]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[25]),
        .Q(sg_a_p2[25]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[26]),
        .Q(sg_a_p2[26]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[27]),
        .Q(sg_a_p2[27]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[28]),
        .Q(sg_a_p2[28]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[29]),
        .Q(sg_a_p2[29]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[30]),
        .Q(sg_a_p2[30]),
        .R(1'b0));
  FDRE \sg_a_p2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_a_p1[31]),
        .Q(sg_a_p2[31]),
        .R(1'b0));
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(1),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    sg_ab_64_p0_reg
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,xls[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT({sg_ab_64_p0_reg_n_24,sg_ab_64_p0_reg_n_25,sg_ab_64_p0_reg_n_26,sg_ab_64_p0_reg_n_27,sg_ab_64_p0_reg_n_28,sg_ab_64_p0_reg_n_29,sg_ab_64_p0_reg_n_30,sg_ab_64_p0_reg_n_31,sg_ab_64_p0_reg_n_32,sg_ab_64_p0_reg_n_33,sg_ab_64_p0_reg_n_34,sg_ab_64_p0_reg_n_35,sg_ab_64_p0_reg_n_36,sg_ab_64_p0_reg_n_37,sg_ab_64_p0_reg_n_38,sg_ab_64_p0_reg_n_39,sg_ab_64_p0_reg_n_40,sg_ab_64_p0_reg_n_41,sg_ab_64_p0_reg_n_42,sg_ab_64_p0_reg_n_43,sg_ab_64_p0_reg_n_44,sg_ab_64_p0_reg_n_45,sg_ab_64_p0_reg_n_46,sg_ab_64_p0_reg_n_47,sg_ab_64_p0_reg_n_48,sg_ab_64_p0_reg_n_49,sg_ab_64_p0_reg_n_50,sg_ab_64_p0_reg_n_51,sg_ab_64_p0_reg_n_52,sg_ab_64_p0_reg_n_53}),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,Q[16:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_sg_ab_64_p0_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_sg_ab_64_p0_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_sg_ab_64_p0_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b1),
        .CEP(1'b1),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_sg_ab_64_p0_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_sg_ab_64_p0_reg_OVERFLOW_UNCONNECTED),
        .P({sg_ab_64_p0_reg_n_58,sg_ab_64_p0_reg_n_59,sg_ab_64_p0_reg_n_60,sg_ab_64_p0_reg_n_61,sg_ab_64_p0_reg_n_62,sg_ab_64_p0_reg_n_63,sg_ab_64_p0_reg_n_64,sg_ab_64_p0_reg_n_65,sg_ab_64_p0_reg_n_66,sg_ab_64_p0_reg_n_67,sg_ab_64_p0_reg_n_68,sg_ab_64_p0_reg_n_69,sg_ab_64_p0_reg_n_70,sg_ab_64_p0_reg_n_71,sg_ab_64_p0_reg_n_72,sg_ab_64_p0_reg_n_73,sg_ab_64_p0_reg_n_74,sg_ab_64_p0_reg_n_75,sg_ab_64_p0_reg_n_76,sg_ab_64_p0_reg_n_77,sg_ab_64_p0_reg_n_78,sg_ab_64_p0_reg_n_79,sg_ab_64_p0_reg_n_80,sg_ab_64_p0_reg_n_81,sg_ab_64_p0_reg_n_82,sg_ab_64_p0_reg_n_83,sg_ab_64_p0_reg_n_84,sg_ab_64_p0_reg_n_85,sg_ab_64_p0_reg_n_86,sg_ab_64_p0_reg_n_87,sg_ab_64_p0_reg_n_88,sg_ab_64_p0_reg_n_89,sg_ab_64_p0_reg_n_90,sg_ab_64_p0_reg_n_91,sg_ab_64_p0_reg_n_92,sg_ab_64_p0_reg_n_93,sg_ab_64_p0_reg_n_94,sg_ab_64_p0_reg_n_95,sg_ab_64_p0_reg_n_96,sg_ab_64_p0_reg_n_97,sg_ab_64_p0_reg_n_98,sg_ab_64_p0_reg_n_99,sg_ab_64_p0_reg_n_100,sg_ab_64_p0_reg_n_101,sg_ab_64_p0_reg_n_102,sg_ab_64_p0_reg_n_103,sg_ab_64_p0_reg_n_104,sg_ab_64_p0_reg_n_105}),
        .PATTERNBDETECT(NLW_sg_ab_64_p0_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_sg_ab_64_p0_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({sg_ab_64_p0_reg_n_106,sg_ab_64_p0_reg_n_107,sg_ab_64_p0_reg_n_108,sg_ab_64_p0_reg_n_109,sg_ab_64_p0_reg_n_110,sg_ab_64_p0_reg_n_111,sg_ab_64_p0_reg_n_112,sg_ab_64_p0_reg_n_113,sg_ab_64_p0_reg_n_114,sg_ab_64_p0_reg_n_115,sg_ab_64_p0_reg_n_116,sg_ab_64_p0_reg_n_117,sg_ab_64_p0_reg_n_118,sg_ab_64_p0_reg_n_119,sg_ab_64_p0_reg_n_120,sg_ab_64_p0_reg_n_121,sg_ab_64_p0_reg_n_122,sg_ab_64_p0_reg_n_123,sg_ab_64_p0_reg_n_124,sg_ab_64_p0_reg_n_125,sg_ab_64_p0_reg_n_126,sg_ab_64_p0_reg_n_127,sg_ab_64_p0_reg_n_128,sg_ab_64_p0_reg_n_129,sg_ab_64_p0_reg_n_130,sg_ab_64_p0_reg_n_131,sg_ab_64_p0_reg_n_132,sg_ab_64_p0_reg_n_133,sg_ab_64_p0_reg_n_134,sg_ab_64_p0_reg_n_135,sg_ab_64_p0_reg_n_136,sg_ab_64_p0_reg_n_137,sg_ab_64_p0_reg_n_138,sg_ab_64_p0_reg_n_139,sg_ab_64_p0_reg_n_140,sg_ab_64_p0_reg_n_141,sg_ab_64_p0_reg_n_142,sg_ab_64_p0_reg_n_143,sg_ab_64_p0_reg_n_144,sg_ab_64_p0_reg_n_145,sg_ab_64_p0_reg_n_146,sg_ab_64_p0_reg_n_147,sg_ab_64_p0_reg_n_148,sg_ab_64_p0_reg_n_149,sg_ab_64_p0_reg_n_150,sg_ab_64_p0_reg_n_151,sg_ab_64_p0_reg_n_152,sg_ab_64_p0_reg_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_sg_ab_64_p0_reg_UNDERFLOW_UNCONNECTED));
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("CASCADE"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(1),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    sg_ab_64_p1_reg
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACIN({sg_ab_64_p0_reg_n_24,sg_ab_64_p0_reg_n_25,sg_ab_64_p0_reg_n_26,sg_ab_64_p0_reg_n_27,sg_ab_64_p0_reg_n_28,sg_ab_64_p0_reg_n_29,sg_ab_64_p0_reg_n_30,sg_ab_64_p0_reg_n_31,sg_ab_64_p0_reg_n_32,sg_ab_64_p0_reg_n_33,sg_ab_64_p0_reg_n_34,sg_ab_64_p0_reg_n_35,sg_ab_64_p0_reg_n_36,sg_ab_64_p0_reg_n_37,sg_ab_64_p0_reg_n_38,sg_ab_64_p0_reg_n_39,sg_ab_64_p0_reg_n_40,sg_ab_64_p0_reg_n_41,sg_ab_64_p0_reg_n_42,sg_ab_64_p0_reg_n_43,sg_ab_64_p0_reg_n_44,sg_ab_64_p0_reg_n_45,sg_ab_64_p0_reg_n_46,sg_ab_64_p0_reg_n_47,sg_ab_64_p0_reg_n_48,sg_ab_64_p0_reg_n_49,sg_ab_64_p0_reg_n_50,sg_ab_64_p0_reg_n_51,sg_ab_64_p0_reg_n_52,sg_ab_64_p0_reg_n_53}),
        .ACOUT(NLW_sg_ab_64_p1_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({sg_b_p0[31],sg_b_p0[31],sg_b_p0[31],sg_b_p0[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_sg_ab_64_p1_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_sg_ab_64_p1_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_sg_ab_64_p1_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b1),
        .CEP(1'b1),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_sg_ab_64_p1_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_sg_ab_64_p1_reg_OVERFLOW_UNCONNECTED),
        .P(NLW_sg_ab_64_p1_reg_P_UNCONNECTED[47:0]),
        .PATTERNBDETECT(NLW_sg_ab_64_p1_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_sg_ab_64_p1_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({sg_ab_64_p0_reg_n_106,sg_ab_64_p0_reg_n_107,sg_ab_64_p0_reg_n_108,sg_ab_64_p0_reg_n_109,sg_ab_64_p0_reg_n_110,sg_ab_64_p0_reg_n_111,sg_ab_64_p0_reg_n_112,sg_ab_64_p0_reg_n_113,sg_ab_64_p0_reg_n_114,sg_ab_64_p0_reg_n_115,sg_ab_64_p0_reg_n_116,sg_ab_64_p0_reg_n_117,sg_ab_64_p0_reg_n_118,sg_ab_64_p0_reg_n_119,sg_ab_64_p0_reg_n_120,sg_ab_64_p0_reg_n_121,sg_ab_64_p0_reg_n_122,sg_ab_64_p0_reg_n_123,sg_ab_64_p0_reg_n_124,sg_ab_64_p0_reg_n_125,sg_ab_64_p0_reg_n_126,sg_ab_64_p0_reg_n_127,sg_ab_64_p0_reg_n_128,sg_ab_64_p0_reg_n_129,sg_ab_64_p0_reg_n_130,sg_ab_64_p0_reg_n_131,sg_ab_64_p0_reg_n_132,sg_ab_64_p0_reg_n_133,sg_ab_64_p0_reg_n_134,sg_ab_64_p0_reg_n_135,sg_ab_64_p0_reg_n_136,sg_ab_64_p0_reg_n_137,sg_ab_64_p0_reg_n_138,sg_ab_64_p0_reg_n_139,sg_ab_64_p0_reg_n_140,sg_ab_64_p0_reg_n_141,sg_ab_64_p0_reg_n_142,sg_ab_64_p0_reg_n_143,sg_ab_64_p0_reg_n_144,sg_ab_64_p0_reg_n_145,sg_ab_64_p0_reg_n_146,sg_ab_64_p0_reg_n_147,sg_ab_64_p0_reg_n_148,sg_ab_64_p0_reg_n_149,sg_ab_64_p0_reg_n_150,sg_ab_64_p0_reg_n_151,sg_ab_64_p0_reg_n_152,sg_ab_64_p0_reg_n_153}),
        .PCOUT({sg_ab_64_p1_reg_n_106,sg_ab_64_p1_reg_n_107,sg_ab_64_p1_reg_n_108,sg_ab_64_p1_reg_n_109,sg_ab_64_p1_reg_n_110,sg_ab_64_p1_reg_n_111,sg_ab_64_p1_reg_n_112,sg_ab_64_p1_reg_n_113,sg_ab_64_p1_reg_n_114,sg_ab_64_p1_reg_n_115,sg_ab_64_p1_reg_n_116,sg_ab_64_p1_reg_n_117,sg_ab_64_p1_reg_n_118,sg_ab_64_p1_reg_n_119,sg_ab_64_p1_reg_n_120,sg_ab_64_p1_reg_n_121,sg_ab_64_p1_reg_n_122,sg_ab_64_p1_reg_n_123,sg_ab_64_p1_reg_n_124,sg_ab_64_p1_reg_n_125,sg_ab_64_p1_reg_n_126,sg_ab_64_p1_reg_n_127,sg_ab_64_p1_reg_n_128,sg_ab_64_p1_reg_n_129,sg_ab_64_p1_reg_n_130,sg_ab_64_p1_reg_n_131,sg_ab_64_p1_reg_n_132,sg_ab_64_p1_reg_n_133,sg_ab_64_p1_reg_n_134,sg_ab_64_p1_reg_n_135,sg_ab_64_p1_reg_n_136,sg_ab_64_p1_reg_n_137,sg_ab_64_p1_reg_n_138,sg_ab_64_p1_reg_n_139,sg_ab_64_p1_reg_n_140,sg_ab_64_p1_reg_n_141,sg_ab_64_p1_reg_n_142,sg_ab_64_p1_reg_n_143,sg_ab_64_p1_reg_n_144,sg_ab_64_p1_reg_n_145,sg_ab_64_p1_reg_n_146,sg_ab_64_p1_reg_n_147,sg_ab_64_p1_reg_n_148,sg_ab_64_p1_reg_n_149,sg_ab_64_p1_reg_n_150,sg_ab_64_p1_reg_n_151,sg_ab_64_p1_reg_n_152,sg_ab_64_p1_reg_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_sg_ab_64_p1_reg_UNDERFLOW_UNCONNECTED));
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(1),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    sg_ab_64_p2_reg
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,sg_b_p1[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_sg_ab_64_p2_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({sg_a_p1[31],sg_a_p1[31],sg_a_p1[31],sg_a_p1}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_sg_ab_64_p2_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_sg_ab_64_p2_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_sg_ab_64_p2_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b1),
        .CEP(1'b1),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_sg_ab_64_p2_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_sg_ab_64_p2_reg_OVERFLOW_UNCONNECTED),
        .P({sg_ab_64_p2_reg_n_58,sg_ab_64_p2_reg_n_59,sg_ab_64_p2_reg_n_60,sg_ab_64_p2_reg_n_61,sg_ab_64_p2_reg_n_62,sg_ab_64_p2_reg_n_63,sg_ab_64_p2_reg_n_64,sg_ab_64_p2_reg_n_65,sg_ab_64_p2_reg_n_66,sg_ab_64_p2_reg_n_67,sg_ab_64_p2_reg_n_68,sg_ab_64_p2_reg_n_69,sg_ab_64_p2_reg_n_70,sg_ab_64_p2_reg_n_71,sg_ab_64_p2_reg_n_72,sg_ab_64_p2_reg_n_73,sg_ab_64_p2_reg_n_74,sg_ab_64_p2_reg_n_75,sg_ab_64_p2_reg_n_76,sg_ab_64_p2_reg_n_77,sg_ab_64_p2_reg_n_78,sg_ab_64_p2_reg_n_79,sg_ab_64_p2_reg_n_80,sg_ab_64_p2_reg_n_81,sg_ab_64_p2_reg_n_82,sg_ab_64_p2_reg_n_83,sg_ab_64_p2_reg_n_84,sg_ab_64_p2_reg_n_85,sg_ab_64_p2_reg_n_86,sg_ab_64_p2_reg_n_87,sg_ab_64_p2_reg_n_88,sg_ab_64_p2_reg_n_89,sg_ab_64_p2_reg_n_90,sg_ab_64_p2_reg_n_91,sg_ab_64_p2_reg_n_92,sg_ab_64_p2_reg_n_93,sg_ab_64_p2_reg_n_94,sg_ab_64_p2_reg_n_95,sg_ab_64_p2_reg_n_96,sg_ab_64_p2_reg_n_97,sg_ab_64_p2_reg_n_98,sg_ab_64_p2_reg_n_99,sg_ab_64_p2_reg_n_100,sg_ab_64_p2_reg_n_101,sg_ab_64_p2_reg_n_102,sg_ab_64_p2_reg_n_103,sg_ab_64_p2_reg_n_104,sg_ab_64_p2_reg_n_105}),
        .PATTERNBDETECT(NLW_sg_ab_64_p2_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_sg_ab_64_p2_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({sg_ab_64_p1_reg_n_106,sg_ab_64_p1_reg_n_107,sg_ab_64_p1_reg_n_108,sg_ab_64_p1_reg_n_109,sg_ab_64_p1_reg_n_110,sg_ab_64_p1_reg_n_111,sg_ab_64_p1_reg_n_112,sg_ab_64_p1_reg_n_113,sg_ab_64_p1_reg_n_114,sg_ab_64_p1_reg_n_115,sg_ab_64_p1_reg_n_116,sg_ab_64_p1_reg_n_117,sg_ab_64_p1_reg_n_118,sg_ab_64_p1_reg_n_119,sg_ab_64_p1_reg_n_120,sg_ab_64_p1_reg_n_121,sg_ab_64_p1_reg_n_122,sg_ab_64_p1_reg_n_123,sg_ab_64_p1_reg_n_124,sg_ab_64_p1_reg_n_125,sg_ab_64_p1_reg_n_126,sg_ab_64_p1_reg_n_127,sg_ab_64_p1_reg_n_128,sg_ab_64_p1_reg_n_129,sg_ab_64_p1_reg_n_130,sg_ab_64_p1_reg_n_131,sg_ab_64_p1_reg_n_132,sg_ab_64_p1_reg_n_133,sg_ab_64_p1_reg_n_134,sg_ab_64_p1_reg_n_135,sg_ab_64_p1_reg_n_136,sg_ab_64_p1_reg_n_137,sg_ab_64_p1_reg_n_138,sg_ab_64_p1_reg_n_139,sg_ab_64_p1_reg_n_140,sg_ab_64_p1_reg_n_141,sg_ab_64_p1_reg_n_142,sg_ab_64_p1_reg_n_143,sg_ab_64_p1_reg_n_144,sg_ab_64_p1_reg_n_145,sg_ab_64_p1_reg_n_146,sg_ab_64_p1_reg_n_147,sg_ab_64_p1_reg_n_148,sg_ab_64_p1_reg_n_149,sg_ab_64_p1_reg_n_150,sg_ab_64_p1_reg_n_151,sg_ab_64_p1_reg_n_152,sg_ab_64_p1_reg_n_153}),
        .PCOUT({sg_ab_64_p2_reg_n_106,sg_ab_64_p2_reg_n_107,sg_ab_64_p2_reg_n_108,sg_ab_64_p2_reg_n_109,sg_ab_64_p2_reg_n_110,sg_ab_64_p2_reg_n_111,sg_ab_64_p2_reg_n_112,sg_ab_64_p2_reg_n_113,sg_ab_64_p2_reg_n_114,sg_ab_64_p2_reg_n_115,sg_ab_64_p2_reg_n_116,sg_ab_64_p2_reg_n_117,sg_ab_64_p2_reg_n_118,sg_ab_64_p2_reg_n_119,sg_ab_64_p2_reg_n_120,sg_ab_64_p2_reg_n_121,sg_ab_64_p2_reg_n_122,sg_ab_64_p2_reg_n_123,sg_ab_64_p2_reg_n_124,sg_ab_64_p2_reg_n_125,sg_ab_64_p2_reg_n_126,sg_ab_64_p2_reg_n_127,sg_ab_64_p2_reg_n_128,sg_ab_64_p2_reg_n_129,sg_ab_64_p2_reg_n_130,sg_ab_64_p2_reg_n_131,sg_ab_64_p2_reg_n_132,sg_ab_64_p2_reg_n_133,sg_ab_64_p2_reg_n_134,sg_ab_64_p2_reg_n_135,sg_ab_64_p2_reg_n_136,sg_ab_64_p2_reg_n_137,sg_ab_64_p2_reg_n_138,sg_ab_64_p2_reg_n_139,sg_ab_64_p2_reg_n_140,sg_ab_64_p2_reg_n_141,sg_ab_64_p2_reg_n_142,sg_ab_64_p2_reg_n_143,sg_ab_64_p2_reg_n_144,sg_ab_64_p2_reg_n_145,sg_ab_64_p2_reg_n_146,sg_ab_64_p2_reg_n_147,sg_ab_64_p2_reg_n_148,sg_ab_64_p2_reg_n_149,sg_ab_64_p2_reg_n_150,sg_ab_64_p2_reg_n_151,sg_ab_64_p2_reg_n_152,sg_ab_64_p2_reg_n_153}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_sg_ab_64_p2_reg_UNDERFLOW_UNCONNECTED));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[0]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[0]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_105),
        .Q(\sg_ab_64_p2_reg[0]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[10]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[10]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_95),
        .Q(\sg_ab_64_p2_reg[10]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[11]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[11]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_94),
        .Q(\sg_ab_64_p2_reg[11]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[12]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[12]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_93),
        .Q(\sg_ab_64_p2_reg[12]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[13]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[13]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_92),
        .Q(\sg_ab_64_p2_reg[13]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[14]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[14]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_91),
        .Q(\sg_ab_64_p2_reg[14]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[15]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[15]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_90),
        .Q(\sg_ab_64_p2_reg[15]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[16]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[16]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_89),
        .Q(\sg_ab_64_p2_reg[16]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[1]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[1]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_104),
        .Q(\sg_ab_64_p2_reg[1]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[2]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[2]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_103),
        .Q(\sg_ab_64_p2_reg[2]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[3]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[3]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_102),
        .Q(\sg_ab_64_p2_reg[3]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[4]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[4]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_101),
        .Q(\sg_ab_64_p2_reg[4]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[5]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[5]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_100),
        .Q(\sg_ab_64_p2_reg[5]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[6]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[6]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_99),
        .Q(\sg_ab_64_p2_reg[6]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[7]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[7]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_98),
        .Q(\sg_ab_64_p2_reg[7]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[8]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[8]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_97),
        .Q(\sg_ab_64_p2_reg[8]_srl2_n_0 ));
  (* srl_bus_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg " *) 
  (* srl_name = "\U0/tflite_mbqm_v1_0_S00_AXI_inst/SM0/SM1/sg_ab_64_p2_reg[9]_srl2 " *) 
  SRL16E \sg_ab_64_p2_reg[9]_srl2 
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(s00_axi_aclk),
        .D(sg_ab_64_p0_reg_n_96),
        .Q(\sg_ab_64_p2_reg[9]_srl2_n_0 ));
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(1),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    sg_ab_64_p3_reg
       (.A({sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2[31],sg_a_p2}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_sg_ab_64_p3_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({sg_b_p2[31],sg_b_p2[31],sg_b_p2[31],sg_b_p2}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_sg_ab_64_p3_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_sg_ab_64_p3_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_sg_ab_64_p3_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b1),
        .CEP(1'b1),
        .CLK(s00_axi_aclk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_sg_ab_64_p3_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_sg_ab_64_p3_reg_OVERFLOW_UNCONNECTED),
        .P({sg_ab_64_p3_reg_n_58,sg_ab_64_p3_reg_n_59,sg_ab_64_p3_reg_n_60,sg_ab_64_p3_reg_n_61,sg_ab_64_p3_reg_n_62,sg_ab_64_p3_reg_n_63,sg_ab_64_p3_reg_n_64,sg_ab_64_p3_reg_n_65,sg_ab_64_p3_reg_n_66,sg_ab_64_p3_reg_n_67,sg_ab_64_p3_reg_n_68,sg_ab_64_p3_reg_n_69,sg_ab_64_p3_reg_n_70,sg_ab_64_p3_reg_n_71,sg_ab_64_p3_reg_n_72,sg_ab_64_p3_reg_n_73,sg_ab_64_p3_reg_n_74,sg_ab_64_p3_reg_n_75,sg_ab_64_p3_reg_0[63:34]}),
        .PATTERNBDETECT(NLW_sg_ab_64_p3_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_sg_ab_64_p3_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({sg_ab_64_p2_reg_n_106,sg_ab_64_p2_reg_n_107,sg_ab_64_p2_reg_n_108,sg_ab_64_p2_reg_n_109,sg_ab_64_p2_reg_n_110,sg_ab_64_p2_reg_n_111,sg_ab_64_p2_reg_n_112,sg_ab_64_p2_reg_n_113,sg_ab_64_p2_reg_n_114,sg_ab_64_p2_reg_n_115,sg_ab_64_p2_reg_n_116,sg_ab_64_p2_reg_n_117,sg_ab_64_p2_reg_n_118,sg_ab_64_p2_reg_n_119,sg_ab_64_p2_reg_n_120,sg_ab_64_p2_reg_n_121,sg_ab_64_p2_reg_n_122,sg_ab_64_p2_reg_n_123,sg_ab_64_p2_reg_n_124,sg_ab_64_p2_reg_n_125,sg_ab_64_p2_reg_n_126,sg_ab_64_p2_reg_n_127,sg_ab_64_p2_reg_n_128,sg_ab_64_p2_reg_n_129,sg_ab_64_p2_reg_n_130,sg_ab_64_p2_reg_n_131,sg_ab_64_p2_reg_n_132,sg_ab_64_p2_reg_n_133,sg_ab_64_p2_reg_n_134,sg_ab_64_p2_reg_n_135,sg_ab_64_p2_reg_n_136,sg_ab_64_p2_reg_n_137,sg_ab_64_p2_reg_n_138,sg_ab_64_p2_reg_n_139,sg_ab_64_p2_reg_n_140,sg_ab_64_p2_reg_n_141,sg_ab_64_p2_reg_n_142,sg_ab_64_p2_reg_n_143,sg_ab_64_p2_reg_n_144,sg_ab_64_p2_reg_n_145,sg_ab_64_p2_reg_n_146,sg_ab_64_p2_reg_n_147,sg_ab_64_p2_reg_n_148,sg_ab_64_p2_reg_n_149,sg_ab_64_p2_reg_n_150,sg_ab_64_p2_reg_n_151,sg_ab_64_p2_reg_n_152,sg_ab_64_p2_reg_n_153}),
        .PCOUT(NLW_sg_ab_64_p3_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_sg_ab_64_p3_reg_UNDERFLOW_UNCONNECTED));
  FDRE \sg_ab_64_p3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[0]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[0]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[0]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_105),
        .Q(sg_ab_64_p3_reg_0[17]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[10]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[10]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[10]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_95),
        .Q(sg_ab_64_p3_reg_0[27]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[11]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[11]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[11]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_94),
        .Q(sg_ab_64_p3_reg_0[28]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[12]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[12]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[12]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_93),
        .Q(sg_ab_64_p3_reg_0[29]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[13]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[13]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[13]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_92),
        .Q(sg_ab_64_p3_reg_0[30]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[14]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[14]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[14]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_91),
        .Q(sg_ab_64_p3_reg_0[31]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[15]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[15]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[15]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_90),
        .Q(sg_ab_64_p3_reg_0[32]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[16]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[16]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[16]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_89),
        .Q(sg_ab_64_p3_reg_0[33]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[1]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[1]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[1]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_104),
        .Q(sg_ab_64_p3_reg_0[18]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[2]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[2]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[2]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_103),
        .Q(sg_ab_64_p3_reg_0[19]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[3]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[3]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[3]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_102),
        .Q(sg_ab_64_p3_reg_0[20]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[4]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[4]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[4]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_101),
        .Q(sg_ab_64_p3_reg_0[21]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[5]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[5]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[5]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_100),
        .Q(sg_ab_64_p3_reg_0[22]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[6]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[6]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[6]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_99),
        .Q(sg_ab_64_p3_reg_0[23]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[7]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[7]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[7]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_98),
        .Q(sg_ab_64_p3_reg_0[24]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[8]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[8]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[8]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_97),
        .Q(sg_ab_64_p3_reg_0[25]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\sg_ab_64_p2_reg[9]_srl2_n_0 ),
        .Q(sg_ab_64_p3_reg_0[9]),
        .R(1'b0));
  FDRE \sg_ab_64_p3_reg[9]__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_ab_64_p2_reg_n_96),
        .Q(sg_ab_64_p3_reg_0[26]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[0]),
        .Q(sg_b_p0[0]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[10]),
        .Q(sg_b_p0[10]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[11]),
        .Q(sg_b_p0[11]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[12]),
        .Q(sg_b_p0[12]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[13]),
        .Q(sg_b_p0[13]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[14]),
        .Q(sg_b_p0[14]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[15]),
        .Q(sg_b_p0[15]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[16]),
        .Q(sg_b_p0[16]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[17]),
        .Q(sg_b_p0[17]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[18]),
        .Q(sg_b_p0[18]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[19]),
        .Q(sg_b_p0[19]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[1]),
        .Q(sg_b_p0[1]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[20]),
        .Q(sg_b_p0[20]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[21]),
        .Q(sg_b_p0[21]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[22]),
        .Q(sg_b_p0[22]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[23]),
        .Q(sg_b_p0[23]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[24]),
        .Q(sg_b_p0[24]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[25]),
        .Q(sg_b_p0[25]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[26]),
        .Q(sg_b_p0[26]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[27]),
        .Q(sg_b_p0[27]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[28]),
        .Q(sg_b_p0[28]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[29]),
        .Q(sg_b_p0[29]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[2]),
        .Q(sg_b_p0[2]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[30]),
        .Q(sg_b_p0[30]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[31]),
        .Q(sg_b_p0[31]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[3]),
        .Q(sg_b_p0[3]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[4]),
        .Q(sg_b_p0[4]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[5]),
        .Q(sg_b_p0[5]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[6]),
        .Q(sg_b_p0[6]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[7]),
        .Q(sg_b_p0[7]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[8]),
        .Q(sg_b_p0[8]),
        .R(1'b0));
  FDRE \sg_b_p0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[9]),
        .Q(sg_b_p0[9]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[0]),
        .Q(sg_b_p1[0]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[10]),
        .Q(sg_b_p1[10]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[11]),
        .Q(sg_b_p1[11]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[12]),
        .Q(sg_b_p1[12]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[13]),
        .Q(sg_b_p1[13]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[14]),
        .Q(sg_b_p1[14]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[15]),
        .Q(sg_b_p1[15]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[16]),
        .Q(sg_b_p1[16]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[17]),
        .Q(sg_b_p1[17]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[18]),
        .Q(sg_b_p1[18]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[19]),
        .Q(sg_b_p1[19]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[1]),
        .Q(sg_b_p1[1]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[20]),
        .Q(sg_b_p1[20]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[21]),
        .Q(sg_b_p1[21]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[22]),
        .Q(sg_b_p1[22]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[23]),
        .Q(sg_b_p1[23]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[24]),
        .Q(sg_b_p1[24]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[25]),
        .Q(sg_b_p1[25]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[26]),
        .Q(sg_b_p1[26]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[27]),
        .Q(sg_b_p1[27]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[28]),
        .Q(sg_b_p1[28]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[29]),
        .Q(sg_b_p1[29]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[2]),
        .Q(sg_b_p1[2]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[30]),
        .Q(sg_b_p1[30]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[31]),
        .Q(sg_b_p1[31]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[3]),
        .Q(sg_b_p1[3]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[4]),
        .Q(sg_b_p1[4]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[5]),
        .Q(sg_b_p1[5]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[6]),
        .Q(sg_b_p1[6]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[7]),
        .Q(sg_b_p1[7]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[8]),
        .Q(sg_b_p1[8]),
        .R(1'b0));
  FDRE \sg_b_p1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p0[9]),
        .Q(sg_b_p1[9]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[17]),
        .Q(sg_b_p2[17]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[18]),
        .Q(sg_b_p2[18]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[19]),
        .Q(sg_b_p2[19]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[20]),
        .Q(sg_b_p2[20]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[21]),
        .Q(sg_b_p2[21]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[22]),
        .Q(sg_b_p2[22]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[23]),
        .Q(sg_b_p2[23]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[24]),
        .Q(sg_b_p2[24]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[25]),
        .Q(sg_b_p2[25]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[26]),
        .Q(sg_b_p2[26]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[27]),
        .Q(sg_b_p2[27]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[28]),
        .Q(sg_b_p2[28]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[29]),
        .Q(sg_b_p2[29]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[30]),
        .Q(sg_b_p2[30]),
        .R(1'b0));
  FDRE \sg_b_p2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sg_b_p1[31]),
        .Q(sg_b_p2[31]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__6_i_1
       (.I0(\sg_a_p0_reg[31]_0 [3]),
        .I1(\sg_a_p0_reg[31]_1 [3]),
        .O(S[3]));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__6_i_2
       (.I0(\sg_a_p0_reg[31]_1 [2]),
        .I1(\sg_a_p0_reg[31]_0 [2]),
        .O(S[2]));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__6_i_3
       (.I0(\sg_a_p0_reg[31]_1 [1]),
        .I1(\sg_a_p0_reg[31]_0 [1]),
        .O(S[1]));
  LUT2 #(
    .INIT(4'h6)) 
    sg_xowb_carry__6_i_4
       (.I0(\sg_a_p0_reg[31]_1 [0]),
        .I1(\sg_a_p0_reg[31]_0 [0]),
        .O(S[0]));
endmodule

module fer_soc_bd_tflite_mbqm_0_0_tflite_core2
   (ab_nudge,
    \slv_reg4_reg[7] ,
    \sg_ab_64_p3_reg[14]__0 ,
    \slv_reg4_reg[7]_0 ,
    \slv_reg4_reg[7]_1 ,
    \slv_reg4_reg[7]_2 ,
    \slv_reg4_reg[7]_3 ,
    \slv_reg4_reg[7]_4 ,
    \slv_reg4_reg[7]_5 ,
    \slv_reg4_reg[5] ,
    sg_ab_64_p3_reg,
    sg_ab_64_p3_reg_0,
    sg_ab_64_p3_reg_1,
    sg_ab_64_p3_reg_2,
    sg_ab_64_p3_reg_3,
    sg_ab_64_p3_reg_4,
    \slv_reg4_reg[7]_6 ,
    \slv_reg4_reg[7]_7 ,
    \slv_reg4_reg[7]_8 ,
    \slv_reg4_reg[7]_9 ,
    \slv_reg4_reg[7]_10 ,
    \slv_reg4_reg[7]_11 ,
    \slv_reg4_reg[7]_12 ,
    \slv_reg4_reg[7]_13 ,
    \slv_reg4_reg[7]_14 ,
    \slv_reg4_reg[7]_15 ,
    \slv_reg4_reg[7]_16 ,
    \slv_reg4_reg[7]_17 ,
    \slv_reg4_reg[7]_18 ,
    \slv_reg4_reg[7]_19 ,
    \axi_rdata[19]_i_7_0 ,
    \axi_rdata[19]_i_7_1 ,
    \axi_rdata[15]_i_21 ,
    ab_nudge_carry__0_0,
    \axi_rdata[3]_i_19 ,
    \axi_rdata[7]_i_4 ,
    O,
    \axi_rdata[7]_i_7 ,
    \axi_rdata[3]_i_4 ,
    \axi_rdata_reg[19]_i_2_0 ,
    \axi_rdata_reg[11]_i_2 ,
    \axi_rdata_reg[11]_i_2_0 ,
    \axi_rdata_reg[11]_i_2_1 ,
    \axi_rdata[15]_i_7_0 ,
    \axi_rdata[19]_i_6_0 ,
    \axi_rdata[19]_i_6_1 ,
    \axi_rdata[11]_i_5_0 ,
    \axi_rdata_reg[19] ,
    \axi_rdata[15]_i_6_0 ,
    \axi_rdata[11]_i_4_0 ,
    \axi_rdata[19]_i_7_2 ,
    \axi_rdata_reg[19]_i_2_1 ,
    \axi_rdata[19]_i_6_2 ,
    \axi_rdata[23]_i_11 ,
    \axi_rdata[23]_i_11_0 ,
    \axi_rdata[23]_i_11_1 ,
    \axi_rdata[31]_i_5 ,
    \axi_rdata[15]_i_7_1 ,
    \axi_rdata[19]_i_7_3 ,
    \axi_rdata[23]_i_7 ,
    \axi_rdata[27]_i_7 ,
    \axi_rdata[31]_i_8 ,
    \axi_rdata[31]_i_7 ,
    \axi_rdata_reg[15] );
  output [31:0]ab_nudge;
  output \slv_reg4_reg[7] ;
  output \sg_ab_64_p3_reg[14]__0 ;
  output \slv_reg4_reg[7]_0 ;
  output \slv_reg4_reg[7]_1 ;
  output \slv_reg4_reg[7]_2 ;
  output \slv_reg4_reg[7]_3 ;
  output \slv_reg4_reg[7]_4 ;
  output \slv_reg4_reg[7]_5 ;
  output [1:0]\slv_reg4_reg[5] ;
  output sg_ab_64_p3_reg;
  output sg_ab_64_p3_reg_0;
  output sg_ab_64_p3_reg_1;
  output sg_ab_64_p3_reg_2;
  output sg_ab_64_p3_reg_3;
  output sg_ab_64_p3_reg_4;
  output \slv_reg4_reg[7]_6 ;
  output \slv_reg4_reg[7]_7 ;
  output \slv_reg4_reg[7]_8 ;
  output \slv_reg4_reg[7]_9 ;
  output \slv_reg4_reg[7]_10 ;
  output \slv_reg4_reg[7]_11 ;
  output \slv_reg4_reg[7]_12 ;
  output \slv_reg4_reg[7]_13 ;
  output \slv_reg4_reg[7]_14 ;
  output \slv_reg4_reg[7]_15 ;
  output \slv_reg4_reg[7]_16 ;
  output \slv_reg4_reg[7]_17 ;
  output \slv_reg4_reg[7]_18 ;
  output \slv_reg4_reg[7]_19 ;
  output [7:0]\axi_rdata[19]_i_7_0 ;
  output [0:0]\axi_rdata[19]_i_7_1 ;
  input [63:0]\axi_rdata[15]_i_21 ;
  input [0:0]ab_nudge_carry__0_0;
  input [1:0]\axi_rdata[3]_i_19 ;
  input \axi_rdata[7]_i_4 ;
  input [3:0]O;
  input [3:0]\axi_rdata[7]_i_7 ;
  input [0:0]\axi_rdata[3]_i_4 ;
  input \axi_rdata_reg[19]_i_2_0 ;
  input \axi_rdata_reg[11]_i_2 ;
  input \axi_rdata_reg[11]_i_2_0 ;
  input \axi_rdata_reg[11]_i_2_1 ;
  input \axi_rdata[15]_i_7_0 ;
  input \axi_rdata[19]_i_6_0 ;
  input \axi_rdata[19]_i_6_1 ;
  input \axi_rdata[11]_i_5_0 ;
  input [0:0]\axi_rdata_reg[19] ;
  input \axi_rdata[15]_i_6_0 ;
  input \axi_rdata[11]_i_4_0 ;
  input \axi_rdata[19]_i_7_2 ;
  input \axi_rdata_reg[19]_i_2_1 ;
  input \axi_rdata[19]_i_6_2 ;
  input \axi_rdata[23]_i_11 ;
  input \axi_rdata[23]_i_11_0 ;
  input \axi_rdata[23]_i_11_1 ;
  input \axi_rdata[31]_i_5 ;
  input [3:0]\axi_rdata[15]_i_7_1 ;
  input [3:0]\axi_rdata[19]_i_7_3 ;
  input [3:0]\axi_rdata[23]_i_7 ;
  input [3:0]\axi_rdata[27]_i_7 ;
  input [3:0]\axi_rdata[31]_i_8 ;
  input [0:0]\axi_rdata[31]_i_7 ;
  input [0:0]\axi_rdata_reg[15] ;

  wire [3:0]O;
  wire [18:12]SHIFT_RIGHT;
  wire [31:0]ab_nudge;
  wire [0:0]ab_nudge_carry__0_0;
  wire ab_nudge_carry__0_n_0;
  wire ab_nudge_carry__0_n_1;
  wire ab_nudge_carry__0_n_2;
  wire ab_nudge_carry__0_n_3;
  wire ab_nudge_carry__10_n_0;
  wire ab_nudge_carry__10_n_1;
  wire ab_nudge_carry__10_n_2;
  wire ab_nudge_carry__10_n_3;
  wire ab_nudge_carry__11_n_0;
  wire ab_nudge_carry__11_n_1;
  wire ab_nudge_carry__11_n_2;
  wire ab_nudge_carry__11_n_3;
  wire ab_nudge_carry__12_n_0;
  wire ab_nudge_carry__12_n_1;
  wire ab_nudge_carry__12_n_2;
  wire ab_nudge_carry__12_n_3;
  wire ab_nudge_carry__13_n_0;
  wire ab_nudge_carry__13_n_1;
  wire ab_nudge_carry__13_n_2;
  wire ab_nudge_carry__13_n_3;
  wire ab_nudge_carry__14_n_1;
  wire ab_nudge_carry__14_n_2;
  wire ab_nudge_carry__14_n_3;
  wire ab_nudge_carry__1_n_0;
  wire ab_nudge_carry__1_n_1;
  wire ab_nudge_carry__1_n_2;
  wire ab_nudge_carry__1_n_3;
  wire ab_nudge_carry__2_n_0;
  wire ab_nudge_carry__2_n_1;
  wire ab_nudge_carry__2_n_2;
  wire ab_nudge_carry__2_n_3;
  wire ab_nudge_carry__3_n_0;
  wire ab_nudge_carry__3_n_1;
  wire ab_nudge_carry__3_n_2;
  wire ab_nudge_carry__3_n_3;
  wire ab_nudge_carry__4_n_0;
  wire ab_nudge_carry__4_n_1;
  wire ab_nudge_carry__4_n_2;
  wire ab_nudge_carry__4_n_3;
  wire ab_nudge_carry__5_n_0;
  wire ab_nudge_carry__5_n_1;
  wire ab_nudge_carry__5_n_2;
  wire ab_nudge_carry__5_n_3;
  wire ab_nudge_carry__6_n_0;
  wire ab_nudge_carry__6_n_1;
  wire ab_nudge_carry__6_n_2;
  wire ab_nudge_carry__6_n_3;
  wire ab_nudge_carry__7_n_0;
  wire ab_nudge_carry__7_n_1;
  wire ab_nudge_carry__7_n_2;
  wire ab_nudge_carry__7_n_3;
  wire ab_nudge_carry__8_n_0;
  wire ab_nudge_carry__8_n_1;
  wire ab_nudge_carry__8_n_2;
  wire ab_nudge_carry__8_n_3;
  wire ab_nudge_carry__9_n_0;
  wire ab_nudge_carry__9_n_1;
  wire ab_nudge_carry__9_n_2;
  wire ab_nudge_carry__9_n_3;
  wire ab_nudge_carry_n_0;
  wire ab_nudge_carry_n_1;
  wire ab_nudge_carry_n_2;
  wire ab_nudge_carry_n_3;
  wire \axi_rdata[11]_i_10_n_0 ;
  wire \axi_rdata[11]_i_4_0 ;
  wire \axi_rdata[11]_i_5_0 ;
  wire \axi_rdata[11]_i_8_n_0 ;
  wire \axi_rdata[11]_i_9_n_0 ;
  wire \axi_rdata[15]_i_10_n_0 ;
  wire \axi_rdata[15]_i_11_n_0 ;
  wire \axi_rdata[15]_i_12_n_0 ;
  wire \axi_rdata[15]_i_13_n_0 ;
  wire \axi_rdata[15]_i_14_n_0 ;
  wire \axi_rdata[15]_i_15_n_0 ;
  wire [63:0]\axi_rdata[15]_i_21 ;
  wire \axi_rdata[15]_i_6_0 ;
  wire \axi_rdata[15]_i_7_0 ;
  wire [3:0]\axi_rdata[15]_i_7_1 ;
  wire \axi_rdata[15]_i_8_n_0 ;
  wire \axi_rdata[15]_i_9_n_0 ;
  wire \axi_rdata[19]_i_10_n_0 ;
  wire \axi_rdata[19]_i_11_n_0 ;
  wire \axi_rdata[19]_i_12_n_0 ;
  wire \axi_rdata[19]_i_13_n_0 ;
  wire \axi_rdata[19]_i_14_n_0 ;
  wire \axi_rdata[19]_i_15_n_0 ;
  wire \axi_rdata[19]_i_17_n_0 ;
  wire \axi_rdata[19]_i_18_n_0 ;
  wire \axi_rdata[19]_i_6_0 ;
  wire \axi_rdata[19]_i_6_1 ;
  wire \axi_rdata[19]_i_6_2 ;
  wire [7:0]\axi_rdata[19]_i_7_0 ;
  wire [0:0]\axi_rdata[19]_i_7_1 ;
  wire \axi_rdata[19]_i_7_2 ;
  wire [3:0]\axi_rdata[19]_i_7_3 ;
  wire \axi_rdata[23]_i_11 ;
  wire \axi_rdata[23]_i_11_0 ;
  wire \axi_rdata[23]_i_11_1 ;
  wire [3:0]\axi_rdata[23]_i_7 ;
  wire [3:0]\axi_rdata[27]_i_7 ;
  wire \axi_rdata[31]_i_5 ;
  wire [0:0]\axi_rdata[31]_i_7 ;
  wire [3:0]\axi_rdata[31]_i_8 ;
  wire [1:0]\axi_rdata[3]_i_19 ;
  wire [0:0]\axi_rdata[3]_i_4 ;
  wire \axi_rdata[7]_i_4 ;
  wire [3:0]\axi_rdata[7]_i_7 ;
  wire \axi_rdata_reg[11]_i_2 ;
  wire \axi_rdata_reg[11]_i_2_0 ;
  wire \axi_rdata_reg[11]_i_2_1 ;
  wire [0:0]\axi_rdata_reg[15] ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_1 ;
  wire \axi_rdata_reg[15]_i_2_n_2 ;
  wire \axi_rdata_reg[15]_i_2_n_3 ;
  wire [0:0]\axi_rdata_reg[19] ;
  wire \axi_rdata_reg[19]_i_2_0 ;
  wire \axi_rdata_reg[19]_i_2_1 ;
  wire \axi_rdata_reg[19]_i_2_n_1 ;
  wire \axi_rdata_reg[19]_i_2_n_2 ;
  wire \axi_rdata_reg[19]_i_2_n_3 ;
  wire sg_ab_64_p3_reg;
  wire \sg_ab_64_p3_reg[14]__0 ;
  wire sg_ab_64_p3_reg_0;
  wire sg_ab_64_p3_reg_1;
  wire sg_ab_64_p3_reg_2;
  wire sg_ab_64_p3_reg_3;
  wire sg_ab_64_p3_reg_4;
  wire [1:0]\slv_reg4_reg[5] ;
  wire \slv_reg4_reg[7] ;
  wire \slv_reg4_reg[7]_0 ;
  wire \slv_reg4_reg[7]_1 ;
  wire \slv_reg4_reg[7]_10 ;
  wire \slv_reg4_reg[7]_11 ;
  wire \slv_reg4_reg[7]_12 ;
  wire \slv_reg4_reg[7]_13 ;
  wire \slv_reg4_reg[7]_14 ;
  wire \slv_reg4_reg[7]_15 ;
  wire \slv_reg4_reg[7]_16 ;
  wire \slv_reg4_reg[7]_17 ;
  wire \slv_reg4_reg[7]_18 ;
  wire \slv_reg4_reg[7]_19 ;
  wire \slv_reg4_reg[7]_2 ;
  wire \slv_reg4_reg[7]_3 ;
  wire \slv_reg4_reg[7]_4 ;
  wire \slv_reg4_reg[7]_5 ;
  wire \slv_reg4_reg[7]_6 ;
  wire \slv_reg4_reg[7]_7 ;
  wire \slv_reg4_reg[7]_8 ;
  wire \slv_reg4_reg[7]_9 ;
  wire [3:0]NLW_ab_nudge_carry_O_UNCONNECTED;
  wire [3:0]NLW_ab_nudge_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_ab_nudge_carry__1_O_UNCONNECTED;
  wire [3:3]NLW_ab_nudge_carry__14_CO_UNCONNECTED;
  wire [2:2]NLW_ab_nudge_carry__14_O_UNCONNECTED;
  wire [3:0]NLW_ab_nudge_carry__2_O_UNCONNECTED;
  wire [3:0]NLW_ab_nudge_carry__3_O_UNCONNECTED;
  wire [3:0]NLW_ab_nudge_carry__4_O_UNCONNECTED;
  wire [3:0]NLW_ab_nudge_carry__5_O_UNCONNECTED;
  wire [2:0]NLW_ab_nudge_carry__6_O_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry
       (.CI(1'b0),
        .CO({ab_nudge_carry_n_0,ab_nudge_carry_n_1,ab_nudge_carry_n_2,ab_nudge_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\axi_rdata[15]_i_21 [0]}),
        .O(NLW_ab_nudge_carry_O_UNCONNECTED[3:0]),
        .S({\axi_rdata[15]_i_21 [3:1],ab_nudge_carry__0_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__0
       (.CI(ab_nudge_carry_n_0),
        .CO({ab_nudge_carry__0_n_0,ab_nudge_carry__0_n_1,ab_nudge_carry__0_n_2,ab_nudge_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_ab_nudge_carry__0_O_UNCONNECTED[3:0]),
        .S(\axi_rdata[15]_i_21 [7:4]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__1
       (.CI(ab_nudge_carry__0_n_0),
        .CO({ab_nudge_carry__1_n_0,ab_nudge_carry__1_n_1,ab_nudge_carry__1_n_2,ab_nudge_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_ab_nudge_carry__1_O_UNCONNECTED[3:0]),
        .S(\axi_rdata[15]_i_21 [11:8]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__10
       (.CI(ab_nudge_carry__9_n_0),
        .CO({ab_nudge_carry__10_n_0,ab_nudge_carry__10_n_1,ab_nudge_carry__10_n_2,ab_nudge_carry__10_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(ab_nudge[16:13]),
        .S(\axi_rdata[15]_i_21 [47:44]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__11
       (.CI(ab_nudge_carry__10_n_0),
        .CO({ab_nudge_carry__11_n_0,ab_nudge_carry__11_n_1,ab_nudge_carry__11_n_2,ab_nudge_carry__11_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(ab_nudge[20:17]),
        .S(\axi_rdata[15]_i_21 [51:48]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__12
       (.CI(ab_nudge_carry__11_n_0),
        .CO({ab_nudge_carry__12_n_0,ab_nudge_carry__12_n_1,ab_nudge_carry__12_n_2,ab_nudge_carry__12_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(ab_nudge[24:21]),
        .S(\axi_rdata[15]_i_21 [55:52]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__13
       (.CI(ab_nudge_carry__12_n_0),
        .CO({ab_nudge_carry__13_n_0,ab_nudge_carry__13_n_1,ab_nudge_carry__13_n_2,ab_nudge_carry__13_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(ab_nudge[28:25]),
        .S(\axi_rdata[15]_i_21 [59:56]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__14
       (.CI(ab_nudge_carry__13_n_0),
        .CO({NLW_ab_nudge_carry__14_CO_UNCONNECTED[3],ab_nudge_carry__14_n_1,ab_nudge_carry__14_n_2,ab_nudge_carry__14_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({ab_nudge[31],NLW_ab_nudge_carry__14_O_UNCONNECTED[2],ab_nudge[30:29]}),
        .S(\axi_rdata[15]_i_21 [63:60]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__2
       (.CI(ab_nudge_carry__1_n_0),
        .CO({ab_nudge_carry__2_n_0,ab_nudge_carry__2_n_1,ab_nudge_carry__2_n_2,ab_nudge_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_ab_nudge_carry__2_O_UNCONNECTED[3:0]),
        .S(\axi_rdata[15]_i_21 [15:12]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__3
       (.CI(ab_nudge_carry__2_n_0),
        .CO({ab_nudge_carry__3_n_0,ab_nudge_carry__3_n_1,ab_nudge_carry__3_n_2,ab_nudge_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_ab_nudge_carry__3_O_UNCONNECTED[3:0]),
        .S(\axi_rdata[15]_i_21 [19:16]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__4
       (.CI(ab_nudge_carry__3_n_0),
        .CO({ab_nudge_carry__4_n_0,ab_nudge_carry__4_n_1,ab_nudge_carry__4_n_2,ab_nudge_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_ab_nudge_carry__4_O_UNCONNECTED[3:0]),
        .S(\axi_rdata[15]_i_21 [23:20]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__5
       (.CI(ab_nudge_carry__4_n_0),
        .CO({ab_nudge_carry__5_n_0,ab_nudge_carry__5_n_1,ab_nudge_carry__5_n_2,ab_nudge_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_ab_nudge_carry__5_O_UNCONNECTED[3:0]),
        .S(\axi_rdata[15]_i_21 [27:24]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__6
       (.CI(ab_nudge_carry__5_n_0),
        .CO({ab_nudge_carry__6_n_0,ab_nudge_carry__6_n_1,ab_nudge_carry__6_n_2,ab_nudge_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({\axi_rdata[15]_i_21 [31:30],1'b0,1'b0}),
        .O({ab_nudge[0],NLW_ab_nudge_carry__6_O_UNCONNECTED[2:0]}),
        .S({\axi_rdata[3]_i_19 ,\axi_rdata[15]_i_21 [29:28]}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__7
       (.CI(ab_nudge_carry__6_n_0),
        .CO({ab_nudge_carry__7_n_0,ab_nudge_carry__7_n_1,ab_nudge_carry__7_n_2,ab_nudge_carry__7_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(ab_nudge[4:1]),
        .S(\axi_rdata[15]_i_21 [35:32]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__8
       (.CI(ab_nudge_carry__7_n_0),
        .CO({ab_nudge_carry__8_n_0,ab_nudge_carry__8_n_1,ab_nudge_carry__8_n_2,ab_nudge_carry__8_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(ab_nudge[8:5]),
        .S(\axi_rdata[15]_i_21 [39:36]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 ab_nudge_carry__9
       (.CI(ab_nudge_carry__8_n_0),
        .CO({ab_nudge_carry__9_n_0,ab_nudge_carry__9_n_1,ab_nudge_carry__9_n_2,ab_nudge_carry__9_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(ab_nudge[12:9]),
        .S(\axi_rdata[15]_i_21 [43:40]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[11]_i_10 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[15]_i_7_1 [1]),
        .O(\axi_rdata[11]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[11]_i_12 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[15]_i_7_1 [0]),
        .O(\slv_reg4_reg[7]_8 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[11]_i_14 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(O[3]),
        .O(\slv_reg4_reg[7]_7 ));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[11]_i_4 
       (.I0(\axi_rdata[11]_i_8_n_0 ),
        .I1(\axi_rdata_reg[19]_i_2_0 ),
        .I2(\axi_rdata[15]_i_15_n_0 ),
        .I3(\axi_rdata_reg[11]_i_2_1 ),
        .I4(\axi_rdata[11]_i_9_n_0 ),
        .I5(\axi_rdata_reg[11]_i_2_0 ),
        .O(\slv_reg4_reg[5] [1]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[11]_i_5 
       (.I0(\axi_rdata[11]_i_10_n_0 ),
        .I1(\axi_rdata_reg[19]_i_2_0 ),
        .I2(\axi_rdata_reg[11]_i_2 ),
        .I3(\axi_rdata_reg[11]_i_2_0 ),
        .I4(\axi_rdata[11]_i_9_n_0 ),
        .I5(\axi_rdata_reg[11]_i_2_1 ),
        .O(\slv_reg4_reg[5] [0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[11]_i_8 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[15]_i_7_1 [2]),
        .O(\axi_rdata[11]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_9 
       (.I0(\axi_rdata[19]_i_18_n_0 ),
        .I1(\axi_rdata[15]_i_7_0 ),
        .I2(\axi_rdata[19]_i_6_0 ),
        .I3(sg_ab_64_p3_reg),
        .I4(\axi_rdata[19]_i_6_1 ),
        .I5(\axi_rdata[11]_i_5_0 ),
        .O(\axi_rdata[11]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[15]_i_10 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[19]_i_7_3 [1]),
        .O(\axi_rdata[15]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_11 
       (.I0(sg_ab_64_p3_reg_2),
        .I1(sg_ab_64_p3_reg_0),
        .I2(\axi_rdata[19]_i_6_0 ),
        .I3(\axi_rdata[19]_i_17_n_0 ),
        .I4(\axi_rdata[19]_i_6_1 ),
        .I5(\axi_rdata[15]_i_6_0 ),
        .O(\axi_rdata[15]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[15]_i_12 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[19]_i_7_3 [0]),
        .O(\axi_rdata[15]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_13 
       (.I0(sg_ab_64_p3_reg_1),
        .I1(sg_ab_64_p3_reg),
        .I2(\axi_rdata[19]_i_6_0 ),
        .I3(\axi_rdata[19]_i_18_n_0 ),
        .I4(\axi_rdata[19]_i_6_1 ),
        .I5(\axi_rdata[15]_i_7_0 ),
        .O(\axi_rdata[15]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[15]_i_14 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[15]_i_7_1 [3]),
        .O(\axi_rdata[15]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_15 
       (.I0(\axi_rdata[19]_i_17_n_0 ),
        .I1(\axi_rdata[15]_i_6_0 ),
        .I2(\axi_rdata[19]_i_6_0 ),
        .I3(sg_ab_64_p3_reg_0),
        .I4(\axi_rdata[19]_i_6_1 ),
        .I5(\axi_rdata[11]_i_4_0 ),
        .O(\axi_rdata[15]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hF000F000F044F077)) 
    \axi_rdata[15]_i_16 
       (.I0(ab_nudge[23]),
        .I1(\axi_rdata[23]_i_11 ),
        .I2(\axi_rdata[23]_i_11_0 ),
        .I3(\axi_rdata[23]_i_11_1 ),
        .I4(ab_nudge[15]),
        .I5(\axi_rdata[31]_i_5 ),
        .O(sg_ab_64_p3_reg));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[15]_i_4 
       (.I0(\axi_rdata[15]_i_8_n_0 ),
        .I1(\axi_rdata_reg[19]_i_2_0 ),
        .I2(\axi_rdata[19]_i_15_n_0 ),
        .I3(\axi_rdata_reg[11]_i_2_1 ),
        .I4(\axi_rdata[15]_i_9_n_0 ),
        .I5(\axi_rdata_reg[11]_i_2_0 ),
        .O(SHIFT_RIGHT[15]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[15]_i_5 
       (.I0(\axi_rdata[15]_i_10_n_0 ),
        .I1(\axi_rdata_reg[19]_i_2_0 ),
        .I2(\axi_rdata[15]_i_11_n_0 ),
        .I3(\axi_rdata_reg[11]_i_2_0 ),
        .I4(\axi_rdata[15]_i_9_n_0 ),
        .I5(\axi_rdata_reg[11]_i_2_1 ),
        .O(SHIFT_RIGHT[14]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[15]_i_6 
       (.I0(\axi_rdata[15]_i_12_n_0 ),
        .I1(\axi_rdata_reg[19]_i_2_0 ),
        .I2(\axi_rdata[15]_i_11_n_0 ),
        .I3(\axi_rdata_reg[11]_i_2_1 ),
        .I4(\axi_rdata[15]_i_13_n_0 ),
        .I5(\axi_rdata_reg[11]_i_2_0 ),
        .O(SHIFT_RIGHT[13]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[15]_i_7 
       (.I0(\axi_rdata[15]_i_14_n_0 ),
        .I1(\axi_rdata_reg[19]_i_2_0 ),
        .I2(\axi_rdata[15]_i_15_n_0 ),
        .I3(\axi_rdata_reg[11]_i_2_0 ),
        .I4(\axi_rdata[15]_i_13_n_0 ),
        .I5(\axi_rdata_reg[11]_i_2_1 ),
        .O(SHIFT_RIGHT[12]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[15]_i_8 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[19]_i_7_3 [2]),
        .O(\axi_rdata[15]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_9 
       (.I0(sg_ab_64_p3_reg_3),
        .I1(\axi_rdata[19]_i_18_n_0 ),
        .I2(\axi_rdata[19]_i_6_0 ),
        .I3(sg_ab_64_p3_reg_1),
        .I4(\axi_rdata[19]_i_6_1 ),
        .I5(sg_ab_64_p3_reg),
        .O(\axi_rdata[15]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[19]_i_10 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[23]_i_7 [1]),
        .O(\axi_rdata[19]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_11 
       (.I0(\axi_rdata[19]_i_6_2 ),
        .I1(sg_ab_64_p3_reg_2),
        .I2(\axi_rdata[19]_i_6_0 ),
        .I3(sg_ab_64_p3_reg_4),
        .I4(\axi_rdata[19]_i_6_1 ),
        .I5(\axi_rdata[19]_i_17_n_0 ),
        .O(\axi_rdata[19]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[19]_i_12 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[23]_i_7 [0]),
        .O(\axi_rdata[19]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_13 
       (.I0(\axi_rdata[19]_i_7_2 ),
        .I1(sg_ab_64_p3_reg_1),
        .I2(\axi_rdata[19]_i_6_0 ),
        .I3(sg_ab_64_p3_reg_3),
        .I4(\axi_rdata[19]_i_6_1 ),
        .I5(\axi_rdata[19]_i_18_n_0 ),
        .O(\axi_rdata[19]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[19]_i_14 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[19]_i_7_3 [3]),
        .O(\axi_rdata[19]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_15 
       (.I0(sg_ab_64_p3_reg_4),
        .I1(\axi_rdata[19]_i_17_n_0 ),
        .I2(\axi_rdata[19]_i_6_0 ),
        .I3(sg_ab_64_p3_reg_2),
        .I4(\axi_rdata[19]_i_6_1 ),
        .I5(sg_ab_64_p3_reg_0),
        .O(\axi_rdata[19]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hF000F000F044F077)) 
    \axi_rdata[19]_i_16 
       (.I0(ab_nudge[27]),
        .I1(\axi_rdata[23]_i_11 ),
        .I2(\axi_rdata[23]_i_11_0 ),
        .I3(\axi_rdata[23]_i_11_1 ),
        .I4(ab_nudge[19]),
        .I5(\axi_rdata[31]_i_5 ),
        .O(sg_ab_64_p3_reg_1));
  LUT6 #(
    .INIT(64'hF000F000F044F077)) 
    \axi_rdata[19]_i_17 
       (.I0(ab_nudge[26]),
        .I1(\axi_rdata[23]_i_11 ),
        .I2(\axi_rdata[23]_i_11_0 ),
        .I3(\axi_rdata[23]_i_11_1 ),
        .I4(ab_nudge[18]),
        .I5(\axi_rdata[31]_i_5 ),
        .O(\axi_rdata[19]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hF000F000F044F077)) 
    \axi_rdata[19]_i_18 
       (.I0(ab_nudge[25]),
        .I1(\axi_rdata[23]_i_11 ),
        .I2(\axi_rdata[23]_i_11_0 ),
        .I3(\axi_rdata[23]_i_11_1 ),
        .I4(ab_nudge[17]),
        .I5(\axi_rdata[31]_i_5 ),
        .O(\axi_rdata[19]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hF000F000F044F077)) 
    \axi_rdata[19]_i_19 
       (.I0(ab_nudge[24]),
        .I1(\axi_rdata[23]_i_11 ),
        .I2(\axi_rdata[23]_i_11_0 ),
        .I3(\axi_rdata[23]_i_11_1 ),
        .I4(ab_nudge[16]),
        .I5(\axi_rdata[31]_i_5 ),
        .O(sg_ab_64_p3_reg_0));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[19]_i_5 
       (.I0(\axi_rdata[19]_i_10_n_0 ),
        .I1(\axi_rdata_reg[19]_i_2_0 ),
        .I2(\axi_rdata_reg[19]_i_2_1 ),
        .I3(\axi_rdata_reg[11]_i_2_1 ),
        .I4(\axi_rdata[19]_i_11_n_0 ),
        .I5(\axi_rdata_reg[11]_i_2_0 ),
        .O(SHIFT_RIGHT[18]));
  LUT6 #(
    .INIT(64'hAAAAAAAA20AA2020)) 
    \axi_rdata[19]_i_6 
       (.I0(\axi_rdata[19]_i_12_n_0 ),
        .I1(\axi_rdata[19]_i_13_n_0 ),
        .I2(\axi_rdata_reg[11]_i_2_0 ),
        .I3(\axi_rdata[19]_i_11_n_0 ),
        .I4(\axi_rdata_reg[11]_i_2_1 ),
        .I5(\axi_rdata_reg[19]_i_2_0 ),
        .O(SHIFT_RIGHT[17]));
  LUT6 #(
    .INIT(64'h8A88AAAA8A888A88)) 
    \axi_rdata[19]_i_7 
       (.I0(\axi_rdata[19]_i_14_n_0 ),
        .I1(\axi_rdata_reg[19]_i_2_0 ),
        .I2(\axi_rdata[19]_i_13_n_0 ),
        .I3(\axi_rdata_reg[11]_i_2_1 ),
        .I4(\axi_rdata[19]_i_15_n_0 ),
        .I5(\axi_rdata_reg[11]_i_2_0 ),
        .O(SHIFT_RIGHT[16]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[19]_i_8 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[23]_i_7 [2]),
        .O(\slv_reg4_reg[7]_9 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[23]_i_10 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[27]_i_7 [1]),
        .O(\slv_reg4_reg[7]_12 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[23]_i_12 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[27]_i_7 [0]),
        .O(\slv_reg4_reg[7]_11 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[23]_i_14 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[23]_i_7 [3]),
        .O(\slv_reg4_reg[7]_10 ));
  LUT6 #(
    .INIT(64'hF000F000F044F077)) 
    \axi_rdata[23]_i_18 
       (.I0(ab_nudge[30]),
        .I1(\axi_rdata[23]_i_11 ),
        .I2(\axi_rdata[23]_i_11_0 ),
        .I3(\axi_rdata[23]_i_11_1 ),
        .I4(ab_nudge[22]),
        .I5(\axi_rdata[31]_i_5 ),
        .O(sg_ab_64_p3_reg_4));
  LUT6 #(
    .INIT(64'hF000F000F044F077)) 
    \axi_rdata[23]_i_19 
       (.I0(ab_nudge[29]),
        .I1(\axi_rdata[23]_i_11 ),
        .I2(\axi_rdata[23]_i_11_0 ),
        .I3(\axi_rdata[23]_i_11_1 ),
        .I4(ab_nudge[21]),
        .I5(\axi_rdata[31]_i_5 ),
        .O(sg_ab_64_p3_reg_3));
  LUT6 #(
    .INIT(64'hF000F000F044F077)) 
    \axi_rdata[23]_i_20 
       (.I0(ab_nudge[28]),
        .I1(\axi_rdata[23]_i_11 ),
        .I2(\axi_rdata[23]_i_11_0 ),
        .I3(\axi_rdata[23]_i_11_1 ),
        .I4(ab_nudge[20]),
        .I5(\axi_rdata[31]_i_5 ),
        .O(sg_ab_64_p3_reg_2));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[23]_i_8 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[27]_i_7 [2]),
        .O(\slv_reg4_reg[7]_13 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[27]_i_10 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[31]_i_8 [1]),
        .O(\slv_reg4_reg[7]_16 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[27]_i_12 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[31]_i_8 [0]),
        .O(\slv_reg4_reg[7]_15 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[27]_i_14 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[27]_i_7 [3]),
        .O(\slv_reg4_reg[7]_14 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[27]_i_8 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[31]_i_8 [2]),
        .O(\slv_reg4_reg[7]_17 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[31]_i_12 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[31]_i_7 ),
        .O(\slv_reg4_reg[7]_19 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[31]_i_16 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[31]_i_8 [3]),
        .O(\slv_reg4_reg[7]_18 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h45)) 
    \axi_rdata[3]_i_11 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[3]_i_4 ),
        .O(\slv_reg4_reg[7]_5 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h45)) 
    \axi_rdata[3]_i_13 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[7]_i_7 [2]),
        .O(\slv_reg4_reg[7]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h45)) 
    \axi_rdata[3]_i_15 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[7]_i_7 [1]),
        .O(\slv_reg4_reg[7]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h45)) 
    \axi_rdata[3]_i_16 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[7]_i_7 [0]),
        .O(\slv_reg4_reg[7]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h45)) 
    \axi_rdata[7]_i_11 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(O[1]),
        .O(\slv_reg4_reg[7]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h45)) 
    \axi_rdata[7]_i_13 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(O[0]),
        .O(\slv_reg4_reg[7]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \axi_rdata[7]_i_14 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(\axi_rdata[7]_i_7 [3]),
        .O(\slv_reg4_reg[7]_6 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h45)) 
    \axi_rdata[7]_i_9 
       (.I0(\axi_rdata[7]_i_4 ),
        .I1(\sg_ab_64_p3_reg[14]__0 ),
        .I2(O[2]),
        .O(\slv_reg4_reg[7] ));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \axi_rdata_reg[15]_i_2 
       (.CI(\axi_rdata_reg[15] ),
        .CO({\axi_rdata_reg[15]_i_2_n_0 ,\axi_rdata_reg[15]_i_2_n_1 ,\axi_rdata_reg[15]_i_2_n_2 ,\axi_rdata_reg[15]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\axi_rdata[19]_i_7_0 [3:0]),
        .S(SHIFT_RIGHT[15:12]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \axi_rdata_reg[19]_i_2 
       (.CI(\axi_rdata_reg[15]_i_2_n_0 ),
        .CO({\axi_rdata[19]_i_7_1 ,\axi_rdata_reg[19]_i_2_n_1 ,\axi_rdata_reg[19]_i_2_n_2 ,\axi_rdata_reg[19]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\axi_rdata[19]_i_7_0 [7:4]),
        .S({\axi_rdata_reg[19] ,SHIFT_RIGHT[18:16]}));
  LUT2 #(
    .INIT(4'h1)) 
    i__carry_i_8
       (.I0(ab_nudge[0]),
        .I1(\axi_rdata[31]_i_5 ),
        .O(\sg_ab_64_p3_reg[14]__0 ));
endmodule

module fer_soc_bd_tflite_mbqm_0_0_tflite_core4
   (\slv_reg4_reg[7] ,
    \slv_reg4_reg[7]_0 ,
    O,
    \slv_reg4_reg[2] ,
    \slv_reg4_reg[1] ,
    \slv_reg4_reg[2]_0 ,
    \slv_reg4_reg[2]_1 ,
    \slv_reg4_reg[2]_2 ,
    \slv_reg4_reg[1]_0 ,
    CO,
    \slv_reg4_reg[2]_3 ,
    \slv_reg4_reg[3] ,
    \slv_reg4_reg[7]_1 ,
    \slv_reg4_reg[0] ,
    \slv_reg4_reg[1]_1 ,
    minusOp_carry__4_0,
    \_inferred__3/i__carry_0 ,
    \_inferred__3/i__carry_1 ,
    ab_nudge,
    \_inferred__3/i__carry__6_0 ,
    \_inferred__3/i__carry__6_1 );
  output [0:0]\slv_reg4_reg[7] ;
  output [3:0]\slv_reg4_reg[7]_0 ;
  output [3:0]O;
  output [3:0]\slv_reg4_reg[2] ;
  output [3:0]\slv_reg4_reg[1] ;
  output [3:0]\slv_reg4_reg[2]_0 ;
  output [3:0]\slv_reg4_reg[2]_1 ;
  output [3:0]\slv_reg4_reg[2]_2 ;
  output [2:0]\slv_reg4_reg[1]_0 ;
  output [0:0]CO;
  output \slv_reg4_reg[2]_3 ;
  output \slv_reg4_reg[3] ;
  output \slv_reg4_reg[7]_1 ;
  output \slv_reg4_reg[0] ;
  output \slv_reg4_reg[1]_1 ;
  input [7:0]minusOp_carry__4_0;
  input \_inferred__3/i__carry_0 ;
  input [0:0]\_inferred__3/i__carry_1 ;
  input [29:0]ab_nudge;
  input \_inferred__3/i__carry__6_0 ;
  input \_inferred__3/i__carry__6_1 ;

  wire [0:0]CO;
  wire [3:0]O;
  wire [30:0]SHIFT_LEFT;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__0_n_0 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__0_n_1 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__0_n_2 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__0_n_3 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__1_n_0 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__1_n_1 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__1_n_2 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__1_n_3 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__2_n_0 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__2_n_1 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__2_n_2 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__2_n_3 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__3_n_0 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__3_n_1 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__3_n_2 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__3_n_3 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__4_n_0 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__4_n_1 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__4_n_2 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__4_n_3 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__5_n_0 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__5_n_1 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__5_n_2 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__5_n_3 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__6_n_2 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry__6_n_3 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry_n_0 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry_n_1 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry_n_2 ;
  wire \SHIFT_RIGHT3_inferred__0/i__carry_n_3 ;
  wire \_inferred__3/i__carry_0 ;
  wire [0:0]\_inferred__3/i__carry_1 ;
  wire \_inferred__3/i__carry__0_n_0 ;
  wire \_inferred__3/i__carry__0_n_1 ;
  wire \_inferred__3/i__carry__0_n_2 ;
  wire \_inferred__3/i__carry__0_n_3 ;
  wire \_inferred__3/i__carry__1_n_0 ;
  wire \_inferred__3/i__carry__1_n_1 ;
  wire \_inferred__3/i__carry__1_n_2 ;
  wire \_inferred__3/i__carry__1_n_3 ;
  wire \_inferred__3/i__carry__2_n_0 ;
  wire \_inferred__3/i__carry__2_n_1 ;
  wire \_inferred__3/i__carry__2_n_2 ;
  wire \_inferred__3/i__carry__2_n_3 ;
  wire \_inferred__3/i__carry__3_n_0 ;
  wire \_inferred__3/i__carry__3_n_1 ;
  wire \_inferred__3/i__carry__3_n_2 ;
  wire \_inferred__3/i__carry__3_n_3 ;
  wire \_inferred__3/i__carry__4_n_0 ;
  wire \_inferred__3/i__carry__4_n_1 ;
  wire \_inferred__3/i__carry__4_n_2 ;
  wire \_inferred__3/i__carry__4_n_3 ;
  wire \_inferred__3/i__carry__5_n_0 ;
  wire \_inferred__3/i__carry__5_n_1 ;
  wire \_inferred__3/i__carry__5_n_2 ;
  wire \_inferred__3/i__carry__5_n_3 ;
  wire \_inferred__3/i__carry__6_0 ;
  wire \_inferred__3/i__carry__6_1 ;
  wire \_inferred__3/i__carry__6_n_1 ;
  wire \_inferred__3/i__carry__6_n_2 ;
  wire \_inferred__3/i__carry__6_n_3 ;
  wire \_inferred__3/i__carry_n_0 ;
  wire \_inferred__3/i__carry_n_1 ;
  wire \_inferred__3/i__carry_n_2 ;
  wire \_inferred__3/i__carry_n_3 ;
  wire [29:0]ab_nudge;
  wire [30:0]b;
  wire i__carry__0_i_1__0_n_0;
  wire i__carry__0_i_1__1_n_0;
  wire i__carry__0_i_1__1_n_1;
  wire i__carry__0_i_1__1_n_2;
  wire i__carry__0_i_1__1_n_3;
  wire i__carry__0_i_2__0_n_0;
  wire i__carry__0_i_2__1_n_0;
  wire i__carry__0_i_3__0_n_0;
  wire i__carry__0_i_3__1_n_0;
  wire i__carry__0_i_4__0_n_0;
  wire i__carry__0_i_4__1_n_0;
  wire i__carry__0_i_5_n_0;
  wire i__carry__1_i_1__0_n_0;
  wire i__carry__1_i_1__1_n_0;
  wire i__carry__1_i_1__1_n_1;
  wire i__carry__1_i_1__1_n_2;
  wire i__carry__1_i_1__1_n_3;
  wire i__carry__1_i_2__0_n_0;
  wire i__carry__1_i_2__1_n_0;
  wire i__carry__1_i_3__0_n_0;
  wire i__carry__1_i_3__1_n_0;
  wire i__carry__1_i_4__0_n_0;
  wire i__carry__1_i_4_n_0;
  wire i__carry__1_i_5_n_0;
  wire i__carry__2_i_1__0_n_0;
  wire i__carry__2_i_1__0_n_1;
  wire i__carry__2_i_1__0_n_2;
  wire i__carry__2_i_1__0_n_3;
  wire i__carry__2_i_1_n_0;
  wire i__carry__2_i_2__0_n_0;
  wire i__carry__2_i_2_n_0;
  wire i__carry__2_i_3__0_n_0;
  wire i__carry__2_i_3_n_0;
  wire i__carry__2_i_4__0_n_0;
  wire i__carry__2_i_4_n_0;
  wire i__carry__2_i_5_n_0;
  wire i__carry__3_i_1__0_n_0;
  wire i__carry__3_i_1__0_n_1;
  wire i__carry__3_i_1__0_n_2;
  wire i__carry__3_i_1__0_n_3;
  wire i__carry__3_i_1_n_0;
  wire i__carry__3_i_2__0_n_0;
  wire i__carry__3_i_2_n_0;
  wire i__carry__3_i_3__0_n_0;
  wire i__carry__3_i_3_n_0;
  wire i__carry__3_i_4__0_n_0;
  wire i__carry__3_i_4_n_0;
  wire i__carry__3_i_5_n_0;
  wire i__carry__4_i_1__0_n_0;
  wire i__carry__4_i_1__0_n_1;
  wire i__carry__4_i_1__0_n_2;
  wire i__carry__4_i_1__0_n_3;
  wire i__carry__4_i_1_n_0;
  wire i__carry__4_i_2__0_n_0;
  wire i__carry__4_i_2_n_0;
  wire i__carry__4_i_3__0_n_0;
  wire i__carry__4_i_3_n_0;
  wire i__carry__4_i_4__0_n_0;
  wire i__carry__4_i_4_n_0;
  wire i__carry__4_i_5__0_n_0;
  wire i__carry__4_i_5_n_0;
  wire i__carry__5_i_1__0_n_0;
  wire i__carry__5_i_1__0_n_1;
  wire i__carry__5_i_1__0_n_2;
  wire i__carry__5_i_1__0_n_3;
  wire i__carry__5_i_1_n_0;
  wire i__carry__5_i_2__0_n_0;
  wire i__carry__5_i_2_n_0;
  wire i__carry__5_i_3__0_n_0;
  wire i__carry__5_i_3_n_0;
  wire i__carry__5_i_4__0_n_0;
  wire i__carry__5_i_4_n_0;
  wire i__carry__5_i_5_n_0;
  wire i__carry__6_i_1__0_n_0;
  wire i__carry__6_i_1__0_n_2;
  wire i__carry__6_i_1__0_n_3;
  wire i__carry__6_i_1_n_0;
  wire i__carry__6_i_2__0_n_0;
  wire i__carry__6_i_2_n_0;
  wire i__carry__6_i_3__0_n_0;
  wire i__carry__6_i_3_n_0;
  wire i__carry__6_i_4_n_0;
  wire i__carry__6_i_5_n_0;
  wire i__carry_i_1__0_n_0;
  wire i__carry_i_1__1_n_0;
  wire i__carry_i_1__1_n_1;
  wire i__carry_i_1__1_n_2;
  wire i__carry_i_1__1_n_3;
  wire i__carry_i_2__0_n_0;
  wire i__carry_i_2__1_n_0;
  wire i__carry_i_3__0_n_0;
  wire i__carry_i_3__1_n_0;
  wire i__carry_i_4__0_n_0;
  wire i__carry_i_4__1_n_0;
  wire i__carry_i_5_n_0;
  wire minusOp_carry__0_i_5_n_0;
  wire minusOp_carry__0_i_6_n_0;
  wire minusOp_carry__0_i_7_n_0;
  wire minusOp_carry__0_i_8_n_0;
  wire minusOp_carry__0_i_9_n_0;
  wire minusOp_carry__0_n_0;
  wire minusOp_carry__0_n_1;
  wire minusOp_carry__0_n_2;
  wire minusOp_carry__0_n_3;
  wire minusOp_carry__0_n_4;
  wire minusOp_carry__0_n_5;
  wire minusOp_carry__0_n_6;
  wire minusOp_carry__0_n_7;
  wire minusOp_carry__1_i_5_n_0;
  wire minusOp_carry__1_i_6_n_0;
  wire minusOp_carry__1_i_7_n_0;
  wire minusOp_carry__1_i_8_n_0;
  wire minusOp_carry__1_n_0;
  wire minusOp_carry__1_n_1;
  wire minusOp_carry__1_n_2;
  wire minusOp_carry__1_n_3;
  wire minusOp_carry__1_n_4;
  wire minusOp_carry__1_n_5;
  wire minusOp_carry__1_n_6;
  wire minusOp_carry__1_n_7;
  wire minusOp_carry__2_i_5_n_0;
  wire minusOp_carry__2_i_6_n_0;
  wire minusOp_carry__2_i_7_n_0;
  wire minusOp_carry__2_i_8_n_0;
  wire minusOp_carry__2_i_9_n_0;
  wire minusOp_carry__2_n_0;
  wire minusOp_carry__2_n_1;
  wire minusOp_carry__2_n_2;
  wire minusOp_carry__2_n_3;
  wire minusOp_carry__2_n_4;
  wire minusOp_carry__2_n_5;
  wire minusOp_carry__2_n_6;
  wire minusOp_carry__2_n_7;
  wire minusOp_carry__3_i_5_n_0;
  wire minusOp_carry__3_i_6_n_0;
  wire minusOp_carry__3_i_7_n_0;
  wire minusOp_carry__3_i_8_n_0;
  wire minusOp_carry__3_n_0;
  wire minusOp_carry__3_n_1;
  wire minusOp_carry__3_n_2;
  wire minusOp_carry__3_n_3;
  wire minusOp_carry__3_n_4;
  wire minusOp_carry__3_n_5;
  wire minusOp_carry__3_n_6;
  wire minusOp_carry__3_n_7;
  wire [7:0]minusOp_carry__4_0;
  wire minusOp_carry__4_i_5_n_0;
  wire minusOp_carry__4_i_6_n_0;
  wire minusOp_carry__4_i_7_n_0;
  wire minusOp_carry__4_i_8_n_0;
  wire minusOp_carry__4_i_9_n_0;
  wire minusOp_carry__4_n_0;
  wire minusOp_carry__4_n_1;
  wire minusOp_carry__4_n_2;
  wire minusOp_carry__4_n_3;
  wire minusOp_carry__4_n_4;
  wire minusOp_carry__4_n_5;
  wire minusOp_carry__4_n_6;
  wire minusOp_carry__4_n_7;
  wire minusOp_carry__5_i_5_n_0;
  wire minusOp_carry__5_i_6_n_0;
  wire minusOp_carry__5_i_7_n_0;
  wire minusOp_carry__5_i_8_n_0;
  wire minusOp_carry__5_i_9_n_0;
  wire minusOp_carry__5_n_0;
  wire minusOp_carry__5_n_1;
  wire minusOp_carry__5_n_2;
  wire minusOp_carry__5_n_3;
  wire minusOp_carry__5_n_4;
  wire minusOp_carry__5_n_5;
  wire minusOp_carry__5_n_6;
  wire minusOp_carry__5_n_7;
  wire minusOp_carry__6_i_3_n_0;
  wire minusOp_carry__6_i_4_n_0;
  wire minusOp_carry__6_i_5_n_0;
  wire minusOp_carry__6_n_2;
  wire minusOp_carry__6_n_3;
  wire minusOp_carry__6_n_5;
  wire minusOp_carry__6_n_6;
  wire minusOp_carry__6_n_7;
  wire minusOp_carry_i_10_n_0;
  wire minusOp_carry_i_11_n_0;
  wire minusOp_carry_i_6_n_0;
  wire minusOp_carry_i_7_n_0;
  wire minusOp_carry_i_8_n_0;
  wire minusOp_carry_i_9_n_0;
  wire minusOp_carry_n_0;
  wire minusOp_carry_n_1;
  wire minusOp_carry_n_2;
  wire minusOp_carry_n_3;
  wire minusOp_carry_n_4;
  wire minusOp_carry_n_5;
  wire minusOp_carry_n_6;
  wire \slv_reg4_reg[0] ;
  wire [3:0]\slv_reg4_reg[1] ;
  wire [2:0]\slv_reg4_reg[1]_0 ;
  wire \slv_reg4_reg[1]_1 ;
  wire [3:0]\slv_reg4_reg[2] ;
  wire [3:0]\slv_reg4_reg[2]_0 ;
  wire [3:0]\slv_reg4_reg[2]_1 ;
  wire [3:0]\slv_reg4_reg[2]_2 ;
  wire \slv_reg4_reg[2]_3 ;
  wire \slv_reg4_reg[3] ;
  wire [0:0]\slv_reg4_reg[7] ;
  wire [3:0]\slv_reg4_reg[7]_0 ;
  wire \slv_reg4_reg[7]_1 ;
  wire [3:2]\NLW_SHIFT_RIGHT3_inferred__0/i__carry__6_CO_UNCONNECTED ;
  wire [3:3]\NLW_SHIFT_RIGHT3_inferred__0/i__carry__6_O_UNCONNECTED ;
  wire [3:0]\NLW__inferred__3/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW__inferred__3/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW__inferred__3/i__carry__1_O_UNCONNECTED ;
  wire [3:0]\NLW__inferred__3/i__carry__2_O_UNCONNECTED ;
  wire [3:0]\NLW__inferred__3/i__carry__3_O_UNCONNECTED ;
  wire [3:0]\NLW__inferred__3/i__carry__4_O_UNCONNECTED ;
  wire [3:0]\NLW__inferred__3/i__carry__5_O_UNCONNECTED ;
  wire [3:0]\NLW__inferred__3/i__carry__6_O_UNCONNECTED ;
  wire [2:2]NLW_i__carry__6_i_1__0_CO_UNCONNECTED;
  wire [3:3]NLW_i__carry__6_i_1__0_O_UNCONNECTED;
  wire [3:2]NLW_minusOp_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_minusOp_carry__6_O_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \SHIFT_RIGHT3_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\SHIFT_RIGHT3_inferred__0/i__carry_n_0 ,\SHIFT_RIGHT3_inferred__0/i__carry_n_1 ,\SHIFT_RIGHT3_inferred__0/i__carry_n_2 ,\SHIFT_RIGHT3_inferred__0/i__carry_n_3 }),
        .CYINIT(SHIFT_LEFT[0]),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O(\slv_reg4_reg[7]_0 ),
        .S({i__carry_i_1__0_n_0,i__carry_i_2__0_n_0,i__carry_i_3__0_n_0,i__carry_i_4__0_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \SHIFT_RIGHT3_inferred__0/i__carry__0 
       (.CI(\SHIFT_RIGHT3_inferred__0/i__carry_n_0 ),
        .CO({\SHIFT_RIGHT3_inferred__0/i__carry__0_n_0 ,\SHIFT_RIGHT3_inferred__0/i__carry__0_n_1 ,\SHIFT_RIGHT3_inferred__0/i__carry__0_n_2 ,\SHIFT_RIGHT3_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O(O),
        .S({i__carry__0_i_1__0_n_0,i__carry__0_i_2__0_n_0,i__carry__0_i_3__0_n_0,i__carry__0_i_4__0_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \SHIFT_RIGHT3_inferred__0/i__carry__1 
       (.CI(\SHIFT_RIGHT3_inferred__0/i__carry__0_n_0 ),
        .CO({\SHIFT_RIGHT3_inferred__0/i__carry__1_n_0 ,\SHIFT_RIGHT3_inferred__0/i__carry__1_n_1 ,\SHIFT_RIGHT3_inferred__0/i__carry__1_n_2 ,\SHIFT_RIGHT3_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O(\slv_reg4_reg[2] ),
        .S({i__carry__1_i_1__0_n_0,i__carry__1_i_2__0_n_0,i__carry__1_i_3__0_n_0,i__carry__1_i_4_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \SHIFT_RIGHT3_inferred__0/i__carry__2 
       (.CI(\SHIFT_RIGHT3_inferred__0/i__carry__1_n_0 ),
        .CO({\SHIFT_RIGHT3_inferred__0/i__carry__2_n_0 ,\SHIFT_RIGHT3_inferred__0/i__carry__2_n_1 ,\SHIFT_RIGHT3_inferred__0/i__carry__2_n_2 ,\SHIFT_RIGHT3_inferred__0/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O(\slv_reg4_reg[1] ),
        .S({i__carry__2_i_1_n_0,i__carry__2_i_2_n_0,i__carry__2_i_3_n_0,i__carry__2_i_4_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \SHIFT_RIGHT3_inferred__0/i__carry__3 
       (.CI(\SHIFT_RIGHT3_inferred__0/i__carry__2_n_0 ),
        .CO({\SHIFT_RIGHT3_inferred__0/i__carry__3_n_0 ,\SHIFT_RIGHT3_inferred__0/i__carry__3_n_1 ,\SHIFT_RIGHT3_inferred__0/i__carry__3_n_2 ,\SHIFT_RIGHT3_inferred__0/i__carry__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O(\slv_reg4_reg[2]_0 ),
        .S({i__carry__3_i_1_n_0,i__carry__3_i_2_n_0,i__carry__3_i_3_n_0,i__carry__3_i_4_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \SHIFT_RIGHT3_inferred__0/i__carry__4 
       (.CI(\SHIFT_RIGHT3_inferred__0/i__carry__3_n_0 ),
        .CO({\SHIFT_RIGHT3_inferred__0/i__carry__4_n_0 ,\SHIFT_RIGHT3_inferred__0/i__carry__4_n_1 ,\SHIFT_RIGHT3_inferred__0/i__carry__4_n_2 ,\SHIFT_RIGHT3_inferred__0/i__carry__4_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry__4_i_1_n_0,1'b1,1'b1,1'b1}),
        .O(\slv_reg4_reg[2]_1 ),
        .S({i__carry__4_i_2_n_0,i__carry__4_i_3_n_0,i__carry__4_i_4_n_0,i__carry__4_i_5_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \SHIFT_RIGHT3_inferred__0/i__carry__5 
       (.CI(\SHIFT_RIGHT3_inferred__0/i__carry__4_n_0 ),
        .CO({\SHIFT_RIGHT3_inferred__0/i__carry__5_n_0 ,\SHIFT_RIGHT3_inferred__0/i__carry__5_n_1 ,\SHIFT_RIGHT3_inferred__0/i__carry__5_n_2 ,\SHIFT_RIGHT3_inferred__0/i__carry__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O(\slv_reg4_reg[2]_2 ),
        .S({i__carry__5_i_1_n_0,i__carry__5_i_2_n_0,i__carry__5_i_3_n_0,i__carry__5_i_4_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 \SHIFT_RIGHT3_inferred__0/i__carry__6 
       (.CI(\SHIFT_RIGHT3_inferred__0/i__carry__5_n_0 ),
        .CO({\NLW_SHIFT_RIGHT3_inferred__0/i__carry__6_CO_UNCONNECTED [3:2],\SHIFT_RIGHT3_inferred__0/i__carry__6_n_2 ,\SHIFT_RIGHT3_inferred__0/i__carry__6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b1}),
        .O({\NLW_SHIFT_RIGHT3_inferred__0/i__carry__6_O_UNCONNECTED [3],\slv_reg4_reg[1]_0 }),
        .S({1'b0,i__carry__6_i_1_n_0,i__carry__6_i_2_n_0,i__carry__6_i_3_n_0}));
  CARRY4 \_inferred__3/i__carry 
       (.CI(1'b0),
        .CO({\_inferred__3/i__carry_n_0 ,\_inferred__3/i__carry_n_1 ,\_inferred__3/i__carry_n_2 ,\_inferred__3/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(b[3:0]),
        .O(\NLW__inferred__3/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_2__1_n_0,i__carry_i_3__1_n_0,i__carry_i_4__1_n_0,i__carry_i_5_n_0}));
  CARRY4 \_inferred__3/i__carry__0 
       (.CI(\_inferred__3/i__carry_n_0 ),
        .CO({\_inferred__3/i__carry__0_n_0 ,\_inferred__3/i__carry__0_n_1 ,\_inferred__3/i__carry__0_n_2 ,\_inferred__3/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI(b[7:4]),
        .O(\NLW__inferred__3/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_2__1_n_0,i__carry__0_i_3__1_n_0,i__carry__0_i_4__1_n_0,i__carry__0_i_5_n_0}));
  CARRY4 \_inferred__3/i__carry__1 
       (.CI(\_inferred__3/i__carry__0_n_0 ),
        .CO({\_inferred__3/i__carry__1_n_0 ,\_inferred__3/i__carry__1_n_1 ,\_inferred__3/i__carry__1_n_2 ,\_inferred__3/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI(b[11:8]),
        .O(\NLW__inferred__3/i__carry__1_O_UNCONNECTED [3:0]),
        .S({i__carry__1_i_2__1_n_0,i__carry__1_i_3__1_n_0,i__carry__1_i_4__0_n_0,i__carry__1_i_5_n_0}));
  CARRY4 \_inferred__3/i__carry__2 
       (.CI(\_inferred__3/i__carry__1_n_0 ),
        .CO({\_inferred__3/i__carry__2_n_0 ,\_inferred__3/i__carry__2_n_1 ,\_inferred__3/i__carry__2_n_2 ,\_inferred__3/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI(b[15:12]),
        .O(\NLW__inferred__3/i__carry__2_O_UNCONNECTED [3:0]),
        .S({i__carry__2_i_2__0_n_0,i__carry__2_i_3__0_n_0,i__carry__2_i_4__0_n_0,i__carry__2_i_5_n_0}));
  CARRY4 \_inferred__3/i__carry__3 
       (.CI(\_inferred__3/i__carry__2_n_0 ),
        .CO({\_inferred__3/i__carry__3_n_0 ,\_inferred__3/i__carry__3_n_1 ,\_inferred__3/i__carry__3_n_2 ,\_inferred__3/i__carry__3_n_3 }),
        .CYINIT(1'b0),
        .DI(b[19:16]),
        .O(\NLW__inferred__3/i__carry__3_O_UNCONNECTED [3:0]),
        .S({i__carry__3_i_2__0_n_0,i__carry__3_i_3__0_n_0,i__carry__3_i_4__0_n_0,i__carry__3_i_5_n_0}));
  CARRY4 \_inferred__3/i__carry__4 
       (.CI(\_inferred__3/i__carry__3_n_0 ),
        .CO({\_inferred__3/i__carry__4_n_0 ,\_inferred__3/i__carry__4_n_1 ,\_inferred__3/i__carry__4_n_2 ,\_inferred__3/i__carry__4_n_3 }),
        .CYINIT(1'b0),
        .DI(b[23:20]),
        .O(\NLW__inferred__3/i__carry__4_O_UNCONNECTED [3:0]),
        .S({i__carry__4_i_2__0_n_0,i__carry__4_i_3__0_n_0,i__carry__4_i_4__0_n_0,i__carry__4_i_5__0_n_0}));
  CARRY4 \_inferred__3/i__carry__5 
       (.CI(\_inferred__3/i__carry__4_n_0 ),
        .CO({\_inferred__3/i__carry__5_n_0 ,\_inferred__3/i__carry__5_n_1 ,\_inferred__3/i__carry__5_n_2 ,\_inferred__3/i__carry__5_n_3 }),
        .CYINIT(1'b0),
        .DI(b[27:24]),
        .O(\NLW__inferred__3/i__carry__5_O_UNCONNECTED [3:0]),
        .S({i__carry__5_i_2__0_n_0,i__carry__5_i_3__0_n_0,i__carry__5_i_4__0_n_0,i__carry__5_i_5_n_0}));
  CARRY4 \_inferred__3/i__carry__6 
       (.CI(\_inferred__3/i__carry__5_n_0 ),
        .CO({CO,\_inferred__3/i__carry__6_n_1 ,\_inferred__3/i__carry__6_n_2 ,\_inferred__3/i__carry__6_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry__6_i_1__0_n_0,b[30:28]}),
        .O(\NLW__inferred__3/i__carry__6_O_UNCONNECTED [3:0]),
        .S({i__carry__6_i_2__0_n_0,i__carry__6_i_3__0_n_0,i__carry__6_i_4_n_0,i__carry__6_i_5_n_0}));
  LUT5 #(
    .INIT(32'hAAAAFFFE)) 
    i__carry__0_i_1__0
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry_i_10_n_0),
        .O(i__carry__0_i_1__0_n_0));
  CARRY4 i__carry__0_i_1__1
       (.CI(i__carry_i_1__1_n_0),
        .CO({i__carry__0_i_1__1_n_0,i__carry__0_i_1__1_n_1,i__carry__0_i_1__1_n_2,i__carry__0_i_1__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(b[7:4]),
        .S({minusOp_carry__0_n_4,minusOp_carry__0_n_5,minusOp_carry__0_n_6,minusOp_carry__0_n_7}));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    i__carry__0_i_2__0
       (.I0(minusOp_carry_i_11_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry_i_10_n_0),
        .O(i__carry__0_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__0_i_2__1
       (.I0(b[7]),
        .I1(minusOp_carry__0_n_5),
        .I2(ab_nudge[6]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__0_i_2__1_n_0));
  LUT4 #(
    .INIT(16'hFBFF)) 
    i__carry__0_i_3__0
       (.I0(minusOp_carry_i_11_n_0),
        .I1(\slv_reg4_reg[0] ),
        .I2(minusOp_carry__4_0[0]),
        .I3(\slv_reg4_reg[1]_1 ),
        .O(i__carry__0_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__0_i_3__1
       (.I0(b[6]),
        .I1(minusOp_carry__0_n_6),
        .I2(ab_nudge[5]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__0_i_3__1_n_0));
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    i__carry__0_i_4__0
       (.I0(minusOp_carry_i_11_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry_i_10_n_0),
        .O(i__carry__0_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__0_i_4__1
       (.I0(b[5]),
        .I1(minusOp_carry__0_n_7),
        .I2(ab_nudge[4]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__0_i_4__1_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__0_i_5
       (.I0(b[4]),
        .I1(minusOp_carry_n_4),
        .I2(ab_nudge[3]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__0_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    i__carry__1_i_1__0
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry_i_10_n_0),
        .O(i__carry__1_i_1__0_n_0));
  CARRY4 i__carry__1_i_1__1
       (.CI(i__carry__0_i_1__1_n_0),
        .CO({i__carry__1_i_1__1_n_0,i__carry__1_i_1__1_n_1,i__carry__1_i_1__1_n_2,i__carry__1_i_1__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(b[11:8]),
        .S({minusOp_carry__1_n_4,minusOp_carry__1_n_5,minusOp_carry__1_n_6,minusOp_carry__1_n_7}));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    i__carry__1_i_2__0
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(i__carry__1_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__1_i_2__1
       (.I0(b[11]),
        .I1(minusOp_carry__1_n_5),
        .I2(ab_nudge[10]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__1_i_2__1_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    i__carry__1_i_3__0
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(\slv_reg4_reg[1]_1 ),
        .I2(\slv_reg4_reg[0] ),
        .I3(minusOp_carry__4_0[0]),
        .O(i__carry__1_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__1_i_3__1
       (.I0(b[10]),
        .I1(minusOp_carry__1_n_6),
        .I2(ab_nudge[9]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__1_i_3__1_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    i__carry__1_i_4
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(i__carry__1_i_4_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__1_i_4__0
       (.I0(b[9]),
        .I1(minusOp_carry__1_n_7),
        .I2(ab_nudge[8]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__1_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__1_i_5
       (.I0(b[8]),
        .I1(minusOp_carry__0_n_4),
        .I2(ab_nudge[7]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__1_i_5_n_0));
  LUT5 #(
    .INIT(32'hAAAAFFFE)) 
    i__carry__2_i_1
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry_i_10_n_0),
        .O(i__carry__2_i_1_n_0));
  CARRY4 i__carry__2_i_1__0
       (.CI(i__carry__1_i_1__1_n_0),
        .CO({i__carry__2_i_1__0_n_0,i__carry__2_i_1__0_n_1,i__carry__2_i_1__0_n_2,i__carry__2_i_1__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(b[15:12]),
        .S({minusOp_carry__2_n_4,minusOp_carry__2_n_5,minusOp_carry__2_n_6,minusOp_carry__2_n_7}));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    i__carry__2_i_2
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry_i_10_n_0),
        .O(i__carry__2_i_2_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__2_i_2__0
       (.I0(b[15]),
        .I1(minusOp_carry__2_n_5),
        .I2(ab_nudge[14]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__2_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hFBFF)) 
    i__carry__2_i_3
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(\slv_reg4_reg[0] ),
        .I2(minusOp_carry__4_0[0]),
        .I3(\slv_reg4_reg[1]_1 ),
        .O(i__carry__2_i_3_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__2_i_3__0
       (.I0(b[14]),
        .I1(minusOp_carry__2_n_6),
        .I2(ab_nudge[13]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__2_i_3__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    i__carry__2_i_4
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry_i_10_n_0),
        .O(i__carry__2_i_4_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__2_i_4__0
       (.I0(b[13]),
        .I1(minusOp_carry__2_n_7),
        .I2(ab_nudge[12]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__2_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__2_i_5
       (.I0(b[12]),
        .I1(minusOp_carry__1_n_4),
        .I2(ab_nudge[11]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__2_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    i__carry__3_i_1
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry_i_10_n_0),
        .O(i__carry__3_i_1_n_0));
  CARRY4 i__carry__3_i_1__0
       (.CI(i__carry__2_i_1__0_n_0),
        .CO({i__carry__3_i_1__0_n_0,i__carry__3_i_1__0_n_1,i__carry__3_i_1__0_n_2,i__carry__3_i_1__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(b[19:16]),
        .S({minusOp_carry__3_n_4,minusOp_carry__3_n_5,minusOp_carry__3_n_6,minusOp_carry__3_n_7}));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    i__carry__3_i_2
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(i__carry__3_i_2_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__3_i_2__0
       (.I0(b[19]),
        .I1(minusOp_carry__3_n_5),
        .I2(ab_nudge[18]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__3_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    i__carry__3_i_3
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(\slv_reg4_reg[1]_1 ),
        .I2(\slv_reg4_reg[0] ),
        .I3(minusOp_carry__4_0[0]),
        .O(i__carry__3_i_3_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__3_i_3__0
       (.I0(b[18]),
        .I1(minusOp_carry__3_n_6),
        .I2(ab_nudge[17]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__3_i_3__0_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    i__carry__3_i_4
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(i__carry__3_i_4_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__3_i_4__0
       (.I0(b[17]),
        .I1(minusOp_carry__3_n_7),
        .I2(ab_nudge[16]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__3_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__3_i_5
       (.I0(b[16]),
        .I1(minusOp_carry__2_n_4),
        .I2(ab_nudge[15]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__3_i_5_n_0));
  LUT5 #(
    .INIT(32'h44444445)) 
    i__carry__4_i_1
       (.I0(minusOp_carry__5_i_9_n_0),
        .I1(minusOp_carry_i_10_n_0),
        .I2(minusOp_carry__4_0[2]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry__4_0[1]),
        .O(i__carry__4_i_1_n_0));
  CARRY4 i__carry__4_i_1__0
       (.CI(i__carry__3_i_1__0_n_0),
        .CO({i__carry__4_i_1__0_n_0,i__carry__4_i_1__0_n_1,i__carry__4_i_1__0_n_2,i__carry__4_i_1__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(b[23:20]),
        .S({minusOp_carry__4_n_4,minusOp_carry__4_n_5,minusOp_carry__4_n_6,minusOp_carry__4_n_7}));
  LUT5 #(
    .INIT(32'hFFFF00FE)) 
    i__carry__4_i_2
       (.I0(minusOp_carry__4_0[1]),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[2]),
        .I3(minusOp_carry_i_10_n_0),
        .I4(minusOp_carry__5_i_9_n_0),
        .O(i__carry__4_i_2_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__4_i_2__0
       (.I0(b[23]),
        .I1(minusOp_carry__4_n_5),
        .I2(ab_nudge[22]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__4_i_2__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    i__carry__4_i_3
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry_i_10_n_0),
        .I4(minusOp_carry__2_i_9_n_0),
        .O(i__carry__4_i_3_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__4_i_3__0
       (.I0(b[22]),
        .I1(minusOp_carry__4_n_6),
        .I2(ab_nudge[21]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__4_i_3__0_n_0));
  LUT4 #(
    .INIT(16'hFBFF)) 
    i__carry__4_i_4
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(\slv_reg4_reg[0] ),
        .I2(minusOp_carry__4_0[0]),
        .I3(\slv_reg4_reg[1]_1 ),
        .O(i__carry__4_i_4_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__4_i_4__0
       (.I0(b[21]),
        .I1(minusOp_carry__4_n_7),
        .I2(ab_nudge[20]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__4_i_4__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    i__carry__4_i_5
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry_i_10_n_0),
        .O(i__carry__4_i_5_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__4_i_5__0
       (.I0(b[20]),
        .I1(minusOp_carry__3_n_4),
        .I2(ab_nudge[19]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__4_i_5__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    i__carry__5_i_1
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry_i_10_n_0),
        .I4(minusOp_carry__5_i_9_n_0),
        .O(i__carry__5_i_1_n_0));
  CARRY4 i__carry__5_i_1__0
       (.CI(i__carry__4_i_1__0_n_0),
        .CO({i__carry__5_i_1__0_n_0,i__carry__5_i_1__0_n_1,i__carry__5_i_1__0_n_2,i__carry__5_i_1__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(b[27:24]),
        .S({minusOp_carry__5_n_4,minusOp_carry__5_n_5,minusOp_carry__5_n_6,minusOp_carry__5_n_7}));
  LUT5 #(
    .INIT(32'hFFFFFDFF)) 
    i__carry__5_i_2
       (.I0(minusOp_carry__4_0[0]),
        .I1(minusOp_carry_i_10_n_0),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry__5_i_9_n_0),
        .O(i__carry__5_i_2_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__5_i_2__0
       (.I0(b[27]),
        .I1(minusOp_carry__5_n_5),
        .I2(ab_nudge[26]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__5_i_2__0_n_0));
  LUT4 #(
    .INIT(16'hFFFB)) 
    i__carry__5_i_3
       (.I0(\slv_reg4_reg[1]_1 ),
        .I1(\slv_reg4_reg[0] ),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__5_i_9_n_0),
        .O(i__carry__5_i_3_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__5_i_3__0
       (.I0(b[26]),
        .I1(minusOp_carry__5_n_6),
        .I2(ab_nudge[25]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__5_i_3__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFDFFF)) 
    i__carry__5_i_4
       (.I0(minusOp_carry__4_0[0]),
        .I1(minusOp_carry_i_10_n_0),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry__5_i_9_n_0),
        .O(i__carry__5_i_4_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__5_i_4__0
       (.I0(b[25]),
        .I1(minusOp_carry__5_n_7),
        .I2(ab_nudge[24]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__5_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__5_i_5
       (.I0(b[24]),
        .I1(minusOp_carry__4_n_4),
        .I2(ab_nudge[23]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__5_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    i__carry__6_i_1
       (.I0(minusOp_carry__5_i_9_n_0),
        .I1(minusOp_carry_i_10_n_0),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry__4_0[2]),
        .O(i__carry__6_i_1_n_0));
  CARRY4 i__carry__6_i_1__0
       (.CI(i__carry__5_i_1__0_n_0),
        .CO({i__carry__6_i_1__0_n_0,NLW_i__carry__6_i_1__0_CO_UNCONNECTED[2],i__carry__6_i_1__0_n_2,i__carry__6_i_1__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b1,1'b0,1'b0}),
        .O({NLW_i__carry__6_i_1__0_O_UNCONNECTED[3],b[30:28]}),
        .S({1'b1,minusOp_carry__6_n_5,minusOp_carry__6_n_6,minusOp_carry__6_n_7}));
  LUT4 #(
    .INIT(16'hFFDF)) 
    i__carry__6_i_2
       (.I0(\slv_reg4_reg[0] ),
        .I1(minusOp_carry__4_0[0]),
        .I2(\slv_reg4_reg[1]_1 ),
        .I3(minusOp_carry__5_i_9_n_0),
        .O(i__carry__6_i_2_n_0));
  LUT3 #(
    .INIT(8'h9A)) 
    i__carry__6_i_2__0
       (.I0(i__carry__6_i_1__0_n_0),
        .I1(\_inferred__3/i__carry__6_1 ),
        .I2(minusOp_carry__6_n_5),
        .O(i__carry__6_i_2__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFBF)) 
    i__carry__6_i_3
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry_i_10_n_0),
        .I4(minusOp_carry__5_i_9_n_0),
        .O(i__carry__6_i_3_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__6_i_3__0
       (.I0(b[30]),
        .I1(minusOp_carry__6_n_6),
        .I2(ab_nudge[29]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__6_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__6_i_4
       (.I0(b[29]),
        .I1(minusOp_carry__6_n_7),
        .I2(ab_nudge[28]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__6_i_4_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry__6_i_5
       (.I0(b[28]),
        .I1(minusOp_carry__5_n_4),
        .I2(ab_nudge[27]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry__6_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    i__carry_i_1__0
       (.I0(minusOp_carry_i_11_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry_i_10_n_0),
        .O(i__carry_i_1__0_n_0));
  CARRY4 i__carry_i_1__1
       (.CI(1'b0),
        .CO({i__carry_i_1__1_n_0,i__carry_i_1__1_n_1,i__carry_i_1__1_n_2,i__carry_i_1__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\slv_reg4_reg[7] }),
        .O(b[3:0]),
        .S({minusOp_carry_n_4,minusOp_carry_n_5,minusOp_carry_n_6,\_inferred__3/i__carry_1 }));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    i__carry_i_2__0
       (.I0(minusOp_carry_i_11_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(i__carry_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry_i_2__1
       (.I0(b[3]),
        .I1(minusOp_carry_n_5),
        .I2(ab_nudge[2]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry_i_2__1_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    i__carry_i_3__0
       (.I0(minusOp_carry_i_11_n_0),
        .I1(\slv_reg4_reg[1]_1 ),
        .I2(\slv_reg4_reg[0] ),
        .I3(minusOp_carry__4_0[0]),
        .O(i__carry_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry_i_3__1
       (.I0(b[2]),
        .I1(minusOp_carry_n_6),
        .I2(ab_nudge[1]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry_i_3__1_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    i__carry_i_4__0
       (.I0(minusOp_carry_i_11_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(i__carry_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9995)) 
    i__carry_i_4__1
       (.I0(b[1]),
        .I1(\slv_reg4_reg[7] ),
        .I2(ab_nudge[0]),
        .I3(\_inferred__3/i__carry__6_0 ),
        .O(i__carry_i_4__1_n_0));
  LUT3 #(
    .INIT(8'h65)) 
    i__carry_i_5
       (.I0(b[0]),
        .I1(\_inferred__3/i__carry_0 ),
        .I2(minusOp_carry__4_0[7]),
        .O(i__carry_i_5_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 minusOp_carry
       (.CI(1'b0),
        .CO({minusOp_carry_n_0,minusOp_carry_n_1,minusOp_carry_n_2,minusOp_carry_n_3}),
        .CYINIT(SHIFT_LEFT[0]),
        .DI(SHIFT_LEFT[4:1]),
        .O({minusOp_carry_n_4,minusOp_carry_n_5,minusOp_carry_n_6,\slv_reg4_reg[7] }),
        .S({minusOp_carry_i_6_n_0,minusOp_carry_i_7_n_0,minusOp_carry_i_8_n_0,minusOp_carry_i_9_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 minusOp_carry__0
       (.CI(minusOp_carry_n_0),
        .CO({minusOp_carry__0_n_0,minusOp_carry__0_n_1,minusOp_carry__0_n_2,minusOp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(SHIFT_LEFT[8:5]),
        .O({minusOp_carry__0_n_4,minusOp_carry__0_n_5,minusOp_carry__0_n_6,minusOp_carry__0_n_7}),
        .S({minusOp_carry__0_i_5_n_0,minusOp_carry__0_i_6_n_0,minusOp_carry__0_i_7_n_0,minusOp_carry__0_i_8_n_0}));
  LUT5 #(
    .INIT(32'h0000AAAB)) 
    minusOp_carry__0_i_1
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__0_i_9_n_0),
        .O(SHIFT_LEFT[8]));
  LUT5 #(
    .INIT(32'h00000010)) 
    minusOp_carry__0_i_2
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry_i_11_n_0),
        .O(SHIFT_LEFT[7]));
  LUT4 #(
    .INIT(16'h0020)) 
    minusOp_carry__0_i_3
       (.I0(\slv_reg4_reg[1]_1 ),
        .I1(minusOp_carry__4_0[0]),
        .I2(\slv_reg4_reg[0] ),
        .I3(minusOp_carry_i_11_n_0),
        .O(SHIFT_LEFT[6]));
  LUT5 #(
    .INIT(32'h00000040)) 
    minusOp_carry__0_i_4
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry_i_11_n_0),
        .O(SHIFT_LEFT[5]));
  LUT5 #(
    .INIT(32'hAAAAFFFE)) 
    minusOp_carry__0_i_5
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry_i_10_n_0),
        .O(minusOp_carry__0_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    minusOp_carry__0_i_6
       (.I0(minusOp_carry_i_11_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry_i_10_n_0),
        .O(minusOp_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'hFBFF)) 
    minusOp_carry__0_i_7
       (.I0(minusOp_carry_i_11_n_0),
        .I1(\slv_reg4_reg[0] ),
        .I2(minusOp_carry__4_0[0]),
        .I3(\slv_reg4_reg[1]_1 ),
        .O(minusOp_carry__0_i_7_n_0));
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    minusOp_carry__0_i_8
       (.I0(minusOp_carry_i_11_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry_i_10_n_0),
        .O(minusOp_carry__0_i_8_n_0));
  LUT6 #(
    .INIT(64'hFFFFDF00FFFFFFFF)) 
    minusOp_carry__0_i_9
       (.I0(minusOp_carry__4_0[5]),
        .I1(\slv_reg4_reg[2]_3 ),
        .I2(minusOp_carry__4_0[6]),
        .I3(minusOp_carry__4_0[7]),
        .I4(\slv_reg4_reg[7]_1 ),
        .I5(\slv_reg4_reg[3] ),
        .O(minusOp_carry__0_i_9_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 minusOp_carry__1
       (.CI(minusOp_carry__0_n_0),
        .CO({minusOp_carry__1_n_0,minusOp_carry__1_n_1,minusOp_carry__1_n_2,minusOp_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(SHIFT_LEFT[12:9]),
        .O({minusOp_carry__1_n_4,minusOp_carry__1_n_5,minusOp_carry__1_n_6,minusOp_carry__1_n_7}),
        .S({minusOp_carry__1_i_5_n_0,minusOp_carry__1_i_6_n_0,minusOp_carry__1_i_7_n_0,minusOp_carry__1_i_8_n_0}));
  LUT5 #(
    .INIT(32'h00000100)) 
    minusOp_carry__1_i_1
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry__0_i_9_n_0),
        .O(SHIFT_LEFT[12]));
  LUT5 #(
    .INIT(32'h00000200)) 
    minusOp_carry__1_i_2
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry__0_i_9_n_0),
        .O(SHIFT_LEFT[11]));
  LUT4 #(
    .INIT(16'h0004)) 
    minusOp_carry__1_i_3
       (.I0(minusOp_carry__4_0[0]),
        .I1(\slv_reg4_reg[0] ),
        .I2(\slv_reg4_reg[1]_1 ),
        .I3(minusOp_carry__0_i_9_n_0),
        .O(SHIFT_LEFT[10]));
  LUT5 #(
    .INIT(32'h00000800)) 
    minusOp_carry__1_i_4
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry__0_i_9_n_0),
        .O(SHIFT_LEFT[9]));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    minusOp_carry__1_i_5
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry_i_10_n_0),
        .O(minusOp_carry__1_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    minusOp_carry__1_i_6
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(minusOp_carry__1_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    minusOp_carry__1_i_7
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(\slv_reg4_reg[1]_1 ),
        .I2(\slv_reg4_reg[0] ),
        .I3(minusOp_carry__4_0[0]),
        .O(minusOp_carry__1_i_7_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    minusOp_carry__1_i_8
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(minusOp_carry__1_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 minusOp_carry__2
       (.CI(minusOp_carry__1_n_0),
        .CO({minusOp_carry__2_n_0,minusOp_carry__2_n_1,minusOp_carry__2_n_2,minusOp_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(SHIFT_LEFT[16:13]),
        .O({minusOp_carry__2_n_4,minusOp_carry__2_n_5,minusOp_carry__2_n_6,minusOp_carry__2_n_7}),
        .S({minusOp_carry__2_i_5_n_0,minusOp_carry__2_i_6_n_0,minusOp_carry__2_i_7_n_0,minusOp_carry__2_i_8_n_0}));
  LUT5 #(
    .INIT(32'h0000AAAB)) 
    minusOp_carry__2_i_1
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__2_i_9_n_0),
        .O(SHIFT_LEFT[16]));
  LUT5 #(
    .INIT(32'h00000010)) 
    minusOp_carry__2_i_2
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry__0_i_9_n_0),
        .O(SHIFT_LEFT[15]));
  LUT4 #(
    .INIT(16'h0020)) 
    minusOp_carry__2_i_3
       (.I0(\slv_reg4_reg[1]_1 ),
        .I1(minusOp_carry__4_0[0]),
        .I2(\slv_reg4_reg[0] ),
        .I3(minusOp_carry__0_i_9_n_0),
        .O(SHIFT_LEFT[14]));
  LUT5 #(
    .INIT(32'h00000040)) 
    minusOp_carry__2_i_4
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry__0_i_9_n_0),
        .O(SHIFT_LEFT[13]));
  LUT5 #(
    .INIT(32'hAAAAFFFE)) 
    minusOp_carry__2_i_5
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry_i_10_n_0),
        .O(minusOp_carry__2_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    minusOp_carry__2_i_6
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry_i_10_n_0),
        .O(minusOp_carry__2_i_6_n_0));
  LUT4 #(
    .INIT(16'hFBFF)) 
    minusOp_carry__2_i_7
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(\slv_reg4_reg[0] ),
        .I2(minusOp_carry__4_0[0]),
        .I3(\slv_reg4_reg[1]_1 ),
        .O(minusOp_carry__2_i_7_n_0));
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    minusOp_carry__2_i_8
       (.I0(minusOp_carry__0_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry_i_10_n_0),
        .O(minusOp_carry__2_i_8_n_0));
  LUT6 #(
    .INIT(64'hFFFFDF00FFFFFFFF)) 
    minusOp_carry__2_i_9
       (.I0(minusOp_carry__4_0[5]),
        .I1(\slv_reg4_reg[2]_3 ),
        .I2(minusOp_carry__4_0[6]),
        .I3(minusOp_carry__4_0[7]),
        .I4(\slv_reg4_reg[3] ),
        .I5(\slv_reg4_reg[7]_1 ),
        .O(minusOp_carry__2_i_9_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 minusOp_carry__3
       (.CI(minusOp_carry__2_n_0),
        .CO({minusOp_carry__3_n_0,minusOp_carry__3_n_1,minusOp_carry__3_n_2,minusOp_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(SHIFT_LEFT[20:17]),
        .O({minusOp_carry__3_n_4,minusOp_carry__3_n_5,minusOp_carry__3_n_6,minusOp_carry__3_n_7}),
        .S({minusOp_carry__3_i_5_n_0,minusOp_carry__3_i_6_n_0,minusOp_carry__3_i_7_n_0,minusOp_carry__3_i_8_n_0}));
  LUT5 #(
    .INIT(32'h00000100)) 
    minusOp_carry__3_i_1
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry__2_i_9_n_0),
        .O(SHIFT_LEFT[20]));
  LUT5 #(
    .INIT(32'h00000200)) 
    minusOp_carry__3_i_2
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry__2_i_9_n_0),
        .O(SHIFT_LEFT[19]));
  LUT4 #(
    .INIT(16'h0004)) 
    minusOp_carry__3_i_3
       (.I0(minusOp_carry__4_0[0]),
        .I1(\slv_reg4_reg[0] ),
        .I2(\slv_reg4_reg[1]_1 ),
        .I3(minusOp_carry__2_i_9_n_0),
        .O(SHIFT_LEFT[18]));
  LUT5 #(
    .INIT(32'h00000800)) 
    minusOp_carry__3_i_4
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry__2_i_9_n_0),
        .O(SHIFT_LEFT[17]));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    minusOp_carry__3_i_5
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry_i_10_n_0),
        .O(minusOp_carry__3_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    minusOp_carry__3_i_6
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(minusOp_carry__3_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    minusOp_carry__3_i_7
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(\slv_reg4_reg[1]_1 ),
        .I2(\slv_reg4_reg[0] ),
        .I3(minusOp_carry__4_0[0]),
        .O(minusOp_carry__3_i_7_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    minusOp_carry__3_i_8
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(minusOp_carry__3_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 minusOp_carry__4
       (.CI(minusOp_carry__3_n_0),
        .CO({minusOp_carry__4_n_0,minusOp_carry__4_n_1,minusOp_carry__4_n_2,minusOp_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(SHIFT_LEFT[24:21]),
        .O({minusOp_carry__4_n_4,minusOp_carry__4_n_5,minusOp_carry__4_n_6,minusOp_carry__4_n_7}),
        .S({minusOp_carry__4_i_5_n_0,minusOp_carry__4_i_6_n_0,minusOp_carry__4_i_7_n_0,minusOp_carry__4_i_8_n_0}));
  LUT6 #(
    .INIT(64'h0000080000000000)) 
    minusOp_carry__4_i_1
       (.I0(minusOp_carry__4_0[7]),
        .I1(minusOp_carry__4_0[6]),
        .I2(minusOp_carry__4_0[4]),
        .I3(minusOp_carry__4_0[3]),
        .I4(minusOp_carry__4_i_9_n_0),
        .I5(minusOp_carry__4_0[5]),
        .O(SHIFT_LEFT[24]));
  LUT5 #(
    .INIT(32'h00000100)) 
    minusOp_carry__4_i_2
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry_i_10_n_0),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry__4_0[2]),
        .O(SHIFT_LEFT[23]));
  LUT4 #(
    .INIT(16'h0020)) 
    minusOp_carry__4_i_3
       (.I0(\slv_reg4_reg[1]_1 ),
        .I1(minusOp_carry__4_0[0]),
        .I2(\slv_reg4_reg[0] ),
        .I3(minusOp_carry__2_i_9_n_0),
        .O(SHIFT_LEFT[22]));
  LUT5 #(
    .INIT(32'h00000040)) 
    minusOp_carry__4_i_4
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry__2_i_9_n_0),
        .O(SHIFT_LEFT[21]));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    minusOp_carry__4_i_5
       (.I0(minusOp_carry__4_0[5]),
        .I1(minusOp_carry__4_i_9_n_0),
        .I2(minusOp_carry__4_0[3]),
        .I3(minusOp_carry__4_0[4]),
        .I4(minusOp_carry__4_0[6]),
        .I5(minusOp_carry__4_0[7]),
        .O(minusOp_carry__4_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    minusOp_carry__4_i_6
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry_i_10_n_0),
        .I4(minusOp_carry__2_i_9_n_0),
        .O(minusOp_carry__4_i_6_n_0));
  LUT4 #(
    .INIT(16'hFBFF)) 
    minusOp_carry__4_i_7
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(\slv_reg4_reg[0] ),
        .I2(minusOp_carry__4_0[0]),
        .I3(\slv_reg4_reg[1]_1 ),
        .O(minusOp_carry__4_i_7_n_0));
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    minusOp_carry__4_i_8
       (.I0(minusOp_carry__2_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry_i_10_n_0),
        .O(minusOp_carry__4_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    minusOp_carry__4_i_9
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[1]),
        .O(minusOp_carry__4_i_9_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 minusOp_carry__5
       (.CI(minusOp_carry__4_n_0),
        .CO({minusOp_carry__5_n_0,minusOp_carry__5_n_1,minusOp_carry__5_n_2,minusOp_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(SHIFT_LEFT[28:25]),
        .O({minusOp_carry__5_n_4,minusOp_carry__5_n_5,minusOp_carry__5_n_6,minusOp_carry__5_n_7}),
        .S({minusOp_carry__5_i_5_n_0,minusOp_carry__5_i_6_n_0,minusOp_carry__5_i_7_n_0,minusOp_carry__5_i_8_n_0}));
  LUT5 #(
    .INIT(32'h00010000)) 
    minusOp_carry__5_i_1
       (.I0(minusOp_carry__5_i_9_n_0),
        .I1(minusOp_carry_i_10_n_0),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(SHIFT_LEFT[28]));
  LUT5 #(
    .INIT(32'h00040000)) 
    minusOp_carry__5_i_2
       (.I0(minusOp_carry__5_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry_i_10_n_0),
        .I4(minusOp_carry__4_0[0]),
        .O(SHIFT_LEFT[27]));
  LUT4 #(
    .INIT(16'h0010)) 
    minusOp_carry__5_i_3
       (.I0(minusOp_carry__5_i_9_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(\slv_reg4_reg[0] ),
        .I3(\slv_reg4_reg[1]_1 ),
        .O(SHIFT_LEFT[26]));
  LUT5 #(
    .INIT(32'h00400000)) 
    minusOp_carry__5_i_4
       (.I0(minusOp_carry__5_i_9_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry_i_10_n_0),
        .I4(minusOp_carry__4_0[0]),
        .O(SHIFT_LEFT[25]));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    minusOp_carry__5_i_5
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry_i_10_n_0),
        .I4(minusOp_carry__5_i_9_n_0),
        .O(minusOp_carry__5_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFDFF)) 
    minusOp_carry__5_i_6
       (.I0(minusOp_carry__4_0[0]),
        .I1(minusOp_carry_i_10_n_0),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry__5_i_9_n_0),
        .O(minusOp_carry__5_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFFB)) 
    minusOp_carry__5_i_7
       (.I0(\slv_reg4_reg[1]_1 ),
        .I1(\slv_reg4_reg[0] ),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__5_i_9_n_0),
        .O(minusOp_carry__5_i_7_n_0));
  LUT5 #(
    .INIT(32'hFFFFDFFF)) 
    minusOp_carry__5_i_8
       (.I0(minusOp_carry__4_0[0]),
        .I1(minusOp_carry_i_10_n_0),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry__5_i_9_n_0),
        .O(minusOp_carry__5_i_8_n_0));
  LUT6 #(
    .INIT(64'hDF00FFFFFFFFFFFF)) 
    minusOp_carry__5_i_9
       (.I0(minusOp_carry__4_0[5]),
        .I1(\slv_reg4_reg[2]_3 ),
        .I2(minusOp_carry__4_0[6]),
        .I3(minusOp_carry__4_0[7]),
        .I4(\slv_reg4_reg[7]_1 ),
        .I5(\slv_reg4_reg[3] ),
        .O(minusOp_carry__5_i_9_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 minusOp_carry__6
       (.CI(minusOp_carry__5_n_0),
        .CO({NLW_minusOp_carry__6_CO_UNCONNECTED[3:2],minusOp_carry__6_n_2,minusOp_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,SHIFT_LEFT[30:29]}),
        .O({NLW_minusOp_carry__6_O_UNCONNECTED[3],minusOp_carry__6_n_5,minusOp_carry__6_n_6,minusOp_carry__6_n_7}),
        .S({1'b0,minusOp_carry__6_i_3_n_0,minusOp_carry__6_i_4_n_0,minusOp_carry__6_i_5_n_0}));
  LUT4 #(
    .INIT(16'h0400)) 
    minusOp_carry__6_i_1
       (.I0(minusOp_carry__5_i_9_n_0),
        .I1(\slv_reg4_reg[1]_1 ),
        .I2(minusOp_carry__4_0[0]),
        .I3(\slv_reg4_reg[0] ),
        .O(SHIFT_LEFT[30]));
  LUT5 #(
    .INIT(32'h00001000)) 
    minusOp_carry__6_i_2
       (.I0(minusOp_carry__5_i_9_n_0),
        .I1(minusOp_carry_i_10_n_0),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry__4_0[2]),
        .O(SHIFT_LEFT[29]));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    minusOp_carry__6_i_3
       (.I0(minusOp_carry__5_i_9_n_0),
        .I1(minusOp_carry_i_10_n_0),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry__4_0[2]),
        .O(minusOp_carry__6_i_3_n_0));
  LUT4 #(
    .INIT(16'hFFDF)) 
    minusOp_carry__6_i_4
       (.I0(\slv_reg4_reg[0] ),
        .I1(minusOp_carry__4_0[0]),
        .I2(\slv_reg4_reg[1]_1 ),
        .I3(minusOp_carry__5_i_9_n_0),
        .O(minusOp_carry__6_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFBF)) 
    minusOp_carry__6_i_5
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry_i_10_n_0),
        .I4(minusOp_carry__5_i_9_n_0),
        .O(minusOp_carry__6_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry_i_1
       (.I0(minusOp_carry__4_0[7]),
        .O(SHIFT_LEFT[0]));
  LUT4 #(
    .INIT(16'h5545)) 
    minusOp_carry_i_10
       (.I0(minusOp_carry__4_0[7]),
        .I1(minusOp_carry__4_0[6]),
        .I2(\slv_reg4_reg[2]_3 ),
        .I3(minusOp_carry__4_0[5]),
        .O(minusOp_carry_i_10_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFDF00)) 
    minusOp_carry_i_11
       (.I0(minusOp_carry__4_0[5]),
        .I1(\slv_reg4_reg[2]_3 ),
        .I2(minusOp_carry__4_0[6]),
        .I3(minusOp_carry__4_0[7]),
        .I4(\slv_reg4_reg[3] ),
        .I5(\slv_reg4_reg[7]_1 ),
        .O(minusOp_carry_i_11_n_0));
  LUT3 #(
    .INIT(8'h14)) 
    minusOp_carry_i_12
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[1]),
        .O(\slv_reg4_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h0154)) 
    minusOp_carry_i_13
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry__4_0[0]),
        .I3(minusOp_carry__4_0[2]),
        .O(\slv_reg4_reg[1]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    minusOp_carry_i_14
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[3]),
        .I4(minusOp_carry__4_0[4]),
        .O(\slv_reg4_reg[2]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h11111114)) 
    minusOp_carry_i_15
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[3]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry__4_0[2]),
        .O(\slv_reg4_reg[3] ));
  LUT6 #(
    .INIT(64'h2222222222222228)) 
    minusOp_carry_i_16
       (.I0(minusOp_carry__4_0[7]),
        .I1(minusOp_carry__4_0[4]),
        .I2(minusOp_carry__4_0[3]),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[0]),
        .I5(minusOp_carry__4_0[2]),
        .O(\slv_reg4_reg[7]_1 ));
  LUT5 #(
    .INIT(32'h00000100)) 
    minusOp_carry_i_2
       (.I0(minusOp_carry_i_10_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[2]),
        .I4(minusOp_carry_i_11_n_0),
        .O(SHIFT_LEFT[4]));
  LUT5 #(
    .INIT(32'h00000200)) 
    minusOp_carry_i_3
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry_i_11_n_0),
        .O(SHIFT_LEFT[3]));
  LUT4 #(
    .INIT(16'h0004)) 
    minusOp_carry_i_4
       (.I0(minusOp_carry__4_0[0]),
        .I1(\slv_reg4_reg[0] ),
        .I2(\slv_reg4_reg[1]_1 ),
        .I3(minusOp_carry_i_11_n_0),
        .O(SHIFT_LEFT[2]));
  LUT5 #(
    .INIT(32'h00000800)) 
    minusOp_carry_i_5
       (.I0(minusOp_carry__4_0[2]),
        .I1(minusOp_carry__4_0[1]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry_i_11_n_0),
        .O(SHIFT_LEFT[1]));
  LUT5 #(
    .INIT(32'hFFFFFFFB)) 
    minusOp_carry_i_6
       (.I0(minusOp_carry_i_11_n_0),
        .I1(minusOp_carry__4_0[2]),
        .I2(minusOp_carry__4_0[1]),
        .I3(minusOp_carry__4_0[0]),
        .I4(minusOp_carry_i_10_n_0),
        .O(minusOp_carry_i_6_n_0));
  LUT5 #(
    .INIT(32'hFFFBFFFF)) 
    minusOp_carry_i_7
       (.I0(minusOp_carry_i_11_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(minusOp_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    minusOp_carry_i_8
       (.I0(minusOp_carry_i_11_n_0),
        .I1(\slv_reg4_reg[1]_1 ),
        .I2(\slv_reg4_reg[0] ),
        .I3(minusOp_carry__4_0[0]),
        .O(minusOp_carry_i_8_n_0));
  LUT5 #(
    .INIT(32'hFBFFFFFF)) 
    minusOp_carry_i_9
       (.I0(minusOp_carry_i_11_n_0),
        .I1(minusOp_carry__4_0[0]),
        .I2(minusOp_carry_i_10_n_0),
        .I3(minusOp_carry__4_0[1]),
        .I4(minusOp_carry__4_0[2]),
        .O(minusOp_carry_i_9_n_0));
endmodule

module fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm
   (\slv_reg4_reg[2] ,
    D,
    \axi_rdata[31]_i_8 ,
    SR,
    s00_axi_aclk,
    Q,
    \sg_a_p0_reg[31] ,
    \axi_rdata_reg[3]_i_2 ,
    \axi_rdata[19]_i_5 ,
    \axi_rdata_reg[31]_i_3 ,
    sel0,
    \axi_rdata_reg[0] ,
    \qq_reg[0]_0 ,
    \sg_a_p0_reg[31]_0 ,
    \axi_rdata_reg[3]_i_2_0 ,
    s00_axi_aresetn);
  output \slv_reg4_reg[2] ;
  output [0:0]D;
  output [30:0]\axi_rdata[31]_i_8 ;
  output [0:0]SR;
  input s00_axi_aclk;
  input [31:0]Q;
  input [31:0]\sg_a_p0_reg[31] ;
  input \axi_rdata_reg[3]_i_2 ;
  input [7:0]\axi_rdata[19]_i_5 ;
  input \axi_rdata_reg[31]_i_3 ;
  input [2:0]sel0;
  input \axi_rdata_reg[0] ;
  input [0:0]\qq_reg[0]_0 ;
  input [31:0]\sg_a_p0_reg[31]_0 ;
  input \axi_rdata_reg[3]_i_2_0 ;
  input s00_axi_aresetn;

  wire [0:0]D;
  wire [31:0]Q;
  wire [19:10]SHIFT_RIGHT;
  wire SM0_n_32;
  wire SM0_n_33;
  wire SM0_n_36;
  wire SM0_n_37;
  wire SM0_n_38;
  wire SM0_n_39;
  wire SM0_n_40;
  wire SM0_n_41;
  wire SM0_n_42;
  wire SM0_n_43;
  wire SM0_n_44;
  wire SM0_n_68;
  wire SM0_n_69;
  wire SM1_n_64;
  wire SM1_n_65;
  wire SM1_n_66;
  wire SM1_n_67;
  wire SM1_n_68;
  wire SM1_n_69;
  wire SM1_n_70;
  wire SM2_n_32;
  wire SM2_n_33;
  wire SM2_n_34;
  wire SM2_n_35;
  wire SM2_n_36;
  wire SM2_n_37;
  wire SM2_n_38;
  wire SM2_n_39;
  wire SM2_n_42;
  wire SM2_n_43;
  wire SM2_n_44;
  wire SM2_n_45;
  wire SM2_n_46;
  wire SM2_n_47;
  wire SM2_n_48;
  wire SM2_n_49;
  wire SM2_n_50;
  wire SM2_n_51;
  wire SM2_n_52;
  wire SM2_n_53;
  wire SM2_n_54;
  wire SM2_n_55;
  wire SM2_n_56;
  wire SM2_n_57;
  wire SM2_n_58;
  wire SM2_n_59;
  wire SM2_n_60;
  wire SM2_n_61;
  wire SM2_n_70;
  wire SM4_n_0;
  wire SM4_n_1;
  wire SM4_n_10;
  wire SM4_n_11;
  wire SM4_n_12;
  wire SM4_n_13;
  wire SM4_n_14;
  wire SM4_n_15;
  wire SM4_n_16;
  wire SM4_n_17;
  wire SM4_n_18;
  wire SM4_n_19;
  wire SM4_n_2;
  wire SM4_n_20;
  wire SM4_n_21;
  wire SM4_n_22;
  wire SM4_n_23;
  wire SM4_n_24;
  wire SM4_n_25;
  wire SM4_n_26;
  wire SM4_n_27;
  wire SM4_n_28;
  wire SM4_n_29;
  wire SM4_n_3;
  wire SM4_n_30;
  wire SM4_n_31;
  wire SM4_n_32;
  wire SM4_n_34;
  wire SM4_n_35;
  wire SM4_n_36;
  wire SM4_n_37;
  wire SM4_n_4;
  wire SM4_n_5;
  wire SM4_n_6;
  wire SM4_n_7;
  wire SM4_n_8;
  wire SM4_n_9;
  wire [0:0]SR;
  wire [63:31]ab_nudge;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire [7:0]\axi_rdata[19]_i_5 ;
  wire [30:0]\axi_rdata[31]_i_8 ;
  wire \axi_rdata_reg[0] ;
  wire \axi_rdata_reg[31]_i_3 ;
  wire \axi_rdata_reg[3]_i_2 ;
  wire \axi_rdata_reg[3]_i_2_0 ;
  wire [6:0]dd;
  wire qq;
  wire \qq[6]_i_3_n_0 ;
  wire \qq[6]_i_4_n_0 ;
  wire [6:0]qq_reg;
  wire [0:0]\qq_reg[0]_0 ;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [2:0]sel0;
  wire [31:0]\sg_a_p0_reg[31] ;
  wire [31:0]\sg_a_p0_reg[31]_0 ;
  wire [63:0]sg_ab_64_p3_reg;
  wire \slv_reg4_reg[2] ;
  wire [31:0]xls;

  fer_soc_bd_tflite_mbqm_0_0_tflite_core0 SM0
       (.CO(SM4_n_32),
        .D(D),
        .O({SM4_n_29,SM4_n_30}),
        .Q(Q),
        .S({SM1_n_64,SM1_n_65,SM1_n_66,SM1_n_67}),
        .ab_nudge({ab_nudge[63],ab_nudge[61:31]}),
        .\axi_rdata[11]_i_5 (SM2_n_43),
        .\axi_rdata[11]_i_7_0 (SM0_n_68),
        .\axi_rdata[11]_i_7_1 (SM2_n_42),
        .\axi_rdata[19]_i_4_0 (SM2_n_45),
        .\axi_rdata[19]_i_5 ({\axi_rdata[19]_i_5 [7:5],\axi_rdata[19]_i_5 [0]}),
        .\axi_rdata[19]_i_5_0 (\slv_reg4_reg[2] ),
        .\axi_rdata[19]_i_5_1 (SM2_n_44),
        .\axi_rdata[23]_i_6_0 (SM2_n_47),
        .\axi_rdata[23]_i_7_0 (SM2_n_46),
        .\axi_rdata[27]_i_15_0 (SM4_n_34),
        .\axi_rdata[31]_i_17_0 (SM4_n_37),
        .\axi_rdata[31]_i_7_0 (SM4_n_36),
        .\axi_rdata[31]_i_8_0 ({\axi_rdata[31]_i_8 [30:19],\axi_rdata[31]_i_8 [10:0]}),
        .\axi_rdata[3]_i_22_0 (SM4_n_35),
        .\axi_rdata_reg[0] (\axi_rdata_reg[0] ),
        .\axi_rdata_reg[0]_0 (\axi_rdata[0]_i_4_n_0 ),
        .\axi_rdata_reg[11] (SHIFT_RIGHT[11:10]),
        .\axi_rdata_reg[11]_i_2_0 (SM2_n_49),
        .\axi_rdata_reg[11]_i_2_1 (SM2_n_50),
        .\axi_rdata_reg[19]_i_2 (SM2_n_51),
        .\axi_rdata_reg[23] (SM2_n_70),
        .\axi_rdata_reg[23]_i_2_0 (SM2_n_52),
        .\axi_rdata_reg[23]_i_2_1 (SM2_n_53),
        .\axi_rdata_reg[23]_i_2_2 (SM2_n_54),
        .\axi_rdata_reg[23]_i_2_3 (SM2_n_55),
        .\axi_rdata_reg[27]_i_2_0 (SM2_n_56),
        .\axi_rdata_reg[27]_i_2_1 (SM2_n_57),
        .\axi_rdata_reg[27]_i_2_2 (SM2_n_58),
        .\axi_rdata_reg[27]_i_2_3 (SM2_n_59),
        .\axi_rdata_reg[31]_i_3_0 (\axi_rdata_reg[31]_i_3 ),
        .\axi_rdata_reg[31]_i_3_1 (SM2_n_60),
        .\axi_rdata_reg[31]_i_3_2 (SM2_n_61),
        .\axi_rdata_reg[31]_i_3_3 (SM2_n_33),
        .\axi_rdata_reg[3]_i_2_0 (SM2_n_39),
        .\axi_rdata_reg[3]_i_2_1 (\axi_rdata_reg[3]_i_2 ),
        .\axi_rdata_reg[3]_i_2_2 (\axi_rdata_reg[3]_i_2_0 ),
        .\axi_rdata_reg[3]_i_2_3 (SM2_n_38),
        .\axi_rdata_reg[3]_i_2_4 (SM2_n_37),
        .\axi_rdata_reg[3]_i_2_5 (SM2_n_36),
        .\axi_rdata_reg[7]_i_2_0 (SM2_n_48),
        .\axi_rdata_reg[7]_i_2_1 (SM2_n_35),
        .\axi_rdata_reg[7]_i_2_2 (SM2_n_34),
        .\axi_rdata_reg[7]_i_2_3 (SM2_n_32),
        .i__carry_i_1__1(SM4_n_0),
        .sel0(sel0),
        .\sg_a_p0_reg[27] (\sg_a_p0_reg[31]_0 [27:0]),
        .\sg_a_p0_reg[31] (\sg_a_p0_reg[31] [30:0]),
        .sg_ab_64_p3_reg(SM0_n_69),
        .\slv_reg1_reg[11] (SM0_n_33),
        .\slv_reg1_reg[23] (SM0_n_44),
        .\slv_reg4_reg[0] (SM0_n_40),
        .\slv_reg4_reg[0]_0 (SM0_n_41),
        .\slv_reg4_reg[3] (SM0_n_42),
        .\slv_reg4_reg[3]_0 (SM0_n_43),
        .\slv_reg4_reg[5] (SM0_n_32),
        .\slv_reg4_reg[5]_0 (SHIFT_RIGHT[19]),
        .\slv_reg4_reg[7] (SM0_n_36),
        .\slv_reg4_reg[7]_0 (SM0_n_37),
        .\slv_reg4_reg[7]_1 (SM0_n_38),
        .\slv_reg4_reg[7]_2 (SM0_n_39),
        .xls(xls));
  fer_soc_bd_tflite_mbqm_0_0_tflite_core1 SM1
       (.Q(Q),
        .S({SM1_n_64,SM1_n_65,SM1_n_66,SM1_n_67}),
        .s00_axi_aclk(s00_axi_aclk),
        .\sg_a_p0_reg[31]_0 (\sg_a_p0_reg[31]_0 [31:28]),
        .\sg_a_p0_reg[31]_1 (\sg_a_p0_reg[31] [31:28]),
        .\sg_ab_64_p3_reg[0]_0 (SM1_n_70),
        .\sg_ab_64_p3_reg[14]__0_0 ({SM1_n_68,SM1_n_69}),
        .sg_ab_64_p3_reg_0(sg_ab_64_p3_reg),
        .xls(xls));
  fer_soc_bd_tflite_mbqm_0_0_tflite_core2 SM2
       (.O({SM4_n_5,SM4_n_6,SM4_n_7,SM4_n_8}),
        .ab_nudge({ab_nudge[63],ab_nudge[61:31]}),
        .ab_nudge_carry__0_0(SM1_n_70),
        .\axi_rdata[11]_i_4_0 (SM0_n_37),
        .\axi_rdata[11]_i_5_0 (SM0_n_36),
        .\axi_rdata[15]_i_21 (sg_ab_64_p3_reg),
        .\axi_rdata[15]_i_6_0 (SM0_n_39),
        .\axi_rdata[15]_i_7_0 (SM0_n_38),
        .\axi_rdata[15]_i_7_1 ({SM4_n_9,SM4_n_10,SM4_n_11,SM4_n_12}),
        .\axi_rdata[19]_i_6_0 (SM4_n_36),
        .\axi_rdata[19]_i_6_1 (SM4_n_37),
        .\axi_rdata[19]_i_6_2 (SM0_n_43),
        .\axi_rdata[19]_i_7_0 (\axi_rdata[31]_i_8 [18:11]),
        .\axi_rdata[19]_i_7_1 (SM2_n_70),
        .\axi_rdata[19]_i_7_2 (SM0_n_42),
        .\axi_rdata[19]_i_7_3 ({SM4_n_13,SM4_n_14,SM4_n_15,SM4_n_16}),
        .\axi_rdata[23]_i_11 (SM4_n_34),
        .\axi_rdata[23]_i_11_0 (SM0_n_33),
        .\axi_rdata[23]_i_11_1 (SM4_n_35),
        .\axi_rdata[23]_i_7 ({SM4_n_17,SM4_n_18,SM4_n_19,SM4_n_20}),
        .\axi_rdata[27]_i_7 ({SM4_n_21,SM4_n_22,SM4_n_23,SM4_n_24}),
        .\axi_rdata[31]_i_5 (SM0_n_44),
        .\axi_rdata[31]_i_7 (SM4_n_31),
        .\axi_rdata[31]_i_8 ({SM4_n_25,SM4_n_26,SM4_n_27,SM4_n_28}),
        .\axi_rdata[3]_i_19 ({SM1_n_68,SM1_n_69}),
        .\axi_rdata[3]_i_4 (\axi_rdata[19]_i_5 [7]),
        .\axi_rdata[7]_i_4 (\axi_rdata_reg[31]_i_3 ),
        .\axi_rdata[7]_i_7 ({SM4_n_1,SM4_n_2,SM4_n_3,SM4_n_4}),
        .\axi_rdata_reg[11]_i_2 (SM0_n_40),
        .\axi_rdata_reg[11]_i_2_0 (\axi_rdata_reg[3]_i_2_0 ),
        .\axi_rdata_reg[11]_i_2_1 (\axi_rdata_reg[3]_i_2 ),
        .\axi_rdata_reg[15] (SM0_n_68),
        .\axi_rdata_reg[19] (SHIFT_RIGHT[19]),
        .\axi_rdata_reg[19]_i_2_0 (SM0_n_32),
        .\axi_rdata_reg[19]_i_2_1 (SM0_n_41),
        .sg_ab_64_p3_reg(SM2_n_42),
        .\sg_ab_64_p3_reg[14]__0 (SM2_n_33),
        .sg_ab_64_p3_reg_0(SM2_n_43),
        .sg_ab_64_p3_reg_1(SM2_n_44),
        .sg_ab_64_p3_reg_2(SM2_n_45),
        .sg_ab_64_p3_reg_3(SM2_n_46),
        .sg_ab_64_p3_reg_4(SM2_n_47),
        .\slv_reg4_reg[5] (SHIFT_RIGHT[11:10]),
        .\slv_reg4_reg[7] (SM2_n_32),
        .\slv_reg4_reg[7]_0 (SM2_n_34),
        .\slv_reg4_reg[7]_1 (SM2_n_35),
        .\slv_reg4_reg[7]_10 (SM2_n_52),
        .\slv_reg4_reg[7]_11 (SM2_n_53),
        .\slv_reg4_reg[7]_12 (SM2_n_54),
        .\slv_reg4_reg[7]_13 (SM2_n_55),
        .\slv_reg4_reg[7]_14 (SM2_n_56),
        .\slv_reg4_reg[7]_15 (SM2_n_57),
        .\slv_reg4_reg[7]_16 (SM2_n_58),
        .\slv_reg4_reg[7]_17 (SM2_n_59),
        .\slv_reg4_reg[7]_18 (SM2_n_60),
        .\slv_reg4_reg[7]_19 (SM2_n_61),
        .\slv_reg4_reg[7]_2 (SM2_n_36),
        .\slv_reg4_reg[7]_3 (SM2_n_37),
        .\slv_reg4_reg[7]_4 (SM2_n_38),
        .\slv_reg4_reg[7]_5 (SM2_n_39),
        .\slv_reg4_reg[7]_6 (SM2_n_48),
        .\slv_reg4_reg[7]_7 (SM2_n_49),
        .\slv_reg4_reg[7]_8 (SM2_n_50),
        .\slv_reg4_reg[7]_9 (SM2_n_51));
  fer_soc_bd_tflite_mbqm_0_0_tflite_core4 SM4
       (.CO(SM4_n_32),
        .O({SM4_n_5,SM4_n_6,SM4_n_7,SM4_n_8}),
        .\_inferred__3/i__carry_0 (SM2_n_33),
        .\_inferred__3/i__carry_1 (SM0_n_69),
        .\_inferred__3/i__carry__6_0 (SM0_n_44),
        .\_inferred__3/i__carry__6_1 (SM0_n_33),
        .ab_nudge(ab_nudge[61:32]),
        .minusOp_carry__4_0(\axi_rdata[19]_i_5 ),
        .\slv_reg4_reg[0] (SM4_n_36),
        .\slv_reg4_reg[1] ({SM4_n_13,SM4_n_14,SM4_n_15,SM4_n_16}),
        .\slv_reg4_reg[1]_0 ({SM4_n_29,SM4_n_30,SM4_n_31}),
        .\slv_reg4_reg[1]_1 (SM4_n_37),
        .\slv_reg4_reg[2] ({SM4_n_9,SM4_n_10,SM4_n_11,SM4_n_12}),
        .\slv_reg4_reg[2]_0 ({SM4_n_17,SM4_n_18,SM4_n_19,SM4_n_20}),
        .\slv_reg4_reg[2]_1 ({SM4_n_21,SM4_n_22,SM4_n_23,SM4_n_24}),
        .\slv_reg4_reg[2]_2 ({SM4_n_25,SM4_n_26,SM4_n_27,SM4_n_28}),
        .\slv_reg4_reg[2]_3 (\slv_reg4_reg[2] ),
        .\slv_reg4_reg[3] (SM4_n_34),
        .\slv_reg4_reg[7] (SM4_n_0),
        .\slv_reg4_reg[7]_0 ({SM4_n_1,SM4_n_2,SM4_n_3,SM4_n_4}),
        .\slv_reg4_reg[7]_1 (SM4_n_35));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(SR));
  LUT4 #(
    .INIT(16'h0004)) 
    \axi_rdata[0]_i_4 
       (.I0(qq_reg[5]),
        .I1(qq_reg[3]),
        .I2(qq_reg[4]),
        .I3(\qq[6]_i_3_n_0 ),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \qq[0]_i_1 
       (.I0(qq_reg[0]),
        .O(dd[0]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \qq[1]_i_1 
       (.I0(qq_reg[0]),
        .I1(qq_reg[1]),
        .O(dd[1]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \qq[2]_i_1 
       (.I0(qq_reg[2]),
        .I1(qq_reg[1]),
        .I2(qq_reg[0]),
        .O(dd[2]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \qq[3]_i_1 
       (.I0(qq_reg[3]),
        .I1(qq_reg[0]),
        .I2(qq_reg[1]),
        .I3(qq_reg[2]),
        .O(dd[3]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \qq[4]_i_1 
       (.I0(qq_reg[4]),
        .I1(qq_reg[2]),
        .I2(qq_reg[1]),
        .I3(qq_reg[0]),
        .I4(qq_reg[3]),
        .O(dd[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \qq[5]_i_1 
       (.I0(qq_reg[5]),
        .I1(qq_reg[4]),
        .I2(qq_reg[3]),
        .I3(qq_reg[0]),
        .I4(qq_reg[1]),
        .I5(qq_reg[2]),
        .O(dd[5]));
  LUT5 #(
    .INIT(32'hAAAAA8AA)) 
    \qq[6]_i_1 
       (.I0(\qq_reg[0]_0 ),
        .I1(\qq[6]_i_3_n_0 ),
        .I2(qq_reg[4]),
        .I3(qq_reg[3]),
        .I4(qq_reg[5]),
        .O(qq));
  LUT3 #(
    .INIT(8'h6A)) 
    \qq[6]_i_2 
       (.I0(qq_reg[6]),
        .I1(\qq[6]_i_4_n_0 ),
        .I2(qq_reg[5]),
        .O(dd[6]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    \qq[6]_i_3 
       (.I0(qq_reg[1]),
        .I1(qq_reg[0]),
        .I2(qq_reg[6]),
        .I3(qq_reg[2]),
        .O(\qq[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \qq[6]_i_4 
       (.I0(qq_reg[2]),
        .I1(qq_reg[1]),
        .I2(qq_reg[0]),
        .I3(qq_reg[3]),
        .I4(qq_reg[4]),
        .O(\qq[6]_i_4_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \qq_reg[0] 
       (.C(s00_axi_aclk),
        .CE(qq),
        .CLR(SR),
        .D(dd[0]),
        .Q(qq_reg[0]));
  FDCE #(
    .INIT(1'b0)) 
    \qq_reg[1] 
       (.C(s00_axi_aclk),
        .CE(qq),
        .CLR(SR),
        .D(dd[1]),
        .Q(qq_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \qq_reg[2] 
       (.C(s00_axi_aclk),
        .CE(qq),
        .CLR(SR),
        .D(dd[2]),
        .Q(qq_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \qq_reg[3] 
       (.C(s00_axi_aclk),
        .CE(qq),
        .CLR(SR),
        .D(dd[3]),
        .Q(qq_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \qq_reg[4] 
       (.C(s00_axi_aclk),
        .CE(qq),
        .CLR(SR),
        .D(dd[4]),
        .Q(qq_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \qq_reg[5] 
       (.C(s00_axi_aclk),
        .CE(qq),
        .CLR(SR),
        .D(dd[5]),
        .Q(qq_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \qq_reg[6] 
       (.C(s00_axi_aclk),
        .CE(qq),
        .CLR(SR),
        .D(dd[6]),
        .Q(qq_reg[6]));
endmodule

module fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_wstrb,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input [3:0]s00_axi_wstrb;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire tflite_mbqm_v1_0_S00_AXI_inst_n_4;

  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(S_AXI_AWREADY),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(tflite_mbqm_v1_0_S00_AXI_inst_n_4),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0_S00_AXI tflite_mbqm_v1_0_S00_AXI_inst
       (.aw_en_reg_0(tflite_mbqm_v1_0_S00_AXI_inst_n_4),
        .aw_en_reg_1(aw_en_i_1_n_0),
        .axi_arready_reg_0(S_AXI_ARREADY),
        .axi_awready_reg_0(S_AXI_AWREADY),
        .axi_bvalid_reg_0(axi_bvalid_i_1_n_0),
        .axi_rvalid_reg_0(axi_rvalid_i_1_n_0),
        .axi_wready_reg_0(S_AXI_WREADY),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm_v1_0_S00_AXI
   (axi_wready_reg_0,
    axi_awready_reg_0,
    axi_arready_reg_0,
    s00_axi_bvalid,
    aw_en_reg_0,
    s00_axi_rvalid,
    s00_axi_rdata,
    s00_axi_aclk,
    axi_bvalid_reg_0,
    aw_en_reg_1,
    axi_rvalid_reg_0,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn);
  output axi_wready_reg_0;
  output axi_awready_reg_0;
  output axi_arready_reg_0;
  output s00_axi_bvalid;
  output aw_en_reg_0;
  output s00_axi_rvalid;
  output [31:0]s00_axi_rdata;
  input s00_axi_aclk;
  input axi_bvalid_reg_0;
  input aw_en_reg_1;
  input axi_rvalid_reg_0;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;

  wire SM0_n_0;
  wire SM0_n_33;
  wire aw_en_reg_0;
  wire aw_en_reg_1;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire \axi_araddr[4]_i_1_n_0 ;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire \axi_awaddr[4]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_reg_0;
  wire axi_bvalid_reg_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_11_n_0 ;
  wire \axi_rdata[31]_i_15_n_0 ;
  wire \axi_rdata[31]_i_4_n_0 ;
  wire \axi_rdata[31]_i_9_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire axi_rvalid_reg_0;
  wire axi_wready0;
  wire axi_wready_reg_0;
  wire [2:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [2:0]sel0;
  wire [31:1]slv_reg0;
  wire [0:0]slv_reg0__0;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire [7:0]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [31:8]slv_reg4__0;
  wire slv_reg_rden;
  wire slv_reg_wren__2;
  wire [31:1]tf_out;

  fer_soc_bd_tflite_mbqm_0_0_tflite_mbqm SM0
       (.D(reg_data_out[0]),
        .Q(slv_reg3),
        .SR(SM0_n_33),
        .\axi_rdata[19]_i_5 (slv_reg4),
        .\axi_rdata[31]_i_8 (tf_out),
        .\axi_rdata_reg[0] (\axi_rdata[0]_i_2_n_0 ),
        .\axi_rdata_reg[31]_i_3 (\axi_rdata[31]_i_9_n_0 ),
        .\axi_rdata_reg[3]_i_2 (\axi_rdata[31]_i_15_n_0 ),
        .\axi_rdata_reg[3]_i_2_0 (\axi_rdata[31]_i_11_n_0 ),
        .\qq_reg[0]_0 (slv_reg0__0),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .sel0(sel0),
        .\sg_a_p0_reg[31] (slv_reg1),
        .\sg_a_p0_reg[31]_0 (slv_reg2),
        .\slv_reg4_reg[2] (SM0_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_reg_1),
        .Q(aw_en_reg_0),
        .S(SM0_n_33));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(axi_arready_reg_0),
        .I3(sel0[0]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(axi_arready_reg_0),
        .I3(sel0[1]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[4]_i_1 
       (.I0(s00_axi_araddr[2]),
        .I1(s00_axi_arvalid),
        .I2(axi_arready_reg_0),
        .I3(sel0[2]),
        .O(\axi_araddr[4]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(sel0[0]),
        .S(SM0_n_33));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(sel0[1]),
        .S(SM0_n_33));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[4]_i_1_n_0 ),
        .Q(sel0[2]),
        .S(SM0_n_33));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(axi_arready_reg_0),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(axi_arready_reg_0),
        .R(SM0_n_33));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(aw_en_reg_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(axi_awready_reg_0),
        .I5(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(aw_en_reg_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(axi_awready_reg_0),
        .I5(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[4]_i_1 
       (.I0(s00_axi_awaddr[2]),
        .I1(aw_en_reg_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(axi_awready_reg_0),
        .I5(p_0_in[2]),
        .O(\axi_awaddr[4]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(SM0_n_33));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(SM0_n_33));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[4]_i_1_n_0 ),
        .Q(p_0_in[2]),
        .R(SM0_n_33));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(axi_awready_reg_0),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(axi_awready_reg_0),
        .R(SM0_n_33));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_reg_0),
        .Q(s00_axi_bvalid),
        .R(SM0_n_33));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(slv_reg3[0]),
        .I1(slv_reg2[0]),
        .I2(sel0[1]),
        .I3(slv_reg1[0]),
        .I4(sel0[0]),
        .I5(slv_reg0__0),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[10]_i_1 
       (.I0(slv_reg4__0[10]),
        .I1(sel0[0]),
        .I2(tf_out[10]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[10]_i_2_n_0 ),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_2 
       (.I0(slv_reg3[10]),
        .I1(slv_reg2[10]),
        .I2(sel0[1]),
        .I3(slv_reg1[10]),
        .I4(sel0[0]),
        .I5(slv_reg0[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[11]_i_1 
       (.I0(slv_reg4__0[11]),
        .I1(sel0[0]),
        .I2(tf_out[11]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[11]_i_3_n_0 ),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_3 
       (.I0(slv_reg3[11]),
        .I1(slv_reg2[11]),
        .I2(sel0[1]),
        .I3(slv_reg1[11]),
        .I4(sel0[0]),
        .I5(slv_reg0[11]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[12]_i_1 
       (.I0(slv_reg4__0[12]),
        .I1(sel0[0]),
        .I2(tf_out[12]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[12]_i_2_n_0 ),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_2 
       (.I0(slv_reg3[12]),
        .I1(slv_reg2[12]),
        .I2(sel0[1]),
        .I3(slv_reg1[12]),
        .I4(sel0[0]),
        .I5(slv_reg0[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[13]_i_1 
       (.I0(slv_reg4__0[13]),
        .I1(sel0[0]),
        .I2(tf_out[13]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[13]_i_2_n_0 ),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_2 
       (.I0(slv_reg3[13]),
        .I1(slv_reg2[13]),
        .I2(sel0[1]),
        .I3(slv_reg1[13]),
        .I4(sel0[0]),
        .I5(slv_reg0[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[14]_i_1 
       (.I0(slv_reg4__0[14]),
        .I1(sel0[0]),
        .I2(tf_out[14]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[14]_i_2_n_0 ),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_2 
       (.I0(slv_reg3[14]),
        .I1(slv_reg2[14]),
        .I2(sel0[1]),
        .I3(slv_reg1[14]),
        .I4(sel0[0]),
        .I5(slv_reg0[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[15]_i_1 
       (.I0(slv_reg4__0[15]),
        .I1(sel0[0]),
        .I2(tf_out[15]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[15]_i_3_n_0 ),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_3 
       (.I0(slv_reg3[15]),
        .I1(slv_reg2[15]),
        .I2(sel0[1]),
        .I3(slv_reg1[15]),
        .I4(sel0[0]),
        .I5(slv_reg0[15]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[16]_i_1 
       (.I0(slv_reg4__0[16]),
        .I1(sel0[0]),
        .I2(tf_out[16]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[16]_i_2_n_0 ),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(slv_reg3[16]),
        .I1(slv_reg2[16]),
        .I2(sel0[1]),
        .I3(slv_reg1[16]),
        .I4(sel0[0]),
        .I5(slv_reg0[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[17]_i_1 
       (.I0(slv_reg4__0[17]),
        .I1(sel0[0]),
        .I2(tf_out[17]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[17]_i_2_n_0 ),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(slv_reg3[17]),
        .I1(slv_reg2[17]),
        .I2(sel0[1]),
        .I3(slv_reg1[17]),
        .I4(sel0[0]),
        .I5(slv_reg0[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[18]_i_1 
       (.I0(slv_reg4__0[18]),
        .I1(sel0[0]),
        .I2(tf_out[18]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[18]_i_2_n_0 ),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_2 
       (.I0(slv_reg3[18]),
        .I1(slv_reg2[18]),
        .I2(sel0[1]),
        .I3(slv_reg1[18]),
        .I4(sel0[0]),
        .I5(slv_reg0[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[19]_i_1 
       (.I0(slv_reg4__0[19]),
        .I1(sel0[0]),
        .I2(tf_out[19]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[19]_i_3_n_0 ),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_3 
       (.I0(slv_reg3[19]),
        .I1(slv_reg2[19]),
        .I2(sel0[1]),
        .I3(slv_reg1[19]),
        .I4(sel0[0]),
        .I5(slv_reg0[19]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[1]_i_1 
       (.I0(slv_reg4[1]),
        .I1(sel0[0]),
        .I2(tf_out[1]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[1]_i_2_n_0 ),
        .O(reg_data_out[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_2 
       (.I0(slv_reg3[1]),
        .I1(slv_reg2[1]),
        .I2(sel0[1]),
        .I3(slv_reg1[1]),
        .I4(sel0[0]),
        .I5(slv_reg0[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[20]_i_1 
       (.I0(slv_reg4__0[20]),
        .I1(sel0[0]),
        .I2(tf_out[20]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[20]_i_2_n_0 ),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_2 
       (.I0(slv_reg3[20]),
        .I1(slv_reg2[20]),
        .I2(sel0[1]),
        .I3(slv_reg1[20]),
        .I4(sel0[0]),
        .I5(slv_reg0[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[21]_i_1 
       (.I0(slv_reg4__0[21]),
        .I1(sel0[0]),
        .I2(tf_out[21]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[21]_i_2_n_0 ),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(slv_reg3[21]),
        .I1(slv_reg2[21]),
        .I2(sel0[1]),
        .I3(slv_reg1[21]),
        .I4(sel0[0]),
        .I5(slv_reg0[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[22]_i_1 
       (.I0(slv_reg4__0[22]),
        .I1(sel0[0]),
        .I2(tf_out[22]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[22]_i_2_n_0 ),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_2 
       (.I0(slv_reg3[22]),
        .I1(slv_reg2[22]),
        .I2(sel0[1]),
        .I3(slv_reg1[22]),
        .I4(sel0[0]),
        .I5(slv_reg0[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[23]_i_1 
       (.I0(slv_reg4__0[23]),
        .I1(sel0[0]),
        .I2(tf_out[23]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[23]_i_3_n_0 ),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_3 
       (.I0(slv_reg3[23]),
        .I1(slv_reg2[23]),
        .I2(sel0[1]),
        .I3(slv_reg1[23]),
        .I4(sel0[0]),
        .I5(slv_reg0[23]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[24]_i_1 
       (.I0(slv_reg4__0[24]),
        .I1(sel0[0]),
        .I2(tf_out[24]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[24]_i_2_n_0 ),
        .O(reg_data_out[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(slv_reg3[24]),
        .I1(slv_reg2[24]),
        .I2(sel0[1]),
        .I3(slv_reg1[24]),
        .I4(sel0[0]),
        .I5(slv_reg0[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[25]_i_1 
       (.I0(slv_reg4__0[25]),
        .I1(sel0[0]),
        .I2(tf_out[25]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[25]_i_2_n_0 ),
        .O(reg_data_out[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(slv_reg3[25]),
        .I1(slv_reg2[25]),
        .I2(sel0[1]),
        .I3(slv_reg1[25]),
        .I4(sel0[0]),
        .I5(slv_reg0[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[26]_i_1 
       (.I0(slv_reg4__0[26]),
        .I1(sel0[0]),
        .I2(tf_out[26]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[26]_i_2_n_0 ),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(slv_reg3[26]),
        .I1(slv_reg2[26]),
        .I2(sel0[1]),
        .I3(slv_reg1[26]),
        .I4(sel0[0]),
        .I5(slv_reg0[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[27]_i_1 
       (.I0(slv_reg4__0[27]),
        .I1(sel0[0]),
        .I2(tf_out[27]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[27]_i_3_n_0 ),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_3 
       (.I0(slv_reg3[27]),
        .I1(slv_reg2[27]),
        .I2(sel0[1]),
        .I3(slv_reg1[27]),
        .I4(sel0[0]),
        .I5(slv_reg0[27]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[28]_i_1 
       (.I0(slv_reg4__0[28]),
        .I1(sel0[0]),
        .I2(tf_out[28]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[28]_i_2_n_0 ),
        .O(reg_data_out[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(slv_reg3[28]),
        .I1(slv_reg2[28]),
        .I2(sel0[1]),
        .I3(slv_reg1[28]),
        .I4(sel0[0]),
        .I5(slv_reg0[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[29]_i_1 
       (.I0(slv_reg4__0[29]),
        .I1(sel0[0]),
        .I2(tf_out[29]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[29]_i_2_n_0 ),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(slv_reg3[29]),
        .I1(slv_reg2[29]),
        .I2(sel0[1]),
        .I3(slv_reg1[29]),
        .I4(sel0[0]),
        .I5(slv_reg0[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[2]_i_1 
       (.I0(slv_reg4[2]),
        .I1(sel0[0]),
        .I2(tf_out[2]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[2]_i_2_n_0 ),
        .O(reg_data_out[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_2 
       (.I0(slv_reg3[2]),
        .I1(slv_reg2[2]),
        .I2(sel0[1]),
        .I3(slv_reg1[2]),
        .I4(sel0[0]),
        .I5(slv_reg0[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[30]_i_1 
       (.I0(slv_reg4__0[30]),
        .I1(sel0[0]),
        .I2(tf_out[30]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[30]_i_2_n_0 ),
        .O(reg_data_out[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(slv_reg3[30]),
        .I1(slv_reg2[30]),
        .I2(sel0[1]),
        .I3(slv_reg1[30]),
        .I4(sel0[0]),
        .I5(slv_reg0[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(axi_arready_reg_0),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h33733133)) 
    \axi_rdata[31]_i_11 
       (.I0(slv_reg4[0]),
        .I1(slv_reg4[7]),
        .I2(slv_reg4[6]),
        .I3(SM0_n_0),
        .I4(slv_reg4[5]),
        .O(\axi_rdata[31]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h00800200)) 
    \axi_rdata[31]_i_15 
       (.I0(slv_reg4[0]),
        .I1(slv_reg4[7]),
        .I2(slv_reg4[6]),
        .I3(SM0_n_0),
        .I4(slv_reg4[5]),
        .O(\axi_rdata[31]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[31]_i_2 
       (.I0(slv_reg4__0[31]),
        .I1(sel0[0]),
        .I2(tf_out[31]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[31]_i_4_n_0 ),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_4 
       (.I0(slv_reg3[31]),
        .I1(slv_reg2[31]),
        .I2(sel0[1]),
        .I3(slv_reg1[31]),
        .I4(sel0[0]),
        .I5(slv_reg0[31]),
        .O(\axi_rdata[31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFDF)) 
    \axi_rdata[31]_i_9 
       (.I0(slv_reg4[7]),
        .I1(slv_reg4[6]),
        .I2(SM0_n_0),
        .I3(slv_reg4[5]),
        .O(\axi_rdata[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[3]_i_1 
       (.I0(slv_reg4[3]),
        .I1(sel0[0]),
        .I2(tf_out[3]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[3]_i_3_n_0 ),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_3 
       (.I0(slv_reg3[3]),
        .I1(slv_reg2[3]),
        .I2(sel0[1]),
        .I3(slv_reg1[3]),
        .I4(sel0[0]),
        .I5(slv_reg0[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[4]_i_1 
       (.I0(slv_reg4[4]),
        .I1(sel0[0]),
        .I2(tf_out[4]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[4]_i_2_n_0 ),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(slv_reg3[4]),
        .I1(slv_reg2[4]),
        .I2(sel0[1]),
        .I3(slv_reg1[4]),
        .I4(sel0[0]),
        .I5(slv_reg0[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[5]_i_1 
       (.I0(slv_reg4[5]),
        .I1(sel0[0]),
        .I2(tf_out[5]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[5]_i_2_n_0 ),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(slv_reg3[5]),
        .I1(slv_reg2[5]),
        .I2(sel0[1]),
        .I3(slv_reg1[5]),
        .I4(sel0[0]),
        .I5(slv_reg0[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[6]_i_1 
       (.I0(slv_reg4[6]),
        .I1(sel0[0]),
        .I2(tf_out[6]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[6]_i_2_n_0 ),
        .O(reg_data_out[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(slv_reg3[6]),
        .I1(slv_reg2[6]),
        .I2(sel0[1]),
        .I3(slv_reg1[6]),
        .I4(sel0[0]),
        .I5(slv_reg0[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[7]_i_1 
       (.I0(slv_reg4[7]),
        .I1(sel0[0]),
        .I2(tf_out[7]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[7]_i_3_n_0 ),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_3 
       (.I0(slv_reg3[7]),
        .I1(slv_reg2[7]),
        .I2(sel0[1]),
        .I3(slv_reg1[7]),
        .I4(sel0[0]),
        .I5(slv_reg0[7]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[8]_i_1 
       (.I0(slv_reg4__0[8]),
        .I1(sel0[0]),
        .I2(tf_out[8]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[8]_i_2_n_0 ),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_2 
       (.I0(slv_reg3[8]),
        .I1(slv_reg2[8]),
        .I2(sel0[1]),
        .I3(slv_reg1[8]),
        .I4(sel0[0]),
        .I5(slv_reg0[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[9]_i_1 
       (.I0(slv_reg4__0[9]),
        .I1(sel0[0]),
        .I2(tf_out[9]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(\axi_rdata[9]_i_2_n_0 ),
        .O(reg_data_out[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_2 
       (.I0(slv_reg3[9]),
        .I1(slv_reg2[9]),
        .I2(sel0[1]),
        .I3(slv_reg1[9]),
        .I4(sel0[0]),
        .I5(slv_reg0[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(SM0_n_33));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(SM0_n_33));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_reg_0),
        .Q(s00_axi_rvalid),
        .R(SM0_n_33));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(axi_wready_reg_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(axi_wready_reg_0),
        .R(SM0_n_33));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg0[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(axi_awready_reg_0),
        .I2(axi_wready_reg_0),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg0__0),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg0[10]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg0[11]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg0[12]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg0[13]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg0[14]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg0[15]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg0[16]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg0[17]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg0[18]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg0[19]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg0[1]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg0[20]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg0[21]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg0[22]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg0[23]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg0[24]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg0[25]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg0[26]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg0[27]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg0[28]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg0[29]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg0[2]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg0[30]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg0[31]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg0[3]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg0[4]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg0[5]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg0[6]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg0[7]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg0[8]),
        .R(SM0_n_33));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg0[9]),
        .R(SM0_n_33));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(SM0_n_33));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(SM0_n_33));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(SM0_n_33));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(SM0_n_33));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(p_0_in[0]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(p_0_in[0]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(p_0_in[0]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(p_0_in[0]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(SM0_n_33));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(SM0_n_33));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg4[0]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4__0[10]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4__0[11]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4__0[12]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4__0[13]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4__0[14]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4__0[15]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4__0[16]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4__0[17]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4__0[18]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4__0[19]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg4[1]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4__0[20]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4__0[21]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4__0[22]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4__0[23]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4__0[24]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4__0[25]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4__0[26]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4__0[27]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4__0[28]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4__0[29]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg4[2]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4__0[30]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4__0[31]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg4[3]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg4[4]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg4[5]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4__0[8]),
        .R(SM0_n_33));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4__0[9]),
        .R(SM0_n_33));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
