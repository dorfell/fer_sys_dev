

## LEDs
set_property -dict { PACKAGE_PIN M14   IOSTANDARD LVCMOS33 } [get_ports { gpio_0_tri_o[0] }]; #IO_L23P_T3_35 Sch=led[0]
set_property -dict { PACKAGE_PIN M15   IOSTANDARD LVCMOS33 } [get_ports { gpio_0_tri_o[1] }]; #IO_L23N_T3_35 Sch=led[1]
set_property -dict { PACKAGE_PIN G14   IOSTANDARD LVCMOS33 } [get_ports { gpio_0_tri_o[2] }]; #IO_0_35 Sch=led[2]
set_property -dict { PACKAGE_PIN D18   IOSTANDARD LVCMOS33 } [get_ports { gpio_0_tri_o[3] }]; #IO_L3N_T0_DQS_AD1N_35 Sch=led[3]
set_output_delay -min 1.000 [get_ports gpio_0_tri_o[*] ];
set_output_delay -max 2.000 [get_ports gpio_0_tri_o[*] ];


## Switches
set_property -dict { PACKAGE_PIN G15   IOSTANDARD LVCMOS33 } [get_ports { gpio_1_tri_i[0] }]; #IO_L19N_T3_VREF_35 Sch=sw[0]
set_property -dict { PACKAGE_PIN P15   IOSTANDARD LVCMOS33 } [get_ports { gpio_1_tri_i[1] }]; #IO_L24P_T3_34 Sch=sw[1]
set_property -dict { PACKAGE_PIN W13   IOSTANDARD LVCMOS33 } [get_ports { gpio_1_tri_i[2] }]; #IO_L4N_T0_34 Sch=sw[2]
set_property -dict { PACKAGE_PIN T16   IOSTANDARD LVCMOS33 } [get_ports { gpio_1_tri_i[3] }]; #IO_L9P_T1_DQS_34 Sch=sw[3]
set_input_delay -min 1.000 [get_ports gpio_1_tri_i[*] ];
set_input_delay -max 2.000 [get_ports gpio_1_tri_i[*] ];


## RGB LED 5 
set_property -dict { PACKAGE_PIN Y11   IOSTANDARD LVCMOS33 } [get_ports { gpio_2_tri_o[0] }]; #IO_L18N_T2_13 Sch=led5_r
set_property -dict { PACKAGE_PIN T5    IOSTANDARD LVCMOS33 } [get_ports { gpio_2_tri_o[1] }]; #IO_L19P_T3_13 Sch=led5_g
set_property -dict { PACKAGE_PIN Y12   IOSTANDARD LVCMOS33 } [get_ports { gpio_2_tri_o[2] }]; #IO_L20P_T3_13 Sch=led5_b
set_output_delay -min 1.000 [get_ports gpio_2_tri_o[*] ];
set_output_delay -max 2.000 [get_ports gpio_2_tri_o[*] ];


## RGB LED 6
set_property -dict { PACKAGE_PIN V16   IOSTANDARD LVCMOS33 } [get_ports { gpio_2_tri_o[3] }]; #IO_L18P_T2_34 Sch=led6_r
set_property -dict { PACKAGE_PIN F17   IOSTANDARD LVCMOS33 } [get_ports { gpio_2_tri_o[4] }]; #IO_L6N_T0_VREF_35 Sch=led6_g
set_property -dict { PACKAGE_PIN M17   IOSTANDARD LVCMOS33 } [get_ports { gpio_2_tri_o[5] }]; #IO_L8P_T1_AD10P_35 Sch=led6_b


## Buttons
set_property -dict { PACKAGE_PIN K18   IOSTANDARD LVCMOS33 } [get_ports { gpio_3_tri_i[0] }]; #IO_L12N_T1_MRCC_35 Sch=btn[0]
set_property -dict { PACKAGE_PIN P16   IOSTANDARD LVCMOS33 } [get_ports { gpio_3_tri_i[1] }]; #IO_L24N_T3_34 Sch=btn[1]
set_property -dict { PACKAGE_PIN K19   IOSTANDARD LVCMOS33 } [get_ports { gpio_3_tri_i[2] }]; #IO_L10P_T1_AD11P_35 Sch=btn[2]
set_property -dict { PACKAGE_PIN Y16   IOSTANDARD LVCMOS33 } [get_ports { gpio_3_tri_i[3] }]; #IO_L7P_T1_34 Sch=btn[3]
set_input_delay -min 1.000 [get_ports gpio_3_tri_i[*] ];
set_input_delay -max 2.000 [get_ports gpio_3_tri_i[*] ];


##Pmod Header JB (UART PL)
#set_property -dict { PACKAGE_PIN V8    IOSTANDARD LVCMOS33     } [get_ports { uart_0_txd }]; #IO_L15P_T2_DQS_13 Sch=jb_p[1]		 
#set_property -dict { PACKAGE_PIN W8    IOSTANDARD LVCMOS33     } [get_ports { uart_0_rxd }]; #IO_L15N_T2_DQS_13 Sch=jb_n[1]


# pwm
#set_property -dict { PACKAGE_PIN N15   IOSTANDARD LVCMOS33 } [get_ports { pls }]; #IO_L21P_T3_DQS_AD14P_35 Sch=JA1_R_p