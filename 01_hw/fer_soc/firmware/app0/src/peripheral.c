/*
---------------------------------------------------------------------
----                                                             ----
---- file:    peripheral.c                                       ----
---- brief:   Fonctions des périphériques                        ----
---- details: Fonctions pour utiliser les périphériques          ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/09/01                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------
*/
#include <stdio.h>
#include <stdlib.h>                                        // Malloc, free
#include "platform.h"
#include "xparameters.h"                                   // Information about AXI peripherals
#include "xgpiops.h"                                       // Processing system GPIO driver
#include "xgpio.h"                                         // AXI GPIO driver
#include "xil_printf.h"                                    // Data over UART
#include "xsdps.h"                                         // SD device driver
#include "ff.h"                                            // FAT file system library


#define delay_time 20000000
#define MIO07  7                                           // Processor led
#define MIO50 50                                           // Processor push button
#define MIO51 51                                           // Processor push button


XGpioPs MIO_GPIO;                                          // Structure pour PS  GPIO
XGpio   AXI_GPIO0;                                         // Structure pour AXI GPIO 0
XGpio   AXI_GPIO1;                                         // Structure pour AXI GPIO 1


static FIL   fil;		                                   // File object
static FATFS fatfs;                                        // FAT file system




/*** Fonction delay ***/
void delay(int time){
  int i;
  for (i = 0; i < time; i++); };




/**********************************************************/
/*** fonctions pour la carte microSD card ***/
/**********************************************************/

/*** Fonction pour lire paramètres de couche dès carte microSD ***/
int ReadParamSD(char FileName[32], uint32_t FileSize, int ParamShape[2], int param[ParamShape[0]][ParamShape[1]]){


  char *SD_File;
  u8 DestinationAddress[FileSize] __attribute__ ((aligned(32)));

  FRESULT Res;
  UINT    NumBytesRead;


  /*
   * To test logical drive 0, Path should be "0:/"
   * For logical drive 1, Path should be "1:/"
   */
  TCHAR *Path = "0:/";


  /*
   * Register volume work area, initialize device
   */
  Res = f_mount(&fatfs, Path, 0);
  if (Res != FR_OK){
    return XST_FAILURE; };
  //xil_printf("OK!");


  /*
   * Open file with required permissions.
   * Here - Creating new file with read/write permissions. .
   * To open file with write permissions, file system should not
   * be in Read Only mode.
   */
  SD_File = (char *)FileName;
  Res = f_open(&fil, SD_File, FA_READ);
  if (Res){
    return XST_FAILURE;	};
  //xil_printf("OK!");


  /*
   * Pointer to beginning of file .
   */
  Res = f_lseek(&fil, 0);
  if (Res){
    return XST_FAILURE; };
  //xil_printf("OK!");


  /*
   * Read data from file.
   */
  Res = f_read(&fil, (void*)DestinationAddress, FileSize, &NumBytesRead);
  if (Res){
    return XST_FAILURE;	};
  //xil_printf("OK!");


  /*
   * Créer le matrix
   */
  int idx =  0;
  for(int i=0; i<ParamShape[0]; i++){
    for(int j=0; j<ParamShape[1]; j++){
      param[i][j] = (DestinationAddress[0x83+idx*8]<<24) | (DestinationAddress[0x82+idx*8]<<16) | (DestinationAddress[0x81+idx*8]<<8) | (DestinationAddress[0x80+idx*8]);
      //xil_printf("valeur: %d \n", param[i][j] );
      idx = idx + 1;
    };
  };


  /*
   * Close file.
   */
  Res = f_close(&fil);
  if (Res){
    return XST_FAILURE;	};

  return XST_SUCCESS;
};





/*** Fonction pour lire tenseurs dès carte microSD ***/
int ReadTensorSD(char FileName[32], uint32_t FileSize, int TensorShape[4], int tensor[TensorShape[0]][TensorShape[1]][TensorShape[2]][TensorShape[3]]){

  char *SD_File;
  u8 DestinationAddress[FileSize] __attribute__ ((aligned(32)));

  FRESULT Res;
  UINT    NumBytesRead;


  /*
   * To test logical drive 0, Path should be "0:/"
   * For logical drive 1, Path should be "1:/"
   */
  TCHAR *Path = "0:/";


  /*
   * Register volume work area, initialize device
   */
  Res = f_mount(&fatfs, Path, 0);
  if (Res != FR_OK){
    return XST_FAILURE; };
  //xil_printf("OK!");


  /*
   * Open file with required permissions.
   * Here - Creating new file with read/write permissions. .
   * To open file with write permissions, file system should not
   * be in Read Only mode.
   */
  SD_File = (char *)FileName;
  Res = f_open(&fil, SD_File, FA_READ);
  if (Res){
    return XST_FAILURE;	};
  //xil_printf("OK!");


  /*
   * Pointer to beginning of file .
   */
  Res = f_lseek(&fil, 0);
  if (Res){
    return XST_FAILURE; };
  //xil_printf("OK!");


  /*
   * Read data from file.
   */
  Res = f_read(&fil, (void*)DestinationAddress, FileSize, &NumBytesRead);
  if (Res){
    return XST_FAILURE;	};
  //xil_printf("OK!");


  /*
   * Créer le tenseur
   */
  int idx =  0;
  for(int i=0; i<TensorShape[0]; i++){
    for(int j=0; j<TensorShape[1]; j++){
      for(int k=0; k<TensorShape[2]; k++){
        for(int l=0; l<TensorShape[3]; l++){
          tensor[i][j][k][l] = (DestinationAddress[0x83+idx*8]<<24) | (DestinationAddress[0x82+idx*8]<<16) | (DestinationAddress[0x81+idx*8]<<8) | (DestinationAddress[0x80+idx*8]);
          //xil_printf("index, value: %x, %d \n", 0x80+idx*8, (int8_t) DestinationAddress[0x80+idx*8]);
          idx = idx + 1;
        };
      };
    };
  };


  /*
   * Close file.
   */
  Res = f_close(&fil);
  if (Res){
    return XST_FAILURE;	};

  return XST_SUCCESS;
};
/**********************************************************/




/**********************************************************/
/*** Fonctions pour les GPIOs et UART                   ***/
/**********************************************************/

/*** Fonction pour éprouver les périphérals ***/
int test_peripheral(void){

  int StatusPS;
  uint32_t bou0, bou1;
  uint32_t pl_sws, pl_bou;

  XGpioPs_Config *GPIOConfigPtrPS;                       // Pointer to the driver structure
  XGpio_Initialize(&AXI_GPIO0, 0);                       // Initialize AXI GPIO 0
  XGpio_Initialize(&AXI_GPIO1, 1);                       // Initialize AXI GPIO 1

  // GPIO PS initialization
  GPIOConfigPtrPS = XGpioPs_LookupConfig(XPAR_XGPIOPS_0_DEVICE_ID); // init pointer
  StatusPS = XGpioPs_CfgInitialize(&MIO_GPIO, GPIOConfigPtrPS, GPIOConfigPtrPS -> BaseAddr);
  if (StatusPS != XST_SUCCESS){
    print("GPIO INIT FAILED\n\r");
    return XST_FAILURE; };

  // Configurer led MIO 07
  XGpioPs_SetDirectionPin(&MIO_GPIO,    MIO07, 0x1);
  XGpioPs_SetOutputEnablePin(&MIO_GPIO, MIO07, 0x1);

  // Configurer bouton 0 -> MIO 50
  XGpioPs_SetDirectionPin(&MIO_GPIO, MIO50, 0x0);

  // Configurer bouton 1 -> MIO 51
  XGpioPs_SetDirectionPin(&MIO_GPIO, MIO51, 0x0);

  // set AXI GPIO 0 channel (1, 2) tristates to Output (0) or inputs (1)
  XGpio_SetDataDirection(&AXI_GPIO0, 1, 0x00000000); // leds
  XGpio_SetDataDirection(&AXI_GPIO0, 2, 0x0000000F); // sws

  // set AXI GPIO 1 channel (1, 2) tristates to Output (0) or inputs (1)
  XGpio_SetDataDirection(&AXI_GPIO1, 1, 0x00000000); // rgbs
  XGpio_SetDataDirection(&AXI_GPIO1, 2, 0x0000000F); // pushs

  // Répéter 2 fois
  for(int i=0; i<2; i++){

    //----- PS GPIO ----
    bou0 = XGpioPs_ReadPin(&MIO_GPIO, MIO50);
    bou1 = XGpioPs_ReadPin(&MIO_GPIO, MIO51);

    xil_printf("valeur de bou0: %lu. \n\r", bou0);
    xil_printf("valeur de bou1: %lu. \n\r", bou1);

    // Déclaration d' une porte logique OR
    if ( bou1 == 0 && bou0 == 0){
	  XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x0);}   // Éteindre PS led
      //print("Eteindre le  MIO07 led... \n\r");}
   	else if ( bou1 == 0 && bou0 == 1){
      XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x1);}   // Éteindre PS led
      //print("Allumer le  MIO07 led... \n\r");}
    else if ( bou1 == 1 && bou0 == 0){
      XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x1);}   // Éteindre PS led
	  //print("Allumer le  MIO07 led... \n\r");}
    else if ( bou1 == 1 && bou0 == 1){
	  XGpioPs_WritePin(&MIO_GPIO, MIO07, 0x1);}   // Éteindre PS led
	  //print("Allumer le MIO07 led... \n\r");}
	else{
      print("Ce n'est pas une option... \n\r");}

   	//----- AXI GPIO ----
    pl_sws = XGpio_DiscreteRead(&AXI_GPIO0, 2);     // Lire les interrupteurs
    pl_bou = XGpio_DiscreteRead(&AXI_GPIO1, 2);     // Lire les boutons

   	xil_printf("valeur de pl_sws: %lu. \n\r", pl_sws);
   	xil_printf("valeur de pl_bou << 3 : %lu. \n\r", pl_bou << 3);
   	xil_printf("valeur de pl_bou: %lu. \n\r", pl_bou);
    xil_printf("---------------------------------------------------\n\r");

   	// Allumer leds avec les interrupteurs
   	XGpio_DiscreteWrite(&AXI_GPIO0, 1, pl_sws);

   	// Allumer les rgbs avec les boutons
   	XGpio_DiscreteWrite(&AXI_GPIO1, 1, (pl_bou + (pl_bou << 3)));

   	delay(delay_time);

    };

    return 0;
};
/**********************************************************/
