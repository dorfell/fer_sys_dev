/*
---------------------------------------------------------------------
----                                                             ----
---- file:    cnnMnist.c                                         ----
---- brief:   CNN modèle for Mnist                               ----
---- details: Implementation du modèle cnn pour Mnist            ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2023/06/27                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2023 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------
*/


#include <stdio.h>
#include <stdlib.h>                                        // Malloc, free
#include "platform.h"
#include "xparameters.h"                                   // Information about AXI peripherals
#include "xil_printf.h"                                    // Data over UART
#include "xsdps.h"                                         // SD device driver
#include "ff.h"                                            // FAT file system library

#include "accel.h"
#include "peripheral.h"




/*
  Global variables for CNN
*/


/******************************************************/
/* Test images	                                      */
/******************************************************/
int  Status1;

char In01Path[32] = "cnn/img/img1.npy";
int  In01Size     = 6400;                                   // File size in bytes
int  In01Shape[4] = {1, 28, 28, 1};
int  In01Tensor[1][28][28][1];




/******************************************************/
/* Tableaux des filtres et paramètres.                */
/******************************************************/
/*** Couche 1 ***/
char Fil01Path[32] = "cnn/cv1/fil.npy ";
int  Fil01Size     = 1128;                                  // File size in bytes
int  Fil01Shape[4] = {5, 5, 5, 1};
int  Fil01Tensor[5][5][5][1];

char Par01Path[32] = "cnn/cv1/par.npy";
int  Par01Size     =  288;                                  // File size in bytes
int  Par01Shape[2] = {4, 5};
int  Par01Matrix[4][5];




/*** Couche 2 ***/
// Pour couche flatten il n'y pas parametres


/*** Couche 3 ***/
char Fil03Path[32] = "cnn/ds1/fil.npy ";
int  Fil03Size     = 78528;                                 // File size in bytes
int  Fil03Shape[2] = {10, 980};
int  Fil03Matrix[10][980];

char Par03Path[32] = "cnn/ds1/par.npy";
int  Par03Size     = 448;                                   // File size in bytes
int  Par03Shape[2] = {4, 10};
int  Par03Matrix[4][10];




/*** Initializing Modèle cnn ***/
int init_cnn(void){

  /*** Lire img1   ***/
  Status1 = ReadTensorSD(In01Path, In01Size, In01Shape, In01Tensor);
  if (Status1 != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};


  /******************************************************/
  /* Lire filtres et paramètres                         */
  /******************************************************/
  /*** Couche 1 ***/
  Status1 = ReadTensorSD(Fil01Path, Fil01Size, Fil01Shape, Fil01Tensor);
  if (Status1 != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};

  Status1 = ReadParamSD(Par01Path, Par01Size, Par01Shape, Par01Matrix);
  if (Status1 != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};


  /*** Couche 2 ***/
  // Pour couche flatten il n'y pas parametres


  /*** Couche 3 ***/
  Status1 = ReadParamSD(Fil03Path, Fil03Size, Fil03Shape, Fil03Matrix);
  if (Status1 != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};

  Status1 = ReadParamSD(Par03Path, Par03Size, Par03Shape, Par03Matrix);
  if (Status1 != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};

  xil_printf("Successfully Read SD Files \r\n");
  return 0;
};




/*** CNN inference */
int cnn_inference(int InShape[4], int InTensor[InShape[0]][InShape[1]][InShape[2]][InShape[3]]){

  xil_printf("\n Running CNN inference... \n");

  /******************************************************/
  /* Couche 1:                                          */
  /*                                                    */
  /*    ----------------------------------------------  */
  /*   | Objectif         | Fonction | Sortie         | */
  /*    ----------------------------------------------  */
  /*   | Padding SAME     | padding  |( 1, 28, 28,  1)| */
  /*   | Filtre           |          |( 5,  5,  5,  1)| */
  /*   | Convolution (5,5)| conv_k5  |( 1, 28, 28,  5)| */
  /*   | MaxPooling  (2,2)| maxp_22  |( 1, 14, 14,  5)| */
  /*    ----------------------------------------------  */
  /******************************************************/

  /*** Padding ***/
  int  Pad01Shape[4] = {1, InShape[1]+4, InShape[2]+4, 1};
  int  Pad01Tensor[Pad01Shape[0]][Pad01Shape[1]][Pad01Shape[2]][Pad01Shape[3]];
  int  zero_point = -Par01Matrix[3][0];                      // zero_point = -offset_ent
  //xil_printf("zero_point: %d \n", zero_point);
  padding(InShape, InTensor, Pad01Shape, Pad01Tensor, zero_point);

  /*** Convolution ***/
  int  Cnv01Shape[4] = {1, Pad01Shape[1]-4, Pad01Shape[2]-4, Fil01Shape[0]};
  int  Cnv01Tensor[Cnv01Shape[0]][Cnv01Shape[1]][Cnv01Shape[2]][Cnv01Shape[3]];
  memset( Cnv01Tensor, 0, Cnv01Shape[0]*Cnv01Shape[1]*Cnv01Shape[2]*Cnv01Shape[3]*sizeof(int) );
  conv_k5(Pad01Shape, Pad01Tensor, Fil01Shape, Fil01Tensor, Par01Shape, Par01Matrix, Cnv01Shape, Cnv01Tensor);

  /*** MaxPooling ***/
  int  Mxp01Shape[4] = {1, Cnv01Shape[1]/2, Cnv01Shape[2]/2, Fil01Shape[0]};
  int  Mxp01Tensor[Mxp01Shape[0]][Mxp01Shape[1]][Mxp01Shape[2]][Mxp01Shape[3]];
  maxp_22(Cnv01Shape, Cnv01Tensor, Mxp01Shape, Mxp01Tensor);
  xil_printf("*** Couche 1: Pad + Cnv + MxP => Fini!, Done! ***\n");



  /*******************************************************/
  /* Couche 2:                                           */
  /*                                                     */
  /*    -----------------------------------------------  */
  /*   | Objectif         | Fonction | Sortie          | */
  /*    -----------------------------------------------  */
  /*   | Entree           |          |(  1, 14, 14, 5) | */
  /*   | Flatten          | Flatten  |  1*14*14*5 = 980| */
  /*    -----------------------------------------------  */
  /*******************************************************/
  int  Flt0Shape[1] = {1*14*14*5};
  int  Flt0Vector[1*14*14*5];
  memset( Flt0Vector, 0, Flt0Shape[0]*sizeof(int) );
  flatten(Mxp01Shape, Mxp01Tensor, Flt0Shape, Flt0Vector);
  xil_printf("*** Couche 2: Flatten => Fini!, Done! ***\n");



  /**************************************************/
  /* Couche 3:                                      */
  /*                                                */
  /*    ------------------------------------------  */
  /*   | Objectif         | Fonction | Sortie     | */
  /*    ------------------------------------------  */
  /*   | Entree           |          |(  1, 980)  | */
  /*   | Filtre           |          |(  5, 980)  | */
  /*   | Dense            | dense    |( 10,   5)  | */
  /*    ------------------------------------------  */
  /**************************************************/
  int  Dns01Shape[1] = {Fil03Shape[0]};
  int  Dns01Vector[Dns01Shape[0]];
  memset( Dns01Vector, 0, Dns01Shape[0]*sizeof(int) );
  dense(Flt0Shape, Flt0Vector, Fil03Shape, Fil03Matrix, Par03Shape, Par03Matrix, Dns01Shape, Dns01Vector);
  xil_printf("*** Couche 3: Dense => Fini!, Done! ***\n");

  /*** Dns01Vector ***/
  xil_printf("\n Vecteur de classification: \n");
  for(int f=0; f<10; f++){
    xil_printf("%d, ", Dns01Vector[f]);
  };
  xil_printf(" \n ");


  /**************************************************/
  /* Classification:                                */
  /**************************************************/

  char label[10][1] = {{"0"}, {"1"}, {"2"}, {"3"}, {"4"}, {"5"}, {"6"}, {"7"}, {"8"}, {"9"}};
  int idx = 0;                                             // Index de la classification

  if ( (Dns01Vector[0] > Dns01Vector[1]) && (Dns01Vector[0] > Dns01Vector[2]) && (Dns01Vector[0] > Dns01Vector[3]) &&
       (Dns01Vector[0] > Dns01Vector[4]) && (Dns01Vector[0] > Dns01Vector[5]) && (Dns01Vector[0] > Dns01Vector[6]) &&
       (Dns01Vector[0] > Dns01Vector[7]) && (Dns01Vector[0] > Dns01Vector[8]) && (Dns01Vector[0] > Dns01Vector[9]) ){
    idx = 0;  }
  else if ((Dns01Vector[1] > Dns01Vector[0]) && (Dns01Vector[1] > Dns01Vector[2]) && (Dns01Vector[1] > Dns01Vector[3]) &&
           (Dns01Vector[1] > Dns01Vector[4]) && (Dns01Vector[1] > Dns01Vector[5]) && (Dns01Vector[1] > Dns01Vector[6]) &&
           (Dns01Vector[1] > Dns01Vector[7]) && (Dns01Vector[1] > Dns01Vector[8]) && (Dns01Vector[1] > Dns01Vector[9]) ){
    idx = 1;  }
  else if ((Dns01Vector[2] > Dns01Vector[0]) && (Dns01Vector[2] > Dns01Vector[1]) && (Dns01Vector[2] > Dns01Vector[3]) &&
           (Dns01Vector[2] > Dns01Vector[4]) && (Dns01Vector[2] > Dns01Vector[5]) && (Dns01Vector[2] > Dns01Vector[6]) &&
           (Dns01Vector[2] > Dns01Vector[7]) && (Dns01Vector[2] > Dns01Vector[8]) && (Dns01Vector[2] > Dns01Vector[9]) ){
    idx = 2;  }
  else if ((Dns01Vector[3] > Dns01Vector[0]) && (Dns01Vector[3] > Dns01Vector[1]) && (Dns01Vector[3] > Dns01Vector[2]) &&
           (Dns01Vector[3] > Dns01Vector[4]) && (Dns01Vector[3] > Dns01Vector[5]) && (Dns01Vector[3] > Dns01Vector[6]) &&
           (Dns01Vector[3] > Dns01Vector[7]) && (Dns01Vector[3] > Dns01Vector[8]) && (Dns01Vector[3] > Dns01Vector[9]) ){
    idx = 3;  }
  else if ((Dns01Vector[4] > Dns01Vector[0]) && (Dns01Vector[4] > Dns01Vector[1]) && (Dns01Vector[4] > Dns01Vector[2]) &&
           (Dns01Vector[4] > Dns01Vector[3]) && (Dns01Vector[4] > Dns01Vector[5]) && (Dns01Vector[4] > Dns01Vector[6]) &&
           (Dns01Vector[4] > Dns01Vector[7]) && (Dns01Vector[4] > Dns01Vector[8]) && (Dns01Vector[4] > Dns01Vector[9]) ){
    idx = 4;  }
  else if ((Dns01Vector[5] > Dns01Vector[0]) && (Dns01Vector[5] > Dns01Vector[1]) && (Dns01Vector[5] > Dns01Vector[2]) &&
           (Dns01Vector[5] > Dns01Vector[3]) && (Dns01Vector[5] > Dns01Vector[4]) && (Dns01Vector[5] > Dns01Vector[6]) &&
           (Dns01Vector[5] > Dns01Vector[7]) && (Dns01Vector[5] > Dns01Vector[8]) && (Dns01Vector[5] > Dns01Vector[9]) ){
    idx = 5;  }
  else if ((Dns01Vector[6] > Dns01Vector[0]) && (Dns01Vector[6] > Dns01Vector[1]) && (Dns01Vector[6] > Dns01Vector[2]) &&
           (Dns01Vector[6] > Dns01Vector[3]) && (Dns01Vector[6] > Dns01Vector[4]) && (Dns01Vector[6] > Dns01Vector[5]) &&
           (Dns01Vector[6] > Dns01Vector[7]) && (Dns01Vector[6] > Dns01Vector[8]) && (Dns01Vector[6] > Dns01Vector[9]) ){
    idx = 6;  }
  else if ((Dns01Vector[7] > Dns01Vector[0]) && (Dns01Vector[7] > Dns01Vector[1]) && (Dns01Vector[7] > Dns01Vector[2]) &&
           (Dns01Vector[7] > Dns01Vector[3]) && (Dns01Vector[7] > Dns01Vector[4]) && (Dns01Vector[7] > Dns01Vector[5]) &&
           (Dns01Vector[7] > Dns01Vector[6]) && (Dns01Vector[7] > Dns01Vector[8]) && (Dns01Vector[7] > Dns01Vector[9]) ){
    idx = 7;  }
  else if ((Dns01Vector[8] > Dns01Vector[0]) && (Dns01Vector[8] > Dns01Vector[1]) && (Dns01Vector[8] > Dns01Vector[2]) &&
           (Dns01Vector[8] > Dns01Vector[3]) && (Dns01Vector[8] > Dns01Vector[4]) && (Dns01Vector[8] > Dns01Vector[5]) &&
           (Dns01Vector[8] > Dns01Vector[6]) && (Dns01Vector[8] > Dns01Vector[7]) && (Dns01Vector[8] > Dns01Vector[9]) ){
    idx = 8;  }
  else if ((Dns01Vector[9] > Dns01Vector[0]) && (Dns01Vector[9] > Dns01Vector[1]) && (Dns01Vector[9] > Dns01Vector[2]) &&
           (Dns01Vector[9] > Dns01Vector[3]) && (Dns01Vector[9] > Dns01Vector[4]) && (Dns01Vector[9] > Dns01Vector[5]) &&
           (Dns01Vector[9] > Dns01Vector[6]) && (Dns01Vector[9] > Dns01Vector[7]) && (Dns01Vector[9] > Dns01Vector[8]) ){
    idx = 9;  }
  else { idx = 0; };


  xil_printf("\n Mnist Classification: \n");
  xil_printf("%c", label[idx][0]);


  xil_printf(" \n *** End CNN inference *** \n\r");
  xil_printf(" \n ");

  return 0;

};
