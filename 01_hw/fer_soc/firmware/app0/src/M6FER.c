/*
---------------------------------------------------------------------
----                                                             ----
---- file:    M6FER.c                                            ----
---- brief:   Modèle FER M6                                      ----
---- details: Implementation du modèle M6          r             ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/10/19                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------
*/


#include <stdio.h>
#include <stdlib.h>                                        // Malloc, free
#include "platform.h"
#include "xparameters.h"                                   // Information about AXI peripherals
#include "xil_printf.h"                                    // Data over UART
#include "xsdps.h"                                         // SD device driver
#include "ff.h"                                            // FAT file system library

#include "accel.h"
#include "peripheral.h"




/*
  Global variables for M6
*/


/******************************************************/
/* Test images	                                      */
/******************************************************/
int  Status;

char In1Path[32] = "img/img1.npy";
int  In1Size     = 32896;                                  // File size in bytes
int  In1Shape[4] = {1, 64, 64, 1};
int  In1Tensor[1][64][64][1];




/******************************************************/
/* Tableaux des filtres et paramètres.                */
/******************************************************/
/*** Couche 1 ***/
char Fil1Path[32] = "M6/cv1/fil.npy ";
int  Fil1Size     = 6528;                                  // File size in bytes
int  Fil1Shape[4] = {32, 5, 5, 1};
int  Fil1Tensor[32][5][5][1];

char Par1Path[32] = "M6/cv1/par.npy";
int  Par1Size     = 1152;                                  // File size in bytes
int  Par1Shape[2] = {4, 32};
int  Par1Matrix[4][32];


/*** Couche 2 ***/
char Fil2Path[32] = "M6/cv2/fil.npy ";
int  Fil2Size     = 409728;                                // File size in bytes
int  Fil2Shape[4] = {64, 5, 5, 32};
int  Fil2Tensor[64][5][5][32];

char Par2Path[32] = "M6/cv2/par.npy";
int  Par2Size     = 2176;                                  // File size in bytes
int  Par2Shape[2] = {4, 64};
int  Par2Matrix[4][64];


/*** Couche 3 ***/
char Fil3Path[32] = "M6/cv3/fil.npy ";
int  Fil3Size     = 1638528;                               // File size in bytes
int  Fil3Shape[4] = {128, 5, 5, 64};
int  Fil3Tensor[128][5][5][64];

char Par3Path[32] = "M6/cv3/par.npy";
int  Par3Size     = 4224;                                  // File size in bytes
int  Par3Shape[2] = {4, 128};
int  Par3Matrix[4][128];


/*** Couche 4 ***/
// Pour couche flatten il n'y pas parametres


/*** Couche 5 ***/
char Fil5Path[32] = "M6/ds1/fil.npy ";
int  Fil5Size     = 393344;                                // File size in bytes
int  Fil5Shape[2] = {6, 8192};
int  Fil5Matrix[6][8192];

char Par5Path[32] = "M6/ds1/par.npy";
int  Par5Size     = 320;                                   // File size in bytes
int  Par5Shape[2] = {4, 6};
int  Par5Matrix[4][6];




/*** Initializing Modèle M6 ***/
int init_M6(void){

  /*** Lire img1   ***/
  Status = ReadTensorSD(In1Path, In1Size, In1Shape, In1Tensor);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};


  /******************************************************/
  /* Lire filtres et paramètres                         */
  /******************************************************/
  /*** Couche 1 ***/
  Status = ReadTensorSD(Fil1Path, Fil1Size, Fil1Shape, Fil1Tensor);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};

  Status = ReadParamSD(Par1Path, Par1Size, Par1Shape, Par1Matrix);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};


  /*** Couche 2 ***/
  Status = ReadTensorSD(Fil2Path, Fil2Size, Fil2Shape, Fil2Tensor);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};

  Status = ReadParamSD(Par2Path, Par2Size, Par2Shape, Par2Matrix);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};


  /*** Couche 3 ***/
  Status = ReadTensorSD(Fil3Path, Fil3Size, Fil3Shape, Fil3Tensor);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};

  Status = ReadParamSD(Par3Path, Par3Size, Par3Shape, Par3Matrix);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};


  /*** Couche 4 ***/
  // Pour couche flatten il n'y pas parametres


  /*** Couche 5 ***/
  Status = ReadParamSD(Fil5Path, Fil5Size, Fil5Shape, Fil5Matrix);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};

  Status = ReadParamSD(Par5Path, Par5Size, Par5Shape, Par5Matrix);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};

  //xil_printf("Successfully Read SD Files \r\n");
  return 0;
};




/*** M6 inference */
int M6_inference(int InShape[4], int InTensor[InShape[0]][InShape[1]][InShape[2]][InShape[3]]){

  xil_printf("\n Running M6 inference... \n");

  /******************************************************/
  /* Couche 1:                                          */
  /*                                                    */
  /*    ----------------------------------------------  */
  /*   | Objectif         | Fonction | Sortie         | */
  /*    ----------------------------------------------  */
  /*   | Padding SAME     | padding  |( 1, 68, 68,  1)| */
  /*   | Filtre           |          |(32,  5,  5,  1)| */
  /*   | Convolution (5,5)| conv_k5  |( 1, 64, 64, 32)| */
  /*   | MaxPooling  (2,2)| maxp_22  |( 1, 32, 32, 32)| */
  /*    ----------------------------------------------  */
  /******************************************************/

  /*** Padding ***/
  int  Pad1Shape[4] = {1, InShape[1]+4, InShape[2]+4, 1};
  int  Pad1Tensor[Pad1Shape[0]][Pad1Shape[1]][Pad1Shape[2]][Pad1Shape[3]];
  int  zero_point = -Par1Matrix[3][0];                      // zero_point = -offset_ent
  //xil_printf("zero_point: %d \n", zero_point);
  padding(InShape, InTensor, Pad1Shape, Pad1Tensor, zero_point);

  /*** Convolution ***/
  int  Cnv1Shape[4] = {1, Pad1Shape[1]-4, Pad1Shape[2]-4, Fil1Shape[0]};
  int  Cnv1Tensor[Cnv1Shape[0]][Cnv1Shape[1]][Cnv1Shape[2]][Cnv1Shape[3]];
  memset( Cnv1Tensor, 0, Cnv1Shape[0]*Cnv1Shape[1]*Cnv1Shape[2]*Cnv1Shape[3]*sizeof(int) );
  conv_k5(Pad1Shape, Pad1Tensor, Fil1Shape, Fil1Tensor, Par1Shape, Par1Matrix, Cnv1Shape, Cnv1Tensor);

  /*** MaxPooling ***/
  int  Mxp1Shape[4] = {1, Cnv1Shape[1]/2, Cnv1Shape[2]/2, Fil1Shape[0]};
  int  Mxp1Tensor[Mxp1Shape[0]][Mxp1Shape[1]][Mxp1Shape[2]][Mxp1Shape[3]];
  maxp_22(Cnv1Shape, Cnv1Tensor, Mxp1Shape, Mxp1Tensor);
  xil_printf("*** Couche 1: Pad + Cnv + MxP => Fini!, Done! ***\n");


  /******************************************************/
  /* Couche 2:                                          */
  /*                                                    */
  /*    ----------------------------------------------  */
  /*   | Objectif         | Fonction | Sortie         | */
  /*    ----------------------------------------------  */
  /*   | Padding SAME     | padding  |( 1, 36, 36, 32)| */
  /*   | Filtre           |          |(64,  5,  5, 32)| */
  /*   | Convolution (5,5)| conv_k5  |( 1, 32, 32, 64)| */
  /*   | MaxPooling  (2,2)| maxp_22  |( 1, 16, 16, 64)| */
  /*    ----------------------------------------------  */
  /******************************************************/

  /*** Padding ***/
  int  Pad2Shape[4] = {1, Mxp1Shape[1]+4, Mxp1Shape[2]+4, Mxp1Shape[3]};
  int  Pad2Tensor[Pad2Shape[0]][Pad2Shape[1]][Pad2Shape[2]][Pad2Shape[3]];
  zero_point = -Par2Matrix[3][0];                      // zero_point = -offset_ent
  //xil_printf("zero_point: %d \n", zero_point);
  padding(Mxp1Shape, Mxp1Tensor, Pad2Shape, Pad2Tensor, zero_point);

  /*** Convolution ***/
  int  Cnv2Shape[4] = {1, Pad2Shape[1]-4, Pad2Shape[2]-4, Fil2Shape[0]};
  int  Cnv2Tensor[Cnv2Shape[0]][Cnv2Shape[1]][Cnv2Shape[2]][Cnv2Shape[3]];
  memset( Cnv2Tensor, 0, Cnv2Shape[0]*Cnv2Shape[1]*Cnv2Shape[2]*Cnv2Shape[3]*sizeof(int) );
  conv_k5(Pad2Shape, Pad2Tensor, Fil2Shape, Fil2Tensor, Par2Shape, Par2Matrix, Cnv2Shape, Cnv2Tensor);

  /*** MaxPooling ***/
  int  Mxp2Shape[4] = {1, Cnv2Shape[1]/2, Cnv2Shape[2]/2, Fil2Shape[0]};
  int  Mxp2Tensor[Mxp2Shape[0]][Mxp2Shape[1]][Mxp2Shape[2]][Mxp2Shape[3]];
  maxp_22(Cnv2Shape, Cnv2Tensor, Mxp2Shape, Mxp2Tensor);
  xil_printf("*** Couche 2: Pad + Cnv + MxP => Fini!, Done! ***\n");


  /*******************************************************/
  /* Couche 3:                                           */
  /*                                                     */
  /*    -----------------------------------------------  */
  /*   | Objectif         | Fonction | Sortie          | */
  /*    -----------------------------------------------  */
  /*   | Padding SAME     | padding  |(  1, 20, 20, 64)| */
  /*   | Filtre           |          |(128,  5,  5, 64)| */
  /*   | Convolution (5,5)| conv_k5  |(  1, 16, 16, 64)| */
  /*   | MaxPooling  (2,2)| maxp_22  |(  1,  8,  8,128)| */
  /*    -----------------------------------------------  */
  /*******************************************************/

  /*** Padding ***/
  int  Pad3Shape[4] = {1, Mxp2Shape[1]+4, Mxp2Shape[2]+4, Mxp2Shape[3]};
  int  Pad3Tensor[Pad3Shape[0]][Pad3Shape[1]][Pad3Shape[2]][Pad3Shape[3]];
  zero_point = -Par3Matrix[3][0];                      // zero_point = -offset_ent
  //xil_printf("zero_point: %d \n", zero_point);
  padding(Mxp2Shape, Mxp2Tensor, Pad3Shape, Pad3Tensor, zero_point);

  /*** Convolution ***/
  int  Cnv3Shape[4] = {1, Pad3Shape[1]-4, Pad3Shape[2]-4, Fil3Shape[0]};
  int  Cnv3Tensor[Cnv3Shape[0]][Cnv3Shape[1]][Cnv3Shape[2]][Cnv3Shape[3]];
  memset( Cnv3Tensor, 0, Cnv3Shape[0]*Cnv3Shape[1]*Cnv3Shape[2]*Cnv3Shape[3]*sizeof(int) );
  conv_k5(Pad3Shape, Pad3Tensor, Fil3Shape, Fil3Tensor, Par3Shape, Par3Matrix, Cnv3Shape, Cnv3Tensor);

  /*** MaxPooling ***/
  int  Mxp3Shape[4] = {1, Cnv3Shape[1]/2, Cnv3Shape[2]/2, Fil3Shape[0]};
  int  Mxp3Tensor[Mxp3Shape[0]][Mxp3Shape[1]][Mxp3Shape[2]][Mxp3Shape[3]];
  maxp_22(Cnv3Shape, Cnv3Tensor, Mxp3Shape, Mxp3Tensor);
  xil_printf("*** Couche 3: Pad + Cnv + MxP => Fini!, Done! ***\n");


  /*******************************************************/
  /* Couche 4:                                           */
  /*                                                     */
  /*    -----------------------------------------------  */
  /*   | Objectif         | Fonction | Sortie          | */
  /*    -----------------------------------------------  */
  /*   | Entree           |          |(  1,  8, 8, 128)| */
  /*   | Flatten          | Flatten  | 1*8*8*128 = 8192| */
  /*    -----------------------------------------------  */
  /*******************************************************/
  int  FltShape[1] = {1*8*8*128};
  int  FltVector[1*8*8*128];
  memset( FltVector, 0, FltShape[0]*sizeof(int) );
  flatten(Mxp3Shape, Mxp3Tensor, FltShape, FltVector);
  xil_printf("*** Couche 4: Flatten => Fini!, Done! ***\n");

  /**************************************************/
  /* Couche 5:                                      */
  /*                                                */
  /*    ------------------------------------------  */
  /*   | Objectif         | Fonction | Sortie     | */
  /*    ------------------------------------------  */
  /*   | Entree           |          |( 1, 8192)  | */
  /*   | Filtre           |          |( 6, 8192)  | */
  /*   | Dense            | dense    |( 1,    6)  | */
  /*    ------------------------------------------  */
  /**************************************************/
  int  Dns1Shape[1] = {Fil5Shape[0]};
  int  Dns1Vector[Dns1Shape[0]];
  memset( Dns1Vector, 0, Dns1Shape[0]*sizeof(int) );
  dense(FltShape, FltVector, Fil5Shape, Fil5Matrix, Par5Shape, Par5Matrix, Dns1Shape, Dns1Vector);
  xil_printf("*** Couche 5: Dense => Fini!, Done! ***\n");

  /*** Dns1Vector ***/
  xil_printf("\n Vecteur de classification: \n");
  for(int f=0; f<6; f++){
    xil_printf("%d, ", Dns1Vector[f]);
  };
  xil_printf(" \n ");


  /**************************************************/
  /* Classification:                                */
  /**************************************************/
  char label[6][3] = {{"HAP"}, {"ANG"}, {"DIS"}, {"FEA"}, {"SAD"}, {"SUR"}};
  int idx = 0;                                             // Index de la classification

  if ( (Dns1Vector[0] > Dns1Vector[1]) &&
       (Dns1Vector[0] > Dns1Vector[2]) && (Dns1Vector[0] > Dns1Vector[3]) &&
	   (Dns1Vector[0] > Dns1Vector[4]) && (Dns1Vector[0] > Dns1Vector[5]) ){
    idx = 0;  }
  else if ((Dns1Vector[1] > Dns1Vector[0]) &&
	       (Dns1Vector[1] > Dns1Vector[2]) && (Dns1Vector[1] > Dns1Vector[3]) &&
		   (Dns1Vector[1] > Dns1Vector[4]) && (Dns1Vector[1] > Dns1Vector[5]) ){
    idx = 1;  }
  else if ((Dns1Vector[2] > Dns1Vector[0]) &&
	       (Dns1Vector[2] > Dns1Vector[1]) && (Dns1Vector[2] > Dns1Vector[3]) &&
		   (Dns1Vector[2] > Dns1Vector[4]) && (Dns1Vector[2] > Dns1Vector[5]) ){
    idx = 2;  }
  else if ((Dns1Vector[3] > Dns1Vector[0]) &&
	       (Dns1Vector[3] > Dns1Vector[1]) && (Dns1Vector[3] > Dns1Vector[2]) &&
		   (Dns1Vector[3] > Dns1Vector[4]) && (Dns1Vector[3] > Dns1Vector[5]) ){
    idx = 3;  }
  else if ((Dns1Vector[4] > Dns1Vector[0]) &&
	       (Dns1Vector[4] > Dns1Vector[1]) && (Dns1Vector[4] > Dns1Vector[2]) &&
		   (Dns1Vector[4] > Dns1Vector[3]) && (Dns1Vector[4] > Dns1Vector[5]) ){
    idx = 4;  }
  else if ((Dns1Vector[5] > Dns1Vector[0]) &&
	       (Dns1Vector[5] > Dns1Vector[1]) && (Dns1Vector[5] > Dns1Vector[2]) &&
		   (Dns1Vector[5] > Dns1Vector[3]) && (Dns1Vector[5] > Dns1Vector[4]) ){
    idx = 5;  }
  else { idx = 0; };


  xil_printf("\n M6 FER Classification: \n");
  for (int i = 0; i < 3; i++){
    xil_printf("%c", label[idx][i]); };

  xil_printf(" \n *** End M6 inference *** \n\r");
  xil_printf(" \n ");

  return 0;

};
