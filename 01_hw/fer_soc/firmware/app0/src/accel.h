/*
---------------------------------------------------------------------
----                                                             ----
---- file:    accel.h                                            ----
---- brief:   Header accélérateur                                ----
---- details: Header accélérateur                                ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/09/02                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------
*/


#ifndef __ACCEL_H_
#define __ACCEL_H_




/**********************************************************/
/*** Fonctions noyau  ***/
/**********************************************************/

/*** Function pour computer une couche de convolution avec kernel (5,5) ***/
int32_t cv_k5_core(
  uint32_t BaseAddress,

  int32_t x01, int32_t x02, int32_t x03, int32_t x04, int32_t x05,
  int32_t x06, int32_t x07, int32_t x08, int32_t x09, int32_t x10,
  int32_t x11, int32_t x12, int32_t x13, int32_t x14, int32_t x15,
  int32_t x16, int32_t x17, int32_t x18, int32_t x19, int32_t x20,
  int32_t x21, int32_t x22, int32_t x23, int32_t x24, int32_t x25,

  int32_t f01, int32_t f02, int32_t f03, int32_t f04, int32_t f05,
  int32_t f06, int32_t f07, int32_t f08, int32_t f09, int32_t f10,
  int32_t f11, int32_t f12, int32_t f13, int32_t f14, int32_t f15,
  int32_t f16, int32_t f17, int32_t f18, int32_t f19, int32_t f20,
  int32_t f21, int32_t f22, int32_t f23, int32_t f24, int32_t f25,

  int32_t offset_ent, int32_t cv_in);


/*** Fonction noyau de la full connected / dense avec kernel (64,64) ***/
int32_t ds_k64_core(
  uint32_t BaseAddress,

  int32_t x01, int32_t x02, int32_t x03, int32_t x04, int32_t x05, int32_t x06, int32_t x07, int32_t x08,
  int32_t x09, int32_t x10, int32_t x11, int32_t x12, int32_t x13, int32_t x14, int32_t x15, int32_t x16,
  int32_t x17, int32_t x18, int32_t x19, int32_t x20, int32_t x21, int32_t x22, int32_t x23, int32_t x24,
  int32_t x25, int32_t x26, int32_t x27, int32_t x28, int32_t x29, int32_t x30, int32_t x31, int32_t x32,
  int32_t x33, int32_t x34, int32_t x35, int32_t x36, int32_t x37, int32_t x38, int32_t x39, int32_t x40,
  int32_t x41, int32_t x42, int32_t x43, int32_t x44, int32_t x45, int32_t x46, int32_t x47, int32_t x48,
  int32_t x49, int32_t x50, int32_t x51, int32_t x52, int32_t x53, int32_t x54, int32_t x55, int32_t x56,
  int32_t x57, int32_t x58, int32_t x59, int32_t x60, int32_t x61, int32_t x62, int32_t x63, int32_t x64,

  int32_t w01, int32_t w02, int32_t w03, int32_t w04, int32_t w05, int32_t w06, int32_t w07, int32_t w08,
  int32_t w09, int32_t w10, int32_t w11, int32_t w12, int32_t w13, int32_t w14, int32_t w15, int32_t w16,
  int32_t w17, int32_t w18, int32_t w19, int32_t w20, int32_t w21, int32_t w22, int32_t w23, int32_t w24,
  int32_t w25, int32_t w26, int32_t w27, int32_t w28, int32_t w29, int32_t w30, int32_t w31, int32_t w32,
  int32_t w33, int32_t w34, int32_t w35, int32_t w36, int32_t w37, int32_t w38, int32_t w39, int32_t w40,
  int32_t w41, int32_t w42, int32_t w43, int32_t w44, int32_t w45, int32_t w46, int32_t w47, int32_t w48,
  int32_t w49, int32_t w50, int32_t w51, int32_t w52, int32_t w53, int32_t w54, int32_t w55, int32_t w56,
  int32_t w57, int32_t w58, int32_t w59, int32_t w60, int32_t w61, int32_t w62, int32_t w63, int32_t w64,

  int32_t offset_ent, int32_t ds_in);


/*** Fonction noyau de MaxPooling avec kernel (2,2) ***/
int32_t mp_22_core(
  uint32_t BaseAddress,
  int32_t x01, int32_t x02, int32_t x03, int32_t x04);


/*** fonction pour padding SAME ***/
void padding(int EntShape[4], int ent[EntShape[0]][EntShape[1]][EntShape[2]][EntShape[3]],
	         int PadShape[4], int pad[PadShape[0]][PadShape[1]][PadShape[2]][PadShape[3]],
			 int zero_point);


/*** Fonction noyau de la mbqm du tflite ***/
int32_t tflite_mbqm(
  uint32_t BaseAddress,
  int32_t cv_in, int32_t bias,
  int32_t M0,    int32_t shift);

/**********************************************************/




/**********************************************************/
/*** Fonctiones pour les couches  ***/
/**********************************************************/

/*** Function pour computer une couche de convolution avec kernel (5,5) ***/
void conv_k5(int EntShape[4], int ent[EntShape[0]][EntShape[1]][EntShape[2]][EntShape[3]],
	         int FilShape[4], int fil[FilShape[0]][FilShape[1]][FilShape[2]][FilShape[3]],
	         int ParShape[2], int par[ParShape[0]][ParShape[1]],
			 int CnvShape[4], int cnv[CnvShape[0]][CnvShape[1]][CnvShape[2]][CnvShape[3]]);


/*** Function pour computer une couche dense/ full connected ***/
void dense(int EntShape[1], int ent[EntShape[0]],
	       int FilShape[2], int fil[FilShape[0]][FilShape[1]],
	       int ParShape[2], int par[ParShape[0]][ParShape[1]],
		   int DnsShape[1], int dns[DnsShape[0]]);



/*** Function flatten: convertir tenseur à tableau ***/
void flatten(int EntShape[4], int ent[EntShape[0]][EntShape[1]][EntShape[2]][EntShape[3]],
	         int FltShape[1], int flt[FltShape[0]]);



/*** Function pour computer une couche maxpool taille (2, 2) ***/
void maxp_22(int CnvShape[4], int cnv[CnvShape[0]][CnvShape[1]][CnvShape[2]][CnvShape[3]],
	         int MxpShape[4], int mxp[MxpShape[0]][MxpShape[1]][MxpShape[2]][MxpShape[3]]);



/**********************************************************/
/*** Fonctions pour éprouver les noyaux  ***/
/**********************************************************/

/*** Épreuver cv_k5_core   ***/
void cv_k5_core_test(void);


#endif
