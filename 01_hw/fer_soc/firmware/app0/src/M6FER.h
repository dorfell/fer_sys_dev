/*
 ---------------------------------------------------------------------
 ----                                                             ----
 ---- file:    M6FER.h                                            ----
 ---- brief:   Header modèle M6 pour FER                          ----
 ---- details: Header modèle M6 pour FER                          ----
 ---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
 ---- date:    2021/10/19                                         ----
 ---- version: 0.1                                                ----
 ---------------------------------------------------------------------
 ----                                                             ----
 ---- Copyright (C) 2021 Dorfell Parra                            ----
 ----                    dlparrap@unal.edu.co                     ----
 ----                                                             ----
 ---- This source file may be used and distributed without        ----
 ---- restriction provided that this copyright statement is not   ----
 ---- removed from the file and that any derivative work contains ----
 ---- the original copyright notice and the associated disclaimer.----
 ----                                                             ----
 ----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
 ---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
 ---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
 ---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
 ---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
 ---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
 ---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
 ---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
 ---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
 ---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
 ---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
 ---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
 ---- POSSIBILITY OF SUCH DAMAGE.                                 ----
 ----                                                             ----
 ---------------------------------------------------------------------
 */

#ifndef __M6FER_H_
#define __M6FER_H_


/*** Initializing Modèle M6 ***/
int init_M6(void);


/*** M6 inference */
int M6_inference(int InShape[4], int InTensor[InShape[0]][InShape[1]][InShape[2]][InShape[3]]);


#endif

