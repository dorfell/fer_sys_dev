/*
---------------------------------------------------------------------
----                                                             ----
---- file:    accel.c                                            ----
---- brief:   Fonctions de l'accelerateur                        ----
---- details: Fonctions pour utiliser l'accelerateur             ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/10/01                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------
*/

#include "conv.h"
#include "dense.h"
#include "tflite_mbqm.h"
#include "mpool.h"
#include "xil_printf.h"                                    // Data over UART
#include "xil_io.h"


#include <stdbool.h>                                       // bool type
#include <assert.h>                                        // bool type




#define max(a,b)({         \
  __typeof__ (a) _a = (a); \
  __typeof__ (b) _b = (b); \
  _a > _b ? _a : _b;       })

#define min(a,b)({           \
  __typeof__ (a) _a = (a); \
  __typeof__ (b) _b = (b); \
  _a < _b ? _a : _b;       })




/**********************************************************/
/*** TFLite fonctions     ***/
/**********************************************************/
static inline int32_t Dup(int32_t x){
  return x; };

static inline int32_t BitAnd(int32_t a, int32_t b){
  return a & b; };

static inline int32_t BitNot(int32_t a){
  //xil_printf("\r\r  a: %d \n",  a);
  //xil_printf("\r\r ~a: %d \n", ~a);
  return ~a; };

static inline int32_t Add(int32_t a, int32_t b){
  return a + b; };

static inline int32_t ShiftRight(int32_t a, int8_t offset){
  return a >> offset; };

static inline int32_t MaskIfNonZero(int32_t a){
  int32_t zero = 0;
  return a ? BitNot(zero) : zero;  };

static inline int32_t MaskIfGreaterThan(int32_t a, int32_t b){
  return MaskIfNonZero(a > b);  };

static inline int32_t MaskIfLessThan(int32_t a, int32_t b){
  return MaskIfNonZero(a < b); };

static inline int32_t SaturatingRoundingDoublingHighMul(int32_t a, int32_t b){
  //bool overflow = a == b && a == std::numeric_limits<std::int32_t>::min();
  bool overflow = ((a == b) && (a == -2147483648));
  /*int64_t a_64(a); int64_t b_64(b);
  int64_t ab_64 = a_64 * b_64;  */

  int64_t a_64 = (int64_t)(a); int64_t b_64 = (int64_t)(b);
  int64_t ab_64 = a_64 * b_64;
  int32_t nudge = ab_64 >= 0 ? (1 << 30) : (1 - (1 << 30));
  //int32_t ab_x2_high32 =  static_cast<std::int32_t>((ab_64 + nudge) / (1ll << 31));
  int32_t ab_x2_high32 =  (int32_t)((ab_64 + nudge) / (1ll << 31));

  //xil_printf(" -- ab_64_high = %x \n", (uint32_t) (ab_64 >> 32));
  //xil_printf(" -- ab_64_low = %x \n",  (uint32_t)  ab_64);
  //xil_printf(" -- nudge = %x \n",      (uint32_t)  nudge);
  //xil_printf(" -- (ab_64 + nudge) high = %x \n", (uint32_t) ((ab_64+nudge) >> 32) );
  //xil_printf(" -- (ab_64 + nudge) low  = %x \n", (uint32_t) (ab_64+nudge));
  //xil_printf(" -- ab_x2_high32 = %x \n",  (uint32_t) ab_x2_high32);

  //return overflow ? std::numeric_limits<std::int32_t>::max() : ab_x2_high32;
  return overflow ? 2147483647 : ab_x2_high32;
};


static inline int32_t RoundingDivideByPOT(int32_t x, int8_t exponent) {
  assert(exponent >= 0);
  assert(exponent <= 31);
  int32_t mask = Dup((1ll << exponent) - 1);
  int32_t zero = Dup(0);
  int32_t one  = Dup(1);
  int32_t remainder = BitAnd(x, mask);
  int32_t threshold = Add(ShiftRight(mask, 1), BitAnd(MaskIfLessThan(x, zero), one));
  return Add( ShiftRight(x, exponent), BitAnd( MaskIfGreaterThan(remainder, threshold), one ) ); };


static inline int32_t MultiplyByQuantizedMultiplier(int32_t x, int32_t quantized_multiplier, int shift){
  int8_t left_shift  = shift > 0 ? shift :      0;
  int8_t right_shift = shift > 0 ? 0     : -shift;
  return RoundingDivideByPOT(SaturatingRoundingDoublingHighMul(x * (1 << left_shift), quantized_multiplier), right_shift); };

/**********************************************************/




/**********************************************************/
/*** Fonctions noyau - IP cores  ***/
/**********************************************************/

/*** Fonction noyau de la convolution avec kernel (5,5) ***/
int32_t cv_k5_core(
  uint32_t BaseAddress,

  int32_t x01, int32_t x02, int32_t x03, int32_t x04, int32_t x05,
  int32_t x06, int32_t x07, int32_t x08, int32_t x09, int32_t x10,
  int32_t x11, int32_t x12, int32_t x13, int32_t x14, int32_t x15,
  int32_t x16, int32_t x17, int32_t x18, int32_t x19, int32_t x20,
  int32_t x21, int32_t x22, int32_t x23, int32_t x24, int32_t x25,

  int32_t w01, int32_t w02, int32_t w03, int32_t w04, int32_t w05,
  int32_t w06, int32_t w07, int32_t w08, int32_t w09, int32_t w10,
  int32_t w11, int32_t w12, int32_t w13, int32_t w14, int32_t w15,
  int32_t w16, int32_t w17, int32_t w18, int32_t w19, int32_t w20,
  int32_t w21, int32_t w22, int32_t w23, int32_t w24, int32_t w25,

  int32_t offset_ent, int32_t cv_in){

  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG1_OFFSET,  x01);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG2_OFFSET,  x02);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG3_OFFSET,  x03);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG4_OFFSET,  x04);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG5_OFFSET,  x05);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG6_OFFSET,  x06);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG7_OFFSET,  x07);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG8_OFFSET,  x08);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG9_OFFSET,  x09);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG10_OFFSET, x10);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG11_OFFSET, x11);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG12_OFFSET, x12);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG13_OFFSET, x13);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG14_OFFSET, x14);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG15_OFFSET, x15);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG16_OFFSET, x16);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG17_OFFSET, x17);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG18_OFFSET, x18);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG19_OFFSET, x19);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG20_OFFSET, x20);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG21_OFFSET, x21);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG22_OFFSET, x22);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG23_OFFSET, x23);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG24_OFFSET, x24);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG25_OFFSET, x25);

  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG26_OFFSET, w01);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG27_OFFSET, w02);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG28_OFFSET, w03);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG29_OFFSET, w04);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG30_OFFSET, w05);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG31_OFFSET, w06);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG32_OFFSET, w07);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG33_OFFSET, w08);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG34_OFFSET, w09);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG35_OFFSET, w10);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG36_OFFSET, w11);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG37_OFFSET, w12);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG38_OFFSET, w13);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG39_OFFSET, w14);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG40_OFFSET, w15);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG41_OFFSET, w16);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG42_OFFSET, w17);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG43_OFFSET, w18);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG44_OFFSET, w19);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG45_OFFSET, w20);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG46_OFFSET, w21);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG47_OFFSET, w22);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG48_OFFSET, w23);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG49_OFFSET, w24);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG50_OFFSET, w25);

  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG51_OFFSET, offset_ent);

  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG52_OFFSET, cv_in);

  // Start convolution
  int32_t cv_out = 0;                                      // (entree+offset)*filtre + cnv[0][i][j][f]
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG0_OFFSET,  0x00000001);
  while( CONV_mReadReg (BaseAddress, CONV_S00_AXI_SLV_REG54_OFFSET) == 0x00000000 ){};
  cv_out = CONV_mReadReg (BaseAddress, CONV_S00_AXI_SLV_REG53_OFFSET);
  //xil_printf("cv = cv + (x+offset_ent)*w = %x \n", cv_out);

  // Arreter la convolution
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG0_OFFSET,  0x0000000);

  return cv_out;
};




/*** Fonction noyau de la full connected / dense avec kernel (64,64) ***/
int32_t ds_k64_core(
  uint32_t BaseAddress,

  int32_t x01, int32_t x02, int32_t x03, int32_t x04, int32_t x05, int32_t x06, int32_t x07, int32_t x08,
  int32_t x09, int32_t x10, int32_t x11, int32_t x12, int32_t x13, int32_t x14, int32_t x15, int32_t x16,
  int32_t x17, int32_t x18, int32_t x19, int32_t x20, int32_t x21, int32_t x22, int32_t x23, int32_t x24,
  int32_t x25, int32_t x26, int32_t x27, int32_t x28, int32_t x29, int32_t x30, int32_t x31, int32_t x32,
  int32_t x33, int32_t x34, int32_t x35, int32_t x36, int32_t x37, int32_t x38, int32_t x39, int32_t x40,
  int32_t x41, int32_t x42, int32_t x43, int32_t x44, int32_t x45, int32_t x46, int32_t x47, int32_t x48,
  int32_t x49, int32_t x50, int32_t x51, int32_t x52, int32_t x53, int32_t x54, int32_t x55, int32_t x56,
  int32_t x57, int32_t x58, int32_t x59, int32_t x60, int32_t x61, int32_t x62, int32_t x63, int32_t x64,

  int32_t w01, int32_t w02, int32_t w03, int32_t w04, int32_t w05, int32_t w06, int32_t w07, int32_t w08,
  int32_t w09, int32_t w10, int32_t w11, int32_t w12, int32_t w13, int32_t w14, int32_t w15, int32_t w16,
  int32_t w17, int32_t w18, int32_t w19, int32_t w20, int32_t w21, int32_t w22, int32_t w23, int32_t w24,
  int32_t w25, int32_t w26, int32_t w27, int32_t w28, int32_t w29, int32_t w30, int32_t w31, int32_t w32,
  int32_t w33, int32_t w34, int32_t w35, int32_t w36, int32_t w37, int32_t w38, int32_t w39, int32_t w40,
  int32_t w41, int32_t w42, int32_t w43, int32_t w44, int32_t w45, int32_t w46, int32_t w47, int32_t w48,
  int32_t w49, int32_t w50, int32_t w51, int32_t w52, int32_t w53, int32_t w54, int32_t w55, int32_t w56,
  int32_t w57, int32_t w58, int32_t w59, int32_t w60, int32_t w61, int32_t w62, int32_t w63, int32_t w64,

  int32_t offset_ent, int32_t ds_in){

  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG1_OFFSET,  x01);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG2_OFFSET,  x02);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG3_OFFSET,  x03);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG4_OFFSET,  x04);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG5_OFFSET,  x05);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG6_OFFSET,  x06);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG7_OFFSET,  x07);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG8_OFFSET,  x08);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG9_OFFSET,  x09);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG10_OFFSET, x10);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG11_OFFSET, x11);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG12_OFFSET, x12);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG13_OFFSET, x13);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG14_OFFSET, x14);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG15_OFFSET, x15);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG16_OFFSET, x16);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG17_OFFSET, x17);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG18_OFFSET, x18);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG19_OFFSET, x19);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG20_OFFSET, x20);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG21_OFFSET, x21);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG22_OFFSET, x22);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG23_OFFSET, x23);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG24_OFFSET, x24);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG25_OFFSET, x25);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG26_OFFSET, x26);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG27_OFFSET, x27);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG28_OFFSET, x28);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG29_OFFSET, x29);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG30_OFFSET, x30);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG31_OFFSET, x31);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG32_OFFSET, x32);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG33_OFFSET, x33);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG34_OFFSET, x34);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG35_OFFSET, x35);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG36_OFFSET, x36);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG37_OFFSET, x37);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG38_OFFSET, x38);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG39_OFFSET, x39);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG40_OFFSET, x40);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG41_OFFSET, x41);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG42_OFFSET, x42);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG43_OFFSET, x43);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG44_OFFSET, x44);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG45_OFFSET, x45);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG46_OFFSET, x46);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG47_OFFSET, x47);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG48_OFFSET, x48);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG49_OFFSET, x49);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG50_OFFSET, x50);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG51_OFFSET, x51);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG52_OFFSET, x52);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG53_OFFSET, x53);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG54_OFFSET, x54);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG55_OFFSET, x55);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG56_OFFSET, x56);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG57_OFFSET, x57);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG58_OFFSET, x58);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG59_OFFSET, x59);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG60_OFFSET, x60);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG61_OFFSET, x61);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG62_OFFSET, x62);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG63_OFFSET, x63);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG64_OFFSET, x64);


  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG65_OFFSET, w01);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG66_OFFSET, w02);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG67_OFFSET, w03);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG68_OFFSET, w04);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG69_OFFSET, w05);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG70_OFFSET, w06);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG71_OFFSET, w07);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG72_OFFSET, w08);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG73_OFFSET, w09);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG74_OFFSET, w10);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG75_OFFSET, w11);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG76_OFFSET, w12);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG77_OFFSET, w13);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG78_OFFSET, w14);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG79_OFFSET, w15);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG80_OFFSET, w16);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG81_OFFSET, w17);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG82_OFFSET, w18);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG83_OFFSET, w19);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG84_OFFSET, w20);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG85_OFFSET, w21);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG86_OFFSET, w22);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG87_OFFSET, w23);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG88_OFFSET, w24);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG89_OFFSET, w25);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG90_OFFSET, w26);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG91_OFFSET, w27);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG92_OFFSET, w28);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG93_OFFSET, w29);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG94_OFFSET, w30);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG95_OFFSET, w31);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG96_OFFSET, w32);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG97_OFFSET, w33);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG98_OFFSET, w34);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG99_OFFSET, w35);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG100_OFFSET, w36);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG101_OFFSET, w37);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG102_OFFSET, w38);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG103_OFFSET, w39);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG104_OFFSET, w40);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG105_OFFSET, w41);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG106_OFFSET, w42);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG107_OFFSET, w43);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG108_OFFSET, w44);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG109_OFFSET, w45);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG110_OFFSET, w46);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG111_OFFSET, w47);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG112_OFFSET, w48);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG113_OFFSET, w49);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG114_OFFSET, w50);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG115_OFFSET, w51);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG116_OFFSET, w52);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG117_OFFSET, w53);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG118_OFFSET, w54);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG119_OFFSET, w55);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG120_OFFSET, w56);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG121_OFFSET, w57);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG122_OFFSET, w58);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG123_OFFSET, w59);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG124_OFFSET, w60);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG125_OFFSET, w61);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG126_OFFSET, w62);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG127_OFFSET, w63);
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG128_OFFSET, w64);

  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG129_OFFSET, offset_ent);

  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG130_OFFSET, ds_in);

  // Start DENSE
  int32_t ds_out = 0;                                      // (entree+offset)*filtre + ds[0][i]
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG0_OFFSET,  0x00000001);
  while( DENSE_mReadReg (BaseAddress, DENSE_S00_AXI_SLV_REG132_OFFSET) == 0x00000000 ){};
  ds_out = DENSE_mReadReg (BaseAddress, DENSE_S00_AXI_SLV_REG131_OFFSET);
  //xil_printf("ds = ds + (x+offset_ent)*w = %x \n", ds_out);

  // Arreter la DENSE
  DENSE_mWriteReg(BaseAddress, DENSE_S00_AXI_SLV_REG0_OFFSET,  0x0000000);

  return ds_out;
};




/*** Fonction noyau de MaxPooling avec kernel (2,2) ***/
int32_t mp_22_core(
  uint32_t BaseAddress,
  int32_t x01, int32_t x02, int32_t x03, int32_t x04){

  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG1_OFFSET, x01);
  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG2_OFFSET, x02);
  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG3_OFFSET, x03);
  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG4_OFFSET, x04);

  // Start MP
  int32_t res = 0;
  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG0_OFFSET,  0x00000001);
  while( MPOOL_mReadReg(BaseAddress, MPOOL_S00_AXI_SLV_REG6_OFFSET) == 0x00000000 ){};
  res = MPOOL_mReadReg(BaseAddress, MPOOL_S00_AXI_SLV_REG5_OFFSET);
  //xil_printf("MaxPooling results = %x \n", res);
  //xil_printf("MaxPooling results = %d \n", res);

  // Arreter le MaxPooling
  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG0_OFFSET,  0x0000000);

  return res;
};




/*** fonction pour padding SAME ***/
void padding(int EntShape[4], int ent[EntShape[0]][EntShape[1]][EntShape[2]][EntShape[3]],
	         int PadShape[4], int pad[PadShape[0]][PadShape[1]][PadShape[2]][PadShape[3]],
			 int zero_point){

  for(int f=0; f<EntShape[0];   f++){                      // boucle pour les différents filtres
    for(int i=0; i<EntShape[1]+4; i++){                    // boucle pour les rangs de l'entrée
      for(int j=0; j<EntShape[2]+4; j++){                  // boucle pour les colonnes de l'entrée
        for(int k=0; k<EntShape[3];   k++){                // boucle pour les cartes de carácteristiques de l'entrée = chaînes de filtres = cc de la entrée

          if( ( ((i>=0) && (i<2)) || ((i>=EntShape[1]+2) && (i<EntShape[1]+4)) ) ||
              ( ((j>=0) && (j<2)) || ((j>=EntShape[2]+2) && (i<EntShape[2]+4)) )    ){
            pad[f][i][j][k] = zero_point; }
          else {
        	pad[f][i][j][k] = ent[f][i-2][j-2][k]; };
        };
      };
    };
  };
};




/*** Fonction noyau de mbqm de tflite ***/
int32_t tflite_mbqm(
  uint32_t BaseAddress,
  int32_t cv_in, int32_t bias,
  int32_t M0,    int32_t shift){

  TFLITE_MBQM_mWriteReg(BaseAddress, TFLITE_MBQM_S00_AXI_SLV_REG1_OFFSET,  cv_in);
  TFLITE_MBQM_mWriteReg(BaseAddress, TFLITE_MBQM_S00_AXI_SLV_REG2_OFFSET,  bias);

  TFLITE_MBQM_mWriteReg(BaseAddress, TFLITE_MBQM_S00_AXI_SLV_REG3_OFFSET,  M0);
  TFLITE_MBQM_mWriteReg(BaseAddress, TFLITE_MBQM_S00_AXI_SLV_REG4_OFFSET,  shift);

  // Start tflite part of convolution
  int32_t tf_out = 0;                                      // MBQM
  TFLITE_MBQM_mWriteReg(BaseAddress, TFLITE_MBQM_S00_AXI_SLV_REG0_OFFSET,  0x00000001);
  while( TFLITE_MBQM_mReadReg (BaseAddress, TFLITE_MBQM_S00_AXI_SLV_REG6_OFFSET) == 0x00000000 ){};
  tf_out = TFLITE_MBQM_mReadReg (BaseAddress, TFLITE_MBQM_S00_AXI_SLV_REG5_OFFSET);
  //xil_printf("mbqm + clamp + offset_sor + clamp = %x \n", tf_out);

  // Arreter la convolution
  TFLITE_MBQM_mWriteReg(BaseAddress, TFLITE_MBQM_S00_AXI_SLV_REG0_OFFSET,  0x0000000);

  return tf_out;
};
/**********************************************************/




/**********************************************************/
/*** Fonctiones pour les couches  ***/
/**********************************************************/

/*** Function pour computer une couche de convolution avec kernel (5,5) ***/
void conv_k5(int EntShape[4], int ent[EntShape[0]][EntShape[1]][EntShape[2]][EntShape[3]],
	         int FilShape[4], int fil[FilShape[0]][FilShape[1]][FilShape[2]][FilShape[3]],
	         int ParShape[2], int par[ParShape[0]][ParShape[1]],
			 int CnvShape[4], int cnv[CnvShape[0]][CnvShape[1]][CnvShape[2]][CnvShape[3]]){

  int32_t M0, shift, bias;
  int32_t offset_ent = par[3][0];
  int32_t offset_sor = par[3][1];
  //int32_t zero_point = -offset_ent;
  //xil_printf("offset_ent: %d \n", offset_ent);
  //xil_printf("offset_sor: %d \n", offset_sor);
  //xil_printf("zero_point: %d \n", zero_point);


  for(int f=0; f<FilShape[0];   f++){                      // boucle pour les différents filtres
    M0    = par[0][f];                                     // M0 per filtre
	shift = par[1][f];                                     // shift per filtre
	bias  = par[2][f];                                     // bias
    //xil_printf("M0:     %d \n", M0);
	//xil_printf("shift:  %d \n", shift);
	//xil_printf("bias:   %d \n", bias);
    for(int i=0; i<EntShape[1]-4; i++){                    // boucle pour les rangs de l'entrée
      for(int j=0; j<EntShape[2]-4; j++){                  // boucle pour les colonnes de l'entrée
        for(int k=0; k<EntShape[3];   k++){                // boucle pour les cartes de carácteristiques de l'entrée = chaînes de filtres = cc de la entrée

          // Faire la convolution
          cnv[0][i][j][f] = cv_k5_core(
            0x43C00000,                                    // BaseAddress conv
            ent[0][0+i][0+j][k], ent[0][0+i][1+j][k], ent[0][0+i][2+j][k], ent[0][0+i][3+j][k], ent[0][0+i][4+j][k],
            ent[0][1+i][0+j][k], ent[0][1+i][1+j][k], ent[0][1+i][2+j][k], ent[0][1+i][3+j][k], ent[0][1+i][4+j][k],
            ent[0][2+i][0+j][k], ent[0][2+i][1+j][k], ent[0][2+i][2+j][k], ent[0][2+i][3+j][k], ent[0][2+i][4+j][k],
            ent[0][3+i][0+j][k], ent[0][3+i][1+j][k], ent[0][3+i][2+j][k], ent[0][3+i][3+j][k], ent[0][3+i][4+j][k],
            ent[0][4+i][0+j][k], ent[0][4+i][1+j][k], ent[0][4+i][2+j][k], ent[0][4+i][3+j][k], ent[0][4+i][4+j][k],

            fil[f][0][0][k], fil[f][0][1][k], fil[f][0][2][k], fil[f][0][3][k], fil[f][0][4][k],
            fil[f][1][0][k], fil[f][1][1][k], fil[f][1][2][k], fil[f][1][3][k], fil[f][1][4][k],
            fil[f][2][0][k], fil[f][2][1][k], fil[f][2][2][k], fil[f][2][3][k], fil[f][2][4][k],
            fil[f][3][0][k], fil[f][3][1][k], fil[f][3][2][k], fil[f][3][3][k], fil[f][3][4][k],
            fil[f][4][0][k], fil[f][4][1][k], fil[f][4][2][k], fil[f][4][3][k], fil[f][4][4][k],

            offset_ent, cnv[0][i][j][f]);
        };
        cnv[0][i][j][f] = tflite_mbqm(
          0x43C10000,                                      // BaseAddress conv_tflite
		  cnv[0][i][j][f],
		  bias, M0, shift );

        // ReLu de Conv
        cnv[0][i][j][f] = min( max(cnv[0][i][j][f], 0), 255);
        //xil_printf(" sum = min( max(sum, 0), 255) = %d \n", cnv[0][i][j][f]);

        // ajouter l'offset de sortie
        cnv[0][i][j][f] = cnv[0][i][j][f] + offset_sor;
        //xil_printf(" sum + offset_sor = %d \n", cnv[0][i][j][f]);

        // Clamp la convolution
        cnv[0][i][j][f] = min( max(cnv[0][i][j][f], -128), 127);
        //xil_printf(" sum = %d \n", cnv[0][i][j][f]);

      };
    };
  };

};




/*** Function pour computer une couche dense/full connected ***/
void dense(int EntShape[1], int ent[EntShape[0]],
	       int FilShape[2], int fil[FilShape[0]][FilShape[1]],
	       int ParShape[2], int par[ParShape[0]][ParShape[1]],
		   int DnsShape[1], int dns[DnsShape[0]]){

  int32_t M0, shift, bias;
  int32_t offset_ent = par[3][0];
  int32_t offset_sor = par[3][1];
  //int32_t zero_point = -offset_ent;
  //xil_printf("offset_ent: %d \n", offset_ent);
  //xil_printf("offset_sor: %d \n", offset_sor);
  //xil_printf("zero_point: %d \n", zero_point);


  for(int f=0; f<FilShape[0];   f++){                      // boucle pour les différents neurons
    M0    = par[0][f];                                     // M0 per filtre
    shift = par[1][f];                                     // shift per filtre
	bias  = par[2][f];                                     // bias
    //xil_printf("M0:     %d \n", M0);
	//xil_printf("shift:  %d \n", shift);
	//xil_printf("bias:   %d \n", bias);

    for(int i=0; i<EntShape[0]/64; i++){                   // boucle pour sauter chaque 64 éléments

      // Computer la multiplication full connected de 64 en 64 éléments
      dns[f] = ds_k64_core(
        0x43C30000,                                        // BaseAddress dense noyau
         ent[0+64*i],  ent[1+64*i],  ent[2+64*i],  ent[3+64*i],  ent[4+64*i],  ent[5+64*i],  ent[6+64*i],  ent[7+64*i],
		 ent[8+64*i],  ent[9+64*i], ent[10+64*i], ent[11+64*i], ent[12+64*i], ent[13+64*i], ent[14+64*i], ent[15+64*i],
		ent[16+64*i], ent[17+64*i], ent[18+64*i], ent[19+64*i], ent[20+64*i], ent[21+64*i], ent[22+64*i], ent[23+64*i],
		ent[24+64*i], ent[25+64*i], ent[26+64*i], ent[27+64*i], ent[28+64*i], ent[29+64*i], ent[30+64*i], ent[31+64*i],
		ent[32+64*i], ent[33+64*i], ent[34+64*i], ent[35+64*i], ent[36+64*i], ent[37+64*i], ent[38+64*i], ent[39+64*i],
		ent[40+64*i], ent[41+64*i], ent[42+64*i], ent[43+64*i], ent[44+64*i], ent[45+64*i], ent[46+64*i], ent[47+64*i],
		ent[48+64*i], ent[49+64*i], ent[50+64*i], ent[51+64*i], ent[52+64*i], ent[53+64*i], ent[54+64*i], ent[55+64*i],
		ent[56+64*i], ent[57+64*i], ent[58+64*i], ent[59+64*i], ent[60+64*i], ent[61+64*i], ent[62+64*i], ent[63+64*i],

         fil[f][0+64*i],  fil[f][1+64*i],  fil[f][2+64*i],  fil[f][3+64*i],  fil[f][4+64*i],  fil[f][5+64*i],  fil[f][6+64*i],  fil[f][7+64*i],
		 fil[f][8+64*i],  fil[f][9+64*i], fil[f][10+64*i], fil[f][11+64*i], fil[f][12+64*i], fil[f][13+64*i], fil[f][14+64*i], fil[f][15+64*i],
		fil[f][16+64*i], fil[f][17+64*i], fil[f][18+64*i], fil[f][19+64*i], fil[f][20+64*i], fil[f][21+64*i], fil[f][22+64*i], fil[f][23+64*i],
		fil[f][24+64*i], fil[f][25+64*i], fil[f][26+64*i], fil[f][27+64*i], fil[f][28+64*i], fil[f][29+64*i], fil[f][30+64*i], fil[f][31+64*i],
		fil[f][32+64*i], fil[f][33+64*i], fil[f][34+64*i], fil[f][35+64*i], fil[f][36+64*i], fil[f][37+64*i], fil[f][38+64*i], fil[f][39+64*i],
        fil[f][40+64*i], fil[f][41+64*i], fil[f][42+64*i], fil[f][43+64*i], fil[f][44+64*i], fil[f][45+64*i], fil[f][46+64*i], fil[f][47+64*i],
		fil[f][48+64*i], fil[f][49+64*i], fil[f][50+64*i], fil[f][51+64*i], fil[f][52+64*i], fil[f][53+64*i], fil[f][54+64*i], fil[f][55+64*i],
		fil[f][56+64*i], fil[f][57+64*i], fil[f][58+64*i], fil[f][59+64*i], fil[f][60+64*i], fil[f][61+64*i], fil[f][62+64*i], fil[f][63+64*i],

        offset_ent, dns[f]);
    };

    //xil_printf(" avant mbqm = %d \n", dns[f]);

    dns[f] = tflite_mbqm(
      0x43C10000,                                      // BaseAddress conv_tflite
      dns[f],
	  bias, M0, shift );

    // ajouter l'offset de sortie
    dns[f] = dns[f] + offset_sor;
    //xil_printf(" sum + offset_sor = %d \n", dns[f]);

    // Clamp la dense
    dns[f] = min( max(dns[f], -128), 127);
    //xil_printf(" sum = %d \n", dns[f]);
  };
};




/*** Function flatten: convertir tenseur à tableau ***/
void flatten(int EntShape[4], int ent[EntShape[0]][EntShape[1]][EntShape[2]][EntShape[3]],
	         int FltShape[1], int flt[FltShape[0]]){
  int idx = 0;
  for(int i=0; i<EntShape[1]; i++){                        // boucle pour les rangs
    for(int j=0; j<EntShape[2]; j++){                      // boucle pour les colonnes
      for(int k=0; k<EntShape[3]; k++){                    // boucle pour les cartes de carácteristiques

        // Faire MaxPooling
        flt[idx] = ent[0][i][j][k];
        idx = idx + 1;
      };
    };
  };
};




/*** Function pour computer une couche maxpool taille (2, 2) ***/
void maxp_22(int CnvShape[4], int cnv[CnvShape[0]][CnvShape[1]][CnvShape[2]][CnvShape[3]],
	         int MxpShape[4], int mxp[MxpShape[0]][MxpShape[1]][MxpShape[2]][MxpShape[3]]){

  for(int i=0; i<MxpShape[1]; i++){                        // boucle pour les rangs
    for(int j=0; j<MxpShape[2]; j++){                      // boucle pour les colonnes
      for(int k=0; k<MxpShape[3]; k++){                    // boucle pour les cartes de carácteristiques

        // Faire MaxPooling
        mxp[0][i][j][k] = mp_22_core(
          0x43C20000,                                      // BaseAddress
          cnv[0][0+i*2][0+j*2][k], cnv[0][0+i*2][1+j*2][k],
		  cnv[0][1+i*2][0+j*2][k], cnv[0][1+i*2][1+j*2][k] );
      };
    };
  };
};

/**********************************************************/







/**********************************************************/
/*** TEST fonctions     ***/
/**********************************************************/

/*** Épreuver cv_k5_core   ***/
void cv_k5_core_test(void){

  // Entrée avec padding.
  int32_t ent[1][5][5][1] =
    {{{{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-128}, {-128}, {-123}, {-121}, {-117}},
      {{-128}, {-128}, {-123}, {-123}, {-123}},
      {{-128}, {-128}, {-122}, {-124}, {-125}}}};

  /*int32_t ent[1][5][5][1] =
    {{{{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-128}, {-123}, {-121}, {-117}, {-109}},
      {{-128}, {-123}, {-123}, {-123}, {-122}},
      {{-128}, {-122}, {-124}, {-125}, {-125}}}};*/

  /*int32_t ent[1][5][5][1] =
    {{{{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-123}, {-121}, {-117}, {-109}, {-104}},
      {{-123}, {-123}, {-123}, {-122}, {-120}},
      {{-122}, {-124}, {-125}, {-125}, {-124}}}};*/

  /*int32_t ent[1][5][5][1] =
    {{{{ -53}, { -43}, { -36}, { -25}, { -19}},
      {{   7}, {  -6}, { -10}, { -22}, { -56}},
      {{ -57}, { -77}, { -98}, {-108}, {-103}},
      {{-104}, { -93}, { -65}, { -42}, { -33}},
      {{ -18}, {  -6}, {  -3}, { -10}, { -32}}}};*/


  int32_t fil[1][5][5][1] =
    {{{{ -23}, {  73}, {  22}, { -70}, { -61}},
      {{ -60}, {-106}, {-107}, {  40}, { -61}},
      {{  84}, { -49}, {  45}, {  39}, { -25}},
      {{  68}, { -59}, { -28}, {  44}, {  -8}},
      {{  48}, {  -8}, {-127}, { -50}, {-109}}}};

  /*int32_t fil[1][5][5][1] =
    {{{{  37}, { -32}, { -16}, {   8}, { -96}},
      {{ -78}, {-100}, { -90}, {   8}, { -61}},
      {{  15}, { -20}, { -19}, { -38}, { -61}},
      {{ -22}, { -70}, {  -9}, { -58}, {  -3}},
      {{-121}, {-127}, { -77}, { -32}, {  85}}}};*/


  // filter 1
  int32_t zero_point = -128;
  int32_t offset_ent = -zero_point;
  int32_t bias       = -17;
  int32_t M0         = 1218999674;
  int32_t shift      = -5;
  int32_t offset_sor = -18;


  // filter 2
  /*int32_t zero_point = -128;
  int32_t offset_ent = -zero_point;
  int32_t bias       = 75;
  int32_t M0         = 1549632462;
  int32_t shift      = -5;
  int32_t offset_sor = -18; */


  xil_printf("\n cv_k5_core_test() ... \n");
  xil_printf("\n Resultats de CPU: \n");
  int32_t sum = 0;                                         // Résultat
  for(int f=0; f<0x01; f++){
    for(int i=0; i<0x05; i++){
      for(int j=0; j<0x05; j++){
        for(int k=0; k<0x01; k++){
          sum += (ent[f][i][j][k]+offset_ent) * fil[f][i][j][k];
        };
      };
    };
  };

  xil_printf(" W*(x+offset_ent) = %d \n", sum);
  xil_printf(" W*(x+offset_ent) = %x \n", sum);

  sum += bias;
  xil_printf(" W*(x+offset_ent)+bias = %d \n", sum);
  xil_printf(" W*(x+offset_ent)+bias = %x \n", sum);

  int sum1 = 0;
  int8_t right_shift = shift > 0 ? 0 : -shift;
  sum1 = RoundingDivideByPOT(sum, right_shift);
  xil_printf(" RoundingDivideByPOT = %d \n", sum1);
  xil_printf(" RoundingDivideByPOT = %x \n", sum1);

  sum = MultiplyByQuantizedMultiplier(sum, M0, shift);
  xil_printf(" MultByQuanMul = %d \n", sum);
  xil_printf(" MultByQuanMul = %x \n", sum);

  // ReLu de tflite
  sum = min( max(sum, 0), 255);
  xil_printf(" sum = min( max(sum, 0), 255) = %d \n", sum);

  // ajouter l'offset de sortie
  sum = sum + offset_sor;
  xil_printf(" sum + offset_sor = %d \n", sum);

  // Clamp la convolution
  sum = min( max(sum, -128), 127);
  xil_printf(" sum = %d \n", sum);



  xil_printf("\n Resultats de FPGA: \n");
  int32_t cv_in = 0;                                       // Valeurs de cartes de carácteristiques précédents
  int32_t res = 0;                                         // Résultat
  res = cv_k5_core(                                        // noyau conv
    0x43C00000,                                            // BaseAddress
    ent[0][0][0][0], ent[0][0][1][0], ent[0][0][2][0], ent[0][0][3][0], ent[0][0][4][0],
    ent[0][1][0][0], ent[0][1][1][0], ent[0][1][2][0], ent[0][1][3][0], ent[0][1][4][0],
    ent[0][2][0][0], ent[0][2][1][0], ent[0][2][2][0], ent[0][2][3][0], ent[0][2][4][0],
    ent[0][3][0][0], ent[0][3][1][0], ent[0][3][2][0], ent[0][3][3][0], ent[0][3][4][0],
    ent[0][4][0][0], ent[0][4][1][0], ent[0][4][2][0], ent[0][4][3][0], ent[0][4][4][0],

    fil[0][0][0][0], fil[0][0][1][0], fil[0][0][2][0], fil[0][0][3][0], fil[0][0][4][0],
    fil[0][1][0][0], fil[0][1][1][0], fil[0][1][2][0], fil[0][1][3][0], fil[0][1][4][0],
    fil[0][2][0][0], fil[0][2][1][0], fil[0][2][2][0], fil[0][2][3][0], fil[0][2][4][0],
    fil[0][3][0][0], fil[0][3][1][0], fil[0][3][2][0], fil[0][3][3][0], fil[0][3][4][0],
    fil[0][4][0][0], fil[0][4][1][0], fil[0][4][2][0], fil[0][4][3][0], fil[0][4][4][0],

    offset_ent, cv_in);

  xil_printf(" W*(x+offset_ent) DEC = %d \n", res);
  xil_printf(" W*(x+offset_ent) HEX = %x \n", res);

  res = tflite_mbqm(                                       // noyau pour tflite mbqm
    0x43C10000,                                            // BaseAddress conv_tflite
	res,
	bias, M0, shift);
  xil_printf(" mbqm + clamp + offset_sor + clamp = sum DEC = %d \n", res);
  xil_printf(" mbqm + clamp + offset_sor + clamp = sum DEC = %x \n", res);

};
