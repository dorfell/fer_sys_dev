/*
 ---------------------------------------------------------------------
 ----                                                             ----
 ---- file:    peripheral.h                                       ----
 ---- brief:   Header périphérique                                ----
 ---- details: Header périphérique                                ----
 ---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
 ---- date:    2021/09/01                                         ----
 ---- version: 0.1                                                ----
 ---------------------------------------------------------------------
 ----                                                             ----
 ---- Copyright (C) 2021 Dorfell Parra                            ----
 ----                    dlparrap@unal.edu.co                     ----
 ----                                                             ----
 ---- This source file may be used and distributed without        ----
 ---- restriction provided that this copyright statement is not   ----
 ---- removed from the file and that any derivative work contains ----
 ---- the original copyright notice and the associated disclaimer.----
 ----                                                             ----
 ----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
 ---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
 ---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
 ---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
 ---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
 ---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
 ---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
 ---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
 ---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
 ---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
 ---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
 ---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
 ---- POSSIBILITY OF SUCH DAMAGE.                                 ----
 ----                                                             ----
 ---------------------------------------------------------------------
 */

#ifndef __PERIPHERAL_H_
#define __PERIPHERAL_H_


/*** Fonction delay   ***/
void delay(int time);




/**********************************************************/
/*** fonctions pour la carte microSD card ***/
/**********************************************************/

/*** Fonction pour lire paramètres de couche dès carte microSD ***/
int ReadParamSD(char FileName[32], uint32_t FileSize, int ParamShape[2], int param[ParamShape[0]][ParamShape[1]]);

/*** Fonction pour lire tenseurs dès carte microSD ***/
int ReadTensorSD(char FileName[32], uint32_t FileSize, int TensorShape[4], int tensor[TensorShape[0]][TensorShape[1]][TensorShape[2]][TensorShape[3]]);
/**********************************************************/



/**********************************************************/
/*** Fonctions pour les GPIOs et UART                   ***/
/**********************************************************/

/*** Fonction pour éprouver les périphérals ***/
int test_peripheral(void);
/**********************************************************/

#endif
