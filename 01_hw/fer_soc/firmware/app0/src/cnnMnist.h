/*
 ---------------------------------------------------------------------
 ----                                                             ----
 ---- file:    cnnMnist.h                                         ----
 ---- brief:   Header modèle CNN pour Mnist                       ----
 ---- details: Header modèle CNN pour Mnist                       ----
 ---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
 ---- date:    2023/06/27                                         ----
 ---- version: 0.1                                                ----
 ---------------------------------------------------------------------
 ----                                                             ----
 ---- Copyright (C) 2023 Dorfell Parra                            ----
 ----                    dlparrap@unal.edu.co                     ----
 ----                                                             ----
 ---- This source file may be used and distributed without        ----
 ---- restriction provided that this copyright statement is not   ----
 ---- removed from the file and that any derivative work contains ----
 ---- the original copyright notice and the associated disclaimer.----
 ----                                                             ----
 ----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
 ---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
 ---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
 ---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
 ---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
 ---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
 ---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
 ---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
 ---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
 ---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
 ---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
 ---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
 ---- POSSIBILITY OF SUCH DAMAGE.                                 ----
 ----                                                             ----
 ---------------------------------------------------------------------
 */

#ifndef __CNNMNIST_H_
#define __CNNMNIST_H_


/*** Initializing Modèle ***/
int init_cnn(void);


/*** cnn inference */
int cnn_inference(int InShape[4], int InTensor[InShape[0]][InShape[1]][InShape[2]][InShape[3]]);


#endif

