/*
---------------------------------------------------------------------
----                                                             ----
---- file:    accel.c                                            ----
---- brief:   Fonctions de l'accelerateur                        ----
---- details: Fonctions pour utiliser l'accelerateur             ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/10/01                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------
*/

#include "conv.h"
#include "conv_tflite.h"
#include "mpool.h"
#include "xil_printf.h"                                    // Data over UART
#include "xil_io.h"


#include <stdbool.h>                                       // bool type
#include <assert.h>                                        // bool type




#define max(a,b)({         \
  __typeof__ (a) _a = (a); \
  __typeof__ (b) _b = (b); \
  _a > _b ? _a : _b;       })

#define min(a,b)({           \
  __typeof__ (a) _a = (a); \
  __typeof__ (b) _b = (b); \
  _a < _b ? _a : _b;       })




/**********************************************************/
/*** TFLite fonctions     ***/
/**********************************************************/
static inline int32_t Dup(int32_t x){
  return x; };

static inline int32_t BitAnd(int32_t a, int32_t b){
  return a & b; };

static inline int32_t BitNot(int32_t a){
  //xil_printf("\r\r  a: %d \n",  a);
  //xil_printf("\r\r ~a: %d \n", ~a);
  return ~a; };

static inline int32_t Add(int32_t a, int32_t b){
  return a + b; };

static inline int32_t ShiftRight(int32_t a, int8_t offset){
  return a >> offset; };

static inline int32_t MaskIfNonZero(int32_t a){
  int32_t zero = 0;
  return a ? BitNot(zero) : zero;  };

static inline int32_t MaskIfGreaterThan(int32_t a, int32_t b){
  return MaskIfNonZero(a > b);  };

static inline int32_t MaskIfLessThan(int32_t a, int32_t b){
  return MaskIfNonZero(a < b); };

static inline int32_t SaturatingRoundingDoublingHighMul(int32_t a, int32_t b){
  //bool overflow = a == b && a == std::numeric_limits<std::int32_t>::min();
  bool overflow = ((a == b) && (a == -2147483648));
  /*int64_t a_64(a); int64_t b_64(b);
  int64_t ab_64 = a_64 * b_64;  */

  int64_t a_64 = (int64_t)(a); int64_t b_64 = (int64_t)(b);
  int64_t ab_64 = a_64 * b_64;
  int32_t nudge = ab_64 >= 0 ? (1 << 30) : (1 - (1 << 30));
  //int32_t ab_x2_high32 =  static_cast<std::int32_t>((ab_64 + nudge) / (1ll << 31));
  int32_t ab_x2_high32 =  (int32_t)((ab_64 + nudge) / (1ll << 31));

  //xil_printf(" -- ab_64_high = %x \n", (uint32_t) (ab_64 >> 32));
  //xil_printf(" -- ab_64_low = %x \n",  (uint32_t)  ab_64);
  //xil_printf(" -- nudge = %x \n",      (uint32_t)  nudge);
  //xil_printf(" -- (ab_64 + nudge) high = %x \n", (uint32_t) ((ab_64+nudge) >> 32) );
  //xil_printf(" -- (ab_64 + nudge) low  = %x \n", (uint32_t) (ab_64+nudge));
  //xil_printf(" -- ab_x2_high32 = %x \n",  (uint32_t) ab_x2_high32);

  //return overflow ? std::numeric_limits<std::int32_t>::max() : ab_x2_high32;
  return overflow ? 2147483647 : ab_x2_high32;
};


static inline int32_t RoundingDivideByPOT(int32_t x, int8_t exponent) {
  assert(exponent >= 0);
  assert(exponent <= 31);
  int32_t mask = Dup((1ll << exponent) - 1);
  int32_t zero = Dup(0);
  int32_t one  = Dup(1);
  int32_t remainder = BitAnd(x, mask);
  int32_t threshold = Add(ShiftRight(mask, 1), BitAnd(MaskIfLessThan(x, zero), one));
  return Add( ShiftRight(x, exponent), BitAnd( MaskIfGreaterThan(remainder, threshold), one ) ); };


static inline int32_t MultiplyByQuantizedMultiplier(int32_t x, int32_t quantized_multiplier, int shift){
  int8_t left_shift  = shift > 0 ? shift :      0;
  int8_t right_shift = shift > 0 ? 0     : -shift;
  return RoundingDivideByPOT(SaturatingRoundingDoublingHighMul(x * (1 << left_shift), quantized_multiplier), right_shift); };

/**********************************************************/




/**********************************************************/
/*** Fonctions noyau - IP cores  ***/
/**********************************************************/

/*** Fonction noyau de la convolution avec kernel (5,5) ***/
int32_t cv_k5_core(
  uint32_t BaseAddress,

  int32_t x01, int32_t x02, int32_t x03, int32_t x04, int32_t x05,
  int32_t x06, int32_t x07, int32_t x08, int32_t x09, int32_t x10,
  int32_t x11, int32_t x12, int32_t x13, int32_t x14, int32_t x15,
  int32_t x16, int32_t x17, int32_t x18, int32_t x19, int32_t x20,
  int32_t x21, int32_t x22, int32_t x23, int32_t x24, int32_t x25,

  int32_t w01, int32_t w02, int32_t w03, int32_t w04, int32_t w05,
  int32_t w06, int32_t w07, int32_t w08, int32_t w09, int32_t w10,
  int32_t w11, int32_t w12, int32_t w13, int32_t w14, int32_t w15,
  int32_t w16, int32_t w17, int32_t w18, int32_t w19, int32_t w20,
  int32_t w21, int32_t w22, int32_t w23, int32_t w24, int32_t w25,

  int32_t offset_ent, int32_t cv_in){

  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG1_OFFSET,  x01);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG2_OFFSET,  x02);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG3_OFFSET,  x03);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG4_OFFSET,  x04);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG5_OFFSET,  x05);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG6_OFFSET,  x06);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG7_OFFSET,  x07);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG8_OFFSET,  x08);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG9_OFFSET,  x09);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG10_OFFSET, x10);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG11_OFFSET, x11);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG12_OFFSET, x12);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG13_OFFSET, x13);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG14_OFFSET, x14);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG15_OFFSET, x15);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG16_OFFSET, x16);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG17_OFFSET, x17);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG18_OFFSET, x18);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG19_OFFSET, x19);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG20_OFFSET, x20);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG21_OFFSET, x21);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG22_OFFSET, x22);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG23_OFFSET, x23);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG24_OFFSET, x24);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG25_OFFSET, x25);

  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG26_OFFSET, w01);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG27_OFFSET, w02);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG28_OFFSET, w03);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG29_OFFSET, w04);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG30_OFFSET, w05);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG31_OFFSET, w06);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG32_OFFSET, w07);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG33_OFFSET, w08);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG34_OFFSET, w09);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG35_OFFSET, w10);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG36_OFFSET, w11);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG37_OFFSET, w12);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG38_OFFSET, w13);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG39_OFFSET, w14);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG40_OFFSET, w15);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG41_OFFSET, w16);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG42_OFFSET, w17);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG43_OFFSET, w18);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG44_OFFSET, w19);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG45_OFFSET, w20);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG46_OFFSET, w21);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG47_OFFSET, w22);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG48_OFFSET, w23);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG49_OFFSET, w24);
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG50_OFFSET, w25);

  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG51_OFFSET, offset_ent);

  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG52_OFFSET, cv_in);

  // Start convolution
  int32_t cv_out = 0;                                       // SaturatingRoundingDoublingHighMul
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG0_OFFSET,  0x00000001);
  while( CONV_mReadReg (BaseAddress, CONV_S00_AXI_SLV_REG54_OFFSET) == 0x00000000 ){};
  cv_out = CONV_mReadReg (BaseAddress, CONV_S00_AXI_SLV_REG53_OFFSET);
  //xil_printf("cv = cv + (x+offset_ent)*w = %x \n", cv_out);

  // Arreter la convolution
  CONV_mWriteReg(BaseAddress, CONV_S00_AXI_SLV_REG0_OFFSET,  0x0000000);

  return cv_out;
};




/*** Fonction noyau de la convolution partie du tflite ***/
int32_t cv_tflite(
  uint32_t BaseAddress,
  int32_t cv_in, int32_t bias,
  int32_t M0, int32_t shift, int32_t offset_sor){

  CONV_TFLITE_mWriteReg(BaseAddress, CONV_TFLITE_S00_AXI_SLV_REG1_OFFSET,  cv_in);
  CONV_TFLITE_mWriteReg(BaseAddress, CONV_TFLITE_S00_AXI_SLV_REG2_OFFSET,  bias);

  CONV_TFLITE_mWriteReg(BaseAddress, CONV_TFLITE_S00_AXI_SLV_REG3_OFFSET,  M0);
  CONV_TFLITE_mWriteReg(BaseAddress, CONV_TFLITE_S00_AXI_SLV_REG4_OFFSET,  shift);
  CONV_TFLITE_mWriteReg(BaseAddress, CONV_TFLITE_S00_AXI_SLV_REG5_OFFSET,  offset_sor);

  // Start tflite part of convolution
  int32_t tf_out = 0;                                      // MBQM + clamp et offset_sor
  CONV_TFLITE_mWriteReg(BaseAddress, CONV_TFLITE_S00_AXI_SLV_REG0_OFFSET,  0x00000001);
  while( CONV_TFLITE_mReadReg (BaseAddress, CONV_TFLITE_S00_AXI_SLV_REG7_OFFSET) == 0x00000000 ){};
  tf_out = CONV_TFLITE_mReadReg (BaseAddress, CONV_TFLITE_S00_AXI_SLV_REG6_OFFSET);
  //xil_printf("mbqm + clamp + offset_sor + clamp = %x \n", tf_out);

  // Arreter la convolution
  CONV_TFLITE_mWriteReg(BaseAddress, CONV_TFLITE_S00_AXI_SLV_REG0_OFFSET,  0x0000000);

  return tf_out;
};




/*** Fonction noyau de MaxPooling avec kernel (2,2) ***/
int32_t mp_22_core(
  uint32_t BaseAddress,
  int32_t x01, int32_t x02, int32_t x03, int32_t x04){

  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG1_OFFSET, x01);
  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG2_OFFSET, x02);
  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG3_OFFSET, x03);
  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG4_OFFSET, x04);

  // Start MP
  int32_t res = 0;
  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG0_OFFSET,  0x00000001);
  while( MPOOL_mReadReg(BaseAddress, MPOOL_S00_AXI_SLV_REG6_OFFSET) == 0x00000000 ){};
  res = MPOOL_mReadReg(BaseAddress, MPOOL_S00_AXI_SLV_REG5_OFFSET);
  //xil_printf("MaxPooling results = %x \n", res);
  //xil_printf("MaxPooling results = %d \n", res);

  // Arreter le MaxPooling
  MPOOL_mWriteReg(BaseAddress, MPOOL_S00_AXI_SLV_REG0_OFFSET,  0x0000000);

  return res;
};




/*** fonction pour padding SAME ***/
void padding(int EntShape[4], int ent[EntShape[0]][EntShape[1]][EntShape[2]][EntShape[3]],
	         int PadShape[4], int pad[PadShape[0]][PadShape[1]][PadShape[2]][PadShape[3]],
			 int zero_point){

  for(int f=0; f<EntShape[0];   f++){                      // boucle pour les différents filtres
    for(int i=0; i<EntShape[1]+4; i++){                    // boucle pour les rangs de l'entrée
      for(int j=0; j<EntShape[2]+4; j++){                  // boucle pour les colonnes de l'entrée
        for(int k=0; k<EntShape[3];   k++){                // boucle pour les cartes de carácteristiques de l'entrée = chaînes de filtres = cc de la entrée

          if( ( ((i>=0) && (i<2)) || ((i>=EntShape[1]+2) && (i<EntShape[1]+4)) ) ||
              ( ((j>=0) && (j<2)) || ((j>=EntShape[2]+2) && (i<EntShape[2]+4)) )    ){
            pad[f][i][j][k] = zero_point; }
          else {
        	pad[f][i][j][k] = ent[f][i-2][j-2][k]; };
        };
      };
    };
  };
};




/**********************************************************/
/*** Fonctiones pour les couches  ***/
/**********************************************************/

/*** Function pour computer une couche de convolution avec kernel (5,5) ***/
void conv_k5(int EntShape[4], int ent[EntShape[0]][EntShape[1]][EntShape[2]][EntShape[3]],
	         int FilShape[4], int fil[FilShape[0]][FilShape[1]][FilShape[2]][FilShape[3]],
	         int ParShape[2], int par[ParShape[0]][ParShape[1]],
			 int CnvShape[4], int cnv[CnvShape[0]][CnvShape[1]][CnvShape[2]][CnvShape[3]]){

  int32_t M0, shift, bias;
  int32_t offset_ent = par[3][0];
  int32_t offset_sor = par[3][1];
  //int32_t zero_point = -offset_ent;
  //xil_printf("offset_ent: %d \n", offset_ent);
  //xil_printf("offset_sor: %d \n", offset_sor);
  //xil_printf("zero_point: %d \n", zero_point);


  for(int f=0; f<FilShape[0];   f++){                      // boucle pour les différents filtres
    M0    = par[0][f];                                     // M0 per filtre
	shift = par[1][f];                                     // shift per filtre
	bias  = par[2][f];                                     // bias
    //xil_printf("M0:     %d \n", M0);
	//xil_printf("shift:  %d \n", shift);
	//xil_printf("bias:   %d \n", bias);
    for(int i=0; i<EntShape[1]-4; i++){                    // boucle pour les rangs de l'entrée
      for(int j=0; j<EntShape[2]-4; j++){                  // boucle pour les colonnes de l'entrée
        for(int k=0; k<EntShape[3];   k++){                // boucle pour les cartes de carácteristiques de l'entrée = chaînes de filtres = cc de la entrée

          // Faire la convolution
          cnv[0][i][j][f] = cv_k5_core(
            0x43C00000,                                    // BaseAddress conv
            ent[0][0+i][0+j][k], ent[0][0+i][1+j][k], ent[0][0+i][2+j][k], ent[0][0+i][3+j][k], ent[0][0+i][4+j][k],
            ent[0][1+i][0+j][k], ent[0][1+i][1+j][k], ent[0][1+i][2+j][k], ent[0][1+i][3+j][k], ent[0][1+i][4+j][k],
            ent[0][2+i][0+j][k], ent[0][2+i][1+j][k], ent[0][2+i][2+j][k], ent[0][2+i][3+j][k], ent[0][2+i][4+j][k],
            ent[0][3+i][0+j][k], ent[0][3+i][1+j][k], ent[0][3+i][2+j][k], ent[0][3+i][3+j][k], ent[0][3+i][4+j][k],
            ent[0][4+i][0+j][k], ent[0][4+i][1+j][k], ent[0][4+i][2+j][k], ent[0][4+i][3+j][k], ent[0][4+i][4+j][k],

            fil[f][0][0][k], fil[f][0][1][k], fil[f][0][2][k], fil[f][0][3][k], fil[f][0][4][k],
            fil[f][1][0][k], fil[f][1][1][k], fil[f][1][2][k], fil[f][1][3][k], fil[f][1][4][k],
            fil[f][2][0][k], fil[f][2][1][k], fil[f][2][2][k], fil[f][2][3][k], fil[f][2][4][k],
            fil[f][3][0][k], fil[f][3][1][k], fil[f][3][2][k], fil[f][3][3][k], fil[f][3][4][k],
            fil[f][4][0][k], fil[f][4][1][k], fil[f][4][2][k], fil[f][4][3][k], fil[f][4][4][k],

            offset_ent, cnv[0][i][j][f]);
        };
        cnv[0][i][j][f] = cv_tflite(
          0x43C10000,                                      // BaseAddress conv_tflite
		  cnv[0][i][j][f],
		  bias, M0, shift, offset_sor);
      };
    };
  };

};




/*** Function pour computer une couche maxpool taille (2, 2) ***/
void maxp_22(int CnvShape[4], int cnv[CnvShape[0]][CnvShape[1]][CnvShape[2]][CnvShape[3]],
	         int MxpShape[4], int mxp[MxpShape[0]][MxpShape[1]][MxpShape[2]][MxpShape[3]]){

  for(int i=0; i<MxpShape[1]; i++){                        // boucle pour les rangs
    for(int j=0; j<MxpShape[2]; j++){                      // boucle pour les colonnes
      for(int k=0; k<MxpShape[3]; k++){                    // boucle pour les cartes de carácteristiques

        // Faire MaxPooling
        mxp[0][i][j][k] = mp_22_core(
          0x43C10000,                                      // BaseAddress
          cnv[0][0+i*2][0+j*2][k], cnv[0][0+i*2][1+j*2][k],
		  cnv[0][1+i*2][0+j*2][k], cnv[0][1+i*2][1+j*2][k] );
      };
    };
  };
};




















/**********************************************************/
/*** TEST fonctions     ***/
/**********************************************************/

/*** Épreuver cv_k5_core   ***/
void cv_k5_core_test(void){

  // Entrée avec padding.
  /*int32_t ent[1][5][5][1] =
    {{{{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-128}, {-128}, {-123}, {-121}, {-117}},
      {{-128}, {-128}, {-123}, {-123}, {-123}},
      {{-128}, {-128}, {-122}, {-124}, {-125}}}};*/

  /*int32_t ent[1][5][5][1] =
    {{{{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-128}, {-123}, {-121}, {-117}, {-109}},
      {{-128}, {-123}, {-123}, {-123}, {-122}},
      {{-128}, {-122}, {-124}, {-125}, {-125}}}};*/

  /*int32_t ent[1][5][5][1] =
    {{{{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-128}, {-128}, {-128}, {-128}, {-128}},
      {{-123}, {-121}, {-117}, {-109}, {-104}},
      {{-123}, {-123}, {-123}, {-122}, {-120}},
      {{-122}, {-124}, {-125}, {-125}, {-124}}}};*/

  int32_t ent[1][5][5][1] =
    {{{{ -53}, { -43}, { -36}, { -25}, { -19}},
      {{   7}, {  -6}, { -10}, { -22}, { -56}},
      {{ -57}, { -77}, { -98}, {-108}, {-103}},
      {{-104}, { -93}, { -65}, { -42}, { -33}},
      {{ -18}, {  -6}, {  -3}, { -10}, { -32}}}};


  /*int32_t fil[1][5][5][1] =
    {{{{ -23}, {  73}, {  22}, { -70}, { -61}},
      {{ -60}, {-106}, {-107}, {  40}, { -61}},
      {{  84}, { -49}, {  45}, {  39}, { -25}},
      {{  68}, { -59}, { -28}, {  44}, {  -8}},
      {{  48}, {  -8}, {-127}, { -50}, {-109}}}};*/

  int32_t fil[1][5][5][1] =
    {{{{  37}, { -32}, { -16}, {   8}, { -96}},
      {{ -78}, {-100}, { -90}, {   8}, { -61}},
      {{  15}, { -20}, { -19}, { -38}, { -61}},
      {{ -22}, { -70}, {  -9}, { -58}, {  -3}},
      {{-121}, {-127}, { -77}, { -32}, {  85}}}};


  // filter 1
  /*int32_t zero_point = -128;
  int32_t offset_ent = -zero_point;
  int32_t bias       = -17;
  int32_t M0         = 1218999674;
  int32_t shift      = -5;
  int32_t offset_sor = -18;*/


  // filter 2
  int32_t zero_point = -128;
  int32_t offset_ent = -zero_point;
  int32_t bias       = 75;
  int32_t M0         = 1549632462;
  int32_t shift      = -5;
  int32_t offset_sor = -18;


  xil_printf("\n cv_k5_core_test() ... \n");
  xil_printf("\n Resultats de CPU: \n");
  int32_t sum = 0;                                         // Résultat
  for(int f=0; f<0x01; f++){
    for(int i=0; i<0x05; i++){
      for(int j=0; j<0x05; j++){
        for(int k=0; k<0x01; k++){
          sum += (ent[f][i][j][k]+offset_ent) * fil[f][i][j][k];
        };
      };
    };
  };

  xil_printf(" W*(x+offset_ent) = %d \n", sum);
  xil_printf(" W*(x+offset_ent) = %x \n", sum);

  sum += bias;
  xil_printf(" W*(x+offset_ent)+bias = %d \n", sum);
  xil_printf(" W*(x+offset_ent)+bias = %x \n", sum);

  int sum1 = 0;
  int8_t right_shift = shift > 0 ? 0 : -shift;
  sum1 = RoundingDivideByPOT(sum, right_shift);
  xil_printf(" RoundingDivideByPOT = %d \n", sum1);
  xil_printf(" RoundingDivideByPOT = %x \n", sum1);

  sum = MultiplyByQuantizedMultiplier(sum, M0, shift);
  xil_printf(" MultByQuanMul = %d \n", sum);
  xil_printf(" MultByQuanMul = %x \n", sum);

  // ReLu de tflite
  sum = min( max(sum, 0), 255);
  xil_printf(" sum = min( max(sum, 0), 255) = %d \n", sum);

  // ajouter l'offset de sortie
  sum = sum + offset_sor;
  xil_printf(" sum + offset_sor = %d \n", sum);

  // Clamp la convolution
  sum = min( max(sum, -128), 127);
  xil_printf(" sum = %d \n", sum);



  xil_printf("\n Resultats de FPGA: \n");
  int32_t cv_in = 0;                                         // Valeurs de cartes de carácteristiques précédents
  int32_t res = 0;                                         // Résultat
  res = cv_k5_core(
    0x43C00000,                                            // BaseAddress
    ent[0][0][0][0], ent[0][0][1][0], ent[0][0][2][0], ent[0][0][3][0], ent[0][0][4][0],
    ent[0][1][0][0], ent[0][1][1][0], ent[0][1][2][0], ent[0][1][3][0], ent[0][1][4][0],
    ent[0][2][0][0], ent[0][2][1][0], ent[0][2][2][0], ent[0][2][3][0], ent[0][2][4][0],
    ent[0][3][0][0], ent[0][3][1][0], ent[0][3][2][0], ent[0][3][3][0], ent[0][3][4][0],
    ent[0][4][0][0], ent[0][4][1][0], ent[0][4][2][0], ent[0][4][3][0], ent[0][4][4][0],

    fil[0][0][0][0], fil[0][0][1][0], fil[0][0][2][0], fil[0][0][3][0], fil[0][0][4][0],
    fil[0][1][0][0], fil[0][1][1][0], fil[0][1][2][0], fil[0][1][3][0], fil[0][1][4][0],
    fil[0][2][0][0], fil[0][2][1][0], fil[0][2][2][0], fil[0][2][3][0], fil[0][2][4][0],
    fil[0][3][0][0], fil[0][3][1][0], fil[0][3][2][0], fil[0][3][3][0], fil[0][3][4][0],
    fil[0][4][0][0], fil[0][4][1][0], fil[0][4][2][0], fil[0][4][3][0], fil[0][4][4][0],

    offset_ent, cv_in);

  xil_printf(" W*(x+offset_ent) DEC = %d \n", res);
  xil_printf(" W*(x+offset_ent) HEX = %x \n", res);
};
