/*
---------------------------------------------------------------------
----                                                             ----
---- file:    M6FER.c                                            ----
---- brief:   Modèle FER M6                                      ----
---- details: Implementation du modèle M6          r             ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/10/19                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------
*/


#include <stdio.h>
#include <stdlib.h>                                        // Malloc, free
#include "platform.h"
#include "xparameters.h"                                   // Information about AXI peripherals
#include "xil_printf.h"                                    // Data over UART
#include "xsdps.h"                                         // SD device driver
#include "ff.h"                                            // FAT file system library

#include "accel.h"
#include "peripheral.h"




/*
  Global variables for M6
*/


/**********************************/
/* Test images	                  */
/**********************************/
int  Status;

char In1Path[32] = "img/img1.npy";
int  In1Size     = 32896;                                  // File size in bytes
int  In1Shape[4] = {1, 64, 64, 1};
int  In1Tensor[1][64][64][1];





/**********************************/
/* Couche 1: conv_k5, tableaux    */
/*           des filtres et       */
/*           paramètres           */
/**********************************/
char Fil1Path[32] = "M6/cv1/fil.npy ";
int  Fil1Size     = 6528;                                  // File size in bytes
int  Fil1Shape[4] = {32, 5, 5, 1};
int  Fil1Tensor[32][5][5][1];

char Par1Path[32] = "M6/cv1/par.npy";
int  Par1Size     = 1152;                                  // File size in bytes
int  Par1Shape[2] = {4, 32};
int  Par1Matrix[4][32];









/*** Initializing Modèle M6 ***/
int init_M6(void){

  /*** Lire img1   ***/
  Status = ReadTensorSD(In1Path, In1Size, In1Shape, In1Tensor);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};


  /**********************************/
  /* Couche 1: Lire filtres et      */
  /*           paramètres           */
  /**********************************/
  Status = ReadTensorSD(Fil1Path, Fil1Size, Fil1Shape, Fil1Tensor);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};

  Status = ReadParamSD(Par1Path, Par1Size, Par1Shape, Par1Matrix);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
    return XST_FAILURE;	};








  //xil_printf("Successfully Read SD Files \r\n");
  return 0;
};






/*** M6 inference */
int M6_inference(int InShape[4], int InTensor[InShape[0]][InShape[1]][InShape[2]][InShape[3]]){

  xil_printf(" M6 inference... \n");

  /*****************************************************/
  /* Couche 1:                                         */
  /*                                                   */
  /*    ---------------------------------------------  */
  /*   | Objectif         | Fonction | Sortie        | */
  /*    ---------------------------------------------  */
  /*   | Padding SAME     | padding  |( 1, 68, 68, 1)| */
  /*   | Convolution (5,5)| conv_k5  |(32, 64, 64, 1)| */
  /*   | MaxPooling  (2,2)| maxp_22  |(32, 32, 32, 1)| */
  /*    ---------------------------------------------  */
  /*****************************************************/
  /*** Padding ***/
  int  Pad1Shape[4] = {1, InShape[1]+4, InShape[2]+4, 1};
  int  Pad1Tensor[Pad1Shape[0]][Pad1Shape[1]][Pad1Shape[2]][Pad1Shape[3]];
  int  zero_point = -Par1Matrix[3][0];                      // zero_point = -offset_ent
  //xil_printf("zero_point: %d \n", zero_point);
  padding(InShape, InTensor, Pad1Shape, Pad1Tensor, zero_point);


  /*** Convolution ***/
  int  Cnv1Shape[4] = {1, Pad1Shape[1]-4, Pad1Shape[2]-4, Fil1Shape[0]};
  int  Cnv1Tensor[Cnv1Shape[0]][Cnv1Shape[1]][Cnv1Shape[2]][Cnv1Shape[3]];
  conv_k5(Pad1Shape, Pad1Tensor, Fil1Shape, Fil1Tensor, Par1Shape, Par1Matrix, Cnv1Shape, Cnv1Tensor);


  /*** MaxPooling ***/
  int  Mxp1Shape[4] = {1, Cn1Shape[1]/2, Cn1Shape[2]/2, Fil1Shape[0]};
  int  Mxp1Tensor[Mxp1Shape[0]][Mxp1Shape[1]][Mxp1Shape[2]][Mxp1Shape[3]];
  maxp_22(Cnv1Shape, Cnv1Tensor, Mxp1Shape, Mxp1Tensor);



  /**********************************/
  /* Couche 1: MaxPooling           */
  /*           (32, 32, 32, 1)      */
  /**********************************/
  // FAIRE LE IPCORE MP.






  /*** Check results ***/
  xil_printf("\n Check InTensor ... \n");
  for(int f=0; f<1; f++){
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
   	    for(int k=0; k<1; k++){
    	  xil_printf("%d, ", InTensor[f][i][j][k]);
        };
      };
      xil_printf(" \n ");
    };
  };


  xil_printf("\n Check PadTensor ... \n");
  for(int f=0; f<1; f++){
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
   	    for(int k=0; k<1; k++){
    	  xil_printf("%d, ", Pad1Tensor[f][i][j][k]);
        };
      };
      xil_printf(" \n ");
    };
  };


  xil_printf("\n Check FilTensor ... \n");
  for(int f=0; f<1; f++){
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
   	    for(int k=0; k<1; k++){
    	  xil_printf("%d, ", Fil1Tensor[f][i][j][k]);
        };
      };
      xil_printf(" \n ");
    };
  };


  xil_printf("\n Check CnvTensor ... \n");
  for(int f=0; f<1; f++){
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
   	    for(int k=0; k<1; k++){
    	  xil_printf("%d, ", Cnv1Tensor[f][i][j][k]);
        };
      };
      xil_printf(" \n ");
    };
  };


  xil_printf("\n Check Mxp1Tensor ... \n");
  for(int f=0; f<1; f++){
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
   	    for(int k=0; k<1; k++){
    	  xil_printf("%d, ", Mxp1Tensor[f][i][j][k]);
        };
      };
      xil_printf(" \n ");
    };
  };


 xil_printf(" \n ");

  return 0;

};
