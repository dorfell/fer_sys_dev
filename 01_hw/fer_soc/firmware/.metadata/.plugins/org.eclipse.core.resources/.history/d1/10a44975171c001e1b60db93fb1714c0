/*
---------------------------------------------------------------------
----                                                             ----
---- file:    main.c                                             ----
---- brief:   Fonction main                                      ----
---- details: Fonction main                                      ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/09/01                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------
*/

#include <stdio.h>
#include <stdlib.h>                                        // Malloc, free
#include "platform.h"
#include "xparameters.h"                                   // Information about AXI peripherals
#include "xil_printf.h"                                    // Data over UART
#include "xsdps.h"                                         // SD device driver
#include "ff.h"                                            // FAT file system library
#include "xtime_l.h"

#include "accel.h"
#include "M6FER.h"
#include "cnnMnist.h"
#include "peripheral.h"


/*** Link to global variables  ***/
extern int In1Shape[4];
extern int In1Tensor[1][64][64][1];                        // Tenseur pour img1 dès JAFFE

extern int In01Shape[4];
extern int In01Tensor[1][28][28][1];                       // Tenseur pour img1 dès Mnist


/*** help                 ***/
static inline void help(void){
 puts("Available commands:");
 puts("***************************************"   );
 puts("help                 - this command."      );
 puts("peri                 - peripheral test."   );
 puts("cvk5                 - cv k5 core test."   );
 puts("M6i1                 - M6+img1 inference." );
 puts("cnn                  - cnn+mnist inference." );
 puts("***************************************"   ); };




/*** Console service      ***/
static inline void console_service(void){

  char token[10] = {};

  XTime tic0, toc0, tic1, toc1;

  scanf("%s", token);                                      // Uart input
  if(strcmp(token,      "help") == 0)
    help();

  else if(strcmp(token, "peri") == 0)
	test_peripheral();

  // fonctions d'accelerateur
  else if(strcmp(token, "cvk5") == 0)
    cv_k5_core_test();

  else if(strcmp(token, "M6i1") == 0)
	XTime_GetTime(&tic0); {
	  M6_inference(In1Shape, In1Tensor); }
    XTime_GetTime(&toc0);
    printf("Elapsed: %f seconds \n", (double)(toc0 - tic0)/COUNTS_PER_SECOND);

  else if(strcmp(token, "cnn") == 0)
	XTime_GetTime(&tic1); {
      cnn_inference(In01Shape, In01Tensor); }
    XTime_GetTime(&toc1);
    printf("Elapsed: %f seconds \n", (double)(toc1 - tic1)/COUNTS_PER_SECOND);

  print("RUNTIME>\n");
};




/**********************************************************/
/*** Main function ***/
/**********************************************************/
int main(){

  init_platform();                                         // Initialization
  init_M6();                                               // Initialization for model M6
  init_cnn();                                              // Initialization for model M6


  print(" \n\r ");
  print("======================\n\r");
  print(" FER SoC              \n\r");
  print(" Par Dorfell Parra    \n\r");
  print(" 2023/06/27           \n\r");
  print("======================\n\r");
  help();

  while(1){
    console_service(); };


  print(" *** END OF PROGRAM *** \n\r");
  cleanup_platform();
  return 0;
};
