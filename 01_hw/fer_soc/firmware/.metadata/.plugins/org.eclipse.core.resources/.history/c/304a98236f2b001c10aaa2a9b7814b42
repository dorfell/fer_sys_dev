/*
---------------------------------------------------------------------
----                                                             ----
---- file:    main.c                                             ----
---- brief:   Fonction main                                      ----
---- details: Fonction main                                      ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/09/01                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------
*/
#include <stdio.h>
#include <stdlib.h>                                        // Malloc, free
#include "platform.h"
#include "xparameters.h"                                   // Information about AXI peripherals
#include "xil_printf.h"                                    // Data over UART
#include "xsdps.h"                                         // SD device driver
#include "ff.h"                                            // FAT file system library

#include "accel.h"
#include "peripheral.h"




/*** help                 ***/
static inline void help(void){
 puts("Available commands:");
 puts("***************************************");
 puts("help                 - this command"    );
 puts("peri                 - peripheral test" );
 puts("conv                 - convolution test");
 puts("***************************************"); };




/*** Console service      ***/
static inline void console_service(void){

  char token[10] = {};

  scanf("%s", token);                                      // Uart input
  if(strcmp(token,      "help") == 0)
    help();
  else if(strcmp(token, "peri") == 0)
	test_peripheral();
  // fonctions d'accelerateur
  else if(strcmp(token, "conv") == 0)
    conv_k5_test();
//  else if(strcmp(token, "read") == 0)
//    ReadFileSD();
  print("RUNTIME>\n");
};




//-----------------------------
// Main function
//-----------------------------
int main(){

  init_platform();                                         // Initialization

  print(" \n\r ");
  print("======================\n\r");
  print(" FER SoC              \n\r");
  print(" Par Dorfell Parra    \n\r");
  print(" 2021/09/01           \n\r");
  print("======================\n\r");
  help();


  /**********************************/
  char InPath[32] = "params/test/ent1.npy";
  int  InSize     = 328;                                   // File size in bytes
  int  InShape[4] = {1, 5, 5, 1};
  int  InTensor[InShape[0]][InShape[1]][InShape[2]][InShape[3]];

  char FilPath[32] = "params/test/fil1.npy";
  int  FilSize     = 328;                                   // File size in bytes
  int  FilShape[4] = {1, 5, 5, 1};
  int  FilTensor[FilShape[0]][FilShape[1]][FilShape[2]][FilShape[3]];

  int Status;
  xil_printf("SD File Read Test \r\n");
  xil_printf("--> fichier: params/test/ent1.npy \n");
  Status = ReadTensorSD(InPath, InSize, InShape, InTensor);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
	return XST_FAILURE;	};
  xil_printf("Successfully Read SD File Test \r\n");

  /*for(int i=0; i<1; i++){
    for(int j=0; j<3; j++){
      for(int k=0; k<3; k++){
        for(int l=0; l<1; l++){
          xil_printf("valeur de tableau: %d \n", ent[i][j][k][l] );
        };
      };
    };
  }; */

  xil_printf("SD File Read Test \r\n");
  xil_printf("--> fichier: params/test/fil1.npy \n");
  Status = ReadTensorSD(FilPath, FilSize, FilShape, FilTensor);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
	return XST_FAILURE;	};
  xil_printf("Successfully Read SD File Test \r\n");

  char ParPath[32] = "params/test/par1.npy";
  int  ParSize     = 1152;                                   // File size in bytes
  int  ParShape[2] = {4, 32};
  int  ParMatrix[ParShape[0]][ParShape[1]];


  xil_printf("SD File Read Test \r\n");
  xil_printf("--> fichier: params/test/par1.npy \n");
  Status = ReadParamSD(ParPath, ParSize, ParShape, ParMatrix);
  if (Status != XST_SUCCESS){
    xil_printf("SD Read File Test failed \r\n");
	return XST_FAILURE;	};
  xil_printf("Successfully Read SD File Test \r\n");

  /*for(int i=0; i<1; i++){
    for(int j=0; j<3; j++){
      for(int k=0; k<3; k++){
        for(int l=0; l<1; l++){
          xil_printf("valeur de tableau: %d \n", ent[i][j][k][l] );
        };
      };
    };
  }; */


  test_conv(InShape, InTensor, FilShape, FilTensor, ParShape, ParMatrix );

  /**********************************/






  while(1){
    console_service(); };


  print(" *** END OF PROGRAM *** \n\r");
  cleanup_platform();
  return 0;
};
