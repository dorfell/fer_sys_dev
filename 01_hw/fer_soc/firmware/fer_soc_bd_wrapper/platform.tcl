# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct /home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/firmware/fer_soc_bd_wrapper/platform.tcl
# 
# OR launch xsct and run below command.
# source /home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/firmware/fer_soc_bd_wrapper/platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {fer_soc_bd_wrapper}\
-hw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}\
-out {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/firmware}

platform write
domain create -name {standalone_ps7_cortexa9_0} -display-name {standalone_ps7_cortexa9_0} -os {standalone} -proc {ps7_cortexa9_0} -runtime {cpp} -arch {32-bit} -support-app {hello_world}
platform generate -domains 
platform active {fer_soc_bd_wrapper}
domain active {zynq_fsbl}
domain active {standalone_ps7_cortexa9_0}
platform generate -quick
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform generate
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform clean
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform clean
platform generate
platform clean
platform generate
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform generate
platform clean
platform generate
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform generate -domains standalone_ps7_cortexa9_0 
platform clean
platform generate
platform generate -domains zynq_fsbl 
platform generate -domains zynq_fsbl 
platform generate -domains 
platform active {fer_soc_bd_wrapper}
bsp reload
domain active {zynq_fsbl}
bsp reload
domain active {zynq_fsbl}
bsp reload
catch {bsp regenerate}
domain active {standalone_ps7_cortexa9_0}
bsp reload
catch {bsp regenerate}
domain active {zynq_fsbl}
bsp reload
bsp write
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
platform clean
platform generate
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
platform clean
platform clean
platform generate
domain active {standalone_ps7_cortexa9_0}
bsp reload
bsp setlib -name xilffs -ver 4.5
bsp write
bsp reload
catch {bsp regenerate}
domain active {zynq_fsbl}
bsp reload
bsp write
domain active {standalone_ps7_cortexa9_0}
bsp write
platform clean
platform clean
platform generate
platform clean
platform clean
platform generate
platform generate -domains standalone_ps7_cortexa9_0 
platform clean
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
domain active {zynq_fsbl}
bsp reload
domain active {standalone_ps7_cortexa9_0}
bsp reload
bsp reload
platform generate -domains 
domain active {zynq_fsbl}
bsp reload
domain active {standalone_ps7_cortexa9_0}
bsp reload
bsp config read_only "false"
bsp reload
domain active {zynq_fsbl}
bsp reload
platform generate -domains 
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform clean
platform generate
platform generate
platform clean
platform generate
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform generate
platform clean
platform generate
platform clean
platform generate
platform generate
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform clean
platform clean
platform generate
platform clean
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform generate
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform clean
platform clean
platform generate
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate -domains 
platform generate
bsp reload
domain active {standalone_ps7_cortexa9_0}
bsp reload
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate
platform generate
platform active {fer_soc_bd_wrapper}
platform config -updatehw {/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/fer_soc_bd_wrapper.xsa}
platform generate
platform clean
platform generate
platform generate
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
