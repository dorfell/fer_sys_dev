
# -*- coding: utf-8 -*-
## 
# @file    par_bin.py
# @brief   Paramètres de la 1ère couche de M6 + jaffe 6 émotions en Numpy avec Tensors
# @details Paramètres de la 1ère couche du modèle M6 entraînée en utilisant Tflite.
#          Le modèle à été entraîné avec Jaffe + dlib + lbp-var64 + aug15 + 6emo.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/10/11
# @version 0.1
"""@package docstring
"""


import numpy as np

print(" \n ");
print("************************************** ");
print(" Création de fichiers binaires avec les");
print(" paramètres pour la couche suivante:   ");
print("                                       ");
print(" Conv2D( filtres=32, kernel=(5,5),     ");
print("   strides=(1,1), padding=SAME,        ");
print("   activation=relu);                   ");
print("************************************** ");

cv1_filtres = \
[[[[ -23],
   [  73],
   [  22],
   [ -70],
   [ -61]],
  [[ -60],
   [-106],
   [-107],
   [  40],
   [ -61]],
  [[  84],
   [ -49],
   [  45],
   [  39],
   [ -25]],
  [[  68],
   [ -59],
   [ -28],
   [  44],
   [  -8]],
  [[  48],
   [  -8],
   [-127],
   [ -50],
   [-109]]],


 [[[  37],
   [ -32],
   [ -16],
   [   8],
   [ -96]],
  [[ -78],
   [-100],
   [ -90],
   [   8],
   [ -61]],
  [[  15],
   [ -20],
   [ -19],
   [ -38],
   [ -61]],
  [[ -22],
   [ -70],
   [  -9],
   [ -58],
   [  -3]],
  [[-121],
   [-127],
   [ -77],
   [ -32],
   [  85]]],


 [[[  31],
   [   3],
   [ -13],
   [  -8],
   [ -83]],
  [[ -48],
   [ -25],
   [ -60],
   [   3],
   [  18]],
  [[  52],
   [ -31],
   [ -65],
   [-127],
   [ -72]],
  [[ -55],
   [  -8],
   [  16],
   [   5],
   [ -54]],
  [[  50],
   [ -42],
   [ -82],
   [ -81],
   [  -4]]],


 [[[  73],
   [ 103],
   [  50],
   [ 105],
   [  40]],
  [[ -21],
   [  17],
   [ -92],
   [  34],
   [  19]],
  [[  72],
   [  -4],
   [  66],
   [  38],
   [ -60]],
  [[   8],
   [  46],
   [  55],
   [ -86],
   [  -6]],
  [[ -56],
   [-127],
   [ -49],
   [ -91],
   [ -51]]],


 [[[  41],
   [  38],
   [ -42],
   [ -47],
   [  27]],
  [[-127],
   [-126],
   [ -74],
   [ -22],
   [  68]],
  [[  17],
   [-101],
   [ -12],
   [   7],
   [ -85]],
  [[ -58],
   [ -96],
   [  14],
   [-114],
   [ -51]],
  [[  51],
   [  14],
   [ -40],
   [ -63],
   [-115]]],


 [[[ -14],
   [ -75],
   [-113],
   [  12],
   [-112]],
  [[  58],
   [ -92],
   [  10],
   [ -67],
   [-119]],
  [[  54],
   [  55],
   [ -41],
   [  54],
   [ -13]],
  [[  48],
   [  40],
   [  72],
   [  10],
   [-109]],
  [[-127],
   [ -68],
   [  48],
   [  77],
   [   0]]],


 [[[ -16],
   [  24],
   [ -22],
   [  58],
   [  20]],
  [[  33],
   [   2],
   [-106],
   [-127],
   [  13]],
  [[ -55],
   [ -95],
   [ -47],
   [ -73],
   [  10]],
  [[ -94],
   [ -67],
   [ -94],
   [  38],
   [  81]],
  [[ -13],
   [ -85],
   [ -64],
   [ -60],
   [  88]]],


 [[[ -40],
   [ -48],
   [  39],
   [  19],
   [  40]],
  [[  48],
   [ -67],
   [ -22],
   [  33],
   [  75]],
  [[ -86],
   [ -30],
   [ -50],
   [  59],
   [  51]],
  [[ 110],
   [ -95],
   [ -93],
   [ -36],
   [  34]],
  [[   7],
   [   7],
   [ -75],
   [-123],
   [-127]]],


 [[[-124],
   [ -70],
   [ -44],
   [-127],
   [-127]],
  [[ -52],
   [ -94],
   [   5],
   [  47],
   [   9]],
  [[  64],
   [  94],
   [   8],
   [ -44],
   [  64]],
  [[ -37],
   [  18],
   [  75],
   [  26],
   [  80]],
  [[-113],
   [  51],
   [  33],
   [-107],
   [ -11]]],


 [[[  -4],
   [   5],
   [  62],
   [ -32],
   [  71]],
  [[ -48],
   [ -96],
   [  43],
   [  73],
   [ -28]],
  [[ -59],
   [-101],
   [  52],
   [  20],
   [  41]],
  [[  22],
   [ -91],
   [  23],
   [ -60],
   [ -49]],
  [[ -88],
   [  41],
   [ -38],
   [ -33],
   [-127]]],


 [[[  24],
   [  53],
   [ -58],
   [-127],
   [ -56]],
  [[  26],
   [ -14],
   [-102],
   [ -24],
   [  39]],
  [[   5],
   [  22],
   [ -73],
   [ -66],
   [ -51]],
  [[ -27],
   [ -41],
   [ -62],
   [  12],
   [-105]],
  [[   2],
   [ -49],
   [ -47],
   [ -52],
   [ -20]]],


 [[[  85],
   [  30],
   [ -60],
   [  66],
   [-127]],
  [[ -58],
   [  91],
   [  96],
   [ 117],
   [ -11]],
  [[-102],
   [  28],
   [  13],
   [ 105],
   [ -53]],
  [[ -71],
   [  27],
   [-127],
   [  34],
   [  51]],
  [[ -12],
   [ -52],
   [ -97],
   [ -32],
   [  34]]],


 [[[ -29],
   [ -54],
   [  32],
   [   5],
   [  53]],

  [[  32],
   [  84],
   [  31],
   [  -1],
   [ -43]],
  [[  35],
   [ -41],
   [ -84],
   [ -20],
   [ -42]],
  [[  -9],
   [-120],
   [-116],
   [  -5],
   [ -40]],
  [[-127],
   [  -6],
   [   7],
   [ -49],
   [  -9]]],


 [[[ -89],
   [ -84],
   [  -2],
   [ -50],
   [  61]],
  [[   1],
   [  -2],
   [ -71],
   [   2],
   [  46]],
  [[-127],
   [  57],
   [ -50],
   [ -57],
   [  14]],
  [[   9],
   [ -13],
   [  32],
   [ -36],
   [  10]],
  [[  58],
   [  71],
   [ -96],
   [ -80],
   [-118]]],


 [[[  22],
   [  -3],
   [ -47],
   [ -47],
   [ -36]],
  [[  -1],
   [ -57],
   [ -25],
   [  54],
   [   8]],
  [[  19],
   [  32],
   [  29],
   [ -35],
   [  59]],
  [[   1],
   [  20],
   [  18],
   [ -35],
   [ -17]],
  [[ -16],
   [ -58],
   [-107],
   [-127],
   [-119]]],


 [[[  36],
   [ -14],
   [  39],
   [  15],
   [ -66]],
  [[ -75],
   [  24],
   [  -9],
   [  18],
   [  21]],
  [[ -91],
   [-127],
   [ -68],
   [ -79],
   [   2]],
  [[ -32],
   [  -5],
   [  12],
   [   2],
   [ -93]],
  [[ -49],
   [ -36],
   [ -33],
   [   4],
   [  15]]],


 [[[ -12],
   [ -68],
   [-126],
   [ -17],
   [  33]],
  [[  50],
   [  -1],
   [-117],
   [ -74],
   [-110]],
  [[ -82],
   [ -33],
   [-108],
   [ -46],
   [  -4]],
  [[  34],
   [ -59],
   [ -35],
   [  29],
   [  83]],
  [[ -44],
   [  35],
   [  25],
   [  21],
   [  17]]],


 [[[  40],
   [  -4],
   [   0],
   [  -9],
   [  27]],
  [[  34],
   [ -23],
   [  42],
   [ -41],
   [ -34]],
  [[ -37],
   [ -18],
   [-127],
   [ -73],
   [   4]],
  [[ -69],
   [ -50],
   [ -82],
   [ -87],
   [ -61]],
  [[  26],
   [  34],
   [ -35],
   [  54],
   [ -33]]],


 [[[ -51],
   [ -94],
   [  -3],
   [ -96],
   [ -30]],
  [[ -42],
   [  -6],
   [ -81],
   [ -37],
   [ -46]],
  [[   3],
   [   1],
   [ -81],
   [   9],
   [ -71]],
  [[  -3],
   [ -79],
   [  20],
   [  -4],
   [  -8]],
  [[-127],
   [ -94],
   [   0],
   [ -65],
   [ -17]]],


 [[[ -14],
   [ -79],
   [  77],
   [  60],
   [-127]],
  [[   0],
   [  44],
   [ -22],
   [  78],
   [ -46]],
  [[  60],
   [   1],
   [-107],
   [ -51],
   [ -23]],
  [[  -4],
   [ -59],
   [ -55],
   [  34],
   [  68]],
  [[ -61],
   [-109],
   [  28],
   [  -6],
   [  18]]],


 [[[ -42],
   [  30],
   [ -57],
   [ -14],
   [ -70]],
  [[ -34],
   [  41],
   [ -19],
   [  11],
   [ -39]],
  [[  21],
   [ -41],
   [ -40],
   [-127],
   [-127]],
  [[  76],
   [ -24],
   [  31],
   [ -76],
   [ -92]],
  [[ -44],
   [ -20],
   [  -9],
   [ -34],
   [  21]]],


 [[[  80],
   [  68],
   [ -34],
   [  51],
   [ -43]],
  [[ -14],
   [ -74],
   [  52],
   [  12],
   [  -6]],
  [[-127],
   [ -73],
   [  14],
   [ -85],
   [ -78]],
  [[ -21],
   [  -9],
   [ -81],
   [   2],
   [ -60]],
  [[  72],
   [  23],
   [  13],
   [  40],
   [   3]]],


 [[[  92],
   [ -46],
   [ -53],
   [ -70],
   [ -41]],
  [[ -29],
   [ -50],
   [  43],
   [ -40],
   [  63]],
  [[-127],
   [ -42],
   [ -70],
   [ -72],
   [ -31]],
  [[-108],
   [   8],
   [ -45],
   [-119],
   [  -9]],
  [[  -7],
   [ -60],
   [ -43],
   [  12],
   [ -59]]],


 [[[ -12],
   [ -17],
   [-127],
   [ -25],
   [ -98]],
  [[  15],
   [ -11],
   [ -68],
   [ -30],
   [ -18]],
  [[  52],
   [   6],
   [ -40],
   [  76],
   [  72]],
  [[  54],
   [ -66],
   [ -98],
   [ -70],
   [  63]],
  [[  59],
   [   5],
   [-122],
   [-115],
   [  50]]],


 [[[   8],
   [ -48],
   [ -31],
   [ -35],
   [ -72]],
  [[-117],
   [  11],
   [  31],
   [ -64],
   [ -86]],
  [[-127],
   [  -3],
   [  20],
   [ -40],
   [-103]],
  [[ -12],
   [-124],
   [  30],
   [ -20],
   [ -38]],
  [[  21],
   [ -62],
   [-107],
   [  43],
   [   7]]],


 [[[-115],
   [ -98],
   [  79],
   [ 117],
   [ -69]],
  [[ -99],
   [  26],
   [  86],
   [ -84],
   [-127]],
  [[ -98],
   [  58],
   [ -83],
   [  66],
   [  16]],
  [[ 106],
   [ 107],
   [  94],
   [  27],
   [  47]],
  [[  48],
   [ 106],
   [  27],
   [ -53],
   [ -86]]],


 [[[  -3],
   [  66],
   [  19],
   [  31],
   [  17]],
  [[ -37],
   [ -24],
   [  47],
   [ -10],
   [  47]],
  [[-122],
   [-127],
   [-103],
   [  33],
   [ -88]],
  [[ -87],
   [   8],
   [ -84],
   [  31],
   [ -63]],
  [[ -49],
   [-108],
   [ -74],
   [  67],
   [ -74]]],


 [[[  42],
   [  72],
   [  17],
   [  85],
   [-112]],
  [[  79],
   [ -72],
   [  45],
   [  25],
   [ -69]],
  [[ -45],
   [  55],
   [  72],
   [  34],
   [-127]],
  [[  98],
   [ -55],
   [  42],
   [ -88],
   [-104]],
  [[ -96],
   [-127],
   [ -72],
   [ -41],
   [ -92]]],


 [[[ -37],
   [-108],
   [  53],
   [  19],
   [  -2]],
  [[   0],
   [  41],
   [ -49],
   [ -81],
   [  28]],
  [[ -64],
   [ -79],
   [  63],
   [  35],
   [  -7]],
  [[   6],
   [   3],
   [   2],
   [ -39],
   [-127]],
  [[-102],
   [  30],
   [-103],
   [ -50],
   [ -57]]],


 [[[  68],
   [  74],
   [ -44],
   [  28],
   [ -10]],
  [[  36],
   [ -99],
   [-127],
   [  31],
   [  -5]],
  [[  88],
   [ -11],
   [  24],
   [  57],
   [ -29]],
  [[ 100],
   [ -75],
   [ -24],
   [  64],
   [ -62]],
  [[ -12],
   [ -99],
   [ -84],
   [ -27],
   [ -87]]],


 [[[ -48],
   [  25],
   [ -48],
   [  32],
   [   4]],
  [[ -59],
   [  26],
   [ -46],
   [  43],
   [   6]],
  [[ -57],
   [-127],
   [  25],
   [ -99],
   [-125]],
  [[ -45],
   [-103],
   [   7],
   [  44],
   [  52]],
  [[ -74],
   [ -44],
   [  43],
   [  71],
   [   3]]],


 [[[  42],
   [  85],
   [ -89],
   [ -60],
   [ -99]],
  [[ -31],
   [ -14],
   [  59],
   [  73],
   [  11]],
  [[ -86],
   [ -85],
   [-124],
   [-127],
   [ -47]],
  [[ -55],
   [-120],
   [ -82],
   [ -53],
   [ -17]],
  [[ 100],
   [   1],
   [   7],
   [  45],
   [ -63]]]];                                              # Couche 15: quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel, Tensor.

cv1_filtres = np.asarray(cv1_filtres); 
print("filtres.shape: ", cv1_filtres.shape);
np.save("fil", cv1_filtres);




print(" \n ");
print("************************************** ");
print(" *** Paramètres de TfLite          *** ");
print(" Valeurs de M0 et shift obtenus a      ");
print(" partir de M et scale                  ");
print("************************************** ");
cv1_M = \
[0.00095265, 0.00121104, 0.00120699, 0.00079292, 0.00098938,
 0.00100186, 0.00090816, 0.00078258, 0.00084234, 0.00114011,
 0.00141139, 0.00074154, 0.00111998, 0.00104284, 0.00139356,
 0.00125942, 0.0010955 , 0.00128346, 0.00131637, 0.00101527,
 0.00119341, 0.00104029, 0.00101142, 0.00102077, 0.00100477,
 0.00081471, 0.00116755, 0.00076776, 0.00097219, 0.00095343,
 0.00102816, 0.000786];                                    # Couche 07: quant_conv2d/: BiasAdd/ReadVariableOp/resource. 
cv1_scale      = 0.05370437;                               # Couche 16: quant_conv2d/: Relu, BiasAdd, Conv2D, ReadVariableOp/resource.




cv1_M0 = \
[1218999674, 1549632462, 1544450130, 2029222110, 1265998947, 1281968208, 1162070796, 2002760226,
 1077848302, 1458871273, 1805997953, 1897731629, 1433113163, 1334405731, 1783182896, 1611538938,
 1401788844, 1642300238, 1684411484, 1299127485, 1527073323, 1331142781, 1294201071, 1306165220,
 1285691809, 2084986562, 1493983172, 1964833233, 1244002827, 1219997753, 1315621377, 2011512609 ];

cv1_shift = \
[ -5, -5, -5, -6, -5, -5, -5, -6,
  -5, -5, -5, -6, -5, -5, -5, -5,
  -5, -5, -5, -5, -5, -5, -5, -5,
  -5, -6, -5, -6, -5, -5, -5, -6 ];

cv1_biases = \
[ -17,  75, -81, -55, -46,-112, -69, -91, -79, -44, -14,-188,
 -103, -58, -78,-111, -60,-130,-100, -30, -63, -68,   0, -86,
  -83,-170, -56, -87,-132,-104, -84,-118];                 # Couche 07: quant_conv2d/: BiasAdd/ReadVariableOp/resource. (int32)

zero_point = -128;                                         # Couche 14: quant_reshape/Reshape. zero_points.

cv1_offset_ent = -zero_point;                              # Couche 14: quant_reshape/Reshape. input_offset = -zero_point.

cv1_offset_sor = -18;                                      # Couche 16: output_offset = zero_point.

cv1_offsets = \
[cv1_offset_ent, cv1_offset_sor, 0, 0, 0, 0, 0, 0,
 0, 0, 0, 0, 0, 0, 0, 0,
 0, 0, 0, 0, 0, 0, 0, 0,
 0, 0, 0, 0, 0, 0, 0, 0 ];




cv1_par = \
[cv1_M0,
 cv1_shift,
 cv1_biases,
 cv1_offsets ];

cv1_par = np.asarray(cv1_par);
print("paramètres structure: ");
print("   [ [cv1_M0      ...]   ");
print("     [cv1_shift   ...]   ");
print("     [cv1_biases  ...]   ");
print("     [cv1_offsets ...] ] ");
print(" \n ");

print("parametres: \n", cv1_par);
print("parametres.shape: ", cv1_par.shape);
np.save("par", cv1_par);




print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
