
# -*- coding: utf-8 -*-
## 
# @file    img_bin.py
# @brief   Créer binaires pour les images du Mnist
# @details Logiciel pour sauvegarder les images de Mnist comme binaires. Ces images
#          ont été traîtes avec: mnist + cnn.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2023/06/26
# @version 0.1
"""@package docstring
"""


import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif'; 
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt 

import numpy as np 
import os 
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3';                  # TF debug messages 
# 0 = all messages are logged (default behavior) 
# 1 = INFO messages are not printed 
# 2 = INFO and WARNING messages are not printed 
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys 
np.set_printoptions(threshold=sys.maxsize)                 # Printing all the weights

from matplotlib.backends.backend_pdf import PdfPages 




def quant_npT( entree, scale, zero_point, verbose):
  ''' Cette fonction fait la quantization utilisée pour
      tflite en utilisant Numpy avec Tensors.
      entree:     tableau Numpy de entrée-Tensor 4D.
      scale:      scale des quantization.
      zero_point: position des zero.   
      clampled:   tableau Numpy de sortie.                 '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  chaines, rangs, colonnes, profondeur = entree.shape;     # [chaînes, rangs, colonnes, profondeur]  
  print("nombre des rangs:    ", rangs);  
  print("nombre des colonnes: ", colonnes);
  clamped = np.zeros( [chaines, rangs, colonnes, profondeur] );
  min_val = -128; max_val = 127;                           # min/max pour int8 
  
  # Faire la quantization
  #----------------------------------  
  unclamped = ( entree / scale ) + zero_point;             # q = r/s + z 
  #print(unclamped);   

  # On utilise clamped pour l'entrée
  for i  in range(0, rangs):                               # rangs
    for j  in range(0, colonnes):                          # colonnes
      clamped[0, i, j, 0] = max( unclamped[0, i, j, 0], min_val );
      clamped[0, i, j, 0] = min(   clamped[0, i, j, 0], max_val );
      clamped[0, i, j, 0] = int( np.round(clamped[0, i, j, 0])  );
  return clamped.astype(np.int64);




print(" \n ");
print("************************************** "); 
print(" Image d'entrée                        "); 
print("************************************** "); 

# Definer l'image d'entrée (64, 64, 1)
img_test = \
[[  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,  84, 185, 159, 151,  60,  36,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0, 222, 254, 254, 254, 254, 241, 198, 198, 198, 198, 198, 198, 198, 198, 170,  52,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,  67, 114,  72, 114, 163, 227, 254, 225, 254, 254, 254, 250, 229, 254, 254, 140,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  17,  66,  14,  67,  67,  67,  59,  21, 236, 254, 106,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  83, 253, 209,  18,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  22, 233, 255,  83,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 129, 254, 238,  44,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  59, 249, 254,  62,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 133, 254, 187,   5,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   9, 205, 248,  58,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 126, 254, 182,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  75, 251, 240,  57,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  19, 221, 254, 166,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   3, 203, 254, 219,  35,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  38, 254, 254,  77,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  31, 224, 254, 115,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 133, 254, 254,  52,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  61, 242, 254, 254,  52,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 121, 254, 254, 219,  40,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 121, 254, 207,  18,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0]];
img_test = np.asarray(img_test);                           # Convertir données à tableau numpy 
img_test = img_test.reshape([1, 28, 28, 1]);               # Reshape à [1, 28, 28, 1]
#np.save("img1", img_test);
print("img_test shape: ", img_test.shape); 
#print("img_test type:  ", type(img_test[0][0][0][0]) );
#print(img_test); 


print(" \n ");
print("************************************** ");
print(" Quantization de l'image               ");
print("************************************** ");
scale = 1.0; zero_point = -128;                            # Couche 6: tfl.quantize
img_test = quant_npT(img_test, scale, zero_point, verbose=1);
np.save("img1", img_test);
print("img_test shape: ", img_test.shape);  print( img_test[0:1, 0:28, 0:28, 0:1] ); 
#sys.exit(0);                                              # Terminer l'execution


# Imprimer l'images 
fig = plt.figure(figsize=(3,3));
img_tmp = img_test.reshape([28, 28]);
plt.imshow(img_tmp, cmap="gray", interpolation=None); 
plt.title("7"); plt.tight_layout(); plt.show(); 
#sys.exit(0);                                              # Terminer l'execution


print("                                      ");
print("**************************************"); 
print("*  ¡Merci d'utiliser ce logiciel!    *"); 
print("*             (8-)                   *"); 
print("**************************************");
