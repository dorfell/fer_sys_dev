
# -*- coding: utf-8 -*-
## 
# @file    par_bin.py
# @brief   Paramètres de la 1ère couche de cnn + Mnist en Numpy avec Tensors
# @details Paramètres de la 1ère couche du modèle cnn entraînée en utilisant Tflite.
#          Le modèle à été entraîné avec cnn + Mnist.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2023/06/26
# @version 0.1
"""@package docstring
"""


import numpy as np

print(" \n ");
print("************************************** ");
print(" Création de fichiers binaires avec les");
print(" paramètres pour la couche suivante:   ");
print("                                       ");
print(" Conv2D( filtres=5, kernel=(5,5),     ");
print("   strides=(1,1), padding=SAME,        ");
print("   activation=relu);                   ");
print("************************************** ");

cv1_filtres = \
[[[[ -45],
   [ -55],
   [  18],
   [   9],
   [  25]],

  [[ -52],
   [  -7],
   [  -4],
   [ -18],
   [  30]],

  [[   4],
   [-124],
   [ -10],
   [ -62],
   [  11]],

  [[  15],
   [ -26],
   [ -44],
   [  -1],
   [ -88]],

  [[  29],
   [  16],
   [ -18],
   [ -30],
   [  18]]],


 [[[   5],
   [  34],
   [  14],
   [  29],
   [  30]],

  [[ -51],
   [  29],
   [  52],
   [ -37],
   [-101]],

  [[ -21],
   [ -84],
   [-125],
   [ -72],
   [ -18]],

  [[ -14],
   [  -7],
   [  14],
   [  33],
   [ -41]],

  [[   3],
   [  -7],
   [   6],
   [  36],
   [ -10]]],


 [[[  12],
   [  -8],
   [ -39],
   [ -53],
   [ -21]],

  [[   0],
   [  -1],
   [   5],
   [-106],
   [   1]],

  [[  11],
   [   4],
   [ -41],
   [-113],
   [ -24]],

  [[  -3],
   [  12],
   [-127],
   [  -1],
   [   6]],

  [[  10],
   [ -42],
   [ -38],
   [ -27],
   [  -2]]],


 [[[ -21],
   [   0],
   [  15],
   [   6],
   [ -37]],

  [[ -52],
   [ -41],
   [  27],
   [  30],
   [ -51]],

  [[  34],
   [ -83],
   [   7],
   [ -38],
   [ -93]],

  [[  -9],
   [   2],
   [-124],
   [ -21],
   [  -9]],

  [[ -36],
   [  -4],
   [ -20],
   [ -24],
   [  15]]],


 [[[  -8],
   [ -10],
   [  35],
   [ -58],
   [ -38]],

  [[  -1],
   [  -5],
   [-127],
   [  -2],
   [  16]],

  [[-107],
   [ -40],
   [ -10],
   [  39],
   [  -6]],

  [[ -47],
   [  40],
   [   1],
   [  20],
   [ -21]],

  [[  -2],
   [  13],
   [ -13],
   [   8],
   [ -94]]]];                                              # Couche 13: quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel, Tensor.
cv1_filtres = np.asarray(cv1_filtres); 
print("filtres.shape: ", cv1_filtres.shape);
np.save("fil", cv1_filtres);




print(" \n ");
print("************************************** ");
print(" *** Paramètres de TfLite          *** ");
print(" Valeurs de M0 et shift obtenus a      ");
print(" partir de M et scale                  ");
print("************************************** ");
cv1_M = \
[0.00113504, 0.00113358, 0.00205739, 0.00242048,
 0.00115395];                                              # Couche 07: conv2d/bias.
cv1_scale      = 0.04729576;                               # Couche 14: quant_conv2d/: Relu, BiasAdd, Conv2D, ReadVariableOp/resource.

cv1_M0 = \
[ 1649182820, 1647061479, 1494666374, 1758446413, 1676658546];

cv1_shift = \
[ -5, -5, -4, -4, -5];

cv1_biases = \
[ 22, -467, -143,   10, -617];                             # Couche 07: conv2d/bias (int32).

zero_point = -128;                                         # Couche 12: quant_reshape/Reshape. zero_points.

cv1_offset_ent = -zero_point;                              # Couche 12: quant_reshape/Reshape. input_offset = -zero_point.

cv1_offset_sor = -1;                                       # Couche 14: output_offset = zero_point.

cv1_offsets = \
[cv1_offset_ent, cv1_offset_sor, 0, 0, 0];

cv1_par = \
[cv1_M0,
 cv1_shift,
 cv1_biases,
 cv1_offsets ];

cv1_par = np.asarray(cv1_par);
print("paramètres structure: ");
print("   [ [cv1_M0      ...]   ");
print("     [cv1_shift   ...]   ");
print("     [cv1_biases  ...]   ");
print("     [cv1_offsets ...] ] ");
print(" \n ");

print("parametres: \n", cv1_par);
print("parametres.shape: ", cv1_par.shape);
np.save("par", cv1_par);




print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
