#
# This file is not part of LiteX (8-P.
#
# Copyright (c) 2021 Dorfell Parra <dlparrap@unal.edu.cok>
# SPDX-License-Identifier: BSD-2-Clause

import os

from migen import *

from litex.soc.interconnect.csr import *
from litex.soc.integration.doc  import AutoDoc, ModuleDoc


# RTL CONV    ------------------------------------------------------------------------------
class RTLCONV(Module, AutoCSR, AutoDoc):
  """Module pour faire la convolution en VHDL
     Module pour faire la convolution entre tenseur en suivant 
     la facon de Tensorflow Lite. Le noyau est écrit en VHDL.
     Noyau connecté en utilisant l'interface CSR. 
  """


  def __init__(self, platform):
    self.intro = ModuleDoc("CONV: Convolution based on tflite on VHDL. The core is connected to the SoC by means of the CSR interface.");

    self.start = CSRStorage(name="start", size=1, reset=0x0, description="""signal start du noyau.""");

    self.x01 = CSRStorage(name="x01", size=8, reset=0x00, description="""element 01 de l'entree.""");
    self.x02 = CSRStorage(name="x02", size=8, reset=0x00, description="""element 02 de l'entree.""");
    self.x03 = CSRStorage(name="x03", size=8, reset=0x00, description="""element 03 de l'entree.""");
    self.x04 = CSRStorage(name="x04", size=8, reset=0x00, description="""element 04 de l'entree.""");
    self.x05 = CSRStorage(name="x05", size=8, reset=0x00, description="""element 05 de l'entree.""");
    self.x06 = CSRStorage(name="x06", size=8, reset=0x00, description="""element 06 de l'entree.""");
    self.x07 = CSRStorage(name="x07", size=8, reset=0x00, description="""element 07 de l'entree.""");
    self.x08 = CSRStorage(name="x08", size=8, reset=0x00, description="""element 08 de l'entree.""");
    self.x09 = CSRStorage(name="x09", size=8, reset=0x00, description="""element 09 de l'entree.""");
    self.x10 = CSRStorage(name="x10", size=8, reset=0x00, description="""element 10 de l'entree.""");
    self.x11 = CSRStorage(name="x11", size=8, reset=0x00, description="""element 11 de l'entree.""");
    self.x12 = CSRStorage(name="x12", size=8, reset=0x00, description="""element 12 de l'entree.""");
    self.x13 = CSRStorage(name="x13", size=8, reset=0x00, description="""element 13 de l'entree.""");
    self.x14 = CSRStorage(name="x14", size=8, reset=0x00, description="""element 14 de l'entree.""");
    self.x15 = CSRStorage(name="x15", size=8, reset=0x00, description="""element 15 de l'entree.""");
    self.x16 = CSRStorage(name="x16", size=8, reset=0x00, description="""element 16 de l'entree.""");
    self.x17 = CSRStorage(name="x17", size=8, reset=0x00, description="""element 17 de l'entree.""");
    self.x18 = CSRStorage(name="x18", size=8, reset=0x00, description="""element 18 de l'entree.""");
    self.x19 = CSRStorage(name="x19", size=8, reset=0x00, description="""element 19 de l'entree.""");
    self.x20 = CSRStorage(name="x20", size=8, reset=0x00, description="""element 20 de l'entree.""");
    self.x21 = CSRStorage(name="x21", size=8, reset=0x00, description="""element 21 de l'entree.""");
    self.x22 = CSRStorage(name="x22", size=8, reset=0x00, description="""element 22 de l'entree.""");
    self.x23 = CSRStorage(name="x23", size=8, reset=0x00, description="""element 23 de l'entree.""");
    self.x24 = CSRStorage(name="x24", size=8, reset=0x00, description="""element 24 de l'entree.""");
    self.x25 = CSRStorage(name="x25", size=8, reset=0x00, description="""element 25 de l'entree.""");

    self.w01 = CSRStorage(name="w01", size=8, reset=0x00, description="""element 01 du filtre.""");
    self.w02 = CSRStorage(name="w02", size=8, reset=0x00, description="""element 02 du filtre.""");
    self.w03 = CSRStorage(name="w03", size=8, reset=0x00, description="""element 03 du filtre.""");
    self.w04 = CSRStorage(name="w04", size=8, reset=0x00, description="""element 04 du filtre.""");
    self.w05 = CSRStorage(name="w05", size=8, reset=0x00, description="""element 05 du filtre.""");
    self.w06 = CSRStorage(name="w06", size=8, reset=0x00, description="""element 06 du filtre.""");
    self.w07 = CSRStorage(name="w07", size=8, reset=0x00, description="""element 07 du filtre.""");
    self.w08 = CSRStorage(name="w08", size=8, reset=0x00, description="""element 08 du filtre.""");
    self.w09 = CSRStorage(name="w09", size=8, reset=0x00, description="""element 09 du filtre.""");
    self.w10 = CSRStorage(name="w10", size=8, reset=0x00, description="""element 10 du filtre.""");
    self.w11 = CSRStorage(name="w11", size=8, reset=0x00, description="""element 11 du filtre.""");
    self.w12 = CSRStorage(name="w12", size=8, reset=0x00, description="""element 12 du filtre.""");
    self.w13 = CSRStorage(name="w13", size=8, reset=0x00, description="""element 13 du filtre.""");
    self.w14 = CSRStorage(name="w14", size=8, reset=0x00, description="""element 14 du filtre.""");
    self.w15 = CSRStorage(name="w15", size=8, reset=0x00, description="""element 15 du filtre.""");
    self.w16 = CSRStorage(name="w16", size=8, reset=0x00, description="""element 16 du filtre.""");
    self.w17 = CSRStorage(name="w17", size=8, reset=0x00, description="""element 17 du filtre.""");
    self.w18 = CSRStorage(name="w18", size=8, reset=0x00, description="""element 18 du filtre.""");
    self.w19 = CSRStorage(name="w19", size=8, reset=0x00, description="""element 19 du filtre.""");
    self.w20 = CSRStorage(name="w20", size=8, reset=0x00, description="""element 20 du filtre.""");
    self.w21 = CSRStorage(name="w21", size=8, reset=0x00, description="""element 21 du filtre.""");
    self.w22 = CSRStorage(name="w22", size=8, reset=0x00, description="""element 22 du filtre.""");
    self.w23 = CSRStorage(name="w23", size=8, reset=0x00, description="""element 23 du filtre.""");
    self.w24 = CSRStorage(name="w24", size=8, reset=0x00, description="""element 24 du filtre.""");
    self.w25 = CSRStorage(name="w25", size=8, reset=0x00, description="""element 25 du filtre.""");

#    self.zero_point = CSRStorage(name="zero_point", size=32, reset=0x00000000, description="""zero_point.""");
    self.offset_ent = CSRStorage(name="offset_ent", size=16, reset=0x0000,     description="""offset_ent.""");
#    self.offset_sor = CSRStorage(name="offset_sor", size=32, reset=0x00000000, description="""offset_sor.""");
    self.bias       = CSRStorage(name="bias",       size=32, reset=0x00000000, description="""bias.""");
    self.M0         = CSRStorage(name="M0",         size=32, reset=0x00000000, description="""M0.""");
#    self.shift      = CSRStorage(name="shift",      size=8,  reset=0x00,       description="""shift.""");

    self.cv_out     = CSRStatus(name="cv_out",      size=32, reset=0x00000000, description="""sortie.""");
    self.dn_cv      = CSRStatus(name="dn_cv",       size=1,  reset=0x0,        description="""Done de la convolution.""");

    # Paramètres
    self.params = dict(

      # Clock, reset, start.
      i_clk = ClockSignal(),
      i_rst = ResetSignal(),

      i_start = self.start.storage,

      # entrée
      i_x01 = self.x01.storage,
      i_x02 = self.x02.storage,
      i_x03 = self.x03.storage,
      i_x04 = self.x04.storage,
      i_x05 = self.x05.storage,
      i_x06 = self.x06.storage,
      i_x07 = self.x07.storage,
      i_x08 = self.x08.storage,
      i_x09 = self.x09.storage,
      i_x10 = self.x10.storage,
      i_x11 = self.x11.storage,
      i_x12 = self.x12.storage,
      i_x13 = self.x13.storage,
      i_x14 = self.x14.storage,
      i_x15 = self.x15.storage,
      i_x16 = self.x16.storage,
      i_x17 = self.x17.storage,
      i_x18 = self.x18.storage,
      i_x19 = self.x19.storage,
      i_x20 = self.x20.storage,
      i_x21 = self.x21.storage,
      i_x22 = self.x22.storage,
      i_x23 = self.x23.storage,
      i_x24 = self.x24.storage,
      i_x25 = self.x25.storage,

      # filtre
      i_w01 = self.w01.storage,
      i_w02 = self.w02.storage,
      i_w03 = self.w03.storage,
      i_w04 = self.w04.storage,
      i_w05 = self.w05.storage,
      i_w06 = self.w06.storage,
      i_w07 = self.w07.storage,
      i_w08 = self.w08.storage,
      i_w09 = self.w09.storage,
      i_w10 = self.w10.storage,
      i_w11 = self.w11.storage,
      i_w12 = self.w12.storage,
      i_w13 = self.w13.storage,
      i_w14 = self.w14.storage,
      i_w15 = self.w15.storage,
      i_w16 = self.w16.storage,
      i_w17 = self.w17.storage,
      i_w18 = self.w18.storage,
      i_w19 = self.w19.storage,
      i_w20 = self.w20.storage,
      i_w21 = self.w21.storage,
      i_w22 = self.w22.storage,
      i_w23 = self.w23.storage,
      i_w24 = self.w24.storage,
      i_w25 = self.w25.storage,

      # Configuration de la convolution
#      i_zero_point = self.zero_point.storage,
      i_offset_ent = self.offset_ent.storage,
      #i_offset_sor = self.offset_sor.storage,
      i_bias       = self.bias.storage,
      i_M0         = self.M0.storage,
 #     i_shift      = self.shift.storage,

      # sortie
      o_cv_out     = self.cv_out.status,
      o_dn_cv      = self.dn_cv.status
    );

    # Add VHDL sources.
    self.add_sources(platform);


  @staticmethod
  def add_sources(platform):
    sources = [

      # conv: top.
      "conv_top.vhd",

      # conv: core_top.
      "conv_core_top.vhd",

      # conv: core0.
      "conv_core0.vhd",

      # conv: core1.
      "conv_core1.vhd",

      # conv: core2.
      "conv_core2.vhd",

      # conv: core3.
      "conv_core3.vhd",

      # conv: tflite top.
      "conv_tflite.vhd",

      # conv: tflite fsm.
      #"tflite_fsm.vhd",

      # conv: tflite core0.
      "tflite_core0.vhd",

      # conv: tflite core1.
      #"tflite_core1.vhd",

      # conv: tflite core2.
      #"tflite_core2.vhd",

      # conv: tflite core3.
      #"tflite_core3.vhd" 
     
    ];

    # Direct use of VHDL sources.
    platform.add_sources("gateware/conv/", *sources)


  def do_finalize(self):
    self.specials += Instance("conv_top", **self.params);
