---------------------------------------------------------------------
----                                                             ----
---- file:    conv_tflite.vhd                                    ----
---- brief:   Module de Convolution                              ----
---- details: Module pour implementer la fonction du tflite      ----
----          MultiplyByQuantizedMultiplier.                     ----
----          Les entrées sont xfb, M0 et shift.                 ----
----          Le résultat est un nombre entier.                  ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/06/20                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity conv_tflite is
  port( 
    --clk, rst :  in std_logic;
    --clk :  in std_logic;
    --start    :  in std_logic;
   
    -- somme(x+offset_ent)*w + bias
    xowb     :  in signed(31 downto 0);


    -- M0
    M0       :  in signed(31 downto 0);

    -- shift
    --shift    :  in signed( 7 downto 0);

    -- sortie du noyau (MultiplyByQuantizedMultiplier)
    mbqm     : out signed(31 downto 0) ); 
    
    -- Done de noyau MultiplyByQuantizedMultiplier
    --dn_mbqm  : out std_logic );
end conv_tflite;


architecture Behavioral of conv_tflite is


  -- Component Declaration for the instances
  ------------------------------------------------------
  
  -- tflite_fsm
--  component tflite_fsm is
--    port( 
--      clk, rst :  in std_logic;
--      start    :  in std_logic;

      -- shift
--      shift   :  in integer range -128 to 127;

      -- SaturatingRoundingDoublingHighMul (SRDHM) sortie
--      srdhm   : in integer range -2147483648 to 2147483647;

      -- left_shift
--      vr_ls   : out integer range -128 to 127;

      -- sortie du noyau (MultiplyByQuantizedMultiplier)
--      mbqm    : out integer range -2147483648 to 2147483647; 
    
      -- Done de noyau MultiplyByQuantizedMultiplier
--      dn_mbqm : out std_logic );
--  end component;


  -- tflite_core0
  component tflite_core0 is
    port( 
      --clk      :  in std_logic;

      -- somme(x+offset_ent)*w + bias$
      xowb     :  in signed(31 downto 0);
         
      -- quantized_multiplier (M0)
      quantized_multiplier :  in signed(31 downto 0);

      -- shift
      --shift    :  in signed( 7 downto 0);

      -- MultiplyByQuantizedMultiplier (MBQM) sortie
      -- x * (1 << left_shift)
      overflow : out std_logic;
      xls      : out signed(31 downto 0) );
  end component tflite_core0;
  
  
  -- tflite_core1
--  component tflite_core1 is
--    port( 
--      clk      :  in std_logic;

      -- x * (1 << left_shift) 
--      xls      :  in integer range -2147483648 to 2147483647;
        
      -- quantized_multiplier (M0)
--      quantized_multiplier :  in integer range -2147483648 to 2147483647;
    
      -- SaturatingRoundingDoublingHighMul (SRDHM)
      -- ab_64 = (int32_t)(a) * (int32_t)(b)
--      ab_64    : out signed(63 downto 0);
--      nudge    : out signed(31 downto 0) );
--  end component tflite_core1;

  
  -- tflite_core2
--  component tflite_core2 is
--    port( 
--      clk      :  in std_logic;

      -- ab_64 = (int32_t)(a) * (int32_t)(b)
--      ab_64    :  in signed(63 downto 0);
--      nudge    :  in signed(31 downto 0);
    
      -- SaturatingRoundingDoublingHighMul (SRDHM)
      -- (ab_64 + nudge)
--      ab_nudge : out signed(47 downto 0) );
--  end component tflite_core2;

  
  -- tflite_core3
--  component tflite_core3 is
--    port( 
--      clk      :  in std_logic;

      -- SaturatingRoundingDoublingHighMul (SRDHM) 
--      overflow :  in std_logic;
      -- (ab_64 + nudge)
--      ab_nudge :  in signed(47 downto 0);

      -- SaturatingRoundingDoublingHighMul (SRDHM) sortie
      -- srdhm <= 2147483647 when overflow = '1' else sg_ab_x2_high32;
--      srdhm    : out integer range -2147483648 to 2147483647 );
--  end component tflite_core3;
  

  signal sg_xls, sg_srdhm : integer range -2147483648 to 2147483647;
  signal sg_vr_ls         : integer range -128 to 127;
  signal sg_ab_64         : signed(63 downto 0);
  signal sg_ab_nudge      : signed(47 downto 0);
  signal sg_nudge         : signed(31 downto 0);
  signal sg_overflow      : std_logic;
  
  
begin


  --! Instantiate the submodules (SM)
  -----------------------------------------
--  SM: tflite_fsm
--    port map(
--      clk     => clk,
--      rst     => rst,
--      start   => start,
      
--      shift   => shift,
      --srdhm   => sg_srdhm,
      --srdhm   => sg_xls,
--      srdhm   => xfb,
--      vr_ls   => sg_vr_ls,
      
--      mbqm    => mbqm, 
--      dn_mbqm => dn_mbqm );
      
  
  SM0: tflite_core0
    port map(
      --clk      => clk,
      
      xowb     => xowb,
      quantized_multiplier => M0,
      --shift    => shift,
      
      --overflow => sg_overflow,
      overflow => open,
      --xls      => sg_xls );
      xls      => mbqm );
 
  
--  SM1: tflite_core1
--    port map(
--      clk      => clk,
      
--      xls      => sg_xls,
--      quantized_multiplier => M0,
      
--      ab_64    => sg_ab_64,
--      nudge    => sg_nudge );

  
--  SM2: tflite_core2
--    port map(
--      clk      => clk,
      
--      ab_64    => sg_ab_64,
--      nudge    => sg_nudge,
      
--      ab_nudge => sg_ab_nudge );
 
  
--  SM3: tflite_core3
--    port map(
--      clk      => clk,
      
--      overflow => sg_overflow,
--      ab_nudge => sg_ab_nudge,
      
--      srdhm    => sg_srdhm );
      
end Behavioral;
