---------------------------------------------------------------------
----                                                             ----
---- file:    tflite_core3.vhd                                   ----
---- brief:   Module noyau 3 de tflite                           ----
---- details: Module pour implementer les operations             ----
----          arithmetiques qui ont besoint de pipeline.         ----
----          SaturatingRoundingDoublingHighMul                  ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/07/04                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity tflite_core3 is
  port( 
    clk      :  in std_logic;

    -- SaturatingRoundingDoublingHighMul (SRDHM) 
    overflow :  in std_logic;
    -- (ab_64 + nudge)
    ab_nudge :  in signed(47 downto 0);

    -- SaturatingRoundingDoublingHighMul (SRDHM) sortie
    -- srdhm <= 2147483647 when overflow = '1' else sg_ab_x2_high32;
    srdhm    : out integer range -2147483648 to 2147483647 );
end tflite_core3;


architecture Behavioral of tflite_core3 is
  
  
  -- ab_x2: signaux pour les registres de pipeline. registers de pipeline p0, p1, p2 ...
  signal sg_ab_nudge_p0, sg_ab_nudge_p1, sg_ab_nudge_p2 : signed(47 downto 0);  
  signal sg_ab_nudge_p3, sg_ab_nudge_p4, sg_ab_nudge_p5 : signed(47 downto 0);  
  signal sg_sl_31_p0,    sg_sl_31_p1,    sg_sl_31_p2    : signed(47 downto 0);
  --signal sg_sl_31_p3,    sg_sl_31_p4,    sg_sl_31_p5    : signed(63 downto 0);
  signal sg_ab_x2_p0,    sg_ab_x2_p1,    sg_ab_x2_p2    : signed(47 downto 0);
  --signal sg_ab_x2_p3,    sg_ab_x2_p4,    sg_ab_x2_p5    : signed(63 downto 0);
  
  
  -- Signaux pour SaturatingRoundingDoublingHighMul
  signal sg_ls_1         : signed(47 downto 0);
  signal sg_ab_x2_high32 : integer range -2147483648 to 2147483647;


  signal sg_ab_nudge_64         : signed(63 downto 0);


  attribute USE_DSP : string;
  attribute USE_DSP of sg_ab_x2_p0 : signal is "YES";

  
begin


  -- (1ll << 31);
  sg_ls_1 <= shift_left(to_signed(1,48), 31);


  -- Depuis SaturatingRoundingDoublingHighMul
  --process(clk)
  --  begin
  --    if (clk'event and clk = '1') then
      
        -- ab_x2_high32 = static_cast<std::int32_t>((ab_64 + nudge) / (1ll << 31));
        -- se rappeler ab_nudge = (ab_64 + nudge)
--        sg_ab_nudge_p0 <= ab_nudge;
--        sg_ab_nudge_p1 <= sg_ab_nudge_p0;
--        sg_ab_nudge_p2 <= sg_ab_nudge_p1;
--        sg_ab_nudge_p3 <= sg_ab_nudge_p2;
--        sg_ab_nudge_p4 <= sg_ab_nudge_p3;
--        sg_ab_nudge_p5 <= sg_ab_nudge_p4;
         
--        sg_sl_31_p0 <= sg_ls_1;
--        sg_sl_31_p1 <= sg_sl_31_p0;
--        sg_sl_31_p2 <= sg_sl_31_p1;
--        --sg_sl_31_p3 <= sg_sl_31_p2;
        --sg_sl_31_p4 <= sg_sl_31_p3;
        --sg_sl_31_p5 <= sg_sl_31_p4;
        
        --sg_ab_x2_p0 <= sg_ab_nudge_p5 / sg_sl_31_p2;
        --sg_ab_x2_p0 <= sg_ab_nudge_p5 / sg_sl_31_p2;
        --sg_ab_x2_p0 <= sg_ab_nudge_p5 / sg_sl_31_p5;
        --sg_ab_x2_p0 <= sg_ab_nudge_p2 - sg_sl_31_p2;
        --sg_ab_x2_p0 <= sg_ab_nudge_p5 - sg_sl_31_p2;
        --sg_ab_x2_p1 <= sg_ab_x2_p0;
        --sg_ab_x2_p2 <= sg_ab_x2_p1;
        --sg_ab_x2_p3 <= sg_ab_x2_p2;
        --sg_ab_x2_p4 <= sg_ab_x2_p3;
        --sg_ab_x2_p5 <= sg_ab_x2_p4;
  
        --vr_ls := to_integer( shift_left(to_signed(1,32), left_shift) ); 
        --sg_ab_x2_p0 <= shift_right(ab_nudge, 31); 
  
 --     end if;
 -- end process;


  --..sg_ab_x2_high32 <= to_integer(sg_ab_x2_p0);
  --sg_ab_x2_high32 <= to_integer(sg_ab_x2_p5);

 
        
  -- SRDHM sortie
  --srdhm    <= 2147483647 when overflow = '1' else sg_ab_x2_high32;
  
  
  
  --sg_ab_x2_p0 <= shift_right(ab_nudge, 31);  
  --sg_ab_nudge_64  <= resize(ab_nudge, 64);
  --sg_ab_x2_high32 <= to_integer(sg_ab_nudge_64(63 downto 31)); 
  
    --srdhm <= to_integer( sg_ab_nudge_64(64 downto 32) );
    srdhm <= to_integer( ab_nudge(31 downto  0) );
  
  
end Behavioral;
