---------------------------------------------------------------------
----                                                             ----
---- file:    tflite_fsm.vhd                                     ----
---- brief:   Module de FSM pour conv_core_tflite                ----
---- details: Module pour implementer le control des             ----
----          tflite_corex.vhd en utilisant une FSM.             ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/07/04                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity tflite_fsm is
  port( 
    clk, rst :  in std_logic;
    start    :  in std_logic;

    -- shift
    shift    :  in integer range -128 to 127;

    -- SaturatingRoundingDoublingHighMul (SRDHM) sortie
    srdhm    :  in integer range -2147483648 to 2147483647;

    -- left_shift
    vr_ls    : out integer range -128 to 127;

    -- sortie du noyau (MultiplyByQuantizedMultiplier)
    mbqm     : out integer range -2147483648 to 2147483647; 
    
    -- Done de noyau MultiplyByQuantizedMultiplier
    dn_mbqm  : out std_logic );
end tflite_fsm;


architecture Behavioral of tflite_fsm is


  -- MaskIfNonZero
  function MaskIfNonZero(
    a : integer :=0) return integer is
    begin
      if (a = 1) then
        return -1;
      else
        return  0;
      end if;
  end function MaskIfNonZero;


  -- MaskIfGreaterThan
  function MaskIfGreaterThan(
    a : integer :=0;
    b : integer :=0) return integer is
    begin
      if (a > b) then
        return MaskIfNonZero(1);
      else
        return MaskIfNonZero(0);
      end if;
  end function MaskIfGreaterThan;


  -- MaskIfLessThan
  function MaskIfLessThan(
    a : integer :=0;
    b : integer :=0) return integer is
    begin
      if (a < b) then
        return MaskIfNonZero(1);
      else
        return MaskIfNonZero(0);
      end if;
  end function MaskIfLessThan;
  
  
  -- RoundingDivideByPOT
  function RoundingDivideByPOT(
    x        : integer :=0;
    exponent : integer :=0) return integer is
  
    variable mask      : integer range -2147483648 to 2147483647;
    variable remainder : integer range -2147483648 to 2147483647;
    variable threshold : integer range -2147483648 to 2147483647;
    variable vr_rdbp   : integer range -2147483648 to 2147483647;

    begin
    
      -- mask
      mask      := to_integer(shift_left(to_signed(1,64), exponent)) - 1;
      
      -- remainder
      remainder := to_integer( to_signed(x,32) and to_signed(mask,32) );

      -- threshold
      threshold := to_integer( shift_right(to_signed(mask,32), 1) ) + to_integer( to_signed(MaskIfLessThan(x, 0),32) and to_signed(1,32) );
      
      -- vr_out
      vr_rdbp   :=  to_integer( shift_right(to_signed(x,32), exponent) ) + to_integer( to_signed(MaskIfGreaterThan(remainder, threshold),32) and to_signed(1,32) ); 
      return vr_rdbp;
  end function RoundingDivideByPOT;


  -- fun_left_shift 
  function fun_left_shift(
    shift                : integer :=0) return integer is
    
    variable left_shift  : integer range -128 to 127;
    variable vr_ls       : integer range -128 to 127;
    
    begin
    
      -- left_shift
      if (shift > 0 ) then 
        left_shift  := shift;
      else 
        left_shift  :=  0;
      end if;

      -- (1 << left_shift)
      vr_ls := to_integer( shift_left(to_signed(1,32), left_shift) ); 
      return vr_ls;
  end function fun_left_shift;


  -- fun_right_shift 
  function fun_right_shift(
    shift                : integer :=0) return integer is
    
    variable right_shift : integer range -128 to 127;
    variable vr_rs       : integer range -128 to 127;
    
    begin
    
      -- right_shift
      if (shift > 0 ) then 
        right_shift := 0;
      else 
        right_shift := -shift;
      end if;

      vr_rs := right_shift; 
      return vr_rs;
  end function fun_right_shift;

  
  type states is (S_IDLE, S_MBQM, S_SRDHM0, S_SRDHM1, S_SRDHM2, S_RDBP);
  signal present_state, next_state: states;

  -- Compteur de 08 cycles: registre, enable,done. 
  signal dd08,    qq08    : std_logic_vector(2 downto 0) := (others=>'0');
  signal en_cc08, dn_cc08 : std_logic; 


  -- Compteur de 12 cycles: registre, enable,done. 
  signal dd12, qq12       : std_logic_vector(3 downto 0) := (others=>'0');
  signal en_cc12, dn_cc12 : std_logic; 

    
  -- Compteur de 16 cycles: registre, enable,done. 
  signal dd16, qq16       : std_logic_vector(3 downto 0) := (others=>'0');
  signal en_cc16, dn_cc16 : std_logic; 


begin


  -- Present State
  ---------------------
  process(clk, rst)
    begin
      if (rst = '1') then
        present_state <= S_IDLE;
      elsif (clk'event and clk='1') then
        present_state <= next_state;
      else
        present_state <= present_state;
      end if;
  end process;


  -- Next state logic
  ---------------------
  process(present_state, start, dn_cc08, dn_cc12, dn_cc16)
    begin 
      case present_state is
       
        when S_IDLE =>
          if (start = '1') then
            next_state <= S_MBQM;
          else 
            next_state <= S_IDLE;
          end if;

        -- MultiplyByQuantizedMultiplier (MBQM)
        -- x * (1 << left_shift)
        when S_MBQM =>
          if (dn_cc08 = '1') then
            next_state <= S_SRDHM0;
          else
            next_state <= S_MBQM;
          end if;

        -- SaturatingRoundingDoublingHighMul (SRDHM)
        -- ab_64 = (int32_t)(a) * (int32_t)(b)
        when S_SRDHM0 =>
          if (dn_cc12 = '1') then
            next_state <= S_SRDHM1;
          else
            next_state <= S_SRDHM0;
          end if;

        -- SaturatingRoundingDoublingHighMul (SRDHM)
        -- (ab_64 + nudge)
        when S_SRDHM1 =>
          if (dn_cc08 = '1') then
            next_state <= S_SRDHM2;
          else
            next_state <= S_SRDHM1;
          end if;

        -- SaturatingRoundingDoublingHighMul (SRDHM)
        -- srdhm <= 2147483647 when overflow = '1' else sg_ab_x2_high32;
        when S_SRDHM2 =>
          if (dn_cc08 = '1') then
            next_state <= S_RDBP;
          else
            next_state <= S_SRDHM2;
          end if;
         
        -- RoundingDivideByPOT
        when S_RDBP =>
          next_state <= S_IDLE;

      end case;
  end process;


  -- Output logic
  ---------------------
  process(present_state, srdhm, shift)
    begin
      case present_state is

        when S_IDLE =>
          en_cc08 <= '0';
          en_cc12 <= '0';
          vr_ls   <= fun_left_shift(shift => shift);
          --mbqm    <= RoundingDivideByPOT( x => srdhm, exponent => fun_right_shift(shift => shift) );
          mbqm    <= srdhm;
          dn_mbqm <= '1';

        -- MultiplyByQuantizedMultiplier (MBQM)
        -- x * (1 << left_shift)
        when S_MBQM =>
          en_cc08 <= '1';
          en_cc12 <= '0';
          vr_ls   <= fun_left_shift(shift => shift);
          mbqm    <=   0;
          dn_mbqm <= '0';

        -- SaturatingRoundingDoublingHighMul (SRDHM)
        -- ab_64 = (int32_t)(a) * (int32_t)(b)
        when S_SRDHM0 =>
          en_cc08 <= '0';
          en_cc12 <= '1';
          vr_ls   <= fun_left_shift(shift => shift);
          mbqm    <=   0;
          dn_mbqm <= '0';

        -- SaturatingRoundingDoublingHighMul (SRDHM)
        -- (ab_64 + nudge)
        when S_SRDHM1 =>
          en_cc08 <= '1';
          en_cc12 <= '0';
          vr_ls   <= fun_left_shift(shift => shift);
          mbqm    <=   0;
          dn_mbqm <= '0';


        -- SaturatingRoundingDoublingHighMul (SRDHM)
        -- srdhm <= 2147483647 when overflow = '1' else sg_ab_x2_high32;
        when S_SRDHM2 =>
          en_cc08 <= '1';
          en_cc12 <= '0';
          vr_ls   <= fun_left_shift(shift => shift);
          mbqm    <=   0;
          dn_mbqm <= '0';

        -- RoundingDivideByPOT
        when S_RDBP =>
          en_cc08 <= '0';
          en_cc12 <= '0';
          vr_ls   <=  fun_left_shift(shift => shift);
          --mbqm    <=  RoundingDivideByPOT( x => srdhm, exponent => fun_right_shift(shift => shift) );
          mbqm    <=  srdhm;
          dn_mbqm <= '1';

      end case;
  end process;


  -- Compteur de 08 cycles (cc08)
  ---------------------

  -- Overflow pour 08 clock cycles
  mux_ovf08: 
    dd08 <= qq08 + 1 when (qq08<x"7") else (others=>'0');

  --  Registre du compteur 
  process (clk, rst)
    begin  
      if (rst = '1') then
        qq08 <= (others => '0');
      elsif (clk'event and clk = '1') then
        if (en_cc08 = '1') then
          qq08 <= dd08;
        else
          qq08 <= qq08;
        end if;
      end if;
  end process;

  -- Enable aprés 08 cycles
  dn_cc08 <= '0' when (qq08<x"7") else '1'; 


  -- Compteur de 12 cycles (cc12)
  ---------------------

  -- Overflow pour 12 clock cycles
  mux_ovf12: 
    dd12 <= qq12 + 1 when (qq12<x"C") else (others=>'0');

  --  Registre du compteur 
  process (clk, rst)
    begin  
      if (rst = '1') then
        qq12 <= (others => '0');
      elsif (clk'event and clk = '1') then
        if (en_cc12 = '1') then
          qq12 <= dd12;
        else
          qq12 <= qq12;
        end if;
      end if;
  end process;

  -- Enable aprés 12 cycles
  dn_cc12 <= '0' when (qq12<x"C") else '1'; 



  -- Compteur de 16 cycles (cc16)
  ---------------------

  -- Overflow pour 16 clock cycles
  mux_ovf16: 
    dd16 <= qq16 + 1 when (qq16<x"F") else (others=>'0');

  --  Registre du compteur 
  process (clk, rst)
    begin  
      if (rst = '1') then
        qq16 <= (others => '0');
      elsif (clk'event and clk = '1') then
        if (en_cc16 = '1') then
          qq16 <= dd16;
        else
          qq16 <= qq16;
        end if;
      end if;
  end process;

  -- Enable aprés 16 cycles
  dn_cc16 <= '0' when (qq16<x"F") else '1'; 
  
end Behavioral;

