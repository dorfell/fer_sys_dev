---------------------------------------------------------------------
----                                                             ----
---- file:    conv_core_top.vhd                                  ----
---- brief:   Noyau pour la convolution                          ----
---- details: Module pour implementer (x + offset_ent)*w         ----
----          Les entrées sont nombres entiers entre             ----
----          -128 et 127 (int8). Les résultats sont             ----
----          nombres entiers en int32.                          ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/07/29                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity conv_core_top is
  port( 
    clk :  in std_logic;

    -- Entrée 
    x01, x02, x03, x04, x05, x06, x07, x08, x09, x10, x11, x12, x13 :  in signed( 7 downto 0);
    x14, x15, x16, x17, x18, x19, x20, x21, x22, x23, x24, x25      :  in signed( 7 downto 0);

    -- Filtre, poids w
    w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, w11, w12, w13 :  in signed( 7 downto 0);
    w14, w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25      :  in signed( 7 downto 0);

    -- Offset entree 
    offset_ent :  in signed(15 downto 0);
    
    -- bias
    bias       :  in signed(31 downto 0);
    
    -- Sortie somme(xow1 + xow2 + ... + xow25) + bias
    xowb       : out signed(31 downto 0) );
end conv_core_top;


architecture Behavioral of conv_core_top is


  -- Component Declaration for the instances
  ------------------------------------------------------
  
  -- conv_core0: (x + offset_ent)
  component conv_core0 is
    port( 
      -- Entrée 
      x01, x02, x03, x04, x05, x06, x07, x08, x09, x10, x11, x12, x13 :  in signed( 7 downto 0);
      x14, x15, x16, x17, x18, x19, x20, x21, x22, x23, x24, x25      :  in signed( 7 downto 0);

      -- Offset entree 
      offset_ent : in signed(15 downto 0);

      -- Sortie (x + offset_ent)
      xo01, xo02, xo03, xo04, xo05, xo06, xo07, xo08, xo09, xo10, xo11, xo12, xo13 :  out signed(15 downto 0);
      xo14, xo15, xo16, xo17, xo18, xo19, xo20, xo21, xo22, xo23, xo24, xo25       :  out signed(15 downto 0) );
  end component conv_core0;


  -- conv_core1: (x + offset_ent)*w
  component conv_core1 is
    port( 
      clk        :  in std_logic;

      -- Entrée  (x + offset_ent)
      xo01, xo02, xo03, xo04, xo05, xo06, xo07, xo08, xo09, xo10, xo11, xo12, xo13 :  in signed(15 downto 0);
      xo14, xo15, xo16, xo17, xo18, xo19, xo20, xo21, xo22, xo23, xo24, xo25       :  in signed(15 downto 0);

      -- Filtre, poids w
      w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, w11, w12, w13 :  in signed( 7 downto 0);
      w14, w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25      :  in signed( 7 downto 0);

      -- Sortie (x + offset_ent)*w
      xow01, xow02, xow03, xow04, xow05, xow06, xow07, xow08, xow09, xow10, xow11, xow12, xow13 : out signed(31 downto 0);
      xow14, xow15, xow16, xow17, xow18, xow19, xow20, xow21, xow22, xow23, xow24, xow25        : out signed(31 downto 0) );
  end component conv_core1;


  -- conv_core2: somme(xow1 + xow2 + ... + xow25)
  component conv_core2 is
    port( 
      -- Entrée (x + offset_ent)*w
      xow01, xow02, xow03, xow04, xow05, xow06, xow07, xow08, xow09, xow10, xow11, xow12, xow13 :  in signed(31 downto 0);
      xow14, xow15, xow16, xow17, xow18, xow19, xow20, xow21, xow22, xow23, xow24, xow25        :  in signed(31 downto 0);

      -- Sortie somme(xow1 + xow2 + ... + xow25)
      xow : out signed(31 downto 0) ); 
  end component conv_core2;


  -- Computer (x + offset_ent)*w + bias
  component conv_core3 is
    port( 
      -- Entrée somme (xow1 + xow2 + ... + xow25)
      xow  :  in signed(31 downto 0);

      -- bias
      bias :  in signed(31 downto 0);

      -- (x + offset_ent)*w + bias$
      xowb : out signed(31 downto 0) );
  end component conv_core3;


  -- signal pour (x + offset_ent)
  signal sg_xo01, sg_xo02, sg_xo03, sg_xo04, sg_xo05 : signed(15 downto 0);
  signal sg_xo06, sg_xo07, sg_xo08, sg_xo09, sg_xo10 : signed(15 downto 0);
  signal sg_xo11, sg_xo12, sg_xo13, sg_xo14, sg_xo15 : signed(15 downto 0);
  signal sg_xo16, sg_xo17, sg_xo18, sg_xo19, sg_xo20 : signed(15 downto 0);
  signal sg_xo21, sg_xo22, sg_xo23, sg_xo24, sg_xo25 : signed(15 downto 0);

  -- signal pour (x + offset_ent)*w
  signal sg_xow01, sg_xow02, sg_xow03, sg_xow04, sg_xow05 : signed(31 downto 0);
  signal sg_xow06, sg_xow07, sg_xow08, sg_xow09, sg_xow10 : signed(31 downto 0);
  signal sg_xow11, sg_xow12, sg_xow13, sg_xow14, sg_xow15 : signed(31 downto 0);
  signal sg_xow16, sg_xow17, sg_xow18, sg_xow19, sg_xow20 : signed(31 downto 0);
  signal sg_xow21, sg_xow22, sg_xow23, sg_xow24, sg_xow25 : signed(31 downto 0);
  
  -- signal pour somme (x + offset_ent)*w
  signal sg_xow  : signed(31 downto 0);


begin


  --! Instantiate the submodules Convolution Core (CC)
  -----------------------------------------
  
  CC0: conv_core0
    port map(
      x01 => x01,
      x02 => x02,
      x03 => x03,
      x04 => x04,
      x05 => x05,
      x06 => x06,
      x07 => x07,
      x08 => x08,
      x09 => x09,
      x10 => x10,
      x11 => x11,
      x12 => x12,
      x13 => x13,
      x14 => x14,
      x15 => x15,
      x16 => x16,
      x17 => x17,
      x18 => x18,
      x19 => x19,
      x20 => x20,
      x21 => x21,
      x22 => x22,
      x23 => x23,
      x24 => x24,
      x25 => x25,
      
      offset_ent => offset_ent,
      
      xo01 => sg_xo01,
      xo02 => sg_xo02,
      xo03 => sg_xo03,
      xo04 => sg_xo04,
      xo05 => sg_xo05,
      xo06 => sg_xo06,
      xo07 => sg_xo07,
      xo08 => sg_xo08,
      xo09 => sg_xo09,
      xo10 => sg_xo10,
      xo11 => sg_xo11,
      xo12 => sg_xo12,
      xo13 => sg_xo13,
      xo14 => sg_xo14,
      xo15 => sg_xo15,
      xo16 => sg_xo16,
      xo17 => sg_xo17,
      xo18 => sg_xo18,
      xo19 => sg_xo19,
      xo20 => sg_xo20,
      xo21 => sg_xo21,
      xo22 => sg_xo22,
      xo23 => sg_xo23,
      xo24 => sg_xo24,
      xo25 => sg_xo25
    );


  CC1: conv_core1
    port map(
      clk => clk,

      xo01 => sg_xo01,
      xo02 => sg_xo02,
      xo03 => sg_xo03,
      xo04 => sg_xo04,
      xo05 => sg_xo05,
      xo06 => sg_xo06,
      xo07 => sg_xo07,
      xo08 => sg_xo08,
      xo09 => sg_xo09,
      xo10 => sg_xo10,
      xo11 => sg_xo11,
      xo12 => sg_xo12,
      xo13 => sg_xo13,
      xo14 => sg_xo14,
      xo15 => sg_xo15,
      xo16 => sg_xo16,
      xo17 => sg_xo17,
      xo18 => sg_xo18,
      xo19 => sg_xo19,
      xo20 => sg_xo20,
      xo21 => sg_xo21,
      xo22 => sg_xo22,
      xo23 => sg_xo23,
      xo24 => sg_xo24,
      xo25 => sg_xo25,
      
      w01 => w01,
      w02 => w02,
      w03 => w03,
      w04 => w04,
      w05 => w05,
      w06 => w06,
      w07 => w07,
      w08 => w08,
      w09 => w09,
      w10 => w10,
      w11 => w11,
      w12 => w12,
      w13 => w13,
      w14 => w14,
      w15 => w15,
      w16 => w16,
      w17 => w17,
      w18 => w18,
      w19 => w19,
      w20 => w20,
      w21 => w21,
      w22 => w22,
      w23 => w23,
      w24 => w24,
      w25 => w25,
       
      xow01 => sg_xow01,
      xow02 => sg_xow02,
      xow03 => sg_xow03,
      xow04 => sg_xow04,
      xow05 => sg_xow05,
      xow06 => sg_xow06,
      xow07 => sg_xow07,
      xow08 => sg_xow08,
      xow09 => sg_xow09,
      xow10 => sg_xow10,
      xow11 => sg_xow11,
      xow12 => sg_xow12,
      xow13 => sg_xow13,
      xow14 => sg_xow14,
      xow15 => sg_xow15,
      xow16 => sg_xow16,
      xow17 => sg_xow17,
      xow18 => sg_xow18,
      xow19 => sg_xow19,
      xow20 => sg_xow20,
      xow21 => sg_xow21,
      xow22 => sg_xow22,
      xow23 => sg_xow23,
      xow24 => sg_xow24,
      xow25 => sg_xow25
    );


  CC2: conv_core2
    port map(
      xow01 => sg_xow01,
      xow02 => sg_xow02,
      xow03 => sg_xow03,
      xow04 => sg_xow04,
      xow05 => sg_xow05,
      xow06 => sg_xow06,
      xow07 => sg_xow07,
      xow08 => sg_xow08,
      xow09 => sg_xow09,
      xow10 => sg_xow10,
      xow11 => sg_xow11,
      xow12 => sg_xow12,
      xow13 => sg_xow13,
      xow14 => sg_xow14,
      xow15 => sg_xow15,
      xow16 => sg_xow16,
      xow17 => sg_xow17,
      xow18 => sg_xow18,
      xow19 => sg_xow19,
      xow20 => sg_xow20,
      xow21 => sg_xow21,
      xow22 => sg_xow22,
      xow23 => sg_xow23,
      xow24 => sg_xow24,
      xow25 => sg_xow25,
     
      xow   => sg_xow
    );


  CC3: conv_core3
    port map(
      xow  => sg_xow,
      bias => bias,
      xowb => xowb 
    );


end Behavioral;
