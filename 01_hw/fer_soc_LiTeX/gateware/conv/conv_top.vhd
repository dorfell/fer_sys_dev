---------------------------------------------------------------------
----                                                             ----
---- file:    conv_top.vhd                                       ----
---- brief:   Module de Convolution TOP                          ----
---- details: Module top level top level pour le noyau de        ----
----          convolution à la facon de tflite.                  ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/06/18                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity conv_top is
  port( 
    clk, rst, start :  in std_logic;

    -- Entrée 
    x01, x02, x03, x04, x05, x06, x07, x08, x09, x10, x11, x12, x13 :  in signed( 7 downto 0);
    x14, x15, x16, x17, x18, x19, x20, x21, x22, x23, x24, x25      :  in signed( 7 downto 0);

    -- Filtre, poids w
    w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, w11, w12, w13 :  in signed( 7 downto 0);
    w14, w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25      :  in signed( 7 downto 0);

    -- Zero point
  --  zero_point :  in signed(31 downto 0);

    -- Offset entree et sortie
    offset_ent :  in signed(15 downto 0);
    --offset_sor :  in signed(15 downto 0);

    -- bias
    bias       :  in signed(31 downto 0);

    -- M0
    M0         :  in signed(31 downto 0);

    -- shift
    --shift      :  in signed( 7 downto 0);

    -- Sortie du noyau
    cv_out     : out signed(31 downto 0);
    
    -- conv_top done
    dn_cv      : out std_logic );
end conv_top;


architecture Behavioral of conv_top is


  -- Component Declaration for the instances
  ------------------------------------------------------

  -- conv_core: somme( (x + offset_ent)*w ) + bias
  component conv_core_top is
    port(
      clk :  in std_logic;

      -- Entrée 
      x01, x02, x03, x04, x05, x06, x07, x08, x09, x10, x11, x12, x13 :  in signed( 7 downto 0);
      x14, x15, x16, x17, x18, x19, x20, x21, x22, x23, x24, x25      :  in signed( 7 downto 0);

      -- Filtre, poids w
      w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, w11, w12, w13 :  in signed( 7 downto 0);
      w14, w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25      :  in signed( 7 downto 0);

      -- Offset entree 
      offset_ent :  in signed(15 downto 0);
    
      -- bias
      bias       :  in signed(31 downto 0);

      -- Sortie somme(xow1 + xow2 + ... + xow25) + bias
      xowb       : out signed(31 downto 0) );
  end component conv_core_top;


  -- conv_tflite
  component conv_tflite
    port(
      --clk   :  in std_logic;
      --clk, rst :  in std_logic;
--      start    :  in std_logic;
   
      -- somme(x+offset_ent)*w + bias
      xowb      :  in signed(31 downto 0);
    
      -- M0
      M0       :  in signed(31 downto 0);

      -- shift
      --shift    :  in signed( 7 downto 0);

      -- left_shift
      --vr_ls    : out integer range -128 to 127;

      -- sortie du noyau (MultiplyByQuantizedMultiplier)
      mbqm     : out signed(31 downto 0) ); 
    
      -- Done de noyau MultiplyByQuantizedMultiplier
 --     dn_mbqm  : out std_logic );
  end component;


  -- signal pour (x + offset_ent)*w
  --signal sg_xow     :  integer range -2147483648 to 2147483647;
--  signal sg_xow     :  signed(31 downto 0);

  -- signal pour somme(x + offset_ent)*w + bias
  signal sg_xowb    :  signed(31 downto 0);

  -- signal start du conv_tflite
--  signal sg_start_mbqm : std_logic;
  
  -- signaux avec la sortie et done de conv_tflite    
 -- signal sg_mbqm    : integer range -2147483648 to 2147483647;
 -- signal sg_dn_mbqm : std_logic;

  -- Compteur de cycles: registre, enable,done. 
  signal dd, qq       : std_logic_vector(5 downto 0) := (others=>'0');
  --signal en_cc12, dn_cc12 : std_logic; 
  --signal dn_cc12 : std_logic;

  
begin


  ---------------------
  -- Compteur de cycles (cc)
  ---------------------
  -- Overflow pour clock cycles
  mux_ovf: 
    dd <= qq when (qq = x"30") else qq + 1;

  -- Registre du compteur
  process (clk, rst)
    begin  
      if (rst = '1') then
        qq <= (others => '0');
      elsif (clk'event and clk = '1') then
        if (start = '1') then
          qq <= dd;
        else
          qq <= qq;
        end if;
      end if;
  end process;

  -- Done aprés cycles
  dn_cv <= '1' when (qq = x"30") else '0'; 




  --! Instantiate the submodules (SM)
  -----------------------------------------
  SM0: conv_core_top
    port map(
      clk => clk,
      
      x01 => x01,
      x02 => x02,
      x03 => x03,
      x04 => x04,
      x05 => x05,
      x06 => x06,
      x07 => x07,
      x08 => x08,
      x09 => x09,
      x10 => x10,
      x11 => x11,
      x12 => x12,
      x13 => x13,
      x14 => x14,
      x15 => x15,
      x16 => x16,
      x17 => x17,
      x18 => x18,
      x19 => x19,
      x20 => x20,
      x21 => x21,
      x22 => x22,
      x23 => x23,
      x24 => x24,
      x25 => x25,
      
      w01 => w01,
      w02 => w02,
      w03 => w03,
      w04 => w04,
      w05 => w05,
      w06 => w06,
      w07 => w07,
      w08 => w08,
      w09 => w09,
      w10 => w10,
      w11 => w11,
      w12 => w12,
      w13 => w13,
      w14 => w14,
      w15 => w15,
      w16 => w16,
      w17 => w17,
      w18 => w18,
      w19 => w19,
      w20 => w20,
      w21 => w21,
      w22 => w22,
      w23 => w23,
      w24 => w24,
      w25 => w25,
      
      offset_ent => offset_ent,
      bias       => bias,
      
      --xowb => cv_out );
      xowb => sg_xowb );
   

  SM1: conv_tflite
    port map(
      --clk     => clk,
---      rst     => rst,
--      start   => sg_start_mbqm,
      
      xowb    => sg_xowb,
      M0      => M0,
      --shift   => shift,
      
      mbqm    => cv_out); 
--      dn_mbqm => sg_dn_mbqm );


end Behavioral;
