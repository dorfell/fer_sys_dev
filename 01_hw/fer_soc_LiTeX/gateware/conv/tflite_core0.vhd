---------------------------------------------------------------------
----                                                             ----
---- file:    tflite_core0.vhd                                   ----
---- brief:   Module noyau 0 de tflite                           ----
---- details: Module pour implementer les operations             ----
----          arithmetiques qui ont besoin de pipeline.          ----
----          MultiplyByQuantizedMultiplier                      ----
----          SaturatingRoundingDoublingHighMul                  ---- 
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/07/04                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity tflite_core0 is
  port( 
    --clk      :  in std_logic;

    -- somme(x+offset_ent)*w + bias$
    xowb     :  in signed(31 downto 0);
        
    -- quantized_multiplier (M0)
    quantized_multiplier :  in signed(31 downto 0);

    -- shift
    -- shift    :  in signed( 7 downto 0);

    -- MultiplyByQuantizedMultiplier (MBQM) sortie
    -- x * (1 << left_shift)
    overflow : out std_logic;
    xls      : out signed(31 downto 0) );
end tflite_core0;


architecture Behavioral of tflite_core0 is


  -- fun_left_shift 
  function fun_left_shift(
    shift               : signed( 7 downto 0) ) return signed is
    
    variable left_shift : signed( 7 downto 0);
    variable vr_ls      : signed(15 downto 0);
    
    begin
     
      -- left_shift$
      if (shift > 0 ) then 
        left_shift  := shift;
      else
        left_shift  :=  (others => '0');
      end if;

      -- (1 << left_shift)
      vr_ls := shift_left(to_signed(1, 16), to_integer(left_shift));
      return vr_ls;
  end function fun_left_shift;


  -- SRDHM overflow
  function SRDHM_overflow(
    a     : signed(31 downto 0);
    b     : signed(31 downto 0) ) return std_logic is
    
    variable overflow     : std_logic;
    
    begin
      -- overflow   
      if ((a = b) and (a = -2147483648)) then
        overflow := '1';
      else
        overflow := '0';
      end if;

      return overflow;
  end function SRDHM_overflow;


  -- xls: signaux pour les registres de pipeline. 3 registers de pipeline p0, p1, p2.
  --signal sg_xowb_p0,  sg_xowb_p1,  sg_xowb_p2  : signed(31 downto 0);
  --signal sg_vr_ls_p0, sg_vr_ls_p1, sg_vr_ls_p2 : signed(15 downto 0);
  --signal sg_xls_p0,   sg_xls_p1,   sg_xls_p2   : signed(31 downto 0);

  --attribute USE_DSP : string;
  --attribute USE_DSP of sg_xls_p0 : signal is "YES";

  
begin


  -- Depuis MultiplyByQuantizedMultiplier
  -- Note: Comment tous les valeurs du shift pour le Modèle FER M6 sont
  --       negatives le résultat du fun_left_shift será toujours 1.
  --       Puis, tout multiplication pour 1 nous donnerá le même chiffre
  --       donc je vais commenter la logique necessarire pour multiplier.
  --       Il est possible de vérifier les paramètres (e.g. shift) en:
  --       https://gitlab.com/dorfell/fer_sys_dev/-/blob/master/00_sw/02_M6_jaffe/01_M6_jaffe_tflite/M6_tflite_param.log
  
  --process(clk)
  --  begin
  --    if (clk'event and clk = '1') then

        -- x * (1 << left_shift)
  --      sg_xowb_p0 <= xowb;
  --     sg_xowb_p1 <= sg_xowb_p0;
  --      sg_xowb_p2 <= sg_xowb_p1;
         
  --      sg_vr_ls_p0 <= fun_left_shift(shift => shift);
  --      sg_vr_ls_p1 <= sg_vr_ls_p0;
  --      sg_vr_ls_p2 <= sg_vr_ls_p1;
       
  --      sg_xls_p0 <= resize( (sg_xowb_p2 * sg_vr_ls_p2), 32);
  --      sg_xls_p1 <= sg_xls_p0;
  --      sg_xls_p2 <= sg_xls_p1;

  --    end if;
  --end process;




  -- SRDHM overflow
  --overflow <= SRDHM_overflow( a => sg_xls_p2,
  overflow <= SRDHM_overflow( a => xowb,
                              b => quantized_multiplier);

  -- sortie
  --xls      <= sg_xls_p2;
  xls      <= xowb;
  
end Behavioral;
