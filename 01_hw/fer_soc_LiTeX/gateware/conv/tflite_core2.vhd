---------------------------------------------------------------------
----                                                             ----
---- file:    tflite_core2.vhd                                   ----
---- brief:   Module noyau 2 de tflite                           ----
---- details: Module pour implementer les operations             ----
----          arithmetiques qui ont besoint de pipeline.         ----
----          SaturatingRoundingDoublingHighMul                  ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/07/04                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity tflite_core2 is
  port( 
    clk      :  in std_logic;

    -- ab_64 = (int32_t)(a) * (int32_t)(b)
    ab_64    :  in signed(63 downto 0);
    nudge    :  in signed(31 downto 0);
    
    -- SaturatingRoundingDoublingHighMul (SRDHM)
    -- (ab_64 + nudge)
    ab_nudge : out signed(47 downto 0) );
end tflite_core2;


architecture Behavioral of tflite_core2 is


  -- ab_nudge_low: signaux pour les registres de pipeline. registers de pipeline p0, p1, p2 ...
  signal sg_ab_48_p0,    sg_ab_48_p1,    sg_ab_48_p2    : signed(47 downto 0);
  signal sg_nudge_p0,    sg_nudge_p1,    sg_nudge_p2    : signed(31 downto 0);
  signal sg_ab_nudge_p0, sg_ab_nudge_p1, sg_ab_nudge_p2 : signed(47 downto 0);
  
  
  attribute USE_DSP : string;
  attribute USE_DSP of sg_ab_nudge_p0 : signal is "YES";

  
begin


  -- Depuis SaturatingRoundingDoublingHighMul
  -- Depuis ab_x2_high32 = static_cast<std::int32_t>((ab_64 + nudge) / (1ll << 31));
  process(clk)
    begin
      if (clk'event and clk = '1') then

        -- Seulement: static_cast<std::int32_t>((ab_64 + nudge)
        sg_ab_48_p0 <= ab_64(47 downto 0);
        sg_ab_48_p1 <= sg_ab_48_p0;
        sg_ab_48_p2 <= sg_ab_48_p1;

        sg_nudge_p0 <= nudge(31 downto 0);
        sg_nudge_p1 <= sg_nudge_p0;
        sg_nudge_p2 <= sg_nudge_p1;
                
        sg_ab_nudge_p0 <= (sg_ab_48_p2 + sg_nudge_p2);
        sg_ab_nudge_p1 <= sg_ab_nudge_p0;
        sg_ab_nudge_p2 <= sg_ab_nudge_p1;
        
      end if;
  end process;

  -- sortie
  ab_nudge <= sg_ab_nudge_p2;
  
end Behavioral;
