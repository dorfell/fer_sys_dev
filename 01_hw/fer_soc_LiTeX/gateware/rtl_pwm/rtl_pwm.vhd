---------------------------------------------------------------------
----                                                             ----
---- file:    pwm.vhd                                            ----
---- brief:   Module de PWM                                      ----
---- details: Module pour implementer un PWM                     ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/05/20                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity pwm is
  port( 
         clk, rstn :  in std_logic;
         --S_AXI_ARESETN :  in std_logic;
        --prd, wdt :  in std_logic_vector(31 downto 0);
        --cnt      : out std_logic_vector( 7 downto 0);
 
        pls      : out std_logic );
end pwm;


architecture Behavioral of pwm is

  signal dd, qq : std_logic_vector(31 downto 0):=(others=>'0');

begin

  -- Overflow de registre. Exemple: pour 1 s le periode (prd) es 00x07735940 = 125MHz
  mux_ovf: 
    --dd <= qq + 1 when (qq<prd) else (others=>'0');
    dd <= qq + 1 when (qq<x"02FAF080") else (others=>'0');


  -- 32-bits register 
  process (clk, rstn)
    begin  
      if (rstn = '0') then
        qq <= (others => '0');
      elsif (clk'event and clk = '1') then
        qq <= dd;
      end if;
  end process;

  -- pouls de sortie avec largeur (width) variable
  --pls <= '1' when (qq<wdt) else '0'; 
  pls <= '1' when (qq<x"017D7840") else '0';

  -- sortir les 8-bits LSB du registre
  --cnt <= qq( 7 downto 0);

end Behavioral;