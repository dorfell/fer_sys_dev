#
# This file is not part of LiteX (8-P.
#
# Copyright (c) 2021 Dorfell Parra <dlparrap@unal.edu.cok>
# SPDX-License-Identifier: BSD-2-Clause
# References:
# https://github.com/betrusted-io/gateware/blob/main/gateware/i2c/core.py
# https://github.com/enjoy-digital/litex/blob/master/litex/soc/cores/gpio.py


import os

from migen import *

from litex.soc.interconnect.csr import *
from litex.soc.integration.doc  import AutoDoc, ModuleDoc


# Simple RTL PWM ------------------------------------------------------------------------------
class RTLPWM(Module, AutoCSR, AutoDoc):
  """Simple Pulse Width Modulation
     Simple VHDL core to demonstrate how to connect an Core to the SoC by means of the 
     CSR interface.
     
     Coeur simple écrit en VHDL pour démontrer comment on peut faire la connexion d'un 
     coeur à un SoC en utilisant l'interface CSR. 
  """


  def __init__(self, platform, pads):
    self.intro = ModuleDoc("VHDL_PWM: Simple VHDL core to demonstrate how to connect an Core to the SoC by means of the CSR interface.");
    self.prd   = CSRStorage(name="prd", size=32, reset=0x00000000, description="""prd: periode de pouls.""");
    self.wdt   = CSRStorage(name="wdt", size=32, reset=0x00000000, description="""wdt: largeur de pouls.""");

    # Paramètres
    self.specials += Instance("pwm",

      # Clk / Rst.
      i_clk = ClockSignal(),
      i_rst = ResetSignal(),

      # Periode et largeur du pouls
      i_prd = self.prd.storage,
      i_wdt = self.wdt.storage,

      # pulse de sortie
      o_pls = pads
    );

    # Direct use of VHDL sources
    platform.add_source("gateware/rtl_pwm/rtl_pwm.vhd");
