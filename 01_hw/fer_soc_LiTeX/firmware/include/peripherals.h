#ifndef __PERIPHERALS_H
#define __PERIPHERALS_H


/*** bt_test              ***/
void bt_test(void);


/*** led_test             ***/
void led_test(void);


/*** rgb_test             ***/
void rgb_test(void);


/*** rtl_pwm_test        ***/
void rtl_pwm_test(void);


/*** sw_test              ***/
void sw_test(void);


/*** tm_test              ***/
void tm_test(void);
#endif
