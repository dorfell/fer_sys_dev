#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <irq.h>
#include <uart.h>
#include <console.h>
#include <generated/csr.h>
#include <hw/common.h> 

#include "../include/accel.h"

#include <stdbool.h>         // bool type
#include <assert.h>          // bool type

#define max(a,b)({         \
  __typeof__ (a) _a = (a); \
  __typeof__ (b) _b = (b); \
  _a > _b ? _a : _b;       })

#define min(a,b)({           \
  __typeof__ (a) _a = (a); \
  __typeof__ (b) _b = (b); \
  _a < _b ? _a : _b;       })


/*
#define max(a,b)             \
({                           \
    __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b;       \
})

#define min(a,b)             \
({                           \
    __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a < _b ? _a : _b;       \
})
*/


/***  busy_wait_ns        ***/
static inline void busy_wait_ns(unsigned int ns){
  timer0_en_write(0);
  timer0_reload_write(0);
  timer0_load_write(CONFIG_CLOCK_FREQUENCY/1000000000*ns);
  timer0_en_write(1);
  timer0_update_value_write(1);
  while(timer0_value_read()) timer0_update_value_write(1); 
};


/*** TFLite fonctions     ***/
static inline int32_t Dup(int32_t x){
  return x; };

static inline int32_t BitAnd(int32_t a, int32_t b){
  return a & b; };

static inline int32_t BitNot(int32_t a){
  //std::cout << " a = " <<  a << std::endl;
  //std::cout << "~a = " << ~a << std::endl;
  return ~a; };

static inline int32_t Add(int32_t a, int32_t b){
  return a + b; };

static inline int32_t ShiftRight(int32_t a, int8_t offset){
  return a >> offset; };

static inline int32_t MaskIfNonZero(int32_t a){
  int32_t zero = 0;
  return a ? BitNot(zero) : zero;  };

static inline int32_t MaskIfGreaterThan(int32_t a, int32_t b){
  return MaskIfNonZero(a > b);  };

static inline int32_t MaskIfLessThan(int32_t a, int32_t b){
  return MaskIfNonZero(a < b); };

static inline int32_t SaturatingRoundingDoublingHighMul(int32_t a, int32_t b){
  //bool overflow = a == b && a == std::numeric_limits<std::int32_t>::min();
  bool overflow = ((a == b) && (a == -2147483648));
/*  int64_t a_64(a); int64_t b_64(b);  
  int64_t ab_64 = a_64 * b_64;  */

  int64_t a_64 = (int64_t)(a); int64_t b_64 = (int64_t)(b);
  int64_t ab_64 = a_64 * b_64;
  int32_t nudge = ab_64 >= 0 ? (1 << 30) : (1 - (1 << 30)); 
//  int32_t ab_x2_high32 =  static_cast<std::int32_t>((ab_64 + nudge) / (1ll << 31)); 
  int32_t ab_x2_high32 =  (int32_t)((ab_64 + nudge) / (1ll << 31)); 
  printf("ab_x2_high32 = %x \n", ab_x2_high32);
/*std::cout << "overflow = " << overflow  << std::endl;
  std::cout << "a_64(a)= "   << a_64  << std::endl;
  std::cout << "b_64(b)= "   << b_64  << std::endl;
  std::cout << "ab_64 = "    << ab_64 << std::endl;
  std::cout << "nudge = "    << nudge << std::endl;
  std::cout << "ab_x2_high32 = " << ab_x2_high32 << std::endl; */ 
  //return overflow ? std::numeric_limits<std::int32_t>::max() : ab_x2_high32;
  return overflow ? 2147483647 : ab_x2_high32;
};


static inline int32_t RoundingDivideByPOT(int32_t x, int8_t exponent) {
  assert(exponent >= 0);
  assert(exponent <= 31);
  int32_t mask = Dup((1ll << exponent) - 1);
  int32_t zero = Dup(0);
  int32_t one  = Dup(1);
  int32_t remainder = BitAnd(x, mask);
  int32_t threshold = Add(ShiftRight(mask, 1), BitAnd(MaskIfLessThan(x, zero), one)); 
/*std::cout << "Mask = "      << mask << std::endl;
  std::cout << "zero = "      << zero << std::endl;
  std::cout << "one  = "      << one  << std::endl;
  std::cout << "Remainder = " << remainder << std::endl;
  std::cout << "Threshold = " << threshold << std::endl;
  std::cout << "ShiftRigh = " << ShiftRight(x, exponent) << std::endl;
  std::cout << "MaskIfGre = " << MaskIfGreaterThan(remainder, threshold) << std::endl;
  std::cout << "Add int32_t= " <<  Add(ShiftRight(x, exponent),  BitAnd( MaskIfGreaterThan(remainder, threshold), one ) ) << std::endl; */
  return Add( ShiftRight(x, exponent), BitAnd( MaskIfGreaterThan(remainder, threshold), one ) ); };


static inline int32_t MultiplyByQuantizedMultiplier(int32_t x, int32_t quantized_multiplier, int shift){
  int8_t left_shift  = shift > 0 ? shift :      0;
  int8_t right_shift = shift > 0 ? 0     : -shift;
  /*std::cout << "x = " << x << std::endl;
  std::cout << "quantized_multiplier = " << quantized_multiplier  << std::endl;
  std::cout << "shift = "                << shift                 << std::endl;
  std::cout << "x * (1 << left_shift)= " << x * (1 << left_shift) << std::endl;  */
  return RoundingDivideByPOT(SaturatingRoundingDoublingHighMul(x * (1 << left_shift), quantized_multiplier), right_shift); };



/***  fonction conv     ***/
static inline int32_t fun_conv( 
  int8_t x01, int8_t x02, int8_t x03, int8_t x04, int8_t x05,
  int8_t x06, int8_t x07, int8_t x08, int8_t x09, int8_t x10,
  int8_t x11, int8_t x12, int8_t x13, int8_t x14, int8_t x15,
  int8_t x16, int8_t x17, int8_t x18, int8_t x19, int8_t x20,
  int8_t x21, int8_t x22, int8_t x23, int8_t x24, int8_t x25,

  int8_t f01, int8_t f02, int8_t f03, int8_t f04, int8_t f05,
  int8_t f06, int8_t f07, int8_t f08, int8_t f09, int8_t f10,
  int8_t f11, int8_t f12, int8_t f13, int8_t f14, int8_t f15,
  int8_t f16, int8_t f17, int8_t f18, int8_t f19, int8_t f20,
  int8_t f21, int8_t f22, int8_t f23, int8_t f24, int8_t f25,

  int16_t zero_point, int16_t offset_ent, int16_t offset_sor,
  int32_t bias, int32_t M0, int8_t shift ){

  int32_t res = 0;

  conv_x01_write(x01); conv_x02_write(x02); conv_x03_write(x03);  conv_x04_write(x04); conv_x05_write(x05);
  conv_x06_write(x06); conv_x07_write(x07); conv_x08_write(x08);  conv_x09_write(x09); conv_x10_write(x10);
  conv_x11_write(x11); conv_x12_write(x12); conv_x13_write(x13);  conv_x14_write(x14); conv_x15_write(x15);
  conv_x16_write(x16); conv_x17_write(x17); conv_x18_write(x18);  conv_x19_write(x19); conv_x20_write(x20);
  conv_x21_write(x21); conv_x22_write(x22); conv_x23_write(x23);  conv_x24_write(x24); conv_x25_write(x25);

  conv_w01_write(f01); conv_w02_write(f02); conv_w03_write(f03);  conv_w04_write(f04); conv_w05_write(f05);
  conv_w06_write(f06); conv_w07_write(f07); conv_w08_write(f08);  conv_w09_write(f09); conv_w10_write(f10);
  conv_w11_write(f11); conv_w12_write(f12); conv_w13_write(f13);  conv_w14_write(f14); conv_w15_write(f15);
  conv_w16_write(f16); conv_w17_write(f17); conv_w18_write(f18);  conv_w19_write(f19); conv_w20_write(f20);
  conv_w21_write(f21); conv_w22_write(f22); conv_w23_write(f23);  conv_w24_write(f24); conv_w25_write(f25);

//  conv_zero_point_write(zero_point);
  conv_offset_ent_write(offset_ent);
//  conv_offset_sor_write(offset_sor);
  conv_bias_write(bias);
  conv_M0_write(M0);
  //conv_shift_write(shift);

  //busy_wait_ns(16);                                        // clk = 125 MHz => 16 ns = 2 cycles
  conv_start_write(1);                                     // Commencer la convolution.
  printf("conv_dn_cv_read() = %d \n", conv_dn_cv_read());
  while(conv_dn_cv_read()==0){};                           // Attendre que l'operation se termine
  res = (int32_t) conv_cv_out_read();
  conv_start_write(0);                                     // Terminer la convolution.
  printf("conv_dn_cv_read() = %d \n", conv_dn_cv_read());


  printf("conv_cv_out_read() DEC = %d \n", res);
  printf("conv_cv_out_read() HEX = %x \n", res);
  return res;
};


/*** conv_k5_test_FPGA    ***/
void conv_k5_test_FPGA(void){

  // Entrée avec padding.
  int8_t ent[1][5][5][1] = 
    {{{{-128}, {-128}, {-128}, {-128}, {-128}}, 
      {{-128}, {-128}, {-128}, {-128}, {-128}}, 
      {{-128}, {-128}, {-123}, {-123}, {-122}}, 
      {{-128}, {-128}, {-121}, {-123}, {-124}}, 
      {{-128}, {-128}, {-117}, {-123}, {-125}}}};

  int16_t zero_point = -128;
  int16_t offset_ent = -zero_point;
  int16_t offset_sor = -18;
  int32_t biases     = -17;
  int32_t M0         = 1218999674;
  int8_t  shift      = -5;

  int8_t fil[1][5][5][1] = 
    {{{{ -23}, { -60}, {  84}, {  68}, {  48}}, 
      {{  73}, {-106}, { -49}, { -59}, {  -8}}, 
      {{  22}, {-107}, {  45}, { -28}, {-127}}, 
      {{ -70}, {  40}, {  39}, {  44}, { -50}}, 
      {{ -61}, { -61}, { -25}, {  -8}, {-109}}}};

  int32_t res = 0;  // Résultat


  printf("conv_k5_test_FPGA...\n");

/*  for(int i=0; i<0x01; i++){
    for(int j=0; j<0x05; j++){
      for(int k=0; k<0x05; k++){
        for(int l=0; l<0x01; l++){
          sum += (ent[i][j][k][l]+offset_ent) * filtres[i][j][k][l];
        };
      };
    };
  };
*/

  res = fun_conv(
    ent[0][0][0][0], ent[0][0][1][0], ent[0][0][2][0], ent[0][0][3][0], ent[0][0][4][0],
    ent[0][1][0][0], ent[0][1][1][0], ent[0][1][2][0], ent[0][1][3][0], ent[0][1][4][0],
    ent[0][2][0][0], ent[0][2][1][0], ent[0][2][2][0], ent[0][2][3][0], ent[0][2][4][0],
    ent[0][3][0][0], ent[0][3][1][0], ent[0][3][2][0], ent[0][3][3][0], ent[0][3][4][0],
    ent[0][4][0][0], ent[0][4][1][0], ent[0][4][2][0], ent[0][4][3][0], ent[0][4][4][0],

    fil[0][0][0][0], fil[0][0][1][0], fil[0][0][2][0], fil[0][0][3][0], fil[0][0][4][0],
    fil[0][1][0][0], fil[0][1][1][0], fil[0][1][2][0], fil[0][1][3][0], fil[0][1][4][0],
    fil[0][2][0][0], fil[0][2][1][0], fil[0][2][2][0], fil[0][2][3][0], fil[0][2][4][0],
    fil[0][3][0][0], fil[0][3][1][0], fil[0][3][2][0], fil[0][3][3][0], fil[0][3][4][0],
    fil[0][4][0][0], fil[0][4][1][0], fil[0][4][2][0], fil[0][4][3][0], fil[0][4][4][0],

    zero_point, offset_ent, offset_sor, biases, M0, shift);

  printf("W*(x+offset_ent) DEC = %d \n", res);
  printf("W*(x+offset_ent) HEX = %x \n", res);


  /*sum += biases;
  printf("W*(x+offset_ent)+bias = %d \n", sum);

  sum = MultiplyByQuantizedMultiplier(sum, M0, shift);
  printf("MultByQuanMul = %d \n", sum);

  sum = min( max(sum, 0), 255);
  printf("sum = min( max(sum, 0), 255) = %d \n", sum); */
};




/*** conv_k5_test_CPU     ***/
void conv_k5_test_CPU(void){

  // Entrée avec padding.
  int ent[1][5][5][1] = 
    {{{{-128}, {-128}, {-128}, {-128}, {-128}}, 
      {{-128}, {-128}, {-128}, {-128}, {-128}}, 
      {{-128}, {-128}, {-123}, {-123}, {-122}}, 
      {{-128}, {-128}, {-121}, {-123}, {-124}}, 
      {{-128}, {-128}, {-117}, {-123}, {-125}} }};

  int zero_point = -128;
  int offset_ent = -zero_point;
  int offset_sor = -18;
  int biases     = -17;
  int M0 = 1218999674;
  int shift = -5;

  int filtres[1][5][5][1] = 
    {{{{ -23}, { -60}, {  84}, {  68}, {  48}}, 
      {{  73}, {-106}, { -49}, { -59}, {  -8}}, 
      {{  22}, {-107}, {  45}, { -28}, {-127}}, 
      {{ -70}, {  40}, {  39}, {  44}, { -50}}, 
      {{ -61}, { -61}, { -25}, {  -8}, {-109}} }};

  //int c[1][5][5][1];
  int sum = 0;

  printf("conv_k5_test_CPU...\n");

  for(int i=0; i<0x01; i++){
    for(int j=0; j<0x05; j++){
      for(int k=0; k<0x05; k++){
        for(int l=0; l<0x01; l++){
          sum += (ent[i][j][k][l]+offset_ent) * filtres[i][j][k][l];
        };
      };
    };
  };
  printf("W*(x+offset_ent) = %d \n", sum);

  sum += biases;
  printf("W*(x+offset_ent)+bias = %d \n", sum);

  sum = MultiplyByQuantizedMultiplier(sum, M0, shift);
  printf("MultByQuanMul = %d \n", sum);

  sum = min( max(sum, 0), 255);
  printf("sum = min( max(sum, 0), 255) = %d \n", sum);

  sum = sum + offset_sor;
  printf("sum + offset_sor = %d \n", sum);

  //sum = max(sum, -128);
  //sum = min(sum,  127);
  sum = min( max(sum, -128), 127);
  printf("sum = %d \n", sum);




  printf("****************** \n");
  printf("Dup= %d \n", Dup(5));
  printf("BitAnd= %d \n", BitAnd(0x5, 0xF));
  printf("Add= %d \n", Add(2,3));
  printf("ShiftRight= %x \n", ShiftRight(0x5, 1) );
  printf("MaskIfNonZero= %x \n", MaskIfNonZero(0x0));
  printf("MaskIfNonZero= %x \n", MaskIfNonZero(0x1));
  printf("MaskIfGreaterThan= %x \n", MaskIfGreaterThan(2,3));
  printf("MaskIfGreaterThan= %x \n", MaskIfGreaterThan(3,2));
  printf("MaskIfLessThan= %x \n", MaskIfLessThan(2,3));
  printf("MaskIfLessThan= %x \n", MaskIfLessThan(3,2));
  printf("****************** \n");
  printf(" entier  5 = %x \n",  5);
  printf(" entier -5 = %x \n", -5);
  printf("****************** \n");
};

