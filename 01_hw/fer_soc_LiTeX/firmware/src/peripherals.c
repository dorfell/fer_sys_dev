#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <irq.h>
#include <uart.h>
#include <console.h>
#include <generated/csr.h>
#include <hw/common.h> 

#include "../include/peripherals.h"


/*** bt_test              ***/
void bt_test(void){
  int val;
  printf("bt_test...\n");
  val = buttons_in_read();
  leds_out_write(val);
  printf("le valeur de bt  est: %x\n", val);
};


/*** led_test             ***/
void led_test(void){
  int i;
  printf("led_test...\n");
  for(i=0; i<0x10; i++){
    leds_out_write(i);
    busy_wait(500); };                                     // 500 ms
  leds_out_write(0x0);
};


/*** rgb_test             ***/
void rgb_test(void){
  int i, j;
  printf("rbg_test...\n");

  rgbled_r_enable_write(0x1);
  rgbled_g_enable_write(0x1);
  rgbled_b_enable_write(0x1);

  rgbled_r_period_write(1024*256);
  rgbled_g_period_write(1024*512);
  rgbled_b_period_write(1024*1024);

  for(i=0; i<0x04; i++){

    for(j=0; j<0x40; j++){
      rgbled_r_width_write(j*0x400);                       // brightness
      rgbled_g_width_write(j*0x400);                       // brightness
      rgbled_b_width_write(j*0x400);                       // brightness
      busy_wait(20); };                                    // 20 ms

    for(j=0; j<0x40; j++){
      rgbled_r_width_write((64-j)*0x400);                  // brightness
      rgbled_g_width_write((64-j)*0x400);                  // brightness
      rgbled_b_width_write((64-j)*0x400);                  // brightness
      busy_wait(20); };                                    // 20 ms
  };


  busy_wait(1000);                                         // 1000 ms
  rgbled_r_enable_write(0x1); rgbled_g_enable_write(0x0); rgbled_b_enable_write(0x0);

  busy_wait(1000);                                          // 1000 ms
  rgbled_r_enable_write(0x0); rgbled_g_enable_write(0x1); rgbled_b_enable_write(0x0);

  busy_wait(1000);                                          // 1000 ms
  rgbled_r_enable_write(0x0); rgbled_g_enable_write(0x0); rgbled_b_enable_write(0x1);

  busy_wait(1000);                                          // 1000 ms
  rgbled_r_enable_write(0x0); rgbled_g_enable_write(0x0); rgbled_b_enable_write(0x0);
};


/*** sw_test              ***/
void sw_test(void){
  int val;
  printf("sw_test...\n");
  val = switches_in_read();
  leds_out_write(val);
  printf("le valeur de sw  est: %x\n", val);
};


/*** tm_test              ***/
void tm_test(void){
  unsigned int ms=1000;
  printf("tm_test...\n");

  /* Init timer */
  timer0_en_write(0);
  timer0_reload_write(0);
  timer0_load_write(CONFIG_CLOCK_FREQUENCY/1000*ms);
  timer0_en_write(1);
  timer0_update_value_write(1);

  leds_out_write(0xF);
  while(timer0_value_read()) timer0_update_value_write(1);
  leds_out_write(0x0);
};


/*** rtl_pwm_test          ***/
void rtl_pwm_test(void){
  uint8_t  cnt;                                            // 8 MSB du registre PWM.
  uint32_t prd, val;                                       // période et largeur de pouls.
  printf("rtl_pwm_test...\n");

  prd = 0x07735940;                                        // Periode de pouls.
  rtlpwm_prd_write(prd);                                   // Écrit le periode.
  
  for(int i=0; i<0x05; i++){
    rtlpwm_wdt_write(0x00989680*(2*i+1));                  // Écrit le largeur.
    val = rtlpwm_wdt_read();                               // Lire le largeur.
    cnt = (uint8_t) rtlpwm_cnt_read();                     // Lire les 8 MSB de compte.
    printf("Le largeur de PWM est: %x \n", val);
    printf("Les 8 MSB du compte:   %x \n", cnt);
    busy_wait(4000); };                                    // 4000 ms = 4 s
    
  rtlpwm_wdt_write(0x00000000);
};
