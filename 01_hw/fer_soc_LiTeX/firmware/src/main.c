#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <irq.h>
#include <uart.h>
#include <console.h>
#include <generated/csr.h>

#include "../include/accel.h"
#include "../include/peripherals.h"


/*** readstr              ***/
static inline char *readstr(void){
  char c[2];
  static char s[64];
  static int ptr = 0;

  if(readchar_nonblock()) {
    c[0] = readchar();
    c[1] = 0;
    switch(c[0]) {
      case 0x7f:
      case 0x08:
        if(ptr > 0) {
          ptr--;
          putsnonl("\x08 \x08"); }
        break;
      case 0x07:
        break;
      case '\r':
      case '\n':
        s[ptr] = 0x00;
        putsnonl("\n");
        ptr = 0;
        return s;
      default:
        if(ptr >= (sizeof(s) - 1))
          break;
        putsnonl(c);
        s[ptr] = c[0];
        ptr++;
        break;
    };
  };
  return NULL;
};


/*** get_token            ***/
static inline char *get_token(char **str){
  char *c, *d;

  c = (char *)strchr(*str, ' ');
  if(c == NULL) {
    d = *str;
    *str = *str+strlen(*str);
    return d; }
  *c = 0;
  d = *str;
  *str = c+1;
  return d;
};


/*** prompt               ***/
static inline void prompt(void){
  printf("RUNTIME>"); };


/*** help                 ***/
static inline void help(void){
  puts("Available commands:");
  puts("********************************************");
  puts("help                            - this command");
  puts("reboot                          - reboot CPU");
  puts("bt                              - buttons test");
  puts("led                             - led test");
  puts("rgb                             - rgb test");
  puts("sw                              - sw test");
  puts("tm                              - timer test");
  puts("********************************************");
  puts("cvc                             - conv_k5 test_CPU");
  puts("cvf                             - conv_k5 test_FPGA");
  puts("pwm                             - rtl_pwm test");
  puts("********************************************");
};


/*** reboot               ***/
static inline void reboot(void){
  ctrl_reset_write(1); };


/*** Console service      ***/
static inline void console_service(void){
  char *str;
  char *token;

  str = readstr();
  if(str == NULL) return;
  token = get_token(&str);
  if(strcmp(token, "help") == 0)
    help();
  else if(strcmp(token, "reboot") == 0)
    reboot();
  else if(strcmp(token, "bt") == 0)
    bt_test();
  else if(strcmp(token, "led") == 0)
    led_test();
  else if(strcmp(token, "rgb") == 0)
    rgb_test();
  else if(strcmp(token, "sw") == 0)
    sw_test();
  else if(strcmp(token, "tm") == 0)
    tm_test();
  // fonctions d'accelerateur
  else if(strcmp(token, "cvc") == 0)
    conv_k5_test_CPU();
  else if(strcmp(token, "cvf") == 0)
    conv_k5_test_FPGA();
  else if(strcmp(token, "pwm") == 0)
    rtl_pwm_test();
  prompt();
};


/****************************/
/*** Main                 ***/
/****************************/
int main(void){
#ifdef CONFIG_CPU_HAS_INTERRUPT
  irq_setmask(0);
  irq_setie(1);
#endif

  uart_init();

  puts("\n FER SoC - CPU testing software built "__DATE__" "__TIME__"\n");
  help();
  prompt();
  
  /* boucle principale */
  while(1){
    console_service(); };
  return 0;
};
