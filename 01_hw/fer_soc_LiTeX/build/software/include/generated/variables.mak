PACKAGES=libcompiler_rt libbase libfatfs liblitespi liblitedram libliteeth liblitesdcard liblitesata bios
PACKAGE_DIRS=/usr/local/LiTeX_2021.04/litex/litex/soc/software/libcompiler_rt /usr/local/LiTeX_2021.04/litex/litex/soc/software/libbase /usr/local/LiTeX_2021.04/litex/litex/soc/software/libfatfs /usr/local/LiTeX_2021.04/litex/litex/soc/software/liblitespi /usr/local/LiTeX_2021.04/litex/litex/soc/software/liblitedram /usr/local/LiTeX_2021.04/litex/litex/soc/software/libliteeth /usr/local/LiTeX_2021.04/litex/litex/soc/software/liblitesdcard /usr/local/LiTeX_2021.04/litex/litex/soc/software/liblitesata /usr/local/LiTeX_2021.04/litex/litex/soc/software/bios
LIBS=libcompiler_rt libbase-nofloat libfatfs liblitespi liblitedram libliteeth liblitesdcard liblitesata
TRIPLE=riscv64-unknown-elf
CPU=vexriscv
CPUFLAGS=-march=rv32im     -mabi=ilp32 -D__vexriscv__
CPUENDIANNESS=little
CLANG=0
CPU_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc/cores/cpu/vexriscv
SOC_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc
COMPILER_RT_DIRECTORY=/usr/local/LiTeX_2021.04/pythondata-software-compiler_rt/pythondata_software_compiler_rt/data
export BUILDINC_DIRECTORY
BUILDINC_DIRECTORY=/home/dorfell/Downloads/fer_sys_dev/01_hw/fer_soc/build/software/include
LIBCOMPILER_RT_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc/software/libcompiler_rt
LIBBASE_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc/software/libbase
LIBFATFS_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc/software/libfatfs
LIBLITESPI_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc/software/liblitespi
LIBLITEDRAM_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc/software/liblitedram
LIBLITEETH_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc/software/libliteeth
LIBLITESDCARD_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc/software/liblitesdcard
LIBLITESATA_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc/software/liblitesata
BIOS_DIRECTORY=/usr/local/LiTeX_2021.04/litex/litex/soc/software/bios