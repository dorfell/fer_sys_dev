################################################################################
# IO constraints
################################################################################
# serial:0.tx
set_property LOC V8 [get_ports {serial_tx}]
set_property IOSTANDARD LVCMOS33 [get_ports {serial_tx}]

# serial:0.rx
set_property LOC W8 [get_ports {serial_rx}]
set_property IOSTANDARD LVCMOS33 [get_ports {serial_rx}]

# clk125:0
set_property LOC K17 [get_ports {clk125}]
set_property IOSTANDARD LVCMOS33 [get_ports {clk125}]

# cpu_reset:0
set_property LOC K16 [get_ports {cpu_reset}]
set_property IOSTANDARD LVCMOS33 [get_ports {cpu_reset}]

# user_btn:0
set_property LOC K18 [get_ports {user_btn0}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_btn0}]

# user_btn:1
set_property LOC P16 [get_ports {user_btn1}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_btn1}]

# user_btn:2
set_property LOC K19 [get_ports {user_btn2}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_btn2}]

# user_btn:3
set_property LOC Y16 [get_ports {user_btn3}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_btn3}]

# user_led:0
set_property LOC M14 [get_ports {user_led0}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_led0}]

# user_led:1
set_property LOC M15 [get_ports {user_led1}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_led1}]

# user_led:2
set_property LOC G14 [get_ports {user_led2}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_led2}]

# user_led:3
set_property LOC D18 [get_ports {user_led3}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_led3}]

# user_rgb_led:0.r
set_property LOC Y11 [get_ports {user_rgb_led0_r}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_rgb_led0_r}]

# user_rgb_led:0.g
set_property LOC T5 [get_ports {user_rgb_led0_g}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_rgb_led0_g}]

# user_rgb_led:0.b
set_property LOC Y12 [get_ports {user_rgb_led0_b}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_rgb_led0_b}]

# user_sw:0
set_property LOC G15 [get_ports {user_sw0}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_sw0}]

# user_sw:1
set_property LOC P15 [get_ports {user_sw1}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_sw1}]

# user_sw:2
set_property LOC W13 [get_ports {user_sw2}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_sw2}]

# user_sw:3
set_property LOC T16 [get_ports {user_sw3}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_sw3}]

# user_pwm:0
set_property LOC N15 [get_ports {user_pwm0}]
set_property IOSTANDARD LVCMOS33 [get_ports {user_pwm0}]

################################################################################
# Design constraints
################################################################################

################################################################################
# Clock constraints
################################################################################


################################################################################
# False path constraints
################################################################################


set_false_path -quiet -through [get_nets -hierarchical -filter {mr_ff == TRUE}]

set_false_path -quiet -to [get_pins -filter {REF_PIN_NAME == PRE} -of_objects [get_cells -hierarchical -filter {ars_ff1 == TRUE || ars_ff2 == TRUE}]]

set_max_delay 2 -quiet -from [get_pins -filter {REF_PIN_NAME == C} -of_objects [get_cells -hierarchical -filter {ars_ff1 == TRUE}]] -to [get_pins -filter {REF_PIN_NAME == D} -of_objects [get_cells -hierarchical -filter {ars_ff2 == TRUE}]]