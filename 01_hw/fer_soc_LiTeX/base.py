#!/usr/bin/env python3

from migen import *

from migen.genlib.io import CRG

from litex.build.generic_platform import *
from litex.build.xilinx           import XilinxPlatform

from litex.soc.integration.soc_core import *
from litex.soc.integration.builder  import *
from litex.soc.cores                import dna

from gateware.ios              import Button, Led, RGBLed, Switch
from gateware.rtl_pwm.core_ex1 import RTLPWM
from gateware.conv.core        import RTLCONV


# IOs ----------------------------------------------------------------------------------------------

_io = [
  ("clk125",    0, Pins("K17"), IOStandard("LVCMOS33")),

  ("cpu_reset", 0, Pins("K16"), IOStandard("LVCMOS33")),

  ("serial", 0,
    Subsignal("tx", Pins("V8")),
    Subsignal("rx", Pins("W8")),
    IOStandard("LVCMOS33"),
    ),

  # GPIOs
  ("user_btn", 0, Pins("K18"), IOStandard("LVCMOS33")),
  ("user_btn", 1, Pins("P16"), IOStandard("LVCMOS33")),
  ("user_btn", 2, Pins("K19"), IOStandard("LVCMOS33")),
  ("user_btn", 3, Pins("Y16"), IOStandard("LVCMOS33")),

  ("user_led",  0, Pins("M14"), IOStandard("LVCMOS33")),
  ("user_led",  1, Pins("M15"), IOStandard("LVCMOS33")),
  ("user_led",  2, Pins("G14"), IOStandard("LVCMOS33")),
  ("user_led",  3, Pins("D18"), IOStandard("LVCMOS33")),

  ("user_rgb_led", 0,
    Subsignal("r", Pins("Y11")),
    Subsignal("g", Pins("T5" )),
    Subsignal("b", Pins("Y12")),
    IOStandard("LVCMOS33"),
  ),

  ("user_sw",  0, Pins("G15"), IOStandard("LVCMOS33")),
  ("user_sw",  1, Pins("P15"), IOStandard("LVCMOS33")),
  ("user_sw",  2, Pins("W13"), IOStandard("LVCMOS33")),
  ("user_sw",  3, Pins("T16"), IOStandard("LVCMOS33")),

  ("user_pwm", 0, Pins("N15"), IOStandard("LVCMOS33"))
];


# Platform -----------------------------------------------------------------------------------------
class Platform(XilinxPlatform):
  default_clk_name   = "clk125";
  default_clk_period = 8.0;

  def __init__(self):
    XilinxPlatform.__init__(self, "xc7z020-clg400-1", _io, toolchain="vivado");


# Design -------------------------------------------------------------------------------------------
# Create our platform (fpga interface)
platform = Platform();

# Create our soc (fpga description)
class FER_SoC(SoCCore):

  def __init__(self, platform):
    sys_clk_freq = int(125e6);

    # SoC with CPU
    SoCCore.__init__(self, platform,
      cpu_type                 = "vexriscv",
      clk_freq                 = 125e6,
      ident                    = "LiteX CPU Test SoC", ident_version=True,
      integrated_rom_size      = 0x8000,
      integrated_main_ram_size = 0x4000);

    # Clock Reset Generation
    self.submodules.crg = CRG(platform.request("clk125"), ~platform.request("cpu_reset"));

    # UART parameters
    self.with_uart                = True;
    self.uart_name                = "serial";
    self.uart_baudrate            = 115200;
    self.uart_fifo_depth          = 16;

    # Timer parameters
    self.with_timer               = True;
    self.timer_uptime             = False;

    # FPGA identification
    self.submodules.dna = dna.DNA();
    self.add_csr("dna");

    # Buttons
    user_buttons = Cat(*[platform.request("user_btn", i) for i in range(4)]);
    self.submodules.buttons = Button(user_buttons);
    self.add_csr("buttons");

    # Led
    user_leds = Cat(*[platform.request("user_led", i) for i in range(4)]);
    self.submodules.leds = Led(user_leds);
    self.add_csr("leds");

    # RGB Led
    self.submodules.rgbled  = RGBLed(platform.request("user_rgb_led",  0));
    self.add_csr("rgbled");

    # Switches
    user_switches = Cat(*[platform.request("user_sw", i) for i in range(4)]);
    self.submodules.switches = Switch(user_switches);
    self.add_csr("switches");

    # RTL PWM
    self.submodules.rtlpwm = RTLPWM(platform, platform.request("user_pwm", 0));
    self.add_csr("rtlpwm");

    # CONV
    self.submodules.conv = RTLCONV(platform);
    self.add_csr("conv");


soc = FER_SoC(platform);


# Build --------------------------------------------------------------------------------------------
builder = Builder(soc, output_dir="build", csr_csv="test/csr.csv");
builder.build(build_name="top");
