---------------------------------------------------------------------
----                                                             ----
---- file:    conv_top_tb.vhd                                    ----
---- brief:   Module de Convolution Top Test bench               ----
---- details: Module test bench pour conv_top.vhd.               ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/08/23                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
entity conv_top_tb is
end conv_top_tb;
 
 
architecture behavior of conv_top_tb is 
 
  -- Component Declaration for the Unit Under Test (UUT)
  component conv_top is
    port( 
      clk, rstn, start :  in std_logic;

      -- Entrée 
      x01, x02, x03, x04, x05, x06, x07, x08, x09, x10, x11, x12, x13 :  in signed( 7 downto 0);
      x14, x15, x16, x17, x18, x19, x20, x21, x22, x23, x24, x25      :  in signed( 7 downto 0);

      -- Filtre, poids w
      w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, w11, w12, w13 :  in signed( 7 downto 0);
      w14, w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25      :  in signed( 7 downto 0);

      -- Offset entree
      offset_ent :  in signed(15 downto 0);
      
      -- cv_tab[0,i,j,f] valeur de cartes de caracteristiques precedents
      cv_in      :  in signed(31 downto 0);

      -- Sortie du noyau
      cv_out     : out signed(31 downto 0);
    
      -- conv_top done
      dn_cv      : out std_logic );
  end component conv_top;

 
  --Inputs -----------------------------
  signal clk, rstn, start : std_logic;
  
  -- Entrée 
  signal x01, x02, x03, x04, x05, x06, x07, x08, x09, x10, x11, x12, x13 :  signed( 7 downto 0) := (others => '0');
  signal x14, x15, x16, x17, x18, x19, x20, x21, x22, x23, x24, x25      :  signed( 7 downto 0) := (others => '0');

      -- Filtre, poids w
  signal w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, w11, w12, w13 :  signed( 7 downto 0) := (others => '0');
  signal w14, w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25      :  signed( 7 downto 0) := (others => '0');

  -- Offset entree 
  signal offset_ent : signed(15 downto 0) := (others => '0');
  
  -- cv_tab[0,i,j,f] valeur de cartes de caracteristiques precedents
  signal cv_in      : signed(31 downto 0) := (others => '0');

  
  --Outputs -----------------------------
  -- Sortie du noyau
  signal cv_out     : signed(31 downto 0) := (others => '0');
    
  -- conv_top done
  signal dn_cv      : std_logic := '0';

   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
   constant clock_period : time := 8 ns;
 
 
begin
 
 
  -- Instantiate the Unit Under Test (UUT)
  uut: conv_top
    port map(
      clk   => clk,
      rstn  => rstn,
      start => start,
      
      x01 => x01,
      x02 => x02,
      x03 => x03,
      x04 => x04,
      x05 => x05,
      x06 => x06,
      x07 => x07,
      x08 => x08,
      x09 => x09,
      x10 => x10,
      x11 => x11,
      x12 => x12,
      x13 => x13,
      x14 => x14,
      x15 => x15,
      x16 => x16,
      x17 => x17,
      x18 => x18,
      x19 => x19,
      x20 => x20,
      x21 => x21,
      x22 => x22,
      x23 => x23,
      x24 => x24,
      x25 => x25,
      
      w01 => w01,
      w02 => w02,
      w03 => w03,
      w04 => w04,
      w05 => w05,
      w06 => w06,
      w07 => w07,
      w08 => w08,
      w09 => w09,
      w10 => w10,
      w11 => w11,
      w12 => w12,
      w13 => w13,
      w14 => w14,
      w15 => w15,
      w16 => w16,
      w17 => w17,
      w18 => w18,
      w19 => w19,
      w20 => w20,
      w21 => w21,
      w22 => w22,
      w23 => w23,
      w24 => w24,
      w25 => w25,
      
      offset_ent => offset_ent,
      
      cv_in  => cv_in,
      
      cv_out => cv_out,
      dn_cv  => dn_cv );


  -- Clock process definitions
  clock_process :process
    begin
	  clk <= '0';
      wait for clock_period/2;
      clk <= '1';
      wait for clock_period/2;
  end process;
 

  -- Stimulus process
  stim_proc: process
    begin		
      -- hold reset state for 8 ns x 2.
      wait for clock_period*2;

      -- insert stimulus here
      rstn <= '1'; start <= '0';
      wait for clock_period*2;
      
      
      rstn <= '0'; start <= '0';
      wait for clock_period*2;


      rstn <= '1'; start <= '1';
      wait for clock_period*16;
      
      ------------------------------ 
      -- Filter 1, Entrée [0:6][0:6]
      ------------------------------
      -- entrée 
      x01 <= x"80"; x02 <= x"80"; x03 <= x"80"; x04 <= x"80"; x05 <= x"80";
      x06 <= x"80"; x07 <= x"80"; x08 <= x"80"; x09 <= x"80"; x10 <= x"80";
      x11 <= x"80"; x12 <= x"80"; x13 <= x"85"; x14 <= x"87"; x15 <= x"8B";      
      x16 <= x"80"; x17 <= x"80"; x18 <= x"85"; x19 <= x"85"; x20 <= x"85";
      x21 <= x"80"; x22 <= x"80"; x23 <= x"86"; x24 <= x"84"; x25 <= x"83";

      -- filtre
      w01 <= x"E9"; w02 <= x"49"; w03 <= x"16"; w04 <= x"BA"; w05 <= x"C3";
      w06 <= x"C4"; w07 <= x"96"; w08 <= x"95"; w09 <= x"28"; w10 <= x"C3";
      w11 <= x"54"; w12 <= x"CF"; w13 <= x"2D"; w14 <= x"27"; w15 <= x"E7";      
      w16 <= x"44"; w17 <= x"C5"; w18 <= x"E4"; w19 <= x"2C"; w20 <= x"F8";
      w21 <= x"30"; w22 <= x"F8"; w23 <= x"81"; w24 <= x"CE"; w25 <= x"93";
      
      offset_ent <= x"FFEE";
      cv_in      <= (others => '0');
      wait for clock_period*16;

      ------------------------------ 
      -- Filter 2, Entrée [9:14][52:57]
      ------------------------------
      -- entrée 
      x01 <= x"CB"; x02 <= x"D5"; x03 <= x"DC"; x04 <= x"E7"; x05 <= x"ED";
      x06 <= x"07"; x07 <= x"FA"; x08 <= x"F6"; x09 <= x"EA"; x10 <= x"C8";
      x11 <= x"C7"; x12 <= x"B3"; x13 <= x"9E"; x14 <= x"94"; x15 <= x"99";      
      x16 <= x"98"; x17 <= x"A3"; x18 <= x"BF"; x19 <= x"D6"; x20 <= x"DF";
      x21 <= x"EE"; x22 <= x"FA"; x23 <= x"FD"; x24 <= x"F6"; x25 <= x"E0";

      -- filtre
      w01 <= x"25"; w02 <= x"E0"; w03 <= x"F0"; w04 <= x"08"; w05 <= x"A0";
      w06 <= x"B2"; w07 <= x"9C"; w08 <= x"A6"; w09 <= x"08"; w10 <= x"C3";
      w11 <= x"0F"; w12 <= x"EC"; w13 <= x"ED"; w14 <= x"DA"; w15 <= x"C3";      
      w16 <= x"EA"; w17 <= x"BA"; w18 <= x"F7"; w19 <= x"C6"; w20 <= x"FD";
      w21 <= x"87"; w22 <= x"81"; w23 <= x"B3"; w24 <= x"E0"; w25 <= x"55";
    
      offset_ent <= x"FFFB";
      cv_in      <= (others => '0');
      wait for clock_period*16;

      ------------------------------ 
      -- Filter 2, Entrée [9:14][52:57]
      ------------------------------
      -- entrée 
      x01 <= x"CB"; x02 <= x"D5"; x03 <= x"DC"; x04 <= x"E7"; x05 <= x"ED";
      x06 <= x"07"; x07 <= x"FA"; x08 <= x"F6"; x09 <= x"EA"; x10 <= x"C8";
      x11 <= x"C7"; x12 <= x"B3"; x13 <= x"9E"; x14 <= x"94"; x15 <= x"99";      
      x16 <= x"98"; x17 <= x"A3"; x18 <= x"BF"; x19 <= x"D6"; x20 <= x"DF";
      x21 <= x"EE"; x22 <= x"FA"; x23 <= x"FD"; x24 <= x"F6"; x25 <= x"E0";

      -- filtre
      w01 <= x"25"; w02 <= x"E0"; w03 <= x"F0"; w04 <= x"08"; w05 <= x"A0";
      w06 <= x"B2"; w07 <= x"9C"; w08 <= x"A6"; w09 <= x"08"; w10 <= x"C3";
      w11 <= x"0F"; w12 <= x"EC"; w13 <= x"ED"; w14 <= x"DA"; w15 <= x"C3";      
      w16 <= x"EA"; w17 <= x"BA"; w18 <= x"F7"; w19 <= x"C6"; w20 <= x"FD";
      w21 <= x"87"; w22 <= x"81"; w23 <= x"B3"; w24 <= x"E0"; w25 <= x"55";
 
      offset_ent <= x"FFFB";
      cv_in      <= x"000002F5";
      wait for clock_period*16;


      wait;
   end process;

end;
