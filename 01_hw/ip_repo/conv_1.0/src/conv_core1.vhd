---------------------------------------------------------------------
----                                                             ----
---- file:    conv_core1.vhd                                     ----
---- brief:   Noyau 1 pour la convolution                        ----
---- details: Module pour implementer (x + offset_ent)*w         ----
----          Les entrée sont en int8 et int16,  et les          ----
----          résultats en (int32).                              ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/07/29                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity conv_core1 is
  port( 
    clk        :  in std_logic;

    -- Entrée  (x + offset_ent)
    xo01, xo02, xo03, xo04, xo05, xo06, xo07, xo08, xo09, xo10, xo11, xo12, xo13 :  in signed(15 downto 0);
    xo14, xo15, xo16, xo17, xo18, xo19, xo20, xo21, xo22, xo23, xo24, xo25       :  in signed(15 downto 0);

    -- Filtre, poids w
    w01, w02, w03, w04, w05, w06, w07, w08, w09, w10, w11, w12, w13 :  in signed( 7 downto 0);
    w14, w15, w16, w17, w18, w19, w20, w21, w22, w23, w24, w25      :  in signed( 7 downto 0);

    -- Sortie (x + offset_ent)*w
    xow01, xow02, xow03, xow04, xow05, xow06, xow07, xow08, xow09, xow10, xow11, xow12, xow13 : out signed(31 downto 0);
    xow14, xow15, xow16, xow17, xow18, xow19, xow20, xow21, xow22, xow23, xow24, xow25        : out signed(31 downto 0) );
end conv_core1;


architecture Behavioral of conv_core1 is

  -- signaux pour les registres de pipeline. 3 registers de pipeline p0, p1, p2.
  signal sg_xo01_p0, sg_xo01_p1, sg_xo01_p2 : signed(15 downto 0);
  signal sg_xo02_p0, sg_xo02_p1, sg_xo02_p2 : signed(15 downto 0); 
  signal sg_xo03_p0, sg_xo03_p1, sg_xo03_p2 : signed(15 downto 0); 
  signal sg_xo04_p0, sg_xo04_p1, sg_xo04_p2 : signed(15 downto 0); 
  signal sg_xo05_p0, sg_xo05_p1, sg_xo05_p2 : signed(15 downto 0); 
  signal sg_xo06_p0, sg_xo06_p1, sg_xo06_p2 : signed(15 downto 0); 
  signal sg_xo07_p0, sg_xo07_p1, sg_xo07_p2 : signed(15 downto 0); 
  signal sg_xo08_p0, sg_xo08_p1, sg_xo08_p2 : signed(15 downto 0); 
  signal sg_xo09_p0, sg_xo09_p1, sg_xo09_p2 : signed(15 downto 0); 
  signal sg_xo10_p0, sg_xo10_p1, sg_xo10_p2 : signed(15 downto 0); 
  signal sg_xo11_p0, sg_xo11_p1, sg_xo11_p2 : signed(15 downto 0); 
  signal sg_xo12_p0, sg_xo12_p1, sg_xo12_p2 : signed(15 downto 0); 
  signal sg_xo13_p0, sg_xo13_p1, sg_xo13_p2 : signed(15 downto 0); 
  signal sg_xo14_p0, sg_xo14_p1, sg_xo14_p2 : signed(15 downto 0); 
  signal sg_xo15_p0, sg_xo15_p1, sg_xo15_p2 : signed(15 downto 0); 
  signal sg_xo16_p0, sg_xo16_p1, sg_xo16_p2 : signed(15 downto 0); 
  signal sg_xo17_p0, sg_xo17_p1, sg_xo17_p2 : signed(15 downto 0); 
  signal sg_xo18_p0, sg_xo18_p1, sg_xo18_p2 : signed(15 downto 0); 
  signal sg_xo19_p0, sg_xo19_p1, sg_xo19_p2 : signed(15 downto 0); 
  signal sg_xo20_p0, sg_xo20_p1, sg_xo20_p2 : signed(15 downto 0); 
  signal sg_xo21_p0, sg_xo21_p1, sg_xo21_p2 : signed(15 downto 0); 
  signal sg_xo22_p0, sg_xo22_p1, sg_xo22_p2 : signed(15 downto 0); 
  signal sg_xo23_p0, sg_xo23_p1, sg_xo23_p2 : signed(15 downto 0); 
  signal sg_xo24_p0, sg_xo24_p1, sg_xo24_p2 : signed(15 downto 0); 
  signal sg_xo25_p0, sg_xo25_p1, sg_xo25_p2 : signed(15 downto 0); 

  signal sg_w01_p0, sg_w01_p1, sg_w01_p2 : signed(15 downto 0);
  signal sg_w02_p0, sg_w02_p1, sg_w02_p2 : signed(15 downto 0); 
  signal sg_w03_p0, sg_w03_p1, sg_w03_p2 : signed(15 downto 0); 
  signal sg_w04_p0, sg_w04_p1, sg_w04_p2 : signed(15 downto 0); 
  signal sg_w05_p0, sg_w05_p1, sg_w05_p2 : signed(15 downto 0); 
  signal sg_w06_p0, sg_w06_p1, sg_w06_p2 : signed(15 downto 0); 
  signal sg_w07_p0, sg_w07_p1, sg_w07_p2 : signed(15 downto 0); 
  signal sg_w08_p0, sg_w08_p1, sg_w08_p2 : signed(15 downto 0); 
  signal sg_w09_p0, sg_w09_p1, sg_w09_p2 : signed(15 downto 0); 
  signal sg_w10_p0, sg_w10_p1, sg_w10_p2 : signed(15 downto 0); 
  signal sg_w11_p0, sg_w11_p1, sg_w11_p2 : signed(15 downto 0); 
  signal sg_w12_p0, sg_w12_p1, sg_w12_p2 : signed(15 downto 0); 
  signal sg_w13_p0, sg_w13_p1, sg_w13_p2 : signed(15 downto 0); 
  signal sg_w14_p0, sg_w14_p1, sg_w14_p2 : signed(15 downto 0); 
  signal sg_w15_p0, sg_w15_p1, sg_w15_p2 : signed(15 downto 0); 
  signal sg_w16_p0, sg_w16_p1, sg_w16_p2 : signed(15 downto 0); 
  signal sg_w17_p0, sg_w17_p1, sg_w17_p2 : signed(15 downto 0); 
  signal sg_w18_p0, sg_w18_p1, sg_w18_p2 : signed(15 downto 0); 
  signal sg_w19_p0, sg_w19_p1, sg_w19_p2 : signed(15 downto 0); 
  signal sg_w20_p0, sg_w20_p1, sg_w20_p2 : signed(15 downto 0); 
  signal sg_w21_p0, sg_w21_p1, sg_w21_p2 : signed(15 downto 0); 
  signal sg_w22_p0, sg_w22_p1, sg_w22_p2 : signed(15 downto 0); 
  signal sg_w23_p0, sg_w23_p1, sg_w23_p2 : signed(15 downto 0); 
  signal sg_w24_p0, sg_w24_p1, sg_w24_p2 : signed(15 downto 0); 
  signal sg_w25_p0, sg_w25_p1, sg_w25_p2 : signed(15 downto 0); 

  signal sg_xow01_p0, sg_xow01_p1, sg_xow01_p2 : signed(31 downto 0);
  signal sg_xow02_p0, sg_xow02_p1, sg_xow02_p2 : signed(31 downto 0);
  signal sg_xow03_p0, sg_xow03_p1, sg_xow03_p2 : signed(31 downto 0);
  signal sg_xow04_p0, sg_xow04_p1, sg_xow04_p2 : signed(31 downto 0);
  signal sg_xow05_p0, sg_xow05_p1, sg_xow05_p2 : signed(31 downto 0);
  signal sg_xow06_p0, sg_xow06_p1, sg_xow06_p2 : signed(31 downto 0);
  signal sg_xow07_p0, sg_xow07_p1, sg_xow07_p2 : signed(31 downto 0);
  signal sg_xow08_p0, sg_xow08_p1, sg_xow08_p2 : signed(31 downto 0);
  signal sg_xow09_p0, sg_xow09_p1, sg_xow09_p2 : signed(31 downto 0);
  signal sg_xow10_p0, sg_xow10_p1, sg_xow10_p2 : signed(31 downto 0);
  signal sg_xow11_p0, sg_xow11_p1, sg_xow11_p2 : signed(31 downto 0);
  signal sg_xow12_p0, sg_xow12_p1, sg_xow12_p2 : signed(31 downto 0);
  signal sg_xow13_p0, sg_xow13_p1, sg_xow13_p2 : signed(31 downto 0);
  signal sg_xow14_p0, sg_xow14_p1, sg_xow14_p2 : signed(31 downto 0);
  signal sg_xow15_p0, sg_xow15_p1, sg_xow15_p2 : signed(31 downto 0);
  signal sg_xow16_p0, sg_xow16_p1, sg_xow16_p2 : signed(31 downto 0);
  signal sg_xow17_p0, sg_xow17_p1, sg_xow17_p2 : signed(31 downto 0);
  signal sg_xow18_p0, sg_xow18_p1, sg_xow18_p2 : signed(31 downto 0);
  signal sg_xow19_p0, sg_xow19_p1, sg_xow19_p2 : signed(31 downto 0);
  signal sg_xow20_p0, sg_xow20_p1, sg_xow20_p2 : signed(31 downto 0);
  signal sg_xow21_p0, sg_xow21_p1, sg_xow21_p2 : signed(31 downto 0);
  signal sg_xow22_p0, sg_xow22_p1, sg_xow22_p2 : signed(31 downto 0);
  signal sg_xow23_p0, sg_xow23_p1, sg_xow23_p2 : signed(31 downto 0);
  signal sg_xow24_p0, sg_xow24_p1, sg_xow24_p2 : signed(31 downto 0);
  signal sg_xow25_p0, sg_xow25_p1, sg_xow25_p2 : signed(31 downto 0);

  attribute USE_DSP : string;
  attribute USE_DSP of sg_xow01_p0, sg_xow02_p0, sg_xow03_p0, sg_xow04_p0, sg_xow05_p0 : signal is "YES";
  attribute USE_DSP of sg_xow06_p0, sg_xow07_p0, sg_xow08_p0, sg_xow09_p0, sg_xow10_p0 : signal is "YES";
  attribute USE_DSP of sg_xow11_p0, sg_xow12_p0, sg_xow13_p0, sg_xow14_p0, sg_xow15_p0 : signal is "YES";
  attribute USE_DSP of sg_xow16_p0, sg_xow17_p0, sg_xow18_p0, sg_xow19_p0, sg_xow20_p0 : signal is "YES";
  attribute USE_DSP of sg_xow21_p0, sg_xow22_p0, sg_xow23_p0, sg_xow24_p0, sg_xow25_p0 : signal is "YES";



begin


  -- Computer (X + offset_ent)*W --
  ---------------------------------
  process(clk)
    begin
      if (clk'event and clk = '1') then

        -- wx01
        sg_xo01_p0 <= xo01;
        sg_xo01_p1 <= sg_xo01_p0;
        sg_xo01_p2 <= sg_xo01_p1;

        sg_w01_p0  <= resize(w01, 16); 
        sg_w01_p1  <= sg_w01_p0;
        sg_w01_p2  <= sg_w01_p1;

        sg_xow01_p0 <= sg_xo01_p2 * sg_w01_p2;
        sg_xow01_p1 <= sg_xow01_p0;
        sg_xow01_p2 <= sg_xow01_p1;
        

        -- wx02
        sg_xo02_p0 <= xo02;
        sg_xo02_p1 <= sg_xo02_p0;
        sg_xo02_p2 <= sg_xo02_p1;

        sg_w02_p0  <= resize(w02, 16); 
        sg_w02_p1  <= sg_w02_p0;
        sg_w02_p2  <= sg_w02_p1;

        sg_xow02_p0 <= sg_xo02_p2 * sg_w02_p2;
        sg_xow02_p1 <= sg_xow02_p0;
        sg_xow02_p2 <= sg_xow02_p1;


        -- wx03
        sg_xo03_p0 <= xo03;
        sg_xo03_p1 <= sg_xo03_p0;
        sg_xo03_p2 <= sg_xo03_p1;

        sg_w03_p0  <= resize(w03, 16); 
        sg_w03_p1  <= sg_w03_p0;
        sg_w03_p2  <= sg_w03_p1;

        sg_xow03_p0 <= sg_xo03_p2 * sg_w03_p2;
        sg_xow03_p1 <= sg_xow03_p0;
        sg_xow03_p2 <= sg_xow03_p1;
        

        -- wx04
        sg_xo04_p0 <= xo04;
        sg_xo04_p1 <= sg_xo04_p0;
        sg_xo04_p2 <= sg_xo04_p1;

        sg_w04_p0  <= resize(w04, 16); 
        sg_w04_p1  <= sg_w04_p0;
        sg_w04_p2  <= sg_w04_p1;

        sg_xow04_p0 <= sg_xo04_p2 * sg_w04_p2;
        sg_xow04_p1 <= sg_xow04_p0;
        sg_xow04_p2 <= sg_xow04_p1;
        

        -- wx05
        sg_xo05_p0 <= xo05;
        sg_xo05_p1 <= sg_xo05_p0;
        sg_xo05_p2 <= sg_xo05_p1;

        sg_w05_p0  <= resize(w05, 16); 
        sg_w05_p1  <= sg_w05_p0;
        sg_w05_p2  <= sg_w05_p1;

        sg_xow05_p0 <= sg_xo05_p2 * sg_w05_p2;
        sg_xow05_p1 <= sg_xow05_p0;
        sg_xow05_p2 <= sg_xow05_p1;
        

        -- wx06
        sg_xo06_p0 <= xo06;
        sg_xo06_p1 <= sg_xo06_p0;
        sg_xo06_p2 <= sg_xo06_p1;

        sg_w06_p0  <= resize(w06, 16); 
        sg_w06_p1  <= sg_w06_p0;
        sg_w06_p2  <= sg_w06_p1;

        sg_xow06_p0 <= sg_xo06_p2 * sg_w06_p2;
        sg_xow06_p1 <= sg_xow06_p0;
        sg_xow06_p2 <= sg_xow06_p1;
        

        -- wx07
        sg_xo07_p0 <= xo07;
        sg_xo07_p1 <= sg_xo07_p0;
        sg_xo07_p2 <= sg_xo07_p1;

        sg_w07_p0  <= resize(w07, 16); 
        sg_w07_p1  <= sg_w07_p0;
        sg_w07_p2  <= sg_w07_p1;

        sg_xow07_p0 <= sg_xo07_p2 * sg_w07_p2;
        sg_xow07_p1 <= sg_xow07_p0;
        sg_xow07_p2 <= sg_xow07_p1;
        

        -- wx08
        sg_xo08_p0 <= xo08;
        sg_xo08_p1 <= sg_xo08_p0;
        sg_xo08_p2 <= sg_xo08_p1;

        sg_w08_p0  <= resize(w08, 16); 
        sg_w08_p1  <= sg_w08_p0;
        sg_w08_p2  <= sg_w08_p1;

        sg_xow08_p0 <= sg_xo08_p2 * sg_w08_p2;
        sg_xow08_p1 <= sg_xow08_p0;
        sg_xow08_p2 <= sg_xow08_p1;
        

        -- wx09
        sg_xo09_p0 <= xo09;
        sg_xo09_p1 <= sg_xo09_p0;
        sg_xo09_p2 <= sg_xo09_p1;

        sg_w09_p0  <= resize(w09, 16); 
        sg_w09_p1  <= sg_w09_p0;
        sg_w09_p2  <= sg_w09_p1;

        sg_xow09_p0 <= sg_xo09_p2 * sg_w09_p2;
        sg_xow09_p1 <= sg_xow09_p0;
        sg_xow09_p2 <= sg_xow09_p1;
        

        -- wx10
        sg_xo10_p0 <= xo10;
        sg_xo10_p1 <= sg_xo10_p0;
        sg_xo10_p2 <= sg_xo10_p1;

        sg_w10_p0  <= resize(w10, 16);
        sg_w10_p1  <= sg_w10_p0;
        sg_w10_p2  <= sg_w10_p1;

        sg_xow10_p0 <= sg_xo10_p2 * sg_w10_p2;
        sg_xow10_p1 <= sg_xow10_p0;
        sg_xow10_p2 <= sg_xow10_p1;
        

        -- wx11
        sg_xo11_p0 <= xo11;
        sg_xo11_p1 <= sg_xo11_p0;
        sg_xo11_p2 <= sg_xo11_p1;

        sg_w11_p0  <= resize(w11, 16);
        sg_w11_p1  <= sg_w11_p0;
        sg_w11_p2  <= sg_w11_p1;

        sg_xow11_p0 <= sg_xo11_p2 * sg_w11_p2;
        sg_xow11_p1 <= sg_xow11_p0;
        sg_xow11_p2 <= sg_xow11_p1;
        

        -- wx12
        sg_xo12_p0 <= xo12;
        sg_xo12_p1 <= sg_xo12_p0;
        sg_xo12_p2 <= sg_xo12_p1;

        sg_w12_p0  <= resize(w12, 16); 
        sg_w12_p1  <= sg_w12_p0;
        sg_w12_p2  <= sg_w12_p1;

        sg_xow12_p0 <= sg_xo12_p2 * sg_w12_p2;
        sg_xow12_p1 <= sg_xow12_p0;
        sg_xow12_p2 <= sg_xow12_p1;


        -- wx13
        sg_xo13_p0 <= xo13;
        sg_xo13_p1 <= sg_xo13_p0;
        sg_xo13_p2 <= sg_xo13_p1;

        sg_w13_p0  <= resize(w13, 16); 
        sg_w13_p1  <= sg_w13_p0;
        sg_w13_p2  <= sg_w13_p1;

        sg_xow13_p0 <= sg_xo13_p2 * sg_w13_p2;
        sg_xow13_p1 <= sg_xow13_p0;
        sg_xow13_p2 <= sg_xow13_p1;
        

        -- wx14
        sg_xo14_p0 <= xo14;
        sg_xo14_p1 <= sg_xo14_p0;
        sg_xo14_p2 <= sg_xo14_p1;

        sg_w14_p0  <= resize(w14, 16); 
        sg_w14_p1  <= sg_w14_p0;
        sg_w14_p2  <= sg_w14_p1;

        sg_xow14_p0 <= sg_xo14_p2 * sg_w14_p2;
        sg_xow14_p1 <= sg_xow14_p0;
        sg_xow14_p2 <= sg_xow14_p1;
        

        -- wx15
        sg_xo15_p0 <= xo15;
        sg_xo15_p1 <= sg_xo15_p0;
        sg_xo15_p2 <= sg_xo15_p1;

        sg_w15_p0  <= resize(w15, 16); 
        sg_w15_p1  <= sg_w15_p0;
        sg_w15_p2  <= sg_w15_p1;

        sg_xow15_p0 <= sg_xo15_p2 * sg_w15_p2;
        sg_xow15_p1 <= sg_xow15_p0;
        sg_xow15_p2 <= sg_xow15_p1;
        

        -- wx16
        sg_xo16_p0 <= xo16;
        sg_xo16_p1 <= sg_xo16_p0;
        sg_xo16_p2 <= sg_xo16_p1;

        sg_w16_p0  <= resize(w16, 16); 
        sg_w16_p1  <= sg_w16_p0;
        sg_w16_p2  <= sg_w16_p1;

        sg_xow16_p0 <= sg_xo16_p2 * sg_w16_p2;
        sg_xow16_p1 <= sg_xow16_p0;
        sg_xow16_p2 <= sg_xow16_p1;


        -- wx17
        sg_xo17_p0 <= xo17;
        sg_xo17_p1 <= sg_xo17_p0;
        sg_xo17_p2 <= sg_xo17_p1;

        sg_w17_p0  <= resize(w17, 16); 
        sg_w17_p1  <= sg_w17_p0;
        sg_w17_p2  <= sg_w17_p1;

        sg_xow17_p0 <= sg_xo17_p2 * sg_w17_p2;
        sg_xow17_p1 <= sg_xow17_p0;
        sg_xow17_p2 <= sg_xow17_p1;


        -- wx18
        sg_xo18_p0 <= xo18;
        sg_xo18_p1 <= sg_xo18_p0;
        sg_xo18_p2 <= sg_xo18_p1;

        sg_w18_p0  <= resize(w18, 16); 
        sg_w18_p1  <= sg_w18_p0;
        sg_w18_p2  <= sg_w18_p1;

        sg_xow18_p0 <= sg_xo18_p2 *  sg_w18_p2;
        sg_xow18_p1 <= sg_xow18_p0;
        sg_xow18_p2 <= sg_xow18_p1;


        -- wx19
        sg_xo19_p0 <= xo19;
        sg_xo19_p1 <= sg_xo19_p0;
        sg_xo19_p2 <= sg_xo19_p1;

        sg_w19_p0  <= resize(w19, 16); 
        sg_w19_p1  <= sg_w19_p0;
        sg_w19_p2  <= sg_w19_p1;

        sg_xow19_p0 <= sg_xo19_p2 * sg_w19_p2;
        sg_xow19_p1 <= sg_xow19_p0;
        sg_xow19_p2 <= sg_xow19_p1;


        -- wx20
        sg_xo20_p0 <= xo20;
        sg_xo20_p1 <= sg_xo20_p0;
        sg_xo20_p2 <= sg_xo20_p1;

        sg_w20_p0  <= resize(w20, 16); 
        sg_w20_p1  <= sg_w20_p0;
        sg_w20_p2  <= sg_w20_p1;

        sg_xow20_p0 <= sg_xo20_p2 * sg_w20_p2;
        sg_xow20_p1 <= sg_xow20_p0;
        sg_xow20_p2 <= sg_xow20_p1;


        -- wx21
        sg_xo21_p0 <= xo21;
        sg_xo21_p1 <= sg_xo21_p0;
        sg_xo21_p2 <= sg_xo21_p1;

        sg_w21_p0  <= resize(w21, 16); 
        sg_w21_p1  <= sg_w21_p0;
        sg_w21_p2  <= sg_w21_p1;

        sg_xow21_p0 <= sg_xo21_p2 * sg_w21_p2;
        sg_xow21_p1 <= sg_xow21_p0;
        sg_xow21_p2 <= sg_xow21_p1;


        -- wx22
        sg_xo22_p0 <= xo22;
        sg_xo22_p1 <= sg_xo22_p0;
        sg_xo22_p2 <= sg_xo22_p1;

        sg_w22_p0  <= resize(w22, 16); 
        sg_w22_p1  <= sg_w22_p0;
        sg_w22_p2  <= sg_w22_p1;

        sg_xow22_p0 <= sg_xo22_p2 * sg_w22_p2;
        sg_xow22_p1 <= sg_xow22_p0;
        sg_xow22_p2 <= sg_xow22_p1;


        -- wx23
        sg_xo23_p0 <= xo23;
        sg_xo23_p1 <= sg_xo23_p0;
        sg_xo23_p2 <= sg_xo23_p1;

        sg_w23_p0  <= resize(w23, 16); 
        sg_w23_p1  <= sg_w23_p0;
        sg_w23_p2  <= sg_w23_p1;

        sg_xow23_p0 <= sg_xo23_p2 * sg_w23_p2;
        sg_xow23_p1 <= sg_xow23_p0;
        sg_xow23_p2 <= sg_xow23_p1;


        -- wx24
        sg_xo24_p0 <= xo24;
        sg_xo24_p1 <= sg_xo24_p0;
        sg_xo24_p2 <= sg_xo24_p1;

        sg_w24_p0  <= resize(w24, 16); 
        sg_w24_p1  <= sg_w24_p0;
        sg_w24_p2  <= sg_w24_p1;

        sg_xow24_p0 <= sg_xo24_p2 * sg_w24_p2;
        sg_xow24_p1 <= sg_xow24_p0;
        sg_xow24_p2 <= sg_xow24_p1;


        -- wx25
        sg_xo25_p0 <= xo25;
        sg_xo25_p1 <= sg_xo25_p0;
        sg_xo25_p2 <= sg_xo25_p1;

        sg_w25_p0  <= resize(w25, 16); 
        sg_w25_p1  <= sg_w25_p0;
        sg_w25_p2  <= sg_w25_p1;

        sg_xow25_p0 <= sg_xo25_p2 * sg_w25_p2;
        sg_xow25_p1 <= sg_xow25_p0;
        sg_xow25_p2 <= sg_xow25_p1;


      end if;
  end process;
  
  xow01 <= sg_xow01_p2;
  xow02 <= sg_xow02_p2;
  xow03 <= sg_xow03_p2;     
  xow04 <= sg_xow04_p2;
  xow05 <= sg_xow05_p2;
  xow06 <= sg_xow06_p2;
  xow07 <= sg_xow07_p2;  
  xow08 <= sg_xow08_p2;  
  xow09 <= sg_xow09_p2;  
  xow10 <= sg_xow10_p2;  
  xow11 <= sg_xow11_p2;  
  xow12 <= sg_xow12_p2;  
  xow13 <= sg_xow13_p2;    
  xow14 <= sg_xow14_p2;
  xow15 <= sg_xow15_p2;  
  xow16 <= sg_xow16_p2;  
  xow17 <= sg_xow17_p2;  
  xow18 <= sg_xow18_p2;  
  xow19 <= sg_xow19_p2;  
  xow20 <= sg_xow20_p2;  
  xow21 <= sg_xow21_p2;  
  xow22 <= sg_xow22_p2;  
  xow23 <= sg_xow23_p2;  
  xow24 <= sg_xow24_p2;  
  xow25 <= sg_xow25_p2;  
  
  
end Behavioral;