---------------------------------------------------------------------
----                                                             ----
---- file:    conv_core2.vhd                                     ----
---- brief:   Noyau 2 pour la convolution                        ----
---- details: Module pour faire la somme des                     ----
----          terms: (x + offset_ent)*w                          ----
----          Les entrée sont en int32 et le                     ----
----          résultats est aussi en (int32).                    ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/07/29                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity conv_core2 is
  port( 
    -- Entrée (x + offset_ent)*w
    xow01, xow02, xow03, xow04, xow05, xow06, xow07, xow08, xow09, xow10, xow11, xow12, xow13 :  in signed(31 downto 0);
    xow14, xow15, xow16, xow17, xow18, xow19, xow20, xow21, xow22, xow23, xow24, xow25        :  in signed(31 downto 0);

    -- Sortie somme(xow1 + xow2 + ... + xow25)
    xow : out signed(31 downto 0) );
end conv_core2;


architecture Behavioral of conv_core2 is
  
begin

  -- Computer la somme de chaque (x + offset_ent)*w
  -- sommer(xow1 + xow2 + ... + xow25) --
  ---------------------------------
  xow <= xow01 + xow02 + xow03 + xow04 + xow05 + xow06 + xow07 + xow08 + xow09 + xow10 + xow11 + xow12 + xow13 + xow14 + xow15 + xow16 + xow17 + xow18 + xow19 + xow20 + xow21 + xow22 + xow23 + xow24 + xow25;
    
end Behavioral;
