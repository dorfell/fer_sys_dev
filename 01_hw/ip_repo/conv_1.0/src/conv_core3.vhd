---------------------------------------------------------------------
----                                                             ----
---- file:    conv_core3.vhd                                     ----
---- brief:   Noyau 3 pour la convolution                        ----
---- details: Module pour faire la somme des                     ----
----          terms: (x + offset_ent)*w + cv_in[0,i,j.f]         ----
----          cv_in: valeur précédent de la convolution.         ----
----          Les entrée sont en int32 et le                     ----
----          résultats est aussi en (int32).                    ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/08/21                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity conv_core3 is
  port( 
    -- Entrée somme (xow1 + xow2 + ... + xow25)
    xow   :  in signed(31 downto 0);
    
    -- cv_tab[0,i,j,f] valeur de cartes de caractéristiques précédents
    cv_in :  in signed(31 downto 0);

    -- (x + offset_ent)*w + cv_in[0,i,j,f]
    xowc  : out signed(31 downto 0) );
end conv_core3;


architecture Behavioral of conv_core3 is
  
begin

  -- Computer (x + offset_ent)*w + cv_in[0,i,j,f]
  ---------------------------------
  xowc <= cv_in + xow;
  
end Behavioral;