---------------------------------------------------------------------
----                                                             ----
---- file:    conv_core0.vhd                                     ----
---- brief:   Noyau 0 pour la convolution                        ----
---- details: Module pour implementer x + offset_ent             ----
----          Les entrées sont nombres entiers entre             ----
----          -128 et 127 (int8). Les résultats sont             ----
----          nombres entiers en int16.                          ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/07/29                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity conv_core0 is
  port( 
    -- Entrée 
    x01, x02, x03, x04, x05, x06, x07, x08, x09, x10, x11, x12, x13 :  in signed( 7 downto 0);
    x14, x15, x16, x17, x18, x19, x20, x21, x22, x23, x24, x25      :  in signed( 7 downto 0);


    -- Offset entree 
    offset_ent : in signed(15 downto 0);

    -- Sortie (x + offset_ent)
    xo01, xo02, xo03, xo04, xo05, xo06, xo07, xo08, xo09, xo10, xo11, xo12, xo13 :  out signed(15 downto 0);
    xo14, xo15, xo16, xo17, xo18, xo19, xo20, xo21, xo22, xo23, xo24, xo25       :  out signed(15 downto 0) );
end conv_core0;


architecture Behavioral of conv_core0 is
  
begin

  -- Computer (x + offset_ent)   --
  ---------------------------------
  xo01 <= resize(x01, 16) + offset_ent; xo02 <= resize(x02, 16) + offset_ent; 
  xo03 <= resize(x03, 16) + offset_ent; xo04 <= resize(x04, 16) + offset_ent; 
  xo05 <= resize(x05, 16) + offset_ent;

  xo06 <= resize(x06, 16) + offset_ent; xo07 <= resize(x07, 16) + offset_ent; 
  xo08 <= resize(x08, 16) + offset_ent; xo09 <= resize(x09, 16) + offset_ent; 
  xo10 <= resize(x10, 16) + offset_ent;

  xo11 <= resize(x11, 16) + offset_ent; xo12 <= resize(x12, 16) + offset_ent; 
  xo13 <= resize(x13, 16) + offset_ent; xo14 <= resize(x14, 16) + offset_ent; 
  xo15 <= resize(x15, 16) + offset_ent;

  xo16 <= resize(x16, 16) + offset_ent; xo17 <= resize(x17, 16) + offset_ent; 
  xo18 <= resize(x18, 16) + offset_ent; xo19 <= resize(x19, 16) + offset_ent; 
  xo20 <= resize(x20, 16) + offset_ent;

  xo21 <= resize(x21, 16) + offset_ent; xo22 <= resize(x22, 16) + offset_ent; 
  xo23 <= resize(x23, 16) + offset_ent; xo24 <= resize(x24, 16) + offset_ent; 
  xo25 <= resize(x25, 16) + offset_ent;


end Behavioral;