---------------------------------------------------------------------
----                                                             ----
---- file:    maxpool.vhd                                        ----
---- brief:   Module de MaxPooling                               ----
---- details: Module top level pour le noyau de                  ----
----          MaxPooling à la facon de tflite.                   ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/10/24                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity maxpool is
  port (
    clk, rstn, start :  in std_logic;
 
    -- Entrée
    x01, x02 :  in signed( 7 downto 0);
    x03, x04 :  in signed( 7 downto 0);
    
    -- Sortie
    mp_out   : out signed( 7 downto 0);
    mp_dn    : out std_logic );

end maxpool;

architecture Behavioral of maxpool is

  
  -- Compteur de cycles: registre, enable,done. 
  signal dd, qq : std_logic_vector(1 downto 0) := (others=>'0');


begin


  ---------------------
  -- Compteur de cycles (cc)
  ---------------------
  -- Overflow pour clock cycles
  mux_ovf: 
    dd <= qq when (qq = "11") else qq + 1;

  -- Registre du compteur
  process (clk, rstn)
    begin  
      if (rstn = '0') then
        qq <= (others => '0');
      elsif (clk'event and clk = '1') then
        if (start = '1') then
          qq <= dd;
        else
          qq <= qq;
        end if;
      end if;
  end process;

  -- Done aprés cycles
  mp_dn <= '1' when (qq = "11") else '0'; 


  ---------------------
  -- MaxPooling (2,2)
  ---------------------
  mp_out <= x01 when ((x01 >= x02) and (x01 >= x03) and (x01 >= x04)) else
            x02 when ((x02 >= x01) and (x02 >= x03) and (x02 >= x04)) else
            x03 when ((x03 >= x01) and (x03 >= x02) and (x03 >= x04)) else
            x04 when ((x04 >= x01) and (x04 >= x02) and (x04 >= x03)) else
            x01;    

end Behavioral;