---------------------------------------------------------------------
----                                                             ----
---- file:    tflite_core5.vhd                                   ----
---- brief:   Module noyau 5 de tflite                           ----
---- details: Module pour implementer les operations             ----
----          de clamp et addition de offset_sor.                ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/11/08                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity tflite_core5 is
  port( 
    -- MultiplyByQuantizedMultiplier
    mbqm       :  in signed(31 downto 0); 

    -- Offset sortie
    offset_sor :  in signed(15 downto 0); 
      
    -- Sortie du noyau
    tf_out     : out signed(31 downto 0) );
end tflite_core5;


architecture Behavioral of tflite_core5 is

  
  -- signaux pour le min, max et le offset_sor
  signal sg_0    : signed(31 downto 0) := to_signed(   0, 32);
  signal sg_255  : signed(31 downto 0) := to_signed( 255, 32);
  
  -- signaux pour le min, max et le offset_sor
  signal sg_n128 : signed(31 downto 0) := to_signed(-128, 32);
  signal sg_127  : signed(31 downto 0) := to_signed( 127, 32);
        
  signal sg_sum0 : signed(31 downto 0); 
  signal sg_sum1 : signed(31 downto 0); 
  
  
begin


  --! Clamp la convolution
  -----------------------------------------

  -- sum = min( max(sum, 0), 255); 
--  sg_sum0 <= mbqm          when ((mbqm > sg_0) and (mbqm < sg_255)) else
--             sg_255        when ((mbqm > sg_0) and (mbqm > sg_255)) else
--             sg_0          when ((mbqm < sg_0) and (sg_0 < sg_255)) else
--             sg_255        when ((mbqm < sg_0) and (sg_0 > sg_255)) else
--             (others=>'0');


--  -- sum = sum + offset_sor
--  sg_sum1 <= sg_sum0 + resize(offset_sor, 32);


--  -- sum = min( max(sum, -128), 127);    
--  tf_out  <= sg_sum1       when ((sg_sum1 > sg_n128) and (sg_sum1 < sg_127)) else
--             sg_127        when ((sg_sum1 > sg_n128) and (sg_sum1 > sg_127)) else
--             sg_n128       when ((sg_sum1 < sg_n128) and (sg_n128 < sg_127)) else
--             sg_127        when ((sg_sum1 < sg_n128) and (sg_n128 > sg_127)) else
--             (others=>'0');
             
  tf_out <= mbqm;             
    
end Behavioral;