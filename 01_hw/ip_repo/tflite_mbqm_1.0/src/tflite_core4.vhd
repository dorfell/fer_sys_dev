---------------------------------------------------------------------
----                                                             ----
---- file:    tflite_core4.vhd                                   ----
---- brief:   Module noyau 3 de tflite                           ----
---- details: Module pour implementer les operations             ----
----          arithmetiques qui ont besoint de pipeline.         ----
----          SaturatingRoundingDoublingHighMul                  ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/11/08                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity tflite_core4 is
  port( 
    -- SaturatingRoundingDoublingHighMul (SRDHM) sortie
    srdhm :  in signed(31 downto 0);
    
    -- shift 
    shift :  in signed(7 downto 0);
    
    -- MultiplyByQuantizedMultiplier (MBQM) sortie
    mbqm  : out signed(31 downto 0) );
end tflite_core4;


architecture Behavioral of tflite_core4 is
  
  --------------------------
  -- Tflite functions 
  --------------------------
  
  -- MaskIfNonZero
  function MaskIfNonZero(
    a : boolean ) return signed is
    variable tmp : signed(31 downto 0);
    begin
      if (a = true) then
        tmp := x"FFFFFFFF";                                -- BitNot(zero)
      else 
        tmp := x"00000000";                                -- zero
      end if;  
      return tmp;
  end function MaskIfNonZero;


  -- MaskIfGreaterThan
  function MaskIfGreaterThan(
    a : signed(31 downto 0);
    b : signed(31 downto 0) ) return signed is
    begin
      return MaskIfNonZero(a > b);
  end function MaskIfGreaterThan;


  -- MaskIfLessThan
  function MaskIfLessThan(
    a : signed(31 downto 0);
    b : signed(31 downto 0) ) return signed is
    begin
      return MaskIfNonZero(a < b);
  end function MaskIfLessThan;


  -- RoundingDivideByPOT
  function RoundingDivideByPOT(
    x        : signed(31 downto 0);
    exponent : signed( 7 downto 0) ) return signed is
  
    variable mask      : signed(31 downto 0);
    variable zero      : signed(31 downto 0) := x"00000000";
    variable one       : signed(31 downto 0) := x"00000001";
    variable remainder : signed(31 downto 0); 
    variable threshold : signed(31 downto 0);

    begin
  
      mask      := shift_left(to_signed(1, 32), to_integer(exponent)) - 1;
      remainder := (x and mask);
      threshold := ( shift_right(mask, 1) + (MaskIfLessThan(x, zero) and one) );
  
      return ( shift_right(x, to_integer(exponent)) + (MaskIfGreaterThan(remainder, threshold) and one) );
  end function RoundingDivideByPOT;

  
  -- MultiplyByQuantizedMultiplier 
  function MultiplyByQuantizedMultiplier(
    x                    : signed(31 downto 0);            -- srdhm 
    shift                : signed( 7 downto 0) ) return signed is
    
    variable right_shift : signed(7 downto 0);
    
    begin
    
      if (shift > 0) then 
        right_shift := (others=>'0');
      else
        right_shift := -shift;
      end if; 
                          
      return RoundingDivideByPOT(x, right_shift); 
  end function MultiplyByQuantizedMultiplier;


  
begin

  -- Sortie de noyau  
  mbqm <= MultiplyByQuantizedMultiplier(srdhm, shift);  
    
end Behavioral;