---------------------------------------------------------------------
----                                                             ----
---- file:    conv_top_tb.vhd                                    ----
---- brief:   Module de Convolution Top Test bench               ----
---- details: Module test bench pour conv_top.vhd.               ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/08/23                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
entity tflite_mbqm_tb is
end tflite_mbqm_tb;
 
 
architecture behavior of tflite_mbqm_tb is 
 
  -- Component Declaration for the Unit Under Test (UUT)
  component tflite_mbqm is
    port( 
      clk, rstn, start :  in std_logic;
  
      -- cv_in somme(x+offset_ent)*w + cv_in[0,i,j,f]
      cv_in  :  in signed(31 downto 0);

      -- bias 
      bias   :  in signed(31 downto 0);

      -- M0
      M0     :  in signed(31 downto 0);

      -- shift
      shift :  in signed( 7 downto 0);
      
      -- sortie du noyau
      tf_out : out signed(31 downto 0);
    
      -- conv_tflite done
      dn_tf : out std_logic ); 
  end component tflite_mbqm;

 
  --Inputs -----------------------------
  signal clk, rstn, start : std_logic;
  
  -- cv_in somme(x+offset_ent)*w + cv_in[0,i,j,f]
  signal cv_in : signed(31 downto 0);
    
  -- bias
  signal bias  : signed(31 downto 0) := (others => '0');

    -- M0
  signal M0    : signed(31 downto 0) := (others => '0');

  -- shift
  signal shift      : signed( 7 downto 0) := (others => '0');

  
  --Outputs -----------------------------
  -- Sortie du noyau
  signal tf_out : signed(31 downto 0) := (others => '0');
    
  -- conv_tflite done
  signal dn_tf  : std_logic := '0';


   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
   constant clock_period : time := 8 ns;
 
 
begin
 
 
  -- Instantiate the Unit Under Test (UUT)
  uut: tflite_mbqm
    port map(
      clk   => clk,
      rstn  => rstn,
      start => start,
      
      cv_in => cv_in,
      bias  => bias,
      
      M0         => M0,
      shift      => shift,
      
      tf_out => tf_out,
      dn_tf  => dn_tf );


  -- Clock process definitions
  clock_process :process
    begin
	  clk <= '0';
      wait for clock_period/2;
      clk <= '1';
      wait for clock_period/2;
  end process;
 

  -- Stimulus process
  stim_proc: process
    begin		
      -- hold reset state for 8 ns x 2.
      wait for clock_period*2;

      -- insert stimulus here
      rstn <= '1'; start <= '0';
      wait for clock_period*2;
      
      
      rstn <= '0'; start <= '0';
      wait for clock_period*2;


      rstn <= '1'; start <= '1';
      wait for clock_period*2;
      
      ------------------------------ 
      -- Filter 1, Entrée [0:6][0:6]
      ------------------------------
      -- entrée 
      cv_in <= x"FFFE9248";                                -- -93624
      bias  <= x"FFFFFFEF";                                --  -17
      
      -- paramètres
      M0    <= x"48A8757A";                                --  1218999674
      shift      <= x"FB";                                 --   -5
       
      wait for clock_period*5;

      ------------------------------ 
      -- Filter 2, Entrée [9:14][52:57]
      ------------------------------
      -- entrée 
      cv_in <= x"FFFE9248";                                -- -93624
      bias       <= x"0000004B";                           --   75
      
      -- paramètres
      M0         <= x"5C5D83CE";                           --  1549632462
      shift      <= x"FB";                                 --   -5
 
      wait for clock_period*5;


      wait;
   end process;

END;
