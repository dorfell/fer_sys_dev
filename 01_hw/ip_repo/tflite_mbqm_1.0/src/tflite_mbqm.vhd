---------------------------------------------------------------------
----                                                             ----
---- file:    conv_tflite.vhd                                    ----
---- brief:   Module de Convolution                              ----
---- details: Module pour implementer las fonctions du tflite    ----
----          comme SaturatingRoundingHighMul,                   ----
----          RoundingDivideByPot et                             ----
----          MultiplyByQuantizedMultiplier.                     ----
----          Les entrées sont xowc, bias, M0.                   ----
----          Le résultat est un nombre entier.                  ----
---- author:  Dorfell Parra - dlparrap@unal.edu.co               ----
---- date:    2021/06/20                                         ----
---- version: 0.1                                                ----
---------------------------------------------------------------------
----                                                             ----
---- Copyright (C) 2021 Dorfell Parra                            ----
----                    dlparrap@unal.edu.co                     ----
----                                                             ----
---- This source file may be used and distributed without        ----
---- restriction provided that this copyright statement is not   ----
---- removed from the file and that any derivative work contains ----
---- the original copyright notice and the associated disclaimer.----
----                                                             ----
----     THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY     ----
---- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED   ----
---- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS   ----
---- FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR      ----
---- OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         ----
---- INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    ----
---- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE   ----
---- GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR        ----
---- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  ----
---- LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT  ----
---- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT  ----
---- OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE         ----
---- POSSIBILITY OF SUCH DAMAGE.                                 ----
----                                                             ----
---------------------------------------------------------------------


-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity tflite_mbqm is
  port( 
    clk, rstn, start :  in std_logic;
  
    -- cv_in somme(x+offset_ent)*w + cv_in[0,i,j,f]
    cv_in  :  in signed(31 downto 0);

    -- bias 
    bias   :  in signed(31 downto 0);

    -- M0
    M0     :  in signed(31 downto 0);

    -- shift
    shift  :  in signed( 7 downto 0);

    -- sortie du noyau
    tf_out : out signed(31 downto 0);
    
     -- conv_tflite done
     dn_tf : out std_logic ); 
end tflite_mbqm;


architecture Behavioral of tflite_mbqm is


  -- Component Declaration for the instances
  ------------------------------------------------------
  
  -- tflite_core0
  component tflite_core0 is
    port( 
      --clk      :  in std_logic;

      -- cv_in somme(x+offset_ent)*w + cv_in[0,i,j,f]
      cv_in  :  in signed(31 downto 0);

      -- bias 
      bias   :  in signed(31 downto 0);
         
      -- quantized_multiplier (M0)
      quantized_multiplier :  in signed(31 downto 0);

      -- shift
      --shift    :  in signed( 7 downto 0);

      -- MultiplyByQuantizedMultiplier (MBQM) sortie
      -- x * (1 << left_shift)
      overflow : out std_logic;
      xls      : out signed(31 downto 0) );
  end component tflite_core0;
  
  
  -- tflite_core1
  component tflite_core1 is
    port( 
      clk      :  in std_logic;

      -- x * (1 << left_shift) 
      xls      :  in signed(31 downto 0);
        
      -- quantized_multiplier (M0)
      quantized_multiplier :  in signed(31 downto 0);
    
      -- SaturatingRoundingDoublingHighMul (SRDHM)
      -- ab_64 = (int32_t)(a) * (int32_t)(b)
      ab_64    : out signed(63 downto 0);
      nudge    : out signed(31 downto 0) );
  end component tflite_core1;

  
  -- tflite_core2
  component tflite_core2 is
    port( 
      clk      :  in std_logic;

      -- ab_64 = (int32_t)(a) * (int32_t)(b)
      ab_64    :  in signed(63 downto 0);
      nudge    :  in signed(31 downto 0);
    
      -- SaturatingRoundingDoublingHighMul (SRDHM)
      -- (ab_64 + nudge)
      ab_nudge : out signed(63 downto 0) );
  end component tflite_core2;

  
  -- tflite_core3
  component tflite_core3 is
    port( 
      -- SaturatingRoundingDoublingHighMul (SRDHM) 
      overflow :  in std_logic;
      
      -- (ab_64 + nudge)
      ab_nudge :  in signed(63 downto 0);

      -- SaturatingRoundingDoublingHighMul (SRDHM) sortie
      -- srdhm <= 2147483647 when overflow = '1' else sg_ab_x2_high32;
      srdhm    : out signed(31 downto 0) );
  end component tflite_core3;
  

  -- tflite_core4
  component tflite_core4 is
    port( 
      -- SaturatingRoundingDoublingHighMul (SRDHM) sortie
      srdhm :  in signed(31 downto 0);
    
      -- shift 
      shift :  in signed(7 downto 0);
    
      -- MultiplyByQuantizedMultiplier (MBQM) sortie
      mbqm  : out signed(31 downto 0) );
  end component tflite_core4;


  -- tflite_core5
  component tflite_core5 is
    port( 
      -- MultiplyByQuantizedMultiplier
      mbqm       :  in signed(31 downto 0); 

      -- Offset sortie
      offset_sor :  in signed(15 downto 0); 
      
      -- Sortie du noyau
      tf_out     : out signed(31 downto 0) );
  end component tflite_core5;

  -- tflite_core0
  signal sg_xls      : signed(31 downto 0);
  signal sg_overflow : std_logic;
  
  -- tflite_core1
  signal sg_ab_64    : signed(63 downto 0);
  signal sg_nudge    : signed(31 downto 0);
  
  -- tflite_core2
  signal sg_ab_nudge : signed(63 downto 0);

  -- tflite_core3
  signal sg_srdhm    : signed(31 downto 0);

  
   -- Compteur de cycles: registre, enable,done. 
  signal dd, qq   : std_logic_vector(6 downto 0) := (others=>'0');

  
  
begin




  ---------------------
  -- Compteur de cycles (cc)
  ---------------------
  -- Overflow pour clock cycles
  mux_ovf: 
    dd <= qq when (qq = x"48") else qq + 1;

  -- Registre du compteur
  process (clk, rstn)
    begin  
      if (rstn = '0') then
        qq <= (others => '0');
      elsif (clk'event and clk = '1') then
        if (start = '1') then
          qq <= dd;
        else
          qq <= qq;
        end if;
      end if;
  end process;

  -- Done aprés cycles
  dn_tf <= '1' when (qq = x"48") else '0'; 




  --! Instantiate the submodules (SM)
  -----------------------------------------
  
  SM0: tflite_core0
    port map(
      --clk      => clk,
      
      cv_in    => cv_in,
      bias     => bias,
      
      quantized_multiplier => M0,
      --shift    => shift,
      
      overflow => sg_overflow,
      xls      => sg_xls );
       
  
  SM1: tflite_core1
    port map(
      clk      => clk,
      
      xls      => sg_xls,
      quantized_multiplier => M0,
      
      ab_64    => sg_ab_64,
      nudge    => sg_nudge );

  
  SM2: tflite_core2
    port map(
      clk      => clk,
      
      ab_64    => sg_ab_64,
      nudge    => sg_nudge,
      
      ab_nudge => sg_ab_nudge );
 
  
  SM3: tflite_core3
    port map(
      overflow => sg_overflow,
      ab_nudge => sg_ab_nudge,
    
      srdhm    => sg_srdhm );
  
  
  SM4: tflite_core4
    port map(
      srdhm => sg_srdhm,
      shift => shift,
    
      mbqm  => tf_out );
        
end Behavioral;