-------------------------------------------------------------------

     =========================================================
     fer_acc_dev/01_hw/ - Développement hardware d'un 
                          accelerateur pour applications FER
     =========================================================

                         Liste des fichiers
                         ---------------

Ce dépôt a des fichiers  qui sont utilisées pour le développement hardware 
d'un accelerateur FER.


fer_soc_LiTeX/
 -Projet en LiTeX d'une SoC avec le Vexriscv qui permet d'ajouter les noyaux
  rtl_pwm et conv. Toutefois, LiTeX n'est pas répétable (j'ai teste plusiers 
  commits du 2021 et les releases en https://github.com/enjoy-digital/litex/releases, 
  même l'unique qui a "fonctionné" c'est la 2021.04 mais seulement avec lab004, 
  quand j'ajoute une source HDL il aparaître des rares compilations de la BIOS
  ce qui affect le BOOT.
  Je aussi reencontré quelques différences entre fichiers mem.init et mem_init3.init
  compiles avec les mêmes fichiers (sans changer même si une virgule). En plus, 
  il y a des issues à la date (2021/08) en https://github.com/enjoy-digital/litex/issues
  qui report ce type de problèmes avec LiTeX. E.g.:
  
  -- **demo hangs on boot if it contains more than 16 bytes of .data #856**
  -- https://github.com/enjoy-digital/litex/issues/856

  -- **litex/vexriscv boot problem #876**
  -- https://github.com/enjoy-digital/litex/issues/876

  -- **VexRiscv Minimal variant won't boot #937** 
  -- https://github.com/enjoy-digital/litex/issues/937

  -- **Replace libbase with picolibc #976** 
  -- https://github.com/enjoy-digital/litex/pull/976

  -- **First bytes printed after CPU reset are incorrect #991** 
  -- https://github.com/enjoy-digital/litex/issues/991

  -- **Fix alignments in demo/linker.ld #1007**
  -- https://github.com/enjoy-digital/litex/pull/1007


fer_soc/
 - Projet en Vivado d'une SoC qui permettre de faire l'inference d'une application FER 
   (Facial Expression Recognition).


ip_repo/
 - Dépôt avec les noyaux necessaires pour l'inference du modle FER.

microSD/
 - MicroSD fichiers pour faire l'inference avec modèle M6 dans le FER_SOC.


Pynq_Zybo-Z7/
 - Zybo-Z7/
   Files needed to create a Pynq image for Zybo-Z7. The board support package takes into
   account the DDR part number MT41K256M16 RE-125, the PS UART 1 and the Crystal 
   oscillator of 33.3333 MHz. In addition, the base overlay has been reduced eliminating
   the Arduino and Raspberry GPIOs, and the constraints file (base.xdc)  was updated.
   The Makefile_sdbuild corresponds to the Makefile located in ~/PYNQ/sdbuild but is
   limited to compile only for the Zybo-Z7.
   The ready to use image can be found in: https://drive.google.com/file/d/1kCT_UZjIuDym3yhI739xY1OY1Q4byCHx/view?usp=sharing
   Youtube video demonstrating the behaviour in: https://youtu.be/a1FPf9TzUzs
 - tensorflow_test/
   Files needed to test Tensorflow2.5 in Pynq 2.5 are also included:
   the Notebook, the wheel by Katsuya Hyodo and the demonstration video. The former also 
   found in: https://www.youtube.com/watch?v=3m7kavySEYE.
 - pytorch_test/
   Wheels for torch1.8 and torchvision0.9 compiled by Chiao-Wei Wang: 
   https://github.com/CW-B-W/PyTorch-and-Vision-for-Raspberry-Pi-4B
   As well as the notebooks from: https://www.youtube.com/watch?v=IC0_FRiX-sw
   Demostration video in: https://www.youtube.com/watch?v=GYP6C3Sat6g
 - FER_M6_pytorch/
   Notebook needed to train de M6 FER model in  the Zybo-Z7. Other files found are the  
   Jaffe transfigured dataset, a *.pdf with the training made in a reference laptop 
   and the video demonstrating the behaviour.   
   

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
