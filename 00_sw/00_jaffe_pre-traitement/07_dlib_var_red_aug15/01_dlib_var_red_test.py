# -*- coding: utf-8 -*-
## 
# @file    01_dlib_var_red_test.py
# @brief   Pré-traitement du Jaffe avec Dlib + lbp-var + redimensionnement
# @details Ce module sauvegardé l'ensemble Jaffe pré-traité avec Dlib,
#          le motif locaux binaire avec le méthode VAR, 
#          le redimensionnement à tailles de 24x24, 32x32,
#          48x48, 56x56, 64x64, 72x72, 96x96 et  128x128,  
#          avec l'interpolation Lanczos4.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/02/28
# @version 0.1
"""@package docstring
"""

import albumentations as A
import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from skimage.feature import local_binary_pattern as lbp_fun
from mes_fonctions import *

from matplotlib.backends.backend_pdf import PdfPages


print("************************************** ");
print(" Charger l'ensamble de données         ");
print("************************************** ");
# Localisation de l'ensamble de données (db)
#loc_db = "../../db/jaffedbase_petit/";                    # Seulment 4 images. 
loc_db = "../../db/jaffedbase/";                           # 213 images. 

# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];

# Lire la base de données
img_dat, img_lab = read_data(loc_db);
img_dat = img_dat[183:213]; img_lab = img_lab[183:213];    # Ensemble d'entraînement
#print(img_dat, img_dat.shape,  img_lab);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Pré-traitement des données avec Dlib  ");
print("************************************** ");
# Pré-traitement des données
img_dat_pre = preprocessing(img_dat);                      # taille (128, 128)
#print("print img_dat_resized", img_dat_resized, len(img_dat_resized), type(img_dat_resized),  img_lab);
print("Image après pré-traitement: \n", img_dat_pre[0], img_dat_pre[0].dtype); 
#plt.imshow(img_dat_pre[0], cmap="gray"); 
#plt.title("Image pré-traité avec Dlib");  plt.show();
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Motifs Binaires Locaux (LBP)          ");
print("************************************** ");
# Paramètres des Motifs binaires locaux (LBP) 
radius = 3;                                                # Rayon de cercle
n_points = 8 * radius;                                     # nombre de points voisins circulairement symétriques.
method= "var";                                             # Méthode pour la determination du motifs. E.g. default, ror, uniform, nri_uniform, var.

lbp_var_lt = [];
for img in img_dat_pre:
  lbp_var = lbp_fun(img, n_points, radius, method);        # Calculer le LBP.
  lbp_var_lt.append(lbp_var);

#plt.imshow(lbp_var, cmap="gray");                         # Imprimer quelques images
#plt.title("Image avec lbp, méthode= var"); plt.show();
#print("Image après lbp var: \n", lbp_var); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" À nombre entier int8                  ");
print("************************************** ");
var_int_lt = [];
for img in lbp_var_lt:
  img = img / img.max();                                   # normalizer img [0, 1].
  img = 255 * img;                                         # À échelle 0 - 255
  img = np.rint(img);                                      # Arrondir à entier
  var_int_lt.append( img.astype(np.uint8) );               # cast à uint8
print("valeurs du lbp_var_int[0]:\n ", var_int_lt[0]); print(var_int_lt[0].shape);
#print("valeurs du lbp_var_int:\n ", var_int_lt); 
print("longitude de liste: ", len(var_int_lt));

#plt.imshow(var_int_lt[0], cmap="gray");                   # Imprimer quelques images
#plt.title("Image avec lbp var en entier 0 à 255");  plt.show();


print("************************************** ");
print(" Redimensionnement de l'image          ");
print("************************************** ");
red24_lt = []; red32_lt = []; red48_lt = []; red56_lt = [];
red64_lt = []; red72_lt = []; red96_lt = []; red128_lt = [];
for img in var_int_lt:
  # 24 x 24 pixels
  red24 = cv2.resize(img, dsize=(24,24), interpolation=cv2.INTER_LANCZOS4);
  red24_lt.append(red24);

  # 32 x 32 pixels
  red32 = cv2.resize(img, dsize=(32,32), interpolation=cv2.INTER_LANCZOS4);
  red32_lt.append(red32);

  # 48 x 48 pixels
  red48 = cv2.resize(img, dsize=(48,48), interpolation=cv2.INTER_LANCZOS4);
  red48_lt.append(red48);

  # 56 x 56 pixels
  red56 = cv2.resize(img, dsize=(56,56), interpolation=cv2.INTER_LANCZOS4);
  red56_lt.append(red56);

  # 64 x 64 pixels
  red64 = cv2.resize(img, dsize=(64,64), interpolation=cv2.INTER_LANCZOS4);
  red64_lt.append(red64);

  # 72 x 72 pixels
  red72 = cv2.resize(img, dsize=(72,72), interpolation=cv2.INTER_LANCZOS4);
  red72_lt.append(red72);

  # 96 x 96 pixels
  red96 = cv2.resize(img, dsize=(96,96), interpolation=cv2.INTER_LANCZOS4);
  red96_lt.append(red96);

  # 128 x 128 pixels
  red128_lt.append(img);

#plt.imshow(red24_lt[0], cmap="gray");                     # Imprimer quelques images
#plt.title("Interpolation Lanczos4 24 x 24 pixels"); plt.show();
#print("Image après le redimensionnement: \n", red_lt[0]); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Sauvegarder les données pour tester   ");
print("************************************** ");
# Sauvegarder images et étiquettes avec dlib + lbp-var + red24
np.save("../../db_transfigure/jaffe_dlib_var24_test_img.npy", red24_lt);
np.save("../../db_transfigure/jaffe_dlib_var24_test_lab.npy", img_lab);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red32 
np.save("../../db_transfigure/jaffe_dlib_var32_test_img.npy", red32_lt);
np.save("../../db_transfigure/jaffe_dlib_var32_test_lab.npy", img_lab);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red48 
np.save("../../db_transfigure/jaffe_dlib_var48_test_img.npy", red48_lt);
np.save("../../db_transfigure/jaffe_dlib_var48_test_lab.npy", img_lab);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red56 
np.save("../../db_transfigure/jaffe_dlib_var56_test_img.npy", red56_lt);
np.save("../../db_transfigure/jaffe_dlib_var56_test_lab.npy", img_lab);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red64
np.save("../../db_transfigure/jaffe_dlib_var64_test_img.npy", red64_lt);
np.save("../../db_transfigure/jaffe_dlib_var64_test_lab.npy", img_lab);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red72
np.save("../../db_transfigure/jaffe_dlib_var72_test_img.npy", red72_lt);
np.save("../../db_transfigure/jaffe_dlib_var72_test_lab.npy", img_lab);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red96 
np.save("../../db_transfigure/jaffe_dlib_var96_test_img.npy", red96_lt);
np.save("../../db_transfigure/jaffe_dlib_var96_test_lab.npy", img_lab);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red128
np.save("../../db_transfigure/jaffe_dlib_var128_test_img.npy", red128_lt);
np.save("../../db_transfigure/jaffe_dlib_var128_test_lab.npy", img_lab);


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
