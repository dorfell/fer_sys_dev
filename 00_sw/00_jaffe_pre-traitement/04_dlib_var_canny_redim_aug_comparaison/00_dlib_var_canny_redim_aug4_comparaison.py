# -*- coding: utf-8 -*-
## 
# @file    00_dlib_var_canny_redim_aug4_comparaison.py
# @brief   Comparaison de l'augmentation des données
# @details Ce module fait la comparaison des 4 différentés techniques pour 
#          l'augmentation des données avec les images qui vient du 
#          pré-traitement: 
#          dlib + lbp-var + canny1986 (50, 100) + redimensionnement 56 x 56 pixels.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/02/10
# @version 0.1
"""@package docstring
"""

import albumentations as A
import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from matplotlib.backends.backend_pdf import PdfPages
from tensorflow import keras


print("************************************** ");
print(" Charger les transfigurations          ");
print("************************************** ");
 
# Charger image + pré-traitement avec dlib
dlib_img = np.load("../../db_transfigure/jaffe_dlib128_img.npy");
dlib_lab = np.load("../../db_transfigure/jaffe_dlib128_lab.npy");
 
# Charger image + pré-traitement  avec dlib + lbp-var
var_img = np.load("../../db_transfigure/jaffe_dlib_var128_img.npy");
var_lab = np.load("../../db_transfigure/jaffe_dlib_var128_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986
can_img = np.load("../../db_transfigure/jaffe_dlib_var_can128_img.npy");
can_lab = np.load("../../db_transfigure/jaffe_dlib_var_can128_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986 + redimensionnent
red_img = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_img.npy");
red_lab = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_lab.npy");


print("************************************** ");
print(" Définir les augmentations             ");
print("************************************** ");
# Définir les pipelines d'augmentation
trf_flp = A.HorizontalFlip(p=0.99);                        # Horizontal flip

trf_rot = A.Rotate(                                        # Rotation
            limit=20, interpolation=4, border_mode=0,      # --> Lanczos4, border=0
            value=0, p=0.99);                              

trf_fr  = A.Compose([                                      # Flip et rotation   
            A.HorizontalFlip(p=0.99),                        
            A.Rotate( 
              limit=20, interpolation=4, border_mode=0,    # --> Lanczos4, border=0
              value=0, p=0.99) ]);                          

trf_ssr = A.ShiftScaleRotate(                              # Shift, scale et rotation
            shift_limit=0.1, scale_limit=0.2, 
            rotate_limit=10, interpolation=4,              # --> Lanczos4
            border_mode=0, value=0, p=0.99);               # --> border=0


print("************************************** ");
print(" Augmentation de données avec dlib     ");
print("************************************** ");
dlib_flp = trf_flp(image=dlib_img[0]);                     # Horizontal flip 
dlib_rot = trf_rot(image=dlib_img[0]);                     # Rotation
dlib_fr  = trf_fr( image=dlib_img[0]);                     # Flip et rotation
dlib_ssr = trf_ssr(image=dlib_img[0]);                     # Shift,scale et rotation

print("Image après flip:                    \n", dlib_flp["image"]); 
print("Image après rotation:                \n", dlib_rot["image"]); 
print("Image après flip et rotation:        \n", dlib_fr["image"]); 
print("Image après shift scale et rotation: \n", dlib_ssr["image"]); 


print("************************************** ");
print(" Augmentation de données avec lbp-var  ");
print("************************************** ");
var_flp = trf_flp(image=var_img[0]);                       # Horizontal flip 
var_rot = trf_rot(image=var_img[0]);                       # Rotation
var_fr  = trf_fr( image=var_img[0]);                       # Flip et rotation
var_ssr = trf_ssr(image=var_img[0]);                       # Shift,scale et rotation

print("Image après flip:                    \n", var_flp["image"]); 
print("Image après rotation:                \n", var_rot["image"]); 
print("Image après flip et rotation:        \n", var_fr["image"]); 
print("Image après shift scale et rotation: \n", var_ssr["image"]); 


print("************************************** ");
print(" Augmentation de données avec Canny86  ");
print("************************************** ");
can_flp = trf_flp(image=can_img[0]);                       # Horizontal flip 
can_rot = trf_rot(image=can_img[0]);                       # Rotation
can_fr  = trf_fr( image=can_img[0]);                       # Flip et rotation
can_ssr = trf_ssr(image=can_img[0]);                       # Shift,scale et rotation

print("Image après flip:                    \n", can_flp["image"]); 
print("Image après rotation:                \n", can_rot["image"]); 
print("Image après flip et rotation:        \n", can_fr["image"]); 
print("Image après shift scale et rotation: \n", can_ssr["image"]); 


print("************************************** ");
print(" Augmentation de données avec redimens ");
print("************************************** ");
red_flp = trf_flp(image=red_img[0]);                       # Horizontal flip 
red_rot = trf_rot(image=red_img[0]);                       # Rotation
red_fr  = trf_fr( image=red_img[0]);                       # Flip et rotation
red_ssr = trf_ssr(image=red_img[0]);                       # Shift,scale et rotation

print("Image après flip:                    \n", red_flp["image"]); 
print("Image après rotation:                \n", red_rot["image"]); 
print("Image après flip et rotation:        \n", red_fr["image"]); 
print("Image après shift scale et rotation: \n", red_ssr["image"]); 


print("************************************** ");
print(" Générer le pdf avec la comparaison    ");
print("************************************** ");
# Fonction qui sert à faire la comparaison des augmentations
with PdfPages("comparaison_4_augmentations.pdf")  as pdf:

  # Augmentation avec dlib
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Reference");                ax1.imshow(dlib_img[0],       cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Flip");                     ax2.imshow(dlib_flp["image"], cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Rotation");                 ax3.imshow(dlib_rot["image"], cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Flip et Rotation");         ax4.imshow(dlib_fr["image"],  cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Shift, scale et Rotation"); ax5.imshow(dlib_ssr["image"], cmap="gray"); 
  fig.suptitle("Augmentation avec Dlib ", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Reference");                ax1.imshow(var_img[0],       cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Flip");                     ax2.imshow(var_flp["image"], cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Rotation");                 ax3.imshow(var_rot["image"], cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Flip et Rotation");         ax4.imshow(var_fr["image"],  cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Shift, scale et Rotation"); ax5.imshow(var_ssr["image"], cmap="gray"); 
  fig.suptitle("Augmentation avec Dlib + lbp-var", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var + canny1986
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Reference");                ax1.imshow(can_img[0],       cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Flip");                     ax2.imshow(can_flp["image"], cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Rotation");                 ax3.imshow(can_rot["image"], cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Flip et Rotation");         ax4.imshow(can_fr["image"],  cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Shift, scale et Rotation"); ax5.imshow(can_ssr["image"], cmap="gray"); 
  fig.suptitle("Augmentation avec Dlib + lbp-var + Canny1986", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var + canny1986 + redimensionnement
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Reference");                ax1.imshow(red_img[0],       cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Flip");                     ax2.imshow(red_flp["image"], cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Rotation");                 ax3.imshow(red_rot["image"], cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Flip et Rotation");         ax4.imshow(red_fr["image"],  cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Shift, scale et Rotation"); ax5.imshow(red_ssr["image"], cmap="gray"); 
  fig.suptitle("Augmentation avec Dlib + lbp-var + Canny1986 + redimensionnement", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

#sys.exit(0);                                              # Terminer l'execution


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
