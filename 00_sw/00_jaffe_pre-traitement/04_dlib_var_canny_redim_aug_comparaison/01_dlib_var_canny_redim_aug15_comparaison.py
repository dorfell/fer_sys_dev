# -*- coding: utf-8 -*-
## 
# @file    00_dlib_var_canny_redim_aug15_comparaison.py
# @brief   Comparaison de l'augmentation des données
# @details Ce module fait la comparaison des 15 différentés techniques pour 
#          l'augmentation des données avec les images qui vient du 
#          pré-traitement: 
#          dlib + lbp-var + canny1986 (50, 100) + redimensionnement 56 x 56 pixels.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/02/10
# @version 0.1
"""@package docstring
"""

import albumentations as A
import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from matplotlib.backends.backend_pdf import PdfPages
from tensorflow import keras


print("************************************** ");
print(" Charger les transfigurations          ");
print("************************************** ");
 
# Charger image + pré-traitement avec dlib
dlib_img = np.load("../../db_transfigure/jaffe_dlib128_img.npy");
dlib_lab = np.load("../../db_transfigure/jaffe_dlib128_lab.npy");
 
# Charger image + pré-traitement  avec dlib + lbp-var
var_img = np.load("../../db_transfigure/jaffe_dlib_var128_img.npy");
var_lab = np.load("../../db_transfigure/jaffe_dlib_var128_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986
can_img = np.load("../../db_transfigure/jaffe_dlib_var_can128_img.npy");
can_lab = np.load("../../db_transfigure/jaffe_dlib_var_can128_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986 + redimensionnent
red_img = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_img.npy");
red_lab = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_lab.npy");


print("************************************** ");
print(" Définir les augmentations             ");
print("************************************** ");
# Définir les pipelines d'augmentation
trf_sh  = A.ShiftScaleRotate(                              # --> Shift
            shift_limit=0.2, scale_limit=0, 
            rotate_limit=0, interpolation=4,               # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0

trf_sc  = A.ShiftScaleRotate(                              # --> Scale
            shift_limit=0, scale_limit=0.2, 
            rotate_limit=0, interpolation=4,               # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0

trf_ro  = A.ShiftScaleRotate(                              # --> Rotation
            shift_limit=0, scale_limit=0, 
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0

trf_ss  = A.ShiftScaleRotate(                              # --> Shift, scale
            shift_limit=0.2, scale_limit=0.2, 
            rotate_limit=0, interpolation=4,               # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0

trf_sr  = A.ShiftScaleRotate(                              # --> Shift, rotation
            shift_limit=0.2, scale_limit=0, 
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0

trf_cr  = A.ShiftScaleRotate(                              # --> Scale, rotation
            shift_limit=0, scale_limit=0.2, 
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0

trf_ssr  = A.ShiftScaleRotate(                             # --> Shift,scale,rotation
            shift_limit=0.2, scale_limit=0.2, 
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0

# même augmentation pour l'image retourné/flip
trf_flp = A.HorizontalFlip(p=0.99);                        # --> Horizontal flip

trf_fsh = A.Compose([                                      # --> Flip, shift   
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift
              shift_limit=0.2, scale_limit=0, 
              rotate_limit=0, interpolation=4,             # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0

trf_fsc = A.Compose([                                      # --> Flip, scale   
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Scale
              shift_limit=0, scale_limit=0.2, 
              rotate_limit=0, interpolation=4,             # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0

trf_fro = A.Compose([                                      # --> Flip, rotation   
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Rotation
              shift_limit=0, scale_limit=0, 
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0

trf_fss = A.Compose([                                      # --> Flip, shift, scale  
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift, scale
              shift_limit=0.2, scale_limit=0.2, 
              rotate_limit=0, interpolation=4,             # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0

trf_fsr = A.Compose([                                      # --> Flip,shift,rotation 
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift, rotation
              shift_limit=0.2, scale_limit=0, 
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0

trf_fcr = A.Compose([                                      # --> Flip,scale,rotation 
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Scale, rotation
              shift_limit=0, scale_limit=0.2, 
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0

trf_fssr = A.Compose([                                     # --> Flip,scale,shift,rot
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift,scale,rotation
              shift_limit=0.2, scale_limit=0.2, 
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0

#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation de données avec dlib     ");
print("************************************** ");
dlib_sh  = trf_sh(image=dlib_img[0]);                      # Shift 
dlib_sc  = trf_sc(image=dlib_img[0]);                      # Scale 
dlib_ro  = trf_ro(image=dlib_img[0]);                      # Rotation
dlib_ss  = trf_ss(image=dlib_img[0]);                      # Shift, scale 
dlib_sr  = trf_sr(image=dlib_img[0]);                      # Shift, rotation 
dlib_cr  = trf_cr(image=dlib_img[0]);                      # Scale, rotation
dlib_ssr = trf_ssr(image=dlib_img[0]);                     # Shift, scale, rotation
dlib_flp = trf_flp(image=dlib_img[0]);                     # Flip
dlib_fsh = trf_fsh(image=dlib_img[0]);                     # Flip shift 
dlib_fsc = trf_fsc(image=dlib_img[0]);                     # Flip scale 
dlib_fro = trf_fro(image=dlib_img[0]);                     # Flip rotation
dlib_fss = trf_fss(image=dlib_img[0]);                     # Flip shift, scale 
dlib_fsr = trf_fsr(image=dlib_img[0]);                     # Flip shift, rotation 
dlib_fcr = trf_fcr(image=dlib_img[0]);                     # Flip scale, rotation
dlib_fssr= trf_fssr(image=dlib_img[0]);                    # Flip shift, scale, rotation

print("Image après shift:                  \n", dlib_sh["image"] ); 
print("Image après scale:                  \n", dlib_sc["image"] ); 
print("Image après rotation:               \n", dlib_ro["image"] ); 
print("Image après shift, scale:           \n", dlib_ss["image"] ); 
print("Image après shift, rotation:        \n", dlib_sr["image"] ); 
print("Image après scale, rotation:        \n", dlib_cr["image"] ); 
print("Image après shift, scale, rotation: \n", dlib_ssr["image"] ); 
print("Image après flip:                       \n", dlib_flp["image"] ); 
print("Image après flip shift:                 \n", dlib_fsh["image"] ); 
print("Image après flip scale:                 \n", dlib_fsc["image"] ); 
print("Image après flip rotation:              \n", dlib_fro["image"] ); 
print("Image après flip shift, scale:          \n", dlib_fss["image"] ); 
print("Image après flip shift, rotation:       \n", dlib_fsr["image"] ); 
print("Image après flip scale, rotation:       \n", dlib_fcr["image"] ); 
print("Image après flip shift, scale, rotation:\n", dlib_fssr["image"] ); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation de données avec lbp-var  ");
print("************************************** ");
var_sh  = trf_sh(image=var_img[0]);                        # Shift 
var_sc  = trf_sc(image=var_img[0]);                        # Scale 
var_ro  = trf_ro(image=var_img[0]);                        # Rotation
var_ss  = trf_ss(image=var_img[0]);                        # Shift, scale 
var_sr  = trf_sr(image=var_img[0]);                        # Shift, rotation 
var_cr  = trf_cr(image=var_img[0]);                        # Scale, rotation
var_ssr = trf_ssr(image=var_img[0]);                       # Shift, scale, rotation
var_flp = trf_flp(image=var_img[0]);                       # Flip
var_fsh = trf_fsh(image=var_img[0]);                       # Flip shift 
var_fsc = trf_fsc(image=var_img[0]);                       # Flip scale 
var_fro = trf_fro(image=var_img[0]);                       # Flip rotation
var_fss = trf_fss(image=var_img[0]);                       # Flip shift, scale 
var_fsr = trf_fsr(image=var_img[0]);                       # Flip shift, rotation 
var_fcr = trf_fcr(image=var_img[0]);                       # Flip scale, rotation
var_fssr= trf_fssr(image=var_img[0]);                      # Flip shift, scale, rotation

print("Image après shift:                  \n", var_sh["image"] ); 
print("Image après scale:                  \n", var_sc["image"] ); 
print("Image après rotation:               \n", var_ro["image"] ); 
print("Image après shift, scale:           \n", var_ss["image"] ); 
print("Image après shift, rotation:        \n", var_sr["image"] ); 
print("Image après scale, rotation:        \n", var_cr["image"] ); 
print("Image après shift, scale, rotation: \n", var_ssr["image"] ); 
print("Image après flip:                       \n", var_flp["image"] ); 
print("Image après flip shift:                 \n", var_fsh["image"] ); 
print("Image après flip scale:                 \n", var_fsc["image"] ); 
print("Image après flip rotation:              \n", var_fro["image"] ); 
print("Image après flip shift, scale:          \n", var_fss["image"] ); 
print("Image après flip shift, rotation:       \n", var_fsr["image"] ); 
print("Image après flip scale, rotation:       \n", var_fcr["image"] ); 
print("Image après flip shift, scale, rotation:\n", var_fssr["image"] ); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation de données avec Canny86  ");
print("************************************** ");
can_sh  = trf_sh(image=can_img[0]);                        # Shift 
can_sc  = trf_sc(image=can_img[0]);                        # Scale 
can_ro  = trf_ro(image=can_img[0]);                        # Rotation
can_ss  = trf_ss(image=can_img[0]);                        # Shift, scale 
can_sr  = trf_sr(image=can_img[0]);                        # Shift, rotation 
can_cr  = trf_cr(image=can_img[0]);                        # Scale, rotation
can_ssr = trf_ssr(image=can_img[0]);                       # Shift, scale, rotation
can_flp = trf_flp(image=can_img[0]);                       # Flip
can_fsh = trf_fsh(image=can_img[0]);                       # Flip shift 
can_fsc = trf_fsc(image=can_img[0]);                       # Flip scale 
can_fro = trf_fro(image=can_img[0]);                       # Flip rotation
can_fss = trf_fss(image=can_img[0]);                       # Flip shift, scale 
can_fsr = trf_fsr(image=can_img[0]);                       # Flip shift, rotation 
can_fcr = trf_fcr(image=can_img[0]);                       # Flip scale, rotation
can_fssr= trf_fssr(image=can_img[0]);                      # Flip shift, scale, rotation

print("Image après shift:                  \n", can_sh["image"] ); 
print("Image après scale:                  \n", can_sc["image"] ); 
print("Image après rotation:               \n", can_ro["image"] ); 
print("Image après shift, scale:           \n", can_ss["image"] ); 
print("Image après shift, rotation:        \n", can_sr["image"] ); 
print("Image après shift, rotation:        \n", can_sr["image"] ); 
print("Image après scale, rotation:        \n", can_cr["image"] ); 
print("Image après flip:                       \n", can_flp["image"] ); 
print("Image après flip shift:                 \n", can_fsh["image"] ); 
print("Image après flip scale:                 \n", can_fsc["image"] ); 
print("Image après flip rotation:              \n", can_fro["image"] ); 
print("Image après flip shift, scale:          \n", can_fss["image"] ); 
print("Image après flip shift, rotation:       \n", can_fsr["image"] ); 
print("Image après flip scale, rotation:       \n", can_fcr["image"] ); 
print("Image après flip shift, scale, rotation:\n", can_fssr["image"] ); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation de données avec redimens ");
print("************************************** ");
red_sh  = trf_sh(image=red_img[0]);                        # Shift 
red_sc  = trf_sc(image=red_img[0]);                        # Scale 
red_ro  = trf_ro(image=red_img[0]);                        # Rotation
red_ss  = trf_ss(image=red_img[0]);                        # Shift, scale 
red_sr  = trf_sr(image=red_img[0]);                        # Shift, rotation 
red_cr  = trf_cr(image=red_img[0]);                        # Scale, rotation
red_ssr = trf_ssr(image=red_img[0]);                       # Shift, scale, rotation
red_flp = trf_flp(image=red_img[0]);                       # Flip
red_fsh = trf_fsh(image=red_img[0]);                       # Flip shift 
red_fsc = trf_fsc(image=red_img[0]);                       # Flip scale 
red_fro = trf_fro(image=red_img[0]);                       # Flip rotation
red_fss = trf_fss(image=red_img[0]);                       # Flip shift, scale 
red_fsr = trf_fsr(image=red_img[0]);                       # Flip shift, rotation 
red_fcr = trf_fcr(image=red_img[0]);                       # Flip scale, rotation
red_fssr= trf_fssr(image=red_img[0]);                      # Flip shift, scale, rotation

print("Image après shift:                  \n", red_sh["image"] ); 
print("Image après scale:                  \n", red_sc["image"] ); 
print("Image après rotation:               \n", red_ro["image"] ); 
print("Image après shift, scale:           \n", red_ss["image"] ); 
print("Image après shift, rotation:        \n", red_sr["image"] ); 
print("Image après scale, rotation:        \n", red_cr["image"] ); 
print("Image après shift, scale, rotation: \n", red_ssr["image"] ); 
print("Image après flip:                       \n", red_flp["image"] ); 
print("Image après flip shift:                 \n", red_fsh["image"] ); 
print("Image après flip scale:                 \n", red_fsc["image"] ); 
print("Image après flip rotation:              \n", red_fro["image"] ); 
print("Image après flip shift, scale:          \n", red_fss["image"] ); 
print("Image après flip shift, rotation:       \n", red_fsr["image"] ); 
print("Image après flip scale, rotation:       \n", red_fcr["image"] ); 
print("Image après flip shift, scale, rotation:\n", red_fssr["image"] ); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Générer le pdf avec la comparaison    ");
print("************************************** ");
# Fonction qui sert à faire la comparaison des augmentations
with PdfPages("comparaison_15_augmentations.pdf")  as pdf:

  # Augmentation avec dlib
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(dlib_img[0],        cmap="gray"); 
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(dlib_sh["image"],   cmap="gray"); 
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(dlib_sc["image"],   cmap="gray"); 
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(dlib_ro["image"],   cmap="gray"); 
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(dlib_ss["image"],   cmap="gray"); 
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(dlib_sr["image"],   cmap="gray"); 
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(dlib_cr["image"],   cmap="gray"); 
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(dlib_ssr["image"],  cmap="gray"); 
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(dlib_flp["image"],  cmap="gray"); 
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(dlib_fsh["image"], cmap="gray"); 
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(dlib_fsc["image"], cmap="gray"); 
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(dlib_fro["image"], cmap="gray"); 
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(dlib_fss["image"], cmap="gray"); 
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(dlib_fsr["image"], cmap="gray"); 
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(dlib_fcr["image"], cmap="gray"); 
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(dlib_fssr["image"],cmap="gray"); 
  fig.suptitle("Augmentations avec Dlib ", fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(var_img[0],        cmap="gray"); 
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(var_sh["image"],   cmap="gray"); 
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(var_sc["image"],   cmap="gray"); 
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(var_ro["image"],   cmap="gray"); 
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(var_ss["image"],   cmap="gray"); 
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(var_sr["image"],   cmap="gray"); 
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(var_cr["image"],   cmap="gray"); 
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(var_ssr["image"],  cmap="gray"); 
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(var_flp["image"],  cmap="gray"); 
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(var_fsh["image"], cmap="gray"); 
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(var_fsc["image"], cmap="gray"); 
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(var_fro["image"], cmap="gray"); 
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(var_fss["image"], cmap="gray"); 
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(var_fsr["image"], cmap="gray"); 
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(var_fcr["image"], cmap="gray"); 
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(var_fssr["image"],cmap="gray"); 
  fig.suptitle("Augmentations avec Dlib + lbp-var", fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var + canny1986
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(can_img[0],        cmap="gray"); 
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(can_sh["image"],   cmap="gray"); 
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(can_sc["image"],   cmap="gray"); 
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(can_ro["image"],   cmap="gray"); 
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(can_ss["image"],   cmap="gray"); 
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(can_sr["image"],   cmap="gray"); 
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(can_cr["image"],   cmap="gray"); 
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(can_ssr["image"],  cmap="gray"); 
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(can_flp["image"],  cmap="gray"); 
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(can_fsh["image"], cmap="gray"); 
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(can_fsc["image"], cmap="gray"); 
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(can_fro["image"], cmap="gray"); 
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(can_fss["image"], cmap="gray"); 
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(can_fsr["image"], cmap="gray"); 
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(can_fcr["image"], cmap="gray"); 
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(can_fssr["image"],cmap="gray"); 
  fig.suptitle("Augmentations avec Dlib + lbp-var + Canny1986", fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var + canny1986 + redimensionnement
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(red_img[0],        cmap="gray"); 
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(red_sh["image"],   cmap="gray"); 
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(red_sc["image"],   cmap="gray"); 
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(red_ro["image"],   cmap="gray"); 
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(red_ss["image"],   cmap="gray"); 
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(red_sr["image"],   cmap="gray"); 
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(red_cr["image"],   cmap="gray"); 
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(red_ssr["image"],  cmap="gray"); 
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(red_flp["image"],  cmap="gray"); 
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(red_fsh["image"], cmap="gray"); 
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(red_fsc["image"], cmap="gray"); 
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(red_fro["image"], cmap="gray"); 
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(red_fss["image"], cmap="gray"); 
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(red_fsr["image"], cmap="gray"); 
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(red_fcr["image"], cmap="gray"); 
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(red_fssr["image"],cmap="gray"); 
  fig.suptitle("Augmentations avec Dlib + lbp-var + Canny1986 + redimensionnement", fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();
#sys.exit(0);                                              # Terminer l'execution


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
