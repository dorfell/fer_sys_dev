# -*- coding: utf-8 -*-
## 
# @file    00_dlib_var_canny_redim_comparaison.py
# @brief   Redimensionner l'image
# @details Ce module redimensionne les images qui vient de Canny 1986 avec 
#          seuil (50, 100). En plus, il fait la comparaison de différentes 
#          tailles.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/01/31
# @version 0.1
"""@package docstring
"""

import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from skimage.feature import local_binary_pattern as lbp_fun
from mes_fonctions import *

from matplotlib.backends.backend_pdf import PdfPages


print("************************************** ");
print(" Charger l'ensamble de données         ");
print("************************************** ");
# Localisation de l'ensamble de données (db)
loc_db = "../../db/jaffedbase_petit/"; 

# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];

# Lire la base de données
img_dat, img_lab = read_data(loc_db);
#print(img_dat, img_dat.shape,  img_lab);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Pré-traitement des données avec Dlib  ");
print("************************************** ");
# Pré-traitement des données
img_dat_pre = preprocessing(img_dat);                      # taille (128, 128)
#print("print img_dat_resized", img_dat_resized, len(img_dat_resized), type(img_dat_resized),  img_lab);
print("Image après pré-traitement: \n", img_dat_pre[0], img_dat_pre[0].dtype); 
plt.imshow(img_dat_pre[0], cmap="gray"); 
plt.title("Image pré-traité avec Dlib");  plt.show();
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Motifs Binaires Locaux (LBP)          ");
print("************************************** ");
# Paramètres des Motifs binaires locaux (LBP) 
radius = 3;                                                # Rayon de cercle
n_points = 8 * radius;                                     # nombre de points voisins circulairement symétriques.
method= "var";                                             # Méthode pour la determination du motifs. E.g. default, ror, uniform, nri_uniform, var.

lbp_var_lt = [];
for img in img_dat_pre:
  lbp_var = lbp_fun(img, n_points, radius, method);        # Calculer le LBP.
  lbp_var_lt.append(lbp_var);

plt.imshow(lbp_var, cmap="gray");                          # Imprimer quelques images
plt.title("Image avec lbp, méthode= var"); plt.show();
print("Image après lbp var: \n", lbp_var); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" À nombre entier int8                  ");
print("************************************** ");
lbp_var_int = lbp_var_lt[0];

lbp_var_int = lbp_var_int / lbp_var_int.max();             # normalizer img [0, 1].
lbp_var_int = 255 * lbp_var_int;                           # À échelle 0 - 255
lbp_var_int = np.rint(lbp_var_int);                        # Arrondir à entier
lbp_var_int = lbp_var_int.astype(np.uint8);                # cast à uint8
print("valeurs du lbp_var_int:\n ", lbp_var_int.shape); print(lbp_var_int.shape);

plt.imshow(lbp_var_int, cmap="gray");                      # Imprimer quelques images
plt.title("Image avec lbp var en entier 0 à 255");  plt.show();
print("Image avec lbp var en entier 0 à 255: \n", lbp_var_int);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Détection des contours                ");
print("************************************** ");
# Paramètres de la détection de contours: seuils --> thresholds: minVal, maxVal
# ex: img_Canny =  cv2.Canny(img, minVal, maxVal); 

# minVal = 50, maxVal = 100
can = cv2.Canny(lbp_var_int, 50, 100); 
plt.imshow(can, cmap="gray");                              # Imprimer quelques images
plt.title("Image avec détection de contours"); plt.show();
print("Image après détection de contours: \n", can); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Redimensionnement de l'image          ");
print("************************************** ");
# 24 x 24 pixels
nea_24 = cv2.resize(can, dsize=(24,24), interpolation=cv2.INTER_NEAREST);
lin_24 = cv2.resize(can, dsize=(24,24), interpolation=cv2.INTER_LINEAR);
cub_24 = cv2.resize(can, dsize=(24,24), interpolation=cv2.INTER_CUBIC);
are_24 = cv2.resize(can, dsize=(24,24), interpolation=cv2.INTER_AREA);
lan_24 = cv2.resize(can, dsize=(24,24), interpolation=cv2.INTER_LANCZOS4);
lix_24 = cv2.resize(can, dsize=(24,24), interpolation=cv2.INTER_LINEAR_EXACT);

# 32 x 32 pixels
nea_32 = cv2.resize(can, dsize=(32,32), interpolation=cv2.INTER_NEAREST);
lin_32 = cv2.resize(can, dsize=(32,32), interpolation=cv2.INTER_LINEAR);
cub_32 = cv2.resize(can, dsize=(32,32), interpolation=cv2.INTER_CUBIC);
are_32 = cv2.resize(can, dsize=(32,32), interpolation=cv2.INTER_AREA);
lan_32 = cv2.resize(can, dsize=(32,32), interpolation=cv2.INTER_LANCZOS4);
lix_32 = cv2.resize(can, dsize=(32,32), interpolation=cv2.INTER_LINEAR_EXACT);

# 48 x 48 pixels
nea_48 = cv2.resize(can, dsize=(48,48), interpolation=cv2.INTER_NEAREST);
lin_48 = cv2.resize(can, dsize=(48,48), interpolation=cv2.INTER_LINEAR);
cub_48 = cv2.resize(can, dsize=(48,48), interpolation=cv2.INTER_CUBIC);
are_48 = cv2.resize(can, dsize=(48,48), interpolation=cv2.INTER_AREA);
lan_48 = cv2.resize(can, dsize=(48,48), interpolation=cv2.INTER_LANCZOS4);
lix_48 = cv2.resize(can, dsize=(48,48), interpolation=cv2.INTER_LINEAR_EXACT);

# 56 x 56 pixels
nea_56 = cv2.resize(can, dsize=(56,56), interpolation=cv2.INTER_NEAREST);
lin_56 = cv2.resize(can, dsize=(56,56), interpolation=cv2.INTER_LINEAR);
cub_56 = cv2.resize(can, dsize=(56,56), interpolation=cv2.INTER_CUBIC);
are_56 = cv2.resize(can, dsize=(56,56), interpolation=cv2.INTER_AREA);
lan_56 = cv2.resize(can, dsize=(56,56), interpolation=cv2.INTER_LANCZOS4);
lix_56 = cv2.resize(can, dsize=(56,56), interpolation=cv2.INTER_LINEAR_EXACT);

# 64 x 64 pixels
nea_64 = cv2.resize(can, dsize=(64,64), interpolation=cv2.INTER_NEAREST);
lin_64 = cv2.resize(can, dsize=(64,64), interpolation=cv2.INTER_LINEAR);
cub_64 = cv2.resize(can, dsize=(64,64), interpolation=cv2.INTER_CUBIC);
are_64 = cv2.resize(can, dsize=(64,64), interpolation=cv2.INTER_AREA);
lan_64 = cv2.resize(can, dsize=(64,64), interpolation=cv2.INTER_LANCZOS4);
lix_64 = cv2.resize(can, dsize=(64,64), interpolation=cv2.INTER_LINEAR_EXACT);

# 72 x 72 pixels
nea_72 = cv2.resize(can, dsize=(72,72), interpolation=cv2.INTER_NEAREST);
lin_72 = cv2.resize(can, dsize=(72,72), interpolation=cv2.INTER_LINEAR);
cub_72 = cv2.resize(can, dsize=(72,72), interpolation=cv2.INTER_CUBIC);
are_72 = cv2.resize(can, dsize=(72,72), interpolation=cv2.INTER_AREA);
lan_72 = cv2.resize(can, dsize=(72,72), interpolation=cv2.INTER_LANCZOS4);
lix_72 = cv2.resize(can, dsize=(72,72), interpolation=cv2.INTER_LINEAR_EXACT);

# 96 x 96 pixels
nea_96 = cv2.resize(can, dsize=(96,96), interpolation=cv2.INTER_NEAREST);
lin_96 = cv2.resize(can, dsize=(96,96), interpolation=cv2.INTER_LINEAR);
cub_96 = cv2.resize(can, dsize=(96,96), interpolation=cv2.INTER_CUBIC);
are_96 = cv2.resize(can, dsize=(96,96), interpolation=cv2.INTER_AREA);
lan_96 = cv2.resize(can, dsize=(96,96), interpolation=cv2.INTER_LANCZOS4);
lix_96 = cv2.resize(can, dsize=(96,96), interpolation=cv2.INTER_LINEAR_EXACT);

# Imprimer les valeurs des interpolation
print("Interpolation Nearest:  \n ", nea_24); print("Interpolation Linear:    \n ", lin_24);
print("Interpolation Cubic:    \n ", cub_24); print("Interpolation Area:      \n ", are_24);
print("Interpolation Lanczos4: \n ", lan_24); print("Interpolation Lin. Exa.: \n ", lix_24);

# Fonction qui sert à comparer le redimensionnement avec différentes interpolations et taillers 
with PdfPages("comparaison_redimensionnement.pdf")  as pdf:

  # 24 x 24 pixels
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Interp. Nearest");   ax1.imshow(nea_24, cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Interp. Linear");    ax2.imshow(lin_24, cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Interp. Cubic");     ax3.imshow(cub_24, cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Interp. Area");      ax4.imshow(are_24, cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Interp. Lanczos4");  ax5.imshow(lan_24, cmap="gray"); 
  ax6 = fig.add_subplot(2, 3, 6); ax6.set_title("Interp. Lin. Exa."); ax6.imshow(lix_24, cmap="gray"); 
  fig.suptitle("Taille 24 x 24 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 32 x 32 pixels
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Interp. Nearest");   ax1.imshow(nea_32, cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Interp. Linear");    ax2.imshow(lin_32, cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Interp. Cubic");     ax3.imshow(cub_32, cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Interp. Area");      ax4.imshow(are_32, cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Interp. Lanczos4");  ax5.imshow(lan_32, cmap="gray"); 
  ax6 = fig.add_subplot(2, 3, 6); ax6.set_title("Interp. Lin. Exa."); ax6.imshow(lix_32, cmap="gray"); 
  fig.suptitle("Taille 32 x 32 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 48 x 48 pixels
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Interp. Nearest");   ax1.imshow(nea_48, cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Interp. Linear");    ax2.imshow(lin_48, cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Interp. Cubic");     ax3.imshow(cub_48, cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Interp. Area");      ax4.imshow(are_48, cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Interp. Lanczos4");  ax5.imshow(lan_48, cmap="gray"); 
  ax6 = fig.add_subplot(2, 3, 6); ax6.set_title("Interp. Lin. Exa."); ax6.imshow(lix_48, cmap="gray"); 
  fig.suptitle("Taille 48 x 48 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 56 x 56 pixels
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Interp. Nearest");   ax1.imshow(nea_56, cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Interp. Linear");    ax2.imshow(lin_56, cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Interp. Cubic");     ax3.imshow(cub_56, cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Interp. Area");      ax4.imshow(are_56, cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Interp. Lanczos4");  ax5.imshow(lan_56, cmap="gray"); 
  ax6 = fig.add_subplot(2, 3, 6); ax6.set_title("Interp. Lin. Exa."); ax6.imshow(lix_56, cmap="gray"); 
  fig.suptitle("Taille 56 x 56 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 64 x 64 pixels
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Interp. Nearest");   ax1.imshow(nea_64, cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Interp. Linear");    ax2.imshow(lin_64, cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Interp. Cubic");     ax3.imshow(cub_64, cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Interp. Area");      ax4.imshow(are_64, cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Interp. Lanczos4");  ax5.imshow(lan_64, cmap="gray"); 
  ax6 = fig.add_subplot(2, 3, 6); ax6.set_title("Interp. Lin. Exa."); ax6.imshow(lix_64, cmap="gray"); 
  fig.suptitle("Taille 64 x 64 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 72 x 72 pixels
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Interp. Nearest");   ax1.imshow(nea_72, cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Interp. Linear");    ax2.imshow(lin_72, cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Interp. Cubic");     ax3.imshow(cub_72, cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Interp. Area");      ax4.imshow(are_72, cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Interp. Lanczos4");  ax5.imshow(lan_72, cmap="gray"); 
  ax6 = fig.add_subplot(2, 3, 6); ax6.set_title("Interp. Lin. Exa."); ax6.imshow(lix_72, cmap="gray"); 
  fig.suptitle("Taille 72 x 72 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 96 x 96 pixels
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Interp. Nearest");   ax1.imshow(nea_96, cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Interp. Linear");    ax2.imshow(lin_96, cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Interp. Cubic");     ax3.imshow(cub_96, cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Interp. Area");      ax4.imshow(are_96, cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Interp. Lanczos4");  ax5.imshow(lan_96, cmap="gray"); 
  ax6 = fig.add_subplot(2, 3, 6); ax6.set_title("Interp. Lin. Exa."); ax6.imshow(lix_96, cmap="gray"); 
  fig.suptitle("Taille 96 x 96 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

#sys.exit(0);                                              # Terminer l'execution


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
