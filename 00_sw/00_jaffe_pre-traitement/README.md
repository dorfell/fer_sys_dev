------------------------------------------------------------------

     =========================================================
     fer_sys_dev/00_sw/00_jaffe_pre-traitement/ - Pré-traitement 
     de l'ensamble des données JAFFE.
     =========================================================

                         Liste des fichiers
                         ---------------

Fichiers pour faire le pré-traitement de l'ensemble des données Jaffe avec Dlib (e.g. la détection de yeux, la rotation, couper les images et l'equalization de l'histogramme), le motif locaux binaire- Local Binaire Pattern (LBP), la détection de contours Canny1986, le rédimensionnement avec l'interpolation Lanczos4) et l'augmentation des données.

00_dlib_lbp_comparaison/
 - Logiciel pour faire le pré-traitement des images avec Dlib et la comparaison des méthodes de Motif Locaux Binaire (Local Binary Pattern LBP) avec  les méthodes Uniform, NRI-Uniform, ROR et VAR. 

01_dlib_var_canny_comparaison/
 - Logiciel pour faire le pré-traitement des images avec Dlib, le motif locaux binaire (LBP) avec le  méthode var,  et faire la comparaison des valeurs minVal, maxVal du filtre Canny 1986 pour la détection de contours.

02_dlib_var_canny_redim_comparaison/
 - Logiciel pour faire le pré-traitement des images avec Dlib, le motif locaux binaire (LBP) avec le  méthode var, la détection de contours avec Canny1986 et seuils (50, 100),  et comparer le redimensionnement des images de 128 x 128 pixels à 24 x 24 px, 32 x 32 px, 48 x 48 px, 56 x 56 px, 64 x 64 px, 72 x 72 px, 96 x 96 px.

03_dlib_var_canny_redim/
 - Logiciel pour faire le pré-traitement des images avec Dlib, le motif locaux binaire (LBP) avec le  méthode var, la détection de contours avec Canny1986 et seuils (50, 100), et le redimensionnement des images de 128 x 128 pixels à 56 x 56 pixels. En plus, ce logiciel sauvegardé l'ensemble de données traitées en fer_sys_dev/00_sw/db_transfigure.

04_dlib_var_canny_redim_aug_comparaison/
 - Logiciel pour faire l'agmentation des données sur les données qui vient du pré-traitement des images avec Dlib, le motif locaux binaire (LBP) avec le  méthode var, la détection de contours avec Canny1986 et seuils (50, 100), et le redimensionnement des images de 128 x 128 pixels à 56 x 56 pixels. Le logiciel fait aussi la comparaison entre les différentes techniques de l'augmentation des données.

05_dlib_var_canny_redim_aug_comparaison/
 - Logiciel pour faire le pré-traitement des images avec Dlib, le motif locaux binaire (LBP) avec le  méthode var, la détection de contours avec Canny1986 et seuils (50, 100), et le redimensionnement des images de 128 x 128 pixels à 56 x 56 pixels. Le logiciel fait aussi la comparaison entre les différentes techniques de l'augmentation des données. En plus, ce logiciel sauvegardé l'ensemble de données traitées en fer_sys_dev/00_sw/db_transfigure.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
