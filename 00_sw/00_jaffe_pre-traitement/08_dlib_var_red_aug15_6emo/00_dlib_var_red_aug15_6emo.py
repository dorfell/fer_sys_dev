# -*- coding: utf-8 -*-
## 
# @file    00_dlib_var_red_aug15_6emo.py
# @brief   Pré-traitement du Jaffe avec Dlib + lbp-var + redimensionnement et augmentation pour 6 émotions
# @details Ce module sauvegardé l'ensemble Jaffe pré-traité avec Dlib,
#          le motif locaux binaire avec le méthode VAR, 
#          le redimensionnement à tailles de 24x24, 32x32,
#          48x48, 56x56, 64x64, 72x72, 96x96 et  128x128,  
#          avec l'interpolation Lanczos4, et exclure l'émotion 
#          neutral pour avoir à la fin 6 émotions.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/03/22
# @version 0.1
"""@package docstring
"""

import albumentations as A
import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from skimage.feature import local_binary_pattern as lbp_fun
from mes_fonctions import *

from matplotlib.backends.backend_pdf import PdfPages


print("************************************** ");
print(" Charger l'ensamble de données         ");
print("************************************** ");
# Localisation de l'ensamble de données (db)
#loc_db = "../../db/jaffedbase_petit/";                    # Seulment 4 images. 
loc_db = "../../db/jaffedbase/";                           # 213 images. 

# Code d'expressions pour 7 émotions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];

# Lire la base de données
img_dat, img_lab = read_data(loc_db);
print("forme des images: ",        img_dat.shape);
print("longueur des étiquettes: ", len(img_lab) );

# Exclure l'émotion neutral pour avoir seulement 6 émotions
img_lt = []; lab_lt = [];
for idx, img in enumerate(img_dat):
  if (img_lab[idx]!=0):
    img_lt.append(img_dat[idx]);
    lab_lt.append(img_lab[idx] - 1);                       # [0-6] à [0-5] 
img_dat = np.array(img_lt);
print("forme des images: ",        img_dat.shape);
print("longueur des étiquettes: ", len(lab_lt)  );

# Code d'expressions pour 6 expressions 
fer_eti = ["HA","AN","DI","FE","SA","SU"];
fer_lab = [  0,    1,   2,   3,   4,  5 ];

# Diviser ensemble de données pour entraîner 
img_dat = img_dat[0:165]; img_lab = lab_lt[0:165];        # Ensemble d'entraînement
#print(img_dat, img_dat.shape,  img_lab);
#sys.exit(0);                                             # Terminer l'execution


print("************************************** ");
print(" Pré-traitement des données avec Dlib  ");
print("************************************** ");
# Pré-traitement des données
img_dat_pre = preprocessing(img_dat);                      # taille (128, 128)
#print("print img_dat_resized", img_dat_resized, len(img_dat_resized), type(img_dat_resized),  img_lab);
print("Image après pré-traitement: \n", img_dat_pre[0], img_dat_pre[0].dtype); 
#plt.imshow(img_dat_pre[0], cmap="gray"); 
#plt.title("Image pré-traité avec Dlib");  plt.show();
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Motifs Binaires Locaux (LBP)          ");
print("************************************** ");
# Paramètres des Motifs binaires locaux (LBP) 
radius = 3;                                                # Rayon de cercle
n_points = 8 * radius;                                     # nombre de points voisins circulairement symétriques.
method= "var";                                             # Méthode pour la determination du motifs. E.g. default, ror, uniform, nri_uniform, var.

lbp_var_lt = [];
for img in img_dat_pre:
  lbp_var = lbp_fun(img, n_points, radius, method);        # Calculer le LBP.
  lbp_var_lt.append(lbp_var);

#plt.imshow(lbp_var, cmap="gray");                         # Imprimer quelques images
#plt.title("Image avec lbp, méthode= var"); plt.show();
#print("Image après lbp var: \n", lbp_var); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" À nombre entier int8                  ");
print("************************************** ");
var_int_lt = [];
for img in lbp_var_lt:
  img = img / img.max();                                   # normalizer img [0, 1].
  img = 255 * img;                                         # À échelle 0 - 255
  img = np.rint(img);                                      # Arrondir à entier
  var_int_lt.append( img.astype(np.uint8) );               # cast à uint8
print("valeurs du lbp_var_int[0]:\n ", var_int_lt[0]); print(var_int_lt[0].shape);
#print("valeurs du lbp_var_int:\n ", var_int_lt); 
print("longitude de liste: ", len(var_int_lt));

#plt.imshow(var_int_lt[0], cmap="gray");                   # Imprimer quelques images
#plt.title("Image avec lbp var en entier 0 à 255");  plt.show();


print("************************************** ");
print(" Redimensionnement de l'image          ");
print("************************************** ");
red24_lt = []; red32_lt = []; red48_lt = []; red56_lt = [];
red64_lt = []; red72_lt = []; red96_lt = []; red128_lt = [];
for img in var_int_lt:
  # 24 x 24 pixels
  red24 = cv2.resize(img, dsize=(24,24), interpolation=cv2.INTER_LANCZOS4);
  red24_lt.append(red24);

  # 32 x 32 pixels
  red32 = cv2.resize(img, dsize=(32,32), interpolation=cv2.INTER_LANCZOS4);
  red32_lt.append(red32);

  # 48 x 48 pixels
  red48 = cv2.resize(img, dsize=(48,48), interpolation=cv2.INTER_LANCZOS4);
  red48_lt.append(red48);

  # 56 x 56 pixels
  red56 = cv2.resize(img, dsize=(56,56), interpolation=cv2.INTER_LANCZOS4);
  red56_lt.append(red56);

  # 64 x 64 pixels
  red64 = cv2.resize(img, dsize=(64,64), interpolation=cv2.INTER_LANCZOS4);
  red64_lt.append(red64);

  # 72 x 72 pixels
  red72 = cv2.resize(img, dsize=(72,72), interpolation=cv2.INTER_LANCZOS4);
  red72_lt.append(red72);

  # 96 x 96 pixels
  red96 = cv2.resize(img, dsize=(96,96), interpolation=cv2.INTER_LANCZOS4);
  red96_lt.append(red96);

  # 128 x 128 pixels
  red128_lt.append(img);

#plt.imshow(red24_lt[0], cmap="gray");                     # Imprimer quelques images
#plt.title("Interpolation Lanczos4 24 x 24 pixels"); plt.show();
#print("Image après le redimensionnement: \n", red_lt[0]); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Définir les augmentations             ");
print("************************************** ");
# Définir les pipelines d'augmentation
trf_sh  = A.ShiftScaleRotate(                              # --> Shift
            shift_limit=0.2, scale_limit=0,
            rotate_limit=0, interpolation=4,               # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
 
trf_sc  = A.ShiftScaleRotate(                              # --> Scale
            shift_limit=0, scale_limit=0.2,
            rotate_limit=0, interpolation=4,               # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
 
trf_ro  = A.ShiftScaleRotate(                              # --> Rotation
            shift_limit=0, scale_limit=0,
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0

trf_ss  = A.ShiftScaleRotate(                              # --> Shift, scale
            shift_limit=0.2, scale_limit=0.2,
            rotate_limit=0, interpolation=4,               # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
 
trf_sr  = A.ShiftScaleRotate(                              # --> Shift, rotation
            shift_limit=0.2, scale_limit=0,
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
 
trf_cr  = A.ShiftScaleRotate(                              # --> Scale, rotation
            shift_limit=0, scale_limit=0.2,
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
 
trf_ssr  = A.ShiftScaleRotate(                             # --> Shift,scale,rotation
            shift_limit=0.2, scale_limit=0.2,
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
 
# même augmentation pour l'image retourné/flip
trf_flp = A.HorizontalFlip(p=0.99);                        # --> Horizontal flip
 
trf_fsh = A.Compose([                                      # --> Flip, shift   
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift
              shift_limit=0.2, scale_limit=0,
              rotate_limit=0, interpolation=4,             # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fsc = A.Compose([                                      # --> Flip, scale   
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Scale
              shift_limit=0, scale_limit=0.2,
              rotate_limit=0, interpolation=4,             # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fro = A.Compose([                                      # --> Flip, rotation   
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Rotation
              shift_limit=0, scale_limit=0,
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fss = A.Compose([                                      # --> Flip, shift, scale  
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift, scale
              shift_limit=0.2, scale_limit=0.2,
              rotate_limit=0, interpolation=4,             # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fsr = A.Compose([                                      # --> Flip,shift,rotation 
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift, rotation
              shift_limit=0.2, scale_limit=0,
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fcr = A.Compose([                                      # --> Flip,scale,rotation 
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Scale, rotation
              shift_limit=0, scale_limit=0.2,
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fssr = A.Compose([                                     # --> Flip,scale,shift,rot
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift,scale,rotation
               shift_limit=0.2, scale_limit=0.2,
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation pour 24x24 pixels        ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
aug24_img_lt = []; aug24_lab_lt = []; 
for idx, img in enumerate(red24_lt):
 
  # Augmentations
  tmp_sh  = trf_sh(image=img);                             # Shift 
  tmp_sc  = trf_sc(image=img);                             # Scale 
  tmp_ro  = trf_ro(image=img);                             # Rotation
  tmp_ss  = trf_ss(image=img);                             # Shift, scale 
  tmp_sr  = trf_sr(image=img);                             # Shift, rotation 
  tmp_cr  = trf_cr(image=img);                             # Scale, rotation
  tmp_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  tmp_flp = trf_flp(image=img);                            # Flip
  tmp_fsh = trf_fsh(image=img);                            # Flip shift 
  tmp_fsc = trf_fsc(image=img);                            # Flip scale 
  tmp_fro = trf_fro(image=img);                            # Flip rotation
  tmp_fss = trf_fss(image=img);                            # Flip shift, scale 
  tmp_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  tmp_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  tmp_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation
 
  # Liste des images
  aug24_img_lt.append(img);              aug24_img_lt.append(tmp_sh[ "image"]); aug24_img_lt.append(tmp_sc[ "image"]); aug24_img_lt.append(tmp_ro[ "image"]);
  aug24_img_lt.append(tmp_ss[ "image"]); aug24_img_lt.append(tmp_sr[ "image"]); aug24_img_lt.append(tmp_cr[ "image"]); aug24_img_lt.append(tmp_ssr["image"]);
  aug24_img_lt.append(tmp_flp["image"]); aug24_img_lt.append(tmp_fsh["image"]); aug24_img_lt.append(tmp_fsc["image"]); aug24_img_lt.append(tmp_fro["image"]);
  aug24_img_lt.append(tmp_fss["image"]); aug24_img_lt.append(tmp_fsr["image"]); aug24_img_lt.append(tmp_fcr["image"]); aug24_img_lt.append(tmp_fssr["image"]);
 
  # Liste des étiquettes
  aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]);
  aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]);
  aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]);
  aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]); aug24_lab_lt.append(img_lab[idx]);
 
print("len du liste:   \n", len(aug24_img_lt) );
print("shape du liste: \n", aug24_img_lt[0].shape );
#print("étiquettes:     \n", aug24_lab_lt);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation pour 32x32 pixels        ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
aug32_img_lt = []; aug32_lab_lt = []; 
for idx, img in enumerate(red32_lt):
 
  # Augmentations
  tmp_sh  = trf_sh(image=img);                             # Shift 
  tmp_sc  = trf_sc(image=img);                             # Scale 
  tmp_ro  = trf_ro(image=img);                             # Rotation
  tmp_ss  = trf_ss(image=img);                             # Shift, scale 
  tmp_sr  = trf_sr(image=img);                             # Shift, rotation 
  tmp_cr  = trf_cr(image=img);                             # Scale, rotation
  tmp_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  tmp_flp = trf_flp(image=img);                            # Flip
  tmp_fsh = trf_fsh(image=img);                            # Flip shift 
  tmp_fsc = trf_fsc(image=img);                            # Flip scale 
  tmp_fro = trf_fro(image=img);                            # Flip rotation
  tmp_fss = trf_fss(image=img);                            # Flip shift, scale 
  tmp_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  tmp_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  tmp_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation
 
  # Liste des images
  aug32_img_lt.append(img);              aug32_img_lt.append(tmp_sh[ "image"]); aug32_img_lt.append(tmp_sc[ "image"]); aug32_img_lt.append(tmp_ro[ "image"]);
  aug32_img_lt.append(tmp_ss[ "image"]); aug32_img_lt.append(tmp_sr[ "image"]); aug32_img_lt.append(tmp_cr[ "image"]); aug32_img_lt.append(tmp_ssr["image"]);
  aug32_img_lt.append(tmp_flp["image"]); aug32_img_lt.append(tmp_fsh["image"]); aug32_img_lt.append(tmp_fsc["image"]); aug32_img_lt.append(tmp_fro["image"]);
  aug32_img_lt.append(tmp_fss["image"]); aug32_img_lt.append(tmp_fsr["image"]); aug32_img_lt.append(tmp_fcr["image"]); aug32_img_lt.append(tmp_fssr["image"]);
 
  # Liste des étiquettes
  aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]);
  aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]);
  aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]);
  aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]); aug32_lab_lt.append(img_lab[idx]);
 
print("len du liste:   \n", len(aug32_img_lt) );
print("shape du liste: \n", aug32_img_lt[0].shape );
#print("étiquettes:     \n", aug32_lab_lt);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation pour 48x48 pixels        ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
aug48_img_lt = []; aug48_lab_lt = []; 
for idx, img in enumerate(red48_lt):
 
  # Augmentations
  tmp_sh  = trf_sh(image=img);                             # Shift 
  tmp_sc  = trf_sc(image=img);                             # Scale 
  tmp_ro  = trf_ro(image=img);                             # Rotation
  tmp_ss  = trf_ss(image=img);                             # Shift, scale 
  tmp_sr  = trf_sr(image=img);                             # Shift, rotation 
  tmp_cr  = trf_cr(image=img);                             # Scale, rotation
  tmp_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  tmp_flp = trf_flp(image=img);                            # Flip
  tmp_fsh = trf_fsh(image=img);                            # Flip shift 
  tmp_fsc = trf_fsc(image=img);                            # Flip scale 
  tmp_fro = trf_fro(image=img);                            # Flip rotation
  tmp_fss = trf_fss(image=img);                            # Flip shift, scale 
  tmp_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  tmp_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  tmp_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation
 
  # Liste des images
  aug48_img_lt.append(img);              aug48_img_lt.append(tmp_sh[ "image"]); aug48_img_lt.append(tmp_sc[ "image"]); aug48_img_lt.append(tmp_ro[ "image"]);
  aug48_img_lt.append(tmp_ss[ "image"]); aug48_img_lt.append(tmp_sr[ "image"]); aug48_img_lt.append(tmp_cr[ "image"]); aug48_img_lt.append(tmp_ssr["image"]);
  aug48_img_lt.append(tmp_flp["image"]); aug48_img_lt.append(tmp_fsh["image"]); aug48_img_lt.append(tmp_fsc["image"]); aug48_img_lt.append(tmp_fro["image"]);
  aug48_img_lt.append(tmp_fss["image"]); aug48_img_lt.append(tmp_fsr["image"]); aug48_img_lt.append(tmp_fcr["image"]); aug48_img_lt.append(tmp_fssr["image"]);
 
  # Liste des étiquettes
  aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]);
  aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]);
  aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]);
  aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]); aug48_lab_lt.append(img_lab[idx]);
 
print("len du liste:   \n", len(aug48_img_lt) );
print("shape du liste: \n", aug48_img_lt[0].shape );
#print("étiquettes:     \n", aug48_lab_lt);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation pour 56x56 pixels        ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
aug56_img_lt = []; aug56_lab_lt = []; 
for idx, img in enumerate(red56_lt):
 
  # Augmentations
  tmp_sh  = trf_sh(image=img);                             # Shift 
  tmp_sc  = trf_sc(image=img);                             # Scale 
  tmp_ro  = trf_ro(image=img);                             # Rotation
  tmp_ss  = trf_ss(image=img);                             # Shift, scale 
  tmp_sr  = trf_sr(image=img);                             # Shift, rotation 
  tmp_cr  = trf_cr(image=img);                             # Scale, rotation
  tmp_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  tmp_flp = trf_flp(image=img);                            # Flip
  tmp_fsh = trf_fsh(image=img);                            # Flip shift 
  tmp_fsc = trf_fsc(image=img);                            # Flip scale 
  tmp_fro = trf_fro(image=img);                            # Flip rotation
  tmp_fss = trf_fss(image=img);                            # Flip shift, scale 
  tmp_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  tmp_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  tmp_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation
 
  # Liste des images
  aug56_img_lt.append(img);              aug56_img_lt.append(tmp_sh[ "image"]); aug56_img_lt.append(tmp_sc[ "image"]); aug56_img_lt.append(tmp_ro[ "image"]);
  aug56_img_lt.append(tmp_ss[ "image"]); aug56_img_lt.append(tmp_sr[ "image"]); aug56_img_lt.append(tmp_cr[ "image"]); aug56_img_lt.append(tmp_ssr["image"]);
  aug56_img_lt.append(tmp_flp["image"]); aug56_img_lt.append(tmp_fsh["image"]); aug56_img_lt.append(tmp_fsc["image"]); aug56_img_lt.append(tmp_fro["image"]);
  aug56_img_lt.append(tmp_fss["image"]); aug56_img_lt.append(tmp_fsr["image"]); aug56_img_lt.append(tmp_fcr["image"]); aug56_img_lt.append(tmp_fssr["image"]);
 
  # Liste des étiquettes
  aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]);
  aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]);
  aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]);
  aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]); aug56_lab_lt.append(img_lab[idx]);
 
print("len du liste:   \n", len(aug56_img_lt) );
print("shape du liste: \n", aug56_img_lt[0].shape );
#print("étiquettes:     \n", aug56_lab_lt);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation pour 64x64 pixels        ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
aug64_img_lt = []; aug64_lab_lt = []; 
for idx, img in enumerate(red64_lt):
 
  # Augmentations
  tmp_sh  = trf_sh(image=img);                             # Shift 
  tmp_sc  = trf_sc(image=img);                             # Scale 
  tmp_ro  = trf_ro(image=img);                             # Rotation
  tmp_ss  = trf_ss(image=img);                             # Shift, scale 
  tmp_sr  = trf_sr(image=img);                             # Shift, rotation 
  tmp_cr  = trf_cr(image=img);                             # Scale, rotation
  tmp_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  tmp_flp = trf_flp(image=img);                            # Flip
  tmp_fsh = trf_fsh(image=img);                            # Flip shift 
  tmp_fsc = trf_fsc(image=img);                            # Flip scale 
  tmp_fro = trf_fro(image=img);                            # Flip rotation
  tmp_fss = trf_fss(image=img);                            # Flip shift, scale 
  tmp_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  tmp_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  tmp_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation
 
  # Liste des images
  aug64_img_lt.append(img);              aug64_img_lt.append(tmp_sh[ "image"]); aug64_img_lt.append(tmp_sc[ "image"]); aug64_img_lt.append(tmp_ro[ "image"]);
  aug64_img_lt.append(tmp_ss[ "image"]); aug64_img_lt.append(tmp_sr[ "image"]); aug64_img_lt.append(tmp_cr[ "image"]); aug64_img_lt.append(tmp_ssr["image"]);
  aug64_img_lt.append(tmp_flp["image"]); aug64_img_lt.append(tmp_fsh["image"]); aug64_img_lt.append(tmp_fsc["image"]); aug64_img_lt.append(tmp_fro["image"]);
  aug64_img_lt.append(tmp_fss["image"]); aug64_img_lt.append(tmp_fsr["image"]); aug64_img_lt.append(tmp_fcr["image"]); aug64_img_lt.append(tmp_fssr["image"]);
 
  # Liste des étiquettes
  aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]);
  aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]);
  aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]);
  aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]); aug64_lab_lt.append(img_lab[idx]);
 
print("len du liste:   \n", len(aug64_img_lt) );
print("shape du liste: \n", aug64_img_lt[0].shape );
#print("étiquettes:     \n", aug64_lab_lt);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation pour 72x72 pixels        ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
aug72_img_lt = []; aug72_lab_lt = []; 
for idx, img in enumerate(red72_lt):
 
  # Augmentations
  tmp_sh  = trf_sh(image=img);                             # Shift 
  tmp_sc  = trf_sc(image=img);                             # Scale 
  tmp_ro  = trf_ro(image=img);                             # Rotation
  tmp_ss  = trf_ss(image=img);                             # Shift, scale 
  tmp_sr  = trf_sr(image=img);                             # Shift, rotation 
  tmp_cr  = trf_cr(image=img);                             # Scale, rotation
  tmp_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  tmp_flp = trf_flp(image=img);                            # Flip
  tmp_fsh = trf_fsh(image=img);                            # Flip shift 
  tmp_fsc = trf_fsc(image=img);                            # Flip scale 
  tmp_fro = trf_fro(image=img);                            # Flip rotation
  tmp_fss = trf_fss(image=img);                            # Flip shift, scale 
  tmp_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  tmp_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  tmp_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation
 
  # Liste des images
  aug72_img_lt.append(img);              aug72_img_lt.append(tmp_sh[ "image"]); aug72_img_lt.append(tmp_sc[ "image"]); aug72_img_lt.append(tmp_ro[ "image"]);
  aug72_img_lt.append(tmp_ss[ "image"]); aug72_img_lt.append(tmp_sr[ "image"]); aug72_img_lt.append(tmp_cr[ "image"]); aug72_img_lt.append(tmp_ssr["image"]);
  aug72_img_lt.append(tmp_flp["image"]); aug72_img_lt.append(tmp_fsh["image"]); aug72_img_lt.append(tmp_fsc["image"]); aug72_img_lt.append(tmp_fro["image"]);
  aug72_img_lt.append(tmp_fss["image"]); aug72_img_lt.append(tmp_fsr["image"]); aug72_img_lt.append(tmp_fcr["image"]); aug72_img_lt.append(tmp_fssr["image"]);
 
  # Liste des étiquettes
  aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]);
  aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]);
  aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]);
  aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]); aug72_lab_lt.append(img_lab[idx]);
 
print("len du liste:   \n", len(aug72_img_lt) );
print("shape du liste: \n", aug72_img_lt[0].shape );
#print("étiquettes:     \n", aug72_lab_lt);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation pour 96x96 pixels        ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
aug96_img_lt = []; aug96_lab_lt = []; 
for idx, img in enumerate(red96_lt):
 
  # Augmentations
  tmp_sh  = trf_sh(image=img);                             # Shift 
  tmp_sc  = trf_sc(image=img);                             # Scale 
  tmp_ro  = trf_ro(image=img);                             # Rotation
  tmp_ss  = trf_ss(image=img);                             # Shift, scale 
  tmp_sr  = trf_sr(image=img);                             # Shift, rotation 
  tmp_cr  = trf_cr(image=img);                             # Scale, rotation
  tmp_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  tmp_flp = trf_flp(image=img);                            # Flip
  tmp_fsh = trf_fsh(image=img);                            # Flip shift 
  tmp_fsc = trf_fsc(image=img);                            # Flip scale 
  tmp_fro = trf_fro(image=img);                            # Flip rotation
  tmp_fss = trf_fss(image=img);                            # Flip shift, scale 
  tmp_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  tmp_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  tmp_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation
 
  # Liste des images
  aug96_img_lt.append(img);              aug96_img_lt.append(tmp_sh[ "image"]); aug96_img_lt.append(tmp_sc[ "image"]); aug96_img_lt.append(tmp_ro[ "image"]);
  aug96_img_lt.append(tmp_ss[ "image"]); aug96_img_lt.append(tmp_sr[ "image"]); aug96_img_lt.append(tmp_cr[ "image"]); aug96_img_lt.append(tmp_ssr["image"]);
  aug96_img_lt.append(tmp_flp["image"]); aug96_img_lt.append(tmp_fsh["image"]); aug96_img_lt.append(tmp_fsc["image"]); aug96_img_lt.append(tmp_fro["image"]);
  aug96_img_lt.append(tmp_fss["image"]); aug96_img_lt.append(tmp_fsr["image"]); aug96_img_lt.append(tmp_fcr["image"]); aug96_img_lt.append(tmp_fssr["image"]);
 
  # Liste des étiquettes
  aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]);
  aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]);
  aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]);
  aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]); aug96_lab_lt.append(img_lab[idx]);
 
print("len du liste:   \n", len(aug96_img_lt) );
print("shape du liste: \n", aug96_img_lt[0].shape );
#print("étiquettes:     \n", aug96_lab_lt);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation pour 128x128 pixels      ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
aug128_img_lt = []; aug128_lab_lt = []; 
for idx, img in enumerate(red128_lt):
 
  # Augmentations
  tmp_sh  = trf_sh(image=img);                             # Shift 
  tmp_sc  = trf_sc(image=img);                             # Scale 
  tmp_ro  = trf_ro(image=img);                             # Rotation
  tmp_ss  = trf_ss(image=img);                             # Shift, scale 
  tmp_sr  = trf_sr(image=img);                             # Shift, rotation 
  tmp_cr  = trf_cr(image=img);                             # Scale, rotation
  tmp_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  tmp_flp = trf_flp(image=img);                            # Flip
  tmp_fsh = trf_fsh(image=img);                            # Flip shift 
  tmp_fsc = trf_fsc(image=img);                            # Flip scale 
  tmp_fro = trf_fro(image=img);                            # Flip rotation
  tmp_fss = trf_fss(image=img);                            # Flip shift, scale 
  tmp_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  tmp_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  tmp_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation
 
  # Liste des images
  aug128_img_lt.append(img);              aug128_img_lt.append(tmp_sh[ "image"]); aug128_img_lt.append(tmp_sc[ "image"]); aug128_img_lt.append(tmp_ro[ "image"]);
  aug128_img_lt.append(tmp_ss[ "image"]); aug128_img_lt.append(tmp_sr[ "image"]); aug128_img_lt.append(tmp_cr[ "image"]); aug128_img_lt.append(tmp_ssr["image"]);
  aug128_img_lt.append(tmp_flp["image"]); aug128_img_lt.append(tmp_fsh["image"]); aug128_img_lt.append(tmp_fsc["image"]); aug128_img_lt.append(tmp_fro["image"]);
  aug128_img_lt.append(tmp_fss["image"]); aug128_img_lt.append(tmp_fsr["image"]); aug128_img_lt.append(tmp_fcr["image"]); aug128_img_lt.append(tmp_fssr["image"]);
 
  # Liste des étiquettes
  aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]);
  aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]);
  aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]);
  aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]); aug128_lab_lt.append(img_lab[idx]);
 
print("len du liste:   \n", len(aug128_img_lt) );
print("shape du liste: \n", aug128_img_lt[0].shape );
#print("étiquettes:     \n", aug128_lab_lt);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Sauvegarder les données augmentées    ");
print("************************************** ");
# Sauvegarder images et étiquettes avec dlib + lbp-var + red24 + aug15
np.save("../../db_transfigure/jaffe_dlib_var24_aug15_6emo_img.npy", aug24_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var24_aug15_6emo_lab.npy", aug24_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red32 + aug15
np.save("../../db_transfigure/jaffe_dlib_var32_aug15_6emo_img.npy", aug32_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var32_aug15_6emo_lab.npy", aug32_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red48 + aug15
np.save("../../db_transfigure/jaffe_dlib_var48_aug15_6emo_img.npy", aug48_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var48_aug15_6emo_lab.npy", aug48_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red56 + aug15
np.save("../../db_transfigure/jaffe_dlib_var56_aug15_6emo_img.npy", aug56_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var56_aug15_6emo_lab.npy", aug56_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red64 + aug15
np.save("../../db_transfigure/jaffe_dlib_var64_aug15_6emo_img.npy", aug64_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var64_aug15_6emo_lab.npy", aug64_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red72 + aug15
np.save("../../db_transfigure/jaffe_dlib_var72_aug15_6emo_img.npy", aug72_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var72_aug15_6emo_lab.npy", aug72_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red96 + aug15
np.save("../../db_transfigure/jaffe_dlib_var96_aug15_6emo_img.npy", aug96_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var96_aug15_6emo_lab.npy", aug96_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + red128 + aug15
np.save("../../db_transfigure/jaffe_dlib_var128_aug15_6emo_img.npy", aug128_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var128_aug15_6emo_lab.npy", aug128_lab_lt);


print("************************************** ");
print(" Charger les données augmentées        ");
print("************************************** ");
# Charger les images et étiquettes avec dlib + lbp-var + red24 + aug15
aug24_img = np.load("../../db_transfigure/jaffe_dlib_var24_aug15_6emo_img.npy");
aug24_lab = np.load("../../db_transfigure/jaffe_dlib_var24_aug15_6emo_lab.npy");
 
# Charger les images et étiquettes avec dlib + lbp-var + red32 + aug15
aug32_img = np.load("../../db_transfigure/jaffe_dlib_var32_aug15_6emo_img.npy");
aug32_lab = np.load("../../db_transfigure/jaffe_dlib_var32_aug15_6emo_lab.npy");

# Charger les images et étiquettes avec dlib + lbp-var + red48 + aug15
aug48_img = np.load("../../db_transfigure/jaffe_dlib_var48_aug15_6emo_img.npy");
aug48_lab = np.load("../../db_transfigure/jaffe_dlib_var48_aug15_6emo_lab.npy");

# Charger les images et étiquettes avec dlib + lbp-var + red56 + aug15
aug56_img = np.load("../../db_transfigure/jaffe_dlib_var56_aug15_6emo_img.npy");
aug56_lab = np.load("../../db_transfigure/jaffe_dlib_var56_aug15_6emo_lab.npy");

# Charger les images et étiquettes avec dlib + lbp-var + red64 + aug15
aug64_img = np.load("../../db_transfigure/jaffe_dlib_var64_aug15_6emo_img.npy");
aug64_lab = np.load("../../db_transfigure/jaffe_dlib_var64_aug15_6emo_lab.npy");

# Charger les images et étiquettes avec dlib + lbp-var + red72 + aug15
aug72_img = np.load("../../db_transfigure/jaffe_dlib_var72_aug15_6emo_img.npy");
aug72_lab = np.load("../../db_transfigure/jaffe_dlib_var72_aug15_6emo_lab.npy");

# Charger les images et étiquettes avec dlib + lbp-var + red96 + aug15
aug96_img = np.load("../../db_transfigure/jaffe_dlib_var96_aug15_6emo_img.npy");
aug96_lab = np.load("../../db_transfigure/jaffe_dlib_var96_aug15_6emo_lab.npy");

# Charger les images et étiquettes avec dlib + lbp-var + red128 + aug15
aug128_img = np.load("../../db_transfigure/jaffe_dlib_var128_aug15_6emo_img.npy");
aug128_lab = np.load("../../db_transfigure/jaffe_dlib_var128_aug15_6emo_lab.npy");


print("************************************** ");
print(" Générer le pdf avec des exemples      ");
print("************************************** ");
# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
 
# Fonction qui sert à générer des exemples d'augmentation 
with PdfPages("exemples_augmentation15.pdf") as pdf:
 
  # Augmentation avec dlib + lbp-var à taille de 24x24 pixels
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(aug24_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(aug24_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(aug24_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(aug24_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(aug24_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(aug24_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(aug24_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(aug24_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(aug24_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(aug24_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(aug24_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(aug24_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(aug24_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(aug24_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(aug24_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(aug24_img[15],cmap="gray");
  fig.suptitle("Augmentation avec dlib + lbp-var et 24x24 px. Expression: "+ fer_eti[aug24_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var à taille de 32x32 pixels
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(aug32_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(aug32_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(aug32_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(aug32_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(aug32_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(aug32_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(aug32_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(aug32_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(aug32_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(aug32_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(aug32_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(aug32_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(aug32_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(aug32_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(aug32_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(aug32_img[15],cmap="gray");
  fig.suptitle("Augmentation avec dlib + lbp-var et 32x32 px. Expression: "+ fer_eti[aug32_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var à taille de 48x48 pixels
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(aug48_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(aug48_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(aug48_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(aug48_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(aug48_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(aug48_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(aug48_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(aug48_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(aug48_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(aug48_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(aug48_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(aug48_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(aug48_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(aug48_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(aug48_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(aug48_img[15],cmap="gray");
  fig.suptitle("Augmentation avec dlib + lbp-var et 48x48 px. Expression: "+ fer_eti[aug48_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var à taille de 56x56 pixels
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(aug56_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(aug56_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(aug56_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(aug56_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(aug56_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(aug56_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(aug56_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(aug56_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(aug56_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(aug56_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(aug56_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(aug56_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(aug56_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(aug56_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(aug56_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(aug56_img[15],cmap="gray");
  fig.suptitle("Augmentation avec dlib + lbp-var et 56x56 px. Expression: "+ fer_eti[aug56_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var à taille de 64x64 pixels
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(aug64_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(aug64_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(aug64_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(aug64_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(aug64_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(aug64_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(aug64_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(aug64_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(aug64_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(aug64_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(aug64_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(aug64_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(aug64_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(aug64_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(aug64_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(aug64_img[15],cmap="gray");
  fig.suptitle("Augmentation avec dlib + lbp-var et 64x64 px. Expression: "+ fer_eti[aug64_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var à taille de 72x72 pixels
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(aug72_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(aug72_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(aug72_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(aug72_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(aug72_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(aug72_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(aug72_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(aug72_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(aug72_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(aug72_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(aug72_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(aug72_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(aug72_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(aug72_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(aug72_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(aug72_img[15],cmap="gray");
  fig.suptitle("Augmentation avec dlib + lbp-var et 72x72 px. Expression: "+ fer_eti[aug72_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var à taille de 96x96 pixels
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(aug96_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(aug96_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(aug96_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(aug96_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(aug96_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(aug96_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(aug96_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(aug96_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(aug96_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(aug96_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(aug96_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(aug96_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(aug96_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(aug96_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(aug96_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(aug96_img[15],cmap="gray");
  fig.suptitle("Augmentation avec dlib + lbp-var et 96x96 px. Expression: "+ fer_eti[aug96_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var à taille de 128x128 pixels
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(aug128_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(aug128_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(aug128_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(aug128_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(aug128_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(aug128_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(aug128_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(aug128_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(aug128_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(aug128_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(aug128_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(aug128_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(aug128_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(aug128_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(aug128_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(aug128_img[15],cmap="gray");
  fig.suptitle("Augmentation avec dlib + lbp-var et 128x128 px. Expression: "+ fer_eti[aug128_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();
#sys.exit(0);                                              # Terminer l'execution


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
