# -*- coding: utf-8 -*-
## 
# @file    mes_fonctions.py 
# @brief   Bibliothèque des fonctions 
# @details Module avec les fonctions pour les pré-traitements des données.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/01/15
# @version 0.1
"""@package docstring
"""

import os
import cv2
import dlib
import math
import numpy as np
import matplotlib.pyplot as plt

from imutils import face_utils



def angle_line_x_axis(point1, point2):
  ''' Pris du: https://github.com/anas-899  
      Auteur: Anas Khayata. '''
  angle_r = math.atan2(point1[1] - point2[1], point1[0] - point2[0]);
  angle_degree = angle_r * 180 / math.pi;
  return angle_degree


def detect_eyes(img):
  ''' Fonction pour détecter les yeux des images en échelle des gris,
      en utilisant le fonction "detector" de la bibliothèque dlib et 
      les landmarks pour regions de la visage.
      Paràmetres:
        * img: Cet une image du e.g. (256, 256)
      Returns:
        * Matrice des images; e.g. (213, 256, 256)
      Adapté du: https://github.com/anas-899  
      Auteur: Anas Khayata. '''

  detector = dlib.get_frontal_face_detector();
    
  # 68 face landmarks dlib model created by Davis King
  # https://github.com/davisking/dlib-models
  # More info https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/
  landmarks_predictor_model = "../../db/shape_predictor_68_face_landmarks.dat";
  predictor = dlib.shape_predictor(landmarks_predictor_model);
  rects = detector(img, 1); # 1 pour resampler l'image 1 fois et faciliter la détection des visages.
  #print(rects); 

  # loop over the face detections
  for (i, rect) in enumerate(rects):
    # determine the facial landmarks for the face region, then
    # convert the landmark (x, y)-coordinates to a NumPy array
    shape = predictor(img, rect);
    shape = face_utils.shape_to_np(shape);
        
    pts_right = shape[36 : 42];   # right eye landmarks
    pts_left = shape[42 : 48];    # left eye landmarks
        
    hull_right = cv2.convexHull(pts_right);
    M_right = cv2.moments(hull_right);

    # calculate x,y coordinate of center
    cX_right = int(M_right["m10"] / M_right["m00"]);
    cY_right = int(M_right["m01"] / M_right["m00"]);
    right_eye_center = (cX_right, cY_right);
   
    hull_left = cv2.convexHull(pts_left);
    M_left = cv2.moments(hull_left);

    # calculate x,y coordinate of center
    cX_left = int(M_left["m10"] / M_left["m00"])
    cY_left = int(M_left["m01"] / M_left["m00"])
    left_eye_center = (cX_left, cY_left)

  return left_eye_center, right_eye_center


def preprocessing(img_dat):
  ''' Fonction pour faire le pré-traitement des données:
      * Détecter les yeux
      * Détecter l'angle de la ligne du axe x  (entre yeux)
      * Tourner l'image
      * Détecter la région d'intérêt de l'image
      * Redimensionner l'image
      * Égalizer l'histogramme
      Paràmetres:
      * img_dat: Matrice des images, e.g. (213, 256, 256).
      Returns:
      * Matrice des images; e.g. (213, 256, 256)
      Adapté du: https://github.com/anas-899  
      Auteur: Anas Khayata. '''
  normalized_faces = []
  for img in img_dat:
    left_eye, rigth_eye = detect_eyes(img);
    #print(left_eye, rigth_eye);
    ang = angle_line_x_axis(left_eye, rigth_eye);
    rot_img = rotateImage(img, ang);
        
    # line length
    D = cv2.norm(np.array(left_eye) - np.array(rigth_eye));
        
    # center of the line
    D_point = [(left_eye[0] + rigth_eye[0]) / 2, (left_eye[1] + rigth_eye[1]) / 2];
        
    # Face ROI
    x_point = int(D_point[0] - (0.9 * D));
    y_point = int(D_point[1] - (0.6 * D));
    width_point = int(1.8 * D);
    height_point = int(2.2 * D);
    r = [x_point, y_point, width_point, height_point];
    face_roi = rot_img[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])];
        
    # resize to (96, 96) ou (128, 128)
    #face_roi = cv2.resize(face_roi,(96,96));
    face_roi = cv2.resize(face_roi,(128,128));
        
    # Equalize Hist
    face_roi = cv2.equalizeHist(face_roi);
    normalized_faces.append(face_roi);
      
    # Imprimer quelques images
    #print(face_roi);
    #plt.imshow(face_roi, cmap="gray"); plt.show();

  return normalized_faces


def read_data(loc_db):
  ''' Fonction pour lire le base des données et 
      obtenir les etiquettes de chaque image avec
      son nom.
      path: localisation des données.
      Adapté du: https://github.com/anas-899  
      Auteur: Anas Khayata. '''
  # 7 expressions: [neutral, happy, angry, disgust, fear, sad, surprise]
  lab_dat = ["NE","HA","AN","DI","FE","SA","SU"];
  ind_lab = [ 0,   1,   2,   3,   4,   5,   6];
  img_dat = [];                          # Matrice des images
  img_lab = [];                          # Vecteur des étiquettes

  nom_fics = sorted(os.listdir(loc_db)); # Nom des fichiers

  for nom_img in nom_fics:
    #print(nom_img);
    img_tmp = cv2.imread(loc_db + nom_img, cv2.IMREAD_GRAYSCALE);
    #print("cv2.imread: ", img_tmp, img_tmp.shape, type(img_tmp)); 
    img_dat.append(img_tmp);
    lab_tmp = nom_img[3:5]; 
    img_lab.append(lab_dat.index(lab_tmp)); 
    #plt.imshow(img_dat[0], cmap="gray"); plt.show();

  img_dat = np.array(img_dat);
  return img_dat, img_lab


def rotateImage(image, angle):
  ''' Pris du: https://github.com/anas-899  
      Auteur: Anas Khayata. '''
  image_center = tuple(np.array(image.shape[1::-1]) / 2);
  rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0);
  result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR);
  return result        


#print("                                      ");
#print("**************************************");
#print("*  ¡Merci d'utiliser ce logiciel!    *");
#print("*             (8-)                   *");
#print("**************************************");
