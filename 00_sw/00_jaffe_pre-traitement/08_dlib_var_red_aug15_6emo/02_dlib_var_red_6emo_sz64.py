# -*- coding: utf-8 -*-
## 
# @file    02_dlib_var_red_6emo_6sz.py
# @brief   Plot du Pré-traitement du Jaffe avec Dlib + lbp-var + redimensionnement pour 6 émotions.
# @details Ce module sauvegardé l'ensemble Jaffe pré-traité avec Dlib,
#          le motif locaux binaire avec le méthode VAR, 
#          le redimensionnement à tailles de 64x64  
#          avec l'interpolation Lanczos4, et exclure l'émotion 
#          neutral pour avoir  à la fin 6 émotions.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2023/03/14
# @version 0.1
"""@package docstring
"""

import albumentations as A
#import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

#from skimage.feature import local_binary_pattern as lbp_fun
#from mes_fonctions import *

from matplotlib.backends.backend_pdf import PdfPages





# Charger les images et étiquettes avec dlib + lbp-var + red64 + aug15$
aug64_img = np.load("../../db_transfigure/jaffe_dlib_var64_aug15_6emo_img.npy");
aug64_lab = np.load("../../db_transfigure/jaffe_dlib_var64_aug15_6emo_lab.npy");


# Augmentation avec dlib + lbp-var à taille de 64x64 pixels
#fig = plt.figure(figsize=(8, 9));
fig = plt.figure( );
ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(aug64_img[0],  cmap="gray"); ax1.axis("off");
ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(aug64_img[1],  cmap="gray"); ax2.axis("off");
ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(aug64_img[2],  cmap="gray"); ax3.axis("off");
ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(aug64_img[3],  cmap="gray"); ax4.axis("off");
ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(aug64_img[4],  cmap="gray"); ax5.axis("off");
ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(aug64_img[5],  cmap="gray"); ax6.axis("off");
ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(aug64_img[6],  cmap="gray"); ax7.axis("off");
ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(aug64_img[7],  cmap="gray"); ax8.axis("off");
ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(aug64_img[8],  cmap="gray"); ax9.axis("off");
ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(aug64_img[9], cmap="gray"); ax10.axis("off");
ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(aug64_img[10],cmap="gray"); ax11.axis("off");
ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(aug64_img[11],cmap="gray"); ax12.axis("off");
ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(aug64_img[12],cmap="gray"); ax13.axis("off");
ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(aug64_img[13],cmap="gray"); ax14.axis("off");
ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(aug64_img[14],cmap="gray"); ax15.axis("off");
ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(aug64_img[15],cmap="gray"); ax16.axis("off");
#fig.suptitle("Augmentation avec dlib + lbp-var et 64x64 px. Expression: "+ fer_eti[aug64_lab[0]], fontsize=14); 
#plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=0.3);
plt.tight_layout( w_pad=0.5, h_pad=0.2);
fig.subplots_adjust(top=0.90);
plt.show();                                             # Commenter pour le pdf$
#pdf.savefig(); plt.close();








print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
