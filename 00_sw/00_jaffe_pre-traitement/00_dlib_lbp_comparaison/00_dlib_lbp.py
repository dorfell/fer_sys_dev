# -*- coding: utf-8 -*-
## 
# @file    00_dlib_lbp.py
# @brief   Dlib + LBP
# @details Ce module fait le pré-traitement des images de l'ensamble des données
#          Jaffe pour amèliorer l'estimation des expressions avec Dlib. En plus,
#          il applique le motif binaire local LBP aux images et fait la 
#          comparaison entre les méthodes LBP.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/01/20
# @version 0.1
"""@package docstring
"""

import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from skimage.feature import local_binary_pattern as lbp_fun
from mes_fonctions import *

from matplotlib.backends.backend_pdf import PdfPages


print("************************************** ");
print(" Charger l'ensamble de données         ");
print("************************************** ");
# Localisation de l'ensamble de données (db)
loc_db = "../../db/jaffe_petit/"; 

# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];

# Lire la base de données
img_dat, img_lab = read_data(loc_db);
#print(img_dat, img_dat.shape,  img_lab);
plt.imshow(img_dat[0], cmap="gray"); 
plt.title("Image original");  plt.show();
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Pré-traitement des données avec Dlib  ");
print("************************************** ");
# Pré-traitement des données
img_dat_pre = preprocessing(img_dat);                      # taille (128, 128)
#print("print img_dat_resized", img_dat_resized, len(img_dat_resized), type(img_dat_resized),  img_lab);
print("Image après pré-traitement: \n", img_dat_pre[0], img_dat_pre[0].dtype); 
plt.imshow(img_dat_pre[0], cmap="gray"); 
plt.title("Image pré-traité avec Dlib ");  plt.show();
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Comparaison des Motifs Binaires       ");
print(" Locaux (LBP)                          ");
print("************************************** ");
# Paramètres des Motifs binaires locaux (LBP) 
radius = 3;                                                # Rayon de cercle
n_points = 8 * radius;                                     # nombre de points voisins circulairement symétriques.
#method= "var";                                            # Méthode pour la determination du motifs. E.g. default, ror, uniform, nri_uniform, var.

lbp_uni_lt = []; lbp_nri_lt = []; lbp_ror_lt = []; lbp_var_lt = [];
for img in img_dat_pre:
  lbp_uni = lbp_fun(img, n_points, radius, "uniform");     # Calculer le LBP.
  lbp_nri = lbp_fun(img, n_points, radius, "nri_uniform"); # Calculer le LBP.
  lbp_ror = lbp_fun(img, n_points, radius, "ror");         # Calculer le LBP.
  lbp_var = lbp_fun(img, n_points, radius, "var");         # Calculer le LBP.

  lbp_uni_lt.append(lbp_uni);  lbp_nri_lt.append(lbp_nri);
  lbp_ror_lt.append(lbp_ror);  lbp_var_lt.append(lbp_var);

plt.imshow(lbp_uni, cmap="gray");                          # Imprimer quelques images
plt.title("Image avec lbp, méthode= uniform");  plt.show();
print("Image après lbp uniform: \n", lbp_uni); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" À nombre entier int8                  ");
print("************************************** ");
lbp_uni_int = lbp_uni_lt[0]; lbp_nri_int = lbp_nri_lt[0];
lbp_ror_int = lbp_ror_lt[0]; lbp_var_int = lbp_var_lt[0];

# LBP uniform
lbp_uni_int = lbp_uni_int / lbp_uni_int.max();             # normalizer img [0, 1].
lbp_uni_int = 255 * lbp_uni_int;                           # À échelle 0 - 255
lbp_uni_int = np.rint(lbp_uni_int);                        # Arrondir à entier
lbp_uni_int = lbp_uni_int.astype(np.uint8);                # cast à uint8
print("valeurs du lbp_uni_int:\n ", lbp_uni_int.shape); print(lbp_uni_int.shape);

# LBP nri_uniform
lbp_nri_int = lbp_nri_int / lbp_nri_int.max();             # normalizer img [0, 1].
lbp_nri_int = 255 * lbp_nri_int;                           # À échelle 0 - 255
lbp_nri_int = np.rint(lbp_nri_int);                        # Arrondir à entier
lbp_nri_int = lbp_nri_int.astype(np.uint8);                # cast à uint8
print("valeurs du lbp_nri_int:\n ", lbp_nri_int.shape); print(lbp_nri_int.shape);

# LBP ror
lbp_ror_int = lbp_ror_int / lbp_ror_int.max();             # normalizer img [0, 1].
lbp_ror_int = 255 * lbp_ror_int;                           # À échelle 0 - 255
lbp_ror_int = np.rint(lbp_ror_int);                        # Arrondir à entier
lbp_ror_int = lbp_ror_int.astype(np.uint8);                # cast à uint8
print("valeurs du lbp_ror_int:\n ", lbp_ror_int.shape); print(lbp_ror_int.shape);

# LBP var
lbp_var_int = lbp_var_int / lbp_var_int.max();             # normalizer img [0, 1].
lbp_var_int = 255 * lbp_var_int;                           # À échelle 0 - 255
lbp_var_int = np.rint(lbp_var_int);                        # Arrondir à entier
lbp_var_int = lbp_var_int.astype(np.uint8);                # cast à uint8
print("valeurs du lbp_var_int:\n ", lbp_var_int.shape); print(lbp_var_int.shape);

plt.imshow(lbp_var_int, cmap="gray");                      # Imprimer quelques images
plt.title("Image avec lbp var en entier 0 à 255");  plt.show();
print("Image avec lbp var en entier 0 à 255: \n", lbp_var_int);
#sys.exit(0);                                              # Terminer l'execution


# Fonction qui sert à comparer les méthodes de LBP 
with PdfPages("lbp_comparaison.pdf")  as export_pdf:
  fig = plt.figure();
  ax1 = fig.add_subplot(1, 4, 1); ax1.set_title("LBP UNI"); ax1.imshow(lbp_uni_int, cmap="gray"); ax1.axis("off");
  ax2 = fig.add_subplot(1, 4, 2); ax2.set_title("LBP NRI"); ax2.imshow(lbp_nri_int, cmap="gray"); ax2.axis("off");
  ax3 = fig.add_subplot(1, 4, 3); ax3.set_title("LBP ROR"); ax3.imshow(lbp_ror_int, cmap="gray"); ax3.axis("off");
  ax4 = fig.add_subplot(1, 4, 4); ax4.set_title("LBP VAR"); ax4.imshow(lbp_var_int, cmap="gray"); ax4.axis("off");
  plt.tight_layout();
  plt.show();                                             # Commenter pour le pdf
  export_pdf.savefig(); plt.close();


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
