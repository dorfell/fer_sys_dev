# -*- coding: utf-8 -*-
## 
# @file    00_dlib_var_canny_redim_aug4.py
# @brief   Augmentation des données sur Jaffe
# @details Ce module fait l'augmentation des données sur les
#          différentes sorties des étapes de pré-traitement
#          de l'ensemble des données Jaffe (i.e. dlib, dlib + var,
#          dlib + var + canny1986 (50, 100), 
#          dlib + var + canny1986 (50, 100) + redimensionnement 56 x 56 pixels).
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/02/11
# @version 0.1
"""@package docstring
"""

import albumentations as A
import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from matplotlib.backends.backend_pdf import PdfPages
from tensorflow import keras


print("************************************** ");
print(" Charger les transfigurations          ");
print("************************************** ");
# Charger image + pré-traitement avec dlib
dlib_img = np.load("../../db_transfigure/jaffe_dlib128_img.npy");
dlib_lab = np.load("../../db_transfigure/jaffe_dlib128_lab.npy");
 
# Charger image + pré-traitement  avec dlib + lbp-var
var_img = np.load("../../db_transfigure/jaffe_dlib_var128_img.npy");
var_lab = np.load("../../db_transfigure/jaffe_dlib_var128_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986
can_img = np.load("../../db_transfigure/jaffe_dlib_var_can128_img.npy");
can_lab = np.load("../../db_transfigure/jaffe_dlib_var_can128_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986 + redimensionnent
red_img = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_img.npy");
red_lab = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_lab.npy");


print("************************************** ");
print(" Définir les augmentations             ");
print("************************************** ");
# Définir les pipelines d'augmentation
trf_flp = A.HorizontalFlip(p=0.99);                        # Horizontal flip

trf_rot = A.Rotate(                                        # Rotation
            limit=20, interpolation=4, border_mode=0,      # --> Lanczos4, border=0
            value=0, p=0.99);                              

trf_fr  = A.Compose([                                      # Flip et rotation   
            A.HorizontalFlip(p=0.99),                        
            A.Rotate( 
              limit=20, interpolation=4, border_mode=0,    # --> Lanczos4, border=0
              value=0, p=0.99) ]);                          

trf_ssr = A.ShiftScaleRotate(                              # Shift, scale et rotation
            shift_limit=0.1, scale_limit=0.2, 
            rotate_limit=10, interpolation=4,              # --> Lanczos4
            border_mode=0, value=0, p=0.99);               # --> border=0


print("************************************** ");
print(" Augmentation avec dlib                ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
dlib_img = dlib_img[0:183]; dlib_lab = dlib_lab[0:183];    
dlib_img_lt = []; dlib_lab_lt = [];
for idx, img in enumerate(dlib_img):

  # Augmentations
  dlib_flp = trf_flp(image=img);                           # Horizontal flip 
  dlib_rot = trf_rot(image=img);                           # Rotation
  dlib_fr  = trf_fr( image=img);                           # Flip et rotation
  dlib_ssr = trf_ssr(image=img);                           # Shift,scale et rotation
  
  # Liste des images
  dlib_img_lt.append(img); 
  dlib_img_lt.append(dlib_flp["image"]); dlib_img_lt.append(dlib_rot["image"]);
  dlib_img_lt.append(dlib_fr["image"]);  dlib_img_lt.append(dlib_ssr["image"]);

  # Liste des étiquettes
  dlib_lab_lt.append(dlib_lab[idx]);
  dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]);
  dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]);

#print("len du liste: \n",   len(dlib_img_lt) );
#print("shape du liste: \n", dlib_img_lt[0].shape );
#print("étiquettes: \n",     dlib_lab_lt);


print("************************************** ");
print(" Augmentation avec dlib + lbp-var      ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
var_img = var_img[0:183]; var_lab = var_lab[0:183];    
var_img_lt = []; var_lab_lt = [];
for idx, img in enumerate(var_img):

  # Augmentations
  var_flp = trf_flp(image=img);                            # Horizontal flip 
  var_rot = trf_rot(image=img);                            # Rotation
  var_fr  = trf_fr( image=img);                            # Flip et rotation
  var_ssr = trf_ssr(image=img);                            # Shift,scale et rotation
  
  # Liste des images
  var_img_lt.append(img); 
  var_img_lt.append(var_flp["image"]); var_img_lt.append(var_rot["image"]);
  var_img_lt.append(var_fr["image"]);  var_img_lt.append(var_ssr["image"]);

  # Liste des étiquettes
  var_lab_lt.append(var_lab[idx]);
  var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]);
  var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]);


print("************************************** ");
print(" Aug. avec dlib + lbp-var + Canny1986  ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
can_img = can_img[0:183]; can_lab = can_lab[0:183];    
can_img_lt = []; can_lab_lt = [];
for idx, img in enumerate(can_img):

  # Augmentations
  can_flp = trf_flp(image=img);                            # Horizontal flip 
  can_rot = trf_rot(image=img);                            # Rotation
  can_fr  = trf_fr( image=img);                            # Flip et rotation
  can_ssr = trf_ssr(image=img);                            # Shift,scale et rotation
  
  # Liste des images
  can_img_lt.append(img); 
  can_img_lt.append(can_flp["image"]); can_img_lt.append(can_rot["image"]);
  can_img_lt.append(can_fr["image"]);  can_img_lt.append(can_ssr["image"]);

  # Liste des étiquettes
  can_lab_lt.append(can_lab[idx]);
  can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]);
  can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]);


print("************************************** ");
print(" Aug. avec dlib + lbp-var + Canny1986 + redimensionnement  ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
red_img = red_img[0:183]; red_lab = red_lab[0:183];    
red_img_lt = []; red_lab_lt = [];
for idx, img in enumerate(red_img):

  # Augmentations
  red_flp = trf_flp(image=img);                            # Horizontal flip 
  red_rot = trf_rot(image=img);                            # Rotation
  red_fr  = trf_fr( image=img);                            # Flip et rotation
  red_ssr = trf_ssr(image=img);                            # Shift,scale et rotation
  
  # Liste des images
  red_img_lt.append(img); 
  red_img_lt.append(red_flp["image"]); red_img_lt.append(red_rot["image"]);
  red_img_lt.append(red_fr["image"]);  red_img_lt.append(red_ssr["image"]);

  # Liste des étiquettes
  red_lab_lt.append(red_lab[idx]);
  red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]);
  red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]);

#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Sauvegarder les données augmentées    ");
print("************************************** ");
# Sauvegarder images et étiquettes avec dlib + augmentation
np.save("../../db_transfigure/jaffe_dlib128_aug4_img.npy", dlib_img_lt);
np.save("../../db_transfigure/jaffe_dlib128_aug4_lab.npy", dlib_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + augmentation
np.save("../../db_transfigure/jaffe_dlib_var128_aug4_img.npy", var_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var128_aug4_lab.npy", var_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + Canny1986 + augmentation
np.save("../../db_transfigure/jaffe_dlib_var_can128_aug4_img.npy", can_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var_can128_aug4_lab.npy", can_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + Canny1986 + redimensionneme nt + augmentation
np.save("../../db_transfigure/jaffe_dlib_var_can_red56_aug4_img.npy", red_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var_can_red56_aug4_lab.npy", red_lab_lt);


print("************************************** ");
print(" Charger les données augmentées        ");
print("************************************** ");
# Charger image + pré-traitement avec dlib + augmentation
dlib_img = np.load("../../db_transfigure/jaffe_dlib128_aug4_img.npy");
dlib_lab = np.load("../../db_transfigure/jaffe_dlib128_aug4_lab.npy");
 
# Charger image + pré-traitement  avec dlib + lbp-var + augmentation
var_img = np.load("../../db_transfigure/jaffe_dlib_var128_aug4_img.npy");
var_lab = np.load("../../db_transfigure/jaffe_dlib_var128_aug4_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986 + augmentation
can_img = np.load("../../db_transfigure/jaffe_dlib_var_can128_aug4_img.npy");
can_lab = np.load("../../db_transfigure/jaffe_dlib_var_can128_aug4_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986 + redimensionnent + augmentation
red_img = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_aug4_img.npy");
red_lab = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_aug4_lab.npy");

#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Générer le pdf avec des exemples      ");
print("************************************** ");
# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];

# Fonction qui sert à générer des exemples d'augmentation 
with PdfPages("exemples_augmentation4.pdf") as pdf:

  # Augmentation avec dlib
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Reference");                ax1.imshow(dlib_img[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Flip");                     ax2.imshow(dlib_img[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Rotation");                 ax3.imshow(dlib_img[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Flip et Rotation");         ax4.imshow(dlib_img[3], cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Shift, scale et Rotation"); ax5.imshow(dlib_img[4], cmap="gray"); 
  fig.suptitle("Augmentation avec Dlib. Expression: "+ fer_eti[dlib_lab[0]], fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Reference");                ax1.imshow(var_img[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Flip");                     ax2.imshow(var_img[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Rotation");                 ax3.imshow(var_img[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Flip et Rotation");         ax4.imshow(var_img[3], cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Shift, scale et Rotation"); ax5.imshow(var_img[4], cmap="gray"); 
  fig.suptitle("Augmentation avec Dlib + lbp-var. Expression: "+ fer_eti[var_lab[0]], fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var + canny1986
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Reference");                ax1.imshow(can_img[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Flip");                     ax2.imshow(can_img[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Rotation");                 ax3.imshow(can_img[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Flip et Rotation");         ax4.imshow(can_img[3], cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Shift, scale et Rotation"); ax5.imshow(can_img[4], cmap="gray"); 
  fig.suptitle("Augmentation avec Dlib + lbp-var + Canny1986. Expression: " + fer_eti[can_lab[0]], fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var + canny1986 + redimensionnement
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("Reference");                ax1.imshow(red_img[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("Flip");                     ax2.imshow(red_img[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("Rotation");                 ax3.imshow(red_img[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("Flip et Rotation");         ax4.imshow(red_img[3], cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("Shift, scale et Rotation"); ax5.imshow(red_img[4], cmap="gray"); 
  fig.suptitle("Augmentation avec Dlib + lbp-var + Canny1986 + redimensionnement. Expression: " + fer_eti[red_lab[0]], fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

#sys.exit(0);                                              # Terminer l'execution

print("************************************** ");
print(" Imprimer quelques valeurs             ");
print("************************************** ");

print("\n Image avec Dlib + augmentation      ");
print("************************************** ");
print("Image de reference:                  \n", dlib_img[0]); 
print("Image après flip:                    \n", dlib_img[1]); 
print("Image après rotation:                \n", dlib_img[2]); 
print("Image après flip et rotation:        \n", dlib_img[3]); 
print("Image après shift scale et rotation: \n", dlib_img[4]); 
print("Nombre et Tailles des images \n", dlib_img.shape );

print("\n Image avec Dlib et lbp-var + augmentation ");
print("************************************** ");
print("Image de reference:                  \n", var_img[0]); 
print("Image après flip:                    \n", var_img[1]); 
print("Image après rotation:                \n", var_img[2]); 
print("Image après flip et rotation:        \n", var_img[3]); 
print("Image après shift scale et rotation: \n", var_img[4]); 
print("Nombre et Tailles des images \n", var_img.shape );

print("\n Image avec Dlib, lbp-var et Canny1986 + augmentation ");
print("************************************** ");
print("Image de reference:                  \n", can_img[0]); 
print("Image après flip:                    \n", can_img[1]); 
print("Image après rotation:                \n", can_img[2]); 
print("Image après flip et rotation:        \n", can_img[3]); 
print("Image après shift scale et rotation: \n", can_img[4]); 
print("Nombre et Tailles des images \n", can_img.shape );

print("\n Image avec Dlib, lbp-var, Canny1986 et redimensionnement + augmentation ");
print("************************************** ");
print("Image de reference:                  \n", red_img[0]); 
print("Image après flip:                    \n", red_img[1]); 
print("Image après rotation:                \n", red_img[2]); 
print("Image après flip et rotation:        \n", red_img[3]); 
print("Image après shift scale et rotation: \n", red_img[4]); 
print("Nombre et Tailles des images \n", red_img.shape );
#sys.exit(0);                                              # Terminer l'execution


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
