# -*- coding: utf-8 -*-
## 
# @file    01_dlib_var_canny_redim_aug15.py
# @brief   Augmentation des données sur Jaffe
# @details Ce module fait l'augmentation des données sur les
#          différentes sorties des étapes de pré-traitement
#          de l'ensemble des données Jaffe (i.e. dlib, dlib + var,
#          dlib + var + canny1986 (50, 100), 
#          dlib + var + canny1986 (50, 100) + redimensionnement 56 x 56 pixels).
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/02/12
# @version 0.1
"""@package docstring
"""

import albumentations as A
import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from matplotlib.backends.backend_pdf import PdfPages
from tensorflow import keras


print("************************************** ");
print(" Charger les transfigurations          ");
print("************************************** ");
# Charger image + pré-traitement avec dlib
dlib_img = np.load("../../db_transfigure/jaffe_dlib128_img.npy");
dlib_lab = np.load("../../db_transfigure/jaffe_dlib128_lab.npy");
 
# Charger image + pré-traitement  avec dlib + lbp-var
var_img = np.load("../../db_transfigure/jaffe_dlib_var128_img.npy");
var_lab = np.load("../../db_transfigure/jaffe_dlib_var128_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986
can_img = np.load("../../db_transfigure/jaffe_dlib_var_can128_img.npy");
can_lab = np.load("../../db_transfigure/jaffe_dlib_var_can128_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986 + redimensionnent
red_img = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_img.npy");
red_lab = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_lab.npy");


print("************************************** ");
print(" Définir les augmentations             ");
print("************************************** ");
# Définir les pipelines d'augmentation
trf_sh  = A.ShiftScaleRotate(                              # --> Shift
            shift_limit=0.2, scale_limit=0,
            rotate_limit=0, interpolation=4,               # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
 
trf_sc  = A.ShiftScaleRotate(                              # --> Scale
            shift_limit=0, scale_limit=0.2,
            rotate_limit=0, interpolation=4,               # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0

trf_ro  = A.ShiftScaleRotate(                              # --> Rotation
            shift_limit=0, scale_limit=0,
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
 
trf_ss  = A.ShiftScaleRotate(                              # --> Shift, scale
            shift_limit=0.2, scale_limit=0.2,
            rotate_limit=0, interpolation=4,               # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
  
trf_sr  = A.ShiftScaleRotate(                              # --> Shift, rotation
            shift_limit=0.2, scale_limit=0,
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
 
trf_cr  = A.ShiftScaleRotate(                              # --> Scale, rotation
            shift_limit=0, scale_limit=0.2,
            rotate_limit=20, interpolation=4,              # + Lanczos4
            border_mode=0, value=0, p=0.99);               # + border=0
 
trf_ssr  = A.ShiftScaleRotate(                             # --> Shift,scale,rotation
             shift_limit=0.2, scale_limit=0.2,
             rotate_limit=20, interpolation=4,              # + Lanczos4
             border_mode=0, value=0, p=0.99);               # + border=0
 
# même augmentation pour l'image retourné/flip
trf_flp = A.HorizontalFlip(p=0.99);                        # --> Horizontal flip
 
trf_fsh = A.Compose([                                      # --> Flip, shift   
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift
              shift_limit=0.2, scale_limit=0,
              rotate_limit=0, interpolation=4,             # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fsc = A.Compose([                                      # --> Flip, scale   
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Scale
              shift_limit=0, scale_limit=0.2,
              rotate_limit=0, interpolation=4,             # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fro = A.Compose([                                      # --> Flip, rotation   
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Rotation
              shift_limit=0, scale_limit=0,
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fss = A.Compose([                                      # --> Flip, shift, scale  
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift, scale
              shift_limit=0.2, scale_limit=0.2,
              rotate_limit=0, interpolation=4,             # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fsr = A.Compose([                                      # --> Flip,shift,rotation 
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Shift, rotation
              shift_limit=0.2, scale_limit=0,
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fcr = A.Compose([                                      # --> Flip,scale,rotation 
            A.HorizontalFlip(p=0.99),                      # -- Retourner  
            A.ShiftScaleRotate(                            # -- Scale, rotation
              shift_limit=0, scale_limit=0.2,
              rotate_limit=20, interpolation=4,            # + Lanczos4
              border_mode=0, value=0, p=0.99) ]);          # + border=0
 
trf_fssr = A.Compose([                                     # --> Flip,scale,shift,rot
             A.HorizontalFlip(p=0.99),                      # -- Retourner  
             A.ShiftScaleRotate(                            # -- Shift,scale,rotation
               shift_limit=0.2, scale_limit=0.2,
               rotate_limit=20, interpolation=4,            # + Lanczos4
               border_mode=0, value=0, p=0.99) ]);          # + border=0
 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Augmentation avec dlib                ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
dlib_img = dlib_img[0:183]; dlib_lab = dlib_lab[0:183];    
dlib_img_lt = []; dlib_lab_lt = [];
for idx, img in enumerate(dlib_img):

  # Augmentations
  dlib_sh  = trf_sh(image=img);                            # Shift 
  dlib_sc  = trf_sc(image=img);                            # Scale 
  dlib_ro  = trf_ro(image=img);                            # Rotation
  dlib_ss  = trf_ss(image=img);                            # Shift, scale 
  dlib_sr  = trf_sr(image=img);                            # Shift, rotation 
  dlib_cr  = trf_cr(image=img);                            # Scale, rotation
  dlib_ssr = trf_ssr(image=img);                           # Shift, scale, rotation
  dlib_flp = trf_flp(image=img);                           # Flip
  dlib_fsh = trf_fsh(image=img);                           # Flip shift 
  dlib_fsc = trf_fsc(image=img);                           # Flip scale 
  dlib_fro = trf_fro(image=img);                           # Flip rotation
  dlib_fss = trf_fss(image=img);                           # Flip shift, scale 
  dlib_fsr = trf_fsr(image=img);                           # Flip shift, rotation 
  dlib_fcr = trf_fcr(image=img);                           # Flip scale, rotation
  dlib_fssr= trf_fssr(image=img);                          # Flip shift, scale, rotation

  
  # Liste des images
  dlib_img_lt.append(img);               dlib_img_lt.append(dlib_sh["image"]);  dlib_img_lt.append(dlib_sc["image"]);  dlib_img_lt.append(dlib_ro["image"]);
  dlib_img_lt.append(dlib_ss["image"]);  dlib_img_lt.append(dlib_sr["image"]);  dlib_img_lt.append(dlib_cr["image"]);  dlib_img_lt.append(dlib_ssr["image"]); 
  dlib_img_lt.append(dlib_flp["image"]); dlib_img_lt.append(dlib_fsh["image"]); dlib_img_lt.append(dlib_fsc["image"]); dlib_img_lt.append(dlib_fro["image"]);
  dlib_img_lt.append(dlib_fss["image"]); dlib_img_lt.append(dlib_fsr["image"]); dlib_img_lt.append(dlib_fcr["image"]); dlib_img_lt.append(dlib_fssr["image"]);

  # Liste des étiquettes
  dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]);
  dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]);
  dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]);
  dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]); dlib_lab_lt.append(dlib_lab[idx]);

#print("len du liste: \n",   len(dlib_img_lt) );
#print("shape du liste: \n", dlib_img_lt[0].shape );
#print("étiquettes: \n",     dlib_lab_lt);


print("************************************** ");
print(" Augmentation avec dlib + lbp-var      ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
var_img = var_img[0:183]; var_lab = var_lab[0:183];    
var_img_lt = []; var_lab_lt = [];
for idx, img in enumerate(var_img):

  # Augmentations
  var_sh  = trf_sh(image=img);                             # Shift 
  var_sc  = trf_sc(image=img);                             # Scale 
  var_ro  = trf_ro(image=img);                             # Rotation
  var_ss  = trf_ss(image=img);                             # Shift, scale 
  var_sr  = trf_sr(image=img);                             # Shift, rotation 
  var_cr  = trf_cr(image=img);                             # Scale, rotation
  var_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  var_flp = trf_flp(image=img);                            # Flip
  var_fsh = trf_fsh(image=img);                            # Flip shift 
  var_fsc = trf_fsc(image=img);                            # Flip scale 
  var_fro = trf_fro(image=img);                            # Flip rotation
  var_fss = trf_fss(image=img);                            # Flip shift, scale 
  var_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  var_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  var_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation

  
  # Liste des images
  var_img_lt.append(img);              var_img_lt.append(var_sh["image"]);  var_img_lt.append(var_sc["image"]);  var_img_lt.append(var_ro["image"]);
  var_img_lt.append(var_ss["image"]);  var_img_lt.append(var_sr["image"]);  var_img_lt.append(var_cr["image"]);  var_img_lt.append(var_ssr["image"]); 
  var_img_lt.append(var_flp["image"]); var_img_lt.append(var_fsh["image"]); var_img_lt.append(var_fsc["image"]); var_img_lt.append(var_fro["image"]);
  var_img_lt.append(var_fss["image"]); var_img_lt.append(var_fsr["image"]); var_img_lt.append(var_fcr["image"]); var_img_lt.append(var_fssr["image"]);

  # Liste des étiquettes
  var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]);
  var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]);
  var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]);
  var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]); var_lab_lt.append(var_lab[idx]);
  

print("************************************** ");
print(" Aug. avec dlib + lbp-var + Canny1986  ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
can_img = can_img[0:183]; can_lab = can_lab[0:183];    
can_img_lt = []; can_lab_lt = [];
for idx, img in enumerate(can_img):

  # Augmentations
  can_sh  = trf_sh(image=img);                             # Shift 
  can_sc  = trf_sc(image=img);                             # Scale 
  can_ro  = trf_ro(image=img);                             # Rotation
  can_ss  = trf_ss(image=img);                             # Shift, scale 
  can_sr  = trf_sr(image=img);                             # Shift, rotation 
  can_cr  = trf_cr(image=img);                             # Scale, rotation
  can_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  can_flp = trf_flp(image=img);                            # Flip
  can_fsh = trf_fsh(image=img);                            # Flip shift 
  can_fsc = trf_fsc(image=img);                            # Flip scale 
  can_fro = trf_fro(image=img);                            # Flip rotation
  can_fss = trf_fss(image=img);                            # Flip shift, scale 
  can_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  can_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  can_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation

  
  # Liste des images
  can_img_lt.append(img);              can_img_lt.append(can_sh["image"]);  can_img_lt.append(can_sc["image"]);  can_img_lt.append(can_ro["image"]);
  can_img_lt.append(can_ss["image"]);  can_img_lt.append(can_sr["image"]);  can_img_lt.append(can_cr["image"]);  can_img_lt.append(can_ssr["image"]); 
  can_img_lt.append(can_flp["image"]); can_img_lt.append(can_fsh["image"]); can_img_lt.append(can_fsc["image"]); can_img_lt.append(can_fro["image"]);
  can_img_lt.append(can_fss["image"]); can_img_lt.append(can_fsr["image"]); can_img_lt.append(can_fcr["image"]); can_img_lt.append(can_fssr["image"]);

  # Liste des étiquettes
  can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]);
  can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]);
  can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]);
  can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]); can_lab_lt.append(can_lab[idx]);


print("************************************** ");
print(" Aug. avec dlib + lbp-var + Canny1986 + redimensionnement  ");
print("************************************** ");
# Augmentation uniquement sur l'ensemble d'entraînement 
red_img = red_img[0:183]; red_lab = red_lab[0:183];    
red_img_lt = []; red_lab_lt = [];
for idx, img in enumerate(red_img):

  # Augmentations
  red_sh  = trf_sh(image=img);                             # Shift 
  red_sc  = trf_sc(image=img);                             # Scale 
  red_ro  = trf_ro(image=img);                             # Rotation
  red_ss  = trf_ss(image=img);                             # Shift, scale 
  red_sr  = trf_sr(image=img);                             # Shift, rotation 
  red_cr  = trf_cr(image=img);                             # Scale, rotation
  red_ssr = trf_ssr(image=img);                            # Shift, scale, rotation
  red_flp = trf_flp(image=img);                            # Flip
  red_fsh = trf_fsh(image=img);                            # Flip shift 
  red_fsc = trf_fsc(image=img);                            # Flip scale 
  red_fro = trf_fro(image=img);                            # Flip rotation
  red_fss = trf_fss(image=img);                            # Flip shift, scale 
  red_fsr = trf_fsr(image=img);                            # Flip shift, rotation 
  red_fcr = trf_fcr(image=img);                            # Flip scale, rotation
  red_fssr= trf_fssr(image=img);                           # Flip shift, scale, rotation

  
  # Liste des images
  red_img_lt.append(img);              red_img_lt.append(red_sh["image"]);  red_img_lt.append(red_sc["image"]);  red_img_lt.append(red_ro["image"]);
  red_img_lt.append(red_ss["image"]);  red_img_lt.append(red_sr["image"]);  red_img_lt.append(red_cr["image"]);  red_img_lt.append(red_ssr["image"]); 
  red_img_lt.append(red_flp["image"]); red_img_lt.append(red_fsh["image"]); red_img_lt.append(red_fsc["image"]); red_img_lt.append(red_fro["image"]);
  red_img_lt.append(red_fss["image"]); red_img_lt.append(red_fsr["image"]); red_img_lt.append(red_fcr["image"]); red_img_lt.append(red_fssr["image"]);

  # Liste des étiquettes
  red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]);
  red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]);
  red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]);
  red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]); red_lab_lt.append(red_lab[idx]);

#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Sauvegarder les données augmentées    ");
print("************************************** ");
# Sauvegarder images et étiquettes avec dlib + augmentation
np.save("../../db_transfigure/jaffe_dlib128_aug15_img.npy", dlib_img_lt);
np.save("../../db_transfigure/jaffe_dlib128_aug15_lab.npy", dlib_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + augmentation
np.save("../../db_transfigure/jaffe_dlib_var128_aug15_img.npy", var_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var128_aug15_lab.npy", var_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + Canny1986 + augmentation
np.save("../../db_transfigure/jaffe_dlib_var_can128_aug15_img.npy", can_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var_can128_aug15_lab.npy", can_lab_lt);

# Sauvegarder images et étiquettes avec dlib + lbp-var + Canny1986 + redimensionneme nt + augmentation
np.save("../../db_transfigure/jaffe_dlib_var_can_red56_aug15_img.npy", red_img_lt);
np.save("../../db_transfigure/jaffe_dlib_var_can_red56_aug15_lab.npy", red_lab_lt);


print("************************************** ");
print(" Charger les données augmentées        ");
print("************************************** ");
# Charger image + pré-traitement avec dlib + augmentation
dlib_img = np.load("../../db_transfigure/jaffe_dlib128_aug15_img.npy");
dlib_lab = np.load("../../db_transfigure/jaffe_dlib128_aug15_lab.npy");
 
# Charger image + pré-traitement  avec dlib + lbp-var + augmentation
var_img = np.load("../../db_transfigure/jaffe_dlib_var128_aug15_img.npy");
var_lab = np.load("../../db_transfigure/jaffe_dlib_var128_aug15_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986 + augmentation
can_img = np.load("../../db_transfigure/jaffe_dlib_var_can128_aug15_img.npy");
can_lab = np.load("../../db_transfigure/jaffe_dlib_var_can128_aug15_lab.npy");
 
# Charger image + pré-traitement avec dlib + lbp-var + canny1986 + redimensionnent + augmentation
red_img = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_aug15_img.npy");
red_lab = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_aug15_lab.npy");

#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Générer le pdf avec des exemples      ");
print("************************************** ");
# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];

# Fonction qui sert à générer des exemples d'augmentation 
with PdfPages("exemples_augmentation15.pdf") as pdf:

  # Augmentation avec dlib
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(dlib_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(dlib_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(dlib_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(dlib_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(dlib_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(dlib_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(dlib_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(dlib_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(dlib_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(dlib_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(dlib_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(dlib_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(dlib_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(dlib_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(dlib_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(dlib_img[15],cmap="gray");
  fig.suptitle("Augmentation avec Dlib. Expression: "+ fer_eti[dlib_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(var_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(var_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(var_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(var_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(var_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(var_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(var_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(var_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(var_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(var_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(var_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(var_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(var_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(var_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(var_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(var_img[15],cmap="gray");
  fig.suptitle("Augmentation avec Dlib + lbp-var. Expression: "+ fer_eti[var_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var + canny1986
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(can_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(can_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(can_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(can_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(can_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(can_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(can_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(can_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(can_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(can_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(can_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(can_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(can_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(can_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(can_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(can_img[15],cmap="gray");
  fig.suptitle("Augmentation avec Dlib + lbp-var + Canny1986. Expression: " + fer_eti[can_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # Augmentation avec dlib + lbp-var + canny1986 + redimensionnement
  fig = plt.figure(figsize=(8, 9));
  ax1 = fig.add_subplot(4, 4, 1); ax1.set_title("origen");                 ax1.imshow(red_img[0],  cmap="gray");
  ax2 = fig.add_subplot(4, 4, 2); ax2.set_title("shift");                  ax2.imshow(red_img[1],  cmap="gray");
  ax3 = fig.add_subplot(4, 4, 3); ax3.set_title("scale");                  ax3.imshow(red_img[2],  cmap="gray");
  ax4 = fig.add_subplot(4, 4, 4); ax4.set_title("rotation");               ax4.imshow(red_img[3],  cmap="gray");
  ax5 = fig.add_subplot(4, 4, 5); ax5.set_title("shift scale");            ax5.imshow(red_img[4],  cmap="gray");
  ax6 = fig.add_subplot(4, 4, 6); ax6.set_title("shift rotation");         ax6.imshow(red_img[5],  cmap="gray");
  ax7 = fig.add_subplot(4, 4, 7); ax7.set_title("scale rotation");         ax7.imshow(red_img[6],  cmap="gray");
  ax8 = fig.add_subplot(4, 4, 8); ax8.set_title("shift scale rotation");   ax8.imshow(red_img[7],  cmap="gray");
  ax9 = fig.add_subplot(4, 4, 9); ax9.set_title("flip");                   ax9.imshow(red_img[8],  cmap="gray");
  ax10= fig.add_subplot(4, 4,10); ax10.set_title("flip shift");            ax10.imshow(red_img[9], cmap="gray");
  ax11= fig.add_subplot(4, 4,11); ax11.set_title("flip scale");            ax11.imshow(red_img[10],cmap="gray");
  ax12= fig.add_subplot(4, 4,12); ax12.set_title("flip rotation");         ax12.imshow(red_img[11],cmap="gray");
  ax13= fig.add_subplot(4, 4,13); ax13.set_title("flip shift scale");      ax13.imshow(red_img[12],cmap="gray");
  ax14= fig.add_subplot(4, 4,14); ax14.set_title("flip shift rotation");   ax14.imshow(red_img[13],cmap="gray");
  ax15= fig.add_subplot(4, 4,15); ax15.set_title("flip scale rotation");   ax15.imshow(red_img[14],cmap="gray");
  ax16= fig.add_subplot(4, 4,16); ax16.set_title("flip scale shift rota"); ax16.imshow(red_img[15],cmap="gray");
  fig.suptitle("Augmentation avec Dlib + lbp-var + Canny1986 + redimensionnement. Expression: " + fer_eti[red_lab[0]], fontsize=14); plt.tight_layout();
  fig.subplots_adjust(top=0.90);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

#sys.exit(0);                                              # Terminer l'execution

print("************************************** ");
print(" Imprimer quelques valeurs             ");
print("************************************** ");

print("\n Image avec Dlib + augmentation      ");
print("************************************** ");
print("Image après shift:                  \n", dlib_img[1] );
print("Image après scale:                  \n", dlib_img[2] );
print("Image après rotation:               \n", dlib_img[3] );
print("Image après shift, scale:           \n", dlib_img[4] );
print("Image après shift, rotation:        \n", dlib_img[5] );
print("Image après scale, rotation:        \n", dlib_img[6] );
print("Image après shift, scale, rotation: \n", dlib_img[7] );
print("Image après flip:                       \n", dlib_img[8]  );
print("Image après flip shift:                 \n", dlib_img[9]  );
print("Image après flip scale:                 \n", dlib_img[10] );
print("Image après flip rotation:              \n", dlib_img[11] );
print("Image après flip shift, scale:          \n", dlib_img[12] );
print("Image après flip shift, rotation:       \n", dlib_img[13] );
print("Image après flip scale, rotation:       \n", dlib_img[14] );
print("Image après flip shift, scale, rotation:\n", dlib_img[15] );
print("Nombre et Tailles des images \n", dlib_img.shape );
#sys.exit(0);                                              # Terminer l'execution


print("\n Image avec Dlib et lbp-var + augmentation ");
print("************************************** ");
print("Image après shift:                  \n", var_img[1] );
print("Image après scale:                  \n", var_img[2] );
print("Image après rotation:               \n", var_img[3] );
print("Image après shift, scale:           \n", var_img[4] );
print("Image après shift, rotation:        \n", var_img[5] );
print("Image après scale, rotation:        \n", var_img[6] );
print("Image après shift, scale, rotation: \n", var_img[7] );
print("Image après flip:                       \n", var_img[8]  );
print("Image après flip shift:                 \n", var_img[9]  );
print("Image après flip scale:                 \n", var_img[10] );
print("Image après flip rotation:              \n", var_img[11] );
print("Image après flip shift, scale:          \n", var_img[12] );
print("Image après flip shift, rotation:       \n", var_img[13] );
print("Image après flip scale, rotation:       \n", var_img[14] );
print("Image après flip shift, scale, rotation:\n", var_img[15] );
print("Nombre et Tailles des images \n", var_img.shape );
#sys.exit(0);                                              # Terminer l'execution

print("\n Image avec Dlib, lbp-var et Canny1986 + augmentation ");
print("************************************** ");
print("Image après shift:                  \n", can_img[1] );
print("Image après scale:                  \n", can_img[2] );
print("Image après rotation:               \n", can_img[3] );
print("Image après shift, scale:           \n", can_img[4] );
print("Image après shift, rotation:        \n", can_img[5] );
print("Image après scale, rotation:        \n", can_img[6] );
print("Image après shift, scale, rotation: \n", can_img[7] );
print("Image après flip:                       \n", can_img[8]  );
print("Image après flip shift:                 \n", can_img[9]  );
print("Image après flip scale:                 \n", can_img[10] );
print("Image après flip rotation:              \n", can_img[11] );
print("Image après flip shift, scale:          \n", can_img[12] );
print("Image après flip shift, rotation:       \n", can_img[13] );
print("Image après flip scale, rotation:       \n", can_img[14] );
print("Image après flip shift, scale, rotation:\n", can_img[15] );
print("Nombre et Tailles des images \n", can_img.shape );
#sys.exit(0);                                              # Terminer l'execution

print("\n Image avec Dlib, lbp-var, Canny1986 et redimensionnement + augmentation ");
print("************************************** ");
print("Image après shift:                  \n", red_img[1] );
print("Image après scale:                  \n", red_img[2] );
print("Image après rotation:               \n", red_img[3] );
print("Image après shift, scale:           \n", red_img[4] );
print("Image après shift, rotation:        \n", red_img[5] );
print("Image après scale, rotation:        \n", red_img[6] );
print("Image après shift, scale, rotation: \n", red_img[7] );
print("Image après flip:                       \n", red_img[8]  );
print("Image après flip shift:                 \n", red_img[9]  );
print("Image après flip scale:                 \n", red_img[10] );
print("Image après flip rotation:              \n", red_img[11] );
print("Image après flip shift, scale:          \n", red_img[12] );
print("Image après flip shift, rotation:       \n", red_img[13] );
print("Image après flip scale, rotation:       \n", red_img[14] );
print("Image après flip shift, scale, rotation:\n", red_img[15] );
print("Nombre et Tailles des images \n", red_img.shape );
#sys.exit(0);                                              # Terminer l'execution


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
