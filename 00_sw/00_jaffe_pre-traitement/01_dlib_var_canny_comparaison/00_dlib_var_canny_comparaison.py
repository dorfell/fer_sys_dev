# -*- coding: utf-8 -*-
## 
# @file    00_dlib_var_canny_comparaison.py
# @brief   Détection de contours
# @details Ce module fait la détection de contours sur les images qui sortent
#          de l'étape LBP avec le méthode var. En plus, il fait la comparaison
#          de différents seuils de filtre Canny proposé en 1986.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/01/21
# @version 0.1
"""@package docstring
"""

import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from skimage.feature import local_binary_pattern as lbp_fun
from mes_fonctions import *

from matplotlib.backends.backend_pdf import PdfPages


print("************************************** ");
print(" Charger l'ensamble de données         ");
print("************************************** ");
# Localisation de l'ensamble de données (db)
loc_db = "../../db/jaffedbase_petit/"; 

# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];

# Lire la base de données
img_dat, img_lab = read_data(loc_db);
#print(img_dat, img_dat.shape,  img_lab);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Pré-traitement des données avec Dlib  ");
print("************************************** ");
# Pré-traitement des données
img_dat_pre = preprocessing(img_dat);                      # taille (128, 128)
#print("print img_dat_resized", img_dat_resized, len(img_dat_resized), type(img_dat_resized),  img_lab);
print("Image après pré-traitement: \n", img_dat_pre[0], img_dat_pre[0].dtype); 
plt.imshow(img_dat_pre[0], cmap="gray"); 
plt.title("Image pré-traité avec Dlib");  plt.show();
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Motifs Binaires Locaux (LBP)          ");
print("************************************** ");
# Paramètres des Motifs binaires locaux (LBP) 
radius = 3;                                                # Rayon de cercle
n_points = 8 * radius;                                     # nombre de points voisins circulairement symétriques.
method= "var";                                             # Méthode pour la determination du motifs. E.g. default, ror, uniform, nri_uniform, var.

lbp_var_lt = [];
for img in img_dat_pre:
  lbp_var = lbp_fun(img, n_points, radius, method);        # Calculer le LBP.
  lbp_var_lt.append(lbp_var);

plt.imshow(lbp_var, cmap="gray");                          # Imprimer quelques images
plt.title("Image avec lbp, méthode= var"); plt.show();
print("Image après lbp var: \n", lbp_var); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" À nombre entier int8                  ");
print("************************************** ");
lbp_var_int = lbp_var_lt[0];

lbp_var_int = lbp_var_int / lbp_var_int.max();             # normalizer img [0, 1].
lbp_var_int = 255 * lbp_var_int;                           # À échelle 0 - 255
lbp_var_int = np.rint(lbp_var_int);                        # Arrondir à entier
lbp_var_int = lbp_var_int.astype(np.uint8);                # cast à uint8
print("valeurs du lbp_var_int:\n ", lbp_var_int.shape); print(lbp_var_int.shape);

plt.imshow(lbp_var_int, cmap="gray");                      # Imprimer quelques images
plt.title("Image avec lbp var en entier 0 à 255");  plt.show();
print("Image avec lbp var en entier 0 à 255: \n", lbp_var_int);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Détection des contours                ");
print("************************************** ");
# Paramètres de la détection de contours: seuils --> thresholds: minVal, maxVal
# ex: img_Canny =  cv2.Canny(img, minVal, maxVal); 
# can =  cv2.Canny(lbp_var_int, minVal, maxVal); 
#plt.imshow(can, cmap="gray");                             # Imprimer quelques images
#plt.title("Image avec détection de contours");  plt.show();
#print("Image après détection de contours: \n", can); 

# minVal 0 
can0_50  = cv2.Canny(lbp_var_int, 0, 50);  can0_100 = cv2.Canny(lbp_var_int, 0, 100); 
can0_150 = cv2.Canny(lbp_var_int, 0, 150); can0_200 = cv2.Canny(lbp_var_int, 0, 200); 
can0_255 = cv2.Canny(lbp_var_int, 0, 255); 

# minVal 50 
can50_100 = cv2.Canny(lbp_var_int, 50, 100); can50_150 = cv2.Canny(lbp_var_int, 50, 150); 
can50_200 = cv2.Canny(lbp_var_int, 50, 200); can50_255 = cv2.Canny(lbp_var_int, 50, 255); 

# minVal 100 
can100_150 = cv2.Canny(lbp_var_int, 100, 150);  can100_200 = cv2.Canny(lbp_var_int, 100, 200); 
can100_255 = cv2.Canny(lbp_var_int, 100, 255);

# minVal 150 
can150_200 = cv2.Canny(lbp_var_int, 150, 200);  can150_255 = cv2.Canny(lbp_var_int,150, 255); 

# minVal 200 
can200_255 = cv2.Canny(lbp_var_int, 200, 255);


# Fonction qui sert à comparer la détection des contours avec différentes seuils 
with PdfPages("comparaison_Canny.pdf")  as pdf:

  # minVal 0 
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 3, 1); ax1.set_title("(0,  50)"); ax1.imshow(can0_50,  cmap="gray"); 
  ax2 = fig.add_subplot(2, 3, 2); ax2.set_title("(0, 100)"); ax2.imshow(can0_100, cmap="gray"); 
  ax3 = fig.add_subplot(2, 3, 3); ax3.set_title("(0, 150)"); ax3.imshow(can0_150, cmap="gray"); 
  ax4 = fig.add_subplot(2, 3, 4); ax4.set_title("(0, 200)"); ax4.imshow(can0_200, cmap="gray"); 
  ax5 = fig.add_subplot(2, 3, 5); ax5.set_title("(0, 255)"); ax5.imshow(can0_255, cmap="gray"); 
  fig.suptitle("Canny avec minVal = 0", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # minVal 50 
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title("(50, 100)"); ax1.imshow(can50_100,  cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title("(50, 150)"); ax2.imshow(can50_150, cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title("(50, 200)"); ax3.imshow(can50_200, cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title("(50, 200)"); ax4.imshow(can50_255, cmap="gray"); 
  fig.suptitle("Canny avec minVal = 50", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # minVal 100 
  fig = plt.figure();
  ax1 = fig.add_subplot(1, 3, 1); ax1.set_title("(100, 150)"); ax1.imshow(can100_150, cmap="gray"); 
  ax2 = fig.add_subplot(1, 3, 2); ax2.set_title("(100, 200)"); ax2.imshow(can100_200, cmap="gray"); 
  ax3 = fig.add_subplot(1, 3, 3); ax3.set_title("(100, 255)"); ax3.imshow(can100_255, cmap="gray"); 
  fig.suptitle("Canny avec minVal = 100", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # minVal 150 
  fig = plt.figure();
  ax1 = fig.add_subplot(1, 2, 1); ax1.set_title("(150, 200)"); ax1.imshow(can150_200,  cmap="gray"); 
  ax2 = fig.add_subplot(1, 2, 2); ax2.set_title("(150, 255)"); ax2.imshow(can150_255, cmap="gray"); 
  fig.suptitle("Canny avec minVal = 150", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # minVal 200 
  fig = plt.figure();
  ax1 = fig.add_subplot(1, 1, 1); ax1.set_title("(200, 255)"); ax1.imshow(can200_255,  cmap="gray"); 
  fig.suptitle("Canny avec minVal = 200", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

#sys.exit(0);                                              # Terminer l'execution


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
