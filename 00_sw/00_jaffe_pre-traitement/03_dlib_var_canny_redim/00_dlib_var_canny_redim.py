# -*- coding: utf-8 -*-
## 
# @file    00_dlib_var_cannny_redim.py
# @brief   Pré-traitement du Jaffe avec Dlib + lbp-var + canny + redimensionnement
# @details Ce module sauvegardé l'ensemble Jaffe pré-traité avec Dlib, le
#          motif locaux binaire avec le méthode VAR, la détection de contours
#          avec Canny 1986 et les seuils: minVal = 50, maxVal = 100, et le
#          redimensionnement de 128 x 128 pixels à 56 x 56 pixels avec 
#          l'interpolation Lanczos4.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/02/01
# @version 0.1
"""@package docstring
"""

import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from skimage.feature import local_binary_pattern as lbp_fun
from mes_fonctions import *

from matplotlib.backends.backend_pdf import PdfPages


print("************************************** ");
print(" Charger l'ensamble de données         ");
print("************************************** ");
# Localisation de l'ensamble de données (db)
#loc_db = "../../db/jaffedbase_petit/";                    # Seulment 4 images. 
loc_db = "../../db/jaffedbase/";                           # 213 images. 

# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];

# Lire la base de données
img_dat, img_lab = read_data(loc_db);
#print(img_dat, img_dat.shape,  img_lab);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Pré-traitement des données avec Dlib  ");
print("************************************** ");
# Pré-traitement des données
img_dat_pre = preprocessing(img_dat);                      # taille (128, 128)
#print("print img_dat_resized", img_dat_resized, len(img_dat_resized), type(img_dat_resized),  img_lab);
print("Image après pré-traitement: \n", img_dat_pre[0], img_dat_pre[0].dtype); 

#plt.imshow(img_dat_pre[0], cmap="gray"); 
#plt.title("Image pré-traité avec Dlib");  plt.show();

# Sauver à fichier binaire de Numpy *.npy
np.save("../../db_transfigure/jaffe_dlib128_img.npy", img_dat_pre );
np.save("../../db_transfigure/jaffe_dlib128_lab.npy", img_lab);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Motifs Binaires Locaux (LBP)          ");
print("************************************** ");
# Paramètres des Motifs binaires locaux (LBP) 
radius = 3;                                                # Rayon de cercle
n_points = 8 * radius;                                     # nombre de points voisins circulairement symétriques.
method= "var";                                             # Méthode pour la determination du motifs. E.g. default, ror, uniform, nri_uniform, var.

lbp_var_lt = [];
for img in img_dat_pre:
  lbp_var = lbp_fun(img, n_points, radius, method);        # Calculer le LBP.
  lbp_var_lt.append(lbp_var);

#plt.imshow(lbp_var, cmap="gray");                         # Imprimer quelques images
#plt.title("Image avec lbp, méthode= var"); plt.show();
print("Image après lbp var: \n", lbp_var); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" À nombre entier int8                  ");
print("************************************** ");
var_int_lt = [];
for img in lbp_var_lt:
  img = img / img.max();                                   # normalizer img [0, 1].
  img = 255 * img;                                         # À échelle 0 - 255
  img = np.rint(img);                                      # Arrondir à entier
  var_int_lt.append( img.astype(np.uint8) );               # cast à uint8
print("valeurs du lbp_var_int[0]:\n ", var_int_lt[0]); print(var_int_lt[0].shape);
#print("valeurs du lbp_var_int:\n ", var_int_lt); 
print("longitude de liste: ", len(var_int_lt));

#plt.imshow(var_int_lt[0], cmap="gray");                   # Imprimer quelques images
#plt.title("Image avec lbp var en entier 0 à 255");  plt.show();

# Sauver à fichier binaire de Numpy *.npy
np.save("../../db_transfigure/jaffe_dlib_var128_img.npy", var_int_lt );
np.save("../../db_transfigure/jaffe_dlib_var128_lab.npy", img_lab);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Détection des contours                ");
print("************************************** ");
# minVal = 50, maxVal = 100
can_lt = [];
for img in var_int_lt:
  img = cv2.Canny(img, 50, 100); 
  can_lt.append(img);

#plt.imshow(can_lt[0], cmap="gray");                       # Imprimer quelques images
#plt.title("Image avec détection de contours"); plt.show();
#print("Image après détection de contours: \n", can_lt[0]); 

# Sauver à fichier binaire de Numpy *.npy
np.save("../../db_transfigure/jaffe_dlib_var_can128_img.npy", can_lt );
np.save("../../db_transfigure/jaffe_dlib_var_can128_lab.npy", img_lab);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Redimensionnement de l'image          ");
print("************************************** ");
red_lt = [];
for img in can_lt:
  # 56 x 56 pixels
  lan_56 = cv2.resize(img, dsize=(56,56), interpolation=cv2.INTER_LANCZOS4);
  red_lt.append(lan_56);

#plt.imshow(red_lt[0], cmap="gray");                       # Imprimer quelques images
#plt.title("Interpolation Lanczos4 56 x 56 pixels"); plt.show();
#print("Image après le redimensionnement: \n", red_lt[0]); 

# Sauver à fichier binaire de Numpy *.npy
np.save("../../db_transfigure/jaffe_dlib_var_can_red56_img.npy", red_lt );
np.save("../../db_transfigure/jaffe_dlib_var_can_red56_lab.npy", img_lab);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Charger les transfigurations          ");
print("************************************** ");

# Charger image + pré-traitement avec dlib
dlib_img = np.load("../../db_transfigure/jaffe_dlib128_img.npy");
dlib_lab = np.load("../../db_transfigure/jaffe_dlib128_lab.npy");

# Charger image + pré-traitement  avec dlib + lbp-var
var_img = np.load("../../db_transfigure/jaffe_dlib_var128_img.npy");
var_lab = np.load("../../db_transfigure/jaffe_dlib_var128_lab.npy");

# Charger image + pré-traitement avec dlib + lbp-var + canny1986
can_img = np.load("../../db_transfigure/jaffe_dlib_var_can128_img.npy");
can_lab = np.load("../../db_transfigure/jaffe_dlib_var_can128_lab.npy");

# Charger image + pré-traitement avec dlib + lbp-var + canny1986 + redimensionnent
red_img = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_img.npy");
red_lab = np.load("../../db_transfigure/jaffe_dlib_var_can_red56_lab.npy");


# Fonction qui sert à comparer les étapes du pré-traitement 
with PdfPages("etapes_du_pretraitement.pdf")  as pdf:

  # pré-traitement avec dlib
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[dlib_lab[0]]); ax1.imshow(dlib_img[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[dlib_lab[1]]); ax2.imshow(dlib_img[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[dlib_lab[2]]); ax3.imshow(dlib_img[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[dlib_lab[3]]); ax4.imshow(dlib_img[3], cmap="gray"); 
  fig.suptitle("Pré-traitement avec Dlib", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # pré-traitement avec dlib + lbp-var
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[var_lab[0]]); ax1.imshow(var_img[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[var_lab[1]]); ax2.imshow(var_img[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[var_lab[2]]); ax3.imshow(var_img[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[var_lab[3]]); ax4.imshow(var_img[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # pré-traitement avec dlib + lbp-var + Canny1986
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[can_lab[0]]); ax1.imshow(can_img[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[can_lab[1]]); ax2.imshow(can_img[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[can_lab[2]]); ax3.imshow(can_img[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[can_lab[3]]); ax4.imshow(can_img[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var + Canny1986", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # pré-traitement avec dlib + lbp-var + Canny1986 + redimensionnement
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[red_lab[0]]); ax1.imshow(red_img[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[red_lab[1]]); ax2.imshow(red_img[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[red_lab[2]]); ax3.imshow(red_img[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[red_lab[3]]); ax4.imshow(red_img[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var + Canny1986 + redimensionnement", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

#sys.exit(0);                                              # Terminer l'execution


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
