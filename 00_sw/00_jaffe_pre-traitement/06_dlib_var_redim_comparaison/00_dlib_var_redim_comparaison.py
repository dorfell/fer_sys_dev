# -*- coding: utf-8 -*-
## 
# @file    00_dlib_var_redim_comparaison.py
# @brief   Pré-traitement du Jaffe avec Dlib + lbp-var + redimensionnement
# @details Ce module sauvegardé l'ensemble Jaffe pré-traité avec Dlib, le
#          motif locaux binaire avec le méthode VAR et il fait la 
#          comparaison de redimensionnement à tailles de 128x128, 
#          96x96, 72x72, 64x64, 56x56, 48x48, 32x32 et 24x24 pixels 
#          avec l'interpolation Lanczos4.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/02/26
# @version 0.1
"""@package docstring
"""

import cv2

import matplotlib as mpl
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12;
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True;
mpl.rcParams['font.family'] = 'sans-serif';
mpl.rcParams['mathtext.fontset']    = 'dejavusans';
mpl.rcParams.update({'font.size': 12});
import matplotlib.pyplot as plt

import numpy as np
import os
import sys

from skimage.feature import local_binary_pattern as lbp_fun
from mes_fonctions import *

from matplotlib.backends.backend_pdf import PdfPages


print("************************************** ");
print(" Charger l'ensamble de données         ");
print("************************************** ");
# Localisation de l'ensamble de données (db)
loc_db = "../../db/jaffedbase_petit/";                     # Seulment 4 images. 

# Code d'expressions 
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];

# Lire la base de données
img_dat, img_lab = read_data(loc_db);
#print(img_dat, img_dat.shape,  img_lab);
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Pré-traitement des données avec Dlib  ");
print("************************************** ");
# Pré-traitement des données
img_dat_pre = preprocessing(img_dat);                      # taille (128, 128)
#print("print img_dat_resized", img_dat_resized, len(img_dat_resized), type(img_dat_resized),  img_lab);
print("Image après pré-traitement: \n", img_dat_pre[0], img_dat_pre[0].dtype); 
#plt.imshow(img_dat_pre[0], cmap="gray"); 
#plt.title("Image pré-traité avec Dlib");  plt.show();
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Motifs Binaires Locaux (LBP)          ");
print("************************************** ");
# Paramètres des Motifs binaires locaux (LBP) 
radius = 3;                                                # Rayon de cercle
n_points = 8 * radius;                                     # nombre de points voisins circulairement symétriques.
method= "var";                                             # Méthode pour la determination du motifs. E.g. default, ror, uniform, nri_uniform, var.

lbp_var_lt = [];
for img in img_dat_pre:
  lbp_var = lbp_fun(img, n_points, radius, method);        # Calculer le LBP.
  lbp_var_lt.append(lbp_var);

#plt.imshow(lbp_var, cmap="gray");                         # Imprimer quelques images
#plt.title("Image avec lbp, méthode= var"); plt.show();
#print("Image après lbp var: \n", lbp_var); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" À nombre entier int8                  ");
print("************************************** ");
var_int_lt = [];
for img in lbp_var_lt:
  img = img / img.max();                                   # normalizer img [0, 1].
  img = 255 * img;                                         # À échelle 0 - 255
  img = np.rint(img);                                      # Arrondir à entier
  var_int_lt.append( img.astype(np.uint8) );               # cast à uint8
print("valeurs du lbp_var_int[0]:\n ", var_int_lt[0]); print(var_int_lt[0].shape);
#print("valeurs du lbp_var_int:\n ", var_int_lt); 
print("longitude de liste: ", len(var_int_lt));

#plt.imshow(var_int_lt[0], cmap="gray");                   # Imprimer quelques images
#plt.title("Image avec lbp var en entier 0 à 255");  plt.show();


print("************************************** ");
print(" Redimensionnement de l'image          ");
print("************************************** ");
red24_lt = []; red32_lt = []; red48_lt = []; red56_lt = [];
red64_lt = []; red72_lt = []; red96_lt = []; red128_lt = [];
for img in var_int_lt:
  # 24 x 24 pixels
  red24 = cv2.resize(img, dsize=(24,24), interpolation=cv2.INTER_LANCZOS4);
  red24_lt.append(red24);

  # 32 x 32 pixels
  red32 = cv2.resize(img, dsize=(32,32), interpolation=cv2.INTER_LANCZOS4);
  red32_lt.append(red32);

  # 48 x 48 pixels
  red48 = cv2.resize(img, dsize=(48,48), interpolation=cv2.INTER_LANCZOS4);
  red48_lt.append(red48);

  # 56 x 56 pixels
  red56 = cv2.resize(img, dsize=(56,56), interpolation=cv2.INTER_LANCZOS4);
  red56_lt.append(red56);

  # 64 x 64 pixels
  red64 = cv2.resize(img, dsize=(64,64), interpolation=cv2.INTER_LANCZOS4);
  red64_lt.append(red64);

  # 72 x 72 pixels
  red72 = cv2.resize(img, dsize=(72,72), interpolation=cv2.INTER_LANCZOS4);
  red72_lt.append(red72);

  # 96 x 96 pixels
  red96 = cv2.resize(img, dsize=(96,96), interpolation=cv2.INTER_LANCZOS4);
  red96_lt.append(red96);

  # 128 x 128 pixels
  red128_lt.append(img);

#plt.imshow(red24_lt[0], cmap="gray");                     # Imprimer quelques images
#plt.title("Interpolation Lanczos4 24 x 24 pixels"); plt.show();
#print("Image après le redimensionnement: \n", red_lt[0]); 
#sys.exit(0);                                              # Terminer l'execution


print("************************************** ");
print(" Faire la comparaison des tailles      ");
print("************************************** ");
# Fonction qui sert à comparer les tailles de dlib + lbp-var 
with PdfPages("comparaison_tailles.pdf")  as pdf:

  # 24 x 24 
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[img_lab[0]]); ax1.imshow(red24_lt[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[img_lab[1]]); ax2.imshow(red24_lt[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[img_lab[2]]); ax3.imshow(red24_lt[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[img_lab[3]]); ax4.imshow(red24_lt[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var, 24 x 24 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 32 x 32  
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[img_lab[0]]); ax1.imshow(red32_lt[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[img_lab[1]]); ax2.imshow(red32_lt[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[img_lab[2]]); ax3.imshow(red32_lt[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[img_lab[3]]); ax4.imshow(red32_lt[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var, 32 x 32 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 48 x 48 
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[img_lab[0]]); ax1.imshow(red48_lt[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[img_lab[1]]); ax2.imshow(red48_lt[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[img_lab[2]]); ax3.imshow(red48_lt[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[img_lab[3]]); ax4.imshow(red48_lt[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var, 48 x 48 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 56 x 56 
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[img_lab[0]]); ax1.imshow(red56_lt[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[img_lab[1]]); ax2.imshow(red56_lt[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[img_lab[2]]); ax3.imshow(red56_lt[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[img_lab[3]]); ax4.imshow(red56_lt[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var, 56 x 56 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 64 x 64  
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[img_lab[0]]); ax1.imshow(red64_lt[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[img_lab[1]]); ax2.imshow(red64_lt[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[img_lab[2]]); ax3.imshow(red64_lt[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[img_lab[3]]); ax4.imshow(red64_lt[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var, 64 x 64 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 72 x 72 
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[img_lab[0]]); ax1.imshow(red72_lt[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[img_lab[1]]); ax2.imshow(red72_lt[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[img_lab[2]]); ax3.imshow(red72_lt[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[img_lab[3]]); ax4.imshow(red72_lt[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var, 72 x 72 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 96 x 96  
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[img_lab[0]]); ax1.imshow(red96_lt[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[img_lab[1]]); ax2.imshow(red96_lt[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[img_lab[2]]); ax3.imshow(red96_lt[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[img_lab[3]]); ax4.imshow(red96_lt[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var, 96 x 96 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();

  # 128 x 128 
  fig = plt.figure();
  ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[img_lab[0]]); ax1.imshow(red128_lt[0], cmap="gray"); 
  ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[img_lab[1]]); ax2.imshow(red128_lt[1], cmap="gray"); 
  ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[img_lab[2]]); ax3.imshow(red128_lt[2], cmap="gray"); 
  ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[img_lab[3]]); ax4.imshow(red128_lt[3], cmap="gray"); 
  fig.suptitle("Dlib + lbp-var, 128 x 128 pixels", fontsize=12); plt.tight_layout();
  fig.subplots_adjust(top=0.88);
  #plt.show();                                             # Commenter pour le pdf
  pdf.savefig(); plt.close();
#sys.exit(0);                                              # Terminer l'execution


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
