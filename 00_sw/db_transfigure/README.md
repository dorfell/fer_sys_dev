-------------------------------------------------------------------

     =========================================================
     fer_sys_dev/00_sw/db_transfigure/ - Ensemble de données transfigurés.
     =========================================================

                         Liste des fichiers
                         ---------------

Ensembles des données transfigurés avec répresentation  de nombres entiers en format de Numpy binaire.

jaffe_dlib128_img.npy
 - Images de Jaffe après pré-traitement avec dlib, taille 128 x 128 pixels.

jaffe_dlib128_lab.npy
 - Étiquettes de Jaffe après pré-traitement avec dlib, taille 128 x 128 pixels.

jaffe_dlib_var128_img.npy
 - Images de Jaffe après pré-traitement  avec dlib et le motif locaux binaire lbp avec méthode var, taille 128 x 128 pixels.

jaffe_dlib_var128_lab.npy
 - Étiquettes de Jaffe après pré-traitement avec dlib et le motif locaux binaire lbp avec méthode var, taille 128 x 128 pixels.

jaffe_dlib_var_can128_img.npy
 - Images de Jaffe après pré-traitement avec dlib, le motif locaux binaire lbp avec méthode var, et la détection de contours avec Canny1986 de seuils (50, 100). Taille 128 x 128 pixels.

jaffe_dlib_var_can128_lab.npy
 - Étiquettes de Jaffe après pré-traitement avec dlib, le motif locaux binaire lbp avec méthode var, et la détection de contours avec Canny1986. Taille 128 x 128 pixels.

jaffe_dlib_var_can_red56_img.npy
 - Images de Jaffe après pré-traitement avec dlib, le motif locaux binaire lbp avec méthode var, la détection de contours avec Canny1986, et le redimensionnement avec l'interpolation Lanczos4 d'un taille 128 x 128 pixels à 56 x 56 pixels.

jaffe_dlib_var_can_red56_lab.npy
 - Étiquettes de Jaffe après pré-traitement avec dlib, le motif locaux binaire lbp avec méthode var, la détection de contours avec Canny1986, et le redimensionnement avec l'interpolation Lanczos4 d'un taille 128 x 128 pixels à 56 x 56 pixels.

jaffe_dlib128_aug4_img.npy
 - Images de Jaffe après pré-traitement avec dlib + augmentation, taille 128 x 128 pixels.

jaffe_dlib128_aug4_lab.npy
 - Étiquettes de Jaffe après pré-traitement avec dlib + augmentation, taille 128 x 128 pixels.

jaffe_dlib_var128_aug4_img.npy
 - Images de Jaffe après pré-traitement avec dlib et le motif locaux binaire lbp avec méthode var + augmentation, taille 128 x 128 pixels.

jaffe_dlib_var128_aug4_lab.npy
 - Étiquettes de Jaffe après pré-traitement avec dlib et le motif locaux binaire lbp avec méthode var + augmentation, taille 128 x 128 pixels.

jaffe_dlib_var_can128_aug4_img.npy
 - Images de Jaffe après pré-traitement avec dlib, le motif locaux binaire lbp avec méthode var, et la détection de contours avec Canny1986 de seuils (50, 100) + augmentation. Taille 128 x 128 pixels.

jaffe_dlib_var_can128_aug4_lab.npy
 - Étiquettes de Jaffe après pré-traitement avec dlib, le motif locaux binaire lbp avec méthode var, et la détection de contours avec Canny1986 + augmentation. Taille 128 x 128 pixels.

jaffe_dlib_var_can_red56_aug4_img.npy
 - Images de Jaffe après pré-traitement avec dlib, le motif locaux binaire lbp avec méthode var, la détection de contours avec Canny1986, et le redimensionnement avec l'interpolation Lanczos4 d'un taille 128 x 128 pixels à 56 x 56 pixels + augmentation.

jaffe_dlib_var_can_red56_aug4_lab.npy
 - Étiquettes de Jaffe après pré-traitement avec dlib, le motif locaux binaire lbp avec méthode var, la détection de contours avec Canny1986, et le redimensionnement avec l'interpolation Lanczos4 d'un taille 128 x 128 pixels à 56 x 56 pixels + augmentation.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
