# -*- coding: utf-8 -*-
## 
# @file    03_cnn_mnist_tflite_metrics.py 
# @brief   CNN trainée avec MNIST et quantifié avec tflite. 
# @details CNN + Mnist + classification metrics
#          Pris du: Quantization aware training in Keras example.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2023/10/06
# @version 0.1
"""@package docstring
"""


import matplotlib as mpl 
mpl.rcParams['legend.fontsize'] = 22;
mpl.rcParams['axes.labelsize']  = 22;
mpl.rcParams['xtick.labelsize'] = 22; 
mpl.rcParams['ytick.labelsize'] = 22;
mpl.rcParams['text.usetex'] = True; 
mpl.rcParams['font.family'] = 'sans-serif'; 
mpl.rcParams['mathtext.fontset']    = 'dejavusans'; 
mpl.rcParams.update({'font.size': 22}); 
import matplotlib.pyplot as plt

import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed


import seaborn as sns
import sys 
np.set_printoptions(threshold=sys.maxsize) # Printing all the weights 
import tempfile
import tensorflow as tf
#physical_devices = tf.config.list_physical_devices('GPU')
#tf.config.experimental.set_memory_growth(physical_devices[0], True)
import tensorflow_model_optimization as tfmot
from tensorflow  import keras

from matplotlib.backends.backend_pdf import PdfPages

 
print(" \n ");
print("************************************** "); 
print(" Charger l'ensemble de données         "); 
print("************************************** ");

# Load MNIST dataset
mnist = keras.datasets.mnist;
(train_images, train_labels), (test_images, test_labels) = mnist.load_data();
print(train_images[0]);

# Normalize the input image so that each pixel value is between 0 to 1.
#train_images = train_images / 255.0;
#test_images  = test_images  / 255.0;



# Dessiner quelques données
#fig = plt.figure();
#ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(train_labels[0]); ax1.imshow(train_images[0], cmap="gray");   
#ax1 = fig.add_subplot(2, 2, 2); ax1.set_title(train_labels[1]); ax1.imshow(train_images[1], cmap="gray");   
#ax1 = fig.add_subplot(2, 2, 3); ax1.set_title(train_labels[2]); ax1.imshow(train_images[2], cmap="gray");   
#ax1 = fig.add_subplot(2, 2, 4); ax1.set_title(train_labels[3]); ax1.imshow(train_images[3], cmap="gray");   
#fig.suptitle("Dataset examples", fontsize=12); plt.tight_layout();
#fig.subplots_adjust(top=0.88);
#plt.show();                                                 
#sys.exit(0);                                              # Terminer l'execution



# Define the model architecture.
model = keras.Sequential([
  keras.layers.InputLayer(input_shape=(28, 28)),
  keras.layers.Reshape(target_shape=(28, 28, 1)),
  keras.layers.Conv2D(filters=5, kernel_size=(5, 5), strides=(1,1), padding="SAME", activation='relu'),
  keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2,2)),
  keras.layers.Flatten(),
  keras.layers.Dense(10)
]);


# Train the digit classification model
lr_val = 0.01;
opt = keras.optimizers.Adam(learning_rate=lr_val);
model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy']);



# *** DO NOT FIT, LOAD THE PREVIOUS PARAMETERS *** 
#batch_size = 32;
#his = model.fit(
#        train_images, train_labels, # batch_size = batch_size,
#        #steps_per_epoch = len(train_images) / batch_size,
#        epochs=10, verbose=2,
#        validation_split=0.1,
#);



# Ouvrir le modèle en hdf5 
#------------------------------
#print(dir(keras.Model));
#model.load("models/cnn_mnist.h5");
model.load_weights("models/cnn_mnist_weights.h5");
#sys.exit(0);                                              # Terminer l'execution



print("************************************** "); 
print(" Evaluation de performance             "); 
print("************************************** ");
# Evaluating
score = model.evaluate(test_images, test_labels, verbose=0);
print('Avec la collection de test');
print('--> Perte:',          score[0]);
print('--> Exactitude:', 100*score[1]);



# Creation de la matrix du confusion
labels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
actual_labels    = test_labels;
predicted_labels = model.predict(test_images);
predicted_labels = tf.concat(predicted_labels, axis=0);
predicted_labels = tf.argmax(predicted_labels, axis=1);


def plot_confusion_matrix(actual, predicted, labels):
  cm = tf.math.confusion_matrix(actual_labels, predicted_labels);
  cm = cm/np.sum(cm, axis=1)[:, np.newaxis];# Normalize.
  cm = np.round(cm, decimals=2);            # Compute percentages.
  ax = sns.heatmap(cm, annot=True, fmt='g', cmap="GnBu");
  #ax = sns.heatmap(cm, annot=True, fmt='g', cmap="Blues");
  #ax = sns.heatmap(cm, annot=True, fmt='g', cmap="YlGnBu");
  #sns.set(rc={'figure.figsize':(12, 12)});
  #sns.set(font_scale=2.0);
  #ax.set_title('Confusion Matrix');
  ax.set_xlabel('Predicted label');
  ax.set_ylabel('True label');
  plt.xticks(rotation=0);
  plt.yticks(rotation=0);
  ax.xaxis.set_ticklabels(labels);
  ax.yaxis.set_ticklabels(labels);




def calculate_classification_metrics(y_actual, y_pred, labels):
  """
    Calculate the precision and recall of a classification model using the ground truth and
    predicted values. 

    Args:
      y_actual: Ground truth labels.
      y_pred: Predicted labels.
      labels: List of classification labels.

    Return:
      Precision and recall measures.
  """
  cm = tf.math.confusion_matrix(y_actual, y_pred);
  tp = np.diag(cm);                         # Diagonal represents true positives.
  precision  = dict(); recall = dict();
  f1Score    = [];     MCC    = dict();
  for i in range(len(labels)):
    col = cm[:, i]; row = cm[i, :];         # Columns and rows from the confusion matrix. 
    fp = np.sum(col) - tp[i];               # Sum of column minus true positive is false negative.
    fn = np.sum(row) - tp[i];               # Sum of row minus true positive, is false negative.
    tn = np.sum(cm) - (fp + fn + tp[i]);    # Sum of all matrix elements minor false 
                                            # positive, false negative and true positive,
                                            # is true negative.
  
    precision[labels[i]]  = tp[i] / (tp[i] + fp); # Precision 
    recall[labels[i]]     = tp[i] / (tp[i] + fn); # Recall
    MCC[labels[i]] = (tp[i]*tn-fp*fn) / np.sqrt( \
                     (tp[i]+fp)*(tp[i]+fn)*(tn+fp)*(tn+fn) ); 

  for idx in range(0, len(labels)):         # idx represents the dictionary keys.
    f1Score.append(2 * (precision[idx] * recall[idx]) / (precision[idx] + recall[idx]));

  print("\n Precision: ", precision, "\n");
  print("\n Recall:  ",   recall, "\n");
  print("\n MCC:  ",      MCC, "\n");
  print("\n F-1 Score: ", f1Score, "\n");

  precisionAvg = 0; recallAvg = 0;
  MCCAvg = 0;       f1ScoreAvg = 0;

  for keys, vals in precision.items():  
    precisionAvg = precisionAvg + vals;
  precisionAvg = precisionAvg/len(precision.items());

  for keys, vals in recall.items():  
    recallAvg = recallAvg + vals;
  recallAvg = recallAvg/len(recall.items());

  for keys, vals in MCC.items():  
    MCCAvg = MCCAvg + vals;
  MCCAvg = MCCAvg/len(MCC.items());

  print("\nPrecision avg: ", precisionAvg);
  print("Recall avg:    ",   recallAvg);
  print("MCC avg:       ",      MCCAvg);
  print("F-1 Score avg: ", sum(f1Score)/len(f1Score), "\n");

  return precision, recall, MCC, f1Score;

fig = plt.figure(1, figsize=(10,9));
plot_confusion_matrix(actual_labels, predicted_labels, labels);
#plt.show();

precision, recall, MCC, f1Score = calculate_classification_metrics(actual_labels, predicted_labels, labels);
#sys.exit(0);                                              # Terminer l'execution




print(" \n ");
print("************************************** ");
print(" Quantization aware model              ");
print("************************************** ");
# Quantize model
quantize_model = tfmot.quantization.keras.quantize_model;

# q_aware stands for quantization aware
q_aware_model = quantize_model(model);

# "quantize_model" requieres a recompile
q_aware_model.compile(optimizer=opt,
                      loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                      metrics=["accuracy"] );                       
q_aware_model.summary();

# Train and evaluate the model against baseline
train_images_subset = train_images[0:1000];                     # 1000 de 60000 échantillons
train_labels_subset = train_labels[0:1000];                     # 1000 de 60000 échantillons

q_aware_model.fit(train_images_subset, train_labels_subset, batch_size=500, epochs=1, validation_split=0.1);
 
_, baseline_model_accuracy =         model.evaluate( test_images, test_labels, verbose=0 );
_, q_aware_model_accuracy  = q_aware_model.evaluate( test_images, test_labels, verbose=0 );

print("Baseline test accuracy: ", baseline_model_accuracy);
print("q aware  test accuracy: ", q_aware_model_accuracy);
#sys.exit(0);                                              # Terminer l'execution




print(" \n ");
print("************************************** ");
print(" Quantized quantization aware model    ");
print("************************************** ");
# Create quantized model for TFLite backend
converter = tf.lite.TFLiteConverter.from_keras_model(q_aware_model);
converter.optimizations = [ tf.lite.Optimize.DEFAULT ];

quantized_tflite_model = converter.convert();

# See persistence of accuracy from TF to TFLite
def evaluate_model(interpreter):
  input_index  = interpreter.get_input_details()[0]["index"];
  output_index = interpreter.get_output_details()[0]["index"];
 
  # Run predictions on every image in the "test" dataset
  prediction_digits = [];
  for i, test_image in enumerate(test_images):

    #plt.imshow(test_image, cmap="gray", interpolation=None);
    #plt.title(test_labels[i]);
    #plt.show();
    
    if ( (i % 1000) == 0):
      print("Evaluated on {n} results so far.".format( n=i ) );
   
    # Pre-processing: add batch dimension and convert to float32 to match
    # the model's input data format.
    test_image = np.expand_dims( test_image, axis=0 ).astype( np.float32 );
    interpreter.set_tensor( input_index, test_image );

    # Run inference
    interpreter.invoke();

    # Post-processing: remove batch dimension and find the digit with high probability
    output = interpreter.tensor( output_index );
    digit = np.argmax( output()[0] );
    prediction_digits.append( digit );

  print(" ");
  # Compare prediction results with ground truth labels to calculate accuracy 
  prediction_digits = np.array( prediction_digits );
  accuracy = ( prediction_digits == test_labels ).mean();

  
  # Confusion matrix
  #labels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  #plot_confusion_matrix(test_labels, prediction_digits, labels);
  #plt.show();

  # Metrics
  #precision, recall, MCC, f1Score = \
  #  calculate_classification_metrics(test_labels, prediction_digits, labels);
  
  return accuracy


interpreter = tf.lite.Interpreter( model_content=quantized_tflite_model );
interpreter.allocate_tensors();
#fig = plt.figure(2, figsize=(9,8));
quantized_q_aware_accuracy = evaluate_model( interpreter );

print("quantized q aware TFLite test_accuracy: ", quantized_q_aware_accuracy );
print("q aware                  test_accuracy: ", q_aware_model_accuracy );

# En sauvegardant le modèle tflite 
#with tf.io.gfile.GFile('models/cnn_quantized_tflite.tflite', 'wb') as f:
#  f.write(quantized_tflite_model);


print(" \n ");
print("************************************** ");
print(" Quantizated model                     ");
print("************************************** ");
# See 4x smaller model from quantization

# Crate float TFLite model
float_converter    = tf.lite.TFLiteConverter.from_keras_model(model);
float_tflite_model = float_converter.convert();

# Measure sizes of models
_, float_file = tempfile.mkstemp(".tflite");
_, quant_file = tempfile.mkstemp(".tflite");

interpreter_float = tf.lite.Interpreter( model_content=float_tflite_model );
interpreter_float.allocate_tensors();
quantized_tffloat_accuracy = evaluate_model( interpreter_float );

# En sauvegardant le modèle tflite 
#with tf.io.gfile.GFile('models/cnn_float_tflite_model.tflite', 'wb') as f:
#  f.write(float_tflite_model);

# Taille du modèles en disque
with open( quant_file, "wb" ) as f:
  f.write( quantized_tflite_model );

with open( float_file, "wb" ) as f:
  f.write( float_tflite_model );

print("Float     model in MB: ", os.path.getsize(float_file) / float( 2**20) );
print("Quantized model in MB: ", os.path.getsize(quant_file) / float( 2**20) );


print(" \n ");
print("**************************************");
print("***            Accuracies          ***");
print("--------------------------------------");
print("TF float (default): ", baseline_model_accuracy);
print("TF q_aware:         ", q_aware_model_accuracy);
print("Quantized q_aware (TFlite):  ", quantized_q_aware_accuracy );
print("Quantized TF float (TFlite): ", quantized_tffloat_accuracy );

fig.tight_layout();
plt.show();

print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
