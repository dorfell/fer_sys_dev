# -*- coding: utf-8 -*-
## 
# @file    01_cnn_tflite_param.py 
# @brief   Imprimer les paramètres du cnn en tflite
# @details Module pour imprimer les tensors et ses entrées et sorties du 
#          modèle cnn en tflite avec mnistar64 + aug15 + 6emo. 
#          Il sert à comprendre comme l'inference est calculée avec un 
#          modèle TFLite (modèle quantizée à nombres entiers du 8-bits).
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2023/06/20
# @version 0.1
"""@package docstring
"""


import matplotlib as mpl 
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12; 
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True; 
mpl.rcParams['font.family'] = 'sans-serif'; 
mpl.rcParams['mathtext.fontset']    = 'dejavusans'; 
mpl.rcParams.update({'font.size': 12}); 
import matplotlib.pyplot as plt

import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys 
np.set_printoptions(threshold=sys.maxsize) # Printing all the weights 
import tempfile
import tensorflow as tf
#physical_devices = tf.config.list_physical_devices('GPU')
#tf.config.experimental.set_memory_growth(physical_devices[0], True)
import tensorflow_model_optimization as tfmot
from tensorflow  import keras

from matplotlib.backends.backend_pdf import PdfPages


print(" \n ");
print("************************************** "); 
print(" Charger l'ensemble de données         "); 
print("************************************** ");

# Load MNIST dataset
mnist = keras.datasets.mnist;
(train_images, train_labels), (test_images, test_labels) = mnist.load_data();


# Normalize the input image so that each pixel value is between 0 to 1.
#train_images = train_images / 255.0;
#test_images  = test_images  / 255.0;


# Dessiner quelques données
#fig = plt.figure();
#plt.imshow(train_images[0], cmap="gray"); 
plt.imshow(test_images[0], cmap="gray"); 
#plt.show();                                                 
#sys.exit(0);                                              # Terminer l'execution




print(" \n ");
print("************************************** "); 
print(" Evaluer la performance du modèle CNN  "); 
print(" TensorFlowLite, i.e. Exactitude.      "); 
print("************************************** "); 

# See persistence of accuracy from TF to TFLite
def evaluate_model(interpreter):
  input_index  = interpreter.get_input_details()[0]["index"];
  output_index = interpreter.get_output_details()[0]["index"];
 
  # Run predictions on every image in the "test" dataset
  prediction_digits = [];
  for i, test_image in enumerate(test_images):

    #plt.imshow(test_image, cmap="gray", interpolation=None);
    #plt.title(test_labels[i]);
    #plt.show();
 
    if ( (i % 1000) == 0):
      print("Evaluated on {n} results so far.".format( n=i ) );
 
    # Pre-processing: add batch dimension and convert to float32 to match
    # the model's input data format.
    test_image = np.expand_dims( test_image, axis=0 ).astype( np.float32 );
    interpreter.set_tensor( input_index, test_image );
 
    # Run inference
    interpreter.invoke();
 
    # Post-processing: remove batch dimension and find the digit with high probability
    output = interpreter.tensor( output_index );
    digit = np.argmax( output()[0] );
    prediction_digits.append( digit );
 
  print(" ");
  # Compare prediction results with ground truth labels to calculate accuracy 
  prediction_digits = np.array( prediction_digits );
  accuracy = ( prediction_digits == test_labels ).mean();
  return accuracy


# Charger le modèle tflite et allouer tensors. 
#---------------------------------- 
inter = tf.lite.Interpreter( model_path="models/cnn_quantized_tflite.tflite" );
inter.allocate_tensors(); 
accuracy = evaluate_model( inter );
print(" TFlite model test_accuracy: ", accuracy ); 


# Imprimer les détails du modèle
#---------------------------------- 
input_details  = inter.get_input_details();  #print("input_details:  ", input_details);
output_details = inter.get_output_details(); #print("output_details: ", output_details); 
tensor_details = inter.get_tensor_details(); #print("tensor_details: ", tensor_details); 
output_index   = inter.get_output_details()[0]["index"];   # Dernier index  


# Créer dictionnaire avec l'information des couches. 
#---------------------------------- 
dic_mod = {};                                              # Dictionnaire avec les nom du chaque couche 
for idx in range( 0, len(inter.get_tensor_details()) ):   
  try:     
    dic_mod[idx] = inter.get_tensor_details()[idx]["name"];   
    #print("couche: ", inter.get_tensor_details()[idx]["name"] );   
  except:
    print("Le position ",idx," est null.");
print("Dictionnaire des couches: \n", dic_mod);

#sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** "); 
print(" Image d'entrée                        "); 
print("************************************** "); 

# Definer l'image d'entrée (64, 64,1)
# x_test[0];
print(test_images[0]);                                              # Terminer l'execution

#sys.exit(0);                                              # Terminer l'execution

img_test = \
[[  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,  84, 185, 159, 151,  60,  36,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0, 222, 254, 254, 254, 254, 241, 198, 198, 198, 198, 198, 198, 198, 198, 170,  52,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,  67, 114,  72, 114, 163, 227, 254, 225, 254, 254, 254, 250, 229, 254, 254, 140,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  17,  66,  14,  67,  67,  67,  59,  21, 236, 254, 106,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  83, 253, 209,  18,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  22, 233, 255,  83,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 129, 254, 238,  44,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  59, 249, 254,  62,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 133, 254, 187,   5,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   9, 205, 248,  58,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 126, 254, 182,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  75, 251, 240,  57,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  19, 221, 254, 166,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   3, 203, 254, 219,  35,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  38, 254, 254,  77,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  31, 224, 254, 115,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 133, 254, 254,  52,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  61, 242, 254, 254,  52,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 121, 254, 254, 219,  40,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 121, 254, 207,  18,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0]];
img_test = np.asarray(img_test);                           # Convertir données à tableau numpy 

# Imprimer quelques images 
fig = plt.figure(figsize=(3,3));
plt.imshow(img_test, cmap="gray", interpolation=None); 
plt.title(test_labels[0]); plt.tight_layout(); 
#plt.show(); 

# Pre-processer l'image d'entrée: ajouter dimension et convertir à virgule flottante 
img_test = np.expand_dims( img_test, axis=0 ).astype( np.float32 ); 
#print(img_test); 
#sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** ");
print(" Entrées et sorties des couches        "); 
print("************************************** "); 
# Mettre l'image du test en la couche d'entrée "input_1" avec idx = 0. (voir dic_mod) 
inter.set_tensor( 0, img_test );                           # 0:"input_1"
inter.invoke();                                            # L'inference 
for idx_ten in range( 0, output_index + 1 ): 
  print("\n Couche: ", idx_ten);  
  print("-------------------------------------- ");  
  print("Paramèters: \n", inter.get_tensor_details()[idx_ten]);  
  print("Tensor:     \n", inter.get_tensor(idx_ten));  
  for ele_ten in range( 0, 20 ): 
    try:
      output = inter.tensor(idx_ten);  
      print("Sortie du tensor ", ele_ten, ": \n", output()[ele_ten] );    
      print("tf.print: ");  tf.print( output()[ele_ten] );    
    except:
      #print("Il n'y a pas résultat pour ce tensor. \n ");   
      break; 
#sys.exit(0);                                              # Terminer l'execution


print(" \n "); 
print("************************************** "); 
print(" Predicition avec x_test[0]            ");
print("************************************** "); 

# Prédiction du x_test[0]
input_index  = inter.get_input_details()[0]["index"]; 
output_index = inter.get_output_details()[0]["index"];
test_image   = np.expand_dims( test_images[0], axis=0 ).astype( np.float32 ); 

inter.set_tensor( input_index, test_image );               # Mis l'entrée
inter.invoke();                                            # Run inference   

# Post-processing: remove batch dimension and find the digit with high probability
output = inter.tensor( output_index );                     # Les predictions
print("outputs: ", output()[0]);

digit = np.argmax( output()[0] );                          # La plus grand probabil..
print("prédiction: ", digit);
print("vrai étiquette: ", test_labels[0]);
#print("x_test[0]: \n", x_test[0].astype( np.int32 ) );
sys.exit(0);                                              # Terminer l'execution


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
