# -*- coding: utf-8 -*-
## 
# @file    00_cnn_tflite.py 
# @brief   CNN trainée avec MNIST et quantifié avec tflite. 
# @details CNN + Mnist
#          Pris du: Quantization aware training in Keras example.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/03/24
# @version 0.1
"""@package docstring
"""


import matplotlib as mpl 
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12; 
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True; 
mpl.rcParams['font.family'] = 'sans-serif'; 
mpl.rcParams['mathtext.fontset']    = 'dejavusans'; 
mpl.rcParams.update({'font.size': 12}); 
import matplotlib.pyplot as plt

import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys 
np.set_printoptions(threshold=sys.maxsize) # Printing all the weights 
import tempfile
import tensorflow as tf
#physical_devices = tf.config.list_physical_devices('GPU')
#tf.config.experimental.set_memory_growth(physical_devices[0], True)
import tensorflow_model_optimization as tfmot
from tensorflow  import keras

from matplotlib.backends.backend_pdf import PdfPages

 
print(" \n ");
print("************************************** "); 
print(" Charger l'ensemble de données         "); 
print("************************************** ");

# Load MNIST dataset
mnist = keras.datasets.mnist;
(train_images, train_labels), (test_images, test_labels) = mnist.load_data();
print(train_images[0]);

# Normalize the input image so that each pixel value is between 0 to 1.
#train_images = train_images / 255.0;
#test_images  = test_images  / 255.0;


# Dessiner quelques données
#fig = plt.figure();
#ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(train_labels[0]); ax1.imshow(train_images[0], cmap="gray");   
#ax1 = fig.add_subplot(2, 2, 2); ax1.set_title(train_labels[1]); ax1.imshow(train_images[1], cmap="gray");   
#ax1 = fig.add_subplot(2, 2, 3); ax1.set_title(train_labels[2]); ax1.imshow(train_images[2], cmap="gray");   
#ax1 = fig.add_subplot(2, 2, 4); ax1.set_title(train_labels[3]); ax1.imshow(train_images[3], cmap="gray");   
#fig.suptitle("Dataset examples", fontsize=12); plt.tight_layout();
#fig.subplots_adjust(top=0.88);
#plt.show();                                                 
#sys.exit(0);                                              # Terminer l'execution



# Define the model architecture.
model = keras.Sequential([
  keras.layers.InputLayer(input_shape=(28, 28)),
  keras.layers.Reshape(target_shape=(28, 28, 1)),
  keras.layers.Conv2D(filters=5, kernel_size=(5, 5), strides=(1,1), padding="SAME", activation='relu'),
  keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2,2)),
  keras.layers.Flatten(),
  keras.layers.Dense(10)
]);


# Train the digit classification model
lr_val = 0.01;
opt = keras.optimizers.Adam(learning_rate=lr_val);
model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy']);


batch_size = 32;
his = model.fit(
        train_images, train_labels, # batch_size = batch_size,
        #steps_per_epoch = len(train_images) / batch_size,
        epochs=10, verbose=2,
        validation_split=0.1,
);




print("************************************** "); 
print(" Evaluation de performance             "); 
print("************************************** ");
# Evaluating
score = model.evaluate(test_images, test_labels, verbose=0);
print('Avec la collection de test');
print('--> Perte:',          score[0]);
print('--> Exactitude:', 100*score[1]);

predictions = model.predict(test_images);
predicted_label = np.argmax(predictions[0]);

#print(history.history);
#print(his.history.keys());

# Fonction qui sert à mesurer le performance du modèle 
with PdfPages("graphique_entrainement.pdf")  as export_pdf:
    
    # summarize history for accuracy
    plt.plot(his.history['accuracy']);
    plt.plot(his.history['val_accuracy']);
    #plt.title('Model Accuracy');
    plt.ylabel('accuracy'); plt.xlabel('epochs');
    plt.legend(['train', 'validation'], loc='lower right');
    plt.grid(alpha=0.3, linestyle='--');
    #plt.savefig("accu_plot.png");
    #export_pdf.savefig(); plt.close();

    # summarize history for loss
    plt.plot(his.history['loss']);
    plt.plot(his.history['val_loss']);
    #plt.title('Perte du modèle');
    plt.ylabel('loss'); plt.xlabel('epochs');
    plt.legend(['train', 'validation'], loc='upper right');
    plt.grid(alpha=0.3, linestyle='--');
    #plt.savefig("loss_plot.png");
    #export_pdf.savefig(); plt.close();


# En sauvegardant le modèle en hdf5 
#------------------------------
#model.save("models/cnn_mnist.h5");
#model.save_weights("models/cnn_mnist_weights.h5");
sys.exit(0);                                              # Terminer l'execution




print(" \n ");
print("************************************** ");
print(" Quantization aware model              ");
print("************************************** ");
# Quantize model
quantize_model = tfmot.quantization.keras.quantize_model;

# q_aware stands for quantization aware
q_aware_model = quantize_model(model);

# "quantize_model" requieres a recompile
q_aware_model.compile(optimizer=opt,
                      loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                      metrics=["accuracy"] );                       
q_aware_model.summary();

# Train and evaluate the model against baseline
train_images_subset = train_images[0:1000];                     # 1000 de 60000 échantillons
train_labels_subset = train_labels[0:1000];                     # 1000 de 60000 échantillons

q_aware_model.fit(train_images_subset, train_labels_subset, batch_size=500, epochs=1, validation_split=0.1);
 
_, baseline_model_accuracy =         model.evaluate( test_images, test_labels, verbose=0 );
_, q_aware_model_accuracy  = q_aware_model.evaluate( test_images, test_labels, verbose=0 );

print("Baseline test accuracy: ", baseline_model_accuracy);
print("q aware  test accuracy: ", q_aware_model_accuracy);
#sys.exit(0);                                              # Terminer l'execution




print(" \n ");
print("************************************** ");
print(" Quantized quantization aware model    ");
print("************************************** ");
# Create quantized model for TFLite backend
converter = tf.lite.TFLiteConverter.from_keras_model(q_aware_model);
converter.optimizations = [ tf.lite.Optimize.DEFAULT ];

quantized_tflite_model = converter.convert();

# See persistence of accuracy from TF to TFLite
def evaluate_model(interpreter):
  input_index  = interpreter.get_input_details()[0]["index"];
  output_index = interpreter.get_output_details()[0]["index"];
 
  # Run predictions on every image in the "test" dataset
  prediction_digits = [];
  for i, test_image in enumerate(test_images):

    #plt.imshow(test_image, cmap="gray", interpolation=None);
    #plt.title(test_labels[i]);
    #plt.show();
    
    if ( (i % 1000) == 0):
      print("Evaluated on {n} results so far.".format( n=i ) );
   
    # Pre-processing: add batch dimension and convert to float32 to match
    # the model's input data format.
    test_image = np.expand_dims( test_image, axis=0 ).astype( np.float32 );
    interpreter.set_tensor( input_index, test_image );

    # Run inference
    interpreter.invoke();

    # Post-processing: remove batch dimension and find the digit with high probability
    output = interpreter.tensor( output_index );
    digit = np.argmax( output()[0] );
    prediction_digits.append( digit );

  print(" ");
  # Compare prediction results with ground truth labels to calculate accuracy 
  prediction_digits = np.array( prediction_digits );
  accuracy = ( prediction_digits == test_labels ).mean();
  return accuracy


interpreter = tf.lite.Interpreter( model_content=quantized_tflite_model );
interpreter.allocate_tensors();
quantized_q_aware_accuracy = evaluate_model( interpreter );

print("quantized q aware TFLite test_accuracy: ", quantized_q_aware_accuracy );
print("q aware                  test_accuracy: ", q_aware_model_accuracy );

# En sauvegardant le modèle tflite 
#with tf.io.gfile.GFile('models/cnn_quantized_tflite.tflite', 'wb') as f:
#  f.write(quantized_tflite_model);


print(" \n ");
print("************************************** ");
print(" Quantizated model                     ");
print("************************************** ");
# See 4x smaller model from quantization

# Crate float TFLite model
float_converter    = tf.lite.TFLiteConverter.from_keras_model(model);
float_tflite_model = float_converter.convert();

# Measure sizes of models
_, float_file = tempfile.mkstemp(".tflite");
_, quant_file = tempfile.mkstemp(".tflite");

interpreter_float = tf.lite.Interpreter( model_content=float_tflite_model );
interpreter_float.allocate_tensors();
quantized_tffloat_accuracy = evaluate_model( interpreter_float );

# En sauvegardant le modèle tflite 
#with tf.io.gfile.GFile('models/cnn_float_tflite_model.tflite', 'wb') as f:
#  f.write(float_tflite_model);

# Taille du modèles en disque
with open( quant_file, "wb" ) as f:
  f.write( quantized_tflite_model );

with open( float_file, "wb" ) as f:
  f.write( float_tflite_model );

print("Float     model in MB: ", os.path.getsize(float_file) / float( 2**20) );
print("Quantized model in MB: ", os.path.getsize(quant_file) / float( 2**20) );


print(" \n ");
print("**************************************");
print("***            Accuracies          ***");
print("--------------------------------------");
print("TF float (default): ", baseline_model_accuracy);
print("TF q_aware:         ", q_aware_model_accuracy);
print("Quantized q_aware (TFlite):  ", quantized_q_aware_accuracy );
print("Quantized TF float (TFlite): ", quantized_tffloat_accuracy );


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
