# -*- coding: utf-8 -*-
## 
# @file    02_M6_tflite_inf_np.py 
# @brief   Inference du M6 + jaffe 6 émotions en Numpy
# @details Module pour réproduire l'inference du modèle M6 tflite
#          avec Python et Numpy. Le modèle à été entraîné avec
#          Jaffe + dlib + lbp-var64 + aug15 + 6emo. 
#          Il sert à comprendre comme l'inference est calculée avec un 
#          modèle TFLite (modèle quantizée à nombres entiers du 8-bits).
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/03/30
# @version 0.1
"""@package docstring
"""


import matplotlib as mpl 
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12; 
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True; 
mpl.rcParams['font.family'] = 'sans-serif'; 
mpl.rcParams['mathtext.fontset']    = 'dejavusans'; 
mpl.rcParams.update({'font.size': 12}); 
import matplotlib.pyplot as plt

import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys 
np.set_printoptions(threshold=sys.maxsize) # Printing all the weights 
import tempfile
import tensorflow as tf
#physical_devices = tf.config.list_physical_devices('GPU')
#tf.config.experimental.set_memory_growth(physical_devices[0], True)
import tensorflow_model_optimization as tfmot
from tensorflow  import keras

from matplotlib.backends.backend_pdf import PdfPages

from ctypes import * 
from mes_fonctions import *


print(" \n "); 
print("************************************** "); 
print(" Testing fonctions en CPP avec ctypes  ");
print("************************************** "); 
monbib = CDLL("mes_fon_CPP/monbib.so");                    # Bibliothèque CPP 
#print(monbib); monbib.print_cpp();
#sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** "); 
print(" Image d'entrée                        "); 
print("************************************** "); 

# Definer l'image d'entrée (64, 64,1)
# x_test[0];
img_test = \
[[  5,   7,  11,  18,  23,  29,  38,  43,  51,  57,  65,  79,  90, 108, 122, 136, 151, 158, 165, 173, 175, 182, 191, 194, 199, 203, 200, 198, 198, 195, 193, 196, 207, 205, 214, 223, 224, 222, 226, 228, 231, 232, 237, 238, 239, 243, 242, 244, 244, 241, 238, 234, 229, 220, 212, 190, 158, 132,  92,  65,  50,  43,  29,  12],
 [  5,   5,   5,   6,   8,   8,   9,   9,  11,  12,  13,  18,  22,  27,  26,  27,  31,  30,  33,  35,  34,  37,  38,  38,  40,  39,  39,  40,  42,  41,  38,  39,  41,  41,  42,  43,  43,  44,  45,  45,  45,  46,  48,  48,  48,  49,  49,  48,  49,  48,  47,  47,  46,  45,  43,  41,  46,  46,  29,  19,  16,  27,  28,  17],
 [  6,   4,   3,   3,   4,   3,   2,   2,   2,   2,   3,   4,   6,   6,   5,   3,   4,   5,   5,   5,   4,   4,   4,   4,   5,   3,   3,   4,   3,   3,   5,   4,   5,   5,   4,   5,   4,   4,   4,   4,   4,   4,   5,   5,   4,   4,   5,   4,   4,   4,   4,   4,   4,   5,   5,   9,  16,  18,   9,   6,   7,  22,  27,  22],
 [  6,   3,   2,   3,   3,   3,   2,   1,   1,   1,   3,   4,   5,   4,   1,   1,   1,   2,   3,   3,   2,   0,   1,   1,   1,   0,   1,   0,   0,   0,   1,   1,   1,   2,   1,   1,   0,   0,   0,   0,   1,   1,   1,   1,   1,   0,   1,   1,   0,   0,   0,   0,   0,   1,   1,   5,  10,  13,   7,   4,   5,  18,  29,  27],
 [  7,   3,   3,   3,   3,   3,   1,   1,   3,   2,   2,   4,   4,   5,   1,   1,   0,   2,   2,   4,   4,   4,   3,   1,   1,   1,   0,   0,   0,   1,   1,   1,   2,   2,   3,   2,   0,   1,   1,   0,   1,   0,   1,   1,   1,   1,   1,   1,   0,   0,   0,   0,   0,   1,   2,   4,   8,  13,   6,   3,   3,  13,  27,  33],
 [  7,   3,   1,   2,   4,   7,  10,  12,  10,   6,   6,   5,   7,   6,   3,   1,   1,   0,   0,   3,   3,   6,   5,   3,   3,   2,   0,   0,   1,   0,   0,   2,   2,   3,   3,   3,   1,   1,   1,   1,   1,   1,   0,   1,   1,   0,   0,   0,   0,   0,   0,   0,   0,   1,   3,   8,   7,  14,   6,   6,   4,  11,  20,  34],
 [  9,   2,   2,   4,   8,  13,  14,  16,  21,  20,  25,  22,  27,  25,  21,  17,  11,   4,   2,   2,   4,   3,   6,   6,   4,   4,   2,   1,   1,   1,   1,   1,   3,   2,   4,   3,   2,   1,   1,   2,   1,   0,   0,   1,   0,   0,   0,   1,   1,   2,   7,  10,  18,  32,  51,  59,  59,  49,  34,  26,  18,  12,  13,  29],
 [  9,   3,   4,   6,   6,   7,   7,   9,  14,  23,  28,  37,  48,  54,  56,  53,  49,  41,  35,  27,  18,  13,   9,   8,   7,   6,   6,   5,   3,   2,   1,   2,   3,   1,   2,   3,   3,   2,   3,   2,   3,   2,   2,   2,   5,  15,  32,  49,  63,  71,  73,  83,  89, 100, 106, 102,  82,  74,  57,  48,  36,  23,  17,  28],
 [  7,   3,   2,   3,   4,   5,   5,   3,   2,   5,  14,  22,  29,  40,  49,  59,  67,  72,  72,  70,  59,  43,  32,  22,  10,   8,   7,   5,   4,   3,   2,   3,   2,   1,   1,   2,   5,   6,   7,  12,  20,  34,  52,  74,  98, 116, 132, 140, 140, 138, 131, 119, 115, 103,  70,  42,  31,  25,  27,  22,  18,  14,  16,  22],
 [  7,   3,   4,   8,  12,  13,  14,  15,  16,  16,  11,   5,   3,   5,  11,  17,  27,  39,  44,  45,  49,  54,  53,  40,  27,  14,   7,   5,   3,   3,   2,   2,   2,   1,   1,   2,   4,   9,  16,  33,  65,  95, 112, 125, 135, 133, 126, 109,  97,  86,  69,  50,  29,  19,  24,  34,  41,  51,  54,  44,  32,  21,  13,  23],
 [ 10,   4,   7,  10,   9,  11,  14,  21,  30,  38,  47,  49,  40,  28,  23,  14,   8,   6,   4,   5,  13,  21,  32,  40,  32,  19,   9,   7,   6,   5,   3,   2,   2,   1,   1,   4,   5,   9,  22,  43,  59,  65,  71,  68,  54,  34,  20,  16,  11,  15,  23,  34,  61,  84,  92,  88,  90,  80,  71,  67,  57,  46,  32,  37],
 [ 10,   4,   3,   2,   2,   2,   3,   8,  18,  35,  53,  68,  73,  71,  63,  59,  53,  51,  48,  35,  20,  10,   7,  14,  22,  17,  11,   9,   8,   4,   5,   3,   1,   2,   4,   4,   5,   9,  18,  21,  21,  16,  19,  30,  43,  51,  64,  78,  91,  97, 107, 119, 122, 115,  93,  63,  44,  30,  19,  22,  20,  35,  36,  43],
 [ 11,   3,   1,   2,   2,   2,   2,   3,   4,   8,  12,  26,  42,  52,  55,  56,  62,  63,  64,  62,  56,  40,  22,  11,  11,   9,   6,   7,   8,   6,   3,   1,   1,   2,   2,   3,   3,   7,  13,  17,  35,  48,  59,  70,  80,  86,  85,  90,  92, 100,  97,  94,  78,  40,  11,   5,   3,   4,   4,   4,   4,  13,  24,  55],
 [ 12,   3,   0,   2,   2,   2,   2,   4,   6,  10,  11,   2,   0,   2,   2,   6,  12,  16,  26,  38,  41,  42,  31,  16,   7,   3,   2,   3,   5,   4,   3,   2,   1,   1,   1,   2,   1,   3,   7,  17,  28,  36,  44,  45,  36,  34,  30,  21,  16,  14,   8,   7,   4,   0,   3,   5,   3,   5,   5,   3,   2,   8,  25,  63],
 [ 15,   3,   1,   1,   3,   3,   8,  12,  20,  35,  52,  60,  61,  63,  57,  54,  54,  41,  27,   8,   3,   8,  12,   9,   3,   1,   2,   2,   6,   6,   5,   4,   2,   4,   4,   3,   2,   3,   4,   6,   5,   3,   3,   4,   3,   9,  29,  47,  62,  72,  75,  74,  72,  68,  61,  50,  32,  20,  11,   4,   3,   9,  20,  62],
 [ 18,   5,   2,   2,   4,   8,  12,  18,  23,  36,  54,  64,  75,  69,  70,  70,  67,  66,  64,  61,  37,   8,   3,   4,   2,   2,   3,   5,   6,   7,   7,   7,   6,   9,  11,  14,  11,   8,   3,   2,   1,   4,   5,   8,  38,  64,  78,  88, 102, 114, 125, 136, 144, 140, 133, 123,  99,  83,  62,  40,  20,  10,  16,  68],

 [ 22,   6,   4,   4,   4,   6,   8,   9,  12,  15,  19,  30,  35,  38,  45,  39,  42,  51,  47,  50,  46,  40,  11,   3,   4,   3,   4,   5,   8,   8,   9,   7,   8,   9,  14,  14,  17,  13,   9,   5,   3,   4,  11,  38,  36,  42,  54,  59,  76,  81,  92, 104, 101,  96,  82,  73,  75,  72,  64,  45,  29,  14,  20,  72],
 [ 26,   5,   4,   7,   6,   3,   0,   0,   2,   8,  18,  19,  21,  14,   0,   1,   0,  24,  42,  40,  33,  35,  41,  12,   8,   4,   4,   6,   9,  10,   9,   7,   3,   3,   4,   5,   8,  11,  11,   9,   8,  14,  38,  25,  30,  48,  71,  68,   6,   0,   0,  11,  40,  44,  31,  23,  18,  11,  16,  24,  22,  20,  30,  86],
 [ 25,   5,   2,   8,  10,   6,   3,   2,   5,  11,  13,   9,  22,  31,  27,  12,  16,  45,  31,  26,  26,  23,  37,  31,   3,   4,   4,   4,  10,  11,  10,   4,   1,   1,   0,   1,   1,   4,   9,   9,  12,  30,  39,  20,  32,  42,  55,  88,  73,  36,  48,  88,  85,  72,  57,  46,  31,  18,  20,  34,  39,  37,  39, 106],
 [ 26,   5,   2,   3,   8,  10,   6,   3,   4,   6,   7,   4,  13,  24,  34,  31,  28,  33,  23,  12,  33,  23,  43,  27,   6,   3,   3,   4,   9,  13,   9,   1,   0,   0,   1,   0,   0,   1,   3,   5,  16,  31,  44,  20,  32,  22,  18,  59,  66,  55,  71,  74,  58,  28,  25,  30,  34,  37,  43,  51,  48,  32,  33, 131],
 [ 24,   5,   1,   0,   3,   6,   6,   6,   4,   3,   5,   5,   5,   9,  20,  22,  17,  13,   7,   9,  17,  29,  37,  20,   4,   4,   1,   3,   9,  13,  11,   7,   3,   1,   1,   1,   1,   1,   1,   4,   9,  30,  32,  30,  12,  11,  14,  15,  31,  40,  51,  39,   8,   4,   8,  16,  18,  30,  44,  35,  25,  10,  30, 144],
 [ 21,   6,   1,   1,   2,   4,   6,   6,   5,   4,   3,   4,   4,   3,   1,   2,   2,   1,   3,   5,   8,  12,  14,   4,   6,   5,   1,   2,   7,  12,  10,  10,   7,   3,   1,   1,   1,   1,   1,   2,   6,  13,  24,  19,  19,   7,   3,   6,   3,   4,   5,   6,   3,   0,   3,   9,  22,  24,  22,  13,   8,   7,  32, 155],
 [ 20,   6,   2,   2,   2,   3,   5,   8,   6,   6,   4,   2,   3,   3,   3,   3,   5,   7,   9,  10,  18,  17,  11,   6,   7,   5,   1,   2,   5,   7,  14,   7,   8,   4,   3,   2,   1,   2,   3,   1,   4,   6,  15,  24,  37,  32,  22,  13,  10,  10,   7,   4,   2,   3,   4,   9,  13,  16,  13,   8,   5,   5,  35, 170],
 [ 21,   6,   2,   3,   3,   4,   5,   8,  10,   9,  10,   7,   8,   8,   7,   7,   9,  10,  13,  15,  19,  12,   6,  10,  11,   4,   1,   2,   2,   5,  11,   7,   5,   2,   2,   1,   2,   3,   3,   5,   2,   2,   6,  14,  27,  39,  41,  33,  24,  16,  11,   9,   8,   8,  10,  11,  12,   9,   9,   6,   4,   6,  35, 173],
 [ 19,   7,   3,   2,   2,   3,   5,   6,   9,  13,  13,  15,  15,  12,  10,  11,  12,  16,  16,  15,  11,   5,   5,  15,   7,   1,   1,   1,   1,   7,  13,   8,   3,   1,   1,   1,   1,   1,   1,   4,   2,   2,   1,   3,   7,  13,  22,  26,  26,  21,  16,  12,  10,  11,  11,   9,   7,   5,   2,   3,   2,   5,  41, 173],
 [ 22,   6,   1,   2,   2,   4,   3,   5,   8,   8,  11,  11,  11,  10,   9,  10,  12,  14,  13,  10,   6,   5,  13,  13,   5,   1,   1,   1,   3,  12,  15,   7,   2,   1,   1,   1,   1,   1,   2,   2,   1,   3,   1,   2,   0,   2,   4,   6,   7,   9,   8,   8,   7,   5,   4,   3,   2,   0,   1,   1,   2,   6,  41, 181],
 [ 21,   7,   2,   1,   3,   5,   5,   5,   4,   5,   6,   9,  11,   8,   8,   7,   8,   7,   3,   2,   4,   8,  13,   7,   1,   1,   1,   1,   2,  14,  14,   5,   2,   2,   3,   2,   1,   2,   0,   2,   1,   1,   2,   2,   0,   0,   0,   1,   1,   2,   1,   1,   1,   0,   1,   0,   0,   0,   0,   0,   1,   6,  42, 189],
 [ 22,   7,   1,   2,   2,   4,   6,   5,   5,   2,   4,   4,   3,   4,   3,   2,   2,   1,   1,   3,   7,   7,   8,   3,   1,   1,   2,   2,   5,  14,  15,   7,   3,   1,   2,   3,   2,   1,   2,   2,   1,   1,   2,   3,   0,   0,   0,   0,   0,   0,   0,   1,   0,   0,   0,   0,   0,   0,   0,   1,   1,   4,  43, 187],
 [ 19,   7,   2,   2,   2,   3,   4,   6,   9,   4,   4,   3,   0,   2,   1,   1,   1,   1,   2,   2,   7,   7,   5,   3,   2,   2,   2,   2,   7,  13,  15,   6,   3,   2,   1,   3,   3,   2,   4,   6,   3,   2,   3,   3,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   1,   4,  40, 188],
 [ 18,   7,   2,   2,   2,   3,   3,   4,   9,   7,   3,   1,   1,   1,   1,   1,   1,   1,   2,   3,   5,   9,   7,   7,   7,   5,   3,   2,   9,  12,  14,   4,   1,   2,   2,   4,   5,   4,   7,  12,  14,  15,  15,  10,   4,   2,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   4,  40, 183],
 [ 18,   7,   2,   2,   3,   3,   3,   4,   8,  10,   5,   2,   1,   1,   1,   1,   0,   2,   2,   3,   6,  18,  19,  14,   8,   5,   5,   5,   8,  12,  13,   5,   1,   1,   2,   4,   5,   3,   4,  10,  14,  20,  26,  29,  15,   5,   2,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   5,  41, 178],
 [ 16,   9,   3,   2,   3,   3,   3,   3,   6,   9,   9,   4,   2,   2,   0,   1,   1,   1,   4,   9,  18,  37,  25,  12,   4,   2,   5,   7,  10,  15,  14,   5,   1,   1,   1,   4,   5,   5,   2,   1,   3,   6,  13,  31,  32,  22,   6,   3,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   6,  40, 174],

 [ 13,  14,   3,   2,   2,   3,   3,   5,   4,   8,   7,   7,   2,   2,   1,   1,   2,   4,  13,  24,  41,  37,  14,   4,   2,   2,   3,   7,  19,  25,  19,   7,   2,   3,   6,   9,  12,   8,   2,   2,   4,   5,   4,  14,  23,  33,  19,   7,   2,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   2,   1,   5,  39, 174],
 [ 11,  13,   6,   3,   2,   3,   3,   6,   4,   6,   6,   5,   3,   3,   3,   3,   3,   9,  22,  39,  33,  16,   4,   4,   3,   2,   2,  10,  28,  26,  12,   2,   2,   3,   3,   5,  13,  11,   3,   3,   5,   7,   8,   4,  14,  27,  33,  16,   5,   1,   1,   0,   0,   0,   0,   0,   0,   1,   0,   3,   4,   4,  38, 169],
 [  7,  12,  10,   5,   3,   3,   3,   6,   7,   5,   5,   5,   3,   3,   2,   2,   4,  14,  27,  32,  14,   3,   4,   4,   1,   1,   2,   9,  28,  27,  10,   5,   7,   6,   4,   7,  11,  10,   4,   4,   1,   5,  11,   6,   3,  12,  27,  22,   8,   2,   1,   1,   0,   0,   0,   0,   0,   1,   2,   3,   2,   5,  36, 167],
 [  3,   9,  11,   7,   3,   2,   3,   5,   6,   8,   9,   7,   4,   3,   3,   3,   5,  14,  22,  15,   5,   3,   3,   3,   1,   0,   1,   6,  21,  29,  24,  15,   9,  11,  15,  18,  15,   7,   1,   1,   1,   7,  12,   6,   4,  10,  14,  19,   9,   4,   2,   1,   0,   0,   0,   0,   0,   1,   3,   4,   2,   4,  35, 163],
 [  1,   6,  11,  10,   3,   2,   2,   4,   4,   5,   8,   7,   5,   3,   1,   1,   4,   8,  14,   9,   7,   4,   2,   2,   2,   4,   8,  13,  21,  27,  26,  19,  13,  12,  14,  18,  24,  36,  39,  35,  20,  13,   8,   6,  14,  17,  16,  13,   6,   5,   2,   1,   1,   0,   0,   0,   2,   3,   3,   4,   2,   5,  35, 154],
 [  0,   3,   9,  12,   6,   3,   2,   2,   3,   3,   5,   8,   5,   4,   2,   2,   2,   4,   8,  12,  11,  10,   5,   2,   6,  12,  14,  15,  20,  24,  27,  25,  25,  25,  22,  19,  20,  19,  14,  21,  18,  10,   8,  14,  22,  15,  14,   8,   5,   2,   3,   2,   1,   0,   0,   2,   3,   4,   4,   5,   3,   5,  32, 144],
 [  0,   1,   7,  11,   9,   4,   3,   2,   2,   3,   5,  12,  16,  10,   3,   1,   1,   2,   4,   9,   9,  13,  13,  14,  15,   9,   8,  10,  18,  23,  17,  15,  19,  21,  19,  15,  25,  27,  23,  22,  20,  18,  22,  26,  18,  11,   5,   4,   3,   1,   1,   2,   2,   2,   2,   4,   4,   4,   2,   3,   3,   5,  30, 128],
 [  0,   0,   2,   8,  11,   6,   3,   3,   3,   2,   5,   7,   9,  18,   7,   3,   1,   1,   2,   3,   6,  11,  16,  23,  34,  38,  44,  56,  58,  58,  50,  42,  31,  24,  22,  31,  44,  60,  70,  56,  34,  22,  20,  15,   6,   3,   2,   1,   2,   1,   1,   1,   1,   2,   3,   5,  11,   9,   2,   3,   4,   7,  28, 115],
 [  0,   0,   1,   5,   9,   7,   4,   3,   3,   3,   4,   3,   6,  15,   9,   6,   4,   2,   1,   3,   4,   5,   5,  12,  21,  35,  38,  45,  49,  46,  47,  57,  56,  50,  44,  39,  29,  11,  16,  20,  16,   8,   6,   3,   2,   5,   6,   6,   4,   2,   0,   1,   2,   3,   4,  12,   9,   6,   7,   4,   4,   4,  24,  98],
 [  0,   0,   0,   2,   6,   8,   3,   3,   2,   2,   2,   2,   3,   6,   7,   7,   6,   7,   7,   8,   9,   8,   7,   5,   6,  10,  12,  14,  13,   4,   9,  15,  21,  21,  19,  16,   7,   2,   1,   2,   2,   2,   4,   4,   6,   9,  10,   9,   7,   5,   3,   3,   2,   5,   7,  14,   3,   2,   7,   3,   4,   3,  30,  90],
 [  0,   0,   0,   1,   4,   7,   6,   2,   2,   2,   1,   1,   3,   4,   5,   6,   8,   8,   9,   9,   8,   8,  11,  12,  10,  10,   5,   6,   5,   5,   3,   2,   2,   2,   1,   2,   3,   3,   3,   3,   4,   5,   6,   6,   9,  11,  14,  18,  20,  13,   6,   4,   5,   5,   6,   5,   4,   3,   4,   4,   1 ,  8,  61,  91],
 [  0,   0,   0,   0,   2,   5,   7,   5,   2,   1,   0,   1,   2,   4,   5,   6,   7,   9,  11,  11,  10,  10,  11,  14,  19,  19,  17,  16,  18,  14,  10,   6,   5,   4,   3,   4,   5,   7,  12,  19,  20,  20,  25,  28,  30,  30,  35,  47,  51,  41,  12,   4,   3,   4,   3,   1,   1,   2,   2,   3,   4,  44,  78,  72],
 [  0,   0,   0,   0,   0,   3,   6,   7,   4,   1,   1,   1,   1,   1,   3,   3,   4,   5,   8,  16,  19,  21,  20,  20,  23,  30,  34,  39,  44,  48,  44,  37,  35,  25,  18,  24,  36,  51,  63,  64,  69,  68,  69,  63,  49,  34,  28,  23,  26,  51,  23,   4,   1,   2,   1,   1,   0,   3,   2,   2,  26,  74,  75,  32],
 [  0,   0,   0,   0,   0,   1,   4,   6,   6,   3,   1,   1,   0,   1,   0,   0,   1,   2,   7,   7,   7,  10,  12,  13,  17,  25,  33,  36,  44,  59,  69,  78,  77,  68,  70,  71,  75,  80,  74,  66,  59,  55,  50,  42,  31,  20,  21,  17,   9,  35,  26,   5,   1,   2,   1,   1,   1,   2,   2,  15,  60,  78,  45,   3],
 [  0,   0,   0,   0,   0,   0,   1,   4,   6,   6,   3,   2,   1,   0,   0,   0,   1,   2,   5,   7,   6,   6,   6,   5,   6,   7,   9,  12,  13,  20,  31,  36,  39,  48,  46,  46,  42,  34,  26,  18,  14,  12,   7,  16,  27,  31,  22,  38,  32,  24,  19,   4,   2,   3,   3,   2,   1,   3,   5,  43,  71,  52,   8,   0],
 [  0,   0,   0,   0,   0,   0,   0,   2,   5,   6,   6,   4,   2,   1,   1,   0,   1,   1,   3,   6,   9,   9,  10,   7,   6,   6,   7,   8,   8,  11,  13,  12,   9,   8,  10,   8,   5,   6,  11,  17,  16,  14,  20,  32,  39,  22,   7,  10,  18,  11,   7,   8,   7,   5,   5,   3,   4,   8,  20,  54,  49,  18,   6,   1],

 [  0,   0,   0,   0,   0,   0,   0,   0,   2,   4,   5,   5,   4,   3,   2,   2,   1,   1,   2,   2,   2,   2,   2,   3,   5,   6,   8,  10,  13,  16,  16,  19,  19,  18,  16,  17,  17,  20,  24,  19,  20,  22,  22,  31,  18,   5,   2,   3,   6,   5,   5,   7,   9,   9,   5,   7,   7,   7,  25,  41,  26,   9,  14,  16],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   3,   4,   3,   4,   3,   2,   3,   2,   3,   3,   4,   2,   1,   1,   1,   1,   2,   4,   6,   7,   8,   8,   8,   7,   7,   6,  10,  11,   9,   7,   5,   5,   8,   9,   3,   3,   6,   6,   8,   6,   4,   4,   4,   4,   5,   4,   3,   4,  25,  28,   9,  11,  15,  23],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   3,   3,   2,   3,   2,   2,   1,   1,   2,   3,   4,   3,   2,   1,   2,   3,   4,   7,   7,   7,   6,   5,   5,   5,   5,   8,   8,   7,   9,   6,   3,   3,   4,   3,   3,   2,   4,   1,   1,   2,   2,   2,   2,   1,   1,   1,   3,  28,  21,   9,  14,   8,  25],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   2,   2,   1,   2,   1,   1,   1,   0,   1,   2,   2,   1,   1,   1,   2,   3,   6,  10,  12,  12,  11,   9,   9,  11,  13,  15,  14,  10,   5,   6,   4,   2,   1,   1,   1,   2,   1,   1,   1,   2,   1,   1,   1,   1,   2,   9,  27,  20,   8,  13,   8,  26],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   1,   1,   1,   1,   1,   0,   0,   0,   0,   1,   1,   1,   1,   1,   1,   2,   3,   5,   7,   7,   6,   4,   5,   5,   6,   5,   4,   5,   6,   2,   1,   1,   1,   1,   0,   1,   2,   4,   5,   3,   1,   2,   3,   2,  21,  29,  16,  11,  12,   9,  29],
 [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   0,   1,   1,   1,   1,   1,   0,   1,   1,   0,   1,   2,   4,   4,   4,   2,   1,   1,   2,   2,   2,   2,   4,   5,   5,   6,   6,   4,   1,   1,   1,   1,   1,   1,   3,   6,   7,   5,   2,   2,   6,   3,  11,  38,  28,  10,  20,  14,  10,  35],
 [  0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   2,   0,   1,   2,   1,   0,   0,   0,   1,   1,   1,   1,   1,   1,   2,   3,   3,   3,   3,   5,   6,   7,   4,   3,   4,   6,   5,   5,   6,   7,   5,   3,   3,   3,   2,   1,   0,   1,   5,   6,   5,   1,   3,  12,   9,  12,  40,  41,  19,  22,  34,  22,  16,  46],
 [  0,   0,   0,   0,   0,   0,   0,   0,   2,   3,   3,   2,   1,   2,   2,   0,   0,   0,   1,   1,   1,   0,   1,   1,   1,   4,   1,   0,   4,   8,  12,  18,  25,  26,  24,  22,  19,  10,   5,   4,   1,   3,   1,   3,   4,   3,   3,   7,   7,   6,   2,  11,  22,  13,  17,  42,  52,  30,  22,  53,  39,  15,  17,  57],
 [  0,   0,   0,   0,   0,   0,   0,   1,   4,   3,   4,   5,   2,   1,   2,   2,   1,   0,   0,   1,   2,   1,   0,   0,   1,   4,   4,   2,   4,   7,  12,  12,  12,  15,  17,  16,  10,   8,   4,   4,   2,   4,   7,  10,   7,   8,  15,  11,   8,   4,  20,  25,  16,  27,  47,  58,  38,  22,  60,  57,  25,   5,  18,  67],
 [  0,   0,   0,   0,   0,   0,   1,   1,   4,   3,   3,   6,   4,   2,   1,   2,   3,   2,   0,   0,   2,   3,   2,   1,   1,   2,   5,   5,   7,   7,  11,  14,  15,  15,  14,   9,   8,   8,   6,   3,   6,   9,   8,  11,  19,  27,  24,  10,  15,  28,  22,  23,  32,  58,  64,  35,  24,  72,  76,  41,   3,   2,  15,  71],
 [  0,   0,   0,   0,   0,   0,   1,   1,   5,   3,   1,   4,   6,   4,   3,   1,   2,   2,   2,   2,   0,   1,   3,   2,   2,   4,   7,  11,  17,  18,  19,  22,  25,  30,  32,  29,  23,  16,   5,   2,   7,  14,  25,  35,  42,  29,  12,  17,  22,  17,  28,  43,  68,  59,  23,  26,  81,  92,  61,   9,   1,   2,  15,  74],
 [  0,   0,   0,   0,   0,   0,   0,   2,   4,   4,   1,   4,   7,   9,   8,   6,   2,   2,   3,   3,   3,   2,   1,   1,   2,   1,   4,   8,  10,  12,  10,   8,   7,  11,  17,  21,  18,  14,  13,  14,  21,  30,  42,  38,  25,  16,  15,  17,  20,  27,  47,  59,  37,  10,  26,  84, 102,  79,  15,   7,   5,   2,  16,  73],
 [  0,   1,   0,   0,   0,   0,   1,   2,   4,   8,  14,  17,  16,  17,  22,  27,  32,  20,   6,   3,   2,   2,   2,   2,   1,   1,   1,   1,   0,   0,   1,   1,   1,   3,   4,   4,   6,  10,  15,  16,  22,  24,  19,  15,  13,  15,  12,  17,  32,  48,  37,  14,   2,  35,  88,  94,  78,  13,   2,   8,   9,   5,  16,  66],
 [  1,   1,   1,   1,   0,   0,   1,   3,  20,  27,  25,  24,  27,  33,  48,  60,  58,  54,  44,  23,   7,   3,   2,   1,   2,   3,   1,   2,   2,   1,   1,   0,   0,   0,   0,   0,   0,   2,   7,  13,  16,  16,  16,  15,  13,  13,  19,  28,  25,  12,   2,   4,  65, 111, 108,  78,  16,  11,  10,   9,  11,   8,  22,  59],
 [  2,   1,   1,   1,   1,   0,   4,  21,  22,  33,  31,  24,  29,  38,  38,  38,  47,  52,  46,  43,  31,  18,  10,   7,   5,   4,   3,   4,   3,   5,   8,  14,  17,  17,  19,  24,  25,  26,  28,  27,  27,  20,  16,  13,  17,  22,  21,  13,   4,   2,  28,  89, 127, 120,  98,  44,  33,  33,  33,  33,  26,  21,  34,  68],
 [  1,   1,   2,   2,   2,   3,  10,  23,  34,  55,  72,  84,  90, 100,  91,  92,  95,  74,  53,  42,  32,  23,  12,   9,   8,   8,  10,  10,  11,  12,  17,  15,  17,  21,  23,  21,  24,  32,  31,  32,  35,  34,  36,  33,  26,  15,   7,   3,   2,  11,  40,  79, 105, 120, 143, 137, 114, 108, 102,  94, 102,  76,  69,  67]];
img_test = np.asarray(img_test);                           # Convertir données à tableau numpy 

# Imprimer quelques images 
fig = plt.figure(figsize=(3,3));
plt.imshow(img_test, cmap="gray", interpolation=None); 
#plt.title("ANGRY (1)"); plt.tight_layout(); plt.show(); 
#sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** "); 
print(" Quantization de l'image               "); 
print("************************************** ");
scale = 0.972549; zero_point = -128;                       # Couche 6: tfl.quantize
img_test = quant_np(img_test, scale, zero_point, verbose=1); 
print(img_test); 
#sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** ");
print(" Conv2D( filtres=32, kernel=(5,5),     ");
print("   strides=(1,1), padding=SAME,        ");
print("   activation=relu);                   ");
print("************************************** ");

# Padding pour kernel de taille (5, 5)
entree = np.asarray(img_test);                             # Convertir données à tableau numpy
rangs, colonnes = entree.shape;                            # 64 x 64 pixels.
padding = np.zeros([rangs+4, colonnes+4])+ zero_point;     # Couche 06: tfl.quantize. zero_point.
padding[0+2:rangs+2, 0+2:colonnes+2] = entree;             # Taille du Kernel (5,5) 
entree = padding;

off_ent  = 128;                                            # Couche 06; tfl.quantize. input_offset = -zero_point.  
scale    = 0.05370437;                                     # Couche 16; quant_conv2d/: Relu, BiasAdd, Conv2D, ReadVariableOp/resource.
off_sor  = -18;                                            # Couche 16; output_offset = zero_point.

conv1_f0 = \
[[ -23,  73,  22, -70, -61], 
 [ -60,-106,-107,  40, -61], 
 [  84, -49,  45,  39, -25], 
 [  68, -59, -28,  44,  -8],  
 [  48,  -8,-127, -50,-109]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f0  = np.asarray(conv1_f0); 
conv1_b0  = -17;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M0  = 0.00095265 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M0), byref(M0), byref(shift) ); 
print("conv1_M0= ", conv1_M0, "M0= ", M0.value, "shift= ", shift.value); 
mc_0 = conv_k5_np(entree, conv1_f0, conv1_b0, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_0);  

conv1_f1 = \
[[  37, -32, -16,   8, -96], 
 [ -78,-100, -90,   8, -61], 
 [  15, -20, -19, -38, -61], 
 [ -22, -70,  -9, -58,  -3],  
 [-121,-127, -77, -32,  85]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f1  = np.asarray(conv1_f1); 
conv1_b1  = 75;                                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M1  = 0.00121104 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M1), byref(M0), byref(shift) ); 
print("conv1_M1= ", conv1_M1, "M0= ", M0.value, "shift= ", shift.value); 
mc_1 = conv_k5_np(entree, conv1_f1, conv1_b1, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_1);  

conv1_f2 = \
[[  31,   3, -13,  -8, -83], 
 [ -48, -25, -60,   3,  18], 
 [  52, -31, -65,-127, -72], 
 [ -55,  -8,  16,   5, -54],  
 [  50, -42, -82, -81,  -4]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f2  = np.asarray(conv1_f2); 
conv1_b2  = -81;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M2  = 0.00120699 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M2), byref(M0), byref(shift) ); 
print("conv1_M2= ", conv1_M2, "M0= ", M0.value, "shift= ", shift.value); 
mc_2 = conv_k5_np(entree, conv1_f2, conv1_b2, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_2);  

conv1_f3 = \
[[  73, 103,  50, 105,  40], 
 [ -21,  17, -92,  34,  19], 
 [  72,  -4,  66,  38, -60], 
 [   8,  46,  55, -86,  -6],  
 [ -56,-127, -49, -91, -51]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f3  = np.asarray(conv1_f3); 
conv1_b3  = -55;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M3  = 0.00079292 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M3), byref(M0), byref(shift) ); 
print("conv1_M3= ", conv1_M3, "M0= ", M0.value, "shift= ", shift.value); 
mc_3 = conv_k5_np(entree, conv1_f3, conv1_b3, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_3);  

conv1_f4 = \
[[  41,  38, -42, -47,  27], 
 [-127,-126, -74, -22,  68], 
 [  17,-101, -12,   7, -85], 
 [ -58, -96,  14,-114, -51],  
 [  51,  14, -40, -63,-115]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f4  = np.asarray(conv1_f4); 
conv1_b4  = -46;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M4  = 0.00098938 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M4), byref(M0), byref(shift) ); 
print("conv1_M4= ", conv1_M4, "M0= ", M0.value, "shift= ", shift.value); 
mc_4 = conv_k5_np(entree, conv1_f4, conv1_b4, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_4);  

conv1_f5 = \
[[ -14, -75,-113,  12,-112], 
 [  58, -92,  10, -67,-119], 
 [  54,  55, -41,  54, -13], 
 [  48,  40,  72,  10,-109],  
 [-127, -68,  48,  77,   0]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f5  = np.asarray(conv1_f5); 
conv1_b5  = -112;                                          # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M5  = 0.00100186 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M5), byref(M0), byref(shift) ); 
print("conv1_M5= ", conv1_M5, "M0= ", M0.value, "shift= ", shift.value); 
mc_5 = conv_k5_np(entree, conv1_f5, conv1_b5, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_5);  

conv1_f6 = \
[[ -16,  24, -22,  58,  20], 
 [  33,   2,-106,-127,  13], 
 [ -55, -95, -47, -73,  10], 
 [ -94, -67, -94,  38,  81],  
 [ -13, -85, -64, -60,  88]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f6  = np.asarray(conv1_f6); 
conv1_b6  = -69;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M6  = 0.00090816 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M6), byref(M0), byref(shift) ); 
print("conv1_M6= ", conv1_M6, "M0= ", M0.value, "shift= ", shift.value); 
mc_6 = conv_k5_np(entree, conv1_f6, conv1_b6, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_6);  

conv1_f7 = \
[[ -40, -48,  39,  19,  40], 
 [  48, -67, -22,  33,  75], 
 [ -86, -30, -50,  59,  51], 
 [ 110, -95, -93, -36,  34],  
 [   7,   7, -75,-123,-127]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f7  = np.asarray(conv1_f7); 
conv1_b7  = -91;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M7  = 0.00078258 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M7), byref(M0), byref(shift) ); 
print("conv1_M7= ", conv1_M7, "M0= ", M0.value, "shift= ", shift.value); 
mc_7 = conv_k5_np(entree, conv1_f7, conv1_b7, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_7);  

conv1_f8 = \
[[-124, -70, -44,-127,-127], 
 [ -52, -94,   5,  47,   9], 
 [  64,  94,   8, -44,  64], 
 [ -37,  18,  75,  26,  80],  
 [-113,  51,  33,-107, -11]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f8  = np.asarray(conv1_f8); 
conv1_b8  = -79;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M8  = 0.00084234 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M8), byref(M0), byref(shift) ); 
print("conv1_M8= ", conv1_M8, "M0= ", M0.value, "shift= ", shift.value); 
mc_8 = conv_k5_np(entree, conv1_f8, conv1_b8, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_8);  

conv1_f9 = \
[[  -4,   5,  62, -32,  71], 
 [ -48, -96,  43,  73, -28], 
 [ -59,-101,  52,  20,  41], 
 [  22, -91,  23, -60, -49],  
 [ -88,  41, -38, -33,-127]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f9  = np.asarray(conv1_f9); 
conv1_b9  = -44;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M9  = 0.00114011 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M9), byref(M0), byref(shift) ); 
print("conv1_M9= ", conv1_M9, "M0= ", M0.value, "shift= ", shift.value); 
mc_9 = conv_k5_np(entree, conv1_f9, conv1_b9, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_9);  

conv1_f10 = \
[[  24,  53, -58,-127, -56], 
 [  26, -14,-102, -24,  39], 
 [   5,  22, -73, -66, -51], 
 [ -27, -41, -62,  12,-105],  
 [   2, -49, -47, -52, -20]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f10 = np.asarray(conv1_f10); 
conv1_b10 = -14;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M10 = 0.00141139 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M10), byref(M0), byref(shift) ); 
print("conv1_M10= ", conv1_M10, "M0= ", M0.value, "shift= ", shift.value); 
mc_10 = conv_k5_np(entree, conv1_f10, conv1_b10, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_10);  

conv1_f11 = \
[[  85,  30, -60,  66,-127], 
 [ -58,  91,  96, 117, -11], 
 [-102,  28,  13, 105, -53], 
 [ -71,  27,-127,  34,  51],  
 [ -12, -52, -97, -32,  34]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f11 = np.asarray(conv1_f11); 
conv1_b11 = -188;                                          # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M11 = 0.00074154 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M11), byref(M0), byref(shift) ); 
print("conv1_M10= ", conv1_M11, "M0= ", M0.value, "shift= ", shift.value); 
mc_11 = conv_k5_np(entree, conv1_f11, conv1_b11, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_11);  

conv1_f12 = \
[[ -29, -54,  32,   5,  53], 
 [  32,  84,  31,  -1, -43], 
 [  35, -41, -84, -20, -42], 
 [  -9,-120,-116,  -5, -40],  
 [-127,  -6,   7, -49,  -9]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f12 = np.asarray(conv1_f12); 
conv1_b12 = -103;                                          # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M12 = 0.00111998 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M12), byref(M0), byref(shift) ); 
print("conv1_M12= ", conv1_M12, "M0= ", M0.value, "shift= ", shift.value); 
mc_12 = conv_k5_np(entree, conv1_f12, conv1_b12, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_12);  

conv1_f13 = \
[[ -89, -84,  -2, -50,  61], 
 [   1,  -2, -71,   2,  46], 
 [-127,  57, -50, -57,  14], 
 [   9, -13,  32, -36,  10],  
 [  58,  71, -96, -80,-118]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f13 = np.asarray(conv1_f13); 
conv1_b13 = -58;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M13 = 0.00104284 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M13), byref(M0), byref(shift) ); 
print("conv1_M13= ", conv1_M13, "M0= ", M0.value, "shift= ", shift.value); 
mc_13 = conv_k5_np(entree, conv1_f13, conv1_b13, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_13);  

conv1_f14 = \
[[  22,  -3, -47, -47,  36], 
 [  -1, -57, -25,  54,   8], 
 [  19,  32,  29, -35,  59], 
 [   1,  20,  18, -35, -17],  
 [ -16, -58,-107,-127,-119]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f14 = np.asarray(conv1_f14); 
conv1_b14 = -78;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M14 = 0.00139356 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M14), byref(M0), byref(shift) ); 
print("conv1_M13= ", conv1_M13, "M0= ", M0.value, "shift= ", shift.value); 
mc_14 = conv_k5_np(entree, conv1_f14, conv1_b14, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_14);  

conv1_f15 = \
[[  36, -14,  39,  15, -66], 
 [ -75,  24,  -9,  18,  21], 
 [ -91,-127, -68, -79,   2], 
 [ -32,  -5,  12,   2, -93],  
 [ -49, -36, -33,   4,  15]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f15 = np.asarray(conv1_f15); 
conv1_b15 = -111;                                          # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M15 = 0.00125942 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M15), byref(M0), byref(shift) ); 
print("conv1_M15= ", conv1_M15, "M0= ", M0.value, "shift= ", shift.value); 
mc_15 = conv_k5_np(entree, conv1_f15, conv1_b15, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_15);  

conv1_f16 = \
[[ -12, -68,-126, -17,  33], 
 [  50,  -1,-117, -74,-110], 
 [ -82, -33,-108, -46,  -4], 
 [  34, -59, -35,  29,  83],  
 [ -44,  35,  25,  21,  17]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f16 = np.asarray(conv1_f16); 
conv1_b16 = -60;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M16 = 0.0010955 / scale;                             # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M16), byref(M0), byref(shift) ); 
print("conv1_M16= ", conv1_M16, "M0= ", M0.value, "shift= ", shift.value); 
mc_16 = conv_k5_np(entree, conv1_f16, conv1_b16, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_16);  

conv1_f17 = \
[[  40,  -4,   0,  -9,  27], 
 [  34, -23,  42, -41, -34], 
 [ -37, -18,-127, -73,   4], 
 [ -69, -50, -82, -87, -61],  
 [  26,  34, -35,  54, -33]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f17 = np.asarray(conv1_f17); 
conv1_b17 = -130;                                          # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M17 = 0.00128346 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M17), byref(M0), byref(shift) ); 
print("conv1_M17= ", conv1_M17, "M0= ", M0.value, "shift= ", shift.value); 
mc_17 = conv_k5_np(entree, conv1_f17, conv1_b17, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_17);  

conv1_f18 = \
[[ -51, -94,  -3, -96, -30], 
 [ -42,  -6, -81, -37, -46], 
 [   3,   1, -81,   9, -71], 
 [  -3, -79,  20,  -4,  -8],  
 [-127, -94,   0, -65, -17]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f18 = np.asarray(conv1_f18); 
conv1_b18 = -100;                                          # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M18 = 0.00131637 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M18), byref(M0), byref(shift) ); 
print("conv1_M18= ", conv1_M18, "M0= ", M0.value, "shift= ", shift.value); 
mc_18 = conv_k5_np(entree, conv1_f18, conv1_b18, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_18);  

conv1_f19 = \
[[ -14, -79,  77,  60,-127], 
 [   0,  44, -22,  78, -46], 
 [  60,   1,-107, -51, -23], 
 [  -4, -59, -55,  34,  68],  
 [ -61,-109,  28,  -6,  18]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f19 = np.asarray(conv1_f19); 
conv1_b19 = -30;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M19 = 0.00101527 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M19), byref(M0), byref(shift) ); 
print("conv1_M19= ", conv1_M19, "M0= ", M0.value, "shift= ", shift.value); 
mc_19 = conv_k5_np(entree, conv1_f19, conv1_b19, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_19);  

conv1_f20 = \
[[ -42,  30, -57, -14, -70], 
 [ -34,  41, -19,  11, -39], 
 [  21, -41, -40,-127,-127], 
 [  76, -24,  31, -76, -92],  
 [ -44, -20,  -9, -34,  21]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f20 = np.asarray(conv1_f20); 
conv1_b20 = -63;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M20 = 0.00119341 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M20), byref(M0), byref(shift) ); 
print("conv1_M20= ", conv1_M20, "M0= ", M0.value, "shift= ", shift.value); 
mc_20 = conv_k5_np(entree, conv1_f20, conv1_b20, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_20);  

conv1_f21 = \
[[  80,  68, -34,  51, -43], 
 [ -14, -74,  52,  12,  -6], 
 [-127, -73,  14, -85, -78], 
 [ -21,  -9, -81,   2, -60],  
 [  72,  23,  13,  40,   3]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f21 = np.asarray(conv1_f21); 
conv1_b21 = -68;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M21 = 0.00104029 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M21), byref(M0), byref(shift) ); 
print("conv1_M21= ", conv1_M21, "M0= ", M0.value, "shift= ", shift.value); 
mc_21 = conv_k5_np(entree, conv1_f21, conv1_b21, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_21);  

conv1_f22 = \
[[  92, -46, -53, -70, -41], 
 [ -29, -50,  43, -40,  63], 
 [ 127, -42, -70, -72, -31], 
 [-108,   8, -45,-119,  -9],  
 [  -7, -60, -43,  12, -59]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f22 = np.asarray(conv1_f22); 
conv1_b22 = 0;                                             # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M22 = 0.00101142 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M22), byref(M0), byref(shift) ); 
print("conv1_M22= ", conv1_M22, "M0= ", M0.value, "shift= ", shift.value); 
mc_22 = conv_k5_np(entree, conv1_f22, conv1_b22, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_22);  

conv1_f23 = \
[[ -12, -17,-127, -25, -98], 
 [  15, -11, -68, -30, -18], 
 [  52,   6, -40,  76,  72], 
 [  54, -66, -98, -70,  63],  
 [  59,   5,-122,-115,  50]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f23 = np.asarray(conv1_f23); 
conv1_b23 = -86;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M23 = 0.00102077 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M23), byref(M0), byref(shift) ); 
print("conv1_M23= ", conv1_M23, "M0= ", M0.value, "shift= ", shift.value); 
mc_23 = conv_k5_np(entree, conv1_f23, conv1_b23, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_23);  

conv1_f24 = \
[[   8, -48, -31, -35, -72], 
 [-117,  11,  31, -64, -86], 
 [-127,  -3,  20, -40,-103], 
 [ -12,-124,  30, -20, -38],  
 [  21, -62,-107,  43,   7]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f24 = np.asarray(conv1_f24); 
conv1_b24 = -83;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M24 = 0.00100477 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M24), byref(M0), byref(shift) ); 
print("conv1_M24= ", conv1_M24, "M0= ", M0.value, "shift= ", shift.value); 
mc_24 = conv_k5_np(entree, conv1_f24, conv1_b24, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_24);  

conv1_f25 = \
[[-115, -98,  79, 117, -69], 
 [ -99,  26,  86, -84,-127], 
 [ -98,  58, -83,  66,  16], 
 [ 106, 107,  94,  27,  47],  
 [  48, 106,  27, -53, -86]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f25 = np.asarray(conv1_f25); 
conv1_b25 = -170;                                          # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M25 = 0.00081471 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M25), byref(M0), byref(shift) ); 
print("conv1_M25= ", conv1_M25, "M0= ", M0.value, "shift= ", shift.value); 
mc_25 = conv_k5_np(entree, conv1_f25, conv1_b25, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_25);  

conv1_f26 = \
[[  -3,  66,  19,  31,  17], 
 [ -37, -24,  47, -10,  47], 
 [-122,-127,-103,  33, -88], 
 [ -87,   8, -84,  31, -63],  
 [ -49,-108, -74,  67, -74]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f26 = np.asarray(conv1_f26); 
conv1_b26 = -56;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M26 = 0.00116755 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M26), byref(M0), byref(shift) ); 
print("conv1_M26= ", conv1_M26, "M0= ", M0.value, "shift= ", shift.value); 
mc_26 = conv_k5_np(entree, conv1_f26, conv1_b26, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_26);  

conv1_f27 = \
[[  42,  72,  17,  85,-112], 
 [  79, -72,  45,  25, -69], 
 [ -45,  55,  72,  34,-127], 
 [  98, -55,  42, -88,-104],  
 [ -96,-127, -72, -41, -92]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f27 = np.asarray(conv1_f27); 
conv1_b27 = -87;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M27 = 0.00076776 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M27), byref(M0), byref(shift) ); 
print("conv1_M27= ", conv1_M27, "M0= ", M0.value, "shift= ", shift.value); 
mc_27 = conv_k5_np(entree, conv1_f27, conv1_b27, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_27);  

conv1_f28 = \
[[ -37,-108,  53,  19,  -2], 
 [   0,  41, -49, -81,  28], 
 [ -64, -79,  63,  35,  -7], 
 [   6,   3,   2, -39,-127],  
 [-102,  30,-103, -50, -57]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f28 = np.asarray(conv1_f28); 
conv1_b28 = -132;                                          # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M28 = 0.00097219 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M28), byref(M0), byref(shift) ); 
print("conv1_M28= ", conv1_M28, "M0= ", M0.value, "shift= ", shift.value); 
mc_28 = conv_k5_np(entree, conv1_f28, conv1_b28, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_28);  

conv1_f29 = \
[[  68,  74, -44,  28, -10], 
 [  36, -99,-127,  31,  -5], 
 [  88, -11,  24,  57, -29], 
 [ 100, -75, -24,  64, -62],  
 [ -12, -99, -84, -27, -87]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f29 = np.asarray(conv1_f29); 
conv1_b29 = -104;                                          # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M29 = 0.00095343 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M29), byref(M0), byref(shift) ); 
print("conv1_M29= ", conv1_M29, "M0= ", M0.value, "shift= ", shift.value); 
mc_29 = conv_k5_np(entree, conv1_f29, conv1_b29, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_29);  

conv1_f30 = \
[[ -48,  25, -48,  32,   4], 
 [ -59,  26, -46,  43,   6], 
 [ -57,-127,  25, -99,-125], 
 [ -45,-103,   7,  44,  52],  
 [ -74, -44,  43,  71,   3]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f30 = np.asarray(conv1_f30); 
conv1_b30 = -84;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M30 = 0.00102816 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M30), byref(M0), byref(shift) ); 
print("conv1_M30= ", conv1_M30, "M0= ", M0.value, "shift= ", shift.value); 
mc_30 = conv_k5_np(entree, conv1_f30, conv1_b30, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_30);  

conv1_f31 = \
[[  42,  85, -89, -60, -99], 
 [ -31, -14,  59,  73,  11], 
 [ -86, -85,-124,-127, -47], 
 [ -55,-120, -82, -53, -17],  
 [ 100,   1,   7,  45, -63]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
conv1_f31 = np.asarray(conv1_f31); 
conv1_b31 = -118;                                          # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
conv1_M31 = 0.000786 / scale;                              # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
shift = c_int(); M0 = c_int(); 
monbib.QuantizeMultiplier( c_double(conv1_M31), byref(M0), byref(shift) ); 
print("conv1_M31= ", conv1_M31, "M0= ", M0.value, "shift= ", shift.value); 
mc_31 = conv_k5_np(entree, conv1_f31, conv1_b31, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_31);  
#sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** "); 
print(" MaxPooling( pool_size=(2,2),          "); 
print("   strides=(2,2) );                    "); 
print("************************************** "); 
mp1_0  = maxpool_np(mc_0, verbose=1); mp1_1  = maxpool_np(mc_1, verbose=1); mp1_2  = maxpool_np(mc_2, verbose=1); mp1_3  = maxpool_np(mc_3, verbose=1); mp1_4  = maxpool_np(mc_4, verbose=1);
mp1_5  = maxpool_np(mc_5, verbose=1); mp1_6  = maxpool_np(mc_6, verbose=1); mp1_7  = maxpool_np(mc_7, verbose=1); mp1_8  = maxpool_np(mc_8, verbose=1); mp1_9  = maxpool_np(mc_9, verbose=1);
mp1_10 = maxpool_np(mc_10,verbose=1); mp1_11 = maxpool_np(mc_11,verbose=1); mp1_12 = maxpool_np(mc_12,verbose=1); mp1_13 = maxpool_np(mc_13,verbose=1); mp1_14 = maxpool_np(mc_14,verbose=1);
mp1_15 = maxpool_np(mc_15,verbose=1); mp1_16 = maxpool_np(mc_16,verbose=1); mp1_17 = maxpool_np(mc_17,verbose=1); mp1_18 = maxpool_np(mc_18,verbose=1); mp1_19 = maxpool_np(mc_19,verbose=1);
mp1_20 = maxpool_np(mc_20,verbose=1); mp1_21 = maxpool_np(mc_21,verbose=1); mp1_22 = maxpool_np(mc_22,verbose=1); mp1_23 = maxpool_np(mc_23,verbose=1); mp1_24 = maxpool_np(mc_24,verbose=1);
mp1_25 = maxpool_np(mc_25,verbose=1); mp1_26 = maxpool_np(mc_26,verbose=1); mp1_27 = maxpool_np(mc_27,verbose=1); mp1_28 = maxpool_np(mc_28,verbose=1); mp1_29 = maxpool_np(mc_29,verbose=1);
mp1_30 = maxpool_np(mc_30,verbose=1); mp1_31 = maxpool_np(mc_31,verbose=1);
#print("mp1_0: \n", mp1_0); print("mp1_1: \n", mp1_1); print("mp1_2: \n", mp1_2);

# Vérifier résultats
aux00 = np.zeros([32, 32]);                                # 32 rangs, 32 filtres 
aux00[:, 0] =  mp1_0[0,:]; aux00[:, 1] =  mp1_1[0,:]; aux00[:, 2] =  mp1_2[0,:]; aux00[:, 3] =  mp1_3[0,:]; aux00[:, 4] =  mp1_4[0, :]; aux00[:, 5] =  mp1_5[0, :]; aux00[:, 6] =  mp1_6[0,:]; aux00[:, 7] =  mp1_7[0,:]; 
aux00[:, 8] =  mp1_8[0,:]; aux00[:, 9] =  mp1_9[0,:]; aux00[:,10] = mp1_10[0,:]; aux00[:,11] = mp1_11[0,:]; aux00[:,12] = mp1_12[0, :]; aux00[:,13] = mp1_13[0, :]; aux00[:,14] = mp1_14[0,:]; aux00[:,15] = mp1_15[0,:]; 
aux00[:,16] = mp1_16[0,:]; aux00[:,17] = mp1_17[0,:]; aux00[:,18] = mp1_18[0,:]; aux00[:,19] = mp1_19[0,:]; aux00[:,20] = mp1_20[0, :]; aux00[:,21] = mp1_21[0, :]; aux00[:,22] = mp1_22[0,:]; aux00[:,23] = mp1_23[0,:]; 
aux00[:,24] = mp1_24[0,:]; aux00[:,25] = mp1_25[0,:]; aux00[:,26] = mp1_26[0,:]; aux00[:,27] = mp1_27[0,:]; aux00[:,28] = mp1_28[0, :]; aux00[:,29] = mp1_29[0, :]; aux00[:,30] = mp1_30[0,:]; aux00[:,31] = mp1_31[0,:]; 
print("aux00: \n", aux00);
sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** ");
print(" Conv2D( filtres=64, kernel=(5,5),     ");
print("   strides=(1,1), padding=SAME,        ");
print("   activation=relu);                   ");
print("************************************** ");

# Padding pour kernel de taille (5, 5)
entree = np.asarray(img_test);                             # Convertir données à tableau numpy
#rangs, colonnes = entree.shape;                            # 64 x 64 pixels.
#padding = np.zeros([rangs+4, colonnes+4])+ zero_point;     # Couche 06: tfl.quantize. zero_point.
#padding[0+2:rangs+2, 0+2:colonnes+2] = entree;             # Taille du Kernel (5,5) 
#entree = padding;

#off_ent  = 128;                                            # Couche 06; tfl.quantize. input_offset = -zero_point.  
#scale    = 0.05370437;                                     # Couche 16; quant_conv2d/: Relu, BiasAdd, Conv2D, ReadVariableOp/resource.
#off_sor  = -18;                                            # Couche 16; output_offset = zero_point.

#conv1_f0 = \
#[[ -23,  73,  22, -70, -61], 
# [ -60,-106,-107,  40, -61], 
# [  84, -49,  45,  39, -25], 
# [  68, -59, -28,  44,  -8],  
# [  48,  -8,-127, -50,-109]];                              # Couche 15; quant_conv2d/: Conv2D, LastValueQuant/FakeQuantWithMinMaxVarsPerChannel.
#conv1_f0  = np.asarray(conv1_f0); 
#conv1_b0  = -17;                                           # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Tensor.
#conv1_M0  = 0.00095625 / scale;                            # Couche 07; quant_conv2d/: BiasAdd/ReadVariableOp/resource. Scales
#shift = c_int(); M0 = c_int(); 
#monbib.QuantizeMultiplier( c_double(conv1_M0), byref(M0), byref(shift) ); 
#print("conv1_M0= ", conv1_M0, "M0= ", M0.value, "shift= ", shift.value); 
#mc_0 = conv_k5_np(entree, conv1_f0, conv1_b0, M0.value, shift.value, off_ent, off_sor, verbose=1); # print(mc_0);  

sys.exit(0);                                              # Terminer l'execution


#aux00 = np.zeros([64, 32]); aux01 = np.zeros([64, 32]); aux02 = np.zeros([64, 32]); aux03 = np.zeros([64, 32]); aux04 = np.zeros([64, 32]); aux05 = np.zeros([64, 32]); aux06 = np.zeros([64, 32]); aux07  = np.zeros([64, 32]);
#aux08 = np.zeros([64, 32]); aux09 = np.zeros([64, 32]); aux10 = np.zeros([64, 32]); aux11 = np.zeros([64, 32]); aux12 = np.zeros([64, 32]); aux13 = np.zeros([64, 32]); aux14 = np.zeros([64, 32]); aux15 = np.zeros([64, 32]); 
#aux16 = np.zeros([64, 32]); aux17 = np.zeros([64, 32]); aux18 = np.zeros([64, 32]); aux19 = np.zeros([64, 32]); aux20 = np.zeros([64, 32]); aux21 = np.zeros([64, 32]); aux22 = np.zeros([64, 32]); aux23 = np.zeros([64, 32]);
#aux24 = np.zeros([64, 32]); aux25 = np.zeros([64, 32]); aux26 = np.zeros([64, 32]); aux27 = np.zeros([64, 32]); aux28 = np.zeros([64, 32]); aux29 = np.zeros([64, 32]); aux30 = np.zeros([64, 32]); aux31 = np.zeros([64, 32]);
#aux32 = np.zeros([64, 32]); aux33 = np.zeros([64, 32]); aux34 = np.zeros([64, 32]); aux35 = np.zeros([64, 32]); aux36 = np.zeros([64, 32]); aux37 = np.zeros([64, 32]); aux38 = np.zeros([64, 32]); aux39 = np.zeros([64, 32]); 
#aux40 = np.zeros([64, 32]); aux41 = np.zeros([64, 32]); aux42 = np.zeros([64, 32]); aux43 = np.zeros([64, 32]); aux44 = np.zeros([64, 32]); aux45 = np.zeros([64, 32]); aux46 = np.zeros([64, 32]); aux47 = np.zeros([64, 32]);
#aux48 = np.zeros([64, 32]); aux49 = np.zeros([64, 32]); aux50 = np.zeros([64, 32]); aux51 = np.zeros([64, 32]); aux52 = np.zeros([64, 32]); aux53 = np.zeros([64, 32]); aux54 = np.zeros([64, 32]); aux55 = np.zeros([64, 32]); 
#aux56 = np.zeros([64, 32]); aux57 = np.zeros([64, 32]); aux58 = np.zeros([64, 32]); aux60 = np.zeros([64, 32]); aux61 = np.zeros([64, 32]); aux62 = np.zeros([64, 32]); aux63 = np.zeros([64, 32]); aux64 = np.zeros([64, 32]);


# aux00
#aux00[:, 0] =  mc_0[0,:]; aux00[:, 1] =  mc_1[0,:]; aux00[:, 2] =  mc_2[0,:]; aux00[:, 3] =  mc_3[0,:]; aux00[:, 4] =  mc_4[0, :]; aux00[:, 5] =  mc_5[0, :]; aux00[:, 6] =  mc_6[0,:]; aux00[:, 7] =  mc_7[0,:]; 
#aux00[:, 8] =  mc_8[0,:]; aux00[:, 9] =  mc_9[0,:]; aux00[:,10] = mc_10[0,:]; aux00[:,11] = mc_11[0,:]; aux00[:,12] = mc_12[0, :]; aux00[:,13] = mc_13[0, :]; aux00[:,14] = mc_14[0,:]; aux00[:,15] = mc_15[0,:]; 
#aux00[:,16] = mc_16[0,:]; aux00[:,17] = mc_17[0,:]; aux00[:,18] = mc_18[0,:]; aux00[:,19] = mc_19[0,:]; aux00[:,20] = mc_20[0, :]; aux00[:,21] = mc_21[0, :]; aux00[:,22] = mc_22[0,:]; aux00[:,23] = mc_23[0,:]; 
#aux00[:,24] = mc_24[0,:]; aux00[:,25] = mc_25[0,:]; aux00[:,26] = mc_26[0,:]; aux00[:,27] = mc_27[0,:]; aux00[:,28] = mc_28[0, :]; aux00[:,29] = mc_29[0, :]; aux00[:,30] = mc_30[0,:]; aux00[:,31] = mc_31[0,:]; 
#aux00[:,32] = mc_32[0,:]; aux00[:,33] = mc_33[0,:]; aux00[:,34] = mc_34[0,:]; aux00[:,35] = mc_35[0,:]; aux00[:,36] = mc_36[0, :]; aux00[:,37] = mc_37[0, :]; aux00[:,38] = mc_38[0,:]; aux00[:,39] = mc_39[0,:]; 
#aux00[:,40] = mc_40[0,:]; aux00[:,41] = mc_41[0,:]; aux00[:,42] = mc_42[0,:]; aux00[:,43] = mc_43[0,:]; aux00[:,44] = mc_44[0, :]; aux00[:,45] = mc_45[0, :]; aux00[:,46] = mc_46[0,:]; aux00[:,47] = mc_47[0,:]; 
#aux00[:,48] = mc_48[0,:]; aux00[:,49] = mc_49[0,:]; aux00[:,50] = mc_50[0,:]; aux00[:,51] = mc_51[0,:]; aux00[:,52] = mc_52[0, :]; aux00[:,53] = mc_53[0, :]; aux00[:,54] = mc_54[0,:]; aux00[:,55] = mc_55[0,:]; 
#aux00[:,56] = mc_56[0,:]; aux00[:,57] = mc_57[0,:]; aux00[:,58] = mc_58[0,:]; aux00[:,59] = mc_59[0,:]; aux00[:,60] = mc_60[0, :]; aux00[:,61] = mc_61[0, :]; aux00[:,62] = mc_62[0,:]; aux00[:,63] = mc_63[0,:]; 


#print("aux00: \n", aux00);





print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
