-------------------------------------------------------------------

     =========================================================
     fer_sys_dev/00_sw/02_M6_jaffe/01_M6_jaffe_tflite - Développement
          en software d'un système pour applications FER
     =========================================================

                         Liste des fichiers
                         ---------------

Fichiers pour l'étudier l'inference de modèle M6 en Tensorflow  Lite.

00_quant_M6_tflite_6emo.py
 - Module pour faire l'entraînement et la quantization du modèle M6 + jaffe 6 émotions. Ce logiciel sauvegardé le modèle quantizé en le dossier models. Le log est enregistré en entrainement.log, graphique_entrainement.pdf et model.png.

01_M6_tflite_param.py
 - Module pour imprimer les résultats de couches et ses paramètres. Les résultats sont enregistrées en M6_tflite_param.log.

02_M6_tflite_inf_np.py
 - Module pour réproduire l'inference du modèle M6 avec Numpy (PAS TERMINÉ).

03_M6_tflite_inf_npT.py
 - Module pour réproduire l'inference du modèle M6 avec Numpy en utilisant tensors 4D. L'inference est correcte et ses résultats sont dans M6_tflite_inf_npT.log.

mes_fonctions.py
 - Fonctions necessaires pour faire l'inference.

mes_fon_CPP/
 - Fonctions necessaires en CPP pour faire l'inference.

models/
 - Dossier avec modèles en format HDF5.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
