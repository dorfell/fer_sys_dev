#!/bin/bash

echo "--------------------------------"
echo "file: 00_report_M6_jaffe_6emo.sh "
echo "Créer un reportage de           "
echo "00_M6_jaffe_6emo.py             "
echo "--------------------------------"

echo "--> Entraînement du modèle ..."
rm *.log *.png 
python3 00_M6_jaffe_baseline.py >>  entrainement.log

echo "--> Entrainement.log à pdf ..."
#enscript -p entrainement.ps entrainement.log
#ps2pdf entrainement.ps entrainement.pdf
enscript entrainement.log --output=- | ps2pdf - >  entrainement.pdf

echo "--> Convertir le modèle à pdf ..."
convert model.png model.pdf

echo "--> Ajouter tout à un seul pdf ..."
pdftk entrainement.pdf model.pdf graphique_entrainement.pdf cat output 00_entrainement_M6_jaffe_baseline_20230209.pdf

echo "--> Éliminer les fichiers crées ..."
rm *.log *.png entrainement.pdf model.pdf graphique_entrainement.pdf 
echo "  "
