# -*- coding: utf-8 -*-
## 
# @file    summary_plot_loss.py 
# @brief   Plot for M6 loss. 
# @details Plot a summary of data from tests. 
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2023/02/18
# @version 0.1
"""@package docstring
"""


import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16});

import pandas as pd
import seaborn as sns
#sns.set_theme(style="whitegrid")
#sns.set_theme(style="darkgrid");
sns.set_style("darkgrid", {'grid.linestyle': '--'});




df = pd.DataFrame(
  {'group': ["learning rates", "learning rates", "learning rates", 
             "pooling layers", "pooling layers",
             "dropouts", "dropouts", "dropouts", "dropouts", "dropouts"],
   'param': ["0.01", "0.001", "0.0001",
             "MaxPooling2D", "AveragePooling2D",
             "0%", "20%", "40%", "60%", "80%"],
   'loss':  [ 2.08, 0.652, 0.808,
              0.652, 0.8425,  
             0.652, 0.5583, 0.4895, 0.998, 1.7775]  
  }  );


g = sns.catplot(kind='bar',  data=df, col='group', x='param', y='loss',
                #hue='param', palette='Paired', dodge=False, sharex=False);
                #hue='param', palette='hls', dodge=False, sharex=False);
                hue='param', palette='tab10', dodge=False, sharex=False);

g.set_axis_labels("", "loss (u)");
#g.set_xticklabels(["learning rate", "pooling layer"]);
g.set_titles("{col_name}", y=-0.15);
#g.fig.suptitle("M6 loss w.r.t parameters");
g.set(ylim=(0, 2.2));
plt.tight_layout();


# iterate through axes
for ax in g.axes.ravel():
  # add annotations
  for c in ax.containers:
    labels = [f'{(v.get_height()):.1f}' for v in c]
    ax.bar_label(c, labels=labels, label_type='edge')
  ax.margins(y=0.2)


plt.show()

