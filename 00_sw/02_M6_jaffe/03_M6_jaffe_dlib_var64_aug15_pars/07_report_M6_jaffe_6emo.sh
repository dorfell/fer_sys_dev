#!/bin/bash

echo "--------------------------------"
echo "file: 07_report_M6_jaffe_6emo.sh "
echo "Créer un reportage de           "
echo "07_M6_jaffe_drop80.py           "
echo "--------------------------------"

echo "--> Entraînement du modèle ..."
rm *.log *.png 
python3 07_M6_jaffe_drop80.py >>  entrainement.log

echo "--> Entrainement.log à pdf ..."
#enscript -p entrainement.ps entrainement.log
#ps2pdf entrainement.ps entrainement.pdf
enscript entrainement.log --output=- | ps2pdf - >  entrainement.pdf

echo "--> Convertir le modèle à pdf ..."
convert model.png model.pdf

echo "--> Ajouter tout à un seul pdf ..."
pdftk entrainement.pdf model.pdf graphique_entrainement.pdf cat output 07_entrainement_M6_jaffe_drop80_20230211.pdf

echo "--> Éliminer les fichiers crées ..."
rm *.log *.png entrainement.pdf model.pdf graphique_entrainement.pdf
echo "  "
