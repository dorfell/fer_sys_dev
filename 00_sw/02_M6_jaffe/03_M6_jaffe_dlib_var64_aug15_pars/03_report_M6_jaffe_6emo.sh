#!/bin/bash

echo "--------------------------------"
echo "file: 03_report_M6_jaffe_6emo.sh "
echo "Créer un reportage de           "
echo "03_M6_jaffe_avgpool.py          "
echo "--------------------------------"

echo "--> Entraînement du modèle ..."
rm *.log *.png 
python3 03_M6_jaffe_avgpool.py >>  entrainement.log

echo "--> Entrainement.log à pdf ..."
#enscript -p entrainement.ps entrainement.log
#ps2pdf entrainement.ps entrainement.pdf
enscript entrainement.log --output=- | ps2pdf - >  entrainement.pdf

echo "--> Convertir le modèle à pdf ..."
convert model.png model.pdf

echo "--> Ajouter tout à un seul pdf ..."
pdftk entrainement.pdf model.pdf graphique_entrainement.pdf cat output 03_entrainement_M6_jaffe_avgpool_20230211.pdf

echo "--> Éliminer les fichiers crées ..."
rm *.log *.png entrainement.pdf model.pdf graphique_entrainement.pdf
echo "  "
