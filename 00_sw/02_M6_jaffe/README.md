-------------------------------------------------------------------

     =========================================================
     fer_sys_dev/00_sw/02_M6_jaffe - Développement en software
                 d'un système pour applications FER
     =========================================================

                         Liste des fichiers
                         ---------------

Fichiers pour le preuves de modèle M6 avec Tensorflow et Tensorflow Lite.

00_M6_jaffe_dlib_var64_aug15/
 - Module pour faire l'entraînement du modèle M6 en Tensorflow  avec Jaffe + pré-traitement avec dlib + lbp-var, et redimensionnement à 64x64 pixels en utilisant Lanczos4. Les entraînements ont été fait pour 6 émotions (sans neutral) et 7 émotions.

01_M6_jaffe_tflite/
 - Module pour étudier l'inference de modèle M6 avec jaffe du 6 émotions, en réprésentation de nombres entiers avec tflite. 

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
