# -*- coding: utf-8 -*-
## 
# @file    mes_fonctions.py 
# @brief   Module des fonctions nécessaires  
# @details Module qui contient les fonctions pour lire les 
#          modèles crée avec TensorFlow en format hdf5. 
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/04/01
# @version 0.1
"""@package docstring
"""

import numpy as np
#np.set_printoptions(threshold=sys.maxsize) # Printing all the weights

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3';                  # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys

import tensorflow as tf
from tensorflow import keras

from ctypes import *


# Importer la bibliothèque C++
monbib = CDLL("mes_fon_CPP/monbib.so");                    # Bibliothèque CPP
#print(monbib);  monbib.print_cpp();                       # preuve la bibliothèque


def conv_k5_npT(entree, filtres, biases, M, scale, offset_ent, offset_sor, verbose):
  ''' Cette fonction fait la convolution (i.e. matmul, bias,
      clamp, ReLU6, quantization) avec kernel size=(5,5) et
      padding=SAME utilisée pour tflite en se servir de Numpy
      et Tensors 4D. 
      entree:            Tensor 4D  Numpy de entrée (uint8).
      filtres:           Tensor 4D de filtres [filtres, 5, 5, chaînes] (uint8).
      biases:            Liste de biases  (int32).
      M:                 Liste de facteurs M.
      scale:             Scale de la couche.
      offset_ent:        Offset d'entrée.
      offset_sor:        Offset de sortie.                
      cv_tab:            Rendre la convolution en tensor 4D'''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  min_val = -128; max_val = 127;                           # min/max pour int8
  ch_ent, rg_ent, co_ent, cc_ent = entree.shape;           # [chaînes (RGB), rangs, colonnes, cartes de caractéristiques]
  nm_fil, rg_fil, co_fil, ch_fil = filtres.shape;          # [nombre des filtres, rangs, colonnes, chaînes]
  cv_tab = np.zeros([1, rg_ent-4, co_ent-4, nm_fil]);      # [1, rangs, colonnes, cartes de caractéristiques]
  #print("entree.shape:", entree.shape); print("filtres.shape:", filtres.shape); print("cv_tab.shape:", cv_tab.shape);

  for f in range(0, nm_fil):                               # boucle pour les différents filtres

    # shift: facteur n < 0 de 2^(-n), 
    # multiplier: facteur de échelle M0, output_shift: facteur n < 0 de 2^(-n).
    shift = c_int(); M0 = c_int();     
    monbib.QuantizeMultiplier( c_double(M[f]/scale), byref(M0), byref(shift) );
    print("M/scale= ", M[f]/scale, "M0= ", M0.value, "shift= ", shift.value);
 
    for i in range(0, rg_ent - 4):                         # boucle pour les rangs de l'entrée
      for j in range(0, co_ent - 4):                       # boucle pour les colonnes de l'entrée
        for k in range(0, cc_ent):                         # boucle pour les cartes de carácteristiques de l'entrée = chaînes de filtres = cc de la entrée 
 
          # Faire la convolution
          #----------------------------------
          ent = entree[0, 0+i:5+i, 0+j:5+j, k];            # Desplacer l'entrée
          #print("\n ent: \n", ent);
          ent = ent + offset_ent;                          # de tflite: input_val+input_offset
          cv_tab[0, i, j, f] = cv_tab[0, i, j, f] + np.tensordot(ent, filtres[f, :, :, k]);  # W*x per chaînes
          #print("\n ent + offset_ent: \n", ent);
          #print("\n filtres: \n", filtres[f, :, :, k]);
          #print("\n w*x: \n", cv_tab[0, i, j, f]);

        # Ajouter le bias
        #----------------------------------
        cv_tab[0, i, j, f] = cv_tab[0, i, j, f] + biases[f];# W*x + bias
        #print("\n w*x + bias: \n", cv_tab[0, i, j, f]);
 
        # À échelle réduite
        #----------------------------------
        nom_int = c_int( int(cv_tab[0, i, j, f]) );        # Ctype int32_t
        monbib.MultiplyByQuantizedMultiplier.restype = \
          c_int;                                           # Valeur à rendre
        cv_tab[0, i, j, f] = monbib.MultiplyByQuantizedMultiplier(nom_int, M0, shift);
        #print("\n MultiplyByQuantizedMultiplier: \n", cv_tab[0, i, j, f]);
 
        # Cast à uint8 [0, 255]
        #----------------------------------
        cv_tab[0, i, j, f] = \
          min( max(cv_tab[0, i, j, f], 0), 255 );          # Cast à uint8
        #print("\n Cast à [0, 255]: \n", cv_tab[0, i, j, f]);
        cv_tab[0, i, j, f] = cv_tab[0, i, j, f] + \
          offset_sor;                                      # de tflite: acc + output_offset
        #print("\n (w*x+bias) + offset_sor: \n", cv_tab[0, i, j, f]);

        # Faire le clamp entre [-128, 127]
        #----------------------------------
        cv_tab[0, i, j, f] = max( cv_tab[0, i, j, f], min_val );
        cv_tab[0, i, j, f] = min( cv_tab[0, i, j, f], max_val );
        #print("\n min( max(conv, -128), 127): \n", cv_tab[0, i, j, f]);
        #sys.exit(0);                                              # Terminer l'execution$
  return cv_tab;


def dense_npT(entree, params, biases, M, scale, offset_ent, offset_sor, verbose):
  ''' Cette fonction calcule la couche full-connected (Dense) 
      utilisée par Tensorflowlite avec padding=VALID et
      activation=None en utilisant Numpy.
      entree:            Tensor 2D  Numpy de entrée (uint8).
      params:            Tensor 2D de parametres [# de classes, cc*width*height] (uint8).
      biases:            Liste de biases  (int32).
      M:                 Liste de facteurs M.
      scale:             Scale de la couche.
      offset_ent:        Offset d'entrée.
      offset_sor:        Offset de sortie.
      fc_vec:            Vecteur Numpy de sortie.          '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  min_val = -128; max_val = 127;                           # Cast de int32 à int8  
  rg_ent, co_ent = entree.shape;                           # [rangs, colonnes]
  rg_par, co_par = params.shape;                           # [rangs, colonnes]
  fc_vec = [];                                             # Vecteur de sortiee

  for cls in range(0, rg_par):                             # boucle pour les classes

    # shift: facteur n < 0 de 2^(-n),
    # multiplier: facteur de échelle M0, output_shift: facteur n < 0 de 2^(-n).
    shift = c_int(); M0 = c_int();
    monbib.QuantizeMultiplier( c_double(M[0]/scale), byref(M0), byref(shift) );
    print("M/scale= ", M[0]/scale, "M0= ", M0.value, "shift= ", shift.value);

    # Full-connected
    #----------------------------------
    ent = entree + offset_ent;                             # de tflite: input_val + input_offset
    Wx_b = \
      np.tensordot([ent[0]], [params[cls]]) + biases[cls]; # W*x + b
    #print(Wx_b);

    # À échelle réduite  
    #---------------------------------- 
    nom_int = c_int( int(Wx_b) );                          # Ctype int32_t
    monbib.MultiplyByQuantizedMultiplier.restype = \
      c_int;                                               # Valeur à rendre 
    Wx_b = monbib.MultiplyByQuantizedMultiplier(nom_int, M0, shift);


    # Faire le clamp entre [-128, 127]
    #----------------------------------   
    Wx_b = Wx_b + offset_sor;                              # de tflite: acc + output_offset
    Wx_b = max( Wx_b, min_val ); 
    Wx_b = min( Wx_b, max_val ); 

    fc_vec.append(Wx_b);
  return fc_vec;


def flatten_npT(entree, verbose):
  ''' Cette fonction fait l'operation  flatten pour convertir
      un Tensor 4D à un tableau des vecteurs en utilisant 
      Numpy.E.g.: un tensor (1, 8, 8, 128) à un tableau
      (1, 8192).
      entree:   Tensor 4D  Numpy de entrée (int8).
      flat_vec: Tableau Numpy de sortie.                   '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize);

  # Initialisation
  #----------------------------------
  ch_ent, rg_ent, co_ent, cc_ent = entree.shape;           # [chaînes, rangs, colonnes, cartes de caractéristiques]
  #print("entree.shape:", entree.shape);
  flat_vec = [];                                           # Tableau de sortie

  for i in range(0, rg_ent):
    for j in range(0, co_ent):
      for k in range(0, cc_ent):

        # Flatten (rangs*colonnes*cartes)
        #----------------------------------
        flat_vec.append(entree[0, i, j, k]);               # Ajouter éléments au tableau.

  flat_vec = np.asarray([flat_vec]);
  return flat_vec;      


def lir_h5_mod(fic_mod, verbose):
  ''' Cette fonction imprime les paramètres du modèle en 
      format hdf5: poids et biais.
      fic_mod:   nombre et position du modèle hdf5.              
      verbose:   imprimir les paramètres: 0 min, 1 tout.   '''

  if (verbose == 1):                                       # Imprimer tous les paramètres
    np.set_printoptions(threshold=sys.maxsize); 

  # Charger le modèle
  #----------------------------------
  model = tf.keras.models.load_model(fic_mod);
  model.summary();

  # Imprimer poids per couche
  #----------------------------------
  print(" ");
  for layer in model.layers:
    print( "--> couche: ", layer ); 
    print( "--> configuration du couche: ", layer.get_config() ); 
    print( "--> paramètres du couche: ", layer.get_weights() );
    print(" ");


def lir_tflite_mod(fic_mod, verbose):
  ''' Cette fonction imprime les paramètres du modèle en 
      format tflite: poids et biais.
      fic_mod:   nombre et position du modèle tflite. 
      verbose:   imprimir les paramètres: 0 min, 1 tout.   '''

  if (verbose == 1):                                       # Imprimer tous les paramètres
    np.set_printoptions(threshold=sys.maxsize); 

  # Charger le modèle tflite en allouer tensors.
  #----------------------------------
  interpreters = tf.lite.Interpreter( model_path=fic_mod );
  interpreters.allocate_tensors();
  
  # Obtenir tensor de entrée et sortie.
  input_details  = interpreters.get_input_details();
  output_details = interpreters.get_output_details();
  tensor_details = interpreters.get_tensor_details();
  #print("tensor_details: ", tensor_details);
  
  # Imprimer poids per couche
  #----------------------------------
  print(" ");
  for i in range( 0, len(interpreters.get_tensor_details()) ):
    try: 
      print("prenom: ", interpreters.get_tensor_details()[i]["name"] );
      if ("sequential" in interpreters.get_tensor_details()[i]["name"] ):
        print("paramètres: ", interpreters.get_tensor(i) );
        print(" ");
    except:
      print("Le tensor ",i," est null.");


def maxpool_npT( entree, verbose):
  ''' Cette fonction fait l'operation  MaxPool avec un 
      fenêtre de (2,2) utilisée  pour tflite en 
      utilisant Numpy et Tensors 4D. 
      entree:  Tensor 4D Numpy de entrée (int8).                
      stride = (2,2) --> str2
      mp_tab: MaxPooling tableau Numpy de sortie.          '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  ch_ent, rg_ent, co_ent, cc_ent = entree.shape;           # Chaînes, rangs, colonnes, cartes de caractéristiques
  rg_mp = int(rg_ent/2); co_mp = int(co_ent/2);            # Rangs et colonnes de sortie
  mp_tab = np.zeros([ch_ent, rg_mp, co_mp, cc_ent]);       # [chaînes, rangs, colonees, cartes] 

  # MaxPooling(pool_size=(2,2), strides=(2,2))  
  #----------------------------------
  for i in range(0, rg_mp):                                # boucle pour les rangs
    for j in range(0, co_mp):                              # boucle pour les colonnes
      for k in range(0, cc_ent):                           # boucle pour les cartes de caractéristiques.
        mp_tab[0, i, j, k] = np.max( entree[0, 2*i:2*i+2, 2*j:2*j+2, k] ); 
  return mp_tab;


def quant_npT( entree, scale, zero_point, verbose):
  ''' Cette fonction fait la quantization utilisée pour 
      tflite en utilisant Numpy avec Tensors. 
      entree:     tableau Numpy de entrée-Tensor 4D.
      scale:      scale des quantization.
      zero_point: position des zero.                      
      clampled:   tableau Numpy de sortie.                 '''

  if (verbose == 1):                                       # Imprimer tout
    np.set_printoptions(threshold=sys.maxsize); 

  # Initialisation
  #----------------------------------
  chaines, rangs, colonnes, profondeur = entree.shape;     # [chaînes, rangs, colonnes, profondeur]
  print("nombre des rangs:    ", rangs); 
  print("nombre des colonnes: ", colonnes);
  clamped = np.zeros( [chaines, rangs, colonnes, profondeur] );
  min_val = -128; max_val = 127;                           # min/max pour int8
  
  # Faire la quantization
  #----------------------------------
  unclamped = ( entree / scale ) + zero_point;             # q = r/s + z
  #print(unclamped);
 
  # On utilise clamped pour l'entrée
  for i  in range(0, rangs):                               # rangs
    for j  in range(0, colonnes):                          # colonnes
      clamped[0, i, j, 0] = max( unclamped[0, i, j, 0], min_val );   
      clamped[0, i, j, 0] = min(   clamped[0, i, j, 0], max_val );  
      clamped[0, i, j, 0] = int( np.round(clamped[0, i, j, 0])  );       
  return clamped;


#print("                                      ");
#print("**************************************");
#print("*  ¡Merci d'utiliser ce logiciel!    *");
#print("*             (8-)                   *");
#print("**************************************");
