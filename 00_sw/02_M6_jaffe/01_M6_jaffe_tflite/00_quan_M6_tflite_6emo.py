# -*- coding: utf-8 -*-
## 
# @file    00_quan_M6_jaffe_tflite_6emo.py 
# @brief   M6 avec Jaffe + dlib + lbp-var64 + aug15 + 6emo, quantifié avec tflite. 
# @details Module pour faire la quantization du modèle M5 avec 
#          Jaffe + dlib + lbp-var64 + aug15 pour 6 émotions, 
#          quantifié avec tflite. 
#          Pris du: Quantization aware training in Keras example.
# @author  Dorfell Parra - dlparrap@unal.edu.co
# @date    2021/03/24
# @version 0.1
"""@package docstring
"""


import matplotlib as mpl 
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12; 
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True; 
mpl.rcParams['font.family'] = 'sans-serif'; 
mpl.rcParams['mathtext.fontset']    = 'dejavusans'; 
mpl.rcParams.update({'font.size': 12}); 
import matplotlib.pyplot as plt

import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

import sys 
np.set_printoptions(threshold=sys.maxsize) # Printing all the weights 
import tempfile
import tensorflow as tf
physical_devices = tf.config.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(physical_devices[0], True)
import tensorflow_model_optimization as tfmot
from tensorflow  import keras

from matplotlib.backends.backend_pdf import PdfPages

 
print(" \n ");
print("************************************** "); 
print(" Charger l'ensemble de données         "); 
print("************************************** ");
# Expressions: angry, disgust, fear, happy, sad, surprise, neutral
fer_eti = ["HA","AN","DI","FE","SA","SU"];                 # Expressions abrégés. 
fer_lab = [   0,   1,   2,   3,   4,  5 ];                 # Index des expressions.
nom_cls = 6;                                               # Nombre des classes.

# 10-fold cross validation: La totalité de l'ensemble de données est
# divisé en 10 sets, 9 pour l'entraînement et 1 pour le testing.

# Charger entraînement: Jaffe + pré-traitement avec dlib + lbp-var + 15 augmentations
x_train = np.load("../../db_transfigure/jaffe_dlib_var64_aug15_6emo_img.npy");      
y_train = np.load("../../db_transfigure/jaffe_dlib_var64_aug15_6emo_lab.npy");

# Charger testing: Jaffe + pré-traitement avec dlib + lbp-var
x_test = np.load("../../db_transfigure/jaffe_dlib_var64_test_6emo_img.npy");      
y_test = np.load("../../db_transfigure/jaffe_dlib_var64_test_6emo_lab.npy");
 
print("Ensemble des données: jaffe_dlib_var64_aug15_6emo_img.npy");
#print("Img dat: ", img_dat, img_dat.shape, type(img_dat), img_lab);
print("Taille des images: ",     x_train.shape, type(x_train));
print("Taille des étiquettes: ", y_train.shape, type(y_train));

x_train = np.array(x_train, "float32");
x_test  = np.array(x_test,  "float32");
print(x_train.shape[0], 'train samples');
print(x_test.shape[0],  'test samples' );

# Dessiner quelques données
#fig = plt.figure(); 
#ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[y_train[0]]); ax1.imshow(x_train[0], cmap="gray");   
#ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[y_train[1]]); ax2.imshow(x_train[1], cmap="gray");
#ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[y_train[2]]); ax3.imshow(x_train[2], cmap="gray");
#ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[y_train[3]]); ax4.imshow(x_train[3], cmap="gray");
#fig.suptitle("Images avec pré-traitement", fontsize=12); plt.tight_layout();
#fig.subplots_adjust(top=0.88);
#plt.show();                                                 
sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** "); 
print(" Modèle                                "); 
print("************************************** "); 
# Définir l'architecture du modèle 
model = keras.Sequential( [
  keras.layers.InputLayer( input_shape=(64, 64) ),   
  keras.layers.Reshape( target_shape=(64, 64, 1) ),    

  keras.layers.Conv2D( filters=32, kernel_size=(5, 5), strides=(1,1), padding="SAME", activation="relu"),   
  keras.layers.MaxPooling2D( pool_size=(2, 2), strides=(2,2) ),   

  keras.layers.Conv2D( filters=64, kernel_size=(5, 5), strides=(1,1), padding="SAME", activation="relu"),   
  keras.layers.MaxPooling2D( pool_size=(2, 2), strides=(2,2) ),   

  keras.layers.Conv2D( filters=128, kernel_size=(5, 5), strides=(1,1), padding="SAME", activation="relu"),   
  keras.layers.MaxPooling2D( pool_size=(2, 2), strides=(2,2) ),   

  keras.layers.Flatten( ),   

  keras.layers.Dense(nom_cls, activation=None) ] ); 

model.summary();
keras.utils.plot_model(model, to_file="model.png", show_shapes="True");
#sys.exit(0);                                              # Terminer l'execution


print("************************************** "); 
print(" Entraînement...                       "); 
print("************************************** ");
# Paramètres d'entraînement
batch_size = 32;                                           # Mini-batch size. 
epochs     = 25;                                           # Nombre des époques.
lr_val     = 0.001;                                        # Learning rate. 
print("batch_size = ", batch_size);
print("learning rate =  ", lr_val);

# Entraînement ...
opt = keras.optimizers.Adam(learning_rate=lr_val);
model.compile(optimizer=opt,
              loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy']);

his = model.fit(                                           # Sauvegarder l'histoire
        x_train, y_train, batch_size=batch_size,
        steps_per_epoch = len(x_train) / batch_size, 
        epochs=epochs, verbose=2, 
        validation_data=(x_test, y_test) );  


print("************************************** "); 
print(" Evaluation de performance             "); 
print("************************************** ");
# Evaluating
score = model.evaluate(x_test, y_test, verbose=0);
print('Avec la collection de test');
print('--> Perte:',          score[0]);
print('--> Exactitude:', 100*score[1]);

predictions = model.predict(x_test);
predicted_label = np.argmax(predictions[0]);

#print(history.history);
#print(his.history.keys());

# Fonction qui sert à mesurer le performance du modèle 
with PdfPages("graphique_entrainement.pdf")  as export_pdf:
    
    # summarize history for accuracy
    plt.plot(his.history['accuracy']);
    plt.plot(his.history['val_accuracy']);
    plt.title('Exactitude du modèle');
    plt.ylabel('Exactitude'); plt.xlabel('époque');
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();
    
    # summarize history for loss
    plt.plot(his.history['loss']);
    plt.plot(his.history['val_loss']);
    plt.title('Perte du modèle');
    plt.ylabel('Perte'); plt.xlabel('époque');
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();

# En sauvegardant le modèle en hdf5 
#------------------------------
model.save("models/M6_6emo.h5");
model.save_weights("models/M6_6emo_weights.h5");
#sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** ");
print(" Quantization aware model              ");
print("************************************** ");
# Quantize model
quantize_model = tfmot.quantization.keras.quantize_model;

# q_aware stands for quantization aware
q_aware_model = quantize_model(model);

# "quantize_model" requieres a recompile
q_aware_model.compile(optimizer=opt,
                      loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                      metrics=["accuracy"] );                       
q_aware_model.summary();

# Train and evaluate the model against baseline
train_images_subset = x_train[0:1000];                     # 1000 de 2640 échantillons
train_labels_subset = y_train[0:1000];                     # 1000 de 2640 échantillons

q_aware_model.fit( train_images_subset, train_labels_subset, batch_size=300, epochs=5, validation_data=(x_test, y_test) );
 
_, baseline_model_accuracy =         model.evaluate( x_test, y_test, verbose=0 );
_, q_aware_model_accuracy  = q_aware_model.evaluate( x_test, y_test, verbose=0 );

print("Baseline test accuracy: ", baseline_model_accuracy);
print("q aware  test accuracy: ", q_aware_model_accuracy);
#sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** ");
print(" Quantized quantization aware model    ");
print("************************************** ");
# Create quantized model for TFLite backend
converter = tf.lite.TFLiteConverter.from_keras_model(q_aware_model);
converter.optimizations = [ tf.lite.Optimize.DEFAULT ];

quantized_tflite_model = converter.convert();

# See persistence of accuracy from TF to TFLite
def evaluate_model(interpreter):
  input_index  = interpreter.get_input_details()[0]["index"];
  output_index = interpreter.get_output_details()[0]["index"];
 
  # Run predictions on every image in the "test" dataset
  prediction_digits = [];
  for i, test_image in enumerate(x_test):

    #plt.imshow(test_image, cmap="gray", interpolation=None);
    #plt.title(test_labels[i]);
    #plt.show();
    
    if ( (i % 1) == 0):
    #if ( (i % 1000) == 0):
      print("Evaluated on {n} results so far.".format( n=i ) );
   
    # Pre-processing: add batch dimension and convert to float32 to match
    # the model's input data format.
    test_image = np.expand_dims( test_image, axis=0 ).astype( np.float32 );
    interpreter.set_tensor( input_index, test_image );

    # Run inference
    interpreter.invoke();

    # Post-processing: remove batch dimension and find the digit with high probability
    output = interpreter.tensor( output_index );
    digit = np.argmax( output()[0] );
    prediction_digits.append( digit );

  print(" ");
  # Compare prediction results with ground truth labels to calculate accuracy 
  prediction_digits = np.array( prediction_digits );
  accuracy = ( prediction_digits == y_test ).mean();
  return accuracy


interpreter = tf.lite.Interpreter( model_content=quantized_tflite_model );
interpreter.allocate_tensors();
quantized_q_aware_accuracy = evaluate_model( interpreter );

print("quantized q aware TFLite test_accuracy: ", quantized_q_aware_accuracy );
print("q aware                  test_accuracy: ", q_aware_model_accuracy );

# En sauvegardant le modèle tflite 
with tf.io.gfile.GFile('models/M6_quantized_tflite.tflite', 'wb') as f:
  f.write(quantized_tflite_model);


print(" \n ");
print("************************************** ");
print(" Quantizated model                     ");
print("************************************** ");
# See 4x smaller model from quantization

# Crate float TFLite model
float_converter    = tf.lite.TFLiteConverter.from_keras_model(model);
float_tflite_model = float_converter.convert();

# Measure sizes of models
_, float_file = tempfile.mkstemp(".tflite");
_, quant_file = tempfile.mkstemp(".tflite");

interpreter_float = tf.lite.Interpreter( model_content=float_tflite_model );
interpreter_float.allocate_tensors();
quantized_tffloat_accuracy = evaluate_model( interpreter_float );

# En sauvegardant le modèle tflite 
with tf.io.gfile.GFile('models/M6_float_tflite_model.tflite', 'wb') as f:
  f.write(float_tflite_model);

# Taille du modèles en disque
with open( quant_file, "wb" ) as f:
  f.write( quantized_tflite_model );

with open( float_file, "wb" ) as f:
  f.write( float_tflite_model );

print("Float     model in MB: ", os.path.getsize(float_file) / float( 2**20) );
print("Quantized model in MB: ", os.path.getsize(quant_file) / float( 2**20) );


print(" \n ");
print("**************************************");
print("***            Accuracies          ***");
print("--------------------------------------");
print("TF float (default): ", baseline_model_accuracy);
print("TF q_aware:         ", q_aware_model_accuracy);
print("Quantized q_aware (TFlite):  ", quantized_q_aware_accuracy );
print("Quantized TF float (TFlite): ", quantized_tffloat_accuracy );


print("                                      ");
print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
