-------------------------------------------------------------------

     =========================================================
     fer_sys_dev/00_sw/01_etu_mod_jaffe/ - Étude des modèles 
     avec Jaffe
     =========================================================

                         Liste des fichiers
                         ---------------

Logiciels pour étudier le comportement des différents modèles avec différentes représentations en utilisant différentes transfigurations de Jaffe.

00_M1_fp/
 - Module pour étudier le comportement du modèle M1 en utilisant une représentation du point flottant. Le modèle M1 est composée du couches CNN. Maxpooling, flatten, et dense.

00_MX_fp/
 - Dossier de plusiers preuves pour trouver un modèle de comportement acceptable. Le modèle choici de ces preuves s'appele M6.

README.md

 - Vous êtes en train de le lire maintenant 8-P. 
