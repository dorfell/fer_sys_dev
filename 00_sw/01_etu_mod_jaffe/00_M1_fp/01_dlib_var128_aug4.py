# -*- coding: utf-8 -*- 
##
# @file    01_dlib_var128_aug4.py 
# @brief   M1 avec Jaffe + pré-traitement avec dlib + lbp-var et 4 augmentations.
# @details Module pour faire l'entraînement de modèle M1 en 
#          utilisant l'ensemble des données Jaffe pré-traité avec 
#          dlib, lbp-var et 4 augmentations. Taille du 128 x 128 pixels. 
# @author  Dorfell Parra - dlparrap@unal.edu.co 
# @date    2021/02/14 
# @version 0.1 
"""@package docstring 
""" 


import matplotlib as mpl 
mpl.rcParams['legend.fontsize'] = 12;
mpl.rcParams['axes.labelsize']  = 12;
mpl.rcParams['xtick.labelsize'] = 12; 
mpl.rcParams['ytick.labelsize'] = 12;
mpl.rcParams['text.usetex'] = True; 
mpl.rcParams['font.family'] = 'sans-serif'; 
mpl.rcParams['mathtext.fontset']    = 'dejavusans'; 
mpl.rcParams.update({'font.size': 12}); 
import matplotlib.pyplot as plt 

import numpy as np 
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'; # TF debug messages 
# 0 = all messages are logged (default behavior) 
# 1 = INFO messages are not printed 
# 2 = INFO and WARNING messages are not printed 
# 3 = INFO, WARNING, and ERROR messages are not printed 

import sys 
np.set_printoptions(threshold=sys.maxsize) # Printing all the weights 
import tempfile 
import tensorflow as tf 
physical_devices = tf.config.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(physical_devices[0], True)
from   tensorflow import keras 

from matplotlib.backends.backend_pdf import PdfPages


print(" \n ");
print("************************************** "); 
print(" Charger l'ensemble de données         ");

print("************************************** ");
# Expressions: angry, disgust, fear, happy, sad, surprise, neutral
fer_eti = ["NE","HA","AN","DI","FE","SA","SU"];            # Expressions abrégés. 
fer_lab = [ 0,   1,   2,   3,   4,   5,   6];              # Index des expressions.
nom_cls = 7;                                               # Nombre des classes.

# Charger Jaffe + pré-traitement avec dlib + lbp-var + 4 augmentations 
img_dat = np.load("../../db_transfigure/jaffe_dlib_var128_aug4_img.npy");     
img_lab = np.load("../../db_transfigure/jaffe_dlib_var128_aug4_lab.npy");
 
print("Ensemble des données: jaffe_dlib_var128_aug4_img.npy");
#print("Img dat: ", img_dat, img_dat.shape, type(img_dat), img_lab);
print("Taille des images: ",     img_dat.shape, type(img_dat));
print("Taille des étiquettes: ", img_lab.shape, type(img_lab));

# Dessiner quelques données
#fig = plt.figure(); 
#ax1 = fig.add_subplot(2, 2, 1); ax1.set_title(fer_eti[img_lab[0]]); ax1.imshow(img_dat[0], cmap="gray");   
#ax2 = fig.add_subplot(2, 2, 2); ax2.set_title(fer_eti[img_lab[1]]); ax2.imshow(img_dat[1], cmap="gray");
#ax3 = fig.add_subplot(2, 2, 3); ax3.set_title(fer_eti[img_lab[2]]); ax3.imshow(img_dat[2], cmap="gray");
#ax4 = fig.add_subplot(2, 2, 4); ax4.set_title(fer_eti[img_lab[3]]); ax4.imshow(img_dat[3], cmap="gray");
#fig.suptitle("Images avec pré-traitement", fontsize=12); plt.tight_layout();
#fig.subplots_adjust(top=0.88);
#plt.show();                                                 

# Passer img à tensors (4D)
# Ex: [[1, 2, 3], [1, 2, 3]] --> [ [[1], [2], [3]], [[1], [2], [3]] ] 
img_4D = [];
for idx, img in enumerate(img_dat):
  tmp_4D = np.expand_dims(img, axis=2);                    # En ajoutant 1 dimension 
  img_4D.append( tmp_4D );
img_4D = np.array(img_4D);
print("Taille des images 4D: ", img_4D.shape );

# 10-fold cross validation: La totalité de l'ensemble de données est
# divisé en 10 sets, 9 pour l'entraînement et 1 pour le testing.
x_train = np.array(img_4D[0:954],  "float32");
y_train = np.array(img_lab[0:954], "float32"); 
print("x_train shape: ", x_train.shape);

x_test  = np.array(img_4D[954:1064],  "float32");
y_test  = np.array(img_lab[954:1064], "float32"); 

print(x_train.shape[0], 'train samples');
print(x_test.shape[0],  'test samples' );
#sys.exit(0);                                              # Terminer l'execution


print(" \n ");
print("************************************** "); 
print(" Modèle                                "); 
print("************************************** "); 
# Définir l'architecture du modèle 
model = keras.Sequential( [
  keras.layers.InputLayer( input_shape=(128, 128) ),   
  keras.layers.Reshape( target_shape=(128,128, 1) ),    

  keras.layers.Conv2D( filters=24, kernel_size=(3, 3), strides=(1,1), padding="VALID", activation="relu"),   
  keras.layers.MaxPooling2D( pool_size=(2, 2) ),   

  keras.layers.Flatten( ),   
  keras.layers.Dense(nom_cls) ] ); 

model.summary();
keras.utils.plot_model(model, to_file="model.png", show_shapes="True");
#sys.exit(0);                                              # Terminer l'execution


print("************************************** "); 
print(" Entraînement...                       "); 
print("************************************** ");
# Paramètres d'entraînement
batch_size = 32;                                           # Mini-batch size. 
epochs     = 100;                                          # Nombre des époques.
lr_val     = 0.01;                                         # Learning rate. 

# Entraînement ...
opt = keras.optimizers.Adam(learning_rate=lr_val);
model.compile(optimizer=opt,
              loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy']);

his = model.fit(                                           # Sauvegarder l'histoire
        x_train, y_train, batch_size=batch_size,
        steps_per_epoch = len(x_train) / batch_size, 
        epochs=epochs, verbose=2, 
        validation_data=(x_test, y_test), workers=8);  
#sys.exit(0);                                              # Terminer l'execution


print("************************************** "); 
print(" Evaluation de performance             "); 
print("************************************** ");
# Evaluating
score = model.evaluate(x_test, y_test, verbose=0);
print('Avec la collection de test');
print('--> Perte:',          score[0]);
print('--> Exactitude:', 100*score[1]);

predictions = model.predict(x_test);
predicted_label = np.argmax(predictions[0]);

#print(history.history);
#print(his.history.keys());

# Fonction qui sert à mesurer le performance du modèle 
with PdfPages("graphique_entrainement.pdf")  as export_pdf:
    
    # summarize history for accuracy
    plt.plot(his.history['accuracy']);
    plt.plot(his.history['val_accuracy']);
    plt.title('Exactitude du modèle');
    plt.ylabel('Exactitude'); plt.xlabel('époque');
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();
    
    # summarize history for loss
    plt.plot(his.history['loss']);
    plt.plot(his.history['val_loss']);
    plt.title('Perte du modèle');
    plt.ylabel('Perte'); plt.xlabel('époque');
    plt.legend(['train', 'test'], loc='upper left');
    export_pdf.savefig(); plt.close();


print("**************************************");
print("*  ¡Merci d'utiliser ce logiciel!    *");
print("*             (8-)                   *");
print("**************************************");
