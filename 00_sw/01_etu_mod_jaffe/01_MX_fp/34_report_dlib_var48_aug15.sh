
#!/bin/bash

echo "-------------------------"
echo "Créer un reportage de    "
echo "34_dlib_var48_aug15.py  "
echo "-------------------------"

echo "--> Entraînement du modèle ..."
rm *.log *.png 
python3 34_dlib_var48_aug15.py >>  entrainement.log

echo "--> Entrainement.log à pdf ..."
#enscript -p entrainement.ps entrainement.log
#ps2pdf entrainement.ps entrainement.pdf
enscript entrainement.log --output=- | ps2pdf - >  entrainement.pdf

echo "--> Convertir le modèle à pdf ..."
convert model.png model.pdf

echo "--> Ajouter tout à un seul pdf ..."
pdftk entrainement.pdf model.pdf graphique_entrainement.pdf cat output 34_entrainement_dlib_var48_aug15_20210228.pdf

echo "--> Éliminer les fichiers crées ..."
rm *.log *.png entrainement.pdf model.pdf graphique_entrainement.pdf 
echo "  "
