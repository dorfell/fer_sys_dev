-------------------------------------------------------------------

     =========================================================
     fer_sys_dev/00_sw - Développement en software d'un système
                         pour applications FER
     =========================================================

                         Liste des fichiers
                         ---------------

Fichiers pour le développement software d'un système pour applications FER.

00_jaffe_pre_et_traitement/
 - Module pour faire le pré-traitement avec Dlib, et le traitement des images avec motif locaux binaire LBP avec méthode var, détection de contours avec Canny 1986 et le redimensionnement avec l'interpolation Lanczos4.

01_etu_mod_jaffe/
 - Module pour étudier des modèles FER avec différentes représentations du Jaffe.

02_M6_jaffe/
 - Module pour étudier l'inference de modèle M6 pour applications FER avec Jaffe + dlib + lbp-var64 + aug15. ¿Est-que Tensorflow lite peut être utilisée?.

db/
 - Ensemble des données de référence pour le développement software d'un système FER.

db_transfigure/
 - Ensemble des données transfiguré après le pré-traitement et le traitement.

modèles  
 - Dossier avec modèles en format HDF5.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
