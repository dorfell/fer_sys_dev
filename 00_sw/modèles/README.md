-------------------------------------------------------------------

     =========================================================
     fer_acc_dev/00_sw - Développement en software d'un accelerateur
                         pour applications FER
     =========================================================

                         Liste des fichiers
                         ---------------

Fichiers pour le développement software d'un accelerateur FER.

00_fp_cifar10/
 - Module pour créer un modèle pour CIFAR10 en Tensorflow et Keras. Pour faire
   reproduire les calculs avec Numpy et utilisant point flottant.

00_fp_mnist/
 - Module pour créer un modèle pour MNIST en Tensorflow et Keras, et reproduire
   les calculs avec Numpy et utilisant point flottant.

01_quant_mnist 
 - Implementation des modèles TFlite avec l'ensamble des données Mnist et paramètres 
   répresentées avec nombres entiers.

02_bnn_cifar10 
 - Implementation des modèles TFlite avec l'ensamble des données CIFAR10 et
   paramètres répresentées en manière binnaire (e.g. -1 et 1).

03_test_ctypes/ 
 - Example de comme utiliser fonctions de C++ en Python avec Ctypes. L'exemple 
   permet de passer pointeurs, et utiliser la fonction std::frexp.

04_test_conv2D/ 
 - Test de la couche Conv2D du Keras avec TensorFlow, Numpy et comment sauve-garder
   le modèle.

05_pretraitement_dlib_jaffe/ 
 - Pretraitment d'ensamble de données Jaffe avec dlib, en la technique de local 
   binary pattterns (LBP). 

00_conv_fp.py
 - Logiciel pour convertir entre decimal et représentation du point fixé.

mes_fonctions.py
 - Module avec les fonctions utilisées dans ce entredepôt.

modèles  
 - Dossier avec modèles en format HDF5.

README.md
 - Vous êtes en train de le lire maintenant 8-P. 
